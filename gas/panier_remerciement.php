<?php
/* Page permettant de remercie le client apr�s sa commande */
require("includes/page_top.php");

/* Permet de charger le fichier de langue de cette page qui se trouve dans INCULDES/LANGUAGES/ */ 
require(DIR_LANGUAGES . basename($_SERVER['PHP_SELF']));

// DEBUT devis initialisation variable
$_SESSION['id_devis']='';
$_SESSION['remise_panier_devis']='';
$_SESSION['total_panier_devis']='';
$_SESSION['devis_moyen_paiement']='';
// Fin devis


//$etat = (int)$_GET['etat'];
$etat = !empty($_GET['etat']) ? $_GET['etat'] : '';




if ($etat==1 || $etat==99) { // Si $etat est egale a 1 ou 99 (paiement cb) initialisation des variables suivantes
	$_SESSION['customer_id'] = (int)$_GET['arg4'];
	$customer_id = (int)$_GET['arg4'];
} elseif (empty($_SESSION['customer_id'])) { // si la session customer_id est vide on redirige sur l'accueil 
	echo '<meta http-equiv="refresh" content="0; URL=https://www.generalarmystore.fr/gas/index.php">';	
} else { // sinon on initialise la variable $customer_id avec $_SESSION['customer_id']
	$customer_id=$_SESSION['customer_id'];
}

// Thierry 12-03-2020 
$session_mode_paiement = isset($_SESSION['mode_paiement']) ? $_SESSION['mode_paiement'] : NULL;
if(($etat==1 || $etat==99) || $session_mode_paiement!=1 ){
	 
	// Inclusion des fonctions d'affichage du panier
	require("includes/fonctions/fonctions_panier.php");
	
	if (empty($customer_id)) { // si la session customer_id est vide on redirige sur l'accueil 
		echo '<meta http-equiv="refresh" content="0; URL=https://www.generalarmystore.fr/gas/compte/connexion.php?panier=true">';
	} else { // sinon
		require("includes/meta/panier_remerciement.php");
		require("includes/meta_head.php");
		require("includes/header.php");
		?>
		<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/panier_paiement.js"></script>
		<div id="corps">
		
			<?php
			$req_commande_client=tep_db_query("select * from orders where orders_id = (select max(orders_id) from orders where customers_id=".$customer_id.") ");
			$res_commande=tep_db_fetch_array($req_commande_client);
			?>    
			<!-- block text -->
			 <div style="display:block; margin-top:15px; float:left;">
				  <div  id="bordure_mdp_haut_verte"></div> 
				  <div id="bordure_mdp_milieu_vert">
					 <div style="padding:0px 20px 0px 20px;">
					  <span style="color:#788403; font-size:15px;">
						<strong>Votre commande vient d'&ecirc;tre prise en compte avec succ&eacute;s ! </strong>
					  </span>
					 </div>
				  </div>
				<div id="bordure_mdp_bas_verte" style="margin-bottom:35px;"></div> 
			</div>
			<div class="clear"></div>
				
			<div style="float:left; margin-right:7px;"><img src="template/base/remerciement/image-carton.png" /></div>
				<div style="float:left; background-image:url(template/base/remerciement/cadre.gif); width:667px; height:190px; text-align:left;">
				<div style="margin-left:20px; margin-top:20px; font-size:13px;">
					Cette commande a &eacute;t&eacute; enregistr&eacute;e par Group Army Store / G�n�ral Army Store<br /><br />
					Date de la Commande : <span style="color:<?php echo COULEUR_1; ?>; font-weight:bold;"><?php
											$timestamp_facture = strtotime($res_commande['date_purchased']);
											echo $date_facture = html_entity_decode(date_fr($timestamp_facture));
											?></span><br />
					Effectu&eacute;e par : <span style="color:<?php echo COULEUR_1; ?>; font-weight:bold;"><?php echo $res_commande['customers_name']; ?></span><br />
					Num&eacute;ro de votre commande : <span style="color:<?php echo COULEUR_1; ?>; font-weight:bold;"><?php echo $res_commande['orders_id']; ?></span><br />
					Commande pay&eacute;e par : <span style="color:<?php echo COULEUR_1; ?>; font-weight:bold;"><?php echo $res_commande['payment_method']; ?></span><br />
					Adresse IP lors de la commande : <span style="color:<?php echo COULEUR_1; ?>; font-weight:bold;"><?php echo $res_commande['customer_ip']; ?></span><br /><br />
					Afin de v&eacute;rifier le traitement de votre commande, <a href="<?php echo BASE_DIR;?>/compte/order_detail.php?id_com=<?php echo $res_commande['orders_id']; ?>" 
				   style="text-decoration:none; color:<?php echo COULEUR_1; ?>;"><strong>cliquez ici</strong></a>         
				</div>
			</div>
			<div class="clear"></div>
				
			<div style="display:block; margin-top:35px; float:left;">
				  <div  id="bordure_mdp_haut_verte"></div> 
				  <div id="bordure_mdp_milieu_vert">
					 <div style="padding:0px 20px 0px 20px; text-align:left;">
						Cette confirmation d'enregistrement de votre commande a &eacute;t&eacute; envoy&eacute;e par email &agrave; <span style="color:<?php echo COULEUR_1; ?>; font-weight:bold;"><?php echo $res_commande['customers_email_address']; ?></span>.<br />
						Vous recevrez un autre email quand votre commande sera exp&eacute;di&eacute;e.<br /><br />
						Notez bien votre num&eacute;ro de commande ci dessus car vous en aurez besoin pour toutes correspondances avec notre site.<br /><br />
						Si vous avez des questions, Consultez notre <a href="<?php echo BASE_DIR;?>/faq.php" 
				   style="text-decoration:none; color:<?php echo COULEUR_1; ?>;"><strong>Foire Aux Questions</strong></a>.<br />
						Vous pouvez nous contacter par notre <a href="<?php echo BASE_DIR;?>/contact.php" 
				   style="text-decoration:none; color:<?php echo COULEUR_1; ?>;"><strong>Formulaire de contact</strong></a>.<br /><br />
						Merci d'avoir choisi Group Army Store / G�n�ral Army Store.
					 </div>
				  </div>
				<div id="bordure_mdp_bas_verte"></div> 
			</div>
			<div class="clear"></div>
		</div>
	 
		<?php
		//if(($etat==1 || $etat==99)){
			// Thierry 12-03-2020 
			$get_arg1 = isset($_GET['arg1']) ? $_GET['arg1'] : NULL;
			$num_commande = $get_arg1;
			$num_commande=$res_commande['orders_id'];?>
	 
			 <script type="text/javascript">
			 var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "https://www.");
			 document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
			 </script>

			<?php			
		require("includes/footer.php"); 
		require("includes/page_bottom.php");
	
		//on vide tous les parametres sessions dont nous avons eu besoin pour le processus de la cammande
		 unset($_SESSION['frais_livraison']);
		 unset($_SESSION['adresse_livraison']);
		 unset($_SESSION['poids_commande']);
		 unset($_SESSION['fdp_panier']);
		 unset($_SESSION['tvap_panier']);
		 unset($_SESSION['ss_total_panier']);
		 unset($_SESSION['tva_total_panier']);
		 unset($_SESSION['mode_paiement']);
		 unset($_SESSION['total_panier']);
		 unset($_SESSION['remise_panier_av']);
		 $_SESSION['code_promo']='';
		 $_SESSION['coupon_frais_de_port']='';
		 $_SESSION['coupon_affectation_remise']='';
		 $_SESSION['type_reduc']='';  
		 $_SESSION['montant_reduc']='';
		 $_SESSION['coupon_minimum_order']='';
		 $_SESSION['coupon_date_deb']='';
		 $_SESSION['coupon_date_fin']='';
		 $_SESSION['commentaire_commande']='';
		 $_SESSION['parrainage_kdo']='';
        // JULIEN 12/03
		 $_SESSION['remise_par_coupon']='';
		 $_SESSION['remise_porte_monnaie']=''; 
        //
		 $_SESSION['remise_coupon']='';	 
	} // fin else empty customer_id
	
} else { // fin if(($etat==1 || $etat==99) || $_SESSION['mode_paiement']!=1 ){
	echo'<meta http-equiv="refresh" content="0; URL=compte/order.php">';
}
?>