<?php  
// Page permettant de faire tous les traitements lors d'enregistremnt d'une commande  
// Penser à modifier le fichier compte/fonction/devis_traitement.php  
// Penser à modifier le fichier gasadmin/nouvelle_commande_traitement.php  
 
require("includes/modules/mod_paiement_spplus.php"); 
//require("includes/configure_spplus.php"); 
function traitement_commande() { 
	 
	//Variables pour le paiement 
	####Carte Bleue#### 
	$ID_MAGASIN=ID_MAGASIN_SPPLUS; 
	$MAIL=MAIL_SPPLUS; 
	$MAIL_TECHNIQUE=MAIL_TECHNIQUE_SPPLUS; 
	$SIRET=SIRET_SPPLUS; 
	 
	$today = date("Y-m-d H:i:s"); 
			 
	$info_client=tep_db_query("	SELECT * 
								FROM customers c, address_book a 
								WHERE c.customers_id = a.customers_id 
								AND c.customers_id = ".$_SESSION['customer_id']." 
								AND address_book_id = customers_default_address_id"); 
	$res=tep_db_fetch_array($info_client); 
	   
	$pays_customer=tep_db_query("select countries_name from ".TABLE_COUNTRIES." where countries_id=".$res['entry_country_id'].""); 
	$res_pays_customer=tep_db_fetch_array($pays_customer); 
		 
	$info_delivery=tep_db_query("select * from ".TABLE_ADDRESS_BOOK." where address_book_id=".$_SESSION['adresse_livraison'].""); 
	$res2=tep_db_fetch_array($info_delivery); 
			 
	$pays_adresse=tep_db_query("select countries_name, countries_iso_code_2 from ".TABLE_COUNTRIES." where countries_id=".$res2['entry_country_id'].""); 
	$res_pays=tep_db_fetch_array($pays_adresse); 
			 
	$moyen_p=tep_db_query("select * from ".TABLE_MODULE_PAIEMENT." where paiement_id=".$_SESSION['mode_paiement'].""); 
	$res_paiement=tep_db_fetch_array($moyen_p); 
	  		  
	$visite=tep_db_query("select *  
					   from ".TABLE_VISITE." 
					   where id_client=".$_SESSION['customer_id']."  
						and id_visite = (select max(id_visite)  
											 from visite  
											 where id_client=".$_SESSION['customer_id'].")"); 
	$res_visite=tep_db_fetch_array($visite); 
	   
	// extraction adresse IP du visiteur  
    $ip = $_SERVER["REMOTE_ADDR"]; 
    // transformation IP  
    $dotted = preg_split( "/[.]+/", $ip); 
    $ip2 = (double) ($dotted[0]*16777216)+($dotted[1]*65536)+($dotted[2]*256)+($dotted[3]);     
                     
	$country_query = tep_db_query("SELECT country_name  
									 FROM `".TABLE_IP_TO_COUNTRY."` 
									 WHERE ".$ip2." BETWEEN ip_from AND ip_to"); 
	$pays = tep_db_fetch_array($country_query); 
 
	$SESSION_remise_panier = isset($_SESSION['remise_panier']) ? $_SESSION['remise_panier'] : NULL; 
     
    $remise=$SESSION_remise_panier+$_SESSION['remise_panier_av']; 
	 
	// SI C'EST UNE DEMANDE DE DEVIS 
	if ($_SESSION['mode_paiement']==8) {  
		$nom_paiement = ''; 
		$orders_date_fin_devis = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m")  , date("d")+15, date("Y"))); 
	} else { 
		$nom_paiement = $res_paiement['paiement_nom']; 
		$orders_date_fin_devis = ''; 
	} 
	  
	$order_info = tep_db_query("INSERT INTO ".TABLE_ORDERS." ( 
										customers_id, 
										customers_name, 
										customers_company, 
										customers_street_address_3, 
										customers_suburb, 
										customers_street_address, 
										customers_street_address_4, 
										customers_city, 
										customers_postcode, 
										customers_country, 
										customers_telephone, 
										customers_email_address, 
										customers_address_format_id, 
										delivery_name, 
										delivery_company, 
										delivery_street_address_3, 
										delivery_suburb, 
										delivery_street_address, 
										delivery_street_address_4, 
										delivery_city, 
										delivery_postcode, 
										delivery_country, 
										delivery_address_format_id, 
										payment_method, 
										date_purchased, 
										orders_status, 
										orders_date_fin_devis, 
										currency, 
										currency_value, 
										customer_ip, 
										customer_ip_country, 
										 
										total,  
										ss_total, 
										tva_total, 
										remise, 
										remise_porte_monnaie, 
										frais_port_client, 
										tva_port, 
										poids 
										 
									) VALUES ( 
									 
										".$_SESSION['customer_id'].", 
										'".$res['entry_firstname']." ".$res['entry_lastname']."', 
										'".$res['entry_company']."', 
										'".addslashes($res['entry_street_address_3'])."', 
										'".addslashes($res['entry_suburb'])."', 
										'".addslashes($res['entry_street_address'])."', 
										'".addslashes($res['entry_street_address_4'])."', 
										'".addslashes($res['entry_city'])."', 
										'".$res['entry_postcode']."', 
										'".$res_pays_customer['countries_name']."', 
										'".addslashes($res['customers_telephone'])."', 
										'".$res['customers_email_address']."', 
										1, 
										'".$res2['entry_firstname']." ".$res2['entry_lastname']."', 
										'".$res2['entry_company']."', 
										'".addslashes($res2['entry_street_address_3'])."', 
										'".addslashes($res2['entry_suburb'])."', 
										'".addslashes($res2['entry_street_address'])."', 
										'".addslashes($res2['entry_street_address_4'])."', 
										'".addslashes($res2['entry_city'])."', 
										'".$res2['entry_postcode']."', 
										'".$res_pays['countries_name']."', 1, 
										'".$nom_paiement."', 
										'".$today."', 
										'".$res_paiement['paiement_statut']."', 
										'".$orders_date_fin_devis ."', 
										'EUR', 
										1, 
										'".$_SERVER["REMOTE_ADDR"]."', 
										'".$pays['country_name']."', 
										 
										".$_SESSION['total_panier'].", 
										".$_SESSION['ss_total_panier'].", 
										".$_SESSION['tva_total_panier'].", 
										".$_SESSION['remise_par_coupon'].", 
										".$_SESSION['remise_porte_monnaie'].", 
										".$_SESSION['fdp_panier'].", 
										".$_SESSION['tvap_panier'].", 
										".$_SESSION['poids_commande']." 
									)"); 
			 
				 
	// R?cuperation de l'id de la commande	 
	$order=tep_db_query("select max(orders_id) as max from ".TABLE_ORDERS." where customers_id=" . $_SESSION['customer_id'] . ""); 
	$oid=tep_db_fetch_array($order); 
	$oid['max']; 
	 
	// Parrainage ajout du produit cadeau 
	/* 
	if(isset($_SESSION['parrainage_kdo']) && $_SESSION['parrainage_kdo']==1){ 
	$orders_products_cadeau = tep_db_query("insert into ".TABLE_ORDERS_PRODUCTS." (orders_id, products_id, products_model, products_name, products_price, products_cost, final_price, products_tax, products_quantity) values (".$oid['max'].", " . ID_ARTICLE_CADEAU_PARRAINAGE . ",'cadeau', 'Cadeau offert par ".NOM_DU_SITE."', 0, 0, 0, 0, 1)"); 
	} 
	*/ 
	 
	// Mise a jour table parrainage 
	/* 
	if($res['email_parrain']!=''){ 
		$red_id_parrain=tep_db_query("select customers_id, customers_firstname, customers_lastname from ".TABLE_CUSTOMERS." where customers_email_address='".$res['email_parrain']."'"); 
		$res_id_parrain=tep_db_fetch_array($red_id_parrain); 
				    
		$maj=tep_db_query("update parrainage set statut='achete' where mail_filleul='".$res['customers_email_address']."' and id_parrain=".$res_id_parrain['customers_id']."");  
			    
		$maj_client_porte_monnaie=tep_db_query("update ".TABLE_CUSTOMERS." set customers_argent_virtuel=customers_argent_virtuel+1 where customers_id=".$res_id_parrain['customers_id'].""); 
			    
		$raison_parraiange='On cr?dite de 3.5 euros le porte monnaie suite au parrainage de ' . $res_id_parrain['customers_firstname'] . ' ' . $res_id_parrain['customers_lastname'] . ' (id:' .$res_id_parrain['customers_id'] . ')'; 
		$raison_parraiange_client='Vous avez ?t? cr?dit? de 3.5 euros suite ? votre parrainage de ' . $res_id_parrain['customers_firstname'] . ' ' . $res_id_parrain['customers_lastname']; 
		$maj_porte_monnaie=tep_db_query("insert into ".TABLE_CUSTOMERS_ARGENT." (customers_id, somme, raison, raison_client, date_versement, user_id) 
											values(".$res_id_parrain['customers_id'].", 2.93, '".addslashes($raison_parraiange)."', '".addslashes($raison_parraiange_client)."', '".$today."', 999)"); 
	} 
	*/ 
	// Fin Mise a jour table parrainage 
	 
	// Panier simple 
    $panier_query = tep_db_query("SELECT cb.products_id , cb.customers_basket_quantity, cb.id_rayon 
                                  FROM ".TABLE_CUSTOMERS_BASKET." as cb 
                                  WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' and cb.products_id <> ''"); 
                                 
	$qte_panier=0; 
	$marge_panier=0; 
	while($panier_data = tep_db_fetch_array($panier_query)){ 
		//$i++; 
        $options = "";
        $article_options = "";
        $option_details = "";
         
        if (isset($panier_data) && ($panier_data != "")) { 
            $article_options = explode("{", $panier_data['products_id']);
        } 
        if (isset($article_options[1]) && $article_options[1] != "") { 
            $options = explode("}", $article_options[1]); 
        } 
        if ((isset($options[0]) && ($options[0] != "")) && ((isset($options[1])) && ($options[1] != ""))) { 
            $option_details = infos_options($options[0], $options[1]); 
        }
        if ((isset($article_options[0]) && ($article_options[0] != ""))) {
            $article = explode("_", $article_options[0]);
        } 
	    
		$article = explode("_", $article_options[0]); 
		// r?cup?re le prix par quantit? 
		$article_details = infos_article($article, $panier_data['customers_basket_quantity']); 
			    
		$id_article = $article[0]; 
			    
		$article_query = tep_db_query("SELECT p.products_id, p.products_model, p.products_price, pd.products_name, p.products_cost, p.products_quantity, p.products_weight, 
									ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1, 
									ppbq.part_price_by_5, ppbq.pro_price_by_5, ppbq.rev_price_by_5, ppbq.adm_price_by_5, 
									ppbq.part_price_by_10, ppbq.pro_price_by_10, ppbq.rev_price_by_10, ppbq.adm_price_by_10, 
									s.specials_new_products_price, 
									pvf.prix_vente 
                                    FROM ".TABLE_PRODUCTS_DESCRIPTION." as pd,  
									". TABLE_PRODUCTS ." as p 
									left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id 
									left join ". TABLE_SPECIALS ." as s on (p.products_id=s.products_id and (expires_date >NOW() or expires_date='0000-00-00 00:00:00')) 
									left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash='0000-00-00'))) 
                                    WHERE pd.products_id = '" . $id_article . "' and pd.products_id = p.products_id"); 
									 
		$article_data = tep_db_fetch_array($article_query); 
		if (isset($article_options[0]) && ($article_options[0] != "")) {
            $cpath = explode("_", $article_options[0]);
            $cp1=$cpath[1]."_".$cpath[2];
        }
		 
		// SI CE N'EST PAS UNE DEMANDE DE DEVIS (id8) 
		// ON RETIRE LES PRODUITS COMMANDES DU STOCK VIRTUEL 
		if ($_SESSION['mode_paiement']!=8) {  
			 
			// DEBUT Change le stock dans les options du produit 
			if ($option_details['nom'] != '') {  
				 
				$products_option_quantity = '	 
										 
					UPDATE 
						products_attributes  
					SET 
						options_quantity = options_quantity-'. $panier_data['customers_basket_quantity'] .' 
					WHERE  
						products_id = '. $article_data['products_id'] .' 
					AND  
						options_values_id = '. $option_details['id_valeur']; 
						 
				tep_db_query($products_option_quantity); 
			} 
			// FIN Change le stock dans les options du produit 
			 
			// DEBUT Change le stock du produit 
			$products_quantity = '	 
				UPDATE  
					products 
				SET 
					products_quantity = products_quantity-'. $panier_data['customers_basket_quantity'] .', 
					products_ordered=products_ordered + '. $panier_data['customers_basket_quantity'] .' 
				WHERE  
					products_id='. $article_data['products_id']; 
			tep_db_query($products_quantity); 
			// FIN Change le stock du produit 
		}  
			    
		$prix = calculate_price_for_product($article_data); 
	    
		if($prix['flash'] > 0){ 
			$prix_prod_final = $prix['HT']; 
		} elseif ($prix['promo'] > 0){ 
			$prix_prod_final = $prix['HT']; 
		} else {		  
			switch($_SESSION['customers_type']) { 
				 
				case 1: //particulier 
					$custom_type = 'part_'; 
					break; 
				case 2: //pro 
					$custom_type = 'pro_'; 
					break; 
				case 3: //rev 
					$custom_type = 'rev_'; 
					break; 
				case 4: //adm 
					$custom_type = 'adm_'; 
					break; 
				default: 
					$custom_type = 'part_'; 
					break; 
			} 
	    
			if($panier_data['customers_basket_quantity'] >= 10) { 
				$prix_prod_final = $article_data[$custom_type .'price_by_10']; 
			} elseif ($panier_data['customers_basket_quantity'] >= 5) { 
				$prix_prod_final = $article_data[$custom_type .'price_by_5']; 
			} else {			   
				$prix_prod_final = $article_data[$custom_type .'price_by_1']; 
			} 
		} 
	   
		 
		/*$id_categorie = $cpath[2]; 
		$req_cat_name=tep_db_query("select categories_name, categories_url, categories_id from categories_description where categories_id=".$id_categorie.""); 
		$res_cat_name=tep_db_fetch_array($req_cat_name);*/ 
			 
		//$nom_prod=bda_product_name_transform($article_data['products_name'], $res_cat_name['categories_id']); 
			 
		// Promotion 
		$orders_products_panier = tep_db_query("insert into ".TABLE_ORDERS_PRODUCTS." (orders_id, products_id, id_rayon, cpath, products_model, products_name, products_price, products_cost, final_price, products_tax, products_quantity, products_weight) values (".$oid['max'].", ".$article_data['products_id'].", '".$panier_data['id_rayon']."', '".$cp1."', '".addslashes($article_data['products_model'])."', '".addslashes($article_data['products_name'])."', ".$article_data['products_price'].", ".$article_data['products_cost'].", ".$prix_prod_final.", 20, ".$panier_data['customers_basket_quantity'].", '". $article_data['products_weight'] ."')"); 
			   
		// SI CE N'EST PAS UNE DEMANDE DE DEVIS 
		if ($_SESSION['mode_paiement']!=8) {  
			// DEBUT Alerte de stock produit		 
			$alert_stock=tep_db_query("select products_quantity_min, products_quantity from ".TABLE_PRODUCTS." where products_id=".$article_data['products_id'].""); 
			$res_alert=tep_db_fetch_array($alert_stock); 
			if($res_alert['products_quantity']<=$res_alert['products_quantity_min']) { 
				mail_stock_alert($article_data['products_id'],$article_data['products_name'],$article_data['products_model']); 
			} 
			// FIN Alerte de stock produit 
		} 
		 
		// Si le produit a des options 
		if (isset($option_details['nom']) && ($option_details['nom']!='')) { 
			 
			// SI CE N'EST PAS UNE DEMANDE DE DEVIS 
			if ($_SESSION['mode_paiement']!=8) {  
				//alerte de stock sp?cifique pour un produit avec option 
				$alert_stock_options = 'select options_quantity, options_quantity_min, products_options_name, products_options_values_name from '. TABLE_PRODUCTS_ATTRIBUTES .' pa, '. TABLE_PRODUCTS_OPTIONS .' po, '. TABLE_PRODUCTS_OPTIONS_VALUES .' pov 
										where pa.options_id=po.products_options_id 
										and pa.options_values_id=pov.products_options_values_id 
										and products_id=\''. $article_data['products_id'] .'\' 
										and options_values_id = \''. $option_details['id_valeur'] .'\''; 
				$alert_stock_options = tep_db_query($alert_stock_options); 
				$res_alert = tep_db_fetch_array($alert_stock_options); 
				 
				if($res_alert['options_quantity']<=$res_alert['options_quantity_min']) { 
					 
					mail_stock_alert($article_data['products_id'], $article_data['products_name'], $article_data['products_model'] .' - '. $res_alert['products_options_name'] .' => '. $res_alert['products_options_values_name']); 
					 
				} 
			} 
			// Recherche du num?ro de la commande  
		   $products_details = tep_db_query("select max(orders_products_id) as max from ".TABLE_ORDERS_PRODUCTS." where orders_id=".$oid['max'].""); 
		   $res_pdeatails=tep_db_fetch_array($products_details); 
		   // insertion du produit avec option dans la commande 
		   $products_options = tep_db_query("insert into ".TABLE_ORDERS_PRODUCTS_ATTRIBUTES." (orders_id, orders_products_id, options_values_id, products_options, products_options_values) value (".$oid['max'].", ".$res_pdeatails['max'].", ". $option_details['id_valeur'] .", '". addslashes($option_details['nom']) ."', '". addslashes($option_details['nom_valeur']) ."')"); 
	    
	   } 
	   // Fin Alerte stock // 
	    
	   $qte_panier+=$panier_data['customers_basket_quantity']; 
	   $marge_panier+=($prix_prod_final-$article_data['products_cost'])*$panier_data['customers_basket_quantity']; 
	} 
	 
	//panier pack 
	$packs_query = tep_db_query("	SELECT cb.pack_id, cb.customers_basket_quantity, cb.id_rayon 
									FROM ".TABLE_CUSTOMERS_BASKET." as cb 
									WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' and cb.pack_id <> ''"); 
	$nb_packs = tep_db_num_rows($packs_query); 
    $qte_pack = 0; 
    $marge_pack=0; 
	if ($nb_packs > 0) { 
		 
		$poids_pack=0; 
		$qte_pack=0; 
		 
		while ($packs_data = tep_db_fetch_array($packs_query)) { 
 
			$id_complet = $packs_data['pack_id']; 
			//$i++; 
			 
			// On r?cup?re les infos sur les options 
			$pack_options = explode("{", $id_complet); 
			$options = explode("}", $pack_options[1]); 
			$options_values_id = $options[1]; 
			$option_details = infos_options($options[0], $options[1]); 
			$premier = true;                  
			// On r?cup?re les infos sur le pack 
			$pack = explode("_", $pack_options[0]); 
			$pack_details = info_pack($pack, $option_details); 
						 
			$id_pack = $pack[0]; 
			$nb_pack = $packs_data['customers_basket_quantity']; 
								 
		  	$articles_pack_query = tep_db_query(" 
											SELECT  
												pp.products_id as products_id_0,  
												pp.products_id_1,  
												pp.products_id_2,  
												p.products_id,  
												p.products_model,  
												p.products_price,  
												pd.products_name,  
												p.products_cost,  
												p.products_quantity,  
												pack_remise, pack_products_quantity 
											FROM  
												".TABLE_PACK." as pp,  
												".TABLE_PRODUCTS_DESCRIPTION." as pd,  
												".TABLE_PRODUCTS." as p 
											WHERE pp.pack_id = " . $id_pack . "  
											AND pd.products_id = p.products_id  
											AND (pp.products_id = pd.products_id or pp.products_id_1 = pd.products_id or pp.products_id_2 = pd.products_id)"); 
		 
			while ($articles_pack_data = tep_db_fetch_array($articles_pack_query)) { 
				 
				//pour le cas ou on a des packs avec plusieurs fois le m?me produit, la requete ne gardait qu'un seul de ces produits, on met donc a jour la quantit?				 
				if ($articles_pack_data['products_id_0']==$articles_pack_data['products_id'] && $articles_pack_data['products_id_0']==$articles_pack_data['products_id_1']) { 
					 
					if ($articles_pack_data['products_id_0']==$articles_pack_data['products_id'] && $articles_pack_data['products_id_0']==$articles_pack_data['products_id_2']) { 
						$quantite=$nb_pack*3; 
					} else {  
						$quantite=$nb_pack*2; 
					} 
					 
				} elseif ($articles_pack_data['products_id_0']==$articles_pack_data['products_id'] && $articles_pack_data['products_id_0']==$articles_pack_data['products_id_2']) { 
					 
					$quantite=$nb_pack*2; 
					 
				} elseif ($articles_pack_data['products_id_1']==$articles_pack_data['products_id'] && $articles_pack_data['products_id_1']==$articles_pack_data['products_id_2']) { 
					 
					$quantite=$nb_pack*2; 
					 
				} else {  
					$quantite = $nb_pack; 
				} 
				 
				$remise = $articles_pack_data['pack_remise']; 
				 
				// MAJ du stock produit et du nombre d'article vendu 
				$products_quantity2 = tep_db_query("UPDATE products SET  
								products_quantity=".($articles_pack_data['products_quantity']-$quantite).",   
								products_ordered=products_ordered + ". $articles_pack_data['products_quantity'] ." 
													WHERE products_id=".$articles_pack_data['products_id'].""); 
				 
				// MAJ du stock des options du produit 
				if (strpos($id_complet, '{') !== FALSE) {				 
					$products_option_quantity = 'UPDATE products_attributes SET options_quantity = options_quantity-'. $nb_pack .' WHERE products_id = '. $articles_pack_data['products_id'] .' AND options_values_id = '. $options_values_id; 
					tep_db_query($products_option_quantity); 
				}				 
				 
				// Email alerte stock pour Boris 
				$alert_stock_p=tep_db_query("SELECT products_quantity_min, products_quantity  
											 FROM ".TABLE_PRODUCTS."  
											 WHERE products_id=".$articles_pack_data['products_id'].""); 
				 
				$res_alert_p=tep_db_fetch_array($alert_stock_p); 
				 
				if($res_alert_p['products_quantity']<=$res_alert_p['products_quantity_min']) { 
					mail_stock_alert($articles_pack_data['products_id'],$articles_pack_data['products_name'],$articles_pack_data['products_model']); 
				} 
				// FIN Email alerte stock pour Boris 
				 
				 
				// ON RECUPERE LE CPATH POUR LE STOCKER EN BASE 
				$cpath2 = explode("_", $pack_options[0]); 
				$cp2=$cpath2[1]."_".$cpath2[2]; 
				 
				// ON INSERE LE PRODUIT DANS LA BDD 
				$orders_products_pack = tep_db_query("INSERT INTO ".TABLE_ORDERS_PRODUCTS." ( 
					orders_id,  
					products_id,  
					id_rayon,  
					cpath,  
					products_model,  
					products_name,  
					products_price,  
					products_cost,  
					final_price,  
					products_tax,  
					products_quantity  
				) VALUES ( 
					".$oid['max'].",  
					".$articles_pack_data['products_id'].",  
					'".$packs_data['id_rayon']."',  
					'".$cp2."',  
					'".addslashes($articles_pack_data['products_model'])."',  
					'".addslashes($articles_pack_data['products_name'])."',  
					".$articles_pack_data['products_price'].",  
					".$articles_pack_data['products_cost'].",  
					".($articles_pack_data['products_price']-($articles_pack_data['pack_remise']/$articles_pack_data['pack_products_quantity'])).",  
					20,  
					".$quantite.")" 
				); 
				 
				// SI LE PRODUIT A UNE OPTION  
				if($option_details['id_valeur']!='' && $premier == true) { 
									 
					$products_details2 = tep_db_query("select max(orders_products_id) as max from ".TABLE_ORDERS_PRODUCTS." where orders_id=".$oid['max']." AND ".$articles_pack_data['products_id_0']." 
					"); 
					$res_pdeatails2 = tep_db_fetch_array($products_details2); 
					$avantdernierenregistrement = ($res_pdeatails2['max']+1); 
					// ON INSERE L'OPTION DANS LA BDD 
					$products_options2 = tep_db_query("insert into ".TABLE_ORDERS_PRODUCTS_ATTRIBUTES." ( 
							orders_id,  
							orders_products_id,  
							options_values_id,  
							products_options,  
							products_options_values 
						) value ( 
							".$oid['max'].",  
							".$avantdernierenregistrement.",  
							'".$option_details['id_valeur']."',  
							'".$option_details['nom']."',  
							'".$option_details['nom_valeur']."')" 
						); 
			   		$premier=false; 
					 
				} 
				$qte_pack += $quantite; 
				$marge_pack=0; 
				$marge_pack+=($articles_pack_data['products_price']-($articles_pack_data['pack_remise']/$articles_pack_data['pack_products_quantity']))*$quantite; 
			 
			}//fin while products_pack 
        } //fin while pack	 
		 
		// Envoi d'un email a Thierry lorsqu'un pack est achet? 
		$sujet_thierry = 'PACK dans CMD ' .$oid['max']; 
		$message_thierry = 'Il y a un pack dans la cmd '.$oid['max']; 
		mail_cmd_thierry($sujet_thierry,$message_thierry); 
		 
	}//fin if nb_pack>0 
	 
	   
	if(isset($_SESSION['remise_porte_monnaie']) && $_SESSION['remise_porte_monnaie']>0 ){ 
		$marge_finale=($marge_panier+$marge_pack)-$_SESSION['remise_porte_monnaie']; 
		$orders_quantity = tep_db_query("UPDATE ".TABLE_ORDERS." SET nb_articles=".($qte_panier+$qte_pack).", marge=".$marge_finale." where orders_id=".$oid['max']); 
		 
		// ajout du code ci dessous  
		$c="Utilisation du porte-monnaie virtuel"; 
		$orders_status_history = tep_db_query("INSERT INTO ".TABLE_ORDERS_STATUS_HISTORY." (orders_id, orders_status_id, date_added, customer_notified, comments) VALUES (".$oid['max'].", ".$res_paiement['paiement_statut'].", '".$today."', 1, '".$c."' )"); 
		 
		 
	} else{ 
		$orders_quantity = tep_db_query("UPDATE ".TABLE_ORDERS." SET nb_articles=".($qte_panier+$qte_pack).", marge=".($marge_panier+$marge_pack)." where orders_id=".$oid['max']); 
		 
		// ajout du code ci dessous 
		if(isset($_SESSION['commentaire_commande']) && !empty($_SESSION['commentaire_commande'])){ 
			$com="'".$_SESSION['commentaire_commande']."'"; 
		} else $com="NULL"; 
		   
		$orders_status_history = tep_db_query("INSERT INTO ".TABLE_ORDERS_STATUS_HISTORY." (orders_id, orders_status_id, date_added, customer_notified, comments) VALUES (".$oid['max'].", ".$res_paiement['paiement_statut'].", '".$today."', 1, ".$com.")"); 
		 
	} 
	   
	/*if(isset($_SESSION['remise_porte_monnaie']) && $_SESSION['remise_porte_monnaie']>0 ){ 
		$c="Utilisation du porte-monnaie virtuel"; 
		$orders_status_history = tep_db_query("insert into ".TABLE_ORDERS_STATUS_HISTORY." (orders_id, orders_status_id, date_added, customer_notified, comments) values (".$oid['max'].", ".$res_paiement['paiement_statut'].", '".$today."', 1, '".$c."' )"); 
	} else { 
		if(isset($_SESSION['commentaire_commande']) && !empty($_SESSION['commentaire_commande'])){ 
			$com="'".$_SESSION['commentaire_commande']."'"; 
		} else $com="NULL"; 
		   
		$orders_status_history = tep_db_query("insert into ".TABLE_ORDERS_STATUS_HISTORY." (orders_id, orders_status_id, date_added, customer_notified, comments) values (".$oid['max'].", ".$res_paiement['paiement_statut'].", '".$today."', 1, ".$com.")"); 
	}*/ 
	 
	// VIDAGE DU PANIER DU CLIENT 
	$panier_client_supr=tep_db_query("delete from ".TABLE_CUSTOMERS."_basket where customers_id=".$_SESSION['customer_id'].""); 
	$panier_attributs_client_supr=tep_db_query("delete from ".TABLE_CUSTOMERS_BASKET_ATTRIBUTES." where customers_id=".$_SESSION['customer_id'].""); 
	  
	  
	  
	// ENVOI DU MAIL AU CLIENT  
	$mail=$res['customers_email_address']; 
	// SI C'EST UNE DEMANDE DE DEVIS 
	if ($_SESSION['mode_paiement'] != 8) {  
		$sujet="Traitement de la commande"; 
		$mess=mail_contenu_commande($oid['max']); 
	} else { 
		//$sujet="Creation de votre devis"; 
		$sujet="Votre devis est disponible"; 
		$mess=mail_contenu_devis_front($oid['max']); // mail_contenu_devis_front 
	}	 
	 
	//mail admin 
	$mess_admin=mail_contenu_commande_admin($oid['max']); 
		   
	// MAJ Date dernier commande du client  
	$customers_info=tep_db_query("	UPDATE  
										customers_info 
									SET  
										customers_info_date_of_last_order='".$today."' 
									WHERE  
										customers_info_id=".$_SESSION['customer_id'].""); 
  
	// MAJ Code promotionnel  
	if(isset($_SESSION['coupon_affectation_remise'])  && !empty($_SESSION['coupon_affectation_remise'])){ 
		$coupon=tep_db_query("	INSERT INTO ".TABLE_COUPON_REDEEM_TRACK." ( 
									coupon_id, customer_id, redeem_date, redeem_ip, order_id, order_remise_montant 
								) VALUES ( 
									".$_SESSION['coupon_id'].", ".$_SESSION['customer_id'].", '".$today."', '".$_SERVER["REMOTE_ADDR"]."', ".$oid['max'].", ".$_SESSION['remise_coupon'].")"); 
	} 
	 // Fin Gestion coupon 
	  
	  
	// MAJ du port monnaie virtuel  
	if(isset($_SESSION['remise_porte_monnaie']) && $_SESSION['remise_porte_monnaie']>0){ 
		$somme_av=tep_db_query("SELECT customers_argent_virtuel FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id'].""); 
		$res_av=tep_db_fetch_array($somme_av); 
 
		$nouv_m=$res_av['customers_argent_virtuel']-$_SESSION['remise_porte_monnaie']; 
		$r='Suite a la commande : '.$oid['max'].' votre porte-monnaie virtuel a ete debite de '.$_SESSION['remise_porte_monnaie'].' ?'; 
 
		$update_cli_av=tep_db_query("UPDATE ".TABLE_CUSTOMERS." SET customers_argent_virtuel=".$nouv_m." WHERE customers_id=".$_SESSION['customer_id'].""); 
		$inset_cli = tep_db_query("insert into ".TABLE_CUSTOMERS_ARGENT." (customers_id, somme, raison, raison_client, date_versement, user_id) values ('".$_SESSION['customer_id']."','".$_SESSION['remise_porte_monnaie']."','".$r."','".$r."','".$today."',999)");		  
	} 
	 
	 
		 
	// mise a jour de la commade en fonction du choix de paiement  
	if ($_SESSION['mode_paiement'] == 1) { // Paiement CB - ID 1 
	 
		$update=tep_db_query("UPDATE ".TABLE_ORDERS." SET etat_paiement=0 WHERE orders_id=".$oid['max'].""); 
		echo lien_paiement_spplus($ID_MAGASIN,$SIRET,$oid['max']); 
		 
	} elseif ($_SESSION['mode_paiement'] == 5) { // Paiement PAYPAL - ID 5 
	 
		$update=tep_db_query("UPDATE ".TABLE_ORDERS." SET etat_paiement=0 WHERE orders_id=".$oid['max'].""); 
		//echo 'Coucou c\'est un paiement PAYPAL'; 
	?> 
	 
	<!--<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">--> 
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="paiement_paypal"> 
	<!--<form action="https://www.paypal.com/cgi-bin/webscr" method="post">--> 
		<input type='hidden' value="<?php echo $_SESSION['total_panier']; ?>" name="amount" /> 
		<input name="currency_code" type="hidden" value="EUR" /> 
		<input name="shipping" type="hidden" value="0.00" /> 
		<input name="tax" type="hidden" value="0.00" /> 
		 
		<?php // URL de retour client si le paiement est valid? ?> 
		<input name="return" type="hidden" value="https://www.generalarmystore.fr/gas/panier_remerciement.php" /> 
		<?php // URL de retour client si le paiement n'est pas valid? ?> 
		<input name="cancel_return" type="hidden" value="https://www.generalarmystore.fr/gas/compte/order.php" /> 
		<?php // url de retour paypal pour confirmation du paiement ?> 
		<input name="notify_url" type="hidden" value="https://www.generalarmystore.fr/gas/paypal-7dql9apmn7idm5qgdr5u3qposa3i2c7e.php" />  
		 
		<?php // Utilise l'adresse que le client a saisie et bloque l'?dition  ?> 
		<input name="address_override" type="hidden" value="1" /> 
		<?php // Adresse que le client a saisie pour sa commande ?> 
		<input name="first_name" type="hidden" value="<?php echo $res2['entry_company']; ?>"> 
		<input name="last_name" type="hidden" value="<?php echo $res2['entry_firstname'] . ' ' . $res2['entry_lastname']; ?>"> 
		<input name="address1" type="hidden" value="<?php echo addslashes($res2['entry_street_address']); ?>"> 
		<input name="address2" type="hidden" value="<?php echo addslashes($res2['entry_suburb']); ?>"> 
		<input name="city" type="hidden" value="<?php echo $res2['entry_city']; ?>"> 
		<input name="zip" type="hidden" value="<?php echo $res2['entry_postcode']; ?>"> 
		<input name="country" type="hidden" value="<?php echo $res_pays['countries_iso_code_2']; ?>"> 
		 
		<?php // Le bouton sur lequel la personne a cliqu? : Acheter maintenant=_xclick -  ?> 
		<input name="cmd" type="hidden" value="_xclick" /> 
		<?php // Adresse email du compte PAYPAL a cr?diter ?> 
		<input name="business" type="hidden" value="compta@generalarmystore.fr" /> 
		<?php // Texte a afficher ?> 
		<input name="item_name" type="hidden" value="Commande General Army Store <?php echo $oid['max']; ?>" /> 
		<?php // Evite l'affichage d'un commentaire par le client ?> 
		<input name="no_note" type="hidden" value="1" /> 
		<input name="lc" type="hidden" value="FR" /> 
		<input name="bn" type="hidden" value="PP-BuyNowBF" /> 
		<?php // identifiant qui permet de r?cup?rer le num?ro de commande pour passer la commande en traitement ?> 
		<input name="custom" type="hidden" value="<?php echo $oid['max']; ?>" /> 
		<!--<input alt="Effectuez vos paiements via PayPal : une solution rapide, gratuite et s?curis?e" name="submit" type="hidden" src="https://www.paypal.com/fr_FR/FR/i/btn/btn_buynow_LG.gif" type="image" />--> 
		<input alt="Effectuez vos paiements via PayPal : une solution rapide, gratuite et s?curis?e" name="submit" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" type="image" /> 
		<img src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" border="0" alt="" width="1" height="1" /> 
	</form> 
	 
	<div style="width:100%px; height:200px; line-height:200px; text-align:center;"> 
		<img src="https://www.generalarmystore.fr/gas/images/chargement-en-cours.gif" alt="" style="vertical-align:middle;"> 
	</div> 
	<script> 
		<!-- 
		window.onload = document.getElementById('paiement_paypal').submit(); 
		//--> 
	</script> 
	 
	<?php 
	} elseif ($_SESSION['mode_paiement'] == 8) { // Demande devis - ID 8 
	 
		mail_cmd_admin($sujet,$mess_admin); 
		mail_client_commande($mail,$sujet,$mess);	 
		echo '<meta http-equiv="refresh" content="0; URL=compte/devis.php">'; 
			 
	} else { 
		//$remise_coupon=$_SESSION['remise_coupon']; 
		//$remise_av=$remise-$remise_coupon; 
	  
		// Suppressions des commandes "en attente de paiement CB" du client s'il paie une commande 
			 
		/*$req_liste_cmd_client = tep_db_query("select orders_id from " . TABLE_ORDERS . " where customers_id = ". $_SESSION['customer_id'] ." and orders_status = '27'"); 
			 
		while ($res_liste_cmd_client = tep_db_fetch_array($req_liste_cmd_client)) { 
					 
			$order_query = tep_db_query("select orders_products_id, products_id, products_quantity from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . $res_liste_cmd_client['orders_id'] . "'"); 
			 
			while ($order = tep_db_fetch_array($order_query)) { 
				$order_attribute_id_query = tep_db_query("SELECT pov.products_options_values_id  
															FROM " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov, " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " opa  
															WHERE opa.products_options_values = pov.products_options_values_name AND orders_products_id = '" . $order['orders_products_id'] . "'"); 
				$order_attribute_id = tep_db_fetch_array($order_attribute_id_query); 
					   
				if (tep_db_num_rows($order_attribute_id_query) != 0) { 
					tep_db_query("update " . TABLE_PRODUCTS_ATTRIBUTES . " set options_quantity = options_quantity + " . $order['products_quantity'] . " where products_id = '" . (int)$order['products_id'] . "' AND options_values_id = '" . $order_attribute_id['products_options_values_id'] . "'"); 
				} 
					   
				tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity + " . $order['products_quantity'] . ", products_ordered = products_ordered - " . $order['products_quantity'] . " where products_id = '" . (int)$order['products_id'] . "'"); 
			} 
						   
			tep_db_query("delete from " . TABLE_ORDERS . " where orders_id = '" . $res_liste_cmd_client['orders_id'] . "'"); 
			tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . $res_liste_cmd_client['orders_id'] . "'"); 
			tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_id = '" . $res_liste_cmd_client['orders_id'] . "'"); 
			tep_db_query("delete from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_id = '" . $res_liste_cmd_client['orders_id'] . "'"); 
		}*/ 
		// Suppressions des commandes "en attente de paiement CB" du client s'il paie une commande 
	  
		mail_cmd_admin($sujet,$mess_admin); 
		mail_client_commande($mail,$sujet,$mess);	  
		echo '<meta http-equiv="refresh" content="0; URL=panier_remerciement.php">'; 
	} 
} 
 
// TEST Alert mail pour BORIS 
	//mail_stock_alert(8014,'Lunette de vis?e STRIKE EAGLE 1-6 x 24 Vortex?','DDG/SE-1624-1'); 
	//mail_stock_alert(8081,'Montage Cantilever CM-202 diam?tre 30mm Vortex?','DDG/CM-202'); 
// TEST Alert mail pour BORIS	 
?>