<?php
require('includes/application_top.php');
include_once  DIR_NET_REVIEWS.'configure.php';
include_once  DIR_NET_REVIEWS_CLASSES.'net_reviews_db.php';
include_once  DIR_NET_REVIEWS_CLASSES.'net_reviews_data.php';
include_once  DIR_NET_REVIEWS_CLASSES.'net_reviews_api.php';
include_once  DIR_NET_REVIEWS_CLASSES.'net_reviews_tools.php';

nrTools::$languageId = $languages_id;

$HTTP_POST_VARS = (isset($HTTP_POST_VARS))? $HTTP_POST_VARS : $_POST;

$db = new Net_Reviews_DB();
// Installation in DB.
// $db->install();
// configuration
$DATA = new Net_Reviews_Data();
$API = new Net_Reviews_Api($HTTP_POST_VARS,$DATA);
$DATA->setup(array('idShop'=>$API->msg('id_shop'),'query'=>$API->msg('query')));

// check for post
if ($HTTP_POST_VARS) {

    $IsActiveVar = $API->isActiveModule();
    // if not active return error.
    if ($IsActiveVar['return'] != 1) {
        // error is serialized and enocdeed
        echo $API->AC_encode_base64(json_encode($isActiveVar));
        exit;
    }
    // now check if the SecurityData.
    $checkSecurityVar = $API->checkSecurityData();
    // if not valid return error.
    if ($checkSecurityVar['return'] != 1 ) {
        // error is serialized and enocdeed
        echo $API->AC_encode_base64(json_encode($checkSecurityVar));
        exit;
    }
    /* ############ DEBUT DU TRAITEMENT ############*/
    // switch case on query type.
    switch ($HTTP_POST_VARS['query']) {
        case 'isActiveModule':
            $toReply = $IsActiveVar;
            break;
        case 'setModuleConfiguration' : 
            $toReply = $DATA->setModuleConfiguration($API);
            break;	
        case 'getModuleAndSiteConfiguration' : 
            $toReply = $DATA->getModuleAndSiteConfiguration($API);
            break;
        case 'getOrders' : 
            $toReply = $API->getOrders();
            break;
        case 'setProductsReviews' : 
            $toReply = $API->setProductsReviews();
            break;	
        case 'truncateTables' : 
            $toReply = $API->truncateTables();
            break;
        case 'getUrlProducts' : 
            $toReply = $API->getUrlProducts();
            break;
        default:
            $reponse['debug']  = "Aucun variable ACTION reçues";
            $reponse['return'] = 2; //A definir
            $reponse['query'] = $HTTP_POST_VARS['query'];
            // error is serialized and enocdeed
            echo $API->AC_encode_base64(json_encode($reponse));
            exit;
    }
    // Affichage du retour des fonctions pour rÃ©cupÃ©ration du rÃ©sultat par AvisVerifies
    echo $API->AC_encode_base64(json_encode($toReply));
    exit;
}
else
{
    $reponse['debug'] = "Aucun variable POST reçues";
    $reponse['return'] = 2; //A definir
    $reponse['query'] = "";
    // error is serialized and enocdeed
    echo $API->AC_encode_base64(json_encode($reponse));
    exit;
}