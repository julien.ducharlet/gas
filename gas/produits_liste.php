<?php
/* Page affichant la liste des articles pour un mod�le donn�. L'affichage peut se faire sous 3 formes : 'Images', 'Icones' et 'Liste' */

$nom_page = "produits_liste";
   
require("includes/page_top.php");

// Si c'est l'affichage du resultat d'une recherche 
if (isset($_REQUEST["recherche"])) { 
	$get_de_recherche = format_recherche($_GET['recherche']);
}
//date du jour 
$today = date("Y-m-d H:i:s");

	switch($_SESSION['customers_type']) {
		
		case '1' :
			$query_custom_type = ' and categories_status_client=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
			break;
		
		case '2' :
			$query_custom_type = ' and categories_status_pro=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_pro=\'1\'';
			break;
		
		case '3' :
			$query_custom_type = ' and categories_status_rev=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_rev=\'1\'';
			break;
		
		case '4' :
			$query_custom_type = ' and categories_status_adm=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_adm=\'1\'';
			break;
		
		default :
			$query_custom_type = ' and categories_status_client=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
			break;
	}

//verifie URL
if(!isset($_REQUEST["recherche"])) {
	
	$query_url="SELECT cd.categories_baseline, cd.categories_name as nom_modele, cd.categories_url as url_modele, rub.rubrique_name, rub.rubrique_url, cd2.categories_name as nom_marque, 
					   cd2.categories_url as url_marque, cd.categories_htc_title_tag, cd.categories_htc_desc_tag, cd.categories_htc_keywords_tag
				FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
												   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
												   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
												   inner join " . TABLE_RAYON . " as rub on rub.rubrique_id=cr.rubrique_id
				WHERE c.parent_id='". (int)$_REQUEST['marque'] ."'
				AND cd.categories_id='". (int)$_REQUEST['modele'] ."'
				AND cr.rubrique_id='". (int)$_REQUEST['rayon'] ."'
				and categories_status='1'
				". $query_custom_type . $query_rubrique_custom_type;
	
	$res_url=tep_db_query($query_url);
	if(tep_db_num_rows($res_url)!=1){
		redirect(404,url('erreur',array()));
	}
	else{
		$row_url = tep_db_fetch_array($res_url);
		$categories_htc_title_tag=$row_url["categories_htc_title_tag"];
		$categories_htc_desc_tag=$row_url["categories_htc_desc_tag"];
		$categories_htc_keywords_tag=$row_url["categories_htc_keywords_tag"];
		$cat_sous_cat=$row_url["nom_marque"]." ".$row_url["nom_modele"];
		$baseline=$row_url["categories_baseline"];
		$url_clean = url("modele",array('id_rayon' => (int)$_REQUEST["rayon"], 'nom_rayon' => $row_url["rubrique_url"], 'id_marque' => (int)$_REQUEST["marque"], 'url_marque' => $row_url["url_marque"], 'id_modele' => (int)$_REQUEST["modele"], 'url_modele' => $row_url["url_modele"]));
		if(isset($_REQUEST["famille"]) && !empty($_REQUEST["famille"])){
			$query_famille="select family_client_url, family_client_htc_title_tag, family_client_htc_desc_tag, family_client_htc_keywords_tag, family_client_name from ".TABLE_CLIENT_FAMILY_INFO." info inner join ".TABLE_CLIENT_FAMILY." fam on fam.family_client_id=info.family_client_id where fam.family_client_id='". (int)$_REQUEST["famille"] ."'";
			$res_famille=tep_db_query($query_famille);
			
			if(tep_db_num_rows($res_famille)!=1){
				redirect(404,url('erreur',array()));
			}
			
			$row_famille = tep_db_fetch_array($res_famille);
			$categories_htc_title_tag=$row_famille["family_client_name"]." pour ".$row_url["nom_marque"]." ".$row_url["nom_modele"];
			$baseline=$categories_htc_title_tag;
			$url_clean = url("modele_famille",array('id_rayon' => $_REQUEST["rayon"], 'nom_rayon' => $row_url["rubrique_url"], 'id_marque' => $_REQUEST["marque"], 'url_marque' => $row_url["url_marque"], 'id_modele' => $_REQUEST["modele"], 'url_modele' => $row_url["url_modele"], 'id_famille' => $_REQUEST["famille"], 'url_famille' => $row_famille["family_client_url"]));
		}
		if($_SERVER['REQUEST_URI']!=$url_clean){
			redirect(301,$url_clean);
		}
	}

}
require("includes/meta/produits_liste.php");
require("includes/meta_head.php");
require("includes/header.php");
require("css/background_pages.php");
?>

<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/produits_liste.js"></script>
<?php
		
// Si le type de vue n'a jamais �t� d�fini on met par d�faut la vue "images"
if (!isset($_SESSION['type_vue']) || isset($get_de_recherche)) {
	$_SESSION['type_vue'] = 'images';
}

// On analyse les donn�es POST et GET venant des filtres de la page

if (isset($_REQUEST['tri']) && $_REQUEST['tri']!='') {
	
	switch ($_REQUEST['tri']) {
		case 'prix_asc':
			$tri = 'prix'; $ordre = 'asc';
			break;
		case  'prix_desc':
			$tri = 'prix'; $ordre = 'desc';
			break;
		case  'ajout':
			$tri = 'ajout'; $ordre = 'desc';
			break;
		case  'nom':
			$tri = 'nom'; $ordre = 'asc';
			break;
		default :
			$tri = 'nom'; $ordre = 'asc';
			break;
	}
} else {
	$tri = 'nom'; 
	$ordre = 'asc';
}

// On stocke le bout de requ�te qui va nous �tre utile par la suite pour filtrer les r�sultats
$rqt_tri = " pd.products_name ASC";
if($tri == 'nom') $rqt_tri = " pd.products_name " . $ordre;
elseif($tri == 'prix') $rqt_tri = " p.products_price " . $ordre;
elseif($tri == 'ajout') $rqt_tri = " p.products_date_added " . $ordre;
$rqt_filtre ='';
if (isset($_REQUEST['famille']) && $_REQUEST['famille'] != '')
	$rqt_filtre .= 'and p.family_client_id = ' . (int)$_REQUEST['famille'] ." ";
if (isset($_REQUEST['fabriquant']) && $_REQUEST['fabriquant'] != '')
	$rqt_filtre .= 'and p.manufacturers_id = '. (int)$_REQUEST['fabriquant'] ." ";
	
?>

<div id="corps">
	<input type="hidden" id="base_dir" value="<?php echo BASE_DIR . "/"; ?>" />
    
	<!-- Affichage du fil d'ariane :
			Le module se charge aussi de d�clarer et d'assigner une valeur aux variables $id_rayon, $nom_rayon, $url_rayon, $id_marque, $nom_marque, $id_modele, $nom_modele -->
	<?php
    if (!isset($_REQUEST["recherche"])) {
    	include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_fil_ariane.php");
	}
	?>
    
    <div id="produits">
		<?php
        if (!isset($_REQUEST["recherche"])) {
			include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_recherche_avancee.php");
		} else {
			echo "<h2><span class='texte_recherche'>Vous avez recherch� : </span><span class='valeur_recherche'>" . $get_de_recherche . "</span></h2>";
		}
		?>
        
        <?php
		if(!isset($get_de_recherche)) {
			?>
        <div class="titre_filtres" style="padding-top:10px;"><strong>Astuce</strong> : Pensez � filtrer votre recherche !</div>
        <div class="filtres">
        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
            Famille d'article :
                <select name="famille" onchange="this.form.submit();">
	                <option value="">Toutes les familles</option>
				<?php
				if (isset($_REQUEST['modele'])) {
					$rqt_family = "and ptc.categories_id = '". (int)$_REQUEST['modele'] . "' ";
				} else {
					$rqt_family = '';
				}
				
                $family_query = tep_db_query("SELECT distinct(f.family_client_name), f.family_client_id, finfo.family_client_url
											  FROM " . TABLE_PRODUCTS . " as p, " . TABLE_CLIENT_FAMILY . " as f, " . TABLE_CLIENT_FAMILY_INFO . " as finfo, " . TABLE_PRODUCTS_TO_CATEGORIES . " as ptc
											  WHERE p.products_id = ptc.products_id and p.family_client_id = f.family_client_id and finfo.family_client_id = f.family_client_id " . $rqt_family . " and p.products_status <> 0 
											  ORDER BY  f.family_client_name");
				while($family_data = tep_db_fetch_array($family_query)){
					if(!isset($_REQUEST["famille"]) || empty($_REQUEST["famille"])){
						$baseline.=" | <a href=\"".url("modele_famille",array('id_rayon' => (int)$_REQUEST["rayon"], 'nom_rayon' => $row_url["rubrique_url"], 'id_marque' => (int)$_REQUEST["marque"], 'url_marque' => $row_url["url_marque"], 'id_modele' => (int)$_REQUEST["modele"], 'url_modele' => $row_url["url_modele"], 'id_famille' => $family_data["family_client_id"], 'url_famille' => $family_data["family_client_url"]))."\">".$family_data['family_client_name']." pour ".$cat_sous_cat."</a>";
					}
				?>
                    <option <?php if ($family_data['family_client_id'] == $_REQUEST['famille']) echo 'selected="selected"'; ?> value="<?php echo $family_data['family_client_id']; ?>"><?php echo str_replace('Les ', '', $family_data['family_client_name']); ?></option>
				<?php
                }
                ?>
                </select>
            Marque : 
                <select name="fabriquant" onchange="this.form.submit();">
                    <option value="">Toutes les marques</option>
				<?php
                $family_query = tep_db_query("SELECT distinct(m.manufacturers_name), m.manufacturers_id
											  FROM " . TABLE_PRODUCTS . " p, " . TABLE_MANUFACTURERS . " as m, " . TABLE_PRODUCTS_TO_CATEGORIES . " as ptc
											  WHERE p.products_id = ptc.products_id and p.manufacturers_id = m.manufacturers_id " . $rqt_family . "and p.products_status <> 0 
											  ORDER BY m.manufacturers_name");
				
				//if (!empty($_REQUEST['fabriquant'])) { $_REQUEST['fabriquant']=''; }
				
				while($family_data = tep_db_fetch_array($family_query)){
				
				if (empty($_REQUEST['fabriquant'])) {
					echo '<option value="' . $family_data['manufacturers_id'] . '">' . str_replace("Accessoires ", "", $family_data["manufacturers_name"]) .'</option>';
				} else {
				?>
                    <option <?php if ($family_data['manufacturers_id'] == $_REQUEST['fabriquant']) echo 'selected="selected"'; ?> value="<?php echo $family_data['manufacturers_id']; ?>"><?php echo str_replace('Accessoires ', '', $family_data['manufacturers_name']); ?></option>
				<?php
                }
				}
                ?>
                </select>
            Trier : 
                <select name="tri" onchange="this.form.submit();">
                    <option <?php if ($tri == 'nom') echo 'selected="selected"'; ?> value="nom">par le nom</option>
                    <option <?php if ($tri == 'ajout') echo 'selected="selected"'; ?> value="ajout">par nouveaut�</option>
                    <option <?php if ($tri == 'prix' && $ordre == 'desc') echo 'selected="selected"'; ?> value="prix_desc">du + cher au - cher</option>
                    <option <?php if ($tri == 'prix' && $ordre == 'asc') echo 'selected="selected"'; ?> value="prix_asc">du - cher au + cher</option>
                </select>
                
                <?php if (isset($_REQUEST["recherche"])) { ?>
            	<input type="hidden" name="recherche" value="<?php echo $get_de_recherche; ?>" />
                <?php } ?>
        </form>
        </div>
        <?php } ?>
        
        <div class="mod_manque">Vous cherchez un article et il n'est pas pr�sent ? <a href="<?php echo BASE_DIR; ?>/contact.php">Contactez-nous</a> et nous l'ajouterons pour vous.</div>
        <?php
		if (isset($_REQUEST["recherche"]) && !empty($_REQUEST["recherche"])) {
			$nb_articles_par_page = 60;
			
			if (!isset($_REQUEST["page"])) {
				$page = 1;
			} else {
				$page = (int)$_REQUEST["page"];
			}
			
			$premier_article_page = ($page-1) * $nb_articles_par_page;

			$products_query = tep_db_query("SELECT pd.products_id, pd.products_name, pd.products_url, pd.balise_title_lien_image, pd.balise_title_lien_texte, 
										   p.products_price, p.products_bimage, p.products_sort_order, p.products_date_added, p.products_manage_stock, p.products_note,
										   r.rubrique_url, r.rubrique_name, r.rubrique_id, 
										   cd.categories_id, cd.categories_name, cd.categories_url, 
										   cd2.categories_id as id_marque, cd2.categories_url as url_marque, cd2.categories_name as nom_marque ,
										   ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1,
										   s.specials_new_products_price,
										   pvf.prix_vente
										   
											FROM " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_PRODUCTS . " p 
											left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id
											left join ". TABLE_SPECIALS ." as s on (p.products_id=s.products_id and (expires_date>NOW() or expires_date='0000-00-00 00:00:00'))
											left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash='0000-00-00'))),
											" . TABLE_CATEGORIES . " c, 
											" . TABLE_PRODUCTS_TO_CATEGORIES . " p2c,
											" . TABLE_CATEGORIES_DESCRIPTION . " cd, 
											" . TABLE_CATEGORIES_DESCRIPTION . " cd2, 
											" . TABLE_RAYON . " r, 
											" . TABLE_CATEGORIES_RAYON . " cr 
											
											WHERE p.products_status = '1' " . $rqt_filtre . "
											and p.products_id = pd.products_id
											and p.products_id = p2c.products_id
											and p2c.categories_id = c.categories_id
											and cd.categories_id=c.categories_id
											and c.categories_id = cr.categories_id
											and cr.rubrique_id = r.rubrique_id
											and c.parent_id = cd2.categories_id
											and (pd.products_name like '%" . mysql_real_escape_string($get_de_recherche) . "%'
												or p.products_model like '%" . mysql_real_escape_string($get_de_recherche) . "%'
												or p.products_ref_origine like '%" . mysql_real_escape_string($get_de_recherche) . "%'
												or cd.categories_name like '%" . mysql_real_escape_string($get_de_recherche) . "%')
											". $query_custom_type . $query_rubrique_custom_type ."
											GROUP BY p.products_id
											ORDER BY " . $rqt_tri);
											
		} else {
			$products_query = tep_db_query("SELECT pd.products_id, pd.products_name, pd.products_url, pd.balise_title_lien_image, pd.balise_title_lien_texte, 
										   p.products_price, p.products_bimage, p.products_sort_order, p.products_date_added, p.products_manage_stock, p.products_note,
										   ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1,
										   s.specials_new_products_price,
										   pvf.prix_vente
											
											FROM " . TABLE_PRODUCTS_DESCRIPTION . " as pd,
											" . TABLE_PRODUCTS . " as p
											left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id
											left join ". TABLE_SPECIALS ." as s on (p.products_id=s.products_id and (expires_date>NOW() or expires_date='0000-00-00 00:00:00'))
											left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash='0000-00-00'))),
											". TABLE_CATEGORIES ." c,
											". TABLE_PRODUCTS_TO_CATEGORIES ." ptc,
											". TABLE_CATEGORIES_RAYON ." cr,
											". TABLE_RAYON ." r
											
											WHERE p.products_id=ptc.products_id 
											and p.products_id = pd.products_id ". $rqt_filtre ."
											and p.products_status <> 0
											and ptc.categories_id=c.categories_id
											and c.categories_id=cr.categories_id
											and cr.rubrique_id=r.rubrique_id
											and ptc.categories_id='" . $id_modele . "'
											". $query_custom_type . $query_rubrique_custom_type ."
											
											ORDER BY " . $rqt_tri);
		}
		?>
        
        <div class="nbr_produits">Il y a <strong><?php echo tep_db_num_rows($products_query); ?></strong> Articles disponibles</div>
        <div style="clear:both;"></div>
        
        <?php if(!isset($get_de_recherche)) { ?><div id="articles_images"> <?php } else { ?><div id="articles_images" style="display:block"><?php } ?>
        
	        <table>
    	        <tr>
                <?php
                if (tep_db_num_rows($products_query) > 0) {
                    $i = 0; $j = 0;
					$nb_articles_ligne = 5;
					$est_affiche = true;
                    while($products_data = tep_db_fetch_array($products_query)) {
						$j++;
						
						if (isset($_REQUEST['recherche'])) {
							$est_affiche = ($j > $premier_article_page && $j <= $premier_article_page + $nb_articles_par_page);
						}
						
						if ($est_affiche) {
							$i++;
							
							if (isset($_REQUEST["recherche"]) && !empty($_REQUEST["recherche"])) {
								$id_rayon = $products_data['rubrique_id'];
								$nom_rayon = $products_data['rubrique_name'];
								$url_rayon = $products_data['rubrique_url'];
								$id_modele = $products_data['categories_id'];
								$nom_modele = $products_data['categories_name'];
								$url_modele = $products_data['categories_url'];
								$id_marque = $products_data['id_marque'];
								$nom_marque = $products_data['nom_marque'];
								$url_marque = $products_data['url_marque'];
							}
							
							$products_name = bda_product_name_transform($products_data['products_name'], $id_modele);
							$products_url = bda_product_name_transform($products_data['products_url'], $id_modele);
							
							$prix = calculate_price_for_product($products_data);
							//$id_avisverifies = $products_data['products_id'];
							
							$url = url('article', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele, 'url_modele' => $url_modele, 'id_article' => $products_data['products_id'], 'url_article' => $products_url));
							
							?>

                                	<td class="article" <?php if ($i % $nb_articles_ligne == 1) echo 'style="border-left: none;"'; ?> onclick="location.href='<?php echo $url; ?>';">
                                        <div class="image_article">
                                            <a href="<?php echo $url; ?>" title="<?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?>"><img src="<?php echo BASE_DIR . "/images/products_pictures/petite/" . $products_data['products_bimage']; ?>" alt="<?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?>" width="140px"  height="140px" /></a>
                                            <?php
											
											if($prix['flash'] > 0){ ?>
                                            	
                                            	<img class="image_article_filigrane" src="<?php echo BASE_DIR . "/template/base/liste_produits/vente-flash.png"; ?>" alt="Vente Flash <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>" title="Solde sur <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>" />
                                            <?php
                                            }
                                            elseif($products_data['products_manage_stock'] == 'destock_visible') { ?>
                                            	
												<img class="image_article_filigrane" src="<?php echo BASE_DIR . "/template/base/liste_produits/destockage.png"; ?>" alt="destockage <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>" title="Fin de serie <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>" />
											<?php
                                            }
											elseif($prix['promo'] > 0){ ?>
                                            	
                                            	<img class="image_article_filigrane" src="<?php echo BASE_DIR . "/template/base/liste_produits/promotion.png"; ?>" alt="Promotion <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>" title="Solde sur <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>" />
                                            <?php
                                            }?>
                                        </div>
                                        <div class="nom_article">
                                            <a href="<?php echo $url; ?>" title="<?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>"><?php echo $products_name; ?></a>
                                        </div>
                                        <div class="prix_article">
                                            <?php
												if (show_price($prix) != 0) { 
													prix2img(show_price($prix), 'moyen');
												}
											?>
                                        </div>
                                        <span><?php if($prix['type']=='HT') echo 'Hors Taxes'; ?></span>
										<?php 
										// AVIS VERIFIES
										if ($_SERVER["REMOTE_ADDR"] == '82.225.186.0') { // 82.225.186.124 78.245.149.37
										
										} 
										
										if ($products_data['products_note'] != 0 ) {
											$note = $products_data['products_note'];
											
											echo '<div text-align="center" style="padding-top:10px;">';
											// Fonction d'affichage des notes en image avec demi note
											echo avis_verifies_complet($note);
											echo '</div>';
										} 
										// END AVIS VERIFIES
										?>
                                	</td>
								<?php 
									if ($i % $nb_articles_ligne == 0) {
										?>
                                			</tr><tr>
                                        <?php
									}
								?>
							<?php
						}
                    }
                } else {
                ?>
					<div style="background-color:#CCC">
					<br />
					<br />
					<strong>Il n'y a encore aucun article list� dans cette cat�gorie.<br />
					<br />
					Si vous cherchez un article bien pr�cis, vous pouvez nous contacter et nous nous efforcerons de vous le trouver !<br />
					<br />
					<br />
					</strong>
					</div>
                <?php
                }
                ?>
				</tr>
            </table>
        </div>
        
        
        <?php if(!isset($get_de_recherche)) { ?>
        <div id="articles_icones">
            <table>
                <tr>
                    <th>&nbsp;</th>
                    <th><a href="<?php echo $url; ?>"><img src="<?php if ($tri == 'nom' && $ordre == 'asc') echo BASE_DIR .'/template/base/liste_produits/flech_tri_haut.png'; else echo BASE_DIR .'/template/base/liste_produits/flech_tri_bas.png'; ?>" alt="" height="7" width="7"/>&nbsp;Nom</a></th>
                    <th><a href="<?php echo $url; ?>"><img src="<?php if ($tri == 'prix' && $ordre == 'asc') echo BASE_DIR .'/template/base/liste_produits/flech_tri_haut.png'; else echo BASE_DIR .'/template/base/liste_produits/flech_tri_bas.png'; ?>" alt="" height="7" width="7"/>&nbsp;Prix <?php echo $prix['type']; ?></a></th>
                    <th>Quantit�</th>
                    <th>&nbsp;</th>
                </tr>
                <?php
                if (tep_db_num_rows($products_query) > 0) {
                    
					$i = 0;
					$j = 0;
					
					tep_db_data_seek($products_query, 0);
					$est_affiche = true;
                    while($products_data = tep_db_fetch_array($products_query)) {
						
						$j++;
						
						if (isset($_REQUEST['recherche'])) {
							
							$est_affiche = ($j > $premier_article_page && $j <= $premier_article_page + $nb_articles_par_page);
						}
						
						if ($est_affiche) {
							
							$i++;
							if (isset($get_de_recherche) && !empty($get_de_recherche)) {
								$id_rayon = $products_data['rubrique_id'];
								$nom_rayon = $products_data['rubrique_name'];
								$url_rayon = $products_data['rubrique_url'];
								$id_modele = $products_data['categories_id'];
								$nom_modele = $products_data['categories_name'];
								$url_modele = $products_data['categories_url'];
								$id_marque = $products_data['id_marque'];
								$nom_marque = $products_data['nom_marque'];
								$url_marque = $products_data['url_marque'];
							}
							
							$products_name = bda_product_name_transform($products_data['products_name'], $id_modele);
							$products_url = bda_product_name_transform($products_data['products_url'], $id_modele);
							
							$prix = calculate_price_for_product($products_data);
							
							$url = url('article', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele, 'url_modele' => $url_modele, 'id_article' => $products_data['products_id'], 'url_article' => $products_url));
							?>
							<tr class="<?php if (($i % 2) == 0) {echo 'fond_1';} else {echo 'fond_2';} ?>">
								<td class="image_article"><a href="<?php echo $url; ?>" title="<?php echo $products_data['balise_title_lien_texte'];?>tttt" ><img src="<?php echo BASE_DIR . "/images/products_pictures/petite/" . $products_data['products_bimage']; ?>" title="Accessoire <?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);  ?>" alt="<?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?>" /></a></td>
								<td class="nom" onclick="location.href='<?php echo $url; ?>';"><?php echo "<a href=\"" . $url . "\" title=\"".bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele)."\">" . $products_name . "</a>"; ?></td>
								<td class="prix">
									<?php
									  
									  if($prix['flash'] > 0) {
										  
										  echo "<span style='color:#8e2eaa; font-size:13px;'><strong>Flash</strong></span>";
										  echo "<br /><span style='text-decoration:line-through;'>". format_to_money($prix['normal']) ." &euro;</span>";
										  echo "<br />". format_to_money($prix['flash']) ." &euro;";
									  }
									  elseif($prix['promo'] > 0) {
										  
										  echo "<span style='color:#8e2eaa; font-size:13px;'><strong>Promo</strong></span>";
										  echo "<br /><span style='text-decoration:line-through;'>". format_to_money($prix['normal']) ." &euro;</span>";
										  echo "<br />". format_to_money($prix['promo']) ." &euro;";
									  }
									  else echo format_to_money($prix['normal']) ." �";
									?>
                                </td>
								<td class="quantite"><input id="qty_<?php echo $products_data['products_id'] .'_'. $id_marque .'_'. $id_modele; ?>" type="text" size="2" disabled="disabled" value="1"/></td>
								<td class="ajout_panier" >
                                	<!--<a href="<?php echo BASE_DIR; ?>/includes/lightboxes/ajout_panier.php?parent='icones'&amp;nom=<?php echo $products_name; ?>&amp;produit=<?php echo $products_data['products_id']; ?>&amp;<?php echo tep_get_all_get_params(); ?>" class="lbOn">-->
                                	<img src="<?php echo BASE_DIR; ?>/template/base/liste_produits/icone_panier_grand.png"
                                    	 title="achat <?php echo bda_product_name_transform(strip_tags($products_data['products_name']), $id_modele); ?>"
                                         alt="Achat <?php echo bda_product_name_transform(strip_tags($products_data['products_name']), $id_modele); ?> "
                                         height="49"
                                         width="49"
                                         id="products_<?php echo $products_data['products_id'] .'_'. $id_marque .'_'. $id_modele; ?>"
                                         class="add_one_product_to_basket pointer"/>
									<!--</a>-->
								</td>
							</tr>
							<?php
						}
					}
                } else {
                ?>
                <tr>
                    <td colspan="5" style="text-align: center; background-color:#CCC; font-weight: bold;">
					<br />
					<br />
					<strong>Il n'y a encore aucun article list� dans cette cat�gorie.<br />
					<br />
					Si vous cherchez un article bien pr�cis, vous pouvez nous contacter et nous nous efforcerons de vous le trouver !<br />
					<br />
					<br />
					</strong>
					</td>
                </tr>
                <?php
                }
                ?>
            </table>
        </div>
        
        
		<?php } ?>
        <div style="clear:both;"></div>
		
		<div class="pagination">
            <?php
			// On n'affiche la pagination uniquement dans la page des resultats de recherche
			if (isset($_REQUEST["recherche"])) { 
			
				$nb_articles = tep_db_num_rows($products_query);
				$nb_pages = ceil($nb_articles / $nb_articles_par_page);
				
				if ($nb_articles > $nb_articles_par_page) {
					if ($nb_pages >= 2) {
						
						$page_precedente=$page-1;
						$page_suivante=$page+1;
						if ($page!=1) {
							echo "<a href='" . $_SERVER['PHP_SELF'] . "?page=". $page_precedente ."' title='Page pr�c�dente'><strong>&laquo;</strong></a>";
						}
						for ($i = 1; $i <= $nb_pages; $i++) {
							if ($i==$page) {
								echo "<a href='" . $_SERVER['PHP_SELF'] . "?recherche=" . $get_de_recherche . "&page=" . $i ."' title='page ".$i."' class='active'>". $i ."</a>";
							} else {
								echo "<a href='" . $_SERVER['PHP_SELF'] . "?recherche=" . $get_de_recherche . "&page=" . $i ."' title='page ".$i."'><strong>". $i ."</strong></a>";
							}
						}
						if ($page!=$nb_pages ) {
							echo "<a href='" . $_SERVER['PHP_SELF'] . "?page=". $page_suivante ."' title='Page suivante'><strong>&raquo;</strong></a>";
						}
						
					}
				}
			}
			?>
        </div>
		
        <div class="mod_manque">
			Vous cherchez un article et il n'est pas pr�sent ? <a href="<?php echo BASE_DIR; ?>/contact.php">Contactez-nous</a> et nous l'ajouterons pour vous.
		</div>
	</div>
	
	
	<?php // AVIS VERIFIES #7C7972 ?>
	<div style="margin:20px 5px 5px 5px; padding:10px; background-color:#ffffff; border-top:1px dashed grey;">		
		<iframe id='AV_widget_iframe' frameBorder="0" width="100%" height="100%" src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2ahorizontal_index.html"></iframe>
	</div>
	<?php // End AVIS VERIFIES ?>
	
</div>
<div id="overlay"></div>
<div id="lightbox">
</div>

<!-- Div ou la requ�te AJAX va �crire l'affectation de variable de session -->
<div id="affect_session_var"></div>

<!-- On affiche la bonne div correspondant au type de vue choisie -->
<?php if(!isset($get_de_recherche)) { ?>	
<script type="text/javascript">change_type('<?php echo $_SESSION['type_vue']; ?>');</script>
<?php } ?>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>