<?php
/* Auteur : Paul
   Page d'erreur lorsque le client n'a pas entr� les bons param�tres dans l'URL*/

require("includes/page_top.php");
require("includes/meta/url_error.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
		<span style="color: red; font-weight: bold;">
			La page que vous demandez n'�xiste plus. <br />
			<br />
			Merci de revenir sur la page d'accueil du site en cliquant sur le lien ci dessous !
		</span>
	<br />
	<br />
	<br />	
    <a href="../index.php">Revenir sur la page d'Accueil</a>
	<br />
	<br />
	<br />
	<br />
	<br />
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>