<?php
/* Plan du site*/

require("includes/page_top.php");
require("includes/meta/plan_du_site.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">

<?php

/* initialisation des variables */
$i = 0;

switch($_SESSION['customers_type']) {
	
	case 1 :
		
		$rub_query = 'and rayon_status_client=\'1\' ';
		$cat_query = 'and categories_status_client=\'1\' ';
	break;
	
	case 2 :
		$rub_query = 'and rayon_status_pro=\'1\' ';
		$cat_query = 'and categories_status_pro=\'1\' ';
	break;
	
	case 3 :
		$rub_query = 'and rayon_status_rev=\'1\' ';
		$cat_query = 'and categories_status_rev=\'1\' ';
	break;
	
	case 4 :
		$rub_query = 'and rayon_status_adm=\'1\' ';
		$cat_query = 'and categories_status_adm=\'1\' ';
	break;
	
	default :
		$rub_query = 'and rayon_status_tous=\'1\' ';
		$cat_query = 'and categories_status_tous=\'1\' ';
	break;
	
}


$query_rayon = 'select rubrique_id, rubrique_name, rubrique_url from '. TABLE_RAYON .' where rubrique_status=\'1\' '. $rub_query .'order by rayon_status_tous';
$query_rayon = tep_db_query($query_rayon);

$nb_rayon = tep_db_num_rows($query_rayon);

while($data_rayon = tep_db_fetch_array($query_rayon)) {
	
	if($i++ > 0) echo '</div>';
	
	echo '<div style="float:left;width:'. round(100/$nb_rayon) .'%;">';
	
	echo '<b><a href="'. WEBSITE . BASE_DIR .'/'. $data_rayon['rubrique_id'] .'-'. str_replace(' ', '-', $data_rayon['rubrique_url']) .'.html" style="text-decoration:none;">'. $data_rayon['rubrique_name'] .'</a></b><br />';
	
	$query_cat = 'select c.categories_id, categories_name, categories_url from '. TABLE_CATEGORIES_RAYON .' cr, '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd ';
	$query_cat .= 'where cr.categories_id=c.categories_id and c.categories_id=cd.categories_id and parent_id=0 and categories_status=\'1\' '. $cat_query .'and sort_order>0 and rubrique_id='. $data_rayon['rubrique_id'] .' order by sort_order';
	
	$query_cat = tep_db_query($query_cat);
	
	while($data_cat = tep_db_fetch_array($query_cat)) {
		
		$subcat_exists = true;
		
		$categorie = $data_cat['categories_id'];
		
		echo '<p><b><a href="'. WEBSITE . BASE_DIR .'/'. $data_rayon['rubrique_id'] .'-'. $data_rayon['rubrique_url'] .'/'. $data_cat['categories_id'] .'-'. str_replace(' ', '-',$data_cat['categories_url']) .'.html" style="text-decoration:none;">'. $data_cat['categories_name'] .'</a></b></p>';
		
		$query_subcat = 'select c.categories_id, categories_name, categories_url from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd ';
		$query_subcat .= 'where cd.categories_id=c.categories_id and c.parent_id='. $categorie .' and categories_status=\'1\' and sort_order>0 order by sort_order';
		//echo $query_subcat;
		$query_subcat = tep_db_query($query_subcat);
		
		if(tep_db_num_rows($query_subcat) > 0) {
			
			while($data_subcat = tep_db_fetch_array($query_subcat)) {
			
				echo '<p><em><a href="'. WEBSITE . BASE_DIR .'/'. $data_rayon['rubrique_id'] .'-'. $data_rayon['rubrique_url'] .'/'. $data_cat['categories_id'] .'-'. str_replace(' ', '-',$data_cat['categories_url']) .'/'. $data_subcat['categories_id'] .'-'. str_replace(' ', '-',$data_subcat['categories_url']) .'.html" style="text-decoration:none;">'. $data_subcat['categories_name'] .'</a></em></p>';
			}
			
			$categorie = $data_subcat['categories_id'];
		}
	}
}

?>
</div><div style="clear:both;"></div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>