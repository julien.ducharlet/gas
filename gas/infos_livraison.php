<?php
/* Auteur : Paul 
   Affiche les infos de livraison pour chaque pays*/

$nom_page = "infos_livraison";
   
require("includes/page_top.php");

$res_pays = tep_db_query("SELECT *
							FROM " . TABLE_INFOS_LIVRAISON . "
							WHERE id_livraison = '" . $_REQUEST['id_livraison'] . "'");
$pays_data = tep_db_fetch_array($res_pays);
$url_clean="/livraison/".$pays_data['id_livraison']."-".$pays_data['url'].".html";
$baseline=$pays_data["meta_baseline"];

if($_SERVER['REQUEST_URI']!=$url_clean){
	redirect(301,$url_clean);
}

require("includes/meta/infos_livraison.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">
	<div id="infos_livraison">
    	<div class="titre"><?php echo $pays_data['titre']; ?></div>
        
        <div class="zone_1">
        <?php echo $pays_data['texte']; ?>
        </div>
        
        <div class="image_fourgonnette">
        </div>
        
        <div style="clear:both;"></div>

		<div class="zone_2">
            <div class="zone_2_top">
            </div>
            <div class="zone_2_corps">
			Tarif des frais de port de la poste Francaise en fonction du poids (Kg) de votre commande
            <table>
                	<tr>
                    	<td class="titre_tableau">Poids jusqu'�</td>
						<?php
                            $livraison_query = tep_db_query("SELECT *
                                                             FROM " . TABLE_MODULE_LIVRAISON . "
                                                             WHERE zone_livraison_id = '" . $pays_data['countries_id'] . "'");
                            while ($livraison_data = tep_db_fetch_array($livraison_query)) {
                                echo '<td>' . round($livraison_data['poids_livraison'],1) . ' kg</td>';
                            }
                        ?>
					</tr>
                    <tr>
                    	<td class="titre_tableau">Prix <?php if(taux_taxe()==1) echo "HT"; else echo "TTC"; ?></td>
                        <?php
							tep_db_data_seek($livraison_query, 0);
                            while ($livraison_data = tep_db_fetch_array($livraison_query)) {
                                echo '<td>' . format_to_money($livraison_data['prix_livraison']*taux_taxe()) . ' �</td>';
                            }
                        ?>
					</tr>
                </table>
            </div>
            <div class="zone_2_bottom">
            </div>
        </div>
        
	</div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>