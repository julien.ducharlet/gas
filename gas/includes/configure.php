<?php
/* Fichiers regroupant les constantes de configuration */
	
	define('NOM_DU_SITE', 'Group Army Store');
	define('NOM_DU_SITE_MAJ', 'Group Army store');
	define('TEL_NORMAL', '09 53 31 59 47');
	define('TEL_PRO', '09 53 31 59 47');
	define('ADRESSE_SITE', 'www.GeneralArmyStore.fr');
	
	
	define('WEBSITE', 'https://www.generalarmystore.fr');
	// desactivation par Thierry le 19/04/2018 car d�j� dans le configure_css.php
	//define('BASE_DIR', '/gas');
	define('BASE_DIR_ADMIN', '/gasadmin');
	define('ABSOLUTE_DIR', '/var/www/vhosts/generalarmystore.fr/httpdocs');
	define('DATABASE_TABLE_DIR', '/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php');
	define('DIR_LANGUAGES', '/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/languages/french/');
	
	// Param�tres de la base de donn�es
	define('DB_SERVER', 'localhost');
	define('DB_SERVER_USERNAME', 'generalarmystore');
	define('DB_SERVER_PASSWORD', 'EveiLaiz5g');
	define('DB_DATABASE', 'generalarmystore');
	define('USE_PCONNECT', 'false'); // Utilisation de connexions persistantes ?
	define('STORE_SESSIONS', 'mysql'); // leave empty '' for default handler or set to 'mysql'
	
	define('GESTION_PRIX_PAR_QUANTITE', '1');
	define('GESTION_ONGLET_PRIX_PAR_QUANTITE', '1');
	
	define('TEXTE_PRO', '&nbsp;');
	
	define('ADRESSE', '<strong>GENERAL ARMY STORE</strong><br />
            21, rue Pasteur<br />
            Entr&eacute;e rue H. Bajard<br />
            26260 SAINT DONAT sur L\'HERBASSE<br /><br /><br />');
	
	// desactivation par Thierry le 19/04/2018 
	//define('SERVICE_COMMERCIAL', 'Vous pouvez nous joindre par t&eacute;l&eacute;phone <strong>du lundi au vendredi</strong>, de <strong>9h &agrave; 12h30</strong> et de <strong>14h &agrave; 18h30</strong>');
	
	define('TEMPS_REPONSE', '&nbsp;'); // utilis� pour indiqu� le temps de r�ponse au email 
	
	define('SMS_NOTIFICATION', '0');
	define('AFFICHE_PLUS_RECHERCHES', '0');
	define('ID_ARTICLE_CADEAU_PARRAINAGE', '2756');
	
	define('GEO_ZONE_AVEC_TVA', 9); // 9 c'est SANS TVA ???
	define('GEO_ZONE_SANS_TVA', 8); // 8 c'est AVEC TVA ???
	
	define('SESSION_BLOCK_SPIDERS ', 'false');
	define('SESSION_FORCE_COOKIE_USE', 'false');
?>
