<?php
$meta_title="La page que vous avez demandée n'existe plus";
$meta_description="Cette page n'est plus disponible pour le moment ou définitivement, merci de revenir a l'accuiel du site.";
$meta_keywords="Page inexistante, page erreur";
?>