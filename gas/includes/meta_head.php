<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" lang="fr">

<head>

<meta name="y_key" content="19544b5c9ac430ab" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="verify-v1" content="7IeWVS06x6p6PV+Imbvj7vnBk7enLUsFO4S2jogKKg4=" />
<?php include ('favicon.php'); ?>

<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/png_fix/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/smartlook/smartlook.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo BASE_DIR; ?>/css/lightbox.css.php" />


<script type="text/javascript" src="<?php echo BASE_DIR; ?>/compte/fonction/function.js"></script>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/compte/fonction/date.js"></script>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/faq.js"></script>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/compte/fonction/account.js"></script>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/compte/fonction/address_book.js"></script>

<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/general.js"></script>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/jquery/jquery.js"></script>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/jquery/jquery.pngFix.js"></script>

<!-- Tarte Au Citron -->
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/tarteaucitron/tarteaucitron.js"></script>

<script type="text/javascript">
tarteaucitron.init({
	"hashtag": "#tarteaucitron", /* Ouverture automatique du panel avec le hashtag */
	"highPrivacy": false, /* d�sactiver le consentement implicite (en naviguant) ? */
	"orientation": "bottom", /* le bandeau doit �tre en haut (top) ou en bas (bottom) ? */
	"adblocker": false, /* Afficher un message si un adblocker est d�tect� */
	"showAlertSmall": true, /* afficher le petit bandeau en bas � droite ? */
	"cookieslist": true, /* Afficher la liste des cookies install�s ? */
	"removeCredit": true /* supprimer le lien vers la source ? */
});

$(document).ready(function(){
	$(document).pngFix(); 
	
	
	<?php 
	// add product to basket 
	// Plus besoin de produits_liste, promotions et nouveautes
	if(	get_page_name()=='produits_fiches.php' || 
				get_page_name()=='produits_liste.php' || 
				get_page_name()=='promotions.php' || 
				get_page_name()=='nouveautes.php') {
		?>
		
		var produit = '<?php echo (isset($_GET['produit']) && !empty($_GET['produit'])) ? (int)$_GET['produit'] : 0; ?>';
		var marque = '<?php echo (isset($_GET['marque']) && !empty($_GET['marque'])) ? (int)$_GET['marque'] : 0; ?>';
		var modele = '<?php echo (isset($_GET['modele']) && !empty($_GET['modele'])) ? (int)$_GET['modele'] : 0; ?>';
		var id_rayon = '<?php echo (isset($_GET['rayon']) && !empty($_GET['rayon'])) ? (int)$_GET['rayon'] : 0; ?>';
		
		var id_article = produit +'_'+ marque +'_'+ modele;
		var qte_product = $("#quantite_fiche_"+ id_article).val();
		var option_id = "";
		
		var qte_product = $("#quantite_fiche_"+ id_article).val();
		
		var $optionsHTML = $("#pack_options");
		$("#pack_options").remove();
		
		
		$(".add_product_to_basket").click(function() {
			
			qte_product = $("#quantite_fiche_"+ id_article).val();
			
			$("#lightbox_pack").hide();
			$("#lightbox_moins_cher").hide();
			$("#lightbox_question").hide();
			$(".pack_images").hide();
			
			$("#lightbox_product").show();
			$(".product_images").show();
			$("#options").show();
			
			$("#overlay").show();
			$("#lightbox").slideDown("slow");
		});
		
		$(".add_one_product_to_basket").click(function() {
			
			currentId = $(this).attr('id').split('_');
			produit = currentId[1];
			marque = currentId[2];
			modele = currentId[3];
			
			id_article = produit +'_'+ marque +'_'+ modele;
			
			qte_product = $("#qty_"+ id_article).val();
			
			if(qte_product<1) qte_product = 1;
			
			validate_form_one_product("<?php echo WEBSITE . BASE_DIR; ?>/includes/lightboxes/ajout_panier.php?pID="+ produit +"&cPath="+ marque +'_'+ modele);
			
			$("#lightbox_product").show();
			$(".product_images").show();
			$("#options").show();
			
			$("#overlay").show();
			$("#lightbox").slideDown("slow");
		});
		
		$(".add_pack_to_basket").click(function() {
			
			id_pack = $(this).attr("id");
			id_pack = id_pack.split("_");
			id_pack = id_pack[1];
			
			$("#lightbox_product").hide();
			$("#lightbox_moins_cher").hide();
			$("#lightbox_question").hide();
			$(".product_images").hide();
			
			$("#lightbox_pack").show();
			$(".pack_images").show();
			$("#options").show();
			
			$("#lightbox_pack div").hide();
			$("#pack_"+ id_pack).show();
			$("#pack_options").show();
			
			$optionsHTML.prependTo("#pack_"+ id_pack +" .images");
			
			$("#overlay").show();
			$("#lightbox").slideDown("slow");
		});
			
		//gestion des lightbox-products
		$(".product_back_to_product").live('click', function() {
			option_type = "product_options";			
			option_id = verif_options(option_type);			
			if ((option_id!="" && document.getElementsByName(option_type).length>=1) || document.getElementsByName(option_type).length == 0) {			
				validate_form("<?php echo WEBSITE . BASE_DIR; ?>/includes/js/ajout_panier.php?ajax=1&id_rayon="+ id_rayon +"&id_article="+ id_article + option_id +"&qte_article="+ qte_product, '',
						  "<?php echo WEBSITE . BASE_DIR; ?>/produits_liste.php?rayon="+ id_rayon +"&marque="+ marque +"&modele="+ modele);
			} else {
				alert('Vous devez choisir une option');
			}
		});
	
		$(".product_validate_basket").live('click', function() {
			option_type = "product_options";
			option_id = verif_options(option_type);
			if ( (option_id!="" && document.getElementsByName(option_type).length>=1) || document.getElementsByName(option_type).length == 0) {
				validate_form("<?php echo WEBSITE . BASE_DIR; ?>/includes/js/ajout_panier.php?ajax=1&id_rayon="+ id_rayon +"&id_article="+ id_article + option_id +"&qte_article="+ qte_product, '',
				"<?php echo WEBSITE . BASE_DIR; ?>/panier.php");
			} else {
				alert('Vous devez choisir une option');
			}
		});
		
		$(".send_mail_moins_cher").click(function() {
			if (verifyMoinsCherForm()) {				
				validate_form("<?php echo WEBSITE . BASE_DIR; ?>/includes/js/send_prix_moins_cher.php?ajax=1&"+ $("#form_moins_cher").serialize(), '', null);
			}
		});
		
		$(".send_question").click(function() {
			if (verifyQuestionForm()) {				
				validate_form("<?php echo WEBSITE . BASE_DIR; ?>/includes/js/send_question.php?ajax=1", $("#form_question").serialize(), null);
			}
		});
		
		//gestion des lightbox-packs
		$(".pack_back_to_product").click(function() {			
			option_type = "product_options";
			option_id = verif_options();
			
			var qte_product = $("#quantite_fiche_"+ id_article).val();
			
			if ((option_id!="" && document.getElementsByName(option_type).length>1) || document.getElementsByName(option_type).length == 0 || document.getElementsByName(option_type).length == 1) {
				validate_form("<?php echo WEBSITE . BASE_DIR; ?>/includes/js/ajout_panier.php?ajax=1&id_rayon="+ id_rayon +"&id_pack="+ id_pack +"_"+ marque +"_"+ modele + option_id +"&qte_pack=1", '',
				"<?php echo WEBSITE . BASE_DIR; ?>/produits_liste.php?rayon="+ id_rayon +"&marque="+ marque +"&modele="+ modele);
			} else {
				alert('Vous devez choisir une option');
			}
		});
	
		$(".pack_validate_basket").click(function() {
			option_type = "product_options";
			option_id = verif_options(option_type);
			var qte_product = $("#quantite_fiche_"+ id_article).val();
			
			if ((option_id!="" && document.getElementsByName(option_type).length>1) || document.getElementsByName(option_type).length == 0 || document.getElementsByName(option_type).length == 1) {
				validate_form("<?php echo WEBSITE . BASE_DIR; ?>/includes/js/ajout_panier.php?ajax=1&id_rayon="+ id_rayon +"&id_pack="+ id_pack +"_"+ marque +"_"+ modele + option_id +"&qte_pack=1", '',
				"<?php echo WEBSITE . BASE_DIR; ?>/panier.php");
			} else {
				alert('Vous devez choisir une option');
			}
		});
		
		$("#moins_cher_ailleurs_button").click(function() {
			
			$("#lightbox_product").hide();
			$(".product_images").hide();
			$("#lightbox_pack").hide();
			$(".pack_images").hide();
			$("#options").hide();
			
			$("#lightbox_question").hide();
			$("#lightbox_moins_cher").show();
			
			$("#overlay").show();
			$("#lightbox").slideDown("slow");
		});
		
		$("#question_button_prix").click(function() {
			
			$("#lightbox_product").hide();
			$(".product_images").hide();
			$("#lightbox_pack").hide();
			$(".pack_images").hide();
			$("#options").hide();
			
			$("#lightbox_moins_cher").hide();
			$("#lightbox_question").show();
			
			$("#overlay").show();
			$("#lightbox").slideDown("slow");
		});	
		
		$("#question_button").click(function() {
			
			$("#lightbox_product").hide();
			$(".product_images").hide();
			$("#lightbox_pack").hide();
			$(".pack_images").hide();
			$("#options").hide();
			
			$("#lightbox_moins_cher").hide();
			$("#lightbox_question").show();
			
			$("#overlay").show();
			$("#lightbox").slideDown("slow");
		});	
	<?php } ?>
	
	function verif_options(option_id) {
		formChecked = false;
		option_value = "";
		for(i=0 ; i < document.getElementsByName(option_id).length ; i++) {
			if(document.getElementsByName(option_id)[i].checked) option_value = document.getElementsByName(option_id)[i].value;
		}
		return option_value;
	}

	function validate_form(page_ajax, data, redirect) {
		$.ajax({
		   type: "POST",
		   url: page_ajax,
		   
		   data: data,
		   dataType: "text",
		   processData: false,
		   success: function(data){
				
				data = data.split("&");
				if (data.length==2) {
					mess_alert = data[0].split("=");
					mess_valid = data[1].split("=");
					if (mess_alert[0]=='alert') {//retour du message de l'envoi du prix moins cher
						alert(mess_alert[1]);
					}
					if(mess_valid[1]=="1") close_lightbox("lightbox", redirect);
				} else {
					close_lightbox("lightbox", redirect);
				}
		   }
		});
	}
	
	function validate_form_one_product(page_ajax) {		
		$.ajax({
		   type: "GET",
		   url: page_ajax,
		   dataType: "text",
		   processData: false,
		   success: function(data){
				$("#lightbox").html(data);
		   }
		});
	}
	
	// gestion du click en dehors de la lightbox
	$("#overlay").click(function() {
		close_lightbox("lightbox", null);
	});

	<!-- add address to book -->
	$(".add_address").click(function() {
		
		document.adresse.reset();
		
		//changement du titre de la box
		$("#lightbox h1").html("Ajouter une adresse");
		$("id_adresse").val();
		
		// pour sp�cifier s'il y a modification ou pas
		$(".images").html('<input type="hidden" name="add" /><img src="template/base/boutons/enregistrer_adresse.jpg" alt="Enregistrer l\'adresse" onclick="verif_modif(1);" />');
		
		$("#overlay").show();
		$("#lightbox").slideDown("slow");
	});
	
	$(".change_address").click(function() {
		
		//changement du titre de la box
		$("#titre_address").val($("#entry_address_title").val());
		$("#id_adresse").val($("#address_book_id").val());
		
		// pour sp�cifier s'il y a modification ou pas
		$(".images").html('<input type="hidden" name="mod" /><img src="template/base/boutons/enregistrer_les_modifications.jpg" alt="Enregistrer l\'adresse" onclick="verif_modif(0);" />');
		
		switch($("#entry_gender").val()) {
			case "m" : $("#mr").attr("checked", "checked");
			break;
			case "f": $("#mme").attr("checked", "checked");
			break;
			case "d": $("#mlle").attr("checked", "checked");
			break;
		}
		
		$("#lightbox h1").html("Modifier cette adresse");
		
		$("#nom_address").val($("#entry_lastname").val());
		$("#prenom_address").val($("#entry_firstname").val());
		$("#societe_address").val($("#entry_company").val());
		$("#a_address").val($("#entry_street_address").val());
		$("#ap_address").val($("#entry_suburb").val());
		$("#app_address").val($("#entry_street_address_3").val());
		$("#appp_address").val($("#entry_street_address_4").val());
		$("#cp_address").val($("#entry_postcode").val());
		$("#ville_address").val($("#entry_city").val());
		$("#pays").val($("#entry_country_id").val());
		//document.getElementById("pays").selectedIndex = 2;// =$("#entry_country_id").val();
		
		$("#overlay").show();
		$("#lightbox").slideDown("slow");
	});
	
	$(".close_lightbox").live('click', function() {
		close_lightbox("lightbox", null);
	});
})

function close_lightbox(box_id, redirect) {
	
	$("#"+ box_id).slideUp("slow").queue(function() {
												  
		$("#overlay").hide();
		$(this).dequeue();
		if(redirect != null) document.location.href=redirect;
	});
}

BASE_DIR = '<?php echo BASE_DIR .'/'; ?>';

function verifyMoinsCherForm() {
	isValid = true;
	if (document.getElementById('url_product').value=='') {
		isValid = false;
		alert('Vous devez rentrer une url pour l\'article');
	}
	else if (document.getElementById('prix_moins_cher').value=='') {
		isValid = false;
		alert('Vous devez rentrer le prix de l\'article');
	}
	else if (!verifMail(document.getElementById('ad_mail').value)) {
		isValid = false;
		alert('Vous devez rentrer une adresse mail valide');
	}
	return isValid;
}

function verifyQuestionForm() {
	isValid = true;
		if (document.getElementById('custom_name').value=='') {
		isValid = false;
		alert('Veuillez rentrer votre nom');
	}
	else if (document.getElementById('custom_tel').value=='') {
		isValid = false;
		alert('Veuillez rentrer votre num�ro de t�l�phone');
	}
	else if (!verifMail(document.getElementById('custom_mail').value)) {
		isValid = false;
		alert('Vous devez rentrer une adresse mail valide');
	}
	else if (document.getElementById('custom_question').value=='') {
		isValid = false;
		alert('Vous n\'avez pas pos� de question');
	}
	return isValid;
}

</script>

<!--[if lt IE 8]>
  <div style='border: 1px solid #F7941D; background: #FEEFDA; text-align: center; clear: both; height: 75px; position: relative; width=990px; margin:auto;'>
    <div style='position: absolute; right: 3px; top: 3px; font-family: courier new; font-weight: bold;'><a href='#' onclick='javascript:this.parentNode.parentNode.style.display="none"; return false;'><img src='https://www.ie6nomore.com/files/theme/ie6nomore-cornerx.jpg' style='border: none;' alt='Close this notice'/></a></div>
    <div style='width: 930px; margin: 0 auto; text-align: left; padding: 0; overflow: hidden; color: black;'>
      <div style='width: 60px; float: left;'><img src='https://www.ie6nomore.com/files/theme/ie6nomore-warning.jpg' alt='Warning!'/></div>
      <div style='width: 600px; float: left; font-family: Arial, sans-serif; text-align:center;'>
        <div style='font-size: 13px; font-weight: bold; margin-top: 12px;'>Vous utilisez un navigateur trop ancien pour profiter pleinement de notre site internet</div>
        <div style='font-size: 12px; margin-top: 6px; line-height: 12px;'>Prenez le temps de mettre votre navigateur � jour, en cliquant sur une des images sur la droite.<br>En cas de probl�me pour une commande, vous pouvez nous contacter au <b><?php echo TEL_NORMAL; ?></b> (tarif local)</div>
      </div>
      <div style='width: 70px; float: left;'><a href='https://fr.www.mozilla.com/fr/' target='_blank'><img src='https://www.ie6nomore.com/files/theme/ie6nomore-firefox.jpg' style='border: none;' alt='Get Firefox 3.5'/></a></div>
      <div style='width: 70px; float: left;'><a href='https://www.microsoft.com/downloads/details.aspx?FamilyID=341c2ad5-8c3d-4347-8c03-08cdecd8852b&DisplayLang=fr' target='_blank'><img src='https://www.ie6nomore.com/files/theme/ie6nomore-ie8.jpg' style='border: none;' alt='Get Internet Explorer 8'/></a></div>
      <div style='width: 70px; float: left;'><a href='https://www.apple.com/fr/safari/download/' target='_blank'><img src='https://www.ie6nomore.com/files/theme/ie6nomore-safari.jpg' style='border: none;' alt='Get Safari 4'/></a></div>
      <div style='float: left;'><a href='https://www.google.com/chrome?hl=fr' target='_blank'><img src='https://www.ie6nomore.com/files/theme/ie6nomore-chrome.jpg' style='border: none;' alt='Get Google Chrome'/></a></div>
    </div>
  </div>
<![endif]-->

<link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_DIR; ?>/css/base.css.php" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_DIR; ?>/css/contenu_panier.css.php" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_DIR; ?>/css/compte.css.php" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_DIR; ?>/css/plus_pop.css.php" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_DIR; ?>/css/top_ten.css.php" />

<?php
if (empty($nom_page)) { $nom_page =''; }
if ($nom_page == "infos_livraison")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/infos_livraison.css.php" />';

if ($nom_page == "accueil")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/accueil.css.php" />';

if ($nom_page == "categories_liste")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/categories_liste.css.php" />';

if ($nom_page == "modeles_liste")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/modele_liste.css.php" />';

if ($nom_page == "conditions_generales")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/conditions_generales.css.php" />';

if ($nom_page == "contact")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/contact.css.php" />';

if ($nom_page == "panier" || $nom_page == "panier_livraison")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/panier.css.php" />';

if ($nom_page == "produits_liste")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/produits_liste.css.php" />';

if ($nom_page == "produits_fiches")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/produits_fiches.css.php" />';

if ($nom_page == "qui_sommes_nous")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/qui_sommes_nous.css.php" />';

if ($nom_page == "recrutement")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/recrutement.css.php" />';
	
if ($nom_page == "partenaires")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/partenaires.css.php" />';

if ($nom_page == "details_pour_pro")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/details_pour_pro.css.php" />';

if ($nom_page == "details_pour_administration")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/details_pour_administration.css.php" />';

if ($nom_page == "tableau_des_tailles")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/tableau_des_tailles.css.php" />';

if ($nom_page == "nouveautes")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/produits_liste.css.php" />';

if ($nom_page == "promotions")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/produits_liste.css.php" />';

if ($nom_page == "marque")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/marque.css.php" />';

if ($nom_page == "ventes_flash")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/ventes_flash.css.php" />';

if ($nom_page == "inscription")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/inscription.css.php" />';

if ($nom_page == "account")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/account.css.php" />';
	
if ($nom_page == "address_book")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/address_book.css.php" />';
	
if ($nom_page == "account_edit" || $nom_page == "create_account_part" || $nom_page == "create_account_pro" || $nom_page == "sponsorship" || $nom_page == "panier_livraison")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/provisoire.css.php" />';

if ($nom_page == "account_edit")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/account_edit.css.php" />';

if ($nom_page == "inscription-particulier" || $nom_page == "inscription-professionnel" || $nom_page == "inscription-administration" || $nom_page == "inscription-association" || $nom_page == "inscription-entreprise" || $nom_page == "inscription-revendeur")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/inscription-all.css.php" />';
	
if ($nom_page == "devis" || $nom_page == "electronic_purse" || $nom_page == "order")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/commande.css.php" />';
	
if ($nom_page == "devis_details" || $nom_page == "order_detail")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/commande_details.css.php" />';
	
if ($nom_page == "login")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/login.css.php" />';
	
if ($nom_page == "logout")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/logout.css.php" />';
	
if ($nom_page == "faq")
	echo '<link rel="stylesheet" type="text/css" media="screen" href="' . BASE_DIR . '/css/compte/faq.css.php" />';

	
if(empty($meta_title)){
	require(ABSOLUTE_DIR.BASE_DIR."/includes/meta/defaut.php");
}
?>
<meta name="Description" content="<?php echo $meta_description;?>"/>
<meta name="Keywords" content="<?php echo $meta_keywords;?>"/>
<title><?php echo $meta_title . ' - ' . NOM_DU_SITE;?></title>

</head>

<body>