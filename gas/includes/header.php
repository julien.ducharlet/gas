<?php
/* Module affichant le header de la page comprenant la liste des pays de livraison, le module de recherche, la bannière et le menu général */
?>

<div align="center">

	<div id="header">
		<div class="header_drapeaux">
			
		</div>	
	</div>

	<div id="sous-header">
		
		<div class="header_promos_news">
			<a href="<?php echo BASE_DIR; ?>/nouveautes.php" class="footer_liens" title="Nouveaut&eacute;s de la boutique"><strong><font color="#FFFFFF">NOUVEAUT&Eacute;S</font></strong></a>
			
			<?php if((isset($_SESSION) && !empty($_SESSION['customer_id']) && $_SESSION['customers_type']!=3) || empty($_SESSION['customer_id'])) { ?>
				&nbsp;&nbsp;|&nbsp;&nbsp;
				<a href="<?php echo BASE_DIR; ?>/promotions.php" class="footer_liens" title="Promotions ou solde de la boutique"><strong><font color="#FFFFFF">PROMOTIONS</font></strong></a>			
			<?php } ?>
			
		</div>
		
		<div class="header_liens">
			<?php if (!empty($_SESSION['customer_id'])) { ?>
			
				<a href="<?php echo BASE_DIR; ?>/compte/account.php">Mon Compte</a>
				&nbsp;|&nbsp;
				<a href="<?php echo BASE_DIR; ?>/compte/logout.php?logout=true">Se D&eacute;connecter</a>
			
			<?php } else { ?>
				<a href="<?php echo BASE_DIR; ?>/compte/connexion.php">Mon Compte</a>
				
			<?php } ?>
			&nbsp;|&nbsp;
			<a href="<?php echo BASE_DIR; ?>/contact.php">Contactez-nous</a>
			&nbsp;|&nbsp;
			<a href="<?php echo BASE_DIR; ?>/faq.php" style="font-weight: bold;">Aide ?</a>
		</div>
	</div>

	<div id="background_banniere"></div>
	<div id="banniere">
		<a href="/index.php" title="Retour &agrave; l'accueil - Group Army Store / General Army Store">
			<div class="logo" id="logo-gas"></div>
		</a>
    
		<div class="banniere">   
        <?php //if(isset($_SESSION) && $_SESSION['customer_id']==20281) {
			
			$ban = '';
			$param = '';
			$page_name = get_page_name();
			$zone = get_zone($page_name, $PAGES);
			
			$query = 'select * from '. TABLE_BANNERS .' where status=1 and pages REGEXP \''. $zone .'\' ';
			$query .= 'and ((date_scheduled<=NOW() and (expires_date>=NOW() or expires_date IS NULL)) ';
			$query .= 'or date_scheduled IS NULL) ';
			$query .= 'order by RAND()';
			$query = tep_db_query($query);
			
			$data = tep_db_fetch_array($query);
			
			if(tep_db_num_rows($query) > 0) {
				?><script type="text/javascript">clickBanner(<?php echo $data['banners_id']; ?> ,'show');</script><?php
				if(!empty($data['banners_image'])) {
					$onclick = (!empty($data['banners_url'])) ? 'onclick="clickBanner('. $data['banners_id'] .', \'click\')"' : '';
					if(!empty($data['banners_url'])) $param = 'class="pointer"';
					echo '<img '. $param .' src="'. BASE_DIR .'/images/bannieres/' . $data['banners_image'] .'" alt="'. $data['banners_hover'] .'" '. $onclick .' />';
				}
				else {
					echo $data['banners_html_text'] .'</div>';
				}
			}
		
		?>
	</div>
    
    <?php include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_contenu_panier_baniere.php"); ?>
</div>

<div id="div_menu">
<?php require(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_menu.php"); ?>
</div>