<?php
/* Module g�rant tout le bas de la page avec les copyrights, le profiling mais aussi la fermeture de la base de donn�es ou tout traitement intervenant � la fin de l'affichage */
?>

<div id="copyright">
Copyright &copy; 2006 -  <?php echo date('Y', time()); ?> - <a href="https://www.generalarmystore.fr" title="GeneralArmyStore">www.generalarmystore.fr</a> - Conception et R�alisation &nbsp;<a href="https://www.pimentbleu.fr/membres/thierry-poulain/prestations/creation-de-boutique-e-commerce/" target="_blank" title="Agence Web et multim�dia de Montpellier - Sp�cialiste du E-Commerce">Agence Piment Bleu</a><br />
Siret : 447 513 342 00033 - <a href="<?php echo BASE_DIR; ?>/plan_du_site.php">Plan du site</a> - IP : <?php echo $_SERVER['REMOTE_ADDR']; ?>
</div>

<?php
	// Permet de r�cup�rer les avis g�n�ral de la boutique 
	$handle = fopen("https://cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/AWS/c2aefb07-1640-9f64-294e-c74d06179a2a_infosite_nafnor.txt", "r");
	$lecture_fichier = fgets($handle);
		// on r�cup�re le nombre d'avis et la note global
		$valeur = explode(";" , $lecture_fichier);
	fclose($handle);
?>

<?php
if ($nom_page != "produits_fiches") {
	echo '<div itemscope="" itemtype="https://data-vocabulary.org/Review-aggregate" id="copyright" style="padding-bottom:10px;">
			Nos clients nous font confiance, retrouvez la note de <span itemprop="itemreviewed">General Army Store</span> sur Avis V�rifi�s :
		<span itemprop="itemreviewed"></span>
			<span itemprop="rating" itemscope="" itemtype="https://data-vocabulary.org/Rating">
			<span itemprop="average">' . round($valeur[1]*2, 2) . '</span> /
			<span itemprop="best">10</span> �toiles
		</span>
		(<a href="https://www.avis-verifies.com/avis-clients/generalarmystore.fr" target="_blank" style="text-decoration:none;">
			<span itemprop="votes">' . $valeur[0] . '</span> notes
		</a>)
	</div>';
}
?>

<div id="referencement" style="color: #666"><?php 

if (empty($id_modele)) {
	$id_modele = '';
}

if (isset($baseline) && !empty($baseline)) {
	echo bda_product_name_transform($baseline, $id_modele);
} else if (isset($meta_baseline) && !empty($meta_baseline)) {
	echo bda_product_name_transform($meta_baseline, $id_modele);
}
?>
</div>

</div>

<div align="center"><br><br>
<?php //include('modules/mod_profiling.php'); ?><br />

</div>

<?php if(isset($_GET['etat']) && ($_GET['etat']==1 || $_GET['etat']==99)){ ?>
<!-- Google Code for Conversion BDA Conversion Page -->
<script language="JavaScript" type="text/javascript">
<!--
var google_conversion_id = 1033656165;
var google_conversion_language = "fr";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "n1-bCLGwigEQ5a7x7AM";
//-->
</script>
<script language="JavaScript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<img height="1" width="1" border="0" src="https://www.googleadservices.com/pagead/conversion/1033656165/?label=n1-bCLGwigEQ5a7x7AM&amp;guid=ON&amp;script=0"/>
</noscript>
<?php 
}
else{
?>

<script type="text/javascript">
	tarteaucitron.user.analyticsUa = 'UA-3594953-1';
	tarteaucitron.user.analyticsMore = function () { /* add here your optionnal ga.push() */ };
	(tarteaucitron.job = tarteaucitron.job || []).push('analytics');
</script>

<?php } ?>

</body>
</html>

<?php
tep_db_close();
?>