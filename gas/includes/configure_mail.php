<?php 
/* Fichier de configuration des constantes des mails */
   
// Adresses email
define("MAIL_STOCK", "stock@generalarmystore.fr");  // stock@generalarmystore.fr
define("MAIL_INFO", "info@generalarmystore.fr");  // info@generalarmystore.fr
define("MAIL_PRO", "pro@generalarmystore.fr");  // pro@generalarmystore.fr
define("MAIL_ACHAT", "achat@generalarmystore.fr"); // achat@generalarmystore.fr
define("MAIL_RECRUTEMENT", "recrutement@generalarmystore.fr");  // recrutement@generalarmystore.fr
define("MAIL_COMPTA", "compta@generalarmystore.fr");  // compta@generalarmystore.fr
define("MAIL_WEBMASTER", "webmaster@generalarmystore.fr"); //  ok
define("MAIL_THIERRY", "thierry@pimentbleu.fr"); //  ok

// Explications concernant le Porte-Monnaie Virtuel. Utilis� dans : client_list.php, 
define("EXPLICATIONS_PMV", "Il y a deux possibilit&eacute;s : <br />
							<br />
							<em><u>- Soit votre Porte-Monnaie Virtuel contient une somme <strong>PLUS</strong> &eacute;lev&eacute;e que ne l'est le montant de votre panier :</u><br />
							<br />
							Au moment de r&eacute;gler votre commande, \"Paiement par Porte-Monnaie Virtuel\" vous sera propos&eacute; en plus des autres moyens de paiement habituels. C'est celui que vous devez s&eacute;lectionner.<br />
							Votre commande sera alors enti&egrave;rement pay&eacute;e par votre Porte-Monnaie Virtuel.<br/>
							Cochez la case \"J'ai lu et accept&eacute; les Conditions g&eacute;n&eacute;rales de vente\" puis cliquez sur \"Finaliser la Commande\"<br />
							La diff&eacute;rence entre la somme contenue dans votre Porte-Monnaie Virtuel et le montant de votre commande restera cr&eacute;dit&eacute;e dans votre Porte-Monnaie Virtuel pour une utilisation ult&eacute;rieure.<br/>
							<br />
							<u>- Soit votre Porte-Monnaie Virtuel contient une somme <strong>MOINS</strong> &eacute;lev&eacute;e que ne l'est le montant de votre panier :</u><br />
							<br />
							Au moment de r&eacute;gler votre commande, \"Paiement par Porte-Monnaie Virtuel\" vous sera propos&eacute; en plus des autres moyens de paiement habituels. S&eacute;lectionnez-le.<br />
							Cochez la case \"J'ai lu et accept&eacute; les Conditions g&eacute;n&eacute;rales de vente\" puis cliquez sur \"Finaliser la Commande\"<br />
							Il vous sera ensuite demand� de choisir un autre moyen de paiement pour payer la diff&eacute;rence manquante.</em>");
							
// Signature du mail utilis� en bas de chaque mail envoy�s aux clients
define("SIGNATURE_MAIL", "Merci de r&eacute;pondre &agrave; ce courrier &eacute;lectronique si vous avez des questions.<br>
<br>L'&Eacute;quipe GROUP ARMY STORE / GENERAL ARMY STORE<br><a href=\"". WEBSITE ."\" style=\"color:#DC2213;\">". ADRESSE_SITE ."</a> 
<p style=\"text-align:left\">
	<a href=\"https://www.avis-verifies.com/avis-clients/generalarmystore.fr\" target=\"_blank\" title=\"Avis de nos clients\">
		<img src=\"https://www.generalarmystore.fr/gas/images/etoiles/avisverifies-mail.png\" alt=\"Avis de nos clients\">
	</a>
</p> 
");

// Signature du mail du service client utilis� en bas de chaque mail envoy� par le service client
define("SIGNATURE_MAIL_SERVICE_CLIENT", "Merci pour votre confiance et � tr�s bientot sur le site ". NOM_DU_SITE .".<br /><br /><br />Cordialement,<br />Le service client.");

define('SERVICE_COMMERCIAL', 'Vous pouvez nous joindre par t&eacute;l&eacute;phone <strong>du lundi au vendredi</strong>, de <strong>10h &agrave; 12h30</strong> et de <strong>14h &agrave; 18h30</strong>');

// Sujet des mails d'informations concernant les commandes. Utilis� dans : cmd_list_products_cmd.php, cmd_a_livrer.php, 
define("SUJET_INFORMATION_CMD", "Informations sur votre commande");
// Sujet des mails d'informations concernant le Porte-Monnaie Virtuel. Utilis� dans : client_list.php, 
define("SUJET_INFORMATION_PMV", "Informations : Porte-monnaie");
// Sujet traitement des commandes. Utilis� dans : nouvelle_commande_traitement.php, 
define("SUJET_TRAITEMENT_CMD", "Traitement de la commande");
// Sujet devis disponible. Utilis� dans : nouvelle_commande_traitement.php, 
define("SUJET_DEVIS_DISPO", "Votre devis est disponible");

// Sujet de nouveau mot de passe. Utilis� dans : client_edit.php, 
define("SUJET_NOUVEAU_MDP", "Nouveau mot de passe");

?>