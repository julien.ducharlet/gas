<?php 
/* Récupération des fonctions OSCommerce - Fonctions concernant la base de données */ 
 
	function tep_db_connect($server = DB_SERVER, $username = DB_SERVER_USERNAME, $password = DB_SERVER_PASSWORD, $database = DB_DATABASE, $link = 'db_link') { 
		global $$link; 
		 
		if (USE_PCONNECT == 'true') {
			$$link = mysql_pconnect($server, $username, $password); 
		} else { 
			$$link = mysql_connect($server, $username, $password); 
		} 
		 
		if ($$link) mysql_select_db($database); 
			return $$link; 
	} 
	 
	function tep_db_close($link = 'db_link') { 
		global $$link; 
		return mysql_close($$link); 
	} 
	 
	function tep_db_error($query, $errno, $error) {		 
		 
		$headers ='From: "' . NOM_DU_SITE . '"<' . MAIL_WEBMASTER . '>'."\n"; 
		$headers .='Reply-To: ' . MAIL_WEBMASTER ."\n"; 
		$headers .='Content-Type: text/html; charset="iso-8859-1"'."\n"; 
		 
		$date = 'Le '. date("d-m-Y - H:i:s") .'<br />'; 
		$message = '<font color="#000000"><b>' . $errno . ' - ' . $error . '<br><br>' . $query . '<br><br><small><font color="#ff0000">[TEP STOP]</font></small><br><br></b></font>'; 
		$message_mail = '<a href="http://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] .'">Voir cette erreur en ligne</a><br>'; 
		 
		if(!empty($_SESSION['customer_id'])) { 
			 
			$message_mail .= 'Le client n&deg; '. $_SESSION['customer_id'] .' a eu cette erreur<br />'; 
			$message_mail .= '<a href="'. WEBSITE . BASE_DIR_ADMIN .'/client_edit.php?cID='. $_SESSION['customer_id'] .'&action=edit">Voir ce client dans l\'administration</a><br />'; 
			$message_mail .= '<b>'. print_r($_SESSION) .'</b><br /><br />'; 
		} 
		if(!empty($_SERVER['HTTP_REFERER'])) { 
			$message_mail .= 'Referer : <a href="'. $_SERVER['HTTP_REFERER'] .'">'. $_SERVER['HTTP_REFERER'] .'</a>'; 
		} 
		mail(MAIL_WEBMASTER, 'Erreur sur GAS', $date . $message . $message_mail, $headers); 
		 
    	die(); 
  	} 
 
	function tep_db_query($query, $link = 'db_link') { 
		global $$link, $debug; 
		$query_start = microtime(); 
		 
		if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) { 
		error_log('QUERY ' . $query . "\n", 3, STORE_PAGE_PARSE_TIME_LOG); 
		} 
		 
		$result = mysql_query($query, $$link) or tep_db_error($query, mysql_errno(), mysql_error()); 
		 
		if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) { 
		$result_error = mysql_error(); 
		error_log('RESULT ' . $result . ' ' . $result_error . "\n", 3, STORE_PAGE_PARSE_TIME_LOG); 
		} 
		 
		$_start = explode(' ', $query_start); 
		$_end = explode(' ', microtime()); 
		$_time = number_format(($_end[1] + $_end[0] - ($_start[1] + $_start[0])), 6); 
		 
		$debug['QUERIES'][] = $query; 
		$debug['TIME'][] = $_time; 
 
		return $result; 
	} 
	 
	function tep_db_perform($table, $data, $action = 'insert', $parameters = '', $link = 'db_link') { 
		reset($data); 
		if ($action == 'insert') { 
			$query = 'insert into ' . $table . ' ('; 
			while (list($columns, ) = each($data)) { 
				$query .= $columns . ', '; 
			} 
			$query = substr($query, 0, -2) . ') values ('; 
			reset($data); 
			while (list(, $value) = each($data)) { 
				switch ((string)$value) { 
					case 'now()': 
					$query .= 'now(), '; 
					break; 
					case 'null': 
					$query .= 'null, '; 
					break; 
					default: 
					$query .= '\'' . tep_db_input($value) . '\', '; 
					break; 
				} 
			} 
			$query = substr($query, 0, -2) . ')'; 
		} elseif ($action == 'update') { 
			$query = 'update ' . $table . ' set '; 
			while (list($columns, $value) = each($data)) { 
				switch ((string)$value) { 
					case 'now()': 
					$query .= $columns . ' = now(), '; 
					break; 
					case 'null': 
					$query .= $columns .= ' = null, '; 
					break; 
					default: 
					$query .= $columns . ' = \'' . tep_db_input($value) . '\', '; 
					break; 
				} 
			} 
			$query = substr($query, 0, -2) . ' where ' . $parameters; 
		} 
		 
		return tep_db_query($query, $link); 
	} 
	 
	function tep_db_fetch_array($db_query) { 
		return mysql_fetch_array($db_query, MYSQL_ASSOC); 
	} 
	 
	function tep_db_result($result, $row, $field = '') { 
		return mysql_result($result, $row, $field); 
	} 
	 
	function tep_db_num_rows($db_query) { 
		return mysql_num_rows($db_query); 
	} 
	 
	function tep_db_data_seek($db_query, $row_number) { 
		return mysql_data_seek($db_query, $row_number); 
	} 
	 
	function tep_db_insert_id() { 
		return mysql_insert_id(); 
	} 
	 
	function tep_db_free_result($db_query) { 
		return mysql_free_result($db_query); 
	} 
	 
	function tep_db_fetch_fields($db_query) { 
		return mysql_fetch_field($db_query); 
	} 
	 
	function tep_db_output($string) { 
		return htmlspecialchars($string); 
	} 
	 
	function tep_db_input($string) { 
		return addslashes($string); 
	} 
	 
	function tep_db_prepare_input($string) { 
		if (is_string($string)) { 
			return trim(stripslashes($string)); 
		} elseif (is_array($string)) { 
			reset($string); 
			while (list($key, $value) = each($string)) { 
				$string[$key] = tep_db_prepare_input($value); 
			} 
			return $string; 
		} else { 
			return $string; 
		} 
	} 
 
?>