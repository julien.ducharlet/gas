<?php 
/* Fonctions permettant de prendre en compte les codes promo lors du passage de commande */

function coupon($rayon,$famille,$categorie) {
	$VERIF=false;
	//panier simple
    $panier_query = tep_db_query("	
									SELECT cb.products_id , cb.customers_basket_quantity, cb.id_rayon
									FROM ".TABLE_CUSTOMERS_BASKET." as cb
									WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' 
									AND cb.products_id <> ''
								");
                                
	$test=0;
	while($panier_data = tep_db_fetch_array($panier_query)) {
		$i++;
		$article_options = explode("{", $panier_data['products_id']);
		$options = explode("}", $article_options[1]);
		$option_details = infos_options($options[0], $options[1]);
	   
		$article = explode("_", $article_options[0]);
		$id_article = $article[0];
	   
		$article_query = tep_db_query("
										SELECT 
											p.products_id, 
											p.products_model, 
											p.products_price, 
											pd.products_name, 
											p.products_cost, 
											p.products_quantity, 
											p.family_client_id
										FROM 
											".TABLE_PRODUCTS_DESCRIPTION." AS pd,
											".TABLE_PRODUCTS." AS p
										WHERE 
											pd.products_id = '" . $id_article . "' 
											AND 
											pd.products_id = p.products_id
									");
									
		$article_data = tep_db_fetch_array($article_query);
		$cpath = explode("_", $article_options[0]);
		$cp1=$cpath[1];
	   
		$modele_query = tep_db_query("	SELECT categories_name, categories_id
										FROM categories_description 
										WHERE categories_id='" . $cp1 . "'");
		$modele_data = tep_db_fetch_array($modele_query);
		
		if ($rayon==NULL) { $panier_data['id_rayon']=NULL; }
		if ($famille==NULL) { $article_data['family_client_id']=NULL; }
		if ($categorie==NULL) { $modele_data['categories_id']=NULL; }
		
		if ($panier_data['id_rayon']==$rayon && $article_data['family_client_id']==$famille && $modele_data['categories_id']==$categorie) {
			$test+=1;			
		} else { 
			$test+=0;   
		}
	}
	if ($test>0) {
		$VERIF=true;
	} else { 
		$VERIF=false;
	}
	
	return $VERIF;
}

//fonction qui permet de verifier si le coupon verifie si les restriction du coupon
function englob_coupon($code,$montant){
	$req_coupon=tep_db_query("select * from ".TABLE_COUPONS." where coupon_code='".addslashes($code)."'");
	$res_coupon=tep_db_fetch_array($req_coupon);
	$nb_res=tep_db_num_rows($req_coupon);
	
	if ($nb_res!=0) {	
		if ($res_coupon['restrict_to_rayon']==NULL && $res_coupon['restrict_to_family']==NULL && $res_coupon['restrict_to_categories'] == NULL) {
			return corps_coupon($res_coupon['coupon_id'],$montant);				
		} else {
			if(coupon($res_coupon['restrict_to_rayon'],$res_coupon['restrict_to_family'],$res_coupon['restrict_to_categories'])){
			return corps_coupon($res_coupon['coupon_id'],$montant);
			} else {
				if (isset($_SESSION['coupon_affectation_remise'])  && !empty($_SESSION['coupon_affectation_remise'])) {
					$_SESSION['coupon_affectation_remise']='';
					return 1;
				} else { 
					return 2; 
				}
			}
		}
	} else { 
		return 0; 
	}
}

//fonction principale pour l'affectation du code coupon
function corps_coupon($id,$montant) {
	$today = date("Y-m-d H:i:s");
	$req_coupon=tep_db_query("select * from ".TABLE_COUPONS." where coupon_id=".$id."");
	$res_coupon=tep_db_fetch_array($req_coupon);
	
	$req_coupon2=tep_db_query("select count(unique_id) as cpt 
							   from ".TABLE_COUPON_REDEEM_TRACK." 
							   where coupon_id=".$id." ");
	$res_coupon2=tep_db_fetch_array($req_coupon2);
	
	$req_coupon3=tep_db_query("select count(unique_id) as cpt
							   from ".TABLE_COUPON_REDEEM_TRACK." 
							   where coupon_id=".$id." and customer_id=".$_SESSION['customer_id']."");
	$res_coupon3=tep_db_fetch_array($req_coupon3);
	
	if($res_coupon['coupon_active']=='Y') {
		
		if(($res_coupon['uses_per_coupon'] ==0 && $res_coupon['uses_per_user']==0) || ($res_coupon['uses_per_coupon'] >= $res_coupon2['cpt'] && $res_coupon['uses_per_user'] >= $res_coupon3['cpt']) ){
			
			if ($res_coupon['coupon_start_date']<$today && $res_coupon['coupon_expire_date']>$today) {
				if($res_coupon['coupon_minimum_order']<=$montant) {	
					
					if ($res_coupon['coupon_type']=='S' && ($_SESSION['customer_country_id']==1 || $_SESSION['customer_country_id']==292 || $_SESSION['customer_country_id']==350)) {
						$_SESSION['coupon_id']=$id;
						$_SESSION['code_promo']=$res_coupon['coupon_code'];
						$_SESSION['coupon_frais_de_port']=1;
						$_SESSION['coupon_affectation_remise']='oui';
						return 98;
					} else {
						return 97;
					}
					if ($res_coupon['coupon_type']=='P') {
						$_SESSION['coupon_id']=$id;
						$_SESSION['code_promo']=$res_coupon['coupon_code'];
						$_SESSION['type_reduc']=$res_coupon['coupon_type'];
						$_SESSION['montant_reduc']=$res_coupon['coupon_amount'];
						$_SESSION['coupon_affectation_remise']='oui';
						$_SESSION['coupon_frais_de_port']='';
						return 99;
					}
					
				} else { 
					  $_SESSION['coupon_minimum_order']=$res_coupon['coupon_minimum_order'];
					  $_SESSION['coupon_affectation_remise']='';
					  return 3;
				}
				
			} else {
				 $_SESSION['coupon_date_deb']=$res_coupon['coupon_start_date'];
				 $_SESSION['coupon_date_fin']=$res_coupon['coupon_expire_date'];
				 $_SESSION['coupon_affectation_remise']='';
				 return 4;
			}
		} else {
			 $_SESSION['coupon_affectation_remise']='';
			 return 5;
		}
	} else { 
		$_SESSION['coupon_affectation_remise']='';
		return 6;
	}		
}

// Affiche un message au client suivant l'etat que le coupon retourne
function message_coupon($mess){
	
	switch ($mess) {
	case 0:
		echo "Ce code promotionnel n'existe malheureusement pas !";
		break;
	case 1:
		echo "Annulation du code promo";
		break;
	case 2:
		echo "Pas de produits associ&eacute;s &agrave; ce code promo";
		break;
	case 3:
		echo "Pour pouvoir utiliser ce code promotionnel, il vous faut avoir
			  une commande d'un minimum de ".format_to_money($_SESSION['coupon_minimum_order'])."
			  &euro; <br />( montant total des articles hors taxes )";
		break;
	case 4:
		echo "Ce code promotionnel n'est valide qu'entre le ".dat($_SESSION['coupon_date_deb'])."
			 et le ".dat($_SESSION['coupon_date_fin']);
		break;
	case 5:
		echo "Ce coupon a d&eacute;j&agrave; atteint son quota d'utilisation";
		break;
	case 6:
		echo "D&eacute;sol&eacute; ce coupon n'est pas actif !";
		break;
	case 97:
		echo "Les frais de port offert ne s'applique que pour une livraison en France, Monaco et Andorre";
		break;
	case 98:
		echo "Les frais de port vous sont offert gr&acirc;ce au code coupon : ".$_SESSION['code_promo'];
		break;
	case 99:
		echo "Remise effectu&eacute;e gr&acirc;ce au code coupon : ".$_SESSION['code_promo']." <br />Merci de s&eacute;lectionner une m&eacute;thode de paiement ci dessous pour payer votre commande";
		break;
	}
}
?>