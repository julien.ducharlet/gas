<?php
/* Fichier regroupant toutes les fonctions concernant les mails */


function email($mail, $subject, $content, $newsletter_id = 0) {
	
	$headers = "MIME-Version: 1.0\n";
		
	$headers .= "X-Sender: <". ADRESSE_SITE .">\n";
	$headers .= "X-Mailer: PHP\n";
	$headers .= "X-auth-smtp-user: ". MAIL_INFO ." \n";
	$headers .= "X-abuse-contact: ". MAIL_INFO ." \n";
	$headers .= 'From: "' . NOM_DU_SITE . '"<' . MAIL_INFO . '>'."\n";
	$headers .= 'Reply-To: ' . MAIL_INFO ."\n";
	$headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
	
	$message = '<body>';
		
		// pour afficher la news sur le site
		$message .= '<div style="font-size:10px;border-bottom:dotted 1px;margin-bottom:10px;margin-left:auto;margin-right:auto;padding-bottom:5px;text-align:center;width:800px;">Si vous ne parvenez pas � visualiser le contenu de ce mail, <a href="'. WEBSITE . BASE_DIR .'/newsletter.php?news='. md5($newsletter_id) .'">cliquez ici</a></div>';

	
		$message .= '<table style="margin-left:auto;margin-right:auto;width:800px;border:0;padding:0;border-collapse:collapse;" cellpadding="0">
						<tbody>
							<tr>
								<td style="height:121px;"><a href="' . WEBSITE . BASE_DIR .'/index.php?news='. md5($newsletter_id) .'"><img src="' . WEBSITE . BASE_DIR . '/template/base/mail/header800.jpg" alt="Activer l\'affichage des images de votre service email." style="width:800px; border:0; height:121px"></a></td>
							</tr>
							<tr>
								<td style="background-color:#4F4F4F;">
									<div style="color:white;min-height:200px;padding-right: 20px;padding-top: 17px;padding-left: 17px; padding-bottom:17px;">';
	$message .= $content;
	$message .= '					</div></td>
							</tr>
							<tr>
								<td style="background-color:#4F4F4F; text-align:center;">
									<a href="https://www.avis-verifies.com/avis-clients/generalarmystore.fr" target="_blank" title="Avis de nos clients">
										<img src="https://www.generalarmystore.fr/gas/images/etoiles/avisverifies-mail.png" alt="Avis de nos clients">
									</a>
								</td>
							</tr>
							<tr>
								<td style="height:29px">
									<img src="' . WEBSITE . BASE_DIR . '/template/base/mail/footer-vert.jpg" style="width:800px; height:29px;" alt="Pensez &agrave; activer l\'affichage des images de votre service mail.">
								</td>
							</tr>
						</tbody>
					</table>';
		$message .= '<div style="font-size:10px;border-top:dotted 1px;margin-top:10px;margin-left:auto;margin-right:auto;padding-top:5px;text-align:justify;width:800px;">Le pr�sent message est en parfait respect avec la d�ontologie et les bonnes pratiques de la communication par marketing direct �lectronique.
Conform�ment � la l�gislation en vigueur et des diff�rents rectificatifs l�gaux, vous disposez d\'un plein droit d\'acc�s, de modifications ou de suppression des donn�es personnelles vous concernant.<br><br>
 Vous pouvez a tout moment exercer ce droit sur demande �crite ou via notre espace pr�vu a cet effet.
Conform�ment a la loi Informatique et libert�s, vous pouvez vous d�sabonner � tout moment en <a href="'. WEBSITE . BASE_DIR .'/compte/account.php">cliquant ici</a></div>';

	$message .= '</body>';
	
	mail($mail, $subject, $message, $headers);
}


function parseMail($news_id, $news_content) {
	
	preg_match_all('/<a href=\"([^\"]*)\">(.*)<\/a>/iU', html_entity_decode(stripslashes($news_content)), $results);
	
	foreach($results[1] as $link) {
		
		$linkToReplace = $link;
		
		$linkToReplace .= (preg_match("/\?/i", $link)) ? "&" : "?";
		$linkToReplace .= "newsletter=". md5($news_id);
		
		$linkToSearch = str_replace('?', '\?', str_replace('/', '\/', $link));
		
		$news_content = preg_replace("/". $linkToSearch ."/", $linkToReplace, html_entity_decode(stripslashes($news_content)));
	}
	
	$news_content = html_entity_decode(stripslashes($news_content));
	
	return $news_content;
}

// Envoie un mail � un client lors de la demande de mot de passe
function mail_client($mail, $sujet, $mess) {
	$to = $mail; // Destinataires
	$subject = $sujet; // Sujet
	
	// Destinataire de l'email
	$headers ='From: "' . NOM_DU_SITE . '"<' . MAIL_INFO . '>'."\n";
	$headers .='Reply-To: ' . MAIL_INFO ."\n";
	$headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
	
	//Corps du Message au format HTML
	$message = '<body>
					<table style="margin:auto; width:800px; border:0;" cellpadding="0" cellspacing="0">
						<tbody>
							<tr>
								<td style="width:800px; height:124px;">
									<a href="' . WEBSITE . '"><img src="' . WEBSITE . BASE_DIR . '/template/base/mail/header800.jpg" alt="Activer l\'affichage des images de votre service email." style="width:800px; border:0; height:124px"></a>
								</td>
							</tr>
							<tr bgcolor="#d8d2ba">
								<td style="color:black; padding-right: 20px;padding-top: 17px;padding-left: 17px; padding-bottom:17px;" bgcolor="#d8d2ba">
				';
	$message.=$mess;
	$message.='					</td>
								
							</tr>
							<tr>
								<td style="width:800px; height:29px;">
									<img src="' . WEBSITE . BASE_DIR . '/template/base/mail/footer-vert.jpg" style="width:800px; height:29px;" alt="Pensez &agrave; activer l\'affichage des images de votre service mail.">
								</td>
							</tr>
						</tbody>
					</table>
				</body>';     
     // Fonction php mermettant l'envoi de l'email
     mail($to, $subject, $message, $headers);
}

//Envoie un mail au client suite � sa commande
function mail_client_commande($mail, $sujet, $mess, $bcc=''){
	$to = $mail; // Destinataires
	$subject = $sujet; // Sujet
	
	// Destinataire de l'email
	$headers = 'From: "' . NOM_DU_SITE . '"<' . MAIL_INFO . '>'."\n";
	if(!empty($bcc)) $headers .= 'Bcc:'. $bcc ."\n";
	$headers .= 'Reply-To: ' . MAIL_INFO ."\n";
	$headers .= 'Content-Type: text/html; charset="iso-8859-1"'."\n";
	
	//Corps du Message au format HTML
	$message = '<body>
					<table style="margin:auto; width:800px; border:0;" cellpadding="0" cellspacing="0">
						<tbody>
							<tr>
								<td colspan="2"style="width:800px; height:124px;">
									<a href="' . WEBSITE . '"><img src="' . WEBSITE . BASE_DIR . '/template/base/mail/header800.jpg" alt="Activer l\'affichage des images de votre service email." style="width:800px; border:0; height:124px"></a>
								</td>
							</tr>
							<tr bgcolor="#d8d2ba">
								<td style="color:black; padding-right: 20px;padding-top: 17px;padding-left: 17px; padding-bottom:17px;" bgcolor="#d8d2ba">
				';
	$message.=$mess;
	$message.='					</td>
							</tr>
							<tr>
								<td colspan="2" style="width:800px; height:29px;">
									<img src="' . WEBSITE . BASE_DIR . '/template/base/mail/footer-vert.jpg" style="width:800px; height:29px;" alt="Pensez &agrave; activer l\'affichage des images de votre service mail.">
								</td>
							</tr>
						</tbody>
					</table>
				</body>';
     
     // Fonction php mermettant l'envoi de l'email
     return mail($to, $subject, $message, $headers);
}

// Envoie un mail � l'admin lors de la saisie d'un formulaire de contact
function mail_contact($destinataire, $email_client, $num_commande, $telephone, $sujet, $message) {
		if ($destinataire == 'particuliers') {
			$destinataire = MAIL_INFO;
		} else if ($destinataire == 'professionnels') {
			$destinataire = MAIL_PRO;
		} else if ($destinataire == 'achats') {
			$destinataire = MAIL_ACHAT;
		}
		
		$contenu = html_entity_decode('T&eacute;l&eacute;phone : ' . $telephone . "\n\n" . 'Num&eacute;ro de commande : ' . $num_commande . "\n\n" . $message);
		$expediteur = htmlspecialchars("From: " . $email_client . "\r\n");
		mail($destinataire,  stripslashes($sujet),  stripslashes($contenu), $expediteur);
}


// Envoie un mail � l'admin pour avertir de l'etat des stocks
function mail_stock_alert($id_produit,$produit_name,$produit_model) {
	$to  = MAIL_STOCK; // Destinataires
	//$to  = 'thierry@pimentbleu.fr'; // Destinataires
	$subject = "Alerte stock : " . $produit_model .""; // Sujet
	
	// Destinataire de l'email
    $headers ='From: "' . NOM_DU_SITE . '"<' . MAIL_STOCK . '>'."\n";
    $headers .='Reply-To: ' . MAIL_STOCK ."\n";
    $headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
	
	//Corps du Message au format HTML
	$message ="Attention !!! l'article : " . $produit_name;
	if (!empty($produit_model)) {
		$message .=" (" . $produit_model . ") ";
	}
	$message .=" arrive sous la limite du stock minimum<br>"."\n\n";	
	$message .=" <a href='" . WEBSITE . BASE_DIR_ADMIN . "/article_edit_stocks_options.php?pID=".$id_produit."&action=new_product'>" . WEBSITE . BASE_DIR_ADMIN . "/article_edit_stocks_options.php?pID=".$id_produit."&action=new_product</a>";
     
    // Fonction php mermettant l'envoi de l'email
    mail($to, $subject, $message, $headers);	
}

// Envoie un mail � l'admin pour avertir de l'etat spplus
function mail_spplus($mess) {
	$to  = MAIL_COMPTA; // Destinataires
	$subject = "PROBLEME SPPLUS"; // Sujet
	//message recuper� dans le parametre de la fonction (message se trouvant dans la page z24k_s04_r86.php)
	$message=$mess;
	// Destinataire de l'email
    $headers ='From: "' . NOM_DU_SITE . '"<' . MAIL_INFO . '>'."\n";
    $headers .='Reply-To: ' . MAIL_INFO ."\n";
    $headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
     
    // Envoi
    mail($to, $subject, $message, $headers);
}

//Envoie de mail au gerant du site pour prevenir de la commande 
function mail_cmd_admin($sujet,$mess) {
	$to  = MAIL_COMPTA; // Destinataires
	$subject = $sujet." - " . NOM_DU_SITE; // Sujet
	
	$message=$mess;
	
	// Destinataire de l'email
    $headers ='From: "' . NOM_DU_SITE . '"<' . MAIL_WEBMASTER . '>'."\n";
    $headers .='Reply-To: ' . MAIL_WEBMASTER . "\n";
    $headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
     
    // Envoi
    mail($to, $subject, $message, $headers);
}

//Envoie de mail au gerant du site pour prevenir de la commande 
function mail_cmd_thierry($sujet,$mess) {
	$to  = MAIL_THIERRY; // Destinataires
	$subject = $sujet." - " . NOM_DU_SITE; // Sujet
	
	$message=$mess;
	
	// Destinataire de l'email
    $headers ='From: "' . NOM_DU_SITE . '"<' . MAIL_WEBMASTER . '>'."\n";
    $headers .='Reply-To: ' . MAIL_WEBMASTER . "\n";
    $headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
     
    // Envoi
    mail($to, $subject, $message, $headers);
}

//######################################################################################
//############# Fonctions de gestion du contenu des mails envoy�s ######################
//######################################################################################

// R�cup�ration des infos pour l'envoi du mail a l'admin suite a une commande admin
function mail_contenu_commande_admin($num_commande){
	
		$req_order=tep_db_query("select * from orders where orders_id=".$num_commande."");
		$res_order=tep_db_fetch_array($req_order);
		
		$today = date("Y-m-d h:i:s");
		$timestamp_cmd = strtotime($res_order['date_purchased']);
		
		$ret=" Commande faite sur : " . NOM_DU_SITE . "<br><br>";
		$ret.="Num&eacute;ro de commande : ".$num_commande."<br>";
		$ret.="Date de commande : ".date_fr($timestamp_cmd)." <br><br>";
		$ret.="Total commande : ".$res_order['total']."<br><br>";
		$ret.="Nom et pr&eacute;nom du client : ".$res_order['customers_name']."<br/>";
		$ret.="Email : ".$res_order['customers_email_address']."<br><br>";
		$ret.="M&eacute;thode de paiement : ".$res_order['payment_method'];
		
		return $ret;
}


// R�cup�ration des infos pour l'envoi du mail a l'admin suite a un devis
function mail_contenu_devis_admin($num_commande){
	
		$req_order=tep_db_query("select * from orders where orders_id=".$num_commande."");
		$res_order=tep_db_fetch_array($req_order);
		
		$timestamp_cmd = strtotime($res_order['orders_date_fin_devis']);
		$timestamp_cmd2 = strtotime($res_order['date_purchased']);
		
		$ret="La cr&eacute;ation du Devis ".$num_commande." a bien &eacute;t&eacute; &eacute;ffectu&eacute;<br><br>";
		$ret.="Num&eacute;ro du devis : ".$num_commande."<br>";
		$ret.="Date cr&eacute;ation du devis : ".date_fr($timestamp_cmd2)." <br><br>";
		$ret.="Date expiration du devis : ".date_fr($timestamp_cmd)."<br><br>";
		$ret.="Nom et pr&eacute;nom du client : ".$res_order['customers_name']."<br/>";
		$ret.="Email : ".$res_order['customers_email_address']."<br><br>";
		
		return $ret;
}


// R�cup�ration des infos pour l'envoi du mail au client suite a une commande
function mail_contenu_commande($num_commande){
	
	$req_order=tep_db_query("select * from orders where orders_id=".$num_commande."");
	$res_order=tep_db_fetch_array($req_order);
		
	/* r�cup�ration des articles de la commande */
	$articles_query = tep_db_query("	SELECT orders_products_id, products_model, cpath, products_name, final_price, products_quantity 
										FROM " . TABLE_ORDERS_PRODUCTS . " 
										WHERE orders_id=".$num_commande."");	
	
	$today = date("Y-m-d h:i:s");
	$timestamp_facture = strtotime($today);
	
	$ret='	<div style="text-align:right">'.date_fr($timestamp_facture).'</div>
			Bonjour <span style="font-weight:bold; color:#AAB41D;">'.$res_order['customers_name'].'</span>,<br>
			<br>
			Vous venez d\'effectuer une commande sur ' . NOM_DU_SITE . '.<br>
			Son num�ro est le <span style="font-weight:bold;color:#FE5700;">'.$num_commande.'</span><br>
			<br>
			<hr style="color:#AAB41D;"/>
			<br>
			Celle-ci comprend :<br>
			<br>';
			
			$req_id_pays=tep_db_query("select * from countries where countries_name='".$res_order['delivery_country']."'");
			$res_country_id=tep_db_fetch_array($req_id_pays);
			
			while ($articles_data = tep_db_fetch_array($articles_query)) {
				$articles_data['cpath'];
				
				$ret.= $articles_data['products_quantity']." x ";
				$ret.= $articles_data['products_name'];
				//$ret.= " ( ".$articles_data['products_model']." ) ";
				$ret.= ' = '.format_to_money(($articles_data['final_price']*$articles_data['products_quantity'])*parametrage_affiche_prix($res_country_id['countries_id'])).' &euro;';
				
				$req_option=tep_db_query("select products_options, products_options_values
										  from orders_products_attributes
										  where orders_products_id=".$articles_data['orders_products_id']."");
				$nb_option=tep_db_num_rows($req_option);
				if($nb_option==1){ 
				  $res_option=tep_db_fetch_array($req_option);
				  $ret.= '<br>'.$res_option['products_options'].' : '.$res_option['products_options_values'].' ';
				}
				$ret.= '<br>';
			}
	
			$remise = $res_order['remise']+$res_order['remise_porte_monnaie'];
	
	$ret.='	<br>
			Sous-Total HT : '.$sous_total = $res_order['ss_total'].' &euro;<br>
			Frais de livraison HT : '.$fdp = $res_order['frais_port_client'].' &euro;<br>';
	
			if (parametrage_affiche_prix($res_country_id['countries_id']) == 1.2) {
			$ret.='TVA Fran&ccedil;aise 20% : '.$tva = $res_order['tva_total'].' &euro; <br>';
			}
			if ($remise != 0) { $ret.= 'Remise : '.$remise.' &euro;<br>'; }
	
	$ret.='	<span style="font-weight:bold;">Total &agrave; payer : <span style="color:#4E87B4;">'.$total = $res_order['total'].' &euro;</span></span><br>
			<br>
			<hr style="color:#AAB41D;"/>
			<br>
			Vous serez livr&eacute; &agrave; l\'adresse suivante :<br>
			<br>';
		
		if ($res_order['delivery_company'] != '') 
			$ret .= $res_order['delivery_company'].'<br>'; // Societe
		if ($res_order['delivery_name'] != '') 
			$ret .= $res_order['delivery_name'].'<br>'; // Nom + Pr�nom
		if ($res_order['delivery_street_address_3'] != '') 
			$ret .= $res_order['delivery_street_address_3'].'<br>'; //Adresse 1
		if ($res_order['delivery_suburb'] != '') 
			$ret .= $res_order['delivery_suburb'].'<br>'; //Adresse 2
		if ($res_order['delivery_street_address'] != '') 
			$ret .= $res_order['delivery_street_address'].'<br>'; //Adresse 3
		if ($res_order['delivery_street_address_4'] != '') 
			$ret .= $res_order['delivery_street_address_4'].'<br>'; //Adresse 4	
		
		$ret.= $res_order['delivery_postcode'].'&nbsp;&nbsp;'.$res_order['delivery_city'].'<br>';
		$ret.= $res_order['delivery_country'];		
		
	$ret.='	<br>
			<br>
			Contactez-nous rapidement si l\'adresse comporte des erreurs.<br>
			<br>
			<hr style="color:#AAB41D;"/>
			<br>';
		
	$ret.='	Informations de paiement : <br>
			<br>';
		  
	$moyen_p=tep_db_query("select * from module_paiement where paiement_nom='".$res_order['payment_method']."'");
	$res_paiement=tep_db_fetch_array($moyen_p);	  
		
		$ret.= "Votre m&eacute;thode de paiement : <span style='font-weight:bold;color:#4E87B4;'> Paiement par ".$res_paiement['paiement_nom']."</span><br>
		<br>";
	
	if ($res_paiement['paiement_id'] != '1') {
		$ret.= $res_paiement['paiement_details'];	
	}
	
	$ret.='	<br>
			<br>
			<hr style="color:#AAB41D;"/>
			<br>
			Vous avez la possibilit&eacute; de consulter votre historique de commandes &agrave; tout moment en 
			<a href="' . WEBSITE . BASE_DIR . '/compte/order.php" style="color:#FE5700; font-weight:bold; ">cliquant ici</a><br>
			<br>
			<hr style="color:#AAB41D;"/>			
			<br>' . SIGNATURE_MAIL . '';
		
	return $ret;
}

#################
#################
#################

// R�cup�ration des infos pour l'envoi du mail au client suite a une demande de devis
function mail_contenu_devis_front($num_commande){
	
	$req_order=tep_db_query("select * from orders where orders_id=".$num_commande."");
	$res_order=tep_db_fetch_array($req_order);
		
	/* r�cup�ration des articles de la commande */
	$articles_query = tep_db_query("	SELECT 
											orders_products_id, 
											products_model, 
											cpath, 
											products_name, 
											final_price, 
											products_quantity 
										FROM " . TABLE_ORDERS_PRODUCTS . " 
										WHERE orders_id=".$num_commande."");	
	
	// Date du jour
	$today = date("Y-m-d h:i:s");
	$timestamp_facture = strtotime($today);
	// Date de fin de validit� du devis (date du jour + 15 jours)
	$date_fin_devis = date_fr(strtotime(date("Y-m-d H:i:s", mktime(0, 0, 0, date("m")  , date("d")+15, date("Y")))));
	
	$ret= '<div style="text-align:right">'.date_fr($timestamp_facture).'</div>
		  Bonjour <span style="font-weight:bold;color:#AAB41D;">'.$res_order['customers_name'].'</span>,<br>
		  <br>
		  Vous venez de cr�er votre devis chez Group Army Store
		  <br>
		  Son num�ro est le <span style="font-weight:bold;color:#FE5700;">'.$num_commande.'</span><br>
		  Il est valable jusqu\'au <span style="font-weight:bold;">' . $date_fin_devis . '</span> inclus.
		  <br>
		  <br>
		  <hr style="color:#AAB41D;"/>
		  <br>
		  En voici le d�tail :<br>
		  <br>';
		  
		  $req_id_pays=tep_db_query("select * from countries where countries_name='".$res_order['delivery_country']."'");
		  $res_country_id=tep_db_fetch_array($req_id_pays);
			
		  while ($articles_data = tep_db_fetch_array($articles_query)) {
				$articles_data['cpath'];
				
				$ret.= $articles_data['products_quantity'].' x '. $articles_data['products_name'];
				$ret.= ' = '.format_to_money(($articles_data['final_price']*$articles_data['products_quantity'])*parametrage_affiche_prix($res_country_id['countries_id'])).' &euro;';
				
				$req_option=tep_db_query("select products_options, products_options_values
										  from orders_products_attributes
										  where orders_products_id=".$articles_data['orders_products_id']."");
				$nb_option=tep_db_num_rows($req_option);
				
				if($nb_option==1){ 
					$res_option=tep_db_fetch_array($req_option);
					$ret.= '<br>'.$res_option['products_options'].' : '.$res_option['products_options_values'].' ';
				}
				$ret.= '<br>';
			}
	
	$remise = $res_order['remise']+$res_order['remise_porte_monnaie'];
	
	$ret.='
		  <br>
		  Sous-Total HT : '.$sous_total = $res_order['ss_total'].' &euro; 
		  <br>
		  Frais de livraison HT : '.$fdp = $res_order['frais_port_client'].' &euro; 
		  <br>';
	
	if (parametrage_affiche_prix($res_country_id['countries_id']) == 1.2) {
		$ret.='TVA Fran&ccedil;aise 20% : '.$tva = $res_order['tva_total'].' &euro; <br>';
	}
	if ($remise!=0) { $ret.= 'Remise : '.$remise.' &euro;<br>'; }
	
	$ret.='<span style="font-weight:bold;">Total &agrave; payer : <span style="color:#4E87B4;">'.$total = $res_order['total'].' &euro;</span></span>
			<br>
			<br>
			<hr style="color:#AAB41D;"/>
			<br>
			Adresse de livraison souhait�e :<br>
			<br>';
		
		if ($res_order['delivery_company'] != '') 
			$ret .= $res_order['delivery_company'].'<br>'; // Societe
		if ($res_order['delivery_name'] != '') 
			$ret .= $res_order['delivery_name'].'<br>'; // Nom + Pr�nom
		if ($res_order['delivery_street_address_3'] != '') 
			$ret .= $res_order['delivery_street_address_3'].'<br>'; //Adresse 1
		if ($res_order['delivery_suburb'] != '') 
			$ret .= $res_order['delivery_suburb'].'<br>'; //Adresse 2
		if ($res_order['delivery_street_address'] != '') 
			$ret .= $res_order['delivery_street_address'].'<br>'; //Adresse 3
		if ($res_order['delivery_street_address_4'] != '') 
			$ret .= $res_order['delivery_street_address_4'].'<br>'; //Adresse 4	
		
		$ret.= $res_order['delivery_postcode'].'&nbsp;&nbsp;'.$res_order['delivery_city'].'<br>';
		$ret.= $res_order['delivery_country'];		
		
	$ret.= '<br>
			<br>
			Contactez-nous rapidement si l\'adresse comporte des erreurs.<br>
			<br>
			<hr style="color:#AAB41D;"/>
			<br>
			Vous pouvez visualiser et/ou t�l�charger un exemplaire de ce devis en cliquant sur ce lien : 
			<a href="' . WEBSITE . BASE_DIR . '/compte/generation_pdf/facture.php?id_com='.$num_commande.'&cli='.md5($res_order['customers_id']).'" style="font-weight:bold; color:#FE5700;">Devis n� ' . $num_commande . '</a><br>
			<br>
			Pour le valider, vous devrez soit :<br>
			- nous le retourner dat�, tamponn� et sign� par e-mail, fax ou courrier <span style="font-weight:bold; text-decoration:underline;">ET</span> le valider sur le site<br>
			- nous envoyer par e-mail, fax ou courrier un bon de commande administratif<br>
			<br>
			Vous pouvez �galement d\'ores et d�j� payer ce devis directement sur notre site gr�ce aux moyens de paiement qui y sont propos�s : CB, Ch�que, Virement, Paypal.<br>
			<br>
			<hr style="color:#AAB41D;"/>			
			<br>' . SIGNATURE_MAIL .'';
		
	return $ret;
}

#################
#################
#################

#################
#################
#################

// Contenu mail devis client 
function mail_contenu_devis($num_commande){
	
	//Retourne la date au format Jeudi 22 Mai 2008 
	function devis_date_fr($timestamp_date){
		$mois=array('Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre');
		$jour=array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
		
		$mois_fr = $mois[date('n', $timestamp_date)-1];
		$jour_fr = $jour[date('w', $timestamp_date)];
		
		$date_fr = $jour_fr . " " . date('d', $timestamp_date) . " " . $mois_fr . " " . date('Y', $timestamp_date);
		return $date_fr;
	}
	
	$req_order=tep_db_query("select * from orders where orders_id=".$num_commande."");
	$res_order=tep_db_fetch_array($req_order);
		
	/* r�cup�ration des articles de la commande */
	$articles_query = tep_db_query("	SELECT 
											orders_products_id, 
											products_model, 
											cpath, 
											products_name, 
											final_price, 
											products_quantity 
										FROM " . TABLE_ORDERS_PRODUCTS . " 
										WHERE orders_id=".$num_commande."");	
	
	// Date du jour
	$today = date("Y-m-d h:i:s");
	$timestamp_facture = strtotime($today);
	// Date de fin de validit� du devis (saisie dans le back office)
	$date_fin_devis = devis_date_fr(strtotime($res_order['orders_date_fin_devis']));
	
	$ret= '<div style="text-align:right">'.devis_date_fr($timestamp_facture).'</div>
		  Bonjour <span style="font-weight:bold;color:#AAB41D;">'.$res_order['customers_name'].'</span>,<br>
		  <br>
		  Vous venez de cr�er votre devis chez Group Army Store
		  <br>
		  Son num�ro est le <span style="font-weight:bold;color:#FE5700;">'.$num_commande.'</span><br>
		  Il est valable jusqu\'au <span style="font-weight:bold;">' . $date_fin_devis . '</span> inclus.
		  <br>
		  <br>
		  <hr style="color:#AAB41D;"/>
		  <br>
		  En voici le d�tail :<br>
		  <br>';
		  
		  $req_id_pays=tep_db_query("select * from countries where countries_name='".$res_order['delivery_country']."'");
		  $res_country_id=tep_db_fetch_array($req_id_pays);
			
		  while ($articles_data = tep_db_fetch_array($articles_query)) {
				$articles_data['cpath'];
				
				$ret.= $articles_data['products_quantity'].' x '. $articles_data['products_name'];
				$ret.= ' = '.format_to_money(($articles_data['final_price']*$articles_data['products_quantity'])*parametrage_affiche_prix($res_country_id['countries_id'])).' &euro;';
				
				$req_option=tep_db_query("select products_options, products_options_values
										  from orders_products_attributes
										  where orders_products_id=".$articles_data['orders_products_id']."");
				$nb_option=tep_db_num_rows($req_option);
				
				if($nb_option==1){ 
					$res_option=tep_db_fetch_array($req_option);
					$ret.= '<br>'.$res_option['products_options'].' : '.$res_option['products_options_values'].' ';
				}
				$ret.= '<br>';
			}
	
	$remise = $res_order['remise']+$res_order['remise_porte_monnaie'];
	
	$ret.='
		  <br>
		  Sous-Total HT : '.$sous_total = $res_order['ss_total'].' &euro; 
		  <br>
		  Frais de livraison HT : '.$fdp = $res_order['frais_port_client'].' &euro; 
		  <br>';
	
	if (parametrage_affiche_prix($res_country_id['countries_id']) == 1.2) {
		$ret.='TVA Fran&ccedil;aise 20% : '.$tva = $res_order['tva_total'].' &euro; <br>';
	}
	if ($remise!=0) { $ret.= 'Remise : '.$remise.' &euro;<br>'; }
	
	$ret.='<span style="font-weight:bold;">Total &agrave; payer : <span style="color:#4E87B4;">'.$total = $res_order['total'].' &euro;</span></span>
			<br>
			<br>
			<hr style="color:#AAB41D;"/>
			<br>
			Adresse de livraison souhait�e :<br>
			<br>';
		
		if ($res_order['delivery_company'] != '') 
			$ret .= $res_order['delivery_company'].'<br>'; // Societe
		if ($res_order['delivery_name'] != '') 
			$ret .= $res_order['delivery_name'].'<br>'; // Nom + Pr�nom
		if ($res_order['delivery_street_address_3'] != '') 
			$ret .= $res_order['delivery_street_address_3'].'<br>'; //Adresse 1
		if ($res_order['delivery_suburb'] != '') 
			$ret .= $res_order['delivery_suburb'].'<br>'; //Adresse 2
		if ($res_order['delivery_street_address'] != '') 
			$ret .= $res_order['delivery_street_address'].'<br>'; //Adresse 3
		if ($res_order['delivery_street_address_4'] != '') 
			$ret .= $res_order['delivery_street_address_4'].'<br>'; //Adresse 4	
		
		$ret.= $res_order['delivery_postcode'].'&nbsp;&nbsp;'.$res_order['delivery_city'].'<br>';
		$ret.= $res_order['delivery_country'];		
		
	$ret.= '<br>
			<br>
			Contactez-nous rapidement si l\'adresse comporte des erreurs.<br>
			<br>
			<hr style="color:#AAB41D;"/>
			<br>
			Vous pouvez visualiser et/ou t�l�charger un exemplaire de ce devis en cliquant sur ce lien : 
			<a href="' . WEBSITE . BASE_DIR . '/compte/generation_pdf/facture.php?id_com='.$num_commande.'&cli='.md5($res_order['customers_id']).'" style="font-weight:bold; color:#FE5700;">Devis n� ' . $num_commande . '</a><br>
			<br>
			Pour le valider, vous devrez soit :<br>
			- nous le retourner dat�, tamponn� et sign� par e-mail, fax ou courrier <span style="font-weight:bold; text-decoration:underline;">ET</span> le valider sur le site<br>
			- nous envoyer par e-mail, fax ou courrier un bon de commande administratif<br>
			<br>
			Vous pouvez �galement d\'ores et d�j� payer ce devis directement sur notre site gr�ce aux moyens de paiement qui y sont propos�s : CB, Ch�que, Virement, Paypal.<br>
			<br>
			<hr style="color:#AAB41D;"/>			
			<br>' . SIGNATURE_MAIL .'';	
		
	return $ret;
}

#################
#################
#################

function envoi_mail_lost_basket($cid) {
	
	unset($email);
	
	$index = 0;
	$mline = '';
	$previous_is_pack = false;
	
	$query1 = tep_db_query("SELECT cb.products_id pid, cb.pack_id, cb.customers_basket_quantity qty, cb.customers_basket_date_added bdate, cus.customers_firstname fname,
								   cus.customers_lastname lname, cus.customers_email_address email
							FROM      " . TABLE_CUSTOMERS_BASKET . " cb," . TABLE_CUSTOMERS . " cus
							WHERE     cb.customers_id = cus.customers_id  and cus.customers_id = '".$cid."'
							ORDER BY  cb.customers_basket_date_added desc, cb.products_id desc");
	
	while($inrec = tep_db_fetch_array($query1)) {
		
		$infos_products = array();
		
		//pour les produits
		if(!empty($inrec['pid'])) {
			
			$infos_products[0]['id'] = $inrec['pid'];
			$from_pack = false;
			$previous_is_pack = false;
		
		} else {
			
			// Ajout thierry le 19/09/2018
			$from_pack = '';
			
			$pack_id = explode('_', $inrec['pack_id']);
			
			$query_pack = 'select products_id, products_id_1, products_id_2 from '. TABLE_PACK .' where pack_id='. $pack_id[0];
			$query_pack = tep_db_query($query_pack);
			
			$data_pack = tep_db_fetch_array($query_pack);
			
			$infos_products[0]['id'] = $data_pack['products_id'] .'_'. $pack_id[1] .'_'. $pack_id[2];
			$infos_products[1]['id'] = $data_pack['products_id_1'] .'_'. $pack_id[1] .'_'. $pack_id[2];
			if($data_pack['products_id_2'] != 0) $infos_products[2]['id'] = $data_pack['products_id_2'] .'_'. $pack_id[1] .'_'. $pack_id[2];
			$previous_is_pack = ($from_pack) ? true : false;
			$from_pack = true;
		}
		
		if($index++==0 && !$from_pack) {
			
			$mline .= '	<span style="font-weight:bold; color:#FE5700;">Les articles de mon panier</span>
						<hr style="color:#AAB41D;">';
		}
		
		
		for($j=0 ; $j<sizeof($infos_products) ; $j++) {
			
			//on sauve les donn�es du client
			if ($j == 0) {
				$firstName = $inrec['fname'];
				$lastName = $inrec['lname'];
				$mail = $inrec['email'];
				
				$custname = ucwords($firstName ." ". $lastName);
			}
		
			// get the shopping cart
			$query_products = tep_db_query("
									SELECT p.products_price price, p.products_tax_class_id taxclass, p.products_model model, pd.products_name name, pd.products_url
									FROM    " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd
									WHERE   p.products_id = '" . $infos_products[$j]['id'] . "' and pd.products_id = p.products_id");
			$inrec2 = tep_db_fetch_array($query_products);	
			
			//on r�cup�re l'id produit, la marque, le modele
			$repertoire = explode("_", $infos_products[$j]['id']);			
			
			/************************ RECUPERATION DES OPTIONS *******************/
			$options = preg_split('({|})', $repertoire[2]);
			
			$hasOption = (sizeof($options) > 1) ? true : false;
			
			//on contr�le qu'il s'agit bien du premier produit dans le cas d'un pack
			if ($hasOption) {
				
				// on enl�ve l'option du produit s'il y a
				$repertoire[2] = substr($repertoire[2], 0, strpos($repertoire[2], '{'));
				
				if ($j==0) {
					$query_options = 'SELECT products_options_name, products_options_values_name
									FROM products_options, products_options_values
									WHERE products_options_id='. $options[1] .' and products_options_values_id='. $options[2];
					$query_options = tep_db_query($query_options);
					$data_options = tep_db_fetch_array($query_options);
					
					$inrec2['name'] = bda_product_name_transform($inrec2['name'],$repertoire[2]) .' ('. $data_options['products_options_name'] .' : '. $data_options['products_options_values_name'] .')';
				} else {
					$inrec2['name'] = bda_product_name_transform($inrec2['name'], $repertoire[2]);
				}
			} else {
				$inrec2['name'] = bda_product_name_transform($inrec2['name'], $repertoire[2]);
			}
			
			// On commence par r�cup�rer les infos n�cessaires � l'�tablissement de l'url rewrit�e
			$query_rayon = 'select rubrique_id from '. TABLE_CATEGORIES_RAYON .' where categories_id='. $repertoire[1];
			$query_rayon = tep_db_query($query_rayon);
			$data_rayon = tep_db_fetch_array($query_rayon);
			
			$id_rayon = $data_rayon['rubrique_id'];
			$id_marque = $repertoire[1];
			$id_modele = $repertoire[2];
			$id_article = $repertoire[0];
			
			$url = WEBSITE .'/gas/'. $id_rayon . '-1/' . $id_marque . '-2/' . $id_modele . '-3/' . $id_article . '-4.html';
			$url = str_replace(" ", "_", $url);
			
			if ($from_pack) {				
				//les produits pouvant �tre dans des cat�gories diff�rentes, on ne met en lien que le premier produit du pack
				if ($j==0) {
					
					//on ferme le hr des articles
					if($index>1 && !$previous_is_pack) $mline .= '<hr style="color:#AAB41D;">';
					
					//affichage de l'en-t�te des packs
					$mline .= ($previous_is_pack) ? '<br>' : '<br><span style="font-weight:bold; color:#FE5700;">Les Packs de mon panier</span><hr style="color: rgb(170, 180, 29);">';
					
					
					$mline .= $inrec['qty'] . ' x <a href="'. $url .'" style="color:#FE5700;">' . $inrec2['name'] .'</a>';
				} else { 
					$mline .= '&nbsp;+&nbsp;'. $inrec['qty'] .' x '. $inrec2['name']; 
				}
			} else {
				if($index > 1) $mline .= '<br>';
				$mline .= $inrec['qty'] .' x <a href="'. $url .'" style="color:#FE5700;">'. $inrec2['name'] .'</a>';
			}
		}
		
	}//FIN de la liste des articles
	
	$mline .= '<hr style="color:#AAB41D;">';
	
	// E-mail Processing - Requires EMAIL_* defines in the
	$cquery = tep_db_query("select * from orders where customers_id = '". $cid ."'" );
	
	
	$email =	'Bonjour <span style="font-weight: bold; color:#AAB41D;">'. $custname .'</span>,<br><br>';
	$email.=	'Nous vous remercions de votre visite sur notre site <a href="'. WEBSITE .'" style="font-weight: bold; color:#FE5700;">'. ADRESSE_SITE .'</a> et de l\'int�r�t que vous portez � nos articles.<br>';
	
	$email.=	'Nous avons remarqu� que lors de cette visite vous avez s�lectionn� les articles suivants :<br><br>'. $mline .'<br>';
	$email.=	'Cette s�lection est toujours en attente. Ne prenez pas le risque de la perdre.<br>';
	$email.=	'Les articles m�moris�s peuvent ne plus �tre disponibles entre la m�morisation du panier et la validation.<br>Les prix peuvent avoir chang� entre la m�morisation du panier et sa validation.<br><br>';
	$email.=	'Toujours soucieux de proposer le meilleur service � nos clients, nous aimerions conna�tre les raisons pour lesquelles vous n\'�tes pas all� jusqu\'au bout du processus de commande en ne validant pas votre panier.<br><br>';
	$email.=	'Votre exp�rience et votre avis nous permettraient d\'am�liorer nos services et notre site pour mieux r�pondre � vos attentes.<br><br>';
		
	$email.=	'<hr style="color:#AAB41D;"/>			
				<br>' . SIGNATURE_MAIL .'';
	
	$objet = 'Votre panier '. NOM_DU_SITE;
	
	mail_client_commande($mail, $objet, $email);
	
	// See if a record for this customer already exists; if not create one and if so update it
	$donequery = tep_db_query("select * from ". TABLE_SCART ." where customers_id = '". $cid ."'");
	if (tep_db_num_rows($donequery) == 0)
		tep_db_query("insert into " . TABLE_SCART . " (customers_id, dateadded, datemodified, type_modif, id_modificateur ) values ('". $cid ."', '". seadate('0') ."', '". seadate('0') ."', 'email', '0')");
	else
		tep_db_query("update ". TABLE_SCART ." set datemodified = '". seadate('0') ."', type_modif='email', id_modificateur='0' where customers_id = ". $cid );
}


##### FONCTIONS SPECIFIQUES POUR L'ADMIN #####
function getHeaderForAdmin($from, &$mailContent) {
	
	$separator = sha1(date('r', time()));
	
	$boundary_mixed = 'Mixed-'. $separator;
	$boundary_alt = 'Alternative-'. $separator;
	$boundary_rel = 'Related-'. $separator;
	
	$header  = 'From: '. $from ."\n";
	$header .= 'Mime-Version: 1.0' ."\n";
	$header .= 'Content-Type: multipart/alternative; charset=ISO-8859-1; boundary="'. $boundary_alt .'"' ."\n";
	$header .= 'Content-Transfer-Encoding: 8bit';	
	
	$message_text  = 'Ceci est un message au format MIME 1.0 multipart/mixed.' . "\n\n";
	$message_text .= "\n\n\n--" . $boundary_alt . "\n";
	$message_text .= 'Content-Type: text/plain; charset=ISO-8859-1; format=flowed' . "\n\n";
	$message_text .= strip_tags(str_replace('<br>', "\n", $mailContent)) ."\n\n";
	
	$message_text .= "--" . $boundary_alt . "\n";
	$message_text .= 'Content-Type: multipart/related; boundary="'. $boundary_rel .'"' . "\n";
	$message_text .= 'Content-Transfer-Encoding: 8bit'. "\n\n";
	
	#HTML
	$message_text .= "\n\n\n--" . $boundary_rel . "\n";
	$message_text .= 'Content-Type: text/html; charset=ISO-8859-1' ."\n";
	$message_text .= 'Content-Transfer-Encoding: 8bit' . "\n\n";
	
	$image64List = parseMailImagesForAdmin($mailContent);
	
	$message_text .= $mailContent;
	
	foreach($image64List as $image) {
		
		// Add file attachment to the message
		$message_text .= "\n\n" . '--'. $boundary_rel ."\n" .
		'Content-Type: image/'. $image['type'] .';' ."\n" .
		'name="'. $image['name'] .'"' . "\n" .
		'Content-Transfer-Encoding: base64' . "\n" .
		'Content-ID:<'. $image['name'] .'>' . "\n".
		'Content-Disposition: inline;' . "\n".
		'filename="'. $image['name'] .'"' . "\n\n".
		$image['64b'] . "\n\n";
	}
	
	$message_text .= "--". $boundary_rel ."--\n";
	$message_text .= "--". $boundary_alt ."--\n";
	
	$mailContent = $message_text;
	
	return $header;
}

function parseMailImagesForAdmin(&$contentToParse) { //fonction qui va parser le contenu d'un mail pour encoder toutes ses images
	
	$imageList = array();
	preg_match_all('/src=\"([^\"]*)\"/iU', html_entity_decode(stripslashes($contentToParse)), $results);
	preg_match_all('/url\(\"([^\"]*)\"\)/iU', html_entity_decode(stripslashes($contentToParse)), $results2);
	
	$results = array_merge($results[1], $results2[1]);
	//printr($results);
	foreach($results as $image) {
		
		//$imageToReplace = $image;
		
		if(!array_key_exists($image, $imageList)) {//on v�rifie que l'image n'a pas d�j� �t� pars�e
			
			$imageList[$image] = parseImageInMailForAdmin($image); //fonction de l'admin pour encoder l'image en 64bits
			$contentToParse = preg_replace("/". str_replace('/', '\/', $image) ."/", 'cid:'. $imageList[$image]['name'], html_entity_decode(stripslashes($contentToParse)));
		}
	}
	
	return $imageList;
}

function parseImageInMailForAdmin($image) {
	
	$image = str_replace(WEBSITE . BASE_DIR_ADMIN, ABSOLUTE_DIR . BASE_DIR_ADMIN, $image);
	
	$image_name = substr($image, strrpos($image, '/')+1);//+1 => pour ne pas comptabiliser le /
	
	$image_type = substr($image, strrpos($image, '.')+1);//+1 pour ne pas comptabiliser le point
	
	$file = fopen($image,'rb');
	$image_data = fread($file, filesize($image));
	fclose($file);
	
	// Base64 encode the file data
	$image_data = chunk_split(base64_encode($image_data));
	
	return array('name' => $image_name, 'type' => $image_type, '64b' => $image_data);
}


function mail_admin($destinataire, $mailTitle, $contentTitle, $contentLink, $content, $mailType='normal') {
	
	switch($mailType) {
		
		case 'normal' :
			$img = '';
			$color = '#000000';
			break;
		
		case 'alert' :
			$img = '<img src="'. WEBSITE . BASE_DIR_ADMIN .'/images/newsletter/alert.png" alt="Alert">';
			$color = '#000000';
	}
	
	$title = (!empty($contentLink))
		? '<a href="'. $contentLink .'" style="color:#FFFFFF;">'. $contentTitle .'</a>'
		: $contentTitle;
	/*
	$headers = "MIME-Version: 1.0\n";
		
	$headers .= "X-Sender: <". ADRESSE_SITE .">\n";
	$headers .= "X-Mailer: PHP\n";
	$headers .= "X-auth-smtp-user: ". MAIL_INFO ." \n";
	$headers .= "X-abuse-contact: ". MAIL_INFO ." \n";
	$headers .= 'From: "' . NOM_DU_SITE . '"<' . MAIL_INFO . '>'."\n";
	$headers .= 'Reply-To: ' . MAIL_INFO ."\n";
	$headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
	*/
	$html = '<body>';
		
		$html .= '<table style="background-color:#80C3EB;margin-left:auto;margin-right:auto;width:1200px;border:0;padding:0;border-collapse:collapse;" cellpadding="0">
						<tbody>
							<tr style="background-color:#000000;">
								<td>
									<div style="float:left;margin:5px 0 0 10px;">
										'. $img .'
									</div>
									<div>
										<h2 style="color:#FFFFFF;text-align:center;">'. $title .'</h2>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div style="color:'. $color .';min-height:200px;padding:0 5px;">';
	$html .= $content;
	$html .= '					</div></td>
							</tr>
							<tr style="background-color:#000000;color:#FFFFFF;font-size:10px;height:20px;vertical-align:middle;">
								<td>
									<div style="text-align:center;">Ce mail a �t� g�n�r� par un Cron</div>
								</td>
						</tbody>
					</table>';
	$html .= '</body>';
	
	$headers = getHeaderForAdmin(MAIL_INFO, $html);
	
	//echo $message;
	mail($destinataire, $mailTitle, $html, $headers);
}
?>