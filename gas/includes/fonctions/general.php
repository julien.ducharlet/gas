<?php 
 
/*  
   Fichier regroupant toutes les fonctions g?n?rales au site pouvant ?tre utilis?es (et utiles) un peu partout  
*/
 
// Converti une somme (prix d'un article) pass? en param?tre en image de la taille pass?e en param?tre (by Paul) 
function prix2img($prix, $taille) 
{ 
	for($i=0;$i<=strlen($prix)-1;$i++) 
	{ 
		$prix = (string)$prix; 
		echo '<img src="' . BASE_DIR . '/images/prices_pictures/' . $taille . '/prix_' . $prix[$i] . '.jpg" alt="" />'; 
	} 
	 
	echo '<img src="' . BASE_DIR . '/images/prices_pictures/' . $taille . '/prix_E.jpg" alt="" />'; 
} 
 
// Converti un nombre au format mon?taire (affichage de deux d?cimales) 
function format_to_money($prix) { 
	$prix = round($prix, 2); 
	 
	if (!strpos($prix, '.')) { 
		$nb_decimales = 0; 
	} else { 
		$nb_decimales = strlen($prix) - (strpos($prix, '.')+1); 
	} 
	 
	if ($nb_decimales == 1) { 
		$prix .= '0'; 
	} else if ($nb_decimales == 0) { 
		$prix .= '.00'; 
	} 
	 
	return $prix; 
} 
 
// R?cup?re tout les param?tre $_GET d'une page donn?e -> fonction r?cup?r?e d'OSCommerce 
function tep_get_all_get_params($exclude_array = '') { 
	global $HTTP_GET_VARS; 
 
	if (!is_array($exclude_array)) $exclude_array = array(); 
 
	$get_url = ''; 
	if (is_array($HTTP_GET_VARS) && (sizeof($HTTP_GET_VARS) > 0)) { 
		reset($HTTP_GET_VARS); 
		while (list($key, $value) = each($HTTP_GET_VARS)) { 
			if ( (strlen($value) > 0) && ($key != 'error') && (!in_array($key, $exclude_array)) && ($key != 'x') && ($key != 'y') ) { 
				$get_url .= $key . '=' . rawurlencode(stripslashes($value)) . '&amp;'; 
			} 
		} 
	} 
 
	return $get_url; 
} 
 
//Fonction qui transforme le nom de l'article en rempla?ant les # par le nom du mod?le  
function bda_product_name_transform($products_name, $categories_id) { 
	//$products_name = preg_replace('/\[1\]/', 'pour', $products_name); 
	 
	$infos_categorie_query = tep_db_query("SELECT modele.parent_id as marque_id, modele_description.categories_name as modele_name, marque_description.categories_name as marque_name 
										   FROM categories modele, categories_description modele_description, categories_description marque_description 
										   WHERE modele.categories_id = '" . $categories_id . "' AND modele.categories_id = modele_description.categories_id AND modele.parent_id = marque_description.categories_id"); 
	$infos_categorie = tep_db_fetch_array($infos_categorie_query); 
	 
	$products_name = preg_replace('/\[#\]/', 'pour ' . $infos_categorie['marque_name'] . " " . $infos_categorie['modele_name'], $products_name); 
	$products_name = preg_replace('/\[@\]/', $infos_categorie['marque_name'] . " " . $infos_categorie['modele_name'], $products_name); 
	$products_name = preg_replace('/\[\*\]/', $infos_categorie['marque_name'], $products_name); 
	$products_name = preg_replace('/\[?\]/', $infos_categorie['modele_name'], $products_name); 
	return $products_name; 
} 
 
// Retourne une url permettant de trier les articles dans le sens pass? en param?tre et pour le champs pass? en param?tre  
function tri($nom_champs, $sens_tri='asc') { 
	 
	if (isset($_GET['ordre']) && ($_GET['ordre'] == 'asc' && $_GET['tri'] == $nom_champs)) { 
		$sens_tri = 'desc'; 
	} 
	 
	 
	$url = $_SERVER['PHP_SELF'] . "?" . tep_get_all_get_params(array('tri','ordre')) . "tri=" . $nom_champs . "&amp;ordre=" . $sens_tri; 
	return $url; 
	 
} 
 
//Retourne la date au format ex : 24 avril 1986  
function dat($date){ 
	$tab=array('janvier', 'f&eacute;vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'd&eacute;cembre'); 
	$d = $date; 
	list($an, $mois, $jour) = split('-', $d); 
	list($j,$h) = split(' ', $jour); 
	 
	$ret= $j." ".$tab[$mois-1]." ".$an; 
	 
	return $ret; 
} 
 
 
//Retourne la date au format Jeudi 22 Mai 2008  
function date_fr($timestamp_date){ 
	$mois=array('Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre'); 
	$jour=array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'); 
	 
	$mois_fr = $mois[date('n', $timestamp_date)-1]; 
	$jour_fr = $jour[date('w', $timestamp_date)]; 
	 
	$date_fr = $jour_fr . " " . date('d', $timestamp_date) . " " . $mois_fr . " " . date('Y', $timestamp_date); 
	 
	return $date_fr; 
} 
 
// Ajoute un produit au panier  
function ajout_panier($id_rayon, $id_article, $qte_article) { 
	 
	$date_ajout = date('Ymd'); 
	 
	$id_article_explode = explode('_', $id_article); 
	 
	$params_article = explode("{", $id_article); 
	$params_article = explode("}", $params_article[1]); 
	 
	$options_id = $params_article[0]; 
	$options_value_id = $params_article[1]; 
	 
	$prix_article_query = tep_db_query('SELECT pd.products_id, pd.products_name, pd.products_url, pd.products_description,  
									  p.products_price, p.products_weight, products_name, products_model, products_cost, 
									  ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1, 
									  ppbq.part_price_by_5, ppbq.pro_price_by_5, ppbq.rev_price_by_5, ppbq.adm_price_by_5, 
									  ppbq.part_price_by_10, ppbq.pro_price_by_10, ppbq.rev_price_by_10, ppbq.adm_price_by_10, 
									  s.specials_new_products_price, 
									  debut_vente_flash, fin_vente_flash, pvf.prix_vente 
                                        
									   FROM '. TABLE_PRODUCTS_DESCRIPTION .' AS pd  
									   inner JOIN '. TABLE_PRODUCTS .' AS p ON pd.products_id = p.products_id 
									   LEFT JOIN '. TABLE_PRODUCTS_PRICE_BY_QUANTITY .' ppbq ON p.products_id=ppbq.products_id 
									   LEFT JOIN '. TABLE_SPECIALS .' AS s ON (p.products_id=s.products_id AND (expires_date>NOW() OR expires_date=\'0000-00-00 00:00:00\')) 
									   LEFT JOIN '. TABLE_PRODUCTS_VENTES_FLASH .' AS pvf ON (p.products_id=pvf.products_id AND (debut_vente_flash<=NOW() AND (fin_vente_flash >NOW() OR fin_vente_flash=\'0000-00-00\'))) 
                                        
									   WHERE pd.products_id = '. $id_article_explode[0]); 
	 
	$prix_article_data = tep_db_fetch_array($prix_article_query); 
	 
	//calcule le prix en fonction du type de client	 
	$price = calculate_price_for_product($prix_article_data, $_SESSION['customers_type']); 
		 
	//$prix_article = $prix_article_data['products_price']; 
	 
	$test_article_query = tep_db_query("SELECT COUNT(*) AS article_existe FROM " . TABLE_PRODUCTS . " WHERE products_id = '" . $id_article_explode[0] . "'"); 
	$test_article = tep_db_fetch_array($test_article_query); 
	 
	if ($test_article['article_existe'] > 0) { 
		 
		if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] != '') { 
			 
			$retour = tep_db_query("SELECT COUNT(*) AS existe FROM " . TABLE_CUSTOMERS_BASKET . " WHERE customers_id = '" . $_SESSION['customer_id'] . "' AND products_id = '" . $id_article . "'"); 
			$donnees = tep_db_fetch_array($retour); 
			 
			if ($donnees['existe'] == 1) { 
				 
				tep_db_query("UPDATE " . TABLE_CUSTOMERS_BASKET . " SET customers_basket_quantity = customers_basket_quantity + '" . $qte_article . "' WHERE customers_id = '" . $_SESSION['customer_id'] . "' AND products_id = '" . $id_article . "'"); 
				//echo 'Quantit? mise ? jour !!!'; 
			} 
			else 
			if ($donnees['existe'] == 0) { 
				tep_db_query("INSERT INTO " . TABLE_CUSTOMERS_BASKET . " (customers_id, id_rayon, products_id, customers_basket_quantity, final_price, customers_basket_date_added) VALUES('" . $_SESSION['customer_id'] . "', '" . $id_rayon . "', '" . $id_article . "', '" . $qte_article . "', '" . $price['normal'] . "', '" . $date_ajout . "')"); 
				if (isset($options_id) && $options_id != '') { 
					tep_db_query("INSERT INTO " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " (customers_id, products_id, products_options_id, products_options_value_id) VALUES('" . $_SESSION['customer_id'] . "', '" . $id_article . "', '" . $options_id . "', '" . $options_value_id . "')"); 
				} 
				//echo 'Article ajout? !!!'; 
			} 
			else { 
				echo 'Erreur dans la mise ? jour de la base de donn?es ! Veuillez contacter notre service client.'; 
			} 
		} 
		else { 
			 
			$taille = count($_SESSION['panier']); 
			$i = 0; 
			 
			$keyToModify = -1; 
			$lastKey = -1; 
			 
			foreach($_SESSION['panier'] as $key => $value) { 
				 
				if ($value[0] == $id_article) $keyToModify = $key; 
				 
				$lastKey++; 
			} 
			 
			if ($keyToModify != -1) { 
				 
				$_SESSION['panier'][$keyToModify][1] += $qte_article; 
			} 
			else { 
				$_SESSION['panier'][$lastKey+1][0] = $id_article; 
				$_SESSION['panier'][$lastKey+1][1] = $qte_article; 
				$_SESSION['panier'][$lastKey+1][2] = $date_ajout; 
				$_SESSION['panier'][$lastKey+1][3] = $price['normal']; 
				$_SESSION['panier'][$lastKey+1][4] = $id_rayon; 
			} 
		} 
	} 
} 
 
// Ajoute un pack au panier  
function ajout_pack_panier($id_rayon, $id_pack, $qte_pack) { 
	$date_ajout = date('Ymd'); 
	 
	$pack_options_explode = explode('{', $id_pack); 
	$pack_explode = explode('_', $pack_options_explode[0]); 
	 
	$nb_articles_query = tep_db_query("SELECT pack_products_quantity, pack_remise FROM " . TABLE_PACK . " WHERE pack_id = '" . $pack_explode[0] . "'"); 
	$nb_articles_data = tep_db_fetch_array($nb_articles_query); 
	$nb_articles = $nb_articles_data['pack_products_quantity'];	 
	$prix_pack = $nb_articles_data['pack_prix_normal'] - $nb_articles_data['pack_remise']; 
	 
	$pack_infos = info_pack($pack_explode, ''); 
	 
	if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] != '') { 
		$retour = tep_db_query("SELECT COUNT(*) AS existe FROM " . TABLE_CUSTOMERS_BASKET . " WHERE customers_id = '" . $_SESSION['customer_id'] . "' and pack_id = '" . $id_pack . "'"); 
		$donnees = tep_db_fetch_array($retour); 
		 
		if ($donnees['existe'] == 1) { 
			tep_db_query("UPDATE " . TABLE_CUSTOMERS_BASKET . " SET customers_basket_quantity = customers_basket_quantity + '" . $qte_pack . "' WHERE customers_id = '" . $_SESSION['customer_id'] . "' and pack_id = '" . $id_pack . "'"); 
			//echo 'Quantit? mise ? jour !!!'; 
		} 
		else if ($donnees['existe'] == 0) { 
			tep_db_query("INSERT INTO " . TABLE_CUSTOMERS_BASKET . "(customers_id, id_rayon, pack_id, nb_articles_pack, customers_basket_quantity, customers_basket_date_added, final_price) VALUES('" . $_SESSION['customer_id'] . "', '" . $id_rayon . "', '" . $id_pack . "', '" . $nb_articles . "', '" . $qte_pack . "', '" . $date_ajout . "', '" . $pack_infos['prix_ht'] . "')"); 
			//echo 'Article ajout? !!!'; 
		} else { 
			echo 'Erreur dans la mise ? jour de la base de donn?es ! Veuillez contacter notre service client.'; 
		} 
	} else { 
		$taille = count($_SESSION['panier_packs']); 
		$i = 0; 
		 
		while ($_SESSION['panier_packs'][$i][0] != $id_pack && $i != $taille) { 
			$i++; 
		} 
		 
		if ($i != $taille) { 
			$_SESSION['panier_packs'][$i][1] += $qte_pack; 
		} else { 
			$_SESSION['panier_packs'][$taille][0] = $id_pack; 
			$_SESSION['panier_packs'][$taille][1] = $qte_pack; 
			$_SESSION['panier_packs'][$taille][2] = $date_ajout; 
			$_SESSION['panier_packs'][$taille][3] = $nb_articles; 
			$_SESSION['panier_packs'][$taille][4] = $pack_infos['prix_ht']; 
			$_SESSION['panier_packs'][$taille][5] = $id_rayon; 
		} 
	} 
} 
 
// Retire un article du panier  
function suppr_panier($id_article) { 
	if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] != '') { 
		tep_db_query("DELETE FROM " . TABLE_CUSTOMERS_BASKET . " WHERE products_id = '". $id_article ."' and customers_id = '" . $_SESSION['customer_id'] . "'"); 
		tep_db_query("DELETE FROM " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " WHERE products_id = '". $id_article ."' and customers_id = '" . $_SESSION['customer_id'] . "'"); 
		echo 'Article correctement supprim? de la BDD'; 
	} else { 
		 
		$taille = count($_SESSION['panier']); 
		$i = 0; 
		 
		while ($_SESSION['panier'][$i][0] != $id_article && $i != $taille) { 
			$i++; 
		} 
		 
		if ($i != $taille) { 
			unset($_SESSION['panier'][$i]); 
			echo 'Article correctement supprim? de la Session'; 
		} else { 
			echo 'C\'est bizarre l\'article n\'est pas pr?sent dans la Session !'; 
		} 
	} 
} 
 
// Retire un pack du panier  
function suppr_pack_panier($id_pack) { 
	if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] != '') { 
		tep_db_query("DELETE FROM " . TABLE_CUSTOMERS_BASKET . " WHERE pack_id = '". $id_pack ."' and customers_id = '" . $_SESSION['customer_id'] . "'"); 
		echo 'Pack correctement supprim? de la BDD'; 
	} else { 
		$taille = count($_SESSION['panier_packs']); 
		$i = 0; 
		 
		while ($_SESSION['panier_packs'][$i][0] != $id_pack && $i != $taille) { 
			$i++; 
		} 
		 
		if ($i != $taille) { 
			unset($_SESSION['panier_packs'][$i]); 
			echo 'Article correctement supprim? de la Session'; 
		} else { 
			echo 'C\'est bizarre l\'article n\'est pas pr?sent dans la Session !'; 
		} 
	} 
} 
 
function recup_mot_cle($url){ 
	$tab_url=explode("?",$url); 
	if(count($tab_url)>1){ 
		$tab_var=explode("&",$tab_url[1]); 
		foreach($tab_var as $q){ 
			$temp=explode("=",$q); 
			if($temp[0]=="q" || $temp[0]=="as_q" || $temp[0]=="as_epq" || $temp[0]=="query" || $temp[0]=="qs" || $temp[0]=="_nkw" || $temp[0]=="keyword" || $temp[0]=="rdata" || $temp[0]=="x_query" || $temp[0]=="p" || $temp[0]=="field-keywords"){ 
				$temp[1]=str_replace("%C3%A9","?",$temp[1]); 
				$temp[1]=str_replace("�","?",$temp[1]); 
				$temp[1]=str_replace("�","a",$temp[1]); 
				$temp[1]=str_replace("�","?",$temp[1]); 
				$temp[1]=str_replace("�","?",$temp[1]); 
				$temp[1]=str_replace("�","o",$temp[1]); 
				$temp[1]=str_replace("�","?",$temp[1]); 
				if($temp[1]!=''){ 
					return urldecode($temp[1]); 
				} 
			} 
		} 
	} 
	return ""; 
} 
 
// Retire les accents d'une chaine de caract?re  
function strip_accents($string){ 
	return strtr($string,'???????????????????????????????????????????????????', 
						 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'); 
} 
 
// Retire tout les caract?res sp?ciaux d'une url  
function encode_url($url){ 
	$url=str_replace("?","",$url); 
	$url=str_replace('%',"",$url); 
	$url=str_replace("#","",$url); 
	$url=str_replace("?","",$url); 
	$url=str_replace("?","",$url); 
	$url=str_replace("&","et",$url); 
	//$url=str_replace("/","",$url); 
	$url=str_replace(" ","-",$url); 
	$url=str_replace("'","_",$url); 
	$url=str_replace("`","_",$url); 
	$url=str_replace('?','',$url); 
	$url=str_replace('?','',$url); 
	$url=str_replace('?','',$url); 
	$url=str_replace('^','',$url); 
	$url=str_replace('"','',$url); 
	$url=str_replace('{','',$url); 
	$url=str_replace('}','',$url); 
	$url=str_replace('[','',$url); 
	$url=str_replace(']','',$url); 
	$url=str_replace('<','',$url); 
	$url=str_replace('>','',$url); 
	$url=str_replace("__","_",$url); 
	return strip_accents($url); 
} 
 
// Cr?e une URL en fonction des param?tres envoy?s  
function url($type,$args){ 
	switch($type) { 
		 
		case "marques" : 
			$url = "/gas/" . $args["id_marques"] . "-" . str_replace("/","",$args["nom_marques"]) . ".html"; 
			break; 
			 
		case "rayon" : 
			$url = "/gas/" . $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . ".html"; 
			break; 
		case "marque" : 
			$url = "/gas/" . $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . "/" . $args["id_marque"] . "-" . str_replace("/","",$args["url_marque"]) . ".html"; 
			break; 
		case "modele" : 
			$url = "/gas/" . $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . "/" . $args["id_marque"] . "-" . str_replace("/","",$args["url_marque"]) . "/" . $args["id_modele"] . "-" . str_replace("/","",$args["url_modele"]) . ".html"; 
			break; 
		case "modele_famille" : 
			$url = "/gas/" . $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . "/" . $args["id_marque"] . "-" . str_replace("/","",$args["url_marque"]) . "/" . $args["id_modele"] . "-" . $args["id_famille"] . "-" . str_replace("/","",$args["url_famille"]) . "-" . str_replace("/","",$args["url_modele"]) . ".html"; 
			break; 
		case "article" : 
			$url = "/gas/" . $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . "/" . $args["id_marque"] . "-" . str_replace("/","",$args["url_marque"]) . "/" . $args["id_modele"] . "-" . str_replace("/","",$args["url_modele"]) . "/" . $args["id_article"] . "-" . str_replace("/","",$args["url_article"]) . ".html"; 
			break; 
		case "erreur" : 
			$url = BASE_DIR."/url_error.php"; 
			break; 
	} 
//	return encode_url(BASE_DIR.$url); 
	return encode_url($url); 
} 
 
// Fonction de redirection  
function redirect($type,$destination){ 
	switch($type) { 
		//A utiliser 99% du temps 
		case 301 : 
			header("Status: 301 Moved Permanently", false, 301); 
			break; 
		//A ne jamais utiliser sans controle 
		case 404 : 
			header("HTTP/1.0 404 Not Found"); 
			break; 
	} 
	header("Location: https://www.generalarmystore.fr".$destination); 
	exit(); 
} 
 
 
// Fonction pour l'affichage des prix  
function taux_taxe() { 
	 
	if (!isset($_SESSION['customer_country_id']) || empty($_SESSION['customer_country_id'])) { 
		return 1.2; 
	} else {	 
		$pays_client = $_SESSION['customer_country_id']; 
		$type_client = !empty($_SESSION['customers_type']) ? $_SESSION['customers_type'] : '1';
		 
		$req_zone_tva = tep_db_query("select geo_zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where zone_country_id='".$pays_client."'"); 
		$tva = tep_db_fetch_array($req_zone_tva); 
		 
		if($tva['geo_zone_id']==GEO_ZONE_AVEC_TVA){ 
			return 1; 
		} 
		elseif($type_client==1){ 
			return 1.2; 
		} 
		else{ 
			if(isset($_SESSION['customer_id'])){ 
			 
				$req_tva_intra=tep_db_query("select customers_tva from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['customer_id'].""); 
				$nb_res=tep_db_num_rows($req_tva_intra); 
				 
				if($nb_res==0){} 
				elseif($pays_client==1){return 1.2; } 
					else return 1; 
			}else{return 1.2;return 1.2; } 
		} 
	} 
} 
 
// Affichage des prix en fonction du pays pass? en parametre  
function parametrage_affiche_prix($pays){ 
	 
		$type_client = !empty($_SESSION['customers_type']) ? $_SESSION['customers_type'] : '1';
		 
		$req_zone_tva = tep_db_query("select geo_zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where zone_country_id='".$pays."'"); 
		$tva = tep_db_fetch_array($req_zone_tva); 
		 
		if ($tva['geo_zone_id']==GEO_ZONE_AVEC_TVA) { 
			return 1; 
		} elseif ($type_client==1) { 
			return 1.2; 
		} else { 
			if(isset($_SESSION['customer_id'])){ 
			$req_tva_intra=tep_db_query("select customers_tva from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['customer_id'].""); 
			$nb_res=tep_db_num_rows($req_tva_intra); 
			 
			if($nb_res==0){} 
			elseif ($pays==1){ return 1.2; } 
				else return 1;			 
			} else{ return 1.2;return 1.2; }	 
		} 
} 
 
 
function final_tva(){ 
	if (!isset($_SESSION['customer_country_id']) || empty($_SESSION['customer_country_id'])) { 
		return 1.2; 
	} else {	 
		$pays_client = $_SESSION['customer_country_id']; 
		$type_client = $session_customers_type;
		echo "select geo_zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where zone_country_id='".$pays_client."'"; 
		$req_zone_tva = tep_db_query("select geo_zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where zone_country_id='".$pays_client."'"); 
		$tva = tep_db_fetch_array($req_zone_tva); 
		 
		if($tva['geo_zone_id']==GEO_ZONE_AVEC_TVA){ 
			return 1; 
		} 
		elseif($type_client==1){ 
			return 1.2; 
		} 
		else{ 
			if(isset($_SESSION['customer_id'])){ 
			$req_tva_intra=tep_db_query("select customers_tva from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['customer_id'].""); 
			$nb_res=tep_db_num_rows($req_tva_intra); 
			 
			if($nb_res==0){} 
			elseif($pays_client==1){return 1.2; } 
				else return 1; 
			}else{return 1.2;return 1.2; }	 
		} 
	} 
} 
 
function affiche_info($customers_id, $type_client, $client_vip, $tous, $client, $rev, $pro, $adm, $vip) { 
	if($tous==1){ 
		echo "<!-- tous".$tous." -->"; 
		return true; 
	} 
	elseif(isset($type_client) && !empty($type_client) && $customers_id>0){ 
		if($type_client==1){ 
			echo "<!-- client".$type_client.$client.$customers_id." -->"; 
			if($client==1){return true;} 
		} 
		elseif($type_client==2){ 
			echo "<!-- pro -->"; 
			if($pro==1){return true;} 
		} 
		elseif($type_client==3){ 
			echo "<!-- rev -->"; 
			if($rev==1){return true;} 
		} 
		elseif($type_client==4){ 
			echo "<!-- adm -->"; 
			if($adm==1){return true;} 
		} 
	} 
	 
	if($client_vip==1 && $vip==1){ 
		echo "<!-- vip -->"; 
		return true; 
	} 
	return false; 
} 
 
function correction_tva($tva_exacte) { 
	$tva_arrondie = format_to_money($tva_exacte); 
	 
	if (substr($tva_exacte, strpos($tva_exacte, '.')+3, 1) < 5) { 
		$tva_arrondie += 0.01; 
	} 
	 
	$tva = array('arrondie' => $tva_arrondie, 'exacte' => $tva_exacte); 
	 
	return $tva; 
} 
 
// Retire les carat?re de SQL injection 
function format_text($chaine) { 
	$chaine = str_replace('\\', '', $chaine); 
	$chaine = str_replace('\'', '', $chaine); 
	$chaine = str_replace('"', '', $chaine); 
	$chaine = str_replace(',', '', $chaine); 
	return $chaine; 
} 
 
// retire les caract?res sp?ciaux des num?ros de tel & fax 
function format_tel($chaine) { 
	$chaine = str_replace('+', '00', $chaine); 
	$chaine = str_replace(' ', '', $chaine); 
	$chaine = str_replace('-', '', $chaine); 
	$chaine = str_replace('/', '', $chaine); 
	$chaine = str_replace('\\', '', $chaine); 
	$chaine = str_replace('\'', '', $chaine); 
	$chaine = str_replace('"', '', $chaine); 
	$chaine = str_replace(',', '', $chaine); 
	return $chaine; 
} 
 
// retire les caract?res sp?ciaux du num?ro de TVA 
function format_tva($chaine) { 
	$chaine = str_replace(" ", "", $chaine); 
	return $chaine; 
} 
 
function get_page_name() {	 
	$page_url = $_SERVER['PHP_SELF']; 
	$page_url = explode('?', $page_url); 
	$current_page = explode('/', $page_url[0]); 
	return $current_page[sizeof($current_page)-1]; 
} 
 
function get_zone($page = 'index.php', $pages_liste) { 
	$key_to_return = ''; 
	while (list($key, $value) = each($pages_liste)) { 
		if(in_array($page, $value)) { 
			$key_to_return = $key; 
		} 
	} 
	return ($key_to_return != '') ? $key_to_return : 'zone1'; 
} 
 
function poids_livraison() { 
	//panier simple 
	$panier_query = tep_db_query("SELECT cb.products_id , cb.customers_basket_quantity 
                                  FROM ".TABLE_CUSTOMERS_BASKET." as cb 
                                  WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "'"); 
                                 
	$poids_panier=0; 
	while($panier_data = tep_db_fetch_array($panier_query)){ 
		//$i++; 
		$article_options = explode("{", $panier_data['products_id']); 
		$article = explode("_", $article_options[0]); 
		$id_article = $article[0]; 
									 
		$article_query = tep_db_query("SELECT p.products_weight 
                                      FROM ".TABLE_PRODUCTS_DESCRIPTION." as pd, ".TABLE_PRODUCTS." as p 
                                      WHERE pd.products_id = '" . $id_article . "' and pd.products_id = p.products_id"); 
									 
		$article_data = tep_db_fetch_array($article_query); 
		$poids_panier=$poids_panier+($article_data['products_weight']*$panier_data['customers_basket_quantity']); 
	} 
	  
	//panier pack 
	$packs_query = tep_db_query("SELECT cb.pack_id, cb.customers_basket_quantity 
                                  FROM ".TABLE_CUSTOMERS_BASKET." as cb 
                                  WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' and cb.pack_id <> ''"); 
	$nb_packs = tep_db_num_rows($packs_query); 
	$poids_pack=0; 
	if ($nb_packs > 0) {		 
		while ($packs_data = tep_db_fetch_array($packs_query)) { 
			$id_complet = $packs_data['pack_id']; 
			//$i++; 
			$pack = explode("_", $id_complet); 
			$id_pack = $pack[0]; 
			$quantite = $packs_data['customers_basket_quantity']; 
								 
			$articles_pack_query = tep_db_query("SELECT p.products_weight 
												FROM ".TABLE_PACK." as pp, ".TABLE_PRODUCTS_DESCRIPTION." as pd, ".TABLE_PRODUCTS." as p 
												WHERE pp.pack_id = " . $id_pack . " 
											   and (pp.products_id = pd.products_id  
													 or pp.products_id_1 = pd.products_id  
													 or pp.products_id_2 = pd.products_id) 
											and pd.products_id = p.products_id"); 
			$sous_poids=0; 
			while ($articles_pack_data = tep_db_fetch_array($articles_pack_query)) { 
				$poids=$articles_pack_data['products_weight']; 
				$sous_poids=$sous_poids+($poids*$quantite); 
			} 
			$poids_pack=$poids_pack+$sous_poids; 
        } 
	} 
	$poids_total_combo=$poids_panier+$poids_pack; 
	$_SESSION['poids_commande']=$poids_total_combo; 
	return $poids_total_combo;  
} 
 
function prix_livraison() {	 
	$poids_total_combo = poids_livraison(); 
	 
	$messageToReturn = ''; 
	 
	$id_address=tep_db_query("	SELECT cus.customers_default_address_id, a.address_book_id, a.entry_country_id, c.countries_name, c.countries_iso_code_2, c.countries_id_zone_livraison 
								FROM ".TABLE_ADDRESS_BOOK." a, ".TABLE_COUNTRIES." c, ".TABLE_CUSTOMERS." cus  
								WHERE a.entry_country_id=c.countries_id  
								AND a.customers_id=".$_SESSION['customer_id']."  
								AND cus.customers_id = a.customers_id");//*/ 
	 
	while($res_id=tep_db_fetch_array($id_address)) { 
		 
		$req_a = tep_db_query("	SELECT prix_livraison 
								FROM ".TABLE_MODULE_LIVRAISON." 
								WHERE zone_livraison_id=".$res_id['countries_id_zone_livraison']." 
								AND poids_livraison >= ".$poids_total_combo); 
		 
		if (tep_db_num_rows($req_a)>0) {		 
			$res_a = tep_db_fetch_array($req_a); 
			$prix = format_to_money($res_a['prix_livraison']*parametrage_affiche_prix($res_id['entry_country_id'])).' &euro;'; 
		} else { 
			$prix = '<span style="color:#FF0000;">Le poids de votre commande est trop important veuillez nous contacter merci</span>'; 
		} 
		 
		$messageToReturn .= '<div id="prix_livraison'. $res_id['address_book_id'] .'" '; 
	   
		if ((isset($_SESSION['adresse_livraison']) && !empty($_SESSION['adresse_livraison'])) && $_SESSION['adresse_livraison']==$res_id['address_book_id'])  { 
			$messageToReturn .= 'style="display:block;" >'; 
			$messageToReturn .= $prix; 
			$_SESSION['frais_livraison']=format_to_money($res_a['prix_livraison']*parametrage_affiche_prix($res_id['entry_country_id'])); 
	   } elseif (!isset($_SESSION['adresse_livraison']) && $res_id['address_book_id']==$res_id['customers_default_address_id']) {  
			$messageToReturn .= 'style="display:block;" >'; 
			$messageToReturn .= $prix; 
			$_SESSION['frais_livraison']=format_to_money($res_a['prix_livraison']*parametrage_affiche_prix($res_id['entry_country_id'])); 
		} else { 
			$messageToReturn .= 'style="display:none;" >'; 
			$messageToReturn .= $prix; 
		} 
		$messageToReturn .= '</div>'; 
	} 
	return $messageToReturn; 
} 
  
function calculate_price_for_product($data) { 
	 
	if(isset($_SESSION) && !empty($_SESSION['customers_type'])) { 
		 
		$taxe = taux_taxe(); 
		 
		switch($_SESSION['customers_type']) { 
			 
			case 1: //particulier 
				$prix = ($data['part_price_by_1'] != "0.00") ? $data['part_price_by_1'] : $data['products_price']; 
				$prix_promo = (!empty($data['specials_new_products_price'])) ? $data['specials_new_products_price'] : 0; 
				$prix_flash = (!empty($data['prix_vente'])) ? $data['prix_vente'] : 0; 
				 
				$type = ($taxe==1) ? 'HT' : 'TTC'; 
				$tva = $taxe;//ht ou tcc 
				//if(!empty($_SESSION) && $_SESSION['customer_id']==20281) echo $prix_flash .'<br>'; 
				if ($prix_flash > 0) {					 
					$prixHT = $prix_flash; 
					$prix_flash = format_to_money($data['prix_vente'] * $taxe); 
				} elseif ($prix_promo > 0) {					 
					$prixHT = $prix_promo; 
					$prix_promo = format_to_money($data['specials_new_products_price'] * $taxe); 
				} else {  
					$prixHT = $prix; 
				} 
				 
				$prix = format_to_money($prix * $taxe); 
				break; 
				 
			case 2: //pro 
				$prix = ($data['pro_price_by_1'] != "0.00") ? $data['pro_price_by_1'] : $data['products_price']; 
				$prix_promo = (!empty($data['specials_new_products_price']) && $data['specials_new_products_price']<$prix) ? $data['specials_new_products_price'] : 0; 
				$prix_flash = (!empty($data['prix_vente']) && $data['prix_vente']<$prix) ? $data['prix_vente'] : 0; 
				 
				if(!empty($_SESSION['cutomer_tva'])) {					 
					$type = 'HT'; 
					$tva = 1; 
				} else {					 
					$type = 'TTC'; 
					$tva = 1.2; 
				} 
				 
				if ($prix_flash > 0) {					 
					$prixHT = $prix_flash; 
					$prix_flash = format_to_money($data['prix_vente'] * $tva); 
				} elseif ($prix_promo > 0) {					 
					$prixHT = $prix_promo; 
					$prix_promo = format_to_money($data['specials_new_products_price'] * $tva); 
				} else $prixHT = $prix; 
				 
				$prix = format_to_money($prix * $tva); 
				break; 
			 
			case 3: //revendeur 
				$prix = ($data['rev_price_by_1'] != "0.00") ? $data['rev_price_by_1'] : $data['products_price']; 
				$prix_promo = 0; 
				$prix_flash = 0; 
				 
				$type = 'HT'; 
				$tva = 1; 
				 
				$prixHT = $prix; 
				$prix = format_to_money($prix); 
				break; 
				 
			case 4: //administration 
				$prix = ($data['adm_price_by_1'] != "0.00") ? $data['adm_price_by_1'] : $data['products_price']; 
				$prix_promo = (!empty($data['specials_new_products_price']) && $data['specials_new_products_price']<$prix) ? $data['specials_new_products_price'] : 0; 
				$prix_flash = (!empty($data['prix_vente']) && $data['prix_vente']<$prix) ? $data['prix_vente'] : 0; 
				 
				$type = 'HT'; 
				$tva = 1; 
				 
				if ($prix_flash > 0) { 
					$prixHT = $prix_flash; 
					$prix_flash = format_to_money($data['prix_vente']); 
				} elseif ($prix_promo > 0) { 
					 
					$prixHT = $prix_promo; 
					$prix_promo = format_to_money($data['specials_new_products_price']); 
				} else $prixHT = $prix; 
				 
				$prix = format_to_money($prix); 
				break; 
				 
			default: 
				$prix = $data['products_price']; 
				$prix_promo = (!empty($data['specials_new_products_price'])) ? $data['specials_new_products_price'] : 0; 
				$prix_flash = (!empty($data['prix_vente'])) ? $data['prix_vente'] : 0; 
				 
				$type = ($taxe==1) ? 'HT' : 'TTC'; 
				$tva = $taxe;//ht ou tcc 
				 
				if ($prix_flash > 0) {					 
					$prixHT = $prix_flash; 
					$prix_flash = format_to_money($data['prix_vente'] * $taxe); 
				} elseif ($prix_promo > 0) {					 
					$prixHT = $prix_promo; 
					$prix_promo = format_to_money($data['specials_new_products_price'] * $taxe); 
				} else $prixHT = $prix; 
				 
				$prix = format_to_money($prix * $taxe); 
				break; 
		} 
	} else { 
		$prix = $data['products_price']; 
		$prixHT = $prix; 
		$prix_promo = (!empty($data['specials_new_products_price'])) ? format_to_money($data['specials_new_products_price']*taux_taxe()) : 0; 
		$prix = format_to_money($prix * taux_taxe()); 
	} 
	 
	return array('type' => $type, 'HT' => $prixHT, 'normal' => $prix, 'promo' => $prix_promo, 'flash' => $prix_flash, 'tva' => $tva); 
} 
 
function show_price($price) {	 
	if ($price['flash'] > 0) {											   
		$prix_to_show = $price['flash']; 
	} elseif ($price['promo'] > 0) {  
		$prix_to_show = $price['promo']; 
	} else { 		 
		$prix_to_show = $price['normal']; 
	}	 
	return $prix_to_show; 
} 
 
function get_destock($type_stock) { 
	switch($type_stock) { 
		case 'tout' : $destock = false; 
		break; 
		case 'sur_cmd' : $destock = false; 
		break; 
		case 'destock_visible' : $destock = true; 
		break; 
		case 'destock_cache' : $destock = true; 
		break; 
		case 'destock_cache_qte' : $destock = true; 
		break; 
		default : $destock = false; 
	} 
	return $destock; 
} 
 
function getImageProperty($pathToImage) { 
	list($width, $height, $type, $attr) = getimagesize($pathToImage); 
	return array('width' => $width, 'height' => $height); 
} 
 
function printr($array) {	 
	echo '<pre>'; 
		print_r($array); 
	echo '</pre>'; 
} 
 
function _substr($str, $length, $minword = 3) { 
	 
    $sub = ''; 
    $len = 0; 
    
    foreach (explode(' ', $str) as $word) { 
		 
        $part = (($sub != '') ? ' ' : '') . $word; 
        $sub .= $part; 
        $len += strlen($part); 
        
        if (strlen($word) > $minword && strlen($sub) >= $length) { 
            break; 
        } 
    } 
    
    return $sub . (($len < strlen($str)) ? '...' : ''); 
} 
 
function convert_date_vente_flash($datetime) { 
	 
	$date = explode(' ', $datetime); 
	$time = $date[1]; 
	$date = $date[0]; 
	 
	$date = explode('-', $date); 
	$date = array_reverse($date); 
	$date = implode('/', $date); 
	 
	$time = explode(':', $time); 
	$time = $time[0] .'h'. $time[1]; 
	 
	return array("date" => $date, "time" => $time); 
} 
 
function date_is_past($date1, $date2 = '') { //date1 format? comme ds la bdd 
	 
	if ($date1 != '') { 
		 
		if (empty($date2)) $date2 = date('YmdHis'); 
		 
		$date1 = explode(' ', $date1); 
		$heure = $date1[1]; 
		 
		$date1 = explode('-', $date1[0]); 
		$date1 = implode('', $date1); 
		 
		$heure = explode(':', $heure); 
		$heure = implode('', $heure); 
		 
		$date1 .= $heure; 
		 
		return ($date1<$date2) ? true : false; 
	} else return false; 
} 
 
function temps_restant($datetime_fin_flash) { 
	 
	$datetime_fin_flash = explode(' ', $datetime_fin_flash); 
	$date = $datetime_fin_flash[0]; 
	$time = $datetime_fin_flash[1]; 
	 
	$current_datetime = date('Y-m-d H:i:s'); 
	$current_datetime = explode(' ', $current_datetime); 
	 
	$current_date = $current_datetime[0]; 
	$current_time = $current_datetime[1]; 
	 
	$date = explode('-', $date); 
	$current_date = explode('-', $current_date); 
	 
	$time = explode(':', $time); 
	$current_time = explode(':', $current_time); 
	 
	$mois = $date[1] - $current_date[1]; 
	$jour = $date[2] - $current_date[2]; 
	 
	$heure = $time[0] - $current_time[0]; 
	$min = $time[1] - $current_time[1]; 
	$sec = $time[2] - $current_time[2]; 
	 
	if ($sec<0) {		 
		$min--; 
		$sec += 60; 
	} 
	 
	if ($min<0) {		 
		$heure--; 
		$min += 60; 
	} 
	 
	if ($heure<0) {		 
		$jour--; 
		$heure += 24; 
	} 
	 
	if ($jour<0) {		 
		$mois--; 
		$nbre_jours_du_mois = date('t');		 
		$jour += $nbre_jours_du_mois; 
	} 
		 
	return array("jour" => $jour, "heure" => $heure, "min" => $min, "sec" => $sec); 
} 
 
function show_temps_restant($jour, $heure, $min, $sec, $type = 'full') { 
	 
	$temps_restant = ($type=='full') ? 'Il vous reste ' : ''; 
	 
	$temps_restant .= '<span class="relief">'; 
	 
	if ($jour > 0) { 
		 
		$temps_restant .= '<span class="jour">'; 
		$temps_restant .= ($jour == 1) ? $jour .'</span> jour ' : $jour .'</span> jours '; 
	} 
	 
	if ($heure > 0) { 
		 
		$temps_restant .= '<span class="heure">'. $heure .'</span>'; 
		$temps_restant .= ($heure==1 || $heure==0) ? ' heure ' : ' heures '; 
	} 
	 
	$temps_restant .= '<span class="min">'; 
	$temps_restant .= ($min<10) ? '0'. $min : $min; 
	$temps_restant .= '</span>'; 
	$temps_restant .= ($min==1 || $min==0) ? ' minute ' : ' minutes '; 
	 
	$temps_restant .= 'et <span class="sec">'. $sec .'</span>'; 
	$temps_restant .= ($sec==1 || $sec==0) ? ' seconde ' : ' secondes '; 
	 
	$temps_restant .= '</span>'; 
	 
	$temps_restant .= ($type=='full') ? ' pour en profiter.' : ''; 
	 
	return $temps_restant; 
} 
 
// Affiche une image en fonction d'une note pour avis v?rifi?s sur les valeurs 1, 2, 3, 4 et 5  (by Thierry) 
function avis_verifies($type){ 
	switch($type) { 
		case 5 : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/5-etoiles.png" alt="Note de 5/5" style="vertical-align:middle;">'; 
			break; 
		case 4 : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/4-etoiles.png" alt="Note de 4/5" style="vertical-align:middle;">'; 
			break; 
		case 3 : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/3-etoiles.png" alt="Note de 3/5" style="vertical-align:middle;">'; 
			break; 
		case 2 : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/2-etoiles.png" alt="Note de 2/5" style="vertical-align:middle;">'; 
			break; 
		case 1 : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/1-etoiles.png" alt="Note de 1/5" style="vertical-align:middle;">'; 
			break; 
	} 
	return $avis; 
} 
 
// Affiche une image en fonction d'une note pour avis v?rifi?s sur des valeurs de 1 a 5 avec virgule (by Thierry) 
function avis_verifies_complet($type){ 
	switch($type) { 
		case 5 : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/5-etoiles.png" alt="Note de 5/5" style="vertical-align:middle;">'; 
			break; 
		case ($type >= 4.5 && $type <= 4.99) : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/4.5-etoiles.png" alt="Note de 4.5/5" style="vertical-align:middle;">'; 
			break; 
		case ($type >= 4 && $type <= 4.49) : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/4-etoiles.png" alt="Note de 4/5" style="vertical-align:middle;">'; 
			break; 
		case ($type >= 3.5 && $type <= 3.99) : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/3.5-etoiles.png" alt="Note de 3.5/5" style="vertical-align:middle;">'; 
			break;			 
		case ($type >= 3 && $type <= 3.49) : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/3-etoiles.png" alt="Note de 3/5" style="vertical-align:middle;">'; 
			break;			 
		case ($type >= 2.5 && $type <= 2.99) : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/2.5-etoiles.png" alt="Note de 2.5/5" style="vertical-align:middle;">'; 
			break;			 
		case ($type >= 2 && $type <= 2.49) : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/2-etoiles.png" alt="Note de 2/5" style="vertical-align:middle;">'; 
			break;			 
		case ($type >= 1.5 && $type <= 1.99) : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/1.5-etoiles.png" alt="Note de 1.5/5" style="vertical-align:middle;">'; 
			break;			 
		case ($type >= 1 && $type <= 1.49) : 
			$avis = '<img src="https://www.generalarmystore.fr/gas/images/etoiles/1-etoiles.png" alt="Note de 1/5" style="vertical-align:middle;">'; 
			break; 
	} 
	return $avis; 
} 
 
// Retire tout les caract?res sp?ciaux des informations d'un client  
function format_clients($chaine){ 
	 
	$chaine=str_replace('\\', '', $chaine); 
	$chaine=str_replace('\'', ' ', $chaine); 
	$chaine=str_replace('//', '', $chaine); 
	$chaine=str_replace("/","",$chaine); 
	$chaine=str_replace(',', ' ', $chaine); 
	$chaine=str_replace("?"," ",$chaine); 
	$chaine=str_replace("?"," ",$chaine); 
	$chaine=str_replace('%',"",$chaine); 
	$chaine=str_replace("#","",$chaine); 
	$chaine=str_replace("?","",$chaine); 
	$chaine=str_replace("?","",$chaine); 
	$chaine=str_replace("&","et",$chaine); 
	$chaine=str_replace("'"," ",$chaine); 
	$chaine=str_replace("`"," ",$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('^','',$chaine); 
	$chaine=str_replace('"','',$chaine); 
	$chaine=str_replace('{','',$chaine); 
	$chaine=str_replace('}','',$chaine); 
	$chaine=str_replace('[','',$chaine); 
	$chaine=str_replace(']','',$chaine); 
	$chaine=str_replace('<','',$chaine); 
	$chaine=str_replace('>','',$chaine); 
	//$chaine=str_replace("_"," ",$chaine); 
	return strip_accents($chaine); 
} 
 
// Retire tout les caract?res sp?ciaux des informations d'un client  
function format_inscription($chaine){ 
	 
	$chaine=str_replace('\\','', $chaine); 
	$chaine=str_replace('\'','', $chaine); 
	$chaine=str_replace('//','', $chaine); 
	$chaine=str_replace('/','',$chaine); 
	$chaine=str_replace(',', ' ', $chaine); 
	$chaine=str_replace('n?','Num ',$chaine); 
	$chaine=str_replace('N?','Num ',$chaine); 
	$chaine=str_replace('?',' ',$chaine); 
	$chaine=str_replace('?',' ',$chaine); 
	$chaine=str_replace('%','',$chaine); 
	$chaine=str_replace('#','',$chaine); 
	$chaine=str_replace('@','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('&',' et ',$chaine); 
	$chaine=str_replace("'"," ",$chaine); 
	$chaine=str_replace("`"," ",$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('^','',$chaine); 
	$chaine=str_replace('"','',$chaine); 
	$chaine=str_replace('{','',$chaine); 
	$chaine=str_replace('}','',$chaine); 
	$chaine=str_replace('[','',$chaine); 
	$chaine=str_replace(']','',$chaine); 
	$chaine=str_replace('<','',$chaine); 
	$chaine=str_replace('>','',$chaine); 
	$chaine=str_replace('_',' ',$chaine); 
	$chaine=str_replace('-',' ',$chaine); 
	$chaine=str_replace('*',' ',$chaine); 
	 
	return strip_accents($chaine); 
} 
 
// Retire tout les caract?res sp?ciaux des informations d'un client  
function format_recherche($chaine){ 
	 
	//$chaine=str_replace('\'','', $chaine); 
	//$chaine=str_replace('/','',$chaine); 
	$chaine=str_replace(',','', $chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('%','',$chaine); 
	$chaine=str_replace('#','',$chaine); 
	$chaine=str_replace('@','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('&','',$chaine); 
	$chaine=str_replace("'","",$chaine); 
	$chaine=str_replace("`","",$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('?','',$chaine); 
	$chaine=str_replace('^','',$chaine); 
	$chaine=str_replace('"','',$chaine); 
	$chaine=str_replace('{','',$chaine); 
	$chaine=str_replace('}','',$chaine); 
	$chaine=str_replace('[','',$chaine); 
	$chaine=str_replace(']','',$chaine); 
	$chaine=str_replace('<','',$chaine); 
	//$chaine=str_replace('>','',$chaine); 
	//$chaine=str_replace('_','',$chaine); 
	//$chaine=str_replace('-','',$chaine); 
	$chaine=str_replace('*','',$chaine); 
	 
	return strip_accents($chaine); 
} 
 
?>