<?php
/* Fonctions concernant le panier et la gestion de la commande */

//fonction soustotal panier 
function sous_total_panier($id_client){
	$panier_query = tep_db_query("SELECT cb.products_id, cb.customers_basket_quantity, cb.id_rayon 
												  FROM " . TABLE_CUSTOMERS_BASKET . " as cb
												  WHERE cb.customers_id = '" . $id_client . "' and cb.pack_id = ''");
					
					$i = 0;
					$nb_articles = tep_db_num_rows($panier_query);
					$articles_sous_total_HT = 0;
                    $pack_sous_total_HT = 0;
                    
					if ($nb_articles > 0) {
						while($panier_data = tep_db_fetch_array($panier_query)){
							$id_complet = $panier_data['products_id'];
							$i++;
							
							// On r�cup�re les infos sur les options
							$article_options = explode("{", $panier_data['products_id']);
							$options = explode("}", $article_options[1]);
							$option_details = infos_options($options[0], $options[1]);
							
							// On r�cup�re les infos sur l'article
							$article = explode("_", $article_options[0]);
							$quantite = $panier_data['customers_basket_quantity'];
							$article_details = infos_article($article, $quantite);
							
							// On r�cup�re des infos compl�mentaires
							$articles_sous_total_HT += $article_details['prix_ht']*$quantite;
						}
					}
					
					
					 $packs_query = tep_db_query("SELECT cb.pack_id, cb.customers_basket_quantity, cb.id_rayon 
                                                      FROM " . TABLE_CUSTOMERS_BASKET . " as cb
                                                      WHERE cb.customers_id = '" . $id_client . "' and cb.pack_id <> ''");
						
                        $nb_packs = tep_db_num_rows($packs_query);
						
                        if ($nb_packs > 0) {
                            while($packs_data = tep_db_fetch_array($packs_query)){

								$id_complet = $packs_data['pack_id'];
                                $i++;
								
								// On r�cup�re les infos sur les options
								$pack_options = explode("{", $id_complet);
								$options = explode("}", $pack_options[1]);
								$option_details = infos_options($options[0], $options[1]);
                                
								// On r�cup�re les infos sur le pack
                                $pack = explode("_", $pack_options[0]);
								$pack_details = info_pack($pack, $option_details);
								
								// On r�cup�re des infos compl�mentaires
								$quantite = $packs_data['customers_basket_quantity'];
								$pack_sous_total_HT += $pack_details['prix_ht']*$quantite;
							 }
                        }
						return $articles_sous_total_HT + $pack_sous_total_HT;
}

function promo_product($id_article) {
	
	$today = date("Y-m-d H:i:s");
	 
	$req_promo = tep_db_query("select specials_new_products_price, expires_date from specials where products_id=". $id_article ." and (expires_date >'". $today ."' or expires_date='0000-00-00 00:00:00')");
	$res_promo = tep_db_fetch_array($req_promo);
	
	return (tep_db_num_rows($req_promo)==0) ? 0 : $res_promo['specials_new_products_price'];
}

// Affichage des articles du panier d'un client
function affiche_article_panier($nb_articles, $id_complet, $id_rayon, $article_details, $option_details, $quantite, $i, $modifiable, $poids) {
	
	//prise en compte de la zone tva pour affichage
	if (taux_taxe() == 1.2) {
		$tva=1;
	} else { 
		$tva=0; 
	}
	?>
    <tr id="<?php echo $id_complet; ?>">
    	<?php
		$rayon_query = tep_db_query("SELECT rubrique_url
								   FROM " . TABLE_RAYON . "
								   WHERE rubrique_id = '" . $id_rayon . "'");
		$rayon_data = tep_db_fetch_array($rayon_query);
		$url_rayon = $rayon_data['rubrique_url'];
    
        
        $option_details_id = isset($option_details['id']) ? $option_details['id'] : "";
        $option_details_id_valeur = isset($option_details['id_valeur']) ? $option_details['id_valeur'] : "";
		
        $url = url('article', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $article_details['id_marque'], 'url_marque' => $article_details['url_marque'], 'id_modele' => $article_details['id_modele'], 'url_modele' => $article_details['url_modele'], 'id_article' => $article_details['id_article'], 'url_article' => $article_details['url_article']));
		
		
		$id_article = explode("_", $id_complet);
				
				$recup_prix_query = tep_db_query("SELECT p.products_price, p.products_weight, ppbq.* FROM products_price_by_quantity ppbq, products p WHERE ppbq.products_id = '" . $id_article[0] . "' AND p.products_id = '" . $id_article[0] . "'");
				$recup_prix_data = tep_db_fetch_array($recup_prix_query);
			
				
			$infos_options_query = 'SELECT options_quantity
									FROM '. TABLE_PRODUCTS_ATTRIBUTES .' pa,
										 '. TABLE_PRODUCTS_OPTIONS .' po,
										 '. TABLE_PRODUCTS_OPTIONS_VALUES .' pov 
									WHERE pa.products_id=\''. $id_article[0] .'\'
									and pa.options_values_id=pov.products_options_values_id
									and pa.options_id=po.products_options_id
									and pa.options_dispo = \'1\'
									and pa.options_values_id=\''. $option_details_id_valeur .'\'
									and pa.options_id=\''. $option_details_id .'\'';
			$infos_options_query = tep_db_query($infos_options_query);
			
			$data_options = tep_db_fetch_array($infos_options_query);
			
			if(get_destock($article_details['products_manage_stock'])) $article_details['products_quantity'] = $data_options['options_quantity'];
			
		?>
		<td class="image_article">
			<a href="<?php echo $url; ?>">
				<img src="<?php echo BASE_DIR . "/images/products_pictures/micro/" . $article_details['image_article']; ?>" alt="Image" />
			</a>			
		</td>
		<td class="article">
        	<div style="float:left;">
        	<a href="<?php echo $url; ?>">
				<?php echo $article_details['nom_article'];  ?><br>
                <?php if (isset($option_details['id']) && $option_details['id'] != '') { ?>
                    <span class="options"><?php echo $option_details['nom'] . " : " . $option_details['nom_valeur']; ?></span>	
                <?php } ?>				
            </a>
			<br>
				<? // Thierry
				/*
				$poids_total= ($recup_prix_data['products_weight'] * $quantite);
				echo $poids_total . ' Kg';
				$poids_produits += ($recup_prix_data['products_weight'] * $quantite)
				*/
				//echo $quantite .'x' .$recup_prix_data['products_weight'] . '=' . $poids_total_produit; 
				?>
			</div>
			<div style="margin-left:50px;float:left;margin-top:5px;">
				<?php
				
				if ($article_details['prix_flash'] > 0) {
					echo '<span style="font-weight:bold;color:#8e2eaa;">VENTE FLASH</span>';
					echo '<br />';
					echo "<span style='text-decoration:line-through;'>".format_to_money($article_details['prix_ttc'])." &euro;</span>";
					echo "<br /><span'>".format_to_money($article_details['prix_flash'])." &euro;</span>";
				} elseif ($article_details['prix_promo'] > 0) {
					echo '<span style="font-weight:bold;color:#8e2eaa;">PROMO</span>';
					echo '<br />';
					echo "<span style='text-decoration:line-through;'>".format_to_money($article_details['prix_ttc'])." &euro;</span>";
					echo "<br /><span'>".format_to_money($article_details['prix_promo'])." &euro;</span>";
				}
				?>
			</div>
		</td>
		<td class="prix"><span id="prix_unit_<?php echo $i; ?>">
			
			<?php
				
			/* utile seulement pour le javascript : le prix hors taxe est d�j� calcul� dans infos_article()*/
			if ($_SESSION['customers_type'] == 3 && GESTION_PRIX_PAR_QUANTITE == 1) {
				$prix_10_HT = $recup_prix_data['rev_price_by_10'];
				$prix_5_HT = $recup_prix_data['rev_price_by_5'];
				$prix_1_HT = $recup_prix_data['rev_price_by_1'];
			} elseif ($_SESSION['customers_type'] == 1 && GESTION_PRIX_PAR_QUANTITE == 1 && $article_details['prix_promo']==0) {
				$prix_10_HT = $recup_prix_data['part_price_by_10'];
				$prix_5_HT = $recup_prix_data['part_price_by_5'];
				$prix_1_HT = $recup_prix_data['part_price_by_1'];
			} elseif ($_SESSION['customers_type'] == 2 && GESTION_PRIX_PAR_QUANTITE == 1 && $article_details['prix_promo']==0) {
				$prix_10_HT = $recup_prix_data['pro_price_by_10'];
				$prix_5_HT = $recup_prix_data['pro_price_by_5'];
				$prix_1_HT = $recup_prix_data['pro_price_by_1'];
			} elseif ($_SESSION['customers_type'] == 4 && GESTION_PRIX_PAR_QUANTITE == 1 && $article_details['prix_promo']==0) {
				$prix_10_HT = $recup_prix_data['adm_price_by_10'];
				$prix_5_HT = $recup_prix_data['adm_price_by_5'];
				$prix_1_HT = $recup_prix_data['adm_price_by_1'];
			} else {
				$prix_1_HT = $article_details['prix_ttc']/1.2;
				$prix_5_HT = $article_details['prix_ttc']/1.2;
				$prix_10_HT = $article_details['prix_ttc']/1.2;
			}
				
			if ($_SESSION['customers_type'] == 3 && GESTION_PRIX_PAR_QUANTITE == 1) {	
				echo format_to_money($article_details['prix_ht']);
			} elseif ($article_details['prix_flash'] > 0) {
				echo format_to_money($article_details['prix_flash']);
			} elseif ($article_details['prix_promo'] > 0) {
				echo format_to_money($article_details['prix_promo']);
			} else {
				echo format_to_money($article_details['prix_ht']*$article_details['tva']);
			}
			?>
        </span> &euro;</td>
        <?php echo "<input type='hidden' id='prix_1_" . $i . "' value='" . $prix_1_HT . "' /><input type='hidden' id='prix_5_" . $i . "' value='" . $prix_5_HT . "' /><input type='hidden' id='prix_10_" . $i . "' value='" . $prix_10_HT . "' />"; ?>
		<td class="quantite">
        	<?php if ($modifiable) { ?>
            
        	<div class="quantite_2"><input id="qte_<?php echo $i; ?>" disabled="disabled" type="text" name="quantite" value="<?php echo $quantite; ?>" size="1" onblur="<?php if(!empty($_SESSION['customer_id'])) echo "modifie_quantite_panier_log('qte_".$i."', '".$id_complet."', 'article');"; ?> prix_tot('prix_total_<?php echo $i; ?>', 'prix_unit_<?php echo $i; ?>', 'qte_<?php echo $i; ?>', '<?php echo $id_complet; ?>', 'article', '<?php echo $article_details['tva']; ?>', '<?php echo $tva; ?>');" />
                <div class="signe">
                    <input type="image" 
					src="template/base/panier/icone_plus.jpg" 
					alt="+" 
					onclick="plus('qte_<?php echo $i; ?>','<?php echo (get_destock($article_details['products_manage_stock'])) ? $article_details['products_quantity'] : 0;?>'); <?php if(!empty($_SESSION['customer_id'])) echo "modifie_quantite_panier_log('qte_".$i."', '".$id_complet."', 'article')"; ?>; prix_tot('prix_total_<?php echo $i; ?>', 'prix_unit_<?php echo $i; ?>', 'qte_<?php echo $i; ?>', '<?php echo $id_complet; ?>', 'article', '<?php echo $article_details['tva']; ?>', '<?php echo $tva; ?>');" />
					
					<br />
                    <input type="image" src="template/base/panier/icone_moins.jpg" alt="-" onclick="moins('qte_<?php echo $i; ?>'); <?php if(!empty($_SESSION['customer_id'])) echo "modifie_quantite_panier_log('qte_".$i."', '".$id_complet."', 'article');"; ?>  prix_tot('prix_total_<?php echo $i; ?>', 'prix_unit_<?php echo $i; ?>', 'qte_<?php echo $i; ?>', '<?php echo $id_complet; ?>', 'article', '<?php echo $article_details['tva']; ?>', '<?php echo $tva; ?>');" />
                </div>
            </div>
            <?php } else {
            echo $quantite;
            } ?>
        </td>
        <td class="prix_total"><span id="prix_total_<?php echo $i; ?>">
			<?php				
				if ($_SESSION['customers_type'] == 3 && GESTION_PRIX_PAR_QUANTITE == 1) {
					echo format_to_money($article_details['prix_ht']*$quantite);
				} elseif($article_details['prix_flash'] > 0){
					echo format_to_money($article_details['prix_flash']*$quantite);
				} elseif($article_details['prix_promo'] > 0){
					echo format_to_money($article_details['prix_promo']*$quantite);
				} else {
					echo format_to_money($article_details['prix_ttc']*$quantite);
				}
			?>
          </span> &euro;</td>
        <?php if ($modifiable) { ?>
        	<td class="supprimer"><img src="template/base/panier/icone_supprimer.png" alt="Suppr"  onclick="suppr_article(<?php echo "'" . $id_complet . "'," . $i; ?>)"/></td>
        <?php } ?>
	</tr>
    <?php
}

// Affichage des packs du panier d'un client
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!supprimer id_rayon qui a priori sert a rien
function affiche_pack_panier($id_complet, $id_rayon, $pack_details, $quantite, $i, $modifiable) {
	//prise en compte de la zone tva pour affichage
	if (taux_taxe() == 1.2){$tva=1;}
	else $tva=0;
	?>
    <tr id="<?php echo $id_complet; ?>">
        <td class="image_article"><?php echo $pack_details['liste_images_articles']; ?></td>
        <td class="article"><?php echo $pack_details['liste_articles']; ?></td>
        <td class="prix"><?php $taxe = taux_taxe(); echo format_to_money($pack_details['prix_normal_ht'] * $taxe); ?> &euro;</td>
        <td class="prix"><span id="prix_unit_<?php echo $i; ?>"><?php echo format_to_money($pack_details['prix_ht'] * $taxe); ?></span> &euro;</td>
        <td class="prix_remise"><?php echo format_to_money($pack_details['remise'] * $taxe); ?> &euro;</td>
        <td class="quantite">
        <?php if ($modifiable) { 
			// Modification par Thierry le 19/04/2018
		    echo $quantite;
		?>
	    
			<!--<div class="quantite_2">			
				<input id="qte_<?php echo $i; ?>" type="text" name="quantite" value="<?php echo $quantite; ?>" size="1" onblur="<?php if(!empty($_SESSION['customer_id'])) echo "modifie_quantite_panier_log('qte_".$i."', '".$id_complet."', 'pack');"; ?> prix_tot('prix_total_<?php echo $i; ?>', 'prix_unit_<?php echo $i; ?>', 'qte_<?php echo $i; ?>', '<?php echo $id_complet; ?>', 'pack', '<?php echo $tva; ?>');" />
			
				<div class="signe">
					<input type="image" src="template/base/panier/icone_plus.jpg" alt="+" onclick="plus('qte_<?php echo $i; ?>','0'); <?php if(!empty($_SESSION['customer_id'])) echo "modifie_quantite_panier_log('qte_".$i."', '".$id_complet."', 'pack');"; ?> prix_tot('prix_total_<?php echo $i; ?>', 'prix_unit_<?php echo $i; ?>', 'qte_<?php echo $i; ?>', '<?php echo $id_complet; ?>', 'pack', '<?php echo $tva; ?>');" />
					<br />
					<input type="image" src="template/base/panier/icone_moins.jpg" alt="-" onclick="moins('qte_<?php echo $i; ?>'); <?php if(!empty($_SESSION['customer_id'])) echo "modifie_quantite_panier_log('qte_".$i."', '".$id_complet."', 'pack');"; ?> prix_tot('prix_total_<?php echo $i; ?>', 'prix_unit_<?php echo $i; ?>', 'qte_<?php echo $i; ?>', '<?php echo $id_complet; ?>', 'pack', '<?php echo $tva; ?>');" />
				</div>
			</div>-->
		</td>
		<?php } else {
			echo $quantite;
		} ?>
        <td class="prix_total"><span id="prix_total_<?php echo $i; ?>"><?php echo format_to_money(($pack_details['prix_ht']*$quantite)* $taxe); ?></span> &euro;</td>
        <?php if ($modifiable) { ?>
	        <td class="supprimer"><img src="template/base/panier/icone_supprimer.png" alt="Suppr"  onclick="suppr_pack(<?php echo "'" . $id_complet . "'," . $i; ?>)"/></td>
        <?php } ?>
    </tr>
<?php
}

// R�cup�re les infos sur les options d'un article ou d'un pack
function infos_options($options_id, $options_value_id) {
	
	$options_query = tep_db_query("SELECT products_options_name 
								   FROM " . TABLE_PRODUCTS_OPTIONS . "
								   WHERE products_options_id = '" . $options_id . "'");
	$options_data = tep_db_fetch_array($options_query);
	$nom_option = $options_data['products_options_name'];
	
	$options_value_query = tep_db_query("SELECT products_options_values_name 
										 FROM " . TABLE_PRODUCTS_OPTIONS_VALUES . "
										 WHERE products_options_values_id = '" . $options_value_id . "'");
	$options_value_data = tep_db_fetch_array($options_value_query);
	$nom_valeur = $options_value_data['products_options_values_name'];
	
	// Retourne un tableau index� avec les infos des options
	return array('id' => $options_id, 'nom' => $nom_option, 'id_valeur' => $options_value_id, 'nom_valeur' => $nom_valeur);
}

// R�cup�re les infos d'un article
function infos_article($article, $quantite) {
	// On r�cup�re le taux de la taxe
	$taxe = taux_taxe();
	
	// On r�cup�re les infos sur le mod�le de t�l�phone
	$id_modele_tel = $article[2];
	$modele_query = tep_db_query("SELECT categories_name, categories_url, categories_id 
								  FROM " . TABLE_CATEGORIES_DESCRIPTION . "
								  WHERE categories_id = '" . $id_modele_tel . "'");
	$modele_data = tep_db_fetch_array($modele_query);
	$nom_modele = $modele_data['categories_name'];
	$url_modele = $modele_data['categories_url'];
	$id_modele = $modele_data['categories_id'];
	
	// On r�cup�re les infos sur la marque
	$id_marque = $article[1];
	$marque_query = tep_db_query("SELECT categories_name, categories_url 
								  FROM " . TABLE_CATEGORIES_DESCRIPTION . "
								  WHERE categories_id = '" . $id_marque . "'");
	$marque_data = tep_db_fetch_array($marque_query);
	$nom_marque = $marque_data['categories_name'];
	$url_marque = $marque_data['categories_url'];
	
	// On r�cup�re les infos sur l'article
	$id_article = $article[0];
	
	$article_query = tep_db_query("SELECT pd.products_name, pd.products_url, p.products_price, p.products_bimage, p.products_manage_stock, p.products_quantity,
								  ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1,
								  ppbq.part_price_by_5, ppbq.pro_price_by_5, ppbq.rev_price_by_5, ppbq.adm_price_by_5,
								  ppbq.part_price_by_10, ppbq.pro_price_by_10, ppbq.rev_price_by_10, ppbq.adm_price_by_10,
								  s.specials_new_products_price, s.expires_date,
								  pvf.prix_vente
								   FROM " . TABLE_PRODUCTS . " as p
								   left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id
								   left join " . TABLE_SPECIALS . " s on (s.products_id = p.products_id and (expires_date >NOW() or expires_date='0000-00-00 00:00:00'))
								   left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash>NOW() or fin_vente_flash='0000-00-00'))),
								   " . TABLE_PRODUCTS_DESCRIPTION . " as pd
								   WHERE pd.products_id = '" . $id_article . "' and pd.products_id = p.products_id");
	$article_data = tep_db_fetch_array($article_query);
	
	// On r�cup�re les prix par quantit�	
	$nom_article = $article_data['products_name'];
	$nom_article = bda_product_name_transform($nom_article, $id_modele);
	$url_article = $article_data['products_url'];
	$url_article = bda_product_name_transform($url_article, $id_modele);
	
	$image_article = $article_data['products_bimage'];
	$promotion = promo_product($id_article);
	$prix = calculate_price_for_product($article_data);
	
	if($prix['flash'] > 0) {		
		$prix_HT = $prix['HT'];
	} elseif($prix['promo'] > 0) {		
		$prix_HT = $prix['HT'];
	//on v�rifie que les prix par quantit� existent
	} elseif(!empty($article_data['part_price_by_1']) && $article_data['part_price_by_1']!="0.00") {
		
		if ($quantite >= 10 && GESTION_PRIX_PAR_QUANTITE == 1) {			
			if ($_SESSION['customers_type'] == 1) $prix_HT = $article_data['part_price_by_10'];
			elseif ($_SESSION['customers_type'] == 2) $prix_HT = $article_data['pro_price_by_10'];
			elseif ($_SESSION['customers_type'] == 3) $prix_HT = $article_data['rev_price_by_10'];
			elseif ($_SESSION['customers_type'] == 4) $prix_HT = $article_data['adm_price_by_10'];
			else $prix_HT = $article_data['products_price'];
		} elseif ($quantite >= 5 && GESTION_PRIX_PAR_QUANTITE == 1) {			
			if ($_SESSION['customers_type'] == 1) $prix_HT = $article_data['part_price_by_5'];
			elseif ($_SESSION['customers_type'] == 2) $prix_HT = $article_data['pro_price_by_5'];
			else if ($_SESSION['customers_type'] == 3) $prix_HT = $article_data['rev_price_by_5'];
			else if ($_SESSION['customers_type'] == 4) $prix_HT = $article_data['adm_price_by_5'];
			else $prix_HT = $article_data['products_price'];
		} elseif ($quantite >= 1 && GESTION_PRIX_PAR_QUANTITE == 1) {				
			if ($_SESSION['customers_type'] == 1) $prix_HT = $article_data['part_price_by_1'];
			elseif ($_SESSION['customers_type'] == 2) $prix_HT = $article_data['pro_price_by_1'];
			elseif ($_SESSION['customers_type'] == 3) $prix_HT = $article_data['rev_price_by_1'];
			elseif ($_SESSION['customers_type'] == 4) $prix_HT = $article_data['adm_price_by_1'];
			else $prix_HT = $article_data['products_price'];
		}
	} else {
		$prix_HT = $article_data['products_price'];
	}
	
	if($prix_HT == 0 || $prix_HT=='') $prix_HT = $article_data['products_price'];
	
	$prix_article = $prix_HT * $taxe;
	
	// On retourne le tout dans un tableau index�	
	return array('id_article' => $id_article, 'nom_article' => $nom_article, 'url_article' => $url_article, 'image_article' => $image_article, 'id_marque' => $id_marque, 'nom_marque' => $nom_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele_tel, 'nom_modele' => $nom_modele, 'url_modele' => $url_modele, 'products_quantity' => $article_data['products_quantity'], 'products_manage_stock' => $article_data['products_manage_stock'], 'prix_ht' => $prix_HT, 'prix_ttc' => $prix['normal'], 'prix_promo' => $prix['promo'], 'prix_flash' => $prix['flash'], 'tva' => $prix['tva']);
}

// R�cup�re les infos d'un pack
function info_pack($pack, $option_details) {
	// On r�cup�re les diff�rents id
	$id_pack   = $pack[0];
	$id_marque = $pack[1];
	$id_modele = $pack[2];
	
	// On r�cup�re les infos sur le modele
	$modele_query = tep_db_query("SELECT categories_name, categories_url 
								  FROM " . TABLE_CATEGORIES_DESCRIPTION . "
								  WHERE categories_id = '" . $id_modele . "'");
	$modele_data = tep_db_fetch_array($modele_query);
	$nom_modele = $modele_data['categories_name'];
	$url_modele = $modele_data['categories_url'];
	
	// On r�cup�re les infos sur la marque
	$marque_query = tep_db_query("SELECT categories_name, categories_url 
								  FROM " . TABLE_CATEGORIES_DESCRIPTION . "
								  WHERE categories_id = '" . $id_marque . "'");
	$marque_data = tep_db_fetch_array($marque_query);
	$nom_marque = $marque_data['categories_name'];
	$url_marque = $marque_data['categories_url'];
	
	// On r�cup�re les infos sur les articles du pack
	
	$prix_normal_HT = 0;
	$taxe = taux_taxe();
	$liste_articles_pack = '';
	$liste_images_articles_pack = '';
	$i = 0;
	
	$pack_query = tep_db_query("SELECT pp.products_id, pp.products_id_1, pp.products_id_2, pp.pack_remise
										 FROM " . TABLE_PACK . " as pp
										 WHERE pp.pack_id = " . $id_pack );
	
	while ($pack_data = tep_db_fetch_array($pack_query)) {
		$remise = $pack_data['pack_remise'];
		$retour=info_pack_produit($id_marque, $url_marque, $id_modele, $url_modele, $nom_modele, $pack_data['products_id'], $option_details);
		$prix_normal_HT+=$retour[0];
		$liste_images_articles_pack.=$retour[1];
		$liste_articles_pack.=$retour[2];
		if($pack_data['products_id_2']>0){
			$retour=info_pack_produit($id_marque, $url_marque, $id_modele, $url_modele, $nom_modele, $pack_data['products_id_1'], '');
			$prix_normal_HT+=$retour[0];
			$liste_images_articles_pack .= ' <br><img src="' . BASE_DIR . '/template/base/fiche_article/plus_pack.png" alt="+" width="15"/><br> ';
			$liste_images_articles_pack.=$retour[1];
			$liste_articles_pack .= ' <br><img src="' . BASE_DIR . '/template/base/fiche_article/plus_pack.png" alt="+" width="15"/><br> ';
			$liste_articles_pack.=$retour[2];
			
			$retour=info_pack_produit($id_marque, $url_marque, $id_modele, $url_modele, $nom_modele, $pack_data['products_id_2'], '', true);
			$prix_normal_HT+=$retour[0];
			$liste_images_articles_pack .= ' <br><img src="' . BASE_DIR . '/template/base/fiche_article/plus_pack.png" alt="+" width="15"/><br> ';
			$liste_images_articles_pack.=$retour[1];
			$liste_articles_pack .= ' <br><img src="' . BASE_DIR . '/template/base/fiche_article/plus_pack.png" alt="+" width="15"/><br> ';
			$liste_articles_pack.=$retour[2];
		} else{
			$retour=info_pack_produit($id_marque, $url_marque, $id_modele, $url_modele, $nom_modele, $pack_data['products_id_1'], '', true);
			$prix_normal_HT+=$retour[0];
			$liste_images_articles_pack .= ' <br><img src="' . BASE_DIR . '/template/base/fiche_article/plus_pack.png" alt="+" width="15"/> ';
			$liste_images_articles_pack.=$retour[1];
			$liste_articles_pack .= ' <br><img src="' . BASE_DIR . '/template/base/fiche_article/plus_pack.png" alt="+" width="15"/><br> ';
			$liste_articles_pack.=$retour[2];			
		}
	}
	
	// On en d�duit le prix du pack
	$prix_pack_HT = $prix_normal_HT - $remise;
	
	// On retourne le tout dans un tableau index�
	return array('id_pack' => $id_pack, 'id_marque' => $id_marque, 'id_modele' => $id_modele, 'nom_modele' => $nom_modele, 'url_modele' => $url_modele, 'prix_normal_ht' => $prix_normal_HT, 'prix_ht' => $prix_pack_HT, 'remise' => $remise, 'liste_articles' => $liste_articles_pack, 'liste_images_articles' => $liste_images_articles_pack);
}

function info_pack_produit($id_marque, $url_marque, $id_modele, $url_modele, $nom_modele, $products_id, $option_details, $dernier=false) {
	
	$articles_pack_query = tep_db_query("SELECT pd.products_name, pd.products_url, pd.products_id, p.products_bimage, p.products_price, pd.products_id 
										 FROM " . TABLE_PRODUCTS_DESCRIPTION . " as pd, " . TABLE_PRODUCTS . " as p
										 WHERE pd.products_id='".$products_id."' and pd.products_id = p.products_id");
	$articles_pack_data = tep_db_fetch_array($articles_pack_query);
	$prix_HT = $articles_pack_data['products_price'];
	$retour[0] = $prix_HT;
	
	$nom_article = $articles_pack_data['products_name'];
	$nom_article = bda_product_name_transform($nom_article, $id_modele);
	$url_article = $articles_pack_data['products_url'];
	$url_article = bda_product_name_transform($url_article, $id_modele);
	
	$id_article = $articles_pack_data['products_id'];
	
	if (isset($option_details['id']) && $option_details['id'] != '') {
		$option = '<span class="options"><br />' . $option_details['nom'] . ' : ' . $option_details['nom_valeur'] . '</span></a><br /><br />';
	} else if ($dernier) {
		$option = '</a><br /><br /><br />';
	} else {
		$option = '</a>';
	}
	
	$retour[1]= '<img src="' . BASE_DIR . '/images/products_pictures/micro/' . $articles_pack_data['products_bimage'] . '" alt="Image" />';
	$url = url('article', array('id_rayon' => 'id_rayon_a_remplir', 'nom_rayon' => 'nom_rayon_a_remplir', 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele, 'url_modele' => $url_modele, 'id_article' => $id_article, 'url_article' => $url_article));
	// ancienne URL qui ne marche pas avec tous les produits
	//$retour[2]= '<a href="' . $url . '">' . $nom_article . $option;
	// Modification de Thierry le 19/04/2018
	$retour[2]= '<a href="https://www.generalarmystore.fr/gas/produits_liste.php?recherche=' . $nom_article . '">' . $nom_article . $option;
	
	return $retour;	
}

function panier_affichage_final(){
	
	$panier_query = tep_db_query("SELECT cb.products_id, cb.customers_basket_quantity, cb.id_rayon
	FROM ".TABLE_CUSTOMERS_BASKET." as cb
	WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' and cb.pack_id = ''");

	$i = 0;
	$nb_articles = tep_db_num_rows($panier_query);
	
	if ($nb_articles > 0) {   
	?>
		<table id="table_articles">
			<thead>
				<tr>
					<th colspan="2">Articles</th>
					<th>Prix Unitaire</th>
					<th>Quantit&eacute;</th>
					<th>Prix total</th>
				</tr>
			</thead>
			<tbody>
			<?php
			while($panier_data = tep_db_fetch_array($panier_query)){
				$id_complet = $panier_data['products_id'];
				$i++;

				// On r�cup�re les infos sur les options
				$article_options = explode("{", $panier_data['products_id']);
				$options = explode("}", $article_options[1]);
				$option_details = infos_options($options[0], $options[1]);
											
				// On r�cup�re les infos sur l'article
				$article = explode("_", $article_options[0]);
				$quantite = $panier_data['customers_basket_quantity'];
				$article_details = infos_article($article, $quantite);
											
				// On r�cup�re des infos compl�mentaires
				echo $articles_sous_total_HT += $article_details['prix_ht']*$quantite;

				// On affiche les articles du panier
				affiche_article_panier($nb_articles, $id_complet, $panier_data['id_rayon'], $article_details, $option_details, $quantite, $i, false);
			}
	} 
	?>
			</tbody>
		</table>
            
		<?php
		$pack_sous_total_HT = 0;
		$packs_query = tep_db_query("SELECT cb.pack_id, cb.customers_basket_quantity, cb.id_rayon
									FROM ".TABLE_CUSTOMERS_BASKET." as cb
									WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' and cb.pack_id <> ''");
		$nb_packs = tep_db_num_rows($packs_query);
		if ($nb_packs > 0) {
		?>
		<!-- Tableau contenant les Packs choisis par le client -->
		<table id="table_packs">
			<thead>
				<tr>
					<th colspan="2">Packs</th>
					<th>Prix Normal</th>
					<th>Prix du Pack</th>
					<th>Economie R&eacute;alis&eacute;e</th>
					<th>Quantit&eacute;</th>
					<th>Prix total</th>
				</tr>
			</thead>
			<tbody>
                
			<?php
			while($packs_data = tep_db_fetch_array($packs_query)){
				$id_complet = $packs_data['pack_id'];
				$i++;
												
				// On r�cup�re les infos sur les options
				$pack_options = explode("{", $id_complet);
				$options = explode("}", $pack_options[1]);
				$option_details = infos_options($options[0], $options[1]);
												
				// On r�cup�re les infos sur le pack
				$pack = explode("_", $pack_options[0]);
				$pack_details = info_pack($pack, $option_details);

				// On r�cup�re des infos compl�mentaires
				$quantite = $packs_data['customers_basket_quantity'];
				echo $pack_sous_total_HT += $pack_details['prix_ht']*$quantite;

				//On affiche le pack et la liste de ses articles
				affiche_pack_panier($id_complet, $packs_data['id_rayon'], $pack_details, $quantite, $i, false);
			}
		}
		?>
			</tbody>
		</table> 
		<?php
}

function prix_livraison_par_adresse($id_pays, $poids_panier) {
	//if (!empty($id_pays) || !empty($poids_panier) {
		
		$id_address=tep_db_query("select countries_name, countries_iso_code_2, countries_id_zone_livraison
								  from ".TABLE_COUNTRIES." c 
								  where countries_id=".$id_pays."");
								  
		while($res_id=tep_db_fetch_array($id_address)){ 
	  
		   $query = tep_db_query("	SELECT prix_livraison
									FROM ".TABLE_MODULE_LIVRAISON."
									WHERE zone_livraison_id=". $res_id['countries_id_zone_livraison'] ."
									AND poids_livraison >= ". $poids_panier);
			
			if (tep_db_num_rows($query)>0) {
				$data = tep_db_fetch_array($query);
				if($poids_panier<=120){
					return $prix = round($data['prix_livraison']*parametrage_affiche_prix($id_pays),2);
				}  else { echo 'Contactez-nous'; }
				
			} else { 
				return ''; 
			}
		}
	//}
}

?>