<?php /* Module g�rant l'affichage du contenu du panier dans la banni�re */ ?>


	<?php
	// Initialisations
	$nb_articles = 0;
	$nb_packs = 0;
	$montant_panier = 0;
	$taxe = taux_taxe();
	
	// Si l'utilisateur est logu�
	if (isset($_SESSION['customer_id']) && !empty($_SESSION['customer_id'])) {
		$query_panier="SELECT cb.customers_basket_quantity, cb.products_id, cb.nb_articles_pack, cb.final_price 
					   FROM " . TABLE_CUSTOMERS_BASKET . " cb 
					   WHERE cb.customers_id = " . $_SESSION['customer_id'] . "";
		$nb_articles_query = tep_db_query($query_panier);
		// On compte les articles pr�sents dans son panier de la base de donn�es
		while ($nb_articles_data = tep_db_fetch_array($nb_articles_query)) {
			if ($nb_articles_data['products_id'] != '') {
				$nb_articles += $nb_articles_data['customers_basket_quantity'];
				$montant_panier += $nb_articles_data['final_price'] * $nb_articles_data['customers_basket_quantity'] * $taxe;
			} else {
				$nb_packs += $nb_articles_data['customers_basket_quantity'];
				$nb_articles += $nb_articles_data['customers_basket_quantity'] * $nb_articles_data['nb_articles_pack'];
				$montant_panier += $nb_articles_data['final_price'] * $nb_articles_data['customers_basket_quantity'] * $taxe;
			}
		}
	// Sinon
	} else {
		// On compte les articles de la session
		if (isset($_SESSION['panier'])) {
			foreach ($_SESSION['panier'] as $article) {
				$nb_articles += $article[1];
				$montant_panier += $article[3] * $article[1] * $taxe;
			}
		}
		// On compte les packs de la session
		if (isset($_SESSION['panier_packs'])) {
			foreach ($_SESSION['panier_packs'] as $session_pack) {
				$nb_packs += $session_pack[1];
				$nb_articles += $session_pack[1] * $session_pack[3];
				$montant_panier += $session_pack[4] * $session_pack[1] * $taxe;
			}
		}
	}
	
	// G�n�ration des "s" du pluriel
	$articles_pluriel = '';
	$packs_pluriel = '';
	if ($nb_articles > 1) { $articles_pluriel = 's'; }
	if ($nb_packs > 1) { $packs_pluriel = 's'; }
	?>

<div class="nb_articles_panier">

    <div class="titre" onclick="location.href='<?php echo BASE_DIR . "/panier.php";?>';">
		PANIER
	</div>
    
    <div class="texte" onclick="location.href='<?php echo BASE_DIR . "/panier.php";?>';">
	<?php
		if ($nb_articles < 1) {
			//echo "<span class='aucun_article'><br />Vous n'avez pas encore d'article dans votre panier</span>";
			echo "<strong>vide</strong>";
		} else {
			echo "<strong>" . $nb_articles . " article" . $articles_pluriel . "</strong>";
			
			if ($nb_packs > 0) {
				echo " dont " . $nb_packs . " Pack" . $packs_pluriel;
			}
		}
	?>
    </div>
	<? //if ($_SESSION['customer_id']==3) { ?>
	<div class="recherche">
		<form action="<?php echo BASE_DIR; ?>/produits_liste.php" method="get">
			<input name="recherche" type="text" size="20" placeholder="Rechercher" value="<?php if (!empty($_GET['recherche'])) { echo $_GET["recherche"]; } ?>"/>&nbsp;&nbsp;<input type="image" src="<?php echo BASE_DIR; ?>/template/base/icones/loupe-01.png" alt="Rechercher" class="new_recherche_submit"/>
		</form>
	</div>
    <? //} ?>
</div>
