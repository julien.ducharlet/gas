<?php
/* Auteur : Paul -> récupération d'une contrib OSCommerce
   Module permettant d'afficher le ParseTime et le nombre de requêtes (profiling)*/
   
	$time_start = explode(' ', PAGE_PARSE_START_TIME);
	$time_end = explode(' ', microtime());
	$parse_time = number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3);

	echo '<br /><div align="center"><span class="smallText" style="color:#000000">Current Parse Time: <b>' . $parse_time . ' s</b> with <b>' . sizeof($debug['QUERIES']) . ' queries</b></span></div>';
    
	unset($debug);
?>
