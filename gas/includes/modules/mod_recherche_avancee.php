<?php
/* Module affichant la banni�re de promo et permettant d'effectuer une recherche avanc�e sur le rayon, la marque et le mod�le de l'article recherch�.
   Le module est pr�sent sur les pages categories_list.php, modele_list.php et produits_list.php */
?>

<script type="text/javascript" language="javascript">
<!--

	// Redirection du module de recherche avanc�e
	function redirect($niveau) {
		var url_rayon = document.getElementById("rayon_recherche").value;
		var url_marque = document.getElementById("marque_recherche").value;
		if ($niveau == 'rayon') {
			location.href= url_rayon;
		} else if ($niveau == 'marque') {
			location.href= url_marque;
		} else if ($niveau == 'modele') {
			var url_modele = document.getElementById("modele_recherche").value;
			location.href= url_modele;
		}
	}

// -->
</script>

<div id="cadre_promo">
<?php
// S�lection de la bani�re de pub en fonction de la marque 				$_REQUEST['modele']
if (isset($_REQUEST['modele'])) {
	$baniere_query = tep_db_query("SELECT categories_id, categories_header, categories_header_url
								   FROM categories
								   WHERE categories_id = '" . (int)$_GET['modele'] . "'");
	$baniere_data = tep_db_fetch_array($baniere_query);
} else {
	if (isset($_GET['marque'])) {
		$baniere_query = tep_db_query("SELECT categories_id, categories_header, categories_header_url
									   FROM categories
									   WHERE categories_id = '" . (int)$_GET['marque'] . "'");
		$baniere_data = tep_db_fetch_array($baniere_query);
	} else {
		$baniere_query = tep_db_query("SELECT rubrique_header as categories_header
									   FROM rubrique
									   WHERE rubrique_id = '" . (int)$_GET['rayon'] . "'");
		$baniere_data = tep_db_fetch_array($baniere_query);
	}
}

if (!empty($baniere_data['categories_header_url'])) {
	echo '<a href="'.$baniere_data['categories_header_url'].'">';
} 
?>
<img src="<?php echo BASE_DIR; ?>/images/categories_header_pictures/<?php echo $baniere_data['categories_header']; ?>" alt="Banni�re promo" />
<?php 
if (!empty($baniere_data['categories_header_url'])) { echo '</a>';
} 
?>

</div>

<div id="cadre_recherche">
	<!-- classe css imitant le field-set qui n'est plus valide -->
    <div class="fieldnotset">
    	<p class="legend"><span>Effectuer une recherche avanc&eacute;e</span></p>
    	<div class="clear">
            <form action="">
                <select name="recherche" id="rayon_recherche" onchange="redirect('rayon');">
                    <?php
                        $reponse = tep_db_query("	SELECT rubrique_name, rubrique_id, rubrique_url
													FROM rubrique where rubrique_status='1'
													ORDER BY rubrique_id");
                        while($donnees = tep_db_fetch_array($reponse)){
							$url = url('rayon', array('id_rayon' => $donnees['rubrique_id'], 'nom_rayon' => $donnees['rubrique_url']));
                    		?>
                    		<option <?php if ($donnees['rubrique_id'] == $_GET['rayon']) echo "id=\"option_select\" selected=\"selected\""; ?> value="<?php echo $url; ?>"><?php echo $donnees['rubrique_name']; ?></option>
                    		<?php
						}
					?>
                </select><br />
                <select name="recherche" id="marque_recherche" onchange="redirect('marque');">
                    <option value=''>S&eacute;lectionnez</option>
                    <?php
					
                    $reponse2 = tep_db_query("SELECT cd.categories_name, cd.categories_id, cd.categories_url, r.rubrique_name, c.categories_status, c.sort_order,  
											 		 c.categories_status_tous, c.categories_status_client, c.categories_status_rev, c.categories_status_pro, c.categories_status_adm, c.categories_status_vip
											  FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id inner join categories_rubrique as cr on c.categories_id=cr.categories_id, rubrique as r
											  WHERE c.parent_id='0' AND c.sort_order > 0 AND cr.rubrique_id = " . (int)$_GET['rayon'] . " AND cr.rubrique_id = r.rubrique_id AND c.categories_status='1'
											  ORDER BY c.sort_order");
                    while($donnees2 = tep_db_fetch_array($reponse2)){
						
						if(affiche_info(	$session_customer_id, 
											$session_customers_type, 
											$session_customers_vip,
											$donnees2['categories_status_tous'], 
											$donnees2['categories_status_client'], 
											$donnees2['categories_status_rev'], 
											$donnees2['categories_status_pro'], 
											$donnees2['categories_status_adm'], 
											$donnees2['categories_status_vip']
										)
									){
							$url = url('marque', array('id_rayon' => $_GET['rayon'], 'nom_rayon' => $donnees2['rubrique_name'], 'id_marque' => $donnees2['categories_id'], 'url_marque' => $donnees2['categories_url']));
							
							if (empty($_GET["marque"])) {
								echo '<option value="'. $url .'">' .$donnees2['categories_name'] .'</option>';
							} else {
							?>
							<option <?php if ($donnees2['categories_id'] == $_GET['marque']) echo "selected=\"selected\""; ?> value="<?php echo $url; ?>"><?php echo $donnees2['categories_name']; ?></option>
							<?php
							}
						}
                    }
                    ?>
                </select><br />
                <?php if (isset($_GET['marque']) && $_GET['marque'] != '') {?>
                <select name="recherche" id="modele_recherche" onchange="redirect('modele');">
                    <option value=''>S&eacute;lectionnez</option>
                    <?php
                    $modele_query = tep_db_query("SELECT cd.categories_name, cd.categories_url, cd.categories_id, cd_marque.categories_url as url_marque, r.rubrique_name, r.rubrique_url, c.sort_order, 
												 		 categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, categories_status_adm, categories_status_vip
                                                  FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id, categories_description cd_marque, rubrique r  
                                                  WHERE c.parent_id='". (int)$_GET['marque'] ."' AND cd_marque.categories_id = '". (int)$_GET['marque'] ."' AND r.rubrique_id = '". (int)$_GET['rayon'] ."' and c.categories_status='1' 
                                                  ORDER BY c.sort_order");
                    while($modele_data = tep_db_fetch_array($modele_query)){
						
						if (affiche_info(	$session_customer_id, 
											$session_customers_type, 
											$session_customers_vip,
											$modele_data['categories_status_tous'], 
											$modele_data['categories_status_client'], 
											$modele_data['categories_status_rev'], 
											$modele_data['categories_status_pro'], 
											$modele_data['categories_status_adm'], 
											$modele_data['categories_status_vip']
										)
									){
							$url = url('modele', array('id_rayon' => $_GET['rayon'], 'nom_rayon' => $modele_data['rubrique_url'], 'id_marque' => $_GET['marque'], 'url_marque' => $modele_data['url_marque'], 'id_modele' => $modele_data['categories_id'], 'url_modele' => $modele_data['categories_url']));
                    		
							if (empty($_GET["modele"])) {
								echo '<option value="'. $url .'">' .$modele_data['categories_name'] .'</option>';
							} else {
							?>
                        	<option <?php if ($modele_data['categories_id'] == $_GET['modele']) echo "selected=\"selected\""; ?> value="<?php echo $url; ?>"><?php echo $modele_data['categories_name']; ?></option>
                    		<?php
							}
                    	}
					}
                    ?>
                </select> 
                <?php } ?>               
            </form>
    	</div>
	</div>
	<?php //print_r($_GET); 
	//echo '<br><br>';
	//print_r($_SESSION); 
	//echo $nom_page;
	?>
</div>

<div style="clear:both;"></div>