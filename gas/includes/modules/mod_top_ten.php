<?php 
/* 
   Module se chargeant d'afficher les tops_ten suivant la marque ou l'on se trouve (par d�faut de toutes les marques).
   Les tops_tens correspondent au tableau des 10 derniers articles de la cat�gorie et aux 10 articles les plus recherch�s dans la cat�gorie 
*/

if ($session_customers_vip==1) {
	$query_custom_type = ' and marque.categories_status_vip=\'1\' and modele.categories_status_vip=\'1\'';
	$query_rubrique_custom_type = ' and rayon_status_vip=\'1\'';
} else {
	switch($_SESSION['customers_type']) {
		
		case '1' :
			$query_custom_type = ' and marque.categories_status_client=\'1\' and modele.categories_status_client=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
			break;
		
		case '2' :
			$query_custom_type = ' and marque.categories_status_pro=\'1\' and modele.categories_status_pro=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_pro=\'1\'';
			break;
		
		case '3' :
			$query_custom_type = ' and marque.categories_status_rev=\'1\' and modele.categories_status_rev=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_rev=\'1\'';
			break;
		
		case '4' :
			$query_custom_type = ' and marque.categories_status_adm=\'1\' and modele.categories_status_adm=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_adm=\'1\'';
			break;
		
		default :
			$query_custom_type = ' and marque.categories_status_client=\'1\' and modele.categories_status_client=\'1\'';
			$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
			break;
	}
}

	//Si on est dans une marque en particulier on r�cup�re les tops_ten de la marque sinon les tops_ten du site
	if (isset($_GET['marque'])) {
		
		$top_ten_query = tep_db_query("SELECT distinct p.products_id, pd.products_name, pd.products_url, r.rubrique_id, r.rubrique_name, r.rubrique_url, marque_desc.categories_id as id_marque, marque_desc.categories_name as nom_marque, marque_desc.categories_url as url_marque, modele_desc.categories_id as id_modele, modele_desc.categories_name as nom_modele, modele_desc.categories_url as url_modele 
									   FROM " . TABLE_PRODUCTS . " p,
									   " . TABLE_PRODUCTS_DESCRIPTION . " pd,
									   " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c,
									    " . TABLE_CATEGORIES . " marque,
									   " . TABLE_CATEGORIES . " modele,
									   " . TABLE_RAYON . " r,
									   " . TABLE_CATEGORIES_DESCRIPTION . " marque_desc,
									   " . TABLE_CATEGORIES_DESCRIPTION . " modele_desc 
									   
									   WHERE p.products_status = '1'
									   and p.products_ordered > 0
									   and p.products_id = pd.products_id
									   and p.products_id = p2c.products_id
									   and p2c.categories_id = modele.categories_id
									   and modele.parent_id=marque.categories_id
									   and marque.categories_id = '" . (int)$_GET['marque'] . "'
									   AND r.rubrique_id = '" . (int)$_GET['rayon'] . "'
									   AND marque_desc.categories_id = '" . (int)$_GET['marque'] . "'
									   AND p2c.categories_id = modele_desc.categories_id
									   ". $query_custom_type . $query_rubrique_custom_type ."
									   
									   ORDER BY p.products_ordered DESC, pd.products_name 
									   LIMIT 0,10");
		
		$requete_new = "SELECT distinct(pd.products_id), pd.products_name, pd.products_url
                        
						FROM " . TABLE_PRODUCTS_TO_CATEGORIES . " pc,
						". TABLE_PRODUCTS ." p,
						". TABLE_PRODUCTS_DESCRIPTION ." pd,
						". TABLE_CATEGORIES ." marque,
						". TABLE_CATEGORIES ." modele,
						". TABLE_CATEGORIES_RAYON ." cr,
                        ". TABLE_RAYON ." r
						
						WHERE products_status = 1
						and p.products_id=pd.products_id
						and p.products_id=pc.products_id
						and modele.parent_id = marque.categories_id
						and pc.categories_id = modele.categories_id
						and marque.categories_id=". (int)$_GET['marque'] ."
						and marque.categories_id=cr.categories_id
						and cr.rubrique_id=r.rubrique_id
						and modele.categories_status='1'
						". $query_custom_type . $query_rubrique_custom_type;
						
	}elseif (isset($_GET['rayon'])) {
		
		$top_ten_query = tep_db_query("SELECT distinct p.products_id,
									  pd.products_name,
									  pd.products_url,
									  r.rubrique_id,
									  r.rubrique_name,
									  r.rubrique_url,
									  marque_desc.categories_id as id_marque,
									  marque_desc.categories_name as nom_marque,
									  marque_desc.categories_url as url_marque,
									  modele_desc.categories_id as id_modele,
									  modele_desc.categories_name as nom_modele,
									  modele_desc.categories_url as url_modele 
									  
									  FROM " . TABLE_PRODUCTS . " p,
									  ". TABLE_PRODUCTS_DESCRIPTION . " pd,
									  " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c,
									  " . TABLE_CATEGORIES . " marque,
									  " . TABLE_RAYON . " r,
									  " . TABLE_CATEGORIES_DESCRIPTION . " marque_desc,
									  " . TABLE_CATEGORIES_DESCRIPTION . " modele_desc,
									  " . TABLE_CATEGORIES . " modele,
									  categories_rubrique cr 
									  
									  WHERE p.products_status = '1'
									  and p.products_ordered > 0
									  and p.products_id = pd.products_id
									  and p.products_id = p2c.products_id
									  and p2c.categories_id = modele.categories_id
									  and modele.parent_id = marque.categories_id
									  AND r.rubrique_id = '" . (int)$_GET['rayon'] . "'
									  AND marque_desc.categories_id = modele.parent_id
									  AND p2c.categories_id = modele_desc.categories_id
									  AND modele.categories_id = modele_desc.categories_id
									  AND cr.rubrique_id = '" . (int)$_GET['rayon'] . "'
									  AND cr.categories_id = modele.parent_id
									  ". $query_custom_type . $query_rubrique_custom_type ."
									  GROUP BY p.products_id
									  ORDER BY p.products_ordered DESC, pd.products_name 
									  LIMIT 0,10");
		
		$requete_new = "SELECT distinct(pd.products_id), pd.products_name, pd.products_url
                        
						FROM " . TABLE_CATEGORIES_RAYON ." cr,
						". TABLE_CATEGORIES ." marque,
						". TABLE_CATEGORIES ." modele,
						". TABLE_PRODUCTS_TO_CATEGORIES ." pc,
						". TABLE_PRODUCTS ." p,
						". TABLE_PRODUCTS_DESCRIPTION ." pd,
						". TABLE_RAYON ." r
                        
						WHERE products_status = 1
						and p.products_id=pd.products_id
						and p.products_id=pc.products_id
						and pc.categories_id=cr.categories_id
						and cr.rubrique_id=r.rubrique_id
						and cr.categories_id=marque.categories_id
						and modele.categories_id=pc.categories_id
						and cr.rubrique_id=" . $id_rayon . "
						". $query_custom_type . $query_rubrique_custom_type;
						
	}
	else {
		$top_ten_query = tep_db_query("SELECT distinct p.products_id,
									  pd.products_name,
									  pd.products_url,
									  r.rubrique_id,
									  r.rubrique_name,
									  r.rubrique_url,
									  marque_desc.categories_id as id_marque,
									  marque_desc.categories_name as nom_marque,
									  marque_desc.categories_url as url_marque,
									  modele_desc.categories_id as id_modele,
									  modele_desc.categories_name as nom_modele,
									  modele_desc.categories_url as url_modele
									  
									  FROM " . TABLE_PRODUCTS . " p,
									  " . TABLE_PRODUCTS_DESCRIPTION . " pd,
									  " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c,
									  " . TABLE_CATEGORIES . " marque,
									  " . TABLE_RAYON . " r,
									  " . TABLE_CATEGORIES_DESCRIPTION . " marque_desc,
									  " . TABLE_CATEGORIES_DESCRIPTION . " modele_desc,
									  " . TABLE_CATEGORIES . " modele, categories_rubrique cr 
									  
									  WHERE p.products_status = '1'
									  AND p.products_ordered > 0
									  AND p.products_id = pd.products_id
									  AND p.products_id = p2c.products_id
									  AND p2c.categories_id = modele.categories_id
									  and modele.parent_id=marque.categories_id
									  AND marque.categories_id = marque_desc.categories_id
									  AND marque_desc.categories_id = cr.categories_id
									  AND cr.rubrique_id = r.rubrique_id
									  AND modele.categories_id = modele_desc.categories_id 
									  ". $query_custom_type . $query_rubrique_custom_type ."
									  
									  GROUP BY p.products_id
									  ORDER BY p.products_ordered DESC, pd.products_name 
									  LIMIT 0,10");

		$requete_new = "SELECT distinct(pd.products_id), pd.products_name, pd.products_url
                        
						FROM " . TABLE_CATEGORIES_RAYON ." cr,
						". TABLE_CATEGORIES ." marque,
						". TABLE_CATEGORIES ." modele,
						". TABLE_PRODUCTS_TO_CATEGORIES ." pc,
						". TABLE_PRODUCTS ." p,
						". TABLE_PRODUCTS_DESCRIPTION ." pd,
						". TABLE_RAYON ." r
                        
						WHERE products_status = 1
						and p.products_id=pd.products_id
						and p.products_id=pc.products_id
						and pc.categories_id=cr.categories_id
						and cr.rubrique_id=r.rubrique_id
						and cr.categories_id=marque.categories_id
						and modele.categories_id=pc.categories_id
						". $query_custom_type . $query_rubrique_custom_type;
	}
?>

<!-- Dix derniers articles publi�s sur le site -->
<div id="last_ten">
    <?php
    $new_ten_query = tep_db_query($requete_new . " ORDER BY p.products_date_added DESC LIMIT 0,10");
    ?>
    
    &nbsp;Les 10 derni&egrave;res nouveaut&eacute;s
    <hr />
    <table>
        <?php 
        $i = 1;
        while($new_ten_data = tep_db_fetch_array($new_ten_query)){ 
            ?>
            <tr>
            <td class="numero"><?php echo $i . "."; $i++; ?></td>
            <td><?php 
				
				$id_rayon=1;
				$marque ='';
				if (isset($_GET['rayon'])) {
					$id_rayon=$_GET['rayon'];
				}
				if (isset($_GET['marque'])) {
					$marque="cd2.categories_id='".$_GET['marque']."' and ";
				}
				
				if (isset($_GET['rayon'])) {
					$query_url="SELECT cd.categories_id as id_modele, cd.categories_name as nom_modele, cd.categories_url as url_modele, rub.rubrique_id, rub.rubrique_name, rub.rubrique_url, cd2.categories_id as id_marque, cd2.categories_name as nom_marque, cd2.categories_url as url_marque, pd.products_name as nom_produit, pd.products_url as url_article 
								FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
																   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
																   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
																   inner join " . TABLE_RAYON . " as rub on rub.rubrique_id=cr.rubrique_id
																   inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " as pc on c.categories_id=pc.categories_id
																   inner join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on pd.products_id=pc.products_id
								WHERE ".$marque." cr.rubrique_id='" . $id_rayon . "' and pd.products_id='".$new_ten_data['products_id']."'";
				} else {
					$query_url="SELECT cd.categories_id as id_modele, cd.categories_name as nom_modele, cd.categories_url as url_modele, rub.rubrique_id, rub.rubrique_name, rub.rubrique_url, cd2.categories_id as id_marque, cd2.categories_name as nom_marque, cd2.categories_url as url_marque, pd.products_name as nom_produit, pd.products_url as url_article 
								FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
																   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
																   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
																   inner join " . TABLE_RAYON . " as rub on rub.rubrique_id=cr.rubrique_id
																   inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " as pc on c.categories_id=pc.categories_id
																   inner join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on pd.products_id=pc.products_id
								WHERE ".$marque." cr.rubrique_id=rub.rubrique_id and pd.products_id='".$new_ten_data['products_id']."'";
				}
							
				$res_url=tep_db_query($query_url);
				$row_url = tep_db_fetch_array($res_url);
				// Modification par Thierry le 19/04/2018
				//$products_name = bda_product_name_transform($new_ten_data['products_name'], $id_modele);
				//$products_url = bda_product_name_transform($new_ten_data['products_url'], $id_modele);
				$products_name = $new_ten_data['products_name'];
				$products_url = $new_ten_data['products_url'];
				
				$url_clean = url("article",array('id_rayon' => $row_url["rubrique_id"], 'nom_rayon' => $row_url["rubrique_url"], 'id_marque' => $row_url["id_marque"], 'url_marque' => $row_url["url_marque"], 'id_modele' => $row_url["id_modele"], 'url_modele' => $row_url["url_modele"], 'id_article' => $new_ten_data["products_id"], 'url_article' => $products_url));
				echo "<a href='".$url_clean." '>" . $products_name . "</a>";
				?>
            </td>
            </tr>
        <?php } ?>
    </table>
</div>

<!-- Dix articles les plus vendus -->
<div id="top_ten">
    <?php
    /*$top_ten_query = tep_db_query($requete_top . " LIMIT 0,10");
	if(tep_db_num_rows($top_ten_query)==0){
		$top_ten_query = tep_db_query($req_acc . " LIMIT 0,10");	
	}*/
	
	if (isset($_GET['marque']) && $_GET['marque'] != '') {
		
	} else if (isset($_GET['rayon']) && $_GET['rayon'] != '') {
		
	} else {
		
	}
    ?>

    &nbsp;Le top 10 des ventes
    <hr />
    <table>
        <?php 
        $i = 1;
        while($top_ten_data = tep_db_fetch_array($top_ten_query)){ 
            ?>
            <tr>
                <td class="numero"><?php echo $i . "."; $i++; ?></td>
                <td><?php 
					//$products_name = bda_product_name_transform($top_ten_data['products_name'], $id_modele);
					//$products_url = bda_product_name_transform($top_ten_data['products_url'], $id_modele);
					$products_name = $top_ten_data['products_name'];
					$products_url = $top_ten_data['products_url'];
					$url_clean = url("article",array('id_rayon' => $top_ten_data["rubrique_id"], 'nom_rayon' => $top_ten_data["rubrique_url"], 'id_marque' => $top_ten_data["id_marque"], 'url_marque' => $top_ten_data["url_marque"], 'id_modele' => $top_ten_data["id_modele"], 'url_modele' => $top_ten_data["url_modele"], 'id_article' => $top_ten_data["products_id"], 'url_article' => $products_url));
					echo "<a href='".$url_clean." '>" . $products_name . "</a>"; 
					?>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>

<div style="clear:both;"></div>