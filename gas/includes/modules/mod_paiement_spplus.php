<?php 
// penser à modifier le fichier mtp34/paiement_ssplus_admin.php
function lien_paiement_spplus($id_magasin,$siret,$num_commande){
	
	//if($_SERVER['REMOTE_ADDR'] == '81.251.107.237'){
		return getSystemPay($id_magasin,$siret,$num_commande);
	//}
	
	// Chargement de la librairie SP PLUS si cela est nécessaire (API PHP)
    // Ligne à commenter si vous utiliser le kit marchand SP PLUS sous la forme d’un exécutable CGI.
    if ( !extension_loaded('SPPLUS') ) { dl('php_spplus.so'); }
	
	$info_client=tep_db_query("select * 
								from ".TABLE_CUSTOMERS." c, ".TABLE_ADDRESS_BOOK." a 
								where c.customers_id=a.customers_id 
									 and c.customers_id=".$_SESSION['customer_id']." 
									 and a.address_book_id = c.customers_default_address_id");
	$res=tep_db_fetch_array($info_client);
	$mail=$res['customers_email_address'];
	$id_client=$_SESSION['customer_id'];
	$nom=$res['customers_lastname'];
	$prenom=$res['customers_firstname'];
	$societe=$res['customers_company'];
	$mail=$res['customers_email_address'];
	
	$pays_adresse=tep_db_query("select countries_iso_code_2 from ".TABLE_COUNTRIES." where countries_id=".$res['entry_country_id']."");
	$res_pays=tep_db_fetch_array($pays_adresse);
	$language=$res_pays['countries_iso_code_2'];
	
	$montant=format_to_money($_SESSION['total_panier']);
    /* PARAMETRES SP PLUS OBLIGATOIRES
     * siret     : le code siret du site.
     * reference : la référence de la commande, unique pour chaque paiement effectué, limitée à 20 caractères.
     * langue    : la langue choisie par l’internaute.
     * devise    : la devise dans laquelle est exprimé le montant.
     * montant   : le montant total TTC de la commande, le séparateur décimal doit être le caractère point ".".
     * taxe      : la taxe appliquée, non utilisé mais obligatoirement présent avec la valeur 0.00.
     * validite  : la date de validité de la commande.
     * hmac      : la clé hmac calculée à partir des paramètres précédents.
     */ 
	 
	 // DETERMINATION DE LA REFERENCE UNIQUE
	 // NBen 1/ je récupère les 6 premières lettres de l'adresse email
     /*$email=$mail;
	
	 // Remplacement des caractères spéciaux
	 $email = str_replace("@", "", $email);
	 $email = str_replace(".", "", $email);
	 $email = str_replace("-", "", $email);
	 $email = str_replace("_", "", $email);
	 if (strlen($email) >= 6) {
		  $email = substr($email, 0, 6);
	 }*/		
	 // NBen 2/ je lui colle annéemoisjourheureminuteseconde, soit une longueur de 20 caractères
	 // par défaut : $reference=$email.date("YmdHis");  
	 $reference=$num_commande.date("dmYHis");
	 // Fin de le détermination de la référence
	
	 
	 // DETERMINATION DE LA LANGUE
    // (code à compléter suivant les langues disponibles sur votre site)
    switch ($language) {
      case 'EN':
        $langue = "EN";
        break;
      case 'DE':
        $langue = "DE";
        break;
      case 'ES':
        $langue = "ES";
        break;
      case 'FR':
        $langue = "FR";
        break;
      default:
        $langue = "FR";
        break;
    }
	$langue;
	
	// DETERMINATION DE LA DEVISE ET DU CODE SIRET ASSOCIE
    // (code à compléter suivant les devises disponibles sur votre site en associant 
    //  le bon code siret à chaque devise, informations communiquées par SP PLUS)
	$devise = "978";
	
	// DETERMINATION DE LA VALIDITE = le lendemain (J+1 conseillé par SP PLUS)
    $validite = date("d/m/Y", mktime(0,0,0,date("m"),date("d")+1,date("Y")));
	
	// DETERMINATION DE LA TAXE
    $taxe   = "0.00"; 
    
    // Détermination du moyen et de la modalité du paiement
    $moyen    = "CBS";
    $modalite = "1x";
	
	// DETERMINATION DE LA CLE HMAC
    // si la librairie SP PLUS est chargée (dynamiquement ou au niveau du serveur)
    $hmacspplus = "";
    if ( extension_loaded('SPPLUS') ) {
		$hmacspplus = calcul_hmac($id_magasin, $siret, $reference, $langue, $devise, $montant, $taxe, $validite);
    }
	
	//$urlretour="https://boutiquedesaccessoires.fr/bda/panier_remerciement.php";
	//<input type="hidden" name="urlretour" value="'.$urlretour.'" />
	$form_action_url = 'https://www.spplus.net/paiement/init.do';
	
	$process_form = 
	   '<form method="post" action="'.$form_action_url.'" name="spplus">
			<input type="hidden" name="siret" value="'.$siret.'" />
			<input type="hidden" name="reference" value="'.$reference.'" />
			<input type="hidden" name="langue" value="'.$langue.'" />
			<input type="hidden" name="devise" value="'.$devise.'" />
			<input type="hidden" name="montant" value="'.$montant.'" />
			<input type="hidden" name="taxe" value="'.$taxe.'" />
			<input type="hidden" name="validite" value="'.$validite.'" />
			<input type="hidden" name="hmac" value="'.$hmacspplus.'" />
			<input type="hidden" name="moyen" value="'.$moyen.'" />
			<input type="hidden" name="modalite" value="'.$modalite.'" />
			<input type="hidden" name="email" value="'.$mail.'" />
			
							
			<input type="hidden" name="arg1" value="'.$num_commande.'" />
			<input type="hidden" name="arg2" value="'.$id_client." ".$nom." ".$prenom.'" />
			<input type="hidden" name="arg3" value="'.$societe.'" />
			<input type="hidden" name="arg4" value="'.$id_client.'" />			
		</form>
		<script type="text/javascript">document.spplus.submit();</script> ';
	
    return $process_form;	
}

// Load Systempay payment API
include_once('/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/classes/systempay_api.php');
function getSystemPay($id_magasin,$siret,$num_commande){
		// https://www.generalarmystore.fr/gas/panier_recapitulatif.php

		
		// Gateway access parameters
		define('MODULE_PAYMENT_SYSTEMPAY_SITE_ID', '18704807');
		define('MODULE_PAYMENT_SYSTEMPAY_KEY_TEST', '5493617724245696');
		/*define('MODULE_PAYMENT_SYSTEMPAY_KEY_PROD', '');
		define('MODULE_PAYMENT_SYSTEMPAY_MODE', 'TEST'); // PRODUCTION*/
		define('MODULE_PAYMENT_SYSTEMPAY_KEY_PROD', '8032519179444393');
	    define('MODULE_PAYMENT_SYSTEMPAY_MODE', 'PRODUCTION'); // PRODUCTION
		
		define('MODULE_PAYMENT_SYSTEMPAY_GATEWAY_URL', 'https://paiement.systempay.fr/vads-payment/');
		
		// Payment options
		define('MODULE_PAYMENT_SYSTEMPAY_LANGUAGE', 'FR');
		define('MODULE_PAYMENT_SYSTEMPAY_DELAY', '');
		define('MODULE_PAYMENT_SYSTEMPAY_VALIDATION_MODE', '');
		define('MODULE_PAYMENT_SYSTEMPAY_PAYMENT_CARDS', '');
		
		// Gateway return parameters
		define('MODULE_PAYMENT_SYSTEMPAY_REDIRECT_ENABLED', 'False');
		define('MODULE_PAYMENT_SYSTEMPAY_REDIRECT_SUCCESS_TIMEOUT', 5);
		define('MODULE_PAYMENT_SYSTEMPAY_REDIRECT_SUCCESS_MESSAGE', 'Votre paiement a bien été pris en compte, vous allez être redirigé dans quelques instants');
		define('MODULE_PAYMENT_SYSTEMPAY_REDIRECT_ERROR_TIMEOUT', 5);
		define('MODULE_PAYMENT_SYSTEMPAY_REDIRECT_ERROR_MESSAGE', 'Une erreur est survenue, vous allez être redirigé dans quelques instants');
		define('MODULE_PAYMENT_SYSTEMPAY_RETURN_MODE', 'POST'); // POST // GET
		
		$default_url_success = 'https://www.generalarmystore.fr/gas/spplus-7dql9apmn7idm5qgdr5u3qposa3i2c7e.php'; //checkout_process_systempay.php
		$default_url_return = 'https://www.generalarmystore.fr/gas/panier_recapitulatif.php'; //checkout_payment.php
		
		define('MODULE_PAYMENT_SYSTEMPAY_SUCCESS_URL', $default_url_success);
		define('MODULE_PAYMENT_SYSTEMPAY_DEFAULT_URL', $default_url_return);
		define('MODULE_PAYMENT_SYSTEMPAY_ORDER_STATUS_ID', '0');


		$language = 'FR';
		$my_currency = SystempayApi::findCurrencyByAlphaCode('EUR');
		
		$total = number_format($_SESSION['total_panier'], 2, '.', '');
		$total = $my_currency->convertAmountToInteger($total);
		
		$cartID = $num_commande;
		
		
		$customer_address_query = tep_db_query("select c.customers_firstname, c.customers_lastname, c.customers_telephone, c.customers_email_address, ab.entry_firstname, ab.entry_lastname, ab.entry_country_id, ab.entry_company, ab.entry_street_address, ab.entry_suburb, ab.entry_postcode, ab.entry_city, ab.entry_zone_id, z.zone_name, co.countries_id, co.countries_name, co.countries_iso_code_2, co.countries_iso_code_3, co.address_format_id, ab.entry_state from " . TABLE_CUSTOMERS . " c, " . TABLE_ADDRESS_BOOK . " ab left join " . TABLE_ZONES . " z on (ab.entry_zone_id = z.zone_id) left join " . TABLE_COUNTRIES . " co on (ab.entry_country_id = co.countries_id) where c.customers_id = '" . (int)$_SESSION['customer_id'] . "' and ab.customers_id = '" . (int)$_SESSION['customer_id'] . "' and c.customers_default_address_id = ab.address_book_id");
      $customer_address = tep_db_fetch_array($customer_address_query);

	  
	   $shipping_address_query = tep_db_query("select ab.entry_firstname, ab.entry_lastname, ab.entry_company, ab.entry_street_address, ab.entry_suburb, ab.entry_postcode, ab.entry_city, ab.entry_zone_id, z.zone_name, ab.entry_country_id, c.countries_id, c.countries_name, c.countries_iso_code_2, c.countries_iso_code_3, c.address_format_id, ab.entry_state from " . TABLE_ADDRESS_BOOK . " ab left join " . TABLE_ZONES . " z on (ab.entry_zone_id = z.zone_id) left join " . TABLE_COUNTRIES . " c on (ab.entry_country_id = c.countries_id) where ab.customers_id = '" . (int)$_SESSION['customer_id'] . "' and ab.address_book_id = '" . (int)$_SESSION['adresse_livraison'] . "'");
	  $shipping_address = tep_db_fetch_array($shipping_address_query);
	  
	  
	  
	  $customer = array(      'firstname' => $customer_address['customers_firstname'],
                              'lastname' => $customer_address['customers_lastname'],
                              'company' => $customer_address['entry_company'],
                              'street_address' => $customer_address['entry_street_address'],
                              'suburb' => $customer_address['entry_suburb'],
                              'city' => $customer_address['entry_city'],
                              'postcode' => $customer_address['entry_postcode'],
                              'state' => '',
                              'zone_id' => $customer_address['entry_zone_id'],
                              'country' => array('id' => $customer_address['countries_id'], 'title' => $customer_address['countries_name'], 'iso_code_2' => $customer_address['countries_iso_code_2'], 'iso_code_3' => $customer_address['countries_iso_code_3']),
                              'format_id' => $customer_address['address_format_id'],
                              'telephone' => $customer_address['customers_telephone'],
                              'email_address' => $customer_address['customers_email_address']);

      $delivery = array(      'firstname' => $shipping_address['entry_firstname'],
                              'lastname' => $shipping_address['entry_lastname'],
                              'company' => $shipping_address['entry_company'],
                              'street_address' => $shipping_address['entry_street_address'],
                              'suburb' => $shipping_address['entry_suburb'],
                              'city' => $shipping_address['entry_city'],
                              'postcode' => $shipping_address['entry_postcode'],
                              'state' => '',
                              'zone_id' => $shipping_address['entry_zone_id'],
                              'country' => array('id' => $shipping_address['countries_id'], 'title' => $shipping_address['countries_name'], 'iso_code_2' => $shipping_address['countries_iso_code_2'], 'iso_code_3' => $shipping_address['countries_iso_code_3']),
                              'country_id' => $shipping_address['entry_country_id'],
                              'format_id' => $shipping_address['address_format_id']);

      $billing = array(      'firstname' => $customer_address['entry_firstname'],
                             'lastname' => $customer_address['entry_lastname'],
                             'company' => $customer_address['entry_company'],
                             'street_address' => $customer_address['entry_street_address'],
                             'suburb' => $customer_address['entry_suburb'],
                             'city' => $customer_address['entry_city'],
                             'postcode' => $customer_address['entry_postcode'],
                             'state' => '',
                             'zone_id' => $customer_address['entry_zone_id'],
                             'country' => array('id' => $customer_address['countries_id'], 'title' => $customer_address['countries_name'], 'iso_code_2' => $customer_address['countries_iso_code_2'], 'iso_code_3' => $customer_address['countries_iso_code_3']),
                             'country_id' => $customer_address['entry_country_id'],
                             'format_id' => $customer_address['address_format_id']);
	  
	  
		
		
		
		// Use our custom class to generate the html
		$api = new SystempayApi("ISO-8859-1");
		$api->set('amount', 			$total);
		$api->set('order_id',			$cartID);
		$api->set('contrib', 			'www.pimentbleu.fr');
		
		$api->set('platform_url',		MODULE_PAYMENT_SYSTEMPAY_GATEWAY_URL);
		$api->set('site_id',			MODULE_PAYMENT_SYSTEMPAY_SITE_ID);
		$api->set('key_test',			MODULE_PAYMENT_SYSTEMPAY_KEY_TEST);
		$api->set('key_prod',			MODULE_PAYMENT_SYSTEMPAY_KEY_PROD);
		$api->set('ctx_mode',			MODULE_PAYMENT_SYSTEMPAY_MODE);
		$api->set('capture_delay', 		MODULE_PAYMENT_SYSTEMPAY_DELAY);
		$api->set('payment_cards',		MODULE_PAYMENT_SYSTEMPAY_PAYMENT_CARDS);
		$api->set('validation_mode',	MODULE_PAYMENT_SYSTEMPAY_VALIDATION_MODE);
		
		$api->set('currency', 			$my_currency->num);
		$api->set('language',			$language);
		
		$api->set('cust_id',			session_id());
		$api->set('cust_phone',   		$customer['telephone']);
		$api->set('cust_email',			$customer['email_address']);
		$api->set('cust_name',			$billing['firstname'] . ' ' . $billing['lastname']);
		$api->set('cust_address',		$billing['street_address'] . ' ' . $billing['suburb']);
		$api->set('cust_zip',			$billing['postcode']);
		$api->set('cust_city',			$billing['city']);
		$api->set('cust_state',			$billing['state']);
		$api->set('cust_country',		$billing['country']['iso_code_2']);
		
		$api->set('ship_to_name', 		$delivery['firstname'].' '.$delivery['lastname']);
		$api->set('ship_to_street', 	$delivery['street_address']);
		$api->set('ship_to_street2', 	$delivery['suburb']);
		$api->set('ship_to_city',  		$delivery['city']);
		$api->set('ship_to_state',  	$delivery['state']);
		$api->set('ship_to_country', 	$delivery['country']['iso_code_2']);
		$api->set('ship_to_zip', 		$delivery['postcode']);
		$api->set('ship_to_phone_num', 	$customer['telephone']);
		
		$api->set('url_return',			MODULE_PAYMENT_SYSTEMPAY_DEFAULT_URL.'?error_message=' . urlencode('Votre paiement n\'a pas pu être confirmé. Une erreur s\'est produite dans le processus de paiement.'));
		$api->set('url_success',		MODULE_PAYMENT_SYSTEMPAY_SUCCESS_URL);
		$api->set('return_get_params', 	session_name().'='. session_id());
		$api->set('return_mode',		MODULE_PAYMENT_SYSTEMPAY_RETURN_MODE);
		
		$api->set('redirect_enabled',	MODULE_PAYMENT_SYSTEMPAY_REDIRECT_ENABLED);
		$api->set('redirect_success_timeout', 	MODULE_PAYMENT_SYSTEMPAY_REDIRECT_SUCCESS_TIMEOUT);
		$api->set('redirect_success_message', 	MODULE_PAYMENT_SYSTEMPAY_REDIRECT_SUCCESS_MESSAGE);
		$api->set('redirect_error_timeout', 	MODULE_PAYMENT_SYSTEMPAY_REDIRECT_ERROR_TIMEOUT);
		$api->set('redirect_error_message', 	MODULE_PAYMENT_SYSTEMPAY_REDIRECT_ERROR_MESSAGE);
		
		
		
		$form = '<form action="' . MODULE_PAYMENT_SYSTEMPAY_GATEWAY_URL . '" method="POST" name="spplus">';
		$form .= $api->getRequestFieldsHtml();
		$form .= '</form>';
		$form .= '<script type="text/javascript">document.spplus.submit();</script>';
		return $form;
	

}

?>