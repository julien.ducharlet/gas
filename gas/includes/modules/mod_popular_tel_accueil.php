<?php
/* Auteur : Paul
   Module se chargeant d'afficher les t�l�phones les plus populaires de la marque dans laquelle on se trouve (par d�faut de toutes les marques)*/
?>

<div id="tel_plus_populaires">
   
        <h1>&nbsp;Les t&eacute;l&eacute;phones mobiles les plus populaires</h1>
    <div class="liste">
    <?php
		
		
		$plus_pop_query = tep_db_query("SELECT count( 1 ) AS nb, v.cPath, v.cat, v.sous_cat, cd_marque.categories_name as nom_marque,cd_marque.categories_url as url_marque, 
											   cd.categories_name, cd.categories_url, c.categories_image2, cd.balise_title_lien_texte, cd.balise_title_lien_image, 
											   categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, categories_status_adm, categories_status_vip
										FROM visite v, categories_description cd, categories_description cd_marque, categories c
										WHERE v.cPath <> '' and v.sous_cat = cd.categories_id and c.categories_id = cd.categories_id and v.cat = cd_marque.categories_id and c.categories_status='1'
										GROUP BY v.cPath
										ORDER BY nb DESC
										LIMIT 0,15");
		$nb_aff=0;
        while($plus_pop_data = tep_db_fetch_array($plus_pop_query)){
			if($nb_aff<7 && affiche_info($_SESSION['customer_id'], $_SESSION['customers_type'], $_SESSION['customers_vip'], $plus_pop_data['categories_status_tous'], $plus_pop_data['categories_status_client'], $plus_pop_data['categories_status_rev'], $plus_pop_data['categories_status_pro'], $plus_pop_data['categories_status_adm'], $plus_pop_data['categories_status_vip'])){
				$nb_aff++;
				$url = url('modele', array('id_rayon' => '1-Tel', 'nom_rayon' => $url_rayon, 'id_marque' => $plus_pop_data['cat'], 'url_marque' => $plus_pop_data['url_marque'], 'id_modele' => $plus_pop_data['sous_cat'], 'url_modele' => $plus_pop_data['categories_url']));
				?>
				<div class="telephone" onclick="location.href='<?php echo $url; ?>';">
					<a href="<?php echo $url; ?>" title="<?php echo $plus_pop_data['balise_title_lien_texte']." ".$plus_pop_data['nom_marque']." ".$plus_pop_data['categories_name']; ?>"><img src="<?php echo BASE_DIR; ?>/images/categories_pictures/<?php echo $plus_pop_data['categories_image2']; ?>" alt="<?php echo $plus_pop_data['balise_title_lien_image']." ".$plus_pop_data['nom_marque']." ".$plus_pop_data['categories_name']; ?>" height="100px"/></a>
					<a href="<?php echo $url; ?>" title="<?php echo $plus_pop_data['balise_title_lien_texte']." ".$plus_pop_data['nom_marque']." ".$plus_pop_data['categories_name']; ?>" style="text-decoration:none"><span class="nom_marque"><?php echo $plus_pop_data['nom_marque']; ?></span><br /><?php echo $plus_pop_data['categories_name']; ?></a>
				</div>
				<?php
        	}
		}
    ?>
    <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
    <?php  // Fin de l'affichage pour le rayon 1 (t�l�phonie)?>
	<br /><br />
</div>