<?php
// page appel� dans la fiche d'un produit pour lister les articles �galement achet�s avec le produit courant
// modification Thierry le 19/04/2018 ajout dans la requete (ligne 10) de "balise_title_lien_image," et de $data_cat["modele"] a la place de $data_cat["id_modele"] (ligne 87) 

$query = "select p.products_id,
				pd.products_name,
				p.products_price,
				p.products_bimage,
				balise_title_lien_texte,
				balise_title_lien_image,
				pd.products_url as url_article,
				ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1,
				ppbq.part_price_by_5, ppbq.pro_price_by_5, ppbq.rev_price_by_5, ppbq.adm_price_by_5,
				ppbq.part_price_by_10, ppbq.pro_price_by_10, ppbq.rev_price_by_10, ppbq.adm_price_by_10,
				s.specials_new_products_price,
				pvf.prix_vente ";
$query .= "from ". TABLE_ORDERS_PRODUCTS ." opa,
			". TABLE_ORDERS_PRODUCTS ." opb,
			". TABLE_ORDERS ." o,
			". TABLE_PRODUCTS_DESCRIPTION ." pd,
			". TABLE_PRODUCTS ." p
			left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id
			left join ". TABLE_SPECIALS ." as s on (p.products_id=s.products_id and (expires_date >NOW() or expires_date='0000-00-00 00:00:00'))
			left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash>NOW() or fin_vente_flash='0000-00-00'))) ";
$query .= "where opa.products_id = '". $id_produit ."' and opa.orders_id = opb.orders_id and opb.products_id != '". $id_produit ."' and opb.products_id = p.products_id and opb.orders_id = o.orders_id and p.products_status = '1' and p.products_id=pd.products_id group by p.products_id order by o.date_purchased desc limit 0,6";

$query = tep_db_query($query);

$nb_product = 0;

if(tep_db_num_rows($query) > 0) {
    
    ?>
    
    <div style="margin:10px 0px 5px;">Les personnes ayant achet&eacute; cet article ont &eacute;galement achet&eacute; :</div><hr />
    <table style="margin:auto;">
        <tr>
    
    <?php

    while($data = tep_db_fetch_array($query)) {
        
        $query_cat = "select count(*) as total from ". TABLE_PRODUCTS_TO_CATEGORIES ." where products_id=". $data['products_id'] ." and categories_id=". $_REQUEST["modele"];
        $query_cat = tep_db_query($query_cat);
        $data_cat = tep_db_fetch_array($query_cat);
        
        // si le produit est pr�sent dans la m�me cat�gorie
        if($data_cat['total'] > 0) {
        
            $url = url("article",array(
                         'id_rayon' => $_REQUEST["rayon"], 
                         'nom_rayon' => $row_url["rubrique_url"], 
                         'id_marque' => $_REQUEST["marque"], 
                         'url_marque' => $row_url["url_marque"], 
                         'id_modele' => $_REQUEST["modele"], 
                         'url_modele' => $row_url["url_modele"], 
                         'id_article' => $data['products_id'], 
                         'url_article' => bda_product_name_transform($data["url_article"],$row_url["id_modele"])));
            
            $products_name = bda_product_name_transform($data['products_name'], $id_modele);
            
        }
        // sinon on cherche le produit dans une autre cat�gorie
        else {
            
            $query_product_in_other_cat = 'select r.rubrique_id as rayon, rubrique_url, c2.categories_id as marque, cd2.categories_url as url_marque, c1.categories_id as modele, cd1.categories_url as url_modele, pd.products_url as url_article ';
            $query_product_in_other_cat .= 'from '. TABLE_PRODUCTS_DESCRIPTION .' pd, '. TABLE_PRODUCTS_TO_CATEGORIES .' ptc, '. TABLE_CATEGORIES .' c1, '. TABLE_CATEGORIES_DESCRIPTION .' cd1, ';
            $query_product_in_other_cat .= TABLE_CATEGORIES .' c2, '. TABLE_CATEGORIES_DESCRIPTION .' cd2, '. TABLE_CATEGORIES_RAYON .' cr, '. TABLE_RAYON .' r ';
            $query_product_in_other_cat .= 'where ptc.categories_id=c1.categories_id and c1.categories_id=cd1.categories_id and c1.parent_id=c2.categories_id ';
            $query_product_in_other_cat .= 'and c2.categories_id=cd2.categories_id and c2.categories_id=cr.categories_id and cr.rubrique_id=r.rubrique_id ';
            $query_product_in_other_cat .= 'and pd.products_id=ptc.products_id and ptc.products_id='. $data['products_id'] .' limit 0,1';
            
            
            $query_product_in_other_cat = tep_db_query($query_product_in_other_cat);
            $data_cat = tep_db_fetch_array($query_product_in_other_cat);
            
            $url = url("article",array(
                         'id_rayon' => $data_cat["rayon"], 
                         'nom_rayon' => $data_cat["rubrique_url"], 
                         'id_marque' => $data_cat["marque"], 
                         'url_marque' => $data_cat["url_marque"], 
                         'id_modele' => $data_cat["modele"], 
                         'url_modele' => $data_cat["url_modele"], 
                         'id_article' => $data['products_id'], 
                         'url_article' => bda_product_name_transform($data_cat["url_article"], $data_cat["modele"])));
            
            $products_name = bda_product_name_transform($data['products_name'], $data_cat["modele"]);
        }
        
        $prix = calculate_price_for_product($data);
        
            ?>
            
        <td class="article" <?php if($nb_product == 0) echo 'style="border-left: medium none;"'; ?>>
            <div class="image_article">
                <a href="<?php echo $url; ?>" title="<?php echo $data['balise_title_lien_texte'];?>" target="_blank" >
					<img src="<?php echo BASE_DIR . "/images/products_pictures/petite/" . $data['products_bimage']; ?>" title="<?php echo $data['balise_title_lien_image'];  ?>" alt="<?php echo $data['balise_title_lien_image'];?>" width="130" height="130"/>
				</a>
            </div>
            
            <div class="nom_article">
                <a href="<?php echo $url; ?>" title="<?php echo $data['balise_title_lien_texte']; ?>" target="_blank"><?php echo $products_name; ?></a>
            </div>
            
            <div class="prix_article">
                <?php
				
				if ($prix['flash'] > 0) {
					
					prix2img(format_to_money($prix['flash']), 'moyen');
				}
                elseif($prix['promo'] > 0) {
                    
                    prix2img(format_to_money($prix['promo']), 'moyen');
                }
                else {
                    
                    prix2img(format_to_money($prix['normal']), 'moyen');
                }
                ?>
            </div>
            <span><?php if($prix['type']=='HT') echo 'Hors Taxes'; ?></span>
        </td>
            
        <?php
        
        $nb_product++;
    }
    
    ?>
    
        </tr>
    </table>
     <hr />
    
    <?php
}