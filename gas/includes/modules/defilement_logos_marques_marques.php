<script type="text/javascript">
// Indiquez en pixel la largueur de la zone de défilement
var sliderwidth="960px"
// Indiquez en pixel la hauteur de la zone de défilement 60px
var sliderheight="50px"
// Indiquer la vitesse de défilement (de lente a rapide 1-10)
var slidespeed=2
// Indiquez la couleur de fond :
slidebgcolor="#FFFFFF"

// Faire une requete vers la table "manufacturers" et recupérer les champs "manufacturers_id, manufacturers_name, manufacturers_image" et faire une boucle avec que les marque qui ont le champs "manufacturers_image" qui ne soit pas vide et les lister ci dessous.

var leftrightslide=new Array();
var finalslide='';
var index = 0;

<?php
	
	$query_manufacturers = '	SELECT 
									manufacturers_id, 
									manufacturers_name, 
									manufacturers_image, 
									manufacturers_url 
								FROM '. TABLE_MANUFACTURERS .' 
								ORDER BY manufacturers_name';
	$query_manufacturers = tep_db_query($query_manufacturers );
	
	while($data = tep_db_fetch_array($query_manufacturers )) {
		
		$query_count = 'select count(*) as total from '. TABLE_PRODUCTS .' p where products_status=1 and manufacturers_id='. $data['manufacturers_id'];
		$query_count = tep_db_query($query_count);
		
		$data_count = tep_db_fetch_array($query_count);
		
		if($data_count['total'] > 0) {
			$manufacturers_name = str_replace('&', '&amp;', $data['manufacturers_name']);
			$manufacturers_name = str_replace('\'', ' ', $manufacturers_name);
			?>
			leftrightslide[index++] = '<a href="marques.php?marque=<?php echo $data['manufacturers_id']; ?>"><img border="0" src="https://www.generalarmystore.fr/gas/images/marques_pictures/<?php echo $data['manufacturers_image']; ?>" alt="<?php echo $manufacturers_name; ?>"/></a>';
			<?php
		}
	}
?>

// Indiquez en code HTML l'ecart entre deux images :
var imagegap="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
// Indiquez en pixel l'ecart entre le debut des images et la fin (5):
var slideshowgap=5

////  NE PAS MODIFIER CE QUI SE TROUVE EN DESSOUS  ///////

var copyspeed=slidespeed
leftrightslide='<nobr>'+leftrightslide.join(imagegap)+'</nobr>'
var iedom=document.all||document.getElementById
if (iedom)
document.write('<span id="temp" style="visibility:hidden;position:absolute;top:-100px;left:-9000px">'+ leftrightslide +'</span>')
var actualwidth=''
var cross_slide, ns_slide

function fillup(){
	if (iedom){
		cross_slide=document.getElementById? document.getElementById("test2") : document.all.test2
		cross_slide2=document.getElementById? document.getElementById("test3") : document.all.test3
		cross_slide.innerHTML=cross_slide2.innerHTML=leftrightslide
		actualwidth=document.all? cross_slide.offsetWidth : document.getElementById("temp").offsetWidth
		cross_slide2.style.left=actualwidth+slideshowgap+"px"
	} else if (document.layers){
		ns_slide=document.ns_slidemenu.document.ns_slidemenu2
		ns_slide2=document.ns_slidemenu.document.ns_slidemenu3
		ns_slide.document.write(leftrightslide)
		ns_slide.document.close()
		actualwidth=ns_slide.document.width
		ns_slide2.left=actualwidth+slideshowgap
		ns_slide2.document.write(leftrightslide)
		ns_slide2.document.close()
	}
	lefttime=setInterval("slideleft()",30)
}
window.onload=fillup

function slideleft() {
	if (iedom){
		if (parseInt(cross_slide.style.left)>(actualwidth*(-1)+8))
			cross_slide.style.left=parseInt(cross_slide.style.left)-copyspeed+"px"
		else
			cross_slide.style.left=parseInt(cross_slide2.style.left)+actualwidth+slideshowgap+"px"

		if (parseInt(cross_slide2.style.left)>(actualwidth*(-1)+8))
			cross_slide2.style.left=parseInt(cross_slide2.style.left)-copyspeed+"px"
		else
			cross_slide2.style.left=parseInt(cross_slide.style.left)+actualwidth+slideshowgap+"px"

	} else if (document.layers) {
		if (ns_slide.left>(actualwidth*(-1)+8))
			ns_slide.left-=copyspeed
		else
			ns_slide.left=ns_slide2.left+actualwidth+slideshowgap

		if (ns_slide2.left>(actualwidth*(-1)+8))
			ns_slide2.left-=copyspeed
		else
			ns_slide2.left=ns_slide.left+actualwidth+slideshowgap
	}
}


if (iedom||document.layers){
	with (document){
		document.write('<table border="0" cellspacing="5" cellpadding="0"><td>')
		if (iedom) {
			write('<div style="position:relative;width:'+sliderwidth+';height:'+sliderheight+';overflow:hidden">')
			write('<div style="position:absolute;width:'+sliderwidth+';height:'+sliderheight+';background-color:'+slidebgcolor+'" onmouseover="copyspeed=0" onmouseout="copyspeed=slidespeed">')
			write('<div id="test2" style="position:absolute;left:0px;top:10px"></div>')
			write('<div id="test3" style="position:absolute;left:-1000px;top:10px"></div>')
			write('</div></div>')
		} else if (document.layers) {
			write('<ilayer width="+sliderwidth+" height="+sliderheight+" name="ns_slidemenu" bgcolor="+slidebgcolor+">')
			write('<layer left="0" top="0" onmouseover="copyspeed=0" onmouseout="copyspeed=slidespeed" name="ns_slidemenu2"></layer>')
			write('<layer left="0" top="0" onmouseover="copyspeed=0" onmouseout="copyspeed=slidespeed" name="ns_slidemenu3"></layer>')
			write('</ilayer>')
		}
		document.write('</td></table>')
	}
}
</script>