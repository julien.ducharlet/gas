<?php 
/* Module se chargeant d'afficher les 5 dernieres nouveaut�s du site tous les rayons confondus.

Ajouter : 

CSS pour que l'image n'est pas cet espace sur la droite.
V�rifier pourquoi les simples quotes dans le nom de l'article ne passe pas dans la blaise : <a href='".$url_clean."' title='Accessoire t�l�phone ". $products_name ."'>
*/


// On r�cup�re les 5 dernieres nouveaut�s du site

		$requete_new = "SELECT distinct(pd.products_id), pd.products_name, pd.products_url, p.products_bimage
                        FROM " . TABLE_CATEGORIES_RAYON . " c, " . TABLE_PRODUCTS_TO_CATEGORIES . " pc, " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd
                        WHERE products_status = 1 and p.products_id=pd.products_id and p.products_id=pc.products_id and pc.categories_id=c.categories_id
						ORDER BY p.products_date_added DESC LIMIT 0,5";
		$new_ten_query = tep_db_query($requete_new);
?>

<!-- Dix derniers articles publi�s sur le site -->
<div id="top_5_nouveautes">
    
    <br />&nbsp;Les 5 derniers accessoires de la Boutique :
    <hr />
    <table>
        <?php 
        $i = 1;
        while($new_ten_data = tep_db_fetch_array($new_ten_query)){ 
            ?>
            <tr>
            <td class="numero"><?php echo $i . "."; $i++; ?></td>
			<td>&nbsp;&nbsp;&nbsp;
			<img src="../bda/images/products_pictures/micro/<?php echo $new_ten_data["products_bimage"]; ?>" border="0" width="33" height="33" alt="accessoires telephone mobile" />
			</td>
            <td><?php 

				$id_rayon=1;
				
				$query_url="SELECT cd.categories_id as id_modele, cd.categories_name as nom_modele, cd.categories_url as url_modele, rub.rubrique_id, rub.rubrique_name, rub.rubrique_url, cd2.categories_id as id_marque, cd2.categories_name as nom_marque, cd2.categories_url as url_marque, pd.products_name as nom_produit, pd.products_url as url_article 
								FROM " . TABLE_CATEGORIES . " as c 
									inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
									inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
									inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
									inner join " . TABLE_RAYON . " as rub on rub.rubrique_id=cr.rubrique_id
									inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " as pc on c.categories_id=pc.categories_id
									inner join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on pd.products_id=pc.products_id
								WHERE ".$marque." cr.rubrique_id=rub.rubrique_id and pd.products_id='".$new_ten_data['products_id']."'";
				
				$res_url=tep_db_query($query_url);
				$row_url = tep_db_fetch_array($res_url);
				$products_name = bda_product_name_transform($new_ten_data['products_name'], $row_url["id_modele"]);
				$url_clean = url("article",array('id_rayon' => $row_url["rubrique_id"], 'nom_rayon' => $row_url["rubrique_url"], 'id_marque' => $row_url["id_marque"], 'url_marque' => $row_url["url_marque"], 'id_modele' => $row_url["id_modele"], 'url_modele' => $row_url["url_modele"], 'id_article' => $new_ten_data["products_id"], 'url_article' => $products_name));
				echo '<a href="'.$url_clean.'" title="Accessoire t�l�phone '. $products_name .'">' . $products_name . '</a>';
				?>
            </td>
			<td>
				<a href="<?php echo $url_clean; ?>" title="Accessoire mobile <?php echo $products_name; ?>">
					<img src="../<?php echo BASE_DIR; ?>/template/base/boutons/bouton_voir.jpg" width="42" height="22" alt="Accessoire mobile <?php echo $products_name; ?>" />
				</a>
			</td>
            </tr>
        <?php } ?>
    </table>
</div>

<div style="clear:both;"></div>