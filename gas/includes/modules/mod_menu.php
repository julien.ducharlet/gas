<?php
/* Module gérant l'affichage du menu principal du site */
?>

<!-- Fonctions spécifiques au menu principal -->
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/menu.js"></script>

<?php
	
	// Récupération du titre des rayons
	$nb_reponse=0;
	$reponse = tep_db_query("SELECT *
							 FROM rubrique
							 where rubrique_status='1'
							 ORDER BY rubrique_id");
	while($donnees = tep_db_fetch_array($reponse)){
		if ((affiche_info(
				$session_customer_id, 
				$session_customers_type, 
				$session_customers_vip, 
				$donnees['rayon_status_tous'], 
				$donnees['rayon_status_client'],
				$donnees['rayon_status_rev'], 
				$donnees['rayon_status_pro'], 
				$donnees['rayon_status_adm'], 
				$donnees['rayon_status_vip']))
		) {
		$nb_reponse++;	
		
?>
<div class="menu-general" onmouseover="montre('id<?php echo $donnees['rubrique_id']; ?>');" onmouseout="cache('id<?php echo $donnees['rubrique_id']; ?>');"><a href="<?php echo url('rayon', array('id_rayon' => $donnees['rubrique_id'], 'nom_rayon' => $donnees['rubrique_url'])); ?>" ><?php echo strtoupper($donnees['rubrique_name']); ?></a>
	<div class="pour_ie">
        <div class="sous-menu" id="id<?php echo $donnees['rubrique_id']; ?>" onmouseover="montre('id<?php echo $donnees['rubrique_id']; ?>');" onmouseout="cache('id<?php echo $donnees['rubrique_id']; ?>');">
            <?php
                $reponse2 = tep_db_query("SELECT cd.categories_name, cd.categories_id, cd.categories_url, cd.balise_title_lien_texte, categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, categories_status_adm, categories_status_vip
                                         FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id inner join categories_rubrique as cr on c.categories_id=cr.categories_id
                                         WHERE c.parent_id='0' AND c.sort_order > 0 AND cr.rubrique_id = " . $donnees['rubrique_id'] . " and c.categories_status='1'
                                         ORDER BY c.sort_order, cd.categories_name ASC");
										 
                while ($donnees2 = tep_db_fetch_array($reponse2)) {
					if (affiche_info(	$session_customer_id, 
										$session_customers_type, 
										$session_customers_vip, 
										$donnees2['categories_status_tous'], 
										$donnees2['categories_status_client'], 
										$donnees2['categories_status_rev'], 
										$donnees2['categories_status_pro'], 
										$donnees2['categories_status_adm'], 
										$donnees2['categories_status_vip'])
									) {
						echo "<a href=\"" . url('marque', array('id_rayon' => $donnees['rubrique_id'], 'nom_rayon' => $donnees['rubrique_url'], 'id_marque' => $donnees2['categories_id'], 'url_marque' => $donnees2['categories_url'])) . "\" title=\"".$donnees2['balise_title_lien_texte']."\">&nbsp;&nbsp;" . $donnees2['categories_name'] . "</a>\n";
					}
				}
            ?>
        </div>
    </div>
</div>
<?php
		}
	}
?>

<div class="menu-general" onmouseover="montre('id100');" onmouseout="cache('id100');"><a href="https://www.generalarmystore.fr/gas/marques.php?marque=99" title="Les marques que propose Group Army Store / General Army Store ">LES MARQUES</a>
	
</div>






