<?php 
/* Module se chargeant d'afficher les 5 dernieres nouveaut�s du site tous les rayons confondus.

Ajouter : 

CSS pour que l'image n'est pas cet espace sur la droite.
V�rifier pourquoi les simples quotes dans le nom de l'article ne passe pas dans la blaise : <a href='".$url_clean."' title='Accessoires telephone". $products_name ."'>
*/


	// On r�cup�re les tops 5 du site
	$req_acc="SELECT count( 1 ) AS nb_visite, rub.rubrique_id, rub.rubrique_name, p.products_bimage, pd.products_id, pd.products_name, pd.products_url, modele_desc.categories_id as id_modele, modele_desc.categories_name as nom_modele, modele_desc.categories_url as url_modele, marque_desc.categories_id as id_marque, marque_desc.categories_name as nom_marque, marque_desc.categories_url as url_marque
						FROM visite v 
						inner join products p on p.products_id=v.products_id
						inner join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on pd.products_id=v.products_id
						inner join categories marque on marque.categories_id=v.cat
						inner join " . TABLE_CATEGORIES_RAYON . " as cr on marque.categories_id=cr.categories_id
						inner join " . TABLE_RAYON . " as rub on rub.rubrique_id=cr.rubrique_id
						inner join categories_description marque_desc on marque.categories_id=marque_desc.categories_id
						inner join categories modele on modele.categories_id=v.sous_cat
						inner join categories_description modele_desc on modele.categories_id=modele_desc.categories_id
						WHERE v.products_id <>0 AND v.cat <>0 AND v.sous_cat <>0 and products_status = 1
						GROUP BY v.products_id, v.cat, v.sous_cat
						ORDER BY nb_visite DESC
						LIMIT 0,5";
	$requete_top = $req_acc;
	
?>

<!-- Dix articles les plus vendus -->
<div id="top_5_ventes">
    <?php
    $top_ten_query = tep_db_query($requete_top);
    ?>

    &nbsp;Les 5 meilleures ventes d'accessoires de la boutique :
    <hr />
    <table>
        <?php 
        $i = 1;
        while($top_ten_data = tep_db_fetch_array($top_ten_query)){ 
            ?>
            <tr>
                <td class="numero"><?php echo $i . "."; $i++; ?></td>
				<td>&nbsp;&nbsp;&nbsp;
					<img src="../bda/images/products_pictures/micro/<?php echo $top_ten_data["products_bimage"]; ?>" border="0" width="33" height="33" alt="accessoires telephone mobile" />
				</td>
                <td><?php 
					$products_name = bda_product_name_transform($top_ten_data['products_name'], $top_ten_data["id_modele"]);
					$url_clean = url("article",array('id_rayon' => $top_ten_data["rubrique_id"], 'nom_rayon' => $top_ten_data["rubrique_name"], 'id_marque' => $top_ten_data["id_marque"], 'url_marque' => $top_ten_data["url_marque"], 'id_modele' => $top_ten_data["id_modele"], 'url_modele' => $top_ten_data["url_modele"], 'id_article' => $top_ten_data["products_id"], 'url_article' => $products_name));
					echo '<a href="'.$url_clean.'" title="Accessoires telephone'. $products_name .'">' . $products_name . '</a>'; 
					?>
                </td>
				<td>
					<a href=" <?php echo $url_clean; ?>" title="Accessoire mobile <?php echo $products_name; ?>">
						<img src="../<?php echo BASE_DIR; ?>/template/base/boutons/bouton_voir.jpg" width="42" height="22" alt="Accessoires mobile <?php echo $products_name; ?>" />
					</a>
				</td>
            </tr>
        <?php } ?>
    </table>
</div>

<div style="clear:both;"></div>