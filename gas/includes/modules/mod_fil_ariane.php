<?php
/* Auteur : Thierry
   Module affichant le fil d'ariane et d�finissant quelques variables utiles.*/
?>

<!-- Par d�faut le fil commencera par Accueil -->
<div id="fil">
<div itemscope itemtype="https://schema.org/BreadcrumbList">
    Vous &ecirc;tes ici : 
	<span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		<meta itemprop="position" content="1" />
		<a itemtype="https://schema.org/Thing" itemprop="item" href="https://www.generalarmystore.fr/index.php">
			<span itemprop="name">Accueil</span>
			<meta itemprop="url" content="https://www.generalarmystore.fr/index.php" />
		</a>
	</span>
    
    <?php
    // Si on se trouve dans un rayon on affiche le nom du rayon dans le fil
    if (isset($_GET['rayon']) && $_GET['rayon'] != '') {
        $id_rayon = (int)$_GET['rayon'];
        
        $rayon_query = tep_db_query("SELECT rubrique_name, rubrique_url
                                     FROM rubrique
                                     WHERE rubrique_id = " . $id_rayon . "");
        $rayon_data = tep_db_fetch_array($rayon_query);
        $nom_rayon = $rayon_data['rubrique_name'];
		$url_rayon = $rayon_data['rubrique_url'];
		
		$url = url('rayon', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon));
        
        echo '&nbsp;>>&nbsp;
		<span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
			<meta itemprop="position" content="2" />
			<a itemtype="https://schema.org/Thing" itemprop="item" href="' . $url . '">
				<span itemprop="name">' . $nom_rayon . '</span>
				
			</a>
		</span>';
	}
    // Si on se trouve dans une marque on affiche le nom de la marque dans le fil
	if (isset($_GET['marque']) && $_GET['marque'] != '') {
		$id_marque = (int)$_GET['marque'];
		
		$marque_query = tep_db_query("SELECT categories_name, categories_url
									  FROM categories_description
									  WHERE categories_id = " . $id_marque . "");
		$marque_data = tep_db_fetch_array($marque_query);
		$nom_marque = $marque_data['categories_name'];
		$url_marque = $marque_data['categories_url'];
		
		$url = url('marque', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque));
		
		echo '&nbsp;>>&nbsp;
		<span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
			<meta itemprop="position" content="3" />
			<a itemtype="https://schema.org/Thing" itemprop="item" href="' . $url . '">
				<span itemprop="name">' . $nom_marque . '</span>
			</a>
		</span>';
		
		// Si on se trouve dans un modele on affiche le nom du modele dans le fil
		if (isset($_GET['modele']) && $_GET['modele'] != '') {
			$id_modele = (int)$_GET['modele'];
			
			$modele_query = tep_db_query("SELECT categories_name, categories_url, categories_id
										  FROM categories_description
										  WHERE categories_id = " . $id_modele . "");
			$modele_data = tep_db_fetch_array($modele_query);
			$nom_modele = $modele_data['categories_name'];
			$url_modele = $modele_data['categories_url'];
			$id_modele = $modele_data['categories_id'];
			
			$url = url('modele', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele, 'url_modele' => $url_modele));
			
			echo '&nbsp;>>&nbsp;
			<span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
				<meta itemprop="position" content="4" />
				<a itemtype="https://schema.org/Thing" itemprop="item" href="' . $url . '">
					<span itemprop="name">' . $nom_modele . '</span>
				</a>
			</span>
			';
		}
    }
    ?>
</div>
</div>