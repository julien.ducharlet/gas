<?php
/*
Page : logout.php
*/


define('TEXT_INFORMATION', '
	   Vous venez de vous d&eacute;connecter de votre compte sur <span style="color:' . COULEUR_1 . ';"><strong>' . NOM_DU_SITE . '</strong></span>.<br /><br />
                        
                        Vous pouvez laisser cet ordinateur allum&eacute; sans risque qu\'on utilise votre compte.<br /><br />
                        
                        Les articles qui se trouvaient dans votre panier ont &eacute;t&eacute; enregistr&eacute;s.<br />
                        Ils vous seront propos&eacute;s &agrave; nouveau lors de votre prochaine connexion &agrave; votre compte
                        sur <span style="color:' . COULEUR_1 . ';"><strong>' . NOM_DU_SITE . '</strong></span>.<br /><br />
                        
                        Et n\'oubliez pas que <span style="color:' . COULEUR_1 . ';"><strong>' . NOM_DU_SITE . '</strong></span> c\'est toujours : <br /><br />
                        - Un paiement  100% s&eacute;curis&eacute; <br />
                        - Tous vos achats garantis 100% satisfaits ou rembours&eacute;s<br />
                        - Des d&eacute;lais de livraisons r&eacute;duits<br />
                        - Livraison en Europe et aux DOM-TOM<br />
                        - La possibilit&eacute; de suivre vos commandes en ligne<br />
	   
	   ');
?>