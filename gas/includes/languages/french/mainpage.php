<!-- MAIN PAGE EXAMPLE START //--> 
<table style="height: 184px;" border="0" cellspacing="0" cellpadding="5">
<tbody>
<tr>
<td class="Texte_Bas_De_Page" align="center" valign="top">
<p>Bienvenue sur le nouveau site G&eacute;n&eacute;ral Army Store.</p>
<p>&nbsp; De nombreuses nouveaut&eacute;s sont &agrave; y d&eacute;couvrir d&egrave;s &agrave; pr&eacute;sent. De nombreuses autres viendront am&eacute;liorer le site plus encore dans les prochaines semaines</p>
<hr width="100%" size="2" />
<div>
<blockquote>
<blockquote>
<blockquote>Toujours 3 menus pour mieux vous guider : <br />1/ par activit&eacute;s<br />2/ tous les effets vestimentaires de la t&ecirc;te au pied<br />3/ tout le mat&eacute;riel</blockquote>
</blockquote>
</blockquote>
</div>
<hr width="100%" size="2" />
<p>&nbsp;Vous pouvez effectuer vos achats sur ce site gr&acirc;ce &agrave; un <strong>paiement s&eacute;curis&eacute;</strong> par <strong>CB</strong> via <strong>SPPlus </strong>de la Caisse d'Epargne, par Ch&egrave;que, par Mandat Cash ou par Virement Bancaire.</p>
<p>G&eacute;n&eacute;ral Army Store est une <span style="text-decoration: underline;">centrale d'achats</span>. Nous ne disposons pas de toutes les r&eacute;f&eacute;rences en stock permanent. Cependant, nous effectuons des commandes de r&eacute;assort aupr&egrave;s des fabricants plusieurs fois par semaine. Nous sommes donc livr&eacute;s r&eacute;guli&egrave;rement. N&eacute;anmoins, ces conditions peuvent prolonger quelque peu votre <span style="text-decoration: underline;">d&eacute;lai de livraison</span>. Ce d&eacute;lai &eacute;tant habituellement <span style="text-decoration: underline;">compris entre 3 et 14 jours ouvr&eacute;s</span>.</p>
<hr width="100%" size="2" />
<p>Merci de votre fid&eacute;lit&eacute; et bonne visite sur votre nouveau site G&eacute;n&eacute;ral Army Store.</p>
</td>
</tr>
</tbody>
</table>
<!-- MAIN PAGE EXAMPLE END //-->