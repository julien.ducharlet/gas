<?php
//EN
// GENERAL
define('NETREVIEWS_EXPORT_TRUE', 'Oui');
define('NETREVIEWS_EXPORT_FALSE', 'Non');

// Heading
define('BOX_HEADING_NETREVIEWS', 'Avis V&eacute;rifi&eacute;s');
// Sub Menu 1
    // file Name
define('FILENAME_NETREVIEWS_EXPORT', 'net_reviews_export.php');
    // Title
define('BOX_TITLE_NETREVIEWS_EXPORT', 'Export');
// Sub Menu 2
     // file Name
define('FILENAME_NETREVIEWS_CONFIGURATION', 'net_reviews_configuration.php');
    // Title
define('BOX_TITLE_NETREVIEWS_CONFIGURATION', 'Configuration');

// Export
    // Time
define('NETREVIEWS_EXPORT_LABEL_TIME', 'Commandes depuis');
define('NETREVIEWS_EXPORT_WW', 'Semaine');
define('NETREVIEWS_EXPORT_MM', 'Mois');
    // Products
define('NETREVIEWS_EXPORT_LABEL_PRODUCTS', 'Exporter les produits li&eacute;s aux commandes');

// CONFIGURATION
define('NETREVIEWS_CONFIG_LABEL_ACTIVE', 'Activer le module pour ce site');
define('NETREVIEWS_CONFIG_NOTE','Activer le module vous permet d\'afficher nos widgets et les avis produits');
define('NETREVIEWS_CONFIG_KEY','Cl&eacute; Secr&egrave;te');
define('NETREVIEWS_CONFIG_ID','ID Website');
define('NETREVIEWS_CONFIG_UPDATE_MSG', 'Configuration sauvegard&eacute;e !');
define('NETREVIEWS_EXPORT_LABEL_ORDER_STATUS', 'Choisissez les statuts de commandes &agrave; exporter :');

// FRONTEND
define('NETREVIEWS_AVIS','%d Avis');
define('NETREVIEWS_AVISS','%d Avis');
define('NETREVIEWS_SHOW_AVIS','Voir les avis');

define('NETREVIEWS_LE','le');
define('NETREVIEWS_PUBLIE_LE','publié le');
define('NETREVIEWS_DATE_FORMAT','d/m/Y');
define('NETREVIEWS_COMMENTAIRE','Commentaire de');
define('NETREVIEWS_EVALUATION_PRODUITS','Evaluations Produits');
define('NETREVIEWS_NB_AVIS','Nombre d\'avis');
define('NETREVIEWS_NOTE_MOYEN','Note moyenne');
define('NETREVIEWS_AFFICHER_CERTIFICAT','Afficher l\'attestion de confiance');
define('NETREVIEWS_PAGINATION','Plus d\'avis');

define('NETREVIEWS_SHOW_CONVERSATION','Afficher les &eacute;changes');
define('NETREVIEWS_HIDE_CONVERSATION','Masquer les &eacute;changes');

define('NETREVIEWS_AUCUNE_EVALUATION_PRODUITS',"Aucun avis n'a encore &eacute;t&eacute; laiss&eacute; sur ce produit.");

// ***** 2018 Law *************************************************
define('YES','Oui');
define('NO','Non');
define('REVIEWS_CONTROL', 'Avis soumis &agrave; un contr&ocirc;le');
define('REVIEWS_CONTROL_FURTHER_INFO', 'Pour plus d\'informations sur les caract&eacute;ristiques du contr&ocirc;le des avis et la possibilit&eacute; de contacter l\'auteur de l\'avis, merci de consulter nos');
define('GCU','CGU');
define('VERIFIED_REVIEWS','avis-verifies.com');
define('REVIEWS_NO_INDUCEMENTS', 'Aucune contrepartie n\'a &eacute;t&eacute; fournie en &eacute;change des avis');
define('REVIEWS_KEPT_PERIOD', 'Les avis sont publi&eacute;s et conserv&eacute;s pendant une dur&eacute;e de cinq ans');
define('REVIEWS_NOT_MODIFY', 'Les avis ne sont pas modifiables : si un client souhaite modifier son avis, il doit contacter Avis Verifi&eacute;s afin de supprimer l\'avis existant, et en publier un nouveau');
define('REVIEWS_DELETION','Les motifs de suppression des avis sont disponibles');
define('HERE','ici');
define('CHRONOLOGICAL_ORDER', 'Avis affich&eacute;s par ordre chronologique');
define('COMMANDE_ORDER_DATE','suite &agrave; une commande du');
//****************************************************************

//********** Integration de module avec oscommerce dashboard ************
define('NETREVIEWS_ADMIN', 'net_reviews_admin.php');
define('FILENAME_NETREVIEWS_CHECKINSTALLATION', 'net_reviews_checkinstallation.php');
define('BOX_TITLE_NETREVIEWS_CHECKINSTALLATION', 'Check Installation');
define('BOX_TITLE_NETREVIEWS_LOGOUT', 'Log Out');
//******************************************************************* 

define('NETREVIEWS_NO_REVIEWS', 'Aucun avis n\'a encore &eacutet&eacute laiss&eacute sur ce produit.');
define('NETREVIEWS_NUMBER_REVIEWS','Calcul&eacute &agrave partir de %d avis client(s)');
define('NETREVIEWS_SORT','Trier l\'affichage des avis :');
define('NETREVIEWS_MOST_RECENT','Les plus récents');
define('NETREVIEWS_LEAST_RECENT','Les plus anciens');
define('NETREVIEWS_HIGH_RATING','Note la plus élevée');
define('NETREVIEWS_LOW_RATING','Note la plus faible');
define('NETREVIEWS_MOST_HELPFUL','Les plus utiles');
define('NETREVIEWS_MORE_REVIEWS','Afficher plus d\'avis');
define('NETREVIEWS_HELPFUL','Cet avis vous a-t-il été utile ?');