<style>
* {box-sizing:border-box}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Hide the images by default */
.mySlides {
    display: none;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 10px;
  color: white;
  font-weight: bold;
  font-size: 30px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  background-color: rgba(122,122,122,0.8);
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(122,122,122,0.5);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}
</style>

<div class="slideshow-container">

	<div class="mySlides fade">
	  <a href="https://www.generalarmystore.fr/gas/produits_liste.php?recherche=drakken&amp;x=0&amp;y=0" title="Harnais DRAKKEN pour chien de recherche et sauvetage"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Dimatex-Drakken.jpg"  alt="Harnais cynophile DRAKKEN de chez DIMATEX" style="width:100%" /></a>
	</div>

	<div class="mySlides fade">
		<a href="https://www.generalarmystore.fr/gas/2-Les-Tenues-Les-Vetements/153-Chaussures-et-Chaussettes/154-Chaussures-intervention/8739-Chaussures-MAGNUM-Strike-Force-8.0-Double-Zip.html" title="-15% sur les Strike Force DSZ de chez Magnum"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Magnum-Promo-Strike-Force.jpg" border="0" alt="Prix de lancement sur les Strike Force DSZ Magnum Boots !" style="width:100%" /></a>
	</div>
		
	<div class="mySlides fade">
		<a href="https://www.generalarmystore.fr/gas/2-Les-Tenues-Les-Vetements/153-Chaussures-et-Chaussettes/154-Chaussures-intervention/8726-Chaussures-MAGNUM-Assault-Tactical-5.0.html" title="-15% sur les nouvelles Magnum Assault Tactical 5.0 !"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Magnum-Assault-Tactical.jpg" border="0" alt="Nouveaut&eacute; MAGNUM : les Assault Tactical 5.0" style="width:100%" /></a>
	</div>
		
	<div class="mySlides fade">
		<a href="https://www.generalarmystore.fr/gas/3-Le-Matos-Le-Materiel/241-Campement/443-Bivouac/8687-Gourde-Filtrante-Water-To-Go-Outdoor.html" title="Avec Water to Go, buvez une eau pure et saine partout et tout le temps."><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Water-to-go-Gourde-Outdoor.jpg" border="0" alt="WATER to GO : gourde filtrante" style="width:100%" /></a>
	</div>
	
	<div class="mySlides fade">
		<a href="https://www.generalarmystore.fr/gas/2-Les-Tenues-Les-Vetements/170-Blousons-et-Vestes/190-Vestes/8654-Veste-Vert-OD-Softshell-Storm-2.0-TOE.html" title="D�couvrez la Softshell STORM 2.0 dans sa version Vert OD !"></a><a href="https://www.generalarmystore.fr/gas/marques.php?marque=243" title="R�parez vos fermetures � glissi�res sans travaux de couture !"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Zlideon-Curseur.jpg" border="0" alt="La R&eacute;volution ZLIDEON" style="width:100%" /></a>
	</div>
	
	<div class="mySlides fade">
		<a href="https://www.generalarmystore.fr/gas/produits_liste.php?recherche=Kit+Porte-&amp;x=0&amp;y=0" title="R�organisation de vos m�dailles et barrettes : faites le vous-m�me gr�ce aux kits Martineau"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Martineau-Kit-Barrette.jpg" border="0" alt="Porte-d&eacute;coration" style="width:100%" /></a>
	</div>
	
	<div class="mySlides fade">
		<a href="https://www.generalarmystore.fr/gas/produits_liste.php?recherche=Protection+militaire+du+Territoire&amp;x=0&amp;y=0" title="Protection du Territoire : M�daille, Dixmude, Agrafes"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/GAS-Protection-territoire.jpg" border="0" alt="Egide, Harpie, Jupiter, Sentinelle, Trident" style="width:100%" /></a>
	</div>
	
	<div class="mySlides fade">
		<a href="https://www.generalarmystore.fr/gas/2-Les-Tenues-Les-Vetements/171-Gilets-Pulls-Sweats/173-Gilets-de-combat/8528-Housse-Tactique-Gilet-Pare-Balles-Noir.html" title="Housse tactique Arktis pour GPB Gendarmerie et Police">
		<img src="https://www.generalarmystore.fr/gas/images/images_GAS/Arktis-Housse-GPB.jpg" border="0" alt="Nouvelle Housse de GPB" title="Housse tactique Arktis pour GPB" style="width:100%" /></a>
	</div>
	
	<div class="mySlides fade">
		<a href="https://www.generalarmystore.fr/gas/3-Le-Matos-Le-Materiel/125-Sacs,-Musettes,-Etuis/208-Sacoches--Musettes/8477-Trousse-individuelle-de-secours-I-TAK+-Dimatex.html" title="Attentats : avec la Trousse I-TAK, prodiguez les 1ers secours"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Dimatex-I-TAK.jpg" border="0" alt="Trousse de secours I-TAK+" style="width:100%" /></a>
	</div>
	<!-- Next and previous buttons -->
	<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
	<a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>

<div style="text-align:center">
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
</div>

<script>
/*
var slideIndex = 0;
showSlides();

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";  
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}    
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";  
    dots[slideIndex-1].className += " active";
    setTimeout(showSlides, 3000); // Change image every 3 seconds
}
*/
/*
var slideIndex = 0;
showSlides();

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none"; 
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1} 
    slides[slideIndex-1].style.display = "block"; 
    setTimeout(showSlides, 3000); // Change image every 2 seconds
}
*/
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
}
</script>