<table style="text-align: center; " border="0" cellspacing="0" cellpadding="5" width="714">
<tbody>
<tr>
<td class="Texte_Bas_De_Page" style="text-align: center;" valign="top">
<p><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Covid19.jpg" border="0" alt="Information importante : COVID-19" title="Information importante : COVID-19" width="700" height="990" /></p>
<p><strong> </strong></p>
<hr />
<strong> G&eacute;n&eacute;ral Army Store</strong> est le WebShop (ou boutique en ligne pour rester fran&ccedil;ais) des sp&eacute;cialistes du terrain : <em>Militaires,  Agents des Forces de l'Ordre (Gendarmerie, Police Nationale, Police  Municipale, etc...), Sapeurs Pompiers, Airsofteurs<strong>-</strong>Paintballeurs, Chasseurs-P&ecirc;cheurs, Campeurs, Randonneurs, mais &eacute;galement des</em><em><em> Tireurs Sportifs ainsi que des Collectionneurs, des adeptes de la  Reconstitution Historique ou plus g&eacute;n&eacute;ralement des amateurs de Mode  Militaire &amp; Tactique.</em> </em> 
<hr />
<p><a href="https://www.facebook.com/generalarmystore" target="_blank" title="Suivez l'actu G&eacute;n&eacute;ral Army Store en direct sur Facebook"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Page_Acceuil/Facebook-Gas.jpg" border="0" alt="Logo FACEBOOK" title="Suivez l'actu G&eacute;n&eacute;ral Army Store sur Facebook" width="229" height="184" /></a><a href="https://www.youtube.com/user/generalarmystore" target="_blank" title="La WEB TV G&eacute;n&eacute;ral Army Store"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Page_Acceuil/Youtube-Gas.jpg" border="0" alt="Logo YouTube" title="Retrouvez toutes nos vid&eacute;os sur notre Web TV" width="237" height="184" /></a><a href="https://twitter.com/generalarmystor" target="_blank" title="L'info GAS sur Twitter"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Page_Acceuil/Tweeter-Gas.jpg" border="0" alt="Logo Twitter" title="L'actu G&eacute;n&eacute;ral Army Store est aussi sur Twitter" width="234" height="184" /></a></p>
<hr />
<p>Nous  distribuons de l'&eacute;quipement tels que des v&ecirc;tements, des accessoires et  du mat&eacute;riel individuel et collectif, pour vos activit&eacute;s professionnelles et vos loisirs de plein  air (Outdoor).</p>
<p style="text-align: center;">Professionnel du Surplus Militaire depuis 2003, <strong>Group Army Store</strong> travaille chaque jour &agrave; s&eacute;lectionner des produits destin&eacute;s &agrave; vous  satisfaire lors de vos missions professionnelles et/ou dans la pratique de  vos loisirs. Que vous cherchiez des articles &agrave; bas prix, de qualit&eacute; ou  haut de gamme, notre site est con&ccedil;u pour r&eacute;pondre &agrave; vos attentes.</p>
<p style="text-align: center;">Notre gamme comprend plus de <span style="text-decoration: underline;">12000 r&eacute;f&eacute;rences</span> s&eacute;lectionn&eacute;es dans plus <span style="text-decoration: underline;">140 marques</span> de renom.</p>
<hr />
<p><strong>Agr&eacute;ment D.G.G.N.</strong> n&deg; 029238 du 21 Mars 2007 au 31 D&eacute;cembre 2017<br /><strong>Protocole transactionnel D.G.G.N.</strong> depuis le 1er Janvier 2018</p>
<p style="text-align: center;"><strong>Code OTAN (NCAGE)</strong> - FB7B6</p>
</td>
</tr>
</tbody>
</table>