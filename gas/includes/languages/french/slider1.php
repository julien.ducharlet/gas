<!-- Bootstrap -->
<!--
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_DIR; ?>/css/bootstrap.min.css" />
-->

<!--
https://www.generalarmystore.fr/gas/images/images_GAS/Dimatex-Drakken.jpg
https://www.generalarmystore.fr/gas/images/images_GAS/Magnum-Promo-Strike-Force.jpg
https://www.generalarmystore.fr/gas/images/images_GAS/Magnum-Assault-Tactical.jpg
https://www.generalarmystore.fr/gas/images/images_GAS/Water-to-go-Gourde-Outdoor.jpg
https://www.generalarmystore.fr/gas/images/images_GAS/Zlideon-Curseur.jpg
https://www.generalarmystore.fr/gas/images/images_GAS/Martineau-Kit-Barrette.jpg
https://www.generalarmystore.fr/gas/images/images_GAS/GAS-Protection-territoire.jpg
https://www.generalarmystore.fr/gas/images/images_GAS/Arktis-Housse-GPB.jpg
https://www.generalarmystore.fr/gas/images/images_GAS/Dimatex-I-TAK.jpg
-->
<style>
	.slideshow { width: 700px; height: 369px; overflow: hidden; border: 3px solid #F2F2F2; }
	.slideshow ul { width: 400%; height: 369px; padding:0; margin:0; list-style: none; }
	.slideshow li { float: left; }
</style>

<script type="text/javascript">
   $(function(){
      setInterval(function(){
         $(".slideshow ul").animate({marginLeft:-700},800,function(){
            $(this).css({marginLeft:0}).find("li:last").after($(this).find("li:first"));
         })
      }, 3500);
   });
</script>

<div class="slideshow">
	<ul>
		<li>
			<a href="https://www.generalarmystore.fr/gas/produits_liste.php?recherche=drakken&amp;x=0&amp;y=0" title="Harnais DRAKKEN pour chien de recherche et sauvetage"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Dimatex-Drakken.jpg" border="0" alt="Harnais cynophile DRAKKEN de chez DIMATEX" title="Harnais DRAKKEN pour chien de recherche et sauvetage" width="700" height="369" /></a>
		</li>
		
		<li>
			<a href="https://www.generalarmystore.fr/gas/2-Les-Tenues-Les-Vetements/153-Chaussures-et-Chaussettes/154-Chaussures-intervention/8739-Chaussures-MAGNUM-Strike-Force-8.0-Double-Zip.html" title="-15% sur les Strike Force DSZ de chez Magnum"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Magnum-Promo-Strike-Force.jpg" border="0" alt="Prix de lancement sur les Strike Force DSZ Magnum Boots !" title="Profitez de -15% sur lesDSZ Magnum Strike Force double side zip !" width="700" height="369" /></a>
		</li>
		
		<li>
			<a href="https://www.generalarmystore.fr/gas/2-Les-Tenues-Les-Vetements/153-Chaussures-et-Chaussettes/154-Chaussures-intervention/8726-Chaussures-MAGNUM-Assault-Tactical-5.0.html" title="-15% sur les nouvelles Magnum Assault Tactical 5.0 !"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Magnum-Assault-Tactical.jpg" border="0" alt="Nouveaut&eacute; MAGNUM : les Assault Tactical 5.0" title="D&eacute;couvrez les nouvelles Magnum Assault Tactical 5.0 !" width="700" height="369" /></a>
		</li>
		
		<li>
			<a href="https://www.generalarmystore.fr/gas/3-Le-Matos-Le-Materiel/241-Campement/443-Bivouac/8687-Gourde-Filtrante-Water-To-Go-Outdoor.html" title="Avec Water to Go, buvez une eau pure et saine partout et tout le temps."><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Water-to-go-Gourde-Outdoor.jpg" border="0" alt="WATER to GO : gourde filtrante" title="Gourge filtrante WATER to GO pour une eau pure partout et tout le temps" width="700" height="369" /></a>
		</li>
		
		<li>
			<a href="https://www.generalarmystore.fr/gas/2-Les-Tenues-Les-Vetements/170-Blousons-et-Vestes/190-Vestes/8654-Veste-Vert-OD-Softshell-Storm-2.0-TOE.html" title="D&eacute;couvrez la Softshell STORM 2.0 dans sa version Vert OD !"></a><a href="https://www.generalarmystore.fr/gas/marques.php?marque=243" title="R&eacute;parez vos fermetures &agrave; glissi&egrave;res et vos boutons sans travaux de couture !"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Zlideon-Curseur.jpg" border="0" alt="La R&eacute;volution ZLIDEON" title="R&eacute;parez vos fermetures &agrave; glissi&egrave;res et vos boutons sans travaux de couture !" width="700" height="369" /></a>
		</li>
		
		<li>
			<a href="https://www.generalarmystore.fr/gas/produits_liste.php?recherche=Kit+Porte-&amp;x=0&amp;y=0" title="R&eacute;organisation de vos m&eacute;dailles et barrettes : faites le vous-m&ecirc;me gr&acirc;ce aux kits Martineau !"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Martineau-Kit-Barrette.jpg" border="0" alt="Porte-d&eacute;coration" title="R&eacute;organisation de vos m&eacute;dailles et barrettes : faites le vous-m&ecirc;me gr&acirc;ce aux kits Martineau !" width="700" height="369" /></a>
		</li>
		
		<li>
			<a href="https://www.generalarmystore.fr/gas/produits_liste.php?recherche=Protection+militaire+du+Territoire&amp;x=0&amp;y=0" title="Protection du Territoire : M&eacute;daille, Dixmude, Agrafes"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/GAS-Protection-territoire.jpg" border="0" alt="Egide, Harpie, Jupiter, Sentinelle, Trident" title="M&eacute;daille, Dixmude et Agrafes PROTECTION du TERRITOIRE" width="700" height="369" /></a>
		</li>
		
		<li>
			<a href="https://www.generalarmystore.fr/gas/2-Les-Tenues-Les-Vetements/171-Gilets-Pulls-Sweats/173-Gilets-de-combat/8528-Housse-Tactique-Gilet-Pare-Balles-Noir.html" title="Housse tactique Arktis pour GPB Gendarmerie et Police">
			<img src="https://www.generalarmystore.fr/gas/images/images_GAS/Arktis-Housse-GPB.jpg" border="0" alt="Nouvelle Housse de GPB" title="Housse tactique Arktis pour GPB" width="700" height="369" /></a>
		</li>
		
		<li>
			<a href="https://www.generalarmystore.fr/gas/3-Le-Matos-Le-Materiel/125-Sacs,-Musettes,-Etuis/208-Sacoches--Musettes/8477-Trousse-individuelle-de-secours-I-TAK+-Dimatex.html" title="Attentats : avec la Trousse I-TAK, prodiguez les 1ers secours"><img src="https://www.generalarmystore.fr/gas/images/images_GAS/Dimatex-I-TAK.jpg" border="0" alt="Trousse de secours I-TAK+" title="ATTENTATS : I-TAK+, la trousse de secours ultime" width="700" height="369" /></a>
		</li>
	</ul>
</div>










