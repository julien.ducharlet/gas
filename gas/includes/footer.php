<?php
/* Module affichant le footer de la page web comprenant la zone avec 'Paiement 100% s�curis�', etc... Plus la zone des liens 'conditions g�n�rales', 'qui sommes-nous ?', etc...*/
?>

<!-- bande violette qui fait toute la largeur du navigateur en dessous du footer -->
<div id="background_footer" align="center"></div>

<div id="footer">
	<div class="bloc_texte">
        <div class="texte_footer">Paiement 100% s�curis�</div><div class="icone_paiement"></div> 
        <div class="texte_footer">Achat satisfait ou rembours�*</div><div class="icone_satisfait"></div>
        <div class="texte_footer">Livraison par Colissimo</div><div class="icone_expedition"></div>
        <div style="clear: both;"></div>
    </div>
</div>

<div id="footer_2">
    <div id="footer_liens">
        <a href="<?php echo BASE_DIR; ?>/conditions_generales.php" class="footer_liens" title="">Conditions g&eacute;n&eacute;rales</a>
        &nbsp;|&nbsp;
        <a href="<?php echo BASE_DIR; ?>/qui_sommes_nous.php" class="footer_liens" title="">Qui sommes nous ?</a>
        &nbsp;|&nbsp;
        <a href="<?php echo BASE_DIR; ?>/contact.php" class="footer_liens" title="">Contactez nous</a>
        <!--&nbsp;|&nbsp;
        <a href="<?php //echo BASE_DIR; ?>/partenaires.php" class="footer_liens" title="">Partenaires</a> -->
		&nbsp;|&nbsp;
        <a href="<?php echo BASE_DIR; ?>/nouveautes.php" class="footer_liens" title="">Les Nouveaut�s</a>
		<?php
		if((isset($_SESSION) && !empty($_SESSION['customer_id']) && $_SESSION['customers_type']!=3) || empty($_SESSION['customer_id'])) { ?>
                
            &nbsp;|&nbsp; 
            <a href="<?php echo BASE_DIR; ?>/promotions.php" class="footer_liens" title="Visualisez toutes les promotions ou les solde">Les Promotions</a>
        <?php } ?>
        &nbsp;|&nbsp;
        <a href="https://www.avis-verifies.com/avis-clients/generalarmystore.fr" class="footer_liens" title="L'avis des client de Group Army Store" target="_blank">Les avis clients</a>
    </div>
    <div class="icones_cartes"></div>
</div>

<div id="copyright" itemscope="" itemtype="https://data-vocabulary.org/Review-aggregate">
	Copyright &copy; 2006 -  <?php echo date('Y', time()); ?> Group Army Store / General Army Store Siret : 447 513 342 00033 - <a href="<?php echo BASE_DIR; ?>/plan_du_site.php">Plan du site</a><br>
	Conception et R�alisation de la boutique <a href="https://www.pimentbleu.fr/membres/thierry-poulain/prestations/creation-de-boutique-e-commerce/" target="_blank" title="Agence 360� Web et multim�dia de Montpellier - Sp�cialiste du E-Commerce">Agence Piment Bleu</a>
	 - Votre IP : <?php echo $_SERVER['REMOTE_ADDR']; ?>
	

	<?php
		// Permet de r�cup�rer les avis g�n�ral de la boutique 
		$handle = fopen("http://cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/AWS/c2aefb07-1640-9f64-294e-c74d06179a2a_infosite_nafnor.txt", "r");
		$lecture_fichier = fgets($handle);
			// on r�cup�re le nombre d'avis et la note global
			$valeur = explode(";" , $lecture_fichier);
		fclose($handle);
	?>

	<?php
	//if ($nom_page != "produits_fiches") {
		echo '<br><br><a href="https://www.avis-verifies.com/avis-clients/generalarmystore.fr" target="_blank" style="text-decoration:none;">Nos clients nous font confiance, retrouvez la note de <span itemprop="itemreviewed">General Army Store</span> sur Avis V�rifi�s :
			<span itemprop="itemreviewed"></span>
				<span itemprop="rating" itemscope="" itemtype="https://data-vocabulary.org/Rating">
				<span itemprop="average">' . round($valeur[1]*2, 2) . '</span> /
				<span itemprop="best">10</span> �toiles
			</span>
			(<span itemprop="votes">' . $valeur[0] . '</span> notes)</a>
		</div>';
	//}
	?>