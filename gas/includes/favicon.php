<?php
/* AFFICHAGE FAVICON AVEC DIFFERENCE ENTRE FIREFOX ET IE */

	//S'il s'agit de IE on affiche favicon.ico (statique)
	/*if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE' ) !== FALSE ) { 
		echo '<link rel="shortcut icon" href="' . BASE_DIR . '/template/base/favicon/favicon.ico" />'; 
	
	
	//Sinon avec n'importe quel autre navigateur on affiche l'icone animée en .gif
	} else { 
		echo '<link rel="shortcut icon" href="' . BASE_DIR . '/template/base/favicon/animated_favicon1.gif" />'; 
	}*/	
?>

<link rel="apple-touch-icon" sizes="180x180" href="../gas/images/favicon/apple-touch-icon.png?v=M4mpyb56za">
<link rel="icon" type="image/png" sizes="32x32" href="../gas/images/favicon/favicon-32x32.png?v=M4mpyb56za">
<link rel="icon" type="image/png" sizes="16x16" href="../gas/images/favicon/favicon-16x16.png?v=M4mpyb56za">
<link rel="manifest" href="../gas/images/favicon/site.webmanifest?v=M4mpyb56za">
<link rel="mask-icon" href="../gas/images/favicon/safari-pinned-tab.svg?v=M4mpyb56za" color="#5bbad5">
<link rel="shortcut icon" href="../gas/images/favicon/favicon.ico?v=M4mpyb56za">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
