<?php

if (isset($_GET['pID']) && isset($_GET['cPath']) && !empty($_GET['pID']) && !empty($_GET['cPath'])) {
	
	session_start();
	include_once('../configure.php');
	include_once('../fonctions/general.php');
	include_once('../fonctions/base_de_donnees.php');
	include_once(DATABASE_TABLE_DIR);
	
	tep_db_connect();
	
	$product_id = (int)$_GET['pID'];
	$cPath = explode('_', $_GET['cPath']);
	$qty = (isset($_GET['qty']) && (int)$_GET['qty']>1) ? (int)$_GET['qty'] : 1;
	
	$marque = (int)$cPath[0];
	$modele = (int)$cPath[1];
	
	$product_query = tep_db_query("SELECT pd.products_name, pd.products_url, pd.products_description, 
									  p.products_model, p.products_price, p.products_bimage, p.products_quantity, p.products_ecotaxe, p.products_video, p.products_video_preview, p.products_ref_origine, p.products_garantie, p.products_code_barre, p.products_status, p.products_date_available, p.products_manage_stock,
									  ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1,
									  ppbq.part_price_by_5, ppbq.pro_price_by_5, ppbq.rev_price_by_5, ppbq.adm_price_by_5,
									  ppbq.part_price_by_10, ppbq.pro_price_by_10, ppbq.rev_price_by_10, ppbq.adm_price_by_10,
									  s.specials_new_products_price,
									  debut_vente_flash, fin_vente_flash, pvf.prix_vente
                                       FROM " . TABLE_PRODUCTS_DESCRIPTION . " as pd 
									   inner join " . TABLE_PRODUCTS . " as p on pd.products_id = p.products_id
									   left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id
									   left join ". TABLE_SPECIALS ." as s on (p.products_id=s.products_id and (expires_date >NOW() or expires_date='0000-00-00 00:00:00'))
									   left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash='0000-00-00')))
                                       WHERE pd.products_id = " . $product_id);
	
	$product_data = tep_db_fetch_array($product_query);
	
	$infos_options_query = tep_db_query("SELECT pov.products_options_values_name, po.products_options_name, pa.options_id, pa.options_values_id , options_quantity
														 FROM " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_OPTIONS . " po, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov 
														 WHERE pa.products_id = '" . $product_id . "' AND pa.options_values_id = pov.products_options_values_id AND pa.options_id = po.products_options_id AND pa.options_dispo = '1' 
														 ORDER BY pa.products_options_sort_order");
	
?>
	<div style="float:right;">
		<img class="close_lightbox pointer" src="<?php echo BASE_DIR; ?>/template/base/boutons/icone_fermer.png" title="fermer"/>
	</div>
	<div id="lightbox_product">
		<h1>Vous venez d'ajouter ce produit � votre panier</h1>
       	<p><?php echo bda_product_name_transform($product_data['products_name'], $modele); ?></p>    
	</div>
	
	<div id="options">
		<?php
		$i = 0;		
		while ($products_options = tep_db_fetch_array($infos_options_query)) {
			
			if ($i==0) {
				echo $products_options['products_options_name']; 
				echo '<br>';
				echo '<table><tr>';
			}
            
            if ($i%2==0 && $i!=0) {
                echo '</tr><tr>';
            }
            
            echo '<td>';
				echo ($products_options['options_quantity']<=0)
					? '<input type="radio" disabled="disabled" name="product_options" value="{'. $products_options['options_id'] .'}'. $products_options['options_values_id'] .'" />'
					: '<input type="radio" name="product_options" value="{'. $products_options['options_id'] .'}'. $products_options['options_values_id'] .'" />';
					
                echo $products_options['products_options_values_name'];
				if($product_data['products_manage_stock']=="destock_visible") {
					
					echo ($products_options['options_quantity']<=0) ? '&nbsp;(0)' : '&nbsp;('. $products_options['options_quantity'] .')';
				}
            echo '</td>';
			
			$i++;
        }
        ?>
        </tr></table>
    </div>
    <div class="images product_images">
        <img class="product_back_to_product" alt="Continuer mes achats" src="<?php echo BASE_DIR;?>/template/base/ajout_panier_lb/bouton_continuer-achats.jpg"/>
        <img class="product_validate_basket" alt="Valider mon panier" src="<?php echo BASE_DIR;?>/template/base/ajout_panier_lb/bouton_valider-panier.jpg"/>COUCOU
    </div>

<?php
}
?>