<?php
require("../page_top.php");
?>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/panier_livraison.js"></script>
    <div id="lightbox_ajout_panier" style="padding:10px 10px 10px 10px;">
    <?php
	 $id_modif = (int)$_GET['modif'];
	 if($id_modif!='')
	 {
		$adresse_defaut=tep_db_query("select * from address_book where address_book_id=".$id_modif."");
		$res=tep_db_fetch_array($adresse_defaut);
	 }
	 
	?>
    <form name="adresse" method="post" action="<?php echo BASE_DIR;?>/panier_livraison.php">
    	 <div style="float:right;"><a href="<?php echo BASE_DIR;?>/panier_livraison.php">
         	<img src="template/base/boutons/icone_fermer.png" title="fermer" ></a>
         </div>
         <div class="clear"></div>
         <div style="text-align:left; margin-bottom:10px;">
         	<input type="hidden" name="id_adresse" value="<?php if($id_modif!=''){echo $id_modif;} ?>" />
            Titre ( ex : au bureau ):<br />
           <input type="text" id="titre_address" name="titre_address" value="<?php if($id_modif!=''){echo $res['entry_address_title'];} ?>" size="65" />
         </div>
         <div style="text-align:left; margin-bottom:10px;">
            Civilit&eacute; 
            <input type="radio" id="mr" name="civi" value="m"  <?php if($id_modif!=''){if($res['entry_gender']=='m') echo 'checked="checked"';} ?> />Mr
            <input type="radio" id="mme" name="civi" value="f" <?php if($id_modif!=''){if($res['entry_gender']=='f') echo 'checked="checked"';} ?> />Mme
            <input type="radio" id="mlle" name="civi" value="d"  <?php if($id_modif!=''){if($res['entry_gender']=='d') echo 'checked="checked"';} ?> />Mlle
            <div class="clear"></div>
         </div>
         <div style="text-align:left; margin-bottom:10px;">
           <div style="float:left;">
             Nom :<br /> <input type="text" id="nom_address" name="nom_address" value="<?php if($id_modif!=''){echo $res['entry_lastname'];} ?>" />
           </div>
           <div style="float:right;">
             Prenom :<br /> <input type="text" id="prenom_address" name="prenom_address" value="<?php if($id_modif!=''){echo $res['entry_firstname'];} ?>" />
           </div>
           <div class="clear"></div>
         </div>
         <div style="text-align:left; margin-bottom:10px;">
           Societe :<br /> <input type="text" id="societe_address" name="societe_address" value="<?php if($id_modif!=''){echo $res['entry_company'];} ?>" size="65" />
         </div>
         <div style="text-align:left; margin-bottom:10px;">
           Adresse :<br /> <input type="text" id="a_address" maxlength="31" name="a_address" value="<?php if($id_modif!=''){echo $res['entry_street_address'];} ?>" size="65" />
         </div>
         <div style="text-align:left; margin-bottom:10px;">
           Complement d'adresse :<br /> <input type="text" id="ap_address" maxlength="31" name="ap_address" value="<?php if($id_modif!=''){echo $res['entry_suburb'];} ?>" size="65" />
         </div>
         <div style="text-align:left; margin-bottom:10px;">
           <div style="float:left;">
             Code postal :<br /> <input type="text" id="cp_address" name="cp_address" value="<?php if($id_modif!=''){echo $res['entry_postcode'];} ?>" />
           </div>
           <div style="float:right;">
             Ville :<br /> <input type="text" id="ville_address" name="ville_address" value="<?php if($id_modif!=''){echo $res['entry_city'];} ?>" />
           </div>
           <div class="clear"></div>
         </div>
         <div style="text-align:left; margin-bottom:10px;">
           <div style="float:left;">Pays :<br />
              <select id="pays" name="pays">  
                 <option value="pays">pays</option>   
                   	 <?php  
                       $req_pays=tep_db_query("Select countries_id, countries_name From countries order by countries_name");
                       while($res2=tep_db_fetch_array($req_pays))
                        {
                   	   ?>
                       <option <?php if($id_modif!='')if($res['entry_country_id']==$res2['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res2['countries_id'];?>">
							   <?php echo $res2['countries_name'];?>
                       </option>
                     <?php
                         }
                     ?>
               </select>
             </div>
             <div class="clear"></div>
        </div>
        <?php if($id_modif!=''){ ?>
        <input type="hidden" name="mod" />
        <div style="text-align:center; margin-top:30px;"><img src="template/base/boutons/enregistrer_les_modifications.jpg" onClick="verif_modif(0);" /></div>
        <?php 
		}
		else{
		?>
         <input type="hidden" name="add" />
         <input type="checkbox" name="add_livr" value="1" />Choisir cette adresse pour la livraison
         <div style="text-align:center; margin-top:30px;"><img src="template/base/boutons/enregistrer_adresse.jpg" onClick="verif_modif(1);" /></div>
        <?php 
		}?>
      </form>
      <br /><br /><br />
    </div>
</body>
</html>