<?php
	
	//pour rajouter une zone, modifier le champ set de la table banners (le nom de la zone doit correspondre au champ de la table par exemple : $PAGES['zone1'] => ajout de zone1 dans la bdd)
	//liste des pages (variable globale) pour l'ajout d'une page la placer dans une zone
	$PAGES = array();//'zone1', 'zone2', 'zone3');
	
	$PAGES['zone accueil'][] = 'index.php';
		
	$PAGES['zone article'][] = 'categories_liste.php';
	$PAGES['zone article'][] = 'marques.php';
	$PAGES['zone article'][] = 'modeles_liste.php';
	$PAGES['zone article'][] = 'nouveautes.php';
	$PAGES['zone article'][] = 'partenaires.php';
	$PAGES['zone article'][] = 'plan_du_site.php';
	$PAGES['zone article'][] = 'presse.php';
	$PAGES['zone article'][] = 'produits_fiches.php';
	$PAGES['zone article'][] = 'produits_liste.php';
	$PAGES['zone article'][] = 'tableau_des_tailles.php';
			
	$PAGES['zone panier'][] = 'panier.php';
	$PAGES['zone panier'][] = 'panier_livraison.php';
	$PAGES['zone panier'][] = 'panier_paiement.php';
	$PAGES['zone panier'][] = 'panier_recapitulatif.php';
	$PAGES['zone panier'][] = 'panier_remerciement.php';
	
	//$PAGES['zone mon compte'][] = 'login.php';
	$PAGES['zone mon compte'][] = 'account.php';
	$PAGES['zone mon compte'][] = 'account_edit.php';
	$PAGES['zone mon compte'][] = 'order.php';
	$PAGES['zone mon compte'][] = 'address_book.php';
	$PAGES['zone mon compte'][] = 'sponsorship.php';
	$PAGES['zone mon compte'][] = 'order.php';
	$PAGES['zone mon compte'][] = 'order_detail.php';
	$PAGES['zone mon compte'][] = 'devis.php';
	$PAGES['zone mon compte'][] = 'devis_details.php';
	$PAGES['zone mon compte'][] = 'electronic_purse.php';
	$PAGES['zone mon compte'][] = 'newsletter.php';
	$PAGES['zone mon compte'][] = 'logout.php';
	//$PAGES['zone mon compte'][] = 'create_account_pro.php';
	//$PAGES['zone mon compte'][] = 'create_account_part.php';
	
	$PAGES['zone mon compte'][] = 'inscription.php';	
	$PAGES['zone mon compte'][] = 'inscription-particulier.php';	
	$PAGES['zone mon compte'][] = 'inscription-professionnel.php';	
	$PAGES['zone mon compte'][] = 'inscription-administration.php';	
	$PAGES['zone mon compte'][] = 'inscription-association.php';	
	$PAGES['zone mon compte'][] = 'inscription-entreprise.php';	
	$PAGES['zone mon compte'][] = 'inscription-revendeur.php';
	$PAGES['zone mon compte'][] = 'connexion.php';		
	
	$PAGES['zone page annexe'][] = 'faq.php';
	$PAGES['zone page annexe'][] = 'conditions_generales.php';
	$PAGES['zone page annexe'][] = 'contact.php';
	$PAGES['zone page annexe'][] = 'infos_livraison.php';
	$PAGES['zone page annexe'][] = 'promotions.php';
	$PAGES['zone page annexe'][] = 'qui_sommes_nous.php';
	$PAGES['zone page annexe'][] = 'recrutement.php';	
	$PAGES['zone page annexe'][] = 'details_pour_administration.php';	
	$PAGES['zone page annexe'][] = 'details_pour_pro.php';	
?>