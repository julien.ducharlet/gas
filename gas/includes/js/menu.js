/* Auteur : Paul
   Fonctions JavaScript concernant le menu */

// Montre les marques d'un rayon
function montre(id) {
	if (document.getElementById) {
		document.getElementById(id).style.display="block";
	} else if (document.all) {
		document.all[id].style.display="block";
	} else if (document.layers) {
		document.layers[id].display="block";
	}
}

// Cache les marques d'un rayon
function cache(id) {
	if (document.getElementById) {
		document.getElementById(id).style.display="none";
	} else if (document.all) {
		document.all[id].style.display="none";
	} else if (document.layers) {
		document.layers[id].display="none";
	}
}