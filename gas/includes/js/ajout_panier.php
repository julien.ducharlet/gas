<?php
/* Page AJAX d'ajout au panier */

require("../page_top.php");
require("../fonctions/fonctions_panier.php");

tep_db_connect();

if (isset($_GET['id_article']) && $_GET['id_article'] != '') {
	ajout_panier($_GET['id_rayon'], $_GET['id_article'], $_GET['qte_article']);
} else if (isset($_GET['id_pack']) && $_GET['id_pack'] != '') {
	ajout_pack_panier($_GET['id_rayon'], $_GET['id_pack'], $_GET['qte_pack']);
}

?>