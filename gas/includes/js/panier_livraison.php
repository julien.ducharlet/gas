<?php
/* Auteur : Sami
   Fonctions concernant d'affecter des variables sessions pour le choix de l'adresse de livraison commande */
   
session_start();
require("../configure.php");
require("../fonctions/base_de_donnees.php");
require("../fonctions/general.php");
require(DATABASE_TABLE_DIR);
tep_db_connect();

$id = (int)$_GET['id_adresse'];
$frais=$_GET['frais'];
echo $_SESSION['frais_livraison']=$frais;
echo $_SESSION['adresse_livraison']=$id;

$req=tep_db_query("select entry_country_id from ".TABLE_ADDRESS_BOOK." where address_book_id=".$id."");
$res=tep_db_fetch_array($req);	
echo $_SESSION['customer_country_id']=$res['entry_country_id'];
?>