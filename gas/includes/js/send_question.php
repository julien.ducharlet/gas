<?php

require("../page_top.php");
require("../fonctions/fonctions_panier.php");

tep_db_connect();

if (isset($_GET['ajax']) && $_GET['ajax']=1) {
	
	// v�rification du capcha
	if( (isset($_POST['code']) && chk_crypt($_POST['code'])) || (!isset($_POST['code']) && isset($_SESSION) && !empty($_SESSION['customer_id']))) {
		
		$headers ='From: "' . NOM_DU_SITE . '"<' . MAIL_INFO . '>'."\n";
		$headers .='Reply-To: ' . $_POST['custom_mail'] ."\n";
		$headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
		$mail_content = 'Nom du client : <strong>'. utf8_decode($_POST['custom_name']) .'</strong><br />';
		$mail_content .= 'T�l�phone du client : <strong>'. htmlspecialchars(addslashes($_POST['custom_tel'])) .'</strong><br /><br />';
		$mail_content .= 'Nom de l\'Article : <a href="'. htmlspecialchars(addslashes($_POST['current_url'])) .'">'. utf8_decode($_POST['current_product']) .'</a><br /><br />';
		
		if(!isset($_POST['code'])) {//client logu�
			
			$mail_content .= 'ID du client : <a href="'. WEBSITE . BASE_DIR_ADMIN .'/client_edit.php?cID='. $_SESSION['customer_id'] .'&action=edit">'. $_SESSION['customer_id'] .'</a><br />';
		}
		
		$mail_content .= 'Email du client : '. $_POST['custom_mail'] .'<br /><br />';
		
		$mail_content .= '<a style="text-decoration:underline;">Question du client : </a><br />';
		$mail_content .= nl2br(stripslashes(utf8_decode($_POST['custom_question'])));
		
		mail(MAIL_INFO,  'Question sur un Article',  $mail_content, $headers);
		
		echo 'alert=Votre message a bien �t� envoy�, merci';
		echo '&valid=1';
	}
	else {
		
		echo 'alert=Code erron�';
		echo '&valid=0';
	}
}

?>