<?php

session_start();
require("../configure.php");
require("../fonctions/base_de_donnees.php");
require("../fonctions/general.php");
require("../fonctions/fonctions_panier.php");
require(DATABASE_TABLE_DIR);

tep_db_connect();

if($_GET['ajax']==1) {

	$query = 'select * from '. TABLE_BANNERS .' where banners_id='. $_GET['banner_id'];
	$query = tep_db_query($query);
	
	//on vérifie que la bannière existe bien
	if(tep_db_num_rows($query) == 1) {
		
		$data = tep_db_fetch_array($query);
		
		$query = 'select * from '. TABLE_BANNERS_HISTORY .' where banners_id='. $data['banners_id'] .' and banners_history_date like \''. date('Y-m-d') .'%\'';
		$query = tep_db_query($query);
		
		//on vérifie qu'il y a une entrée dans la table banners_history
		if(tep_db_num_rows($query) == 0) {
			
			$query = 'insert into '. TABLE_BANNERS_HISTORY .' (banners_id, banners_history_date) values('. $data['banners_id'] .', \''. date('Y-m-d H:i:s') .'\')';
			
			tep_db_query($query);
		}
		
		
		switch($_GET['type']) {
			
			case 'show':
			
				$query_update = 'update '. TABLE_BANNERS_HISTORY .' set banners_shown=banners_shown+1 where banners_id='. $data['banners_id'] .' and banners_history_date like \''. date('Y-m-d') .'%\'';
				tep_db_query($query_update);
				
			break;
			
			case 'click':
				
				$query_update = 'update '. TABLE_BANNERS_HISTORY .' set banners_clicked=banners_clicked+1 where banners_id='. $data['banners_id'] .' and banners_history_date like \''. date('Y-m-d') .'%\'';
				tep_db_query($query_update);
				
				if (!empty($data['banners_url'])) echo $data['banners_url'];
			break;
		}
		
	}
}
?>