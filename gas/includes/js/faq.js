

  function showHide(elementID) {
    var desc = null;

    if (document.getElementById) {
      desc = document.getElementById("cnt_desc_" + elementID);
    } else if (document.all) {
      desc = document.all["cnt_desc_" + elementID];
    } else if (document.layers) {
      desc = document.layers["cnt_desc_" + elementID];
    }

    if (desc) {
      if (desc.style.display == 'none') {
        expand(elementID);
      } else {
        collapse(elementID);
      }
    }
  }

  function expand(elementID) {
    var cnt = null;
    var desc = null;
    var icon = null;

    if (document.getElementById) {
      cnt = document.getElementById("cnt_" + elementID);
      desc = document.getElementById("cnt_desc_" + elementID);
      icon = document.getElementById("cnt_icon_" + elementID);
    } else if (document.all) {
      cnt = document.all["cnt_" + elementID];
      desc = document.all["cnt_desc_" + elementID];
      icon = document.all["cnt_icon_" + elementID];
    } else if (document.layers) {
      cnt = document.layers["cnt_" + elementID];
      desc = document.layers["cnt_desc_" + elementID];
      icon = document.layers["cnt_icon_" + elementID];
    }

    if (desc.style.display == 'none') {
      cnt.style.backgroundColor = '#f1f1f0';
      cnt.style.border = '1px dotted #000000';
      cnt.style.padding = '5px';
      cnt.style.marginBottom = '5px';
      desc.style.display = 'block';
      icon.src = "template/base/icones_faq_family/logo-loupe-moins.gif"
    }
  }

  function collapse(elementID) {
    var cnt = null;
    var desc = null;
    var icon = null;

    if (document.getElementById) {
      cnt = document.getElementById("cnt_" + elementID);
      desc = document.getElementById("cnt_desc_" + elementID);
      icon = document.getElementById("cnt_icon_" + elementID);
    } else if (document.all) {
      cnt = document.all["cnt_" + elementID];
      desc = document.all["cnt_desc_" + elementID];
      icon = document.all["cnt_icon_" + elementID];
    } else if (document.layers) {
      cnt = document.layers["cnt_" + elementID];
      desc = document.layers["cnt_desc_" + elementID];
      icon = document.layers["cnt_icon_" + elementID];
    }

    if (desc.style.display != 'none') {
      cnt.style.backgroundColor = '#FFFFFF';
      cnt.style.border = '0px dotted #000000';
      cnt.style.padding = '5px';
      cnt.style.marginBottom = '5px';
      desc.style.display = 'none';
      icon.src = "template/base/icones_faq_family/logo-loupe-plus.gif"
    }
  }

  function expandAll() {
    var cnt = null;

    if (document.body.getElementsByTagName) {
      cnt = document.body.getElementsByTagName('DIV');
    } else if (document.body.all) {
      cnt = document.body.all.tags('DIV');
    }

    if (cnt) {
      for (var i=0; i<cnt.length; i++) {
        if (cnt[i].id.substring(0, 4) == 'cnt_') {
          if (cnt[i].id.substring(0, 5) != 'cnt_d') {
            expand(cnt[i].id.substring(4));
          }
        }
      }
    }
  }

  function collapseAll() {
    var cnt = null;

    if (document.body.getElementsByTagName) {
      cnt = document.body.getElementsByTagName('DIV');
    } else if (document.body.all) {
      cnt = document.body.all.tags('DIV');
    }

    if (cnt) {
      for (var i=0; i<cnt.length; i++) {
        if (cnt[i].id.substring(0, 4) == 'cnt_') {
          if (cnt[i].id.substring(0, 5) != 'cnt_d') {
            collapse(cnt[i].id.substring(4));
          }
        }
      }
    }
  }
