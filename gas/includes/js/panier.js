/* Auteur : Paul
   Fonctions JavaScript concernant le panier */

// V�rification de la quantit� afin d'adapter le prix unitaire
function verif_prix_qte(id_article, id_prix_unit, id_qte) {
	var qte = document.getElementById(id_qte).value;
	
	var req;

	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject("Msxml2.XMLHTTP");

	req.onreadystatechange = function()
	{
		if(req.readyState == 4)
		{
			if(req.status == 200)
			{
				if (qte >= 5) {
					document.getElementById(id_prix_unit).innerHTML = req.responseText;
				}
			}
			else
			{
				alert("Error: returned status code " + req.status + " " + req.statusText);
			}
		}		
	};

	req.open("GET", "includes/js/verif_prix_qte.php?id_article="+id_article+"&qte="+qte, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}


// Mise � jour du prix total dans le panier
function prix_tot(id_prix_tot, id_prix_unit, id_qte, id_article, type, taxe, tva) {
	
	if (document.getElementById(id_qte).value > 0) {
		
		qte = document.getElementById(id_qte).value;
	}
	else {
		
		qte = 1;
		document.getElementById(id_qte).value = 1;
	}
	
	var req;

	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject("Msxml2.XMLHTTP");

	req.onreadystatechange = function()
	{
		if(req.readyState == 4)
		{
			if(req.status == 200)
			{
				
				if(req.responseText != '') {
					
					prix = req.responseText.split('&');
					prix_ht = prix[0].split('=');
					prix_ttc = prix[1].split('=');
					
					document.getElementById(id_prix_unit).innerHTML = format_to_money(prix_ttc[1]);
				}
				else {
					
					var indice = id_qte.split('_');
					var id_prix_1 = 'prix_1_'+indice[1];
					var id_prix_5 = 'prix_5_'+indice[1];
					var id_prix_10 = 'prix_10_'+indice[1];
				}
				
				var prix_tot = document.getElementById(id_prix_unit).innerHTML * qte;
				document.getElementById(id_prix_tot).innerHTML = format_to_money(prix_tot);
				
				var total = 0;
				var i = 1;
				while (document.getElementById('prix_total_'+i) != null) {
					
					total += parseInt(document.getElementById('prix_total_'+i).innerHTML*100)/100;
					i++;
				}
				
				document.getElementById('ss_total_ht').innerHTML = format_to_money(total/taxe);
				
				if(tva == 0) {//suisse : pas de tva
					
					document.getElementById('total').innerHTML = format_to_money(total/taxe);
					
				}
				else {//tva � payer
					
					if(taxe > 1) {//TTC : no calcul
						
						document.getElementById('total').innerHTML = format_to_money(total);
					}
					else {
						
						document.getElementById('total').innerHTML = format_to_money(total*1.2);
					}
					
					document.getElementById('tva').innerHTML = format_to_money(document.getElementById('total').innerHTML - document.getElementById('ss_total_ht').innerHTML);
				}
			}
			else
			{
				alert("Error: returned status code " + req.status + " " + req.statusText);
			}
		}		
	};
	
	if (type == 'article') {
		
		req.open("GET", "includes/js/maj_panier.php?id_article="+id_article+"&qte="+qte, true);
	}
	else {
		
		req.open("GET", "includes/js/maj_panier.php?id_pack="+id_article+"&qte="+qte, true);
	}
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}