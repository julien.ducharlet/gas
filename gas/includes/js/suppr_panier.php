<?php
/* Auteur : Paul
   Page AJAX supprimant un objet du panier */

session_start();
require("../configure.php");
require("../fonctions/base_de_donnees.php");
require("../fonctions/general.php");
require(DATABASE_TABLE_DIR);

tep_db_connect();

if (isset($_GET['id_article']) && $_GET['id_article'] != '') {
	suppr_panier($_GET['id_article']);
} else if (isset($_GET['id_pack']) && $_GET['id_pack'] != '') {
	suppr_pack_panier($_GET['id_pack']);
}

?>