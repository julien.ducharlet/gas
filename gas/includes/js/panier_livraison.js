/* Fonctions concernant le choix de l'adresse de livraison du client lors du passage de commande */

function verif_modif(id){
	var titre=document.getElementById("titre_address").value;
	var prenom=document.getElementById("prenom_address").value;
	var nom=document.getElementById("nom_address").value;
	var adresse=document.getElementById("a_address").value;
	var cp=document.getElementById("cp_address").value;
	var ville=document.getElementById("ville_address").value;
	var pays=document.getElementById("pays").value;

	var verif=true;
	var err='';
	
	if(id==1) {
		if(document.getElementById("mr").checked==false && document.getElementById("mme").checked==false && document.getElementById("mlle").checked==false)
		{ err+="Veuillez selectionner une civilit� \n"; verif=false;}
	}
	if(titre==''){err+='Veuillez indiquer un titre � l\'adresse\n'; verif=false;}
	if(prenom==''){err+='Veuillez indiquer un pr�nom \n'; verif=false;}
	if(nom==''){ err+='Veuillez indiquer un nom \n';verif=false;}
	if(adresse==''){ err+='Veuillez indiquer une adresse \n'; verif=false;}
	if(cp==''){err+='Veuillez indiquer un code postal \n'; verif=false;}
	if(ville==''){err+='Veuillez sp�cifier une ville \n'; verif=false;}
	if(pays==''){ err+='Veuillez s�lectionner un pays \n'; verif=false;}

	if(verif==false){alert(err);} else {document.adresse.submit();}
}

function modifier(id)
{	
	var req;
	var frais=document.getElementById("frais"+id).value;
	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject("Msxml2.XMLHTTP");

	req.onreadystatechange = function()
	{
		if(req.readyState == 4)
		{
			if(req.status == 200)
			{
				var r=req.responseText;
				var chg=document.getElementById("cache").value;
				document.getElementById("block_info"+chg).style.display="none";
				document.getElementById("prix_livraison"+chg).style.display="none";
				document.getElementById("cache").value=id;
				document.getElementById("block_info"+id).style.display="block";
				document.getElementById("prix_livraison"+id).style.display="block";
				document.location.href = "panier_livraison.php"; 
			}
			else
			{
				alert("Error: returned status code " + req.status + " " + req.statusText);
			}
		}		
	};
	req.open("GET", "includes/js/panier_livraison.php?id_adresse="+id+"&frais="+frais, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}