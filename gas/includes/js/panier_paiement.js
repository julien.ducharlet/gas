/* Fonctions concernant le choix du paiment du client lors du passage de commande */
   
function continu() {
	if (document.getElementById("cgv").checked==true )
	{document.getElementById("continue").style.display='block';}
	else document.getElementById("continue").style.display='none';
}

function continue_procedure(mod) {
	var cpt=document.getElementById("compteur").value;
	var ok=false;
	for(i=0;i<cpt;i++) {
		if(document.getElementById("paiement"+i).checked==true) { 
			ok=true; 
		}
	}
  
	var verif=true;
	var err='';
	if (ok==false) { 
		verif=false; err+="Merci de sélectionner un mode de règlement.\n"; 
	}
	if(document.getElementById("cgv").checked==false) { 
		verif=false; err+="Pour finaliser votre commande, veuillez prendre connaissance et valider nos Conditions Générales de Vente";
	}
  
	if (verif==true) {
		if (mod==1) {
			document.commentaire.submit();
		} else {
			document.location.href="panier_recapitulatif.php"
		}
	} else { 
		alert(err);
	}
}

function paiement(id) {	
	var req;
	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject("Msxml2.XMLHTTP");

	req.onreadystatechange = function()
	{
		if(req.readyState == 4) {
			if(req.status == 200) {
				var r=req.responseText;
			} else {
				alert("Error: returned status code " + req.status + " " + req.statusText);
			}
		}		
	};
	req.open("GET", "includes/js/panier_paiement.php?id_paiement="+id, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}