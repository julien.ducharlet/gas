/* Fonctions JavaScript concernant la liste des articles */

// Change le type de vue pour la page qui liste les articles d'un mod�le
function change_type(id) {
	var base_dir = document.getElementById("base_dir").value;
	
	if (id == 'images') {
		document.getElementById('images').style.backgroundImage='url(' + base_dir + 'template/base/liste_produits/icone_liste-img_actif.png)';
		document.getElementById('liste').style.backgroundImage='url(' + base_dir + 'template/base/liste_produits/icone_liste-txt_gris.png)';
		document.getElementById('icones').style.backgroundImage='url(' + base_dir + 'template/base/liste_produits/icone_liste-txt-img_gris.png)';
		document.getElementById('articles_images').style.display='block';
		document.getElementById('articles_liste').style.display='none';
		document.getElementById('articles_icones').style.display='none';
	}
	
	if (id == 'icones') {
		document.getElementById('images').style.backgroundImage='url(' + base_dir + 'template/base/liste_produits/icone_liste-img_gris.png)';
		document.getElementById('liste').style.backgroundImage='url(' + base_dir + 'template/base/liste_produits/icone_liste-txt_gris.png)';
		document.getElementById('icones').style.backgroundImage='url(' + base_dir + 'template/base/liste_produits/icone_liste-txt-img_actif.png)';
		document.getElementById('articles_images').style.display='none';
		document.getElementById('articles_liste').style.display='none';
		document.getElementById('articles_icones').style.display='block';
	}
	
	if (id == 'liste') {
		document.getElementById('images').style.backgroundImage='url(' + base_dir + 'template/base/liste_produits/icone_liste-img_gris.png)';
		document.getElementById('liste').style.backgroundImage='url(' + base_dir + 'template/base/liste_produits/icone_liste-txt_actif.png)';
		document.getElementById('icones').style.backgroundImage='url(' + base_dir + 'template/base/liste_produits/icone_liste-txt-img_gris.png)';
		document.getElementById('articles_images').style.display='none';
		document.getElementById('articles_liste').style.display='block';
		document.getElementById('articles_icones').style.display='none';
	}
	
	//AJAX permettant de stocker la nouvelle presentation dans une variable de session
	var req;

	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject("Msxml2.XMLHTTP");

	req.onreadystatechange = function()
	{
		if(req.readyState == 4)
		{
			if(req.status == 200)
			{
				document.getElementById("affect_session_var").innerHTML=req.responseText;
			}
			else
			{
				document.getElementById("affect_session_var").innerHTML="Error: returned status code " + req.status + " " + req.statusText;
			}
		}
	};
	req.open("GET", base_dir + "includes/js/change_type_vue.php?type="+id, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function decompte(span_id, date_fin) {
	if ($("#"+ span_id +" .jour").html()) {
		jour = convert_time($("#"+ span_id +" .jour").html());
	} else {
		jour = 0;
	}
	
	if ($("#"+ span_id +" .heure").html()) {
		heure = convert_time($("#"+ span_id +" .heure").html());
	} else heure = 0;
	
	minute = convert_time($("#"+ span_id +" .min").html());
	sec = convert_time($("#"+ span_id +" .sec").html());
	
	
	if (jour==0 && heure==0 && minute==0 && sec==1) {
		clearInterval(timer[span_id]);
		$("#"+ span_id).html("La vente Flash de ce produit est termin�e");
	}
	
	if (sec > 0) {
		
		if (sec-1 < 10) sec = '0'+ (sec-1);
		else sec = sec-1;
		
		$("#"+ span_id +" .sec").html(sec);
	} else {
		
		$("#"+ span_id +" .sec").html(59);
			
		if (minute > 0) {
			
			if(minute-1 < 10) minute = '0'+ (minute-1);
			else minute = minute-1;
		
			$("#"+ span_id +" .min").html(minute);
		} else {
			
			$("#"+ span_id +" .min").html(59);
			
			if (heure>0) {
				$("#"+ span_id +" .heure").html(heure-1);
			} else {
				
				if (jour != '') {// il reste au moins un jour avant la fin de la vente flash
					$("#"+ span_id +" .jour").html(jour-1);
					$("#"+ span_id +" .heure").html(23);
				} else {
					$("#"+ span_id +" .heure").html('00');
				}
			}
		}
	}
}

function convert_time(chiffre) {
	if (chiffre[0]=='0' && chiffre[1]!='0') {
		chiffre = chiffre[1];
	}
	return parseInt(chiffre);
}