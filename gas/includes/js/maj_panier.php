<?php
/* Auteur : Paul
   Page AJAX mettant à jour le panier du client lors du changement de quantité du panier */

session_start();
require("../configure.php");
require("../fonctions/base_de_donnees.php");
require("../fonctions/general.php");
require("../fonctions/fonctions_panier.php");
require(DATABASE_TABLE_DIR);

tep_db_connect();

// Si l'utilisateur est logué
if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] != '') {
	/*if (isset($_GET['id_article']) && $_GET['id_article'] != '') {
		$rqt = "and products_id = '" . $_GET['id_article'] . "'";
	} else {
		$rqt = "and pack_id = '" . $_GET['id_pack'] . "'";
	}
	
	tep_db_query("UPDATE " . TABLE_CUSTOMERS_BASKET . " SET customers_basket_quantity = '" . $_GET['qte'] . "' WHERE customers_id = '" . $_SESSION['customer_id'] . "' " . $rqt);
	echo $_GET['qte'] . $_GET['id_article'] . "Quantité mise à jour (Logged User) !!!\nUPDATE customers_basket SET customers_basket_quantity = customers_basket_quantity + '" . $_GET['qte'] . "' WHERE customers_id = '" . $_SESSION['customer_id'] . "' " . $rqt;*/
	
	if(isset($_GET['id_article']) && !empty($_GET['id_article'])) {
		
		$article = explode("_", $_GET['id_article']);
		$article_details = infos_article($article, $_GET['qte']);
		
		if ($_SESSION['customers_type'] == 3 && GESTION_PRIX_PAR_QUANTITE == 1) {
			
			$prix_TTC = $article_details['prix_ht'];
		}
		elseif($article_details['prix_flash'] > 0) {
			
			$prix_TTC = $article_details['prix_flash'];
		}
		elseif($article_details['prix_promo'] > 0) {
			
			$prix_TTC = $article_details['prix_promo'];
		}
		else $prix_TTC = $article_details['prix_ht']*$article_details['tva'];
		
		echo 'prix_ht='. $article_details['prix_ht'] .'&prix_tcc='. $prix_TTC;
	}
	
// Si l'utilisateur n'est pas logué
}
else {
	
	if (isset($_GET['id_article']) && $_GET['id_article'] != '') {
		
		$taille = count($_SESSION['panier']);
		$i = 0;
		
		while ($_SESSION['panier'][$i][0] != $_GET['id_article'] && $i != $taille) {
			
			$i++;
		}
		
		if ($i != $taille) {
			
			$_SESSION['panier'][$i][1] = $_GET['qte'];
			
			$article = explode("_", $_GET['id_article']);
			$article_details = infos_article($article, $_GET['qte']);
			
			if($article_details['prix_flash'] > 0) {
				
				$prix_TTC = $article_details['prix_flash'];
			}
			elseif($article_details['prix_promo'] > 0) {
				
				$prix_TTC = $article_details['prix_promo'];
			}
			else $prix_TTC = $article_details['prix_ttc'];
			
			echo 'prix_ht='. $article_details['prix_ht'] .'&prix_tcc='. $prix_TTC;
			
			echo '&Quantité mise à jour (Unlogged User) !!!';
		}
		else {
			
			echo 'Error (Unlogged User) !!!';
		}
	}
	else {
		
		$taille = count($_SESSION['panier_packs']);
		$i = 0;
		
		while ($_SESSION['panier_packs'][$i][0] != $_GET['id_pack'] && $i != $taille) {
			
			$i++;
		}
		
		if ($i != $taille) {
			
			$_SESSION['panier_packs'][$i][1] = $_GET['qte'];
			
			echo 'Quantité mise à jour (Unlogged User) !!!';
		}
		else {
			
			echo 'Error (Unlogged User) !!!';
		}
	}
}
?>