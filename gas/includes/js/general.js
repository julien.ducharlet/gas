/* Fichier regroupant toutes les fonctions JavaScript g�n�rales au site pouvant �tre utilis�es (et utiles) un peu partout */

// Fonctions modifiant la quantit� dans la fiche article
function plus(id, qte_max) {
    var qte;
    qte=document.getElementById(id).value;
	
	if (qte_max>0 && parseInt(qte)>=qte_max) {
		//document.getElementById(id).value=parseInt(qte)+1;
		document.getElementById(id).value=parseInt(qte_max);
		alert('Vous ne pouvez pas commander cet article pour une quantit� sup�rieure � '+ qte_max +'.'+ "\n"+ 'Merci de nous contacter pour avoir plus d\'information.');
	} else {
		document.getElementById(id).value=parseInt(qte)+1;
	}
}

function moins(id) {
    var qte;
    qte=document.getElementById(id).value;
	if (qte > 1){
	document.getElementById(id).value=parseInt(qte)-1;
	}	
}

// v�rification de l'adresse mail
function verifMail(mail) {
	if ((mail.indexOf("@")>=0)&&(mail.indexOf(".")>=0)) return true 
	else return false
}

//fonction pour la modification de la quantite dans le panier
function modifie_quantite_panier_log(id,id_article,type){
	var qte;
	qte=document.getElementById(id).value;
	
	var req;
	
		if (window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");
	
		req.onreadystatechange = function() {  
			if(req.readyState == 4) {
				if(req.status == 200) {
					//location.href=document.getElementById("href_"+button).value;
				} else {
					alert("Error: returned status code " + req.status + " " + req.statusText);
				}
			}		
		};
		req.open("GET","includes/js/modifie_qte_panier.php?id_article="+ id_article +"&qte="+qte+"&type="+type, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);	
}

// Formater un nombre au format mon�taire 
function format_to_money(prix) {
	
	var prix = Math.round(prix*100)/100;
	var str_prix = prix.toString();
	var nb_decimales = 0;
	
	if (str_prix.indexOf('.')+1 != 0) {
		nb_decimales = str_prix.length - (str_prix.indexOf('.')+1);
	}
	
	if (nb_decimales == 1) {
		prix = prix + '0';
	} else if (nb_decimales == 0) {
		prix = prix + '.00';
	}
	
	return prix;
}


// Fonction ajoutant un article au panier en AJAX 
function ajout_article(id_article, id_rayon, type, button) {
	var base_dir = document.getElementById("base_dir").value;
	
	var qte_article = document.getElementById("quantite_"+type+"_"+id_article).value;
	
	var bouton_radio = document.getElementsByName('options');
	var i=0;
	while (bouton_radio[i] != null && !(bouton_radio[i].checked)) {
		i++;
	}
	
	if (bouton_radio[i] != null) {
		one_rb_is_checked = bouton_radio[i].checked;
	} else {
		one_rb_is_checked = false
	}
	
	if (document.getElementById("a_des_attributs").value == "oui" && !one_rb_is_checked) {
		alert("Veuillez cocher une case.");
	} else {
		if (bouton_radio[i] != null){
			id_article_opt = id_article + bouton_radio[i].value;
		} else {
			id_article_opt = id_article;
		}
		
		if (qte_article <= 0) {
			qte_article = 1;
		}
		
		document.getElementById("quantite_"+type+"_"+id_article).value = '';
		
		var req;
	
		if (window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");
	
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				if (req.status == 200) {
					//alert(req.responseText);
					if (type == "fiche") {
						location.href=document.getElementById("href_"+button).value;
					} else {
						if (button == "continuer") {
							document.getElementById("overlay").style.display = "none";
							document.getElementById("lightbox").style.display = "none";
							Element.remove($('lbContent'));
						} else {
							location.href=document.getElementById("href_"+button).value;
						}
					}
				} else {
					alert("Error: returned status code " + req.status + " " + req.statusText);
				}
			}		
		};
		
		req.open("GET", base_dir + "includes/js/ajout_panier.php?id_rayon="+ id_rayon +"&id_article="+id_article_opt+"&qte_article="+qte_article, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	}
}


// Fonction supprimmant un article du panier en AJAX 
function suppr_article(id_article, indice) {
	var req;
	
	if (window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject("Msxml2.XMLHTTP");

	req.onreadystatechange = function() {
		if(req.readyState == 4) {
			if(req.status == 200) {
				//alert(req.responseText);
			} else {
				alert("Error: returned status code " + req.status + " " + req.statusText);
			}
		}		
	};
	req.open("GET", "includes/js/suppr_panier.php?id_article="+id_article, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

	document.getElementById(id_article).style.display = 'none';
	
	var qte = document.getElementById('qte_'+indice).value;
	document.getElementById('qte_'+indice).value = 0;	
	
	var prix_tot = document.getElementById('prix_unit_'+indice).innerHTML * qte;
	document.getElementById('prix_total_'+indice).innerHTML = 0;
	
	var total = document.getElementById('total').innerHTML-prix_tot;
	
	document.getElementById('total').innerHTML = format_to_money(total);
	document.getElementById('ss_total_ht').innerHTML = format_to_money(total/1.2);
	document.getElementById('tva').innerHTML = format_to_money(document.getElementById('total').innerHTML - document.getElementById('ss_total_ht').innerHTML);
	
	if (total <= 0) {
		document.getElementById('bouton_poursuivre').style.display = 'none';
	}
}


// Fonction ajoutant un pack au panier en AJAX 
function ajout_pack(id_pack, id_rayon, button) {
	var base_dir = document.getElementById("base_dir").value;
	var qte_pack = 1;
	
	var bouton_radio = document.getElementsByName('options');
	var i=0;
	while (bouton_radio[i] != null && !(bouton_radio[i].checked)) {
		i++;
	}
	
	if (bouton_radio[i] != null) {
		one_rb_is_checked = bouton_radio[i].checked;
	} else {
		one_rb_is_checked = false
	}
	
	if (document.getElementById("a_des_attributs").value == "oui" && !one_rb_is_checked) {
		alert("Veuillez cocher une case.");
	} else {
		if (bouton_radio[i] != null){
			id_pack_opt = id_pack + bouton_radio[i].value;
		} else {
			id_pack_opt = id_pack;
		}
	
		var req;
	
		if (window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");
	
		req.onreadystatechange = function() {
			if(req.readyState == 4) {
				if(req.status == 200) {
					location.href=document.getElementById("href_"+button).value;
					//alert(req.responseText);
				} else {
					alert("Error: returned status code " + req.status + " " + req.statusText);
				}
			}		
		};
		req.open("GET", base_dir + "includes/js/ajout_panier.php?id_rayon="+ id_rayon +"&id_pack="+id_pack_opt+"&qte_pack="+qte_pack, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
		
	}
}

// Fonction supprimmant un pack du panier en AJAX 
function suppr_pack(id_pack, indice) {

	var req;
	
	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject("Msxml2.XMLHTTP");

	req.onreadystatechange = function() {
		if(req.readyState == 4) {
			if(req.status == 200) {
				/*alert(req.responseText);*/
			} else {
				alert("Error: returned status code " + req.status + " " + req.statusText);
			}
		}		
	};
	req.open("GET", "includes/js/suppr_panier.php?id_pack="+id_pack, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

	document.getElementById(id_pack).style.display = 'none';
	
	var qte = document.getElementById('qte_'+indice).value;
	document.getElementById('qte_'+indice).value = 0;	
	
	var prix_tot = document.getElementById('prix_unit_'+indice).innerHTML * qte;
	document.getElementById('prix_total_'+indice).innerHTML = 0;
	
	var total = document.getElementById('total').innerHTML-prix_tot;
	
	document.getElementById('total').innerHTML = format_to_money(total);
	document.getElementById('ss_total_ht').innerHTML = format_to_money(total/1.2);
	document.getElementById('tva').innerHTML = format_to_money(document.getElementById('total').innerHTML - document.getElementById('ss_total_ht').innerHTML);
	
	if (total <= 0) {
		document.getElementById('bouton_poursuivre').style.display = 'none';
	}
}

<!-- New Module with jquery 1.3.2 -->
function registerAddress() {	

}

function clickBanner(banner_id, type) {
	
	if (window.XMLHttpRequest) req = new XMLHttpRequest();
	else if (window.ActiveXObject) req  = new ActiveXObject("Msxml2.XMLHTTP");

	req.onreadystatechange = function() {
		
		if (req.readyState == 4) {			
			if (req.status == 200) {
				if (req.responseText != '') document.location.href = req.responseText;
			} /* else {
				alert("Error: returned status code " + req.status + " " + req.statusText);
			}*/
		}		
	};
	req.open("GET", BASE_DIR +"includes/js/banner.php?banner_id="+ banner_id +"&type="+ type +"&ajax=1", true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}