<?php
/* CONFIGURATION */

	// Répertoire de base du site
	define("BASE_DIR", "/gas");
	
	// Couleur de fond pour les pages
	define("COULEUR_FOND_PAGE", "#9E9E9E");
	
	// Couleur texte footer
	define("COULEUR_TEXTE_FOOTER", "#FFFFFF");
	
	// Couleur texte fil d'ariane
	define("COULEUR_TEXTE_FIL_ARIANE", "#0188C6"); //#2255f8

/* COULEURS */
	
	// GAS : BLEU  
	define("COULEUR_1", "#2255F8");
	
	// BDA : Violet Foncé -
	define("COULEUR_2", "#4E6855");
	
	// BDA : Violet Clair
	define("COULEUR_3", "#88C093");
	
	// BDA : Violet Clair +
	define("COULEUR_4", "#B8AA85");
	
	// BDA : Violet Clair ++
	define("COULEUR_5", "#cdd7fd");
	
	// BDA : Violet Clair +++
	define("COULEUR_6", "#B8AA85");
	
	
	// GAS : Gris Moyen		// Vert moyen
	define("COULEUR_7", "#676767"); // #738648
	
	// BDA : Gris Moyen		// Vert moyen
	define("COULEUR_8", "#676767"); // #738648
	
	// BDA : Rouge
	define("COULEUR_9", "#F52929");
	
	
	// BDA : Noir
	define("COULEUR_10", "#000000");
	
	// BDA : Gris Foncé
	define("COULEUR_11", "#4F4F4F");
	
	// BDA : Gris Foncé -
	define("COULEUR_12", "#9E9E9E");
	
	// BDA : Gris clair
	define("COULEUR_13", "#BFBFBF");
	
	// BDA : Gris clair +
	define("COULEUR_14", "#E1DEDD");
	
	// BDA : Gris clair ++
	define("COULEUR_15", "#ECEAEA"); // #F0EFF0
	
	// BDA : Blanc
	define("COULEUR_16", "#FFFFFF");
	
	
	// GAS : Gris Moyen		// Vert 1
	define("COULEUR_17", "#676767"); // #909C08
	
	// BDA : Vert 2
	define("COULEUR_18", "#DC2213");
	
	// BDA : Vert 3
	define("COULEUR_19", "#91B817");
	
	// BDA : Vert 4
	define("COULEUR_20", "#3E8725");
	
	// BDA : Vert 5
	define("COULEUR_21", "#BCC670");
	
	// BDA : Vert 6
	define("COULEUR_22", "#fdd8c9");
	
	// BDA : Vert 7
	define("COULEUR_23", "#E9EAB0");
	
	// BDA : Boris
	define("COULEUR_24", "#D8D2BA");
	
	// GAS : Gris bandeaux
	define("COULEUR_25", "#676767"); // #7C7972
	
	// GAS : Vert Sami
	define("COULEUR_26", "#C1CA7B");
	
	// GAS : Vert Sami foncé
	define("COULEUR_27", "#A7B06A");
		
	// BDA : Orange -> GAS : Rouge
	define("COULEUR_28", "#DC2213");
	
	// BDA : Violet -> GAS : Rouge
	define("COULEUR_29", "#DC2213");
	
	// BDA : Violet -> GAS : bleu
	define("COULEUR_30", "#4E87B4");
?>