<?php
/* Auteur : Paul
   Page affichant la liste des articles pour un mod�le donn�. L'affichage peut se faire sous 3 formes : 'Images', 'Icones' et 'Liste'*/

$nom_page = "ventes_flash";
   
require("includes/page_top.php");

require("includes/meta/ventes_flash.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/produits_liste.js"></script>
<script type="text/javascript">

var timer = Array();

</script>


<div id="corps">
	<input type="hidden" id="base_dir" value="<?php echo BASE_DIR . "/"; ?>" />
    

    
    <div id="vente_flash">
	
<?
//  1 - Trier les ventes Flash par la date de mise de debut de la vente flash
//  2 - Modifier la page de CSS pour retirer l'inutile : gas/css/ventes_flash.css.php
//  
//  
//  
//
$query = 'select p.products_id, products_model, products_name, products_url, products_bimage, part_price_by_1, pro_price_by_1, adm_price_by_1,
				 debut_vente_flash, fin_vente_flash, prix_vente,
				 r.rubrique_id, rubrique_url,
				 cd.categories_id as model_id, cd.categories_url as model_url,
				 cd2.categories_id as marque_id, cd2.categories_url as marque_url

		  from '. TABLE_PRODUCTS_VENTES_FLASH .' pvf,
		  '. TABLE_PRODUCTS .' p,
		  '. TABLE_PRODUCTS_DESCRIPTION .' pd,
		  '. TABLE_CATEGORIES .' c,
		  '. TABLE_PRODUCTS_TO_CATEGORIES .' ptc,
		  '. TABLE_CATEGORIES_DESCRIPTION .' cd,
		  '. TABLE_CATEGORIES_DESCRIPTION .' cd2,
		  '. TABLE_PRODUCTS_PRICE_BY_QUANTITY .' ppbq,
		  '. TABLE_RAYON .' r,
		  '. TABLE_CATEGORIES_RAYON .' cr
		  
		  where p.products_status=\'1\' 
		  and pvf.products_id=p.products_id
		  and p.products_id=pd.products_id
		  and pd.products_id=ptc.products_id
		  and ptc.categories_id=cd.categories_id
		  and cd.categories_id=c.categories_id
		  and c.parent_id=cd2.categories_id
		  and cd2.categories_id=cr.categories_id
		  and cr.rubrique_id=r.rubrique_id
		  and pd.products_id=ppbq.products_id
		  and p.products_id=pvf.products_id
		  and (debut_vente_flash <=NOW() and (fin_vente_flash >NOW() or fin_vente_flash=\'0000-00-00\'))
		  
		  group by p.products_id
		  order by debut_vente_flash';
$query = tep_db_query($query);

$i = 0;
$derniereDateVenteFlash = '';

while($data = tep_db_fetch_array($query)) {
	
	$date_debut = convert_date_vente_flash($data['debut_vente_flash']);
	$date_fin = convert_date_vente_flash($data['fin_vente_flash']);
	$decompte = temps_restant($data['fin_vente_flash']);
	
	
	$prix = calculate_price_for_product($data);
	$prix_flash = show_price($prix);
	
	$products_url = bda_product_name_transform($data['products_url'], $data['model_id']);
	
	$url_product = url('article', array('id_rayon' => $data['rubrique_id'], 'nom_rayon' => $data['rubrique_url'], 'id_marque' => $data['marque_id'], 'url_marque' => $data['marque_url'], 'id_modele' => $data['model_id'], 'url_modele' => $data['model_url'], 'id_article' => $data['products_id'], 'url_article' => $products_url));
	
	if($i==0) echo '<h2>Ventes Flash en cours : </h2>';
	
	?><table><tr>
        <td width="300" height="20" align="center">
        	<a href="<?php echo $url_product; ?>">
            	<img src="<?php echo WEBSITE . BASE_DIR ?>/images/products_pictures/normale/<?php echo $data['products_bimage']; ?>" width="175" height="175"/>
            </a>
        </td>
        <td align="left" valign="top">
        	<div class="nom_produit couleur_flash"><?php echo _substr($data['products_name'], 50); ?></div>
            <div style="margin-bottom:20px;">(R�f�rence de l'article : <?php echo $data['products_model']; ?>)</div>
                        Prix catalogue : <?php echo $prix['normal']; ?> &euro;<br />
                        <div style="font-size:22px;font-weight:bold;margin-bottom:5px; float:left; margin-right:10px;" class="prix_flash">Prix vente Flash : <?php echo $prix_flash; ?> &euro;</div>
                        <div style="font-size:12px;font-weight:bold;margin-bottom:20px;margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;R�alisez une �conomie de <?php echo round((1-($prix_flash/$prix['normal']))*100, 0); ?> %</div>
						
                        <!--<div class="couleur_flash" style="font-weight:bold;text-decoration:underline;margin-bottom:10px;">Vente Flash active :</div>-->
                        
                        <!--Du <span><?php echo $date_debut['date']; ?> � <?php echo $date_debut['time']; ?></span> au <span><?php echo $date_fin['date']; ?> � <?php echo $date_fin['time']; ?></span>-->
                        <?php echo '<span id="compteur_'. $data['products_id'] .'">'. show_temps_restant($decompte['jour'], $decompte['heure'], $decompte['min'], $decompte['sec']) .'</span>'; ?>
                        <!--<input type="hidden" id="jour_<?php echo $data['products_id']; ?>" value="<?php echo $decompte['jour']; ?>" />
                        <input type="hidden" id="heure_<?php echo $data['products_id']; ?>" value="<?php echo $decompte['heure']; ?>" />
                        <input type="hidden" id="min_<?php echo $data['products_id']; ?>" value="<?php echo $decompte['min']; ?>" />
                        <input type="hidden" id="sec_<?php echo $data['products_id']; ?>" value="<?php echo $decompte['sec']; ?>" />-->
                        <br /><br />
                        <div style="text-align:left;"><a href="<?php echo $url_product; ?>"><img src="<?php echo BASE_DIR; ?>/template/base/ventes_flash/bouton_visualiser-article.jpg" alt="visualiser" /></a></div>
        </td>
    </tr></table><?php
	
		?><script type="text/javascript">
		
		timer['compteur_<?php echo $data['products_id']; ?>'] = setInterval("decompte('compteur_<?php echo $data['products_id']; ?>')", 1000);
		
		</script><?php
	
	
	$i++;
}






$query = 'select p.products_id, products_model, products_name, products_url, products_bimage, part_price_by_1, pro_price_by_1, adm_price_by_1,
				 debut_vente_flash, fin_vente_flash, prix_vente,
				 r.rubrique_id, rubrique_url,
				 cd.categories_id as model_id, cd.categories_url as model_url,
				 cd2.categories_id as marque_id, cd2.categories_url as marque_url

		  from '. TABLE_PRODUCTS_VENTES_FLASH .' pvf,
		  '. TABLE_PRODUCTS .' p,
		  '. TABLE_PRODUCTS_DESCRIPTION .' pd,
		  '. TABLE_CATEGORIES .' c,
		  '. TABLE_PRODUCTS_TO_CATEGORIES .' ptc,
		  '. TABLE_CATEGORIES_DESCRIPTION .' cd,
		  '. TABLE_CATEGORIES_DESCRIPTION .' cd2,
		  '. TABLE_PRODUCTS_PRICE_BY_QUANTITY .' ppbq,
		  '. TABLE_RAYON .' r,
		  '. TABLE_CATEGORIES_RAYON .' cr
		  
		  where p.products_status=\'1\' 
		  and pvf.products_id=p.products_id
		  and p.products_id=pd.products_id
		  and pd.products_id=ptc.products_id
		  and ptc.categories_id=cd.categories_id
		  and cd.categories_id=c.categories_id
		  and c.parent_id=cd2.categories_id
		  and cd2.categories_id=cr.categories_id
		  and cr.rubrique_id=r.rubrique_id
		  and pd.products_id=ppbq.products_id
		  and p.products_id=pvf.products_id
		  and debut_vente_flash >NOW()
		  
		  group by p.products_id
		  order by debut_vente_flash';
$query = tep_db_query($query);

$i = 0;
$derniereDateVenteFlash = '';

while($data = tep_db_fetch_array($query)) {
	
	$date_debut = convert_date_vente_flash($data['debut_vente_flash']);
	$date_fin = convert_date_vente_flash($data['fin_vente_flash']);
	
	$prix = calculate_price_for_product($data);
	$prix_flash = show_price($prix);
	
	$products_url = bda_product_name_transform($data['products_url'], $data['model_id']);
	
	$url_product = url('article', array('id_rayon' => $data['rubrique_id'], 'nom_rayon' => $data['rubrique_url'], 'id_marque' => $data['marque_id'], 'url_marque' => $data['marque_url'], 'id_modele' => $data['model_id'], 'url_modele' => $data['model_url'], 'id_article' => $data['products_id'], 'url_article' => $products_url));
	
	if($i==0) echo '<h2>Ventes Flash � venir : </h2>';
	
	?><table><tr>
        <td width="300" height="20" align="center">
        	<a href="<?php echo $url_product; ?>">
            	<img src="<?php echo WEBSITE . BASE_DIR ?>/images/products_pictures/normale/<?php echo $data['products_bimage']; ?>" width="175" height="175"/>
            </a>
        </td>
        <td align="left" valign="top">
        	<div class="nom_produit couleur_flash"><?php echo _substr($data['products_name'], 50); ?></div>
            <div style="margin-bottom:20px;">(R�f�rence de l'article : <?php echo $data['products_model']; ?>)</div>
                        Prix catalogue : <?php echo $prix['normal']; ?> &euro;<br />
                        <div style="font-size:22px;font-weight:bold;margin-bottom:5px; float:left; margin-right:10px;" class="prix_flash">Prix vente Flash : <?php echo $prix_flash; ?> &euro;</div>
                        <div style="font-size:12px;font-weight:bold;margin-bottom:20px;margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;R�alisez une �conomie de  <?php echo round((1-($prix_flash/$prix['normal']))*100, 0); ?> %</div>
						
                        <!--<div class="couleur_flash" style="font-weight:bold;text-decoration:underline;margin-bottom:10px;">Vente Flash active :</div>-->
                        <div>La vente d�butera le <span class="relief"><?php echo $date_debut['date']; ?> � <?php echo $date_debut['time']; ?></span>
                        et se terminera le <span class="relief"><?php echo $date_fin['date']; ?> � <?php echo $date_fin['time']; ?></span></div>
                        <br />
                        <div style="text-align:left;"><a href="<?php echo $url_product; ?>"><img src="<?php echo BASE_DIR; ?>/template/base/ventes_flash/bouton_visualiser-article.jpg" alt="visualiser" /></a></div>
        </td>
    </tr></table><?php
	
	$i++;
}

?>
    </div>
    
    <div style="clear:both;"></div>
        
	</div>
</div>



<?php 
require("includes/footer.php");
require("includes/page_bottom.php");
?>