<?php

if (!isset($_GET['secure_key']) OR (isset($_GET['secure_key']) AND $_GET['secure_key'] != '9CQR4DGZA9C8VUMA'))
	die('Please use the Import button instead of calling this file directly');

//@ini_set('display_errors', 'off');
require(dirname(__FILE__).'/includes/configure.php');

$utf8_fix = 0;

if (isset($_GET['encoding_transfer']) && !empty($_GET['encoding_transfer']))
	$utf8_fix = $_GET['encoding_transfer'];

function ExecuteS($link, $query, $table = false, $array = true, $columns = false)
{
	global $utf8_fix;
	$result = false;
	if ($link && $result = mysql_query($query, $link))
	{
		if (mysql_errno())
		{
			if ($query)
				die(mysql_error().'<br /><br /><pre>'.$query.'</pre>');
			die(mysql_error());
		}

		if (!$array)
			return $result;
		$resultArray = array();
		while ($row = mysql_fetch_assoc($result))
		{
			if (is_array($columns))
			{
				foreach ($row as $column => $data)
				{
					if ($utf8_fix == 2)
						$row[$column] = utf8_encode($data);
					if (!in_array($column, $columns))
						unset($row[$column]);
				}
			}
			else
				foreach ($row as $column => $data)
					if ($utf8_fix == 2)
						$row[$column] = utf8_encode($data);
			if ($table)
				$resultArray[$row[$table == 'categories' ? 'categories_id' : ($table == 'customers' ? 'customers_id' : ($table == 'zones_to_geo_zones' ? 'zone_country_id' : ''))]] = $row;
			else
				$resultArray[] = $row;
		}
		return $resultArray;
	}
	
	if (mysql_errno())
	{
		if ($query)
			die(mysql_error().'<br /><br /><pre>'.$query.'</pre>');
		die(mysql_error());
	}
	
	return false;
}

function tableExists($needle, $haystack)
{
	foreach ($haystack as $table)
		if ($table["Tables_in_".DB_DATABASE] == $needle)
			return true;			
	return false;
}
$prefix = "";
function add_attribute_info(&$data)
{
	global $link;
	if (sizeof($data['orders_products']) == 0)
		return;
	$in = "";
	$prefix = "";
	foreach ($data['orders_products'] as $row)
		$in .= ($in == ""?"":",").$row['orders_products_id'];
	$att = executeS($link, 'SELECT * FROM '.$prefix.'orders_products_attributes WHERE orders_products_id IN('.$in.')');
	$attf = array();
	foreach ($att as $row)
	{
		if (isset($attf[$row['orders_products_id']]))
		{
			$attf[$row['orders_products_id']]['group'] .= '|~|'. $row['products_options'];
			$attf[$row['orders_products_id']]['value'] .= '|~|'. $row['products_options_values'];
		}
		else
		{
			$attf[$row['orders_products_id']]['group'] = $row['products_options'];
			$attf[$row['orders_products_id']]['value'] = $row['products_options_values'];
		}
	}
	foreach ($data['orders_products'] as $key => $row)
	{
		$data['orders_products'][$key]['att_group'] = $attf[$row['orders_products_id']]['group'];
		$data['orders_products'][$key]['att_value'] = $attf[$row['orders_products_id']]['value'];
	}
}

$p = 1;
$n = 1000;
$min = 0;
$max = 0;


if (isset($_GET['p']))
	$p = $_GET['p'];

if (isset($_GET['n']))
	$n = $_GET['n'];

if (isset($_GET['min']))
	$min = $_GET['min'];

if (isset($_GET['max']))
	$max = $_GET['max'];
	
if ($link = @mysql_connect(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD))
{
	/* UTF-8 support */
	if ($utf8_fix == 1)
		mysql_query('SET NAMES \'utf8\'', $link);
			
	/* Disable some MySQL limitations */
	mysql_query('SET GLOBAL SQL_MODE=\'\'', $link);	
	
	if (mysql_select_db(DB_DATABASE))
	{
		$data = array();

		$tables = executeS($link, 'SHOW TABLES');
		if (isset($_GET['languages']) AND $_GET['languages'] == 1 AND tableExists($prefix.'languages', $tables))
			$data['languages'] = executeS($link, 'SELECT * FROM '.$prefix.'languages', false, true, array('languages_id', 'name', 'code','image'));

		if (isset($_GET['carriers']) AND $_GET['carriers'] == 1 AND tableExists($prefix.'carriers', $tables))
			$data['carriers'] = executeS($link, 'SELECT * FROM '.$prefix.'carriers LIMIT ' . (($p - 1) * $n) . ', ' . $n);
			
		if (isset($_GET['currencies']) AND $_GET['currencies'] == 1 AND tableExists($prefix.'currencies', $tables))
			$data['currencies'] = executeS($link, 'SELECT * FROM '.$prefix.'currencies LIMIT ' . (($p - 1) * $n) . ', ' . $n);
			
		if (isset($_GET['default_currency']) AND $_GET['default_currency'] == 1 AND tableExists($prefix.'configuration', $tables))
			$data['default_currency'] = executeS($link, 'SELECT * FROM '.$prefix.'configuration WHERE configuration_key = "DEFAULT_CURRENCY" LIMIT ' . (($p - 1) * $n) . ', ' . $n);
	
		if (isset($_GET['geo_zones']) AND $_GET['geo_zones'] == 1 AND tableExists($prefix.'geo_zones', $tables) AND tableExists($prefix.'zones_to_geo_zones', $tables))
		{
			$data['geo_zones'] = executeS($link, 'SELECT * FROM '.$prefix.'geo_zones LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('geo_zone_id', 'geo_zone_name'));
			$data['zones_to_geo_zones'] = executeS($link, 'SELECT * FROM '.$prefix.'zones_to_geo_zones LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('zone_country_id', 'geo_zone_id'));
		}
			
		if (isset($_GET['tax_rates']) AND $_GET['tax_rates'] == 1 AND tableExists($prefix.'tax_rates', $tables))
			$data['tax_rates'] = executeS($link, 'SELECT * FROM '.$prefix.'tax_rates LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('tax_class_id', 'tax_class_id', 'tax_zone_id', 'tax_rate', 'tax_description'));
							

		if (isset($_GET['address_book']) AND $_GET['address_book'] == 1 AND tableExists($prefix.'address_book', $tables)){
			//$data['address_book'] = executeS($link, 'SELECT * FROM `address_book` a LEFT JOIN `customers` c ON (a.`customers_id` = c.`customers_id`) LIMIT ' . (($p - 1) * $n) . ', ' . $n);
			if (tableExists($prefix.'zones', $tables)){
				$data['address_book'] = executeS($link, 'SELECT * FROM `'.$prefix.'address_book` a LEFT JOIN `'.$prefix.'customers` c ON (a.`customers_id` = c.`customers_id`) LEFT JOIN `'.$prefix.'zones` z ON (z.`zone_id` = a.`entry_zone_id`) LIMIT ' . (($p - 1) * $n) . ', ' . $n);
			}else{
				$data['address_book'] = executeS($link, 'SELECT * FROM `'.$prefix.'address_book` a LEFT JOIN `'.$prefix.'customers` c ON (a.`customers_id` = c.`customers_id`) LIMIT ' . (($p - 1) * $n) . ', ' . $n);

			}
		}

		if (isset($_GET['address_book_customer']) AND $_GET['address_book_customer'] == 1 AND tableExists($prefix.'address_book', $tables)){
			if (tableExists('zones', $tables)){
				$data['address_book'] = executeS($link, 'SELECT * FROM `'.$prefix.'address_book` a LEFT JOIN `'.$prefix.'customers` c ON (a.`customers_id` = c.`customers_id`) LEFT JOIN `zones` z ON (z.`zone_id` = a.`entry_zone_id`) WHERE c.`customers_id` = '.$_GET['customer_id']);
			}else{
				$data['address_book'] = executeS($link, 'SELECT * FROM `'.$prefix.'address_book` a LEFT JOIN `'.$prefix.'customers` c ON (a.`customers_id` = c.`customers_id`) WHERE c.`customers_id` = '.$_GET['customer_id']);
			}
		}	
		
		if (isset($_GET['categories_count']) AND $_GET['categories_count'] == 1)
		{
			if (tableExists($prefix.'categories', $tables))
				$data['categories'] = executeS($link, 'SELECT COUNT(*) AS categories_count FROM '.$prefix.'categories');		
				
			if (tableExists($prefix.'categories_description', $tables))
				$data['categories_description'] = executeS($link, 'SELECT COUNT(*) AS categories_description_count FROM '.$prefix.'categories_description');		
		}
		
		if (isset($_GET['categories']) AND $_GET['categories'] == 1 AND tableExists($prefix.'categories', $tables))
		{
			$data['categories'] = executeS($link, 'SELECT * FROM '.$prefix.'categories  ORDER BY sort_order LIMIT ' . (($p - 1) * $n) . ', ' . $n, 'categories', true, array('categories_id', 'categories_image', 'parent_id', 'sort_order', 'date_added', 'last_modified'));
			$data['categories_parents'] = executeS($link, 'SELECT * FROM '.$prefix.'categories ORDER BY sort_order', 'categories', true, array('categories_id', 'parent_id'));
									
			foreach ($data['categories_parents'] as $category)
			{
				$level_depth = 0;
				$parent = $category['categories_id'];

				while ($parent != 0)
				{
					$level_depth++;
					$parent = $data['categories_parents'][$parent]['parent_id'];
				}
				if (isset($data['categories'][$category['categories_id']]))
					$data['categories'][$category['categories_id']]['level_depth'] = $level_depth;
			}
						
		}
		
		if (isset($_GET['categories_description']) AND $_GET['categories_description'] == 1 AND tableExists($prefix.'categories_description', $tables))
			$data['categories_description'] = executeS($link, 'SELECT * FROM '.$prefix.'categories c, '.$prefix.'categories_description cd WHERE ( c.`categories_id` = cd.`categories_id`) ORDER BY sort_order, cd.categories_name LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('categories_id', 'language_id', 'sort_order', 'categories_name','categories_description'));			
		
		if (isset($_GET['countries']) AND $_GET['countries'] == 1 AND tableExists($prefix.'countries', $tables))
			$data['countries'] = executeS($link, 'SELECT * FROM '.$prefix.'countries LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('countries_id', 'countries_name', 'countries_iso_code_2'));
			
		if (isset($_GET['customers']) AND $_GET['customers'] == 1 AND tableExists($prefix.'customers', $tables) AND tableExists($prefix.'customers_info', $tables))
			$data['customers'] = executeS($link, 'SELECT * FROM '.$prefix.'customers c LEFT JOIN '.$prefix.'customers_info ci ON c.customers_id = ci.customers_info_id LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('customers_id', 'customers_firstname', 'customers_lastname', 'customers_dob', 'customers_email_address', 'customers_telephone', 'customers_fax', 'customers_password', 'customers_newsletter', 'customers_group_id','customers_info_date_account_created','customers_info_date_of_last_logon'));
		else if (isset($_GET['customers']) AND $_GET['customers'] == 1 AND tableExists($prefix.'customers', $tables))
			$data['customers'] = executeS($link, 'SELECT * FROM '.$prefix.'customers LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('customers_id', 'customers_firstname', 'customers_lastname', 'customers_dob', 'customers_email_address', 'customers_telephone', 'customers_fax', 'customers_password', 'customers_newsletter', 'customers_group_id'));
			
		if (isset($_GET['customers_count']) AND $_GET['customers_count'] == 1)
		{
			if (tableExists($prefix.'customers', $tables))
				$data['customers'] = executeS($link, 'SELECT COUNT(*) AS customers_count FROM '.$prefix.'customers');
				
			if (tableExists($prefix.'customers_groups', $tables))
				$data['customers_groups'] = executeS($link, 'SELECT COUNT(*) AS customers_groups_count FROM '.$prefix.'customers_groups');
				
			if (tableExists($prefix.'address_book', $tables))				
				$data['address_book'] = executeS($link, 'SELECT COUNT(*) AS address_book_count FROM '.$prefix.'address_book');			
		}
					
		if (isset($_GET['customers_groups']) AND $_GET['customers_groups'] == 1 AND tableExists($prefix.'customers_groups', $tables))
			$data['customers_groups'] = executeS($link, 'SELECT * FROM '.$prefix.'customers_groups LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('customers_group_id', 'customers_group_name', 'customers_group_show_tax'));
		
		if (isset($_GET['customers_basket']) AND $_GET['customers_basket'] == 1 AND tableExists($prefix.'customers_basket', $tables))
		{
			$data['customers_basket'] = executeS($link, 'SELECT * FROM '.$prefix.'customers_basket');

			if (tableExists($prefix.'customers_basket_text_attributes', $tables))
				$data['customers_basket_text_attributes'] = executeS($link, 'SELECT * FROM '.$prefix.'customers_basket_text_attributes LIMIT ' . (($p - 1) * $n) . ', ' . $n);			
		}
		
		if (isset($_GET['manufacturers_count']) AND $_GET['manufacturers_count'] == 1)
		{
			if (tableExists($prefix.'manufacturers', $tables))
				$data['manufacturers'] = executeS($link, 'SELECT COUNT(*) AS manufacturers_count FROM '.$prefix.'manufacturers');		
				
			if (tableExists($prefix.'manufacturers_info', $tables))
				$data['manufacturers_info'] = executeS($link, 'SELECT COUNT(*) AS manufacturers_info_count FROM '.$prefix.'manufacturers_info');		
		}
		
		if (isset($_GET['manufacturers']) AND $_GET['manufacturers'] == 1 AND tableExists($prefix.'manufacturers', $tables))
			$data['manufacturers'] = executeS($link, 'SELECT * FROM '.$prefix.'manufacturers LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('manufacturers_id', 'manufacturers_name', 'manufacturers_image', 'date_added', 'last_modified'));
		
		if (isset($_GET['manufacturers_info']) AND $_GET['manufacturers_info'] == 1 AND tableExists($prefix.'manufacturers_info', $tables))
			$data['manufacturers_info'] = executeS($link, 'SELECT * FROM '.$prefix.'manufacturers_info LIMIT ' . (($p - 1) * $n) . ', ' . $n);
			
		if (isset($_GET['orders']) AND $_GET['orders'] == 1 AND tableExists($prefix.'orders', $tables))
			$data['orders'] = executeS($link, 'SELECT * FROM '.$prefix.'orders LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('orders_id', 'customers_id', 'customers_name', 'customers_telephone', 'customers_email_address', 'delivery_name', 'delivery_company', 'delivery_street_address', 'delivery_suburb', 'delivery_city', 'delivery_postcode', 'delivery_state', 'delivery_country', 'billing_name', 'billing_company', 'billing_street_address', 'billing_suburb', 'billing_city', 'billing_postcode', 'billing_state', 'billing_country', 'payment_method', 'comments', 'last_modified', 'date_purchased', 'orders_status', 'ups_track_num', 'usps_track_num', 'fedex_track_num', 'fedex_freight_track_num', 'dhl_track_num', 'currency', 'currency_value', 'paypal_ipn_id', 'delivery_date'));

		if (isset($_GET['orders_status']) AND $_GET['orders_status'] == 1 AND tableExists($prefix.'orders_status', $tables))
			$data['orders_status'] = executeS($link, 'SELECT * FROM '.$prefix.'orders_status LIMIT ' . (($p - 1) * $n) . ', ' . $n);
			
		if (isset($_GET['orders_status_grouped']) AND $_GET['orders_status_grouped'] == 1 AND tableExists($prefix.'orders_status', $tables))
			$data['orders_status_grouped'] = executeS($link, 'SELECT * FROM '.$prefix.'orders_status group by orders_status_id');
		
							
		if (isset($_GET['missing_customers']) AND $_GET['missing_customers'] == 1)
		{		
			$data['missing_customers'] = executeS($link, 'SELECT o.`customers_id` FROM `'.$prefix.'orders` o LEFT JOIN `'.$prefix.'customers` c ON (o.`customers_id` = c.`customers_id`) WHERE c.`customers_id` IS NULL');
			
			if (is_array($data['missing_customers']))
				$data['missing_customers'] = sizeof($data['missing_customers']);
		}
		
		if (isset($_GET['orders_count']) AND $_GET['orders_count'] == 1)
		{
			if (tableExists($prefix.'orders', $tables))
				$data['orders'] = executeS($link, 'SELECT COUNT(*) AS orders_count FROM '.$prefix.'orders');
				
			if (tableExists($prefix.'orders_products', $tables))
				$data['orders_products'] = executeS($link, 'SELECT COUNT(*) AS orders_products_count FROM '.$prefix.'orders_products');
				
			if (tableExists($prefix.'orders_status_history', $tables))				
				$data['orders_status_history'] = executeS($link, 'SELECT COUNT(*) AS orders_status_history_count FROM '.$prefix.'orders_status_history');
				
			if (tableExists($prefix.'orders_total', $tables))				
				$data['orders_total'] = executeS($link, 'SELECT COUNT(*) AS orders_total_count FROM '.$prefix.'orders_total');
		}
		
		if (isset($_GET['orders_products']) AND $_GET['orders_products'] == 1 AND tableExists($prefix.'orders_products', $tables))
		{
			$data['orders_products'] = executeS($link, 'SELECT op.*,p.products_weight FROM '.$prefix.'orders_products op LEFT JOIN '.$prefix.'products p ON p.products_id = op.products_id ORDER BY op.orders_id ASC LIMIT ' . (($p - 1) * $n) . ', ' . $n);			
			if (tableExists($prefix.'orders_products_attributes', $tables))
				add_attribute_info($data);
		}
		
		if (isset($_GET['orders_status_history']) AND $_GET['orders_status_history'] AND tableExists($prefix.'orders_status_history', $tables))
			$data['orders_status_history'] = executeS($link, 'SELECT * FROM '.$prefix.'orders_status_history LIMIT ' . (($p - 1) * $n) . ', ' . $n);		

		if (isset($_GET['orders_total']) AND $_GET['orders_total'] == 1 AND tableExists($prefix.'orders_total', $tables))
			$data['orders_total'] = executeS($link, 'SELECT * FROM '.$prefix.'orders_total LIMIT ' . (($p - 1) * $n) . ', ' . $n);

		if (isset($_GET['returned_products']) AND $_GET['returned_products'] == 1 AND tableExists($prefix.'returned_products', $tables))
			$data['returned_products'] = executeS($link, 'SELECT * FROM `'.$prefix.'returned_products` r LEFT JOIN `'.$prefix.'orders` o ON (r.`orders_id` = o.`orders_id`) LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('returns_id', 'customers_id', 'order_id', 'returns_status', 'comments'));

		if (isset($_GET['returned_products_data']) AND $_GET['returned_products_data'] == 1 AND tableExists($prefix.'returns_products_data', $tables))
			$data['returned_products_data'] = executeS($link, 'SELECT * FROM `'.$prefix.'returns_products_data` r LEFT JOIN `'.$prefix.'orders` o ON (r.`orders_id` = o.`orders_id`) LIMIT ' . (($p - 1) * $n) . ', ' . $n);
			
		if (isset($_GET['returned_products_count']) AND $_GET['returned_products_count'] == 1)
		{
			if (tableExists($prefix.'returned_products', $tables))				
				$data['returned_products'] = executeS($link, 'SELECT COUNT(*) AS returned_products_count FROM `'.$prefix.'returned_products` r LEFT JOIN `'.$prefix.'orders` o ON (r.`orders_id` = o.`orders_id`)');
				
			if (tableExists($prefix.'returned_products_data', $tables))								
				$data['returned_products_data'] = executeS($link, 'SELECT COUNT(*) AS returned_products_count FROM `'.$prefix.'returns_products_data` r LEFT JOIN `'.$prefix.'orders` o ON (r.`orders_id` = o.`orders_id`)');
		}

		if (isset($_GET['returns_status']) AND $_GET['returns_status'] == 1 AND tableExists($prefix.'returns_status', $tables))
			$data['returns_status'] = executeS($link, 'SELECT * FROM '.$prefix.'returns_status LIMIT ' . (($p - 1) * $n) . ', ' . $n);

		if (isset($_GET['products_count']) AND $_GET['products_count'] == 1)
		{
			if (tableExists($prefix.'products', $tables))
				$data['products'] = executeS($link, 'SELECT COUNT(*) AS products_count FROM '.$prefix.'products');
				
			if (tableExists($prefix.'products_description', $tables))
				$data['products_description'] = executeS($link, 'SELECT COUNT(*) AS products_description_count FROM '.$prefix.'products_description');
				
			if (tableExists($prefix.'products_to_categories', $tables))
				$data['products_to_categories'] = executeS($link, 'SELECT COUNT(*) AS products_to_categories_count FROM '.$prefix.'products_to_categories');
		}

		if (isset($_GET['products']) AND $_GET['products'] == 1 AND tableExists($prefix.'products', $tables))
			$data['products'] = executeS($link, 'SELECT p.*, s.specials_new_products_price AS reduction_price, SUBSTRING(s.specials_date_added, 1, 10) AS reduction_from, SUBSTRING(s.expires_date, 1, 10) AS reduction_to FROM '.$prefix.'products AS p LEFT JOIN '.$prefix.'specials AS s ON p.products_id = s.products_id ORDER BY p.products_id ASC LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('products_id', 'manufacturers_id', 'products_ordered', 'products_family', 'products_tax_class_id', 'products_master', 'products_quantity', 'products_price', 'products_cost', 'products_model', 'products_weight', 'products_status', 'products_featured', 'products_date_added', 'products_last_modified', 'products_ordered', 'products_image', 'products_bimage', 'products_image_lrg', 'products_image_xl_1', 'products_image_xl_2', 'products_image_xl_3', 'products_image_xl_4', 'products_image_xl_5', 'products_image_xl_6', 'products_bsubimage1', 'products_bsubimage2', 'products_bsubimage3', 'products_bsubimage4', 'products_bsubimage5', 'products_bsubimage6', 'reduction_price', 'reduction_from', 'reduction_to'));			

		if (isset($_GET['products_attributes']) AND $_GET['products_attributes'] == 1 AND tableExists($prefix.'products_attributes', $tables))	
			$data['products_attributes'] = executeS($link, 'SELECT * FROM '.$prefix.'products_attributes LIMIT ' . (($p - 1) * $n) . ', ' . $n);
			
		if (isset($_GET['products_description']) AND $_GET['products_description'] == 1 AND tableExists($prefix.'products_description', $tables))
		{
			if ($min > 0 && $max > 0)
				$data['products_description'] = executeS($link, 'SELECT * FROM '.$prefix.'products_description WHERE products_id BETWEEN '.$min.' AND '.$max.' ORDER BY products_id ASC');
			else
				$data['products_description'] = executeS($link, 'SELECT * FROM '.$prefix.'products_description ORDER BY products_id ASC LIMIT ' . (($p - 1) * $n) . ', ' . $n);
		}	
		if (isset($_GET['products_to_categories']) AND $_GET['products_to_categories'] == 1 AND tableExists($prefix.'products_to_categories', $tables))				
			$data['products_to_categories'] = executeS($link, 'SELECT * FROM '.$prefix.'products_to_categories LIMIT ' . (($p - 1) * $n) . ', ' . $n);

		if (isset($_GET['products_options']) AND $_GET['products_options'] == 1 AND tableExists($prefix.'products_options', $tables))
			$data['products_options'] = executeS($link, 'SELECT * FROM '.$prefix.'products_options LIMIT ' . (($p - 1) * $n) . ', ' . $n);

		if (isset($_GET['products_options_values']) AND $_GET['products_options_values'] == 1 AND tableExists($prefix.'products_options_values', $tables))
			$data['products_options_values'] = executeS($link, 'SELECT * FROM '.$prefix.'products_options_values LIMIT ' . (($p - 1) * $n) . ', ' . $n);

		if (isset($_GET['products_options_values_to_products_options']) AND $_GET['products_options_values_to_products_options'] == 1 AND tableExists($prefix.'products_options_values_to_products_options', $tables))
				$data['products_options_values_to_products_options'] = executeS($link, 'SELECT * FROM '.$prefix.'products_options_values_to_products_options LIMIT ' . (($p - 1) * $n) . ', ' . $n);
					
		if (isset($_GET['products_conseils']) AND $_GET['products_conseils'] == 1 AND tableExists($prefix.'products_conseils', $tables))
			$data['products_conseils'] = executeS($link, 'SELECT * FROM '.$prefix.'products_conseils LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('product_id', 'product_lien'));
				
		if (isset($_GET['reviews_count']) AND $_GET['reviews_count'] == 1)
		{
			if (tableExists($prefix.'reviews', $tables))
				$data['reviews'] = executeS($link, 'SELECT COUNT(*) AS reviews_count FROM '.$prefix.'reviews');		
				
			if (tableExists($prefix.'reviews_description', $tables))
				$data['reviews_description'] = executeS($link, 'SELECT COUNT(*) AS reviews_description_count FROM '.$prefix.'reviews_description');		
		}
		
		if (isset($_GET['reviews']) AND $_GET['reviews'] == 1 AND tableExists($prefix.'reviews', $tables))
		{
			$data['reviews'] = executeS($link, 'SELECT * FROM '.$prefix.'reviews LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('reviews_id', 'products_id', 'customers_id', 'customers_name', 'reviews_rating', 'date_added'));

			$data['missing_customers'] = executeS($link, 'SELECT r.`customers_id` FROM `'.$prefix.'reviews` r LEFT JOIN `'.$prefix.'customers` c ON (r.`customers_id` = c.`customers_id`) WHERE c.`customers_id` IS NULL LIMIT ' . (($p - 1) * $n) . ', ' . $n);
			
			if (is_array($data['missing_customers']))
				$data['missing_customers'] = sizeof($data['missing_customers']);
		}
		
		if (isset($_GET['reviews_description']) AND $_GET['reviews_description'] == 1 AND tableExists($prefix.'reviews_description', $tables))				
			$data['reviews_description'] = executeS($link, 'SELECT * FROM '.$prefix.'reviews_description LIMIT ' . (($p - 1) * $n) . ', ' . $n, false, true, array('reviews_id', 'reviews_text'));
		
		@ob_end_clean();
		echo serialize($data);

		@mysql_close($this->_link);
	}
	else
		die('The database selection cannot be made.');
}
else
	die('Link to database cannot be established.');

?>
