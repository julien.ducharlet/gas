<?php
header ("Content-type: image/png");
require("../../includes/configure_mail.php");
$image = imagecreate(260,15);

$blanc = imagecolorallocate($image, 255, 255, 255);
$violet = imagecolorallocate($image, 153, 51, 153);

imagestring($image, 3, 0, 3, MAIL_RECRUTEMENT, $violet);

imagecolortransparent($image, $blanc);


imagepng($image);
?>
