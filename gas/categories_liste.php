<?php
/* RAYON qui Liste les marques d'un rayon */
   
$nom_page = "categories_liste";
require("includes/page_top.php");

switch($_SESSION['customers_type']) {
	case '1' :
		$query_custom_type = ' and categories_status_client=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
		break;
	case '2' :
		$query_custom_type = ' and categories_status_pro=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_pro=\'1\'';
		break;
	case '3' :
		$query_custom_type = ' and categories_status_rev=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_rev=\'1\'';
		break;
	case '4' :
		$query_custom_type = ' and categories_status_adm=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_adm=\'1\'';
		break;
	default :
		$query_custom_type = ' and categories_status_client=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
		break;
}

//verifie URL
$query_url="select rubrique_name, rubrique_url, rubrique_htc_title_tag, rubrique_htc_desc_tag, rubrique_htc_keywords_tag, rubrique_baseline from ".TABLE_RAYON." where rubrique_id='". (int)$_REQUEST["rayon"] . "'". $query_rubrique_custom_type;
$res_url=tep_db_query($query_url);

if (tep_db_num_rows($res_url)!=1) { 
	redirect(404,url('erreur',array()));
} else {
	$row_url = tep_db_fetch_array($res_url);
	
	//################ POUR MOI
	//if ($_SESSION['customer_id']==3) printr($row_url);
	
	$url_clean = url("rayon",array('id_rayon' => (int)$_REQUEST["rayon"], 'nom_rayon' => $row_url["rubrique_url"]));
	
	if ($_SERVER['REQUEST_URI'] != $url_clean) {
		redirect(301,$url_clean);
	}
	
	//################ POUR MOI
	//if ($_SESSION['customer_id']==3) echo $_SERVER['REQUEST_URI'];
	//if ($_SESSION['customer_id']==3) echo '<br>'.$url_clean.'<br>';
	//if ($session_customer_id==3) print_r($_REQUEST);
	
	require("includes/meta/categories_liste.php");
	require("includes/meta_head.php");
	require("includes/header.php"); 
	require("css/background_pages.php");
}
?>

<div id="corps">
	<?php 
	// Affichage du fil d'ariane :
	// Le module se charge aussi de d�clarer et d'assigner une valeur aux variables $id_rayon, $nom_rayon
	include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_fil_ariane.php"); ?>
	
    <div id="rayons">
        <?php
		// Module g�rant la bani�re de pub et le bloc de recherche � droite de la page
        //include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_recherche_avancee.php");
		
		/*if (AFFICHE_PLUS_RECHERCHES == '1') {
			// Module affichant les portables les plus recherch�
			include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_popular_tel.php");
		}*/
		
		// On r�cup�re la liste des marques du rayon (dur�e de la requ�te sous phpMyAdmin : 0.0018 sec.)
		$query_marques="	SELECT 
								cd.categories_name, 
								cd.categories_url, 
								cd.categories_id, 
								c.categories_image2, 
								c.categories_status_client, 
								c.categories_status_rev, 
								c.categories_status_pro, 
								c.categories_status_vip, 
								c.sort_order, 
								c.categories_status, 
								cd.balise_title_lien_image, 
								cd.balise_title_lien_texte
							FROM 
								" . TABLE_CATEGORIES . " AS c INNER JOIN " . TABLE_CATEGORIES_DESCRIPTION . " AS cd ON c.categories_id=cd.categories_id INNER JOIN " . TABLE_CATEGORIES_RAYON . " AS cr ON c.categories_id=cr.categories_id
							WHERE 
								c.parent_id='0' AND 
								c.sort_order > 0 AND 
								cr.rubrique_id = " . (int)$_REQUEST["rayon"] . " AND 
								c.categories_status = '1' ". $query_custom_type ." 
							ORDER BY 
								sort_order ";
									   
		$marques_query = tep_db_query($query_marques);
		
		?>
        <div class="recherche_marque">
            
            <!-- On affiche la liste des marques avec l'alignement d�fini plus haut -->
        	<!-- <div class="liste ">&nbsp;-->
			<div class="liste centre">&nbsp;
            	<?php
				$div_cat = '';
				
				if (tep_db_num_rows($marques_query) > 0) {
					while($marques_data = tep_db_fetch_array($marques_query)) { 
						
						$pathToImage = BASE_DIR . '/images/categories_pictures/' . $marques_data['categories_image2'];
						$size = getImageProperty(ABSOLUTE_DIR . $pathToImage);
						$url_marque = url('marque', array('id_rayon' => $id_rayon, 'nom_rayon' => $nom_rayon, 'id_marque' => $marques_data['categories_id'], 'url_marque' => $marques_data['categories_url']));
						// width="'. $size['width'] .'" height="'. $size['height'] .'"
						
						
						$div_cat .= '<div style="width: 115px; height:160px; float: left; text-align: center; color: #4F4F4F; font-size:11px; padding:0 10px 20px 10px;">';
							$div_cat .= '<a href="'. $url_marque .'" title="'. $marques_data['balise_title_lien_image'] .'">';
								$div_cat .= '<img src="'. $pathToImage . '" alt="'.$marques_data['balise_title_lien_image'].'" width="100px" height="100px" style="border: solid 1px #979797;" />';								
								$div_cat .= '</a><br />';
								$div_cat .=  '<a href="'. $url_marque .'" title="'. $marques_data['balise_title_lien_texte'].'">';
								$div_cat .= $marques_data['categories_name'];
							$div_cat .= '</a>';
						$div_cat .= '</div>';
					}
				} else {
					echo "Pas encore de resultat";
				}	
				?>
                
				<div class="contener" style="width: 960px; margin: auto;">
					<?php echo $div_cat; ?>
					<div style="clear: both;"></div>
				</div>
			</div>
		</div>
		
        <?php
		// Module du top 10 des ventes et des 10 derniers articles
        include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_top_ten.php");
		if($session_customer_id==3) {
			echo $res_url['rubrique_id'];
		}
		?>
		<br>
        <?php // AVIS VERIFIES #7C7972 ?>
		<div style="margin:5px; padding:10px; background-color:#ffffff; border-top:1px dashed grey;">		
			<iframe id='AV_widget_iframe' frameBorder="0" width="100%" height="100%" src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2ahorizontal_index.html"></iframe>
		</div>
		<?php // End AVIS VERIFIES ?>
    </div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>