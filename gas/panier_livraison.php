<?php
/* Page permettant au client de choisir son adresse de livraison */

// MODIF A FAIRE EN 397 

$nom_page = "panier_livraison";
require("includes/page_top.php");

if (empty($_SESSION['customer_id'])) { ?>
	<meta http-equiv="refresh" content="0; URL='<?php echo BASE_DIR; ?>/compte/connexion.php?panier=true'">
	<?php
} else {
	/*$req_redirect=tep_db_query("select * from ".TABLE_CUSTOMERS_BASKET." where customers_id=".$_SESSION['customer_id']."");
	$nb_redirect=tep_db_num_rows($req_redirect);

	if ($nb_redirect==0) { ?>
    	<meta http-equiv="refresh" content="0; URL='<?php echo BASE_DIR; ?>/index.php'">
  	<?php  
	} else { */
	require("includes/meta/panier_livraison.php");
	require("includes/meta_head.php");
	require("includes/header.php");
    

function prix_livraison_par_adresse($id_pays) {
	
	$poids_total_combo=poids_livraison();
	$id_address=tep_db_query("	SELECT countries_name, countries_iso_code_2, countries_id_zone_livraison
								FROM ".TABLE_COUNTRIES." c 
								WHERE countries_id=".$id_pays."");
							  
	while($res_id=tep_db_fetch_array($id_address)){ 
  
	   $query = tep_db_query("	SELECT prix_livraison
								FROM ".TABLE_MODULE_LIVRAISON."
								WHERE zone_livraison_id=". $res_id['countries_id_zone_livraison'] ."
								AND poids_livraison >= ". $poids_total_combo);
	    
		if (tep_db_num_rows($query)>0) {
			$data = tep_db_fetch_array($query);
			//if($poids_total_combo<=30){
			return $prix = round($data['prix_livraison']*parametrage_affiche_prix($id_pays),2);
		} else { 
			return ''; 
		}
	}
}
	
if(!empty($_POST)) {
	$titre_address = strtoupper(format_inscription($_POST['titre_address']));
	$nom_address = strtoupper(format_inscription($_POST['nom_address']));
	$prenom_address = ucwords(strtolower(format_inscription($_POST['prenom_address'])));
	$societe_address = strtoupper(format_inscription($_POST['societe_address']));
	$a_address = ucwords(strtolower(format_inscription($_POST['a_address'])));
	$ap_address = ucwords(strtolower(format_inscription($_POST['ap_address'])));
	$app_address = ucwords(strtolower(format_inscription($_POST['app_address'])));
	$appp_address = ucwords(strtolower(format_inscription($_POST['appp_address'])));
	$cp_address = strtoupper(format_inscription($_POST['cp_address']));		
	$ville_address = strtoupper(format_inscription($_POST['ville_address']));
} else {
	$titre_address = '';
	$nom_address = '';
	$prenom_address = '';
	$societe_address = '';
	$a_address = '';
	$ap_address = '';
	$app_address = '';
	$appp_address = '';
	$cp_address = '';
	$ville_address = '';
}
	
/* Pour l'ajout d'une adresse */
if(isset($_POST['add'])) {
	
	$modif=tep_db_query("INSERT INTO ".TABLE_ADDRESS_BOOK." (
								customers_id,
								entry_address_title,
								entry_gender,
								entry_company,
								entry_firstname,
								entry_lastname,
								entry_street_address,
								entry_suburb,
								entry_street_address_3,
								entry_street_address_4,
								entry_postcode,
								entry_city,
								entry_country_id
							) VALUES (
								".$_SESSION['customer_id'].",
								 '". $titre_address ."',
								 '".$_POST['civi']."',
								 '". $societe_address ."',
								 '". $prenom_address ."',
								 '". $nom_address ."',
								 '". $a_address ."',
								 '". $ap_address ."',
								 '". $app_address ."',
								 '". $appp_address ."',
								 '". $cp_address ."',
								 '". $ville_address ."',
								 ". $_POST['pays'].")"
							);
			 
	$req=tep_db_query("	SELECT max(address_book_id) AS max, entry_country_id 
						FROM ".TABLE_ADDRESS_BOOK." 
						WHERE customers_id=".$_SESSION['customer_id']." 
						GROUP BY customers_id");
	$res=tep_db_fetch_array($req);		
				 
	if ($_POST['add_livr']=='1') {
		$_SESSION['adresse_livraison']=$res['max'];
		$_SESSION['customer_country_id']=$res['entry_country_id'];
	}
}
	
/* Pour la modification d'une adresse */
if (isset($_POST['mod'])) {
	$modif=tep_db_query("	UPDATE ".TABLE_ADDRESS_BOOK." SET 
								entry_address_title='". $titre_address ."',
								entry_gender='".$_POST['civi']."',
								entry_company='". $societe_address ."',
								entry_firstname='". $prenom_address ."',
								entry_lastname='". $nom_address ."',
								entry_street_address='". $a_address ."',
								entry_suburb='". $ap_address ."',
								entry_street_address_3='". $app_address ."',
								entry_street_address_4='". $appp_address ."',
								entry_postcode='". $cp_address ."',
								entry_city='". $ville_address ."',
								entry_country_id='". $pays ."'
							WHERE 
								address_book_id=". $_SESSION['adresse_livraison']."");
									
	$_SESSION['adresse_livraison']=$_POST['id_adresse'];
	$_SESSION['customer_country_id']=$_POST['pays'];
}

$id_modif = '';
?>

<?php
// SEULEMENT POUR THIERRY
/*if ($_SESSION['customer_id']==3) {
	print_r($_SESSION);
	echo '<br>';
	echo '<br><br>';
	echo 'Customers_type : ';
	echo $_SESSION['customers_type']; 
	echo '<br><br>';
	print_r($_POST);
}*/
?>

<style type="text/css">
.container { display: block; position: relative; padding-left: 35px; margin-bottom: 12px; cursor: pointer; font-size: 22px; 
	-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;
}
.container input { position: absolute; opacity: 0; cursor: pointer; height: 0; width: 0;}
.checkmark { position: absolute; top: 0; left: 0; height: 15px; width: 15px; background-color: #eee; border-radius: 50%; border: solid 2px #808080;}
.container:hover input ~ .checkmark { background-color: #ccc; }
.container input:checked ~ .checkmark { background-color: #EEEEEE; /*#2196F3*/ }
.checkmark:after { content: ""; position: absolute; display: none; }
.container input:checked ~ .checkmark:after { display: block; }
.container .checkmark:after { top: 3px; left: 3px; width: 9px; height: 9px; border-radius: 50%; background: #676767; }
</style>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/panier_livraison.js"></script>
<div id="corps">
	<div id="panier">
		<div class="titre" style="margin-bottom:10px;">Choisissez l'adresse &agrave; laquelle vous souhaitez &ecirc;tre livr&eacute;</div>
		<div style="float:left;">
			<div  class="bordure_haut_verte"></div>
			<div class="bordure_pixel_vert">
				<div style="float:left; margin-left:10px; margin-bottom:15px;"><strong>L'adresse de livraison est la suivante :</strong></div>
				<div class="clear"></div>
				<div id="block_adresse_choisie" style="float:left; margin-left:30px; height:160px; width:300px; font-size:13px;">
				<?php
				$adresse_defaut=tep_db_query("	SELECT c.customers_default_address_id, a.* 
												FROM ".TABLE_CUSTOMERS." c, ".TABLE_ADDRESS_BOOK." a 
												WHERE 
													a.customers_id=".$_SESSION['customer_id']." 
												AND 
													c.customers_id = a.customers_id
											");
						  
				while ($res=tep_db_fetch_array($adresse_defaut)) {
					
					//Si la variable $_POST['truc'] existe, alors $truc = $_POST['truc']  sinon elle vaut NULL ou autre 
					$SESSION_adresse_livraison = isset($_SESSION['adresse_livraison']) ? $_SESSION['adresse_livraison'] : $res['address_book_id'];					 
					echo'<input type="hidden" id="cache" value="'.$SESSION_adresse_livraison.'" />';
					
					
					echo'<div id="block_info'.$res['address_book_id'].'"';
							
					if	( 
							(isset($_SESSION['adresse_livraison']) && !empty($_SESSION['adresse_livraison'])) 
							&& 
							// si la session 'adresse_livraison' est egale a l'adresse par default
							$_SESSION['adresse_livraison']==$res['address_book_id']
						) {
					
						echo'style="display:block;">';
						echo '<input type="hidden" id="address_book_id" value="'. $res['address_book_id'] .'" />';
						echo '<input type="hidden" id="entry_address_title" value="'. $res['entry_address_title'] .'" />';
						echo '<input type="hidden" id="entry_gender" value="'. $res['entry_gender'] .'" />';
						echo '<input type="hidden" id="entry_firstname" value="'. $res['entry_firstname'] .'" />';
						echo '<input type="hidden" id="entry_lastname" value="'. $res['entry_lastname'] .'" />';
						echo '<input type="hidden" id="entry_company" value="'. $res['entry_company'] .'" />';
						echo '<input type="hidden" id="entry_street_address" value="'. $res['entry_street_address'] .'" />';
						echo '<input type="hidden" id="entry_suburb" value="'. $res['entry_suburb'] .'" />';
						echo '<input type="hidden" id="entry_street_address_3" value="'. $res['entry_street_address_3'] .'" />';
						echo '<input type="hidden" id="entry_street_address_4" value="'. $res['entry_street_address_4'] .'" />';
						echo '<input type="hidden" id="entry_postcode" value="'. $res['entry_postcode'] .'" />';
						echo '<input type="hidden" id="entry_city" value="'. $res['entry_city'] .'" />';
						echo '<input type="hidden" id="entry_country_id" value="'. $res['entry_country_id'] .'" />';
							
					} elseif (!isset($_SESSION['adresse_livraison'])) {
						
						$_SESSION['adresse_livraison']=$res['address_book_id'];
						$_SESSION['frais_livraison']=prix_livraison_par_adresse($res['entry_country_id']);
						
						echo'style="display:block;">';		
						echo '<input type="hidden" id="address_book_id" value="'. $res['address_book_id'] .'" />';
						echo '<input type="hidden" id="entry_address_title" value="'. $res['entry_address_title'] .'" />';
						echo '<input type="hidden" id="entry_gender" value="'. $res['entry_gender'] .'" />';
						echo '<input type="hidden" id="entry_firstname" value="'. $res['entry_firstname'] .'" />';
						echo '<input type="hidden" id="entry_lastname" value="'. $res['entry_lastname'] .'" />';
						echo '<input type="hidden" id="entry_company" value="'. $res['entry_company'] .'" />';
						echo '<input type="hidden" id="entry_street_address" value="'. $res['entry_street_address'] .'" />';
						echo '<input type="hidden" id="entry_suburb" value="'. $res['entry_suburb'] .'" />';
						echo '<input type="hidden" id="entry_street_address_3" value="'. $res['entry_street_address_3'] .'" />';
						echo '<input type="hidden" id="entry_street_address_4" value="'. $res['entry_street_address_4'] .'" />';
						echo '<input type="hidden" id="entry_postcode" value="'. $res['entry_postcode'] .'" />';
						echo '<input type="hidden" id="entry_city" value="'. $res['entry_city'] .'" />';
						echo '<input type="hidden" id="entry_country_id" value="'. $res['entry_country_id'] .'" />';
					} else {
						echo'style="display:none;">';
					}
							
					$pays_adresse=tep_db_query("SELECT countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id=".$res['entry_country_id']."");
					$res_pays=tep_db_fetch_array($pays_adresse);
							
					$company = ($res['entry_company']=='') ? '' : $res['entry_company'].'<br>';
					//$complement = ($res['entry_suburb']=='') ? '' : $res['entry_suburb'].'<br>';
							
					$adresse = '';
					$adresse .= ($res['entry_street_address_3']=='') ? '' : $res['entry_street_address_3'].'<br>';
					$adresse .= ($res['entry_suburb']=='') ? '' : $res['entry_suburb'].'<br>';
					$adresse .= ($res['entry_street_address']=='') ? '' : $res['entry_street_address'].'<br>';
					$adresse .= ($res['entry_street_address_4']=='') ? '' : $res['entry_street_address_4'].'<br>';
							
					echo $company.$res['entry_lastname']." ".$res['entry_firstname']."<br>".$adresse.$res['entry_postcode']." ".$res['entry_city']."<br>".$res_pays['countries_name'];
					echo'<br />';
					echo '<div style="margin-top:5px;">
							<a href="https://www.generalarmystore.fr/gas/compte/address_book.php"><img class="pointer" src="template/base/boutons/bouton_modifier_ardesse.png" title="modifier cette adresse" />
							</a>
							</div>';
					echo'</div>';
				} ?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="bordure_bas_verte"></div>
		</div>
				
		<?php 
        $addressToShow = "";
        $address_book_id = isset($r['address_book_id']) ? $r['address_book_id'] : "";
		$i = 1;
		$showButtonToContinue = false;
		
		$req = tep_db_query("	SELECT cus.customers_default_address_id, a.*  
								FROM ".TABLE_ADDRESS_BOOK." a, ".TABLE_CUSTOMERS." cus 
								WHERE a.customers_id=".$_SESSION['customer_id']." AND cus.customers_id = a.customers_id 
								ORDER BY address_book_id asc");
		
		while ($r = tep_db_fetch_array($req)) {
			
			$prix_livraison = prix_livraison_par_adresse($r['entry_country_id']);
						 
			$addressToShow .= '<div style="background-color:#d6deb0; height:70px; margin-bottom:10px; text-align:left; padding:10px 5px 10px 10px; " id="blop'.$address_book_id.'">
			<input type="hidden" id="id_adresse'.$address_book_id.'" name="id_client" value="'.$address_book_id.'" />
			<div style="float:left; width:95%;">
				<div style="float:left;">'.$i.'</div>
				<div style="float:right;">
					'. (($prix_livraison=='') ? 'Le poids de votre commande est trop important pour &ecirc;tre livr&eacute; &agrave; cette adresse' : 'Prix de la livraison &agrave; cette adresse 
					<strong>'. format_to_money(prix_livraison_par_adresse($r['entry_country_id'])) .' &euro;</strong>') .'
				</div>
				<div style="margin-left:20px; margin-bottom:10px;"><strong>'.strtoupper($r['entry_address_title']).'</strong></div>
				<div style="width:900px; height:30px;">
					<div style="float:left; margin-left:20px;">';
						$addressToShow .=  strtoupper($r['entry_company']);
						if(!empty($r['entry_company'])) $addressToShow .=  ' - ';
												
							$addressToShow .= ' '.$r['entry_lastname'];
							$addressToShow .= ' '.$r['entry_firstname'].'<br>';			
							$addressToShow .=  $r['entry_street_address_3']. ' ';
							$addressToShow .=  $r['entry_suburb']. ' ';
							$addressToShow .=  $r['entry_street_address']. ' ';
							$addressToShow .=  $r['entry_street_address_4']. ' ';
							$addressToShow .=  '<br>'.$r['entry_postcode'];
							$addressToShow .=  ' '.$r['entry_city'];
							$pays = tep_db_query("	SELECT countries_name
													FROM countries  
													WHERE countries_id =".$r['entry_country_id']."
												");
							$pays_r = tep_db_fetch_array($pays);
							 
							$addressToShow .= ' ('.$pays_r['countries_name']. ')';
							
							$addressToShow .= '</div>
					</div>
					<input type="hidden" id="frais'.$r['address_book_id'].'" value='.format_to_money($prix_livraison).' />
				  
				</div>
				<div style="float:right; margin-top:0px; position:relative;">
					<label class="container">
						<input ';
						if ($prix_livraison=='') $addressToShow .= 'disabled="disabled" ';
							$addressToShow .= 'type="radio" id="modifier'.$r['address_book_id'].'" name="clique" onclick="modifier('.$r['address_book_id'].')" ';
						if((isset($_SESSION['adresse_livraison']) && !empty($_SESSION['adresse_livraison'])) && $_SESSION['adresse_livraison']==$r['address_book_id'] && $prix_livraison!='') {
							$addressToShow .= 'checked="checked"';
							$showButtonToContinue = true;
						} elseif (!isset($_SESSION['adresse_livraison']) && $r['address_book_id']==$r['customers_default_address_id'] && $prix_livraison!='') {
							$addressToShow .= 'checked="checked"';
							$showButtonToContinue = true;
						}
						$addressToShow .= '/>
						<span class="checkmark"></span>
					</label>
				</div>
			</div>';
			$i++;
			} 
			?>
			
		<div style="width:464px; margin-top:0px; margin-right:0px; float:right;">
			<div  class="bordure_haut_verte"></div>
				<div class="bordure_pixel_vert">
					
					<div style="text-align:center; height:166px; padding-top:20px;">
						<div style="float:center; margin:10px 20px 0 20px; font-size:14px; font-weight:600; ">
						Le montant des frais pour une <br>livraison � cette adresse sont de <?php echo prix_livraison(); ?></div><br>
						<?php 
						if ($showButtonToContinue) { 
							echo '<a href="panier_paiement.php"><img src="template/base/panier/bouton_finir-cmd.png" title="Finaliser ma commande" /></a>';
						} else { 
							echo 'Veuillez choisir une adresse valide pour poursuivre votre commande.';
						}
						?>
					</div>
				</div>
			<div  class="bordure_bas_verte"></div>
		</div>
		<div class="clear"></div>
		
		<div class="titre" style="margin-top:20px;">Veuillez s�lectionner l'adresse de livraison pour cette commande</div>
		<div id="block_liste_adresse" style="width:100%; margin-top:10px;">
			<?php echo $addressToShow; ?>
		</div>
		<?php
		$nb_max_address = 10;
		$nb = tep_db_num_rows($req);
		if ($nb < $nb_max_address) { ?>
			<div style="text-align:left;">
				<img class="pointer add_address" src="template/base/boutons/bouton_ajouter_une_nouvelle_adresse.png" title="AJouter une nouvelle adresse" />
                </a>
			</div>
		<?php } ?>
		
		<div style="text-align:right;">
			<?php 
			if ($showButtonToContinue) {
				echo '<a href="panier_paiement.php"><img src="template/base/panier/bouton_finir-cmd.png" title="Finaliser ma commande"  /></a>';
				
				if ($_SESSION['customers_type']==4) { 
					echo '<div style="text-align:right; font-size:14px; padding-top:12px;">Cliquez sur le bouton ci-dessus pour<br> passer votre commande ou cr�er votre devis</div>';
				} 
			
			} else {
				echo 'Veuillez choisir une adresse valide pour poursuivre votre commande.';
			} 
			?>
		</div>
	</div>
</div>
	
	<!-- LIGHTBOX AJOUT ADRESSE -->
	<div id="overlay"></div>

	<div id="lightbox">
		<div id="lightbox_ajout_adresse" style="padding-left:100px;">
		
		<form name="adresse" method="post" action="<?php echo BASE_DIR;?>/panier_livraison.php">
            <div style="float:right"><img class="close_lightbox pointer" src="template/base/boutons/icone_fermer.png" title="fermer"/></div>
			<h1>Ajouter une adresse</h1>
            <p style="width:300px; text-align:right;">Titre (ex : Bureau, Domicile) :&nbsp;&nbsp;</p>
            <input type="text" id="titre_address" name="titre_address" value="" size="25" maxlength="32" style="text-transform:Uppercase;" />
			<span class="obligatoire">*</span>
            
            <div class="row">
                <p style="width:300px; text-align:right;">Civilit� :&nbsp;&nbsp;</p> 
                &nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" id="mr" name="civi" value="m" 
				<?php if($res['entry_gender']=='m') echo 'checked="checked"'; ?> />
				Mr&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="mme" name="civi" value="f" 
				<?php if($res['entry_gender']=='f') echo 'checked="checked"';?> />
				Mme&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="mlle" name="civi" value="d"  
				<?php if($res['entry_gender']=='d') echo 'checked="checked"'; ?> />
				Mlle&nbsp;&nbsp;&nbsp;&nbsp;
				<span class="obligatoire">*</span>
            </div>
            
            <div class="row">
                <div style="float:left;">
                 <p style="width:300px; text-align:right;">Nom :&nbsp;&nbsp;</p><input type="text" id="nom_address" name="nom_address" value="<?php echo $res['entry_lastname']; ?>" size="25" maxlength="32" style="text-transform:Uppercase;" />
				 <span class="obligatoire">*</span>
               </div>
               <div class="clear"></div>
            </div>
			
			<div class="row">
               <div style="float:left;">
                 <p style="width:300px; text-align:right;">Pr&eacute;nom :&nbsp;&nbsp;</p><input type="text" id="prenom_address" name="prenom_address" value="<?php echo $res['entry_firstname']; ?>" size="25" maxlength="32"style="text-transform:capitalize;" />
				 <span class="obligatoire">*</span>
               </div>
               <div class="clear"></div>
            </div>
           	
            <div class="row">
				<p style="width:300px; text-align:right;">Societe :&nbsp;&nbsp;</p><input type="text" id="societe_address" name="societe_address" value="<?php echo $res['entry_company']; ?>" size="25" maxlength="32" style="text-transform:Uppercase;" />
            </div>
            			
			<div class="row">
				<p style="width:300px; text-align:right;">Appt - Etage - Couloir - Esc. :&nbsp;&nbsp;</p><input type="text" id="app_address" name="app_address" value="<?php echo $res['entry_street_address_3']; ?>" size="25" maxlength="35" style="text-transform:capitalize;" />
            </div>
			<div class="row">
				<p style="width:300px; text-align:right;">Entr�e - B�t. - Imm. - R�s. :&nbsp;&nbsp;</p><input type="text" id="ap_address" name="ap_address" value="<?php echo $res['entry_suburb']; ?>" size="25" maxlength="35" style="text-transform:capitalize;" />
            </div>
            <div class="row">
				<p style="width:300px; text-align:right;">N� + Voie (rue, avenue, bvd, �) :&nbsp;&nbsp;</p><input type="text" id="a_address" name="a_address" value="<?php echo $res['entry_street_address']; ?>" size="25" maxlength="35" style="text-transform:capitalize;" />
				<span class="obligatoire">*</span>
            </div>
			<div class="row">
				<p style="width:300px; text-align:right;">Boite Postale - Lieu Dit :&nbsp;&nbsp;</p><input type="text" id="appp_address" name="appp_address" value="<?php echo $res['entry_street_address_4']; ?>" size="25" maxlength="35" style="text-transform:capitalize;" />
            </div>
			
			<div class="row">
				<div style="float:left;">
					<p style="width:300px; text-align:right;">Code postal :&nbsp;&nbsp;</p>
					<input type="text" id="cp_address" name="cp_address" value="<?php echo $res['entry_postcode']; ?>" size="25" maxlength="10" style="text-transform:Uppercase;" />
					<span class="obligatoire">*</span>
				</div>
				<div class="clear"></div>
            </div>
			
			<div class="row">
				<div style="float:left;">
					<p style="width:300px; text-align:right;">Ville : &nbsp;&nbsp;</p>
					<input type="text" id="ville_address"  name="ville_address" value="<?php echo $res['entry_city']; ?>" size="25" maxlength="35" style="text-transform:Uppercase;" />
					<span class="obligatoire">*</span>
				</div>
				<div class="clear"></div>
			</div>
             
			<div class="row">
				<p style="width:300px; text-align:right;">Pays :&nbsp;&nbsp;</p>
				<select id="pays" name="pays" style="width:208px;">     
					
					<option value="">S�lectionnez un pays</option>
					<?php  
					
					if (isset($_POST['pays'])) { $_POST['pays'] =''; }
					// S�lection des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
					$req_pays_base=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND
					countries_id IN (1,247,272,274,273,275,271)
					");
					while($res_req_pays_base=tep_db_fetch_array($req_pays_base)){
					?>
						<?php $POST_pays = isset($_POST['pays']) ? $_POST['pays'] : NULL; // ajout THIERRY 24/03/2020 ?>
						<option <?php if($POST_pays==$res_req_pays_base['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res_req_pays_base['countries_id'];?>">
						<?php echo $res_req_pays_base['countries_name'];?>
						</option>
					<?php
					} 
					?>					
					<optgroup label=" -------------------"> </optgroup>
					<?php 
					// retrait des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
					$req_pays=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND countries_id NOT IN (1,247,272,274,273,275,271) ORDER BY countries_name");
					while($res=tep_db_fetch_array($req_pays)){
					?>
						<option <?php if($POST_pays==$res['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res['countries_id'];?>">
						<?php echo $res['countries_name'];?>
						</option>
					<?php } ?>
				</select>
				<span class="obligatoire">*</span>
			</div>
            
			<div class="clear"></div>
           
			<input type="hidden" name="id_adresse" id="id_adresse" value="<?php echo $_SESSION['adresse_livraison'];?> " />
			<div style="text-align:center; "><input type="checkbox" name="add_livr" value="1" />Choisir cette adresse pour la livraison</div>
            
			<div class="center images"></div>
            </div>
        </form>
    </div>
    
<?php
require("includes/footer.php"); 
require("includes/page_bottom.php");
}//}
?>