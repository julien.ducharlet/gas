<?php
/* Page listant les devis du client */

$nom_page = "devis";

require("../includes/page_top.php");

include("fonction/session.php");

require("../includes/modules/mod_paiement_spplus.php");

if(!empty($_SESSION['customer_id'])){

	require("../includes/meta_head.php");
	// JULIEN 12/03
	if( (isset ($_POST['valide'])) && ($_POST['valide']=="ok") ) {
		$_SESSION['total_panier']=format_to_money($_POST['montant']);
		echo lien_paiement_spplus($id_magasin,$siret,$_POST['commande']);
	}
	
	$req_order=tep_db_query("	SELECT 
									o.orders_id AS od, 
									o.customers_id, 
									date_purchased, 
									orders_date_fin_devis, 
									payment_method, total, 
									orders_status AS os, 
									orders_status_name, 
									orders_numero_facture,   
									o.etat_paiement
								FROM ".TABLE_ORDERS." o, ".TABLE_ORDERS_STATUS." os
								WHERE 
									o.orders_status=os.orders_status_id AND									
									o.orders_status='21' AND
									o.customers_id=".$_SESSION['customer_id']."
								ORDER BY date_purchased DESC");
	
	$nb_com=tep_db_num_rows($req_order);
	
	require("../includes/header.php"); ?>

	<div id="corps">

		<div id="bande_rubrique_g">
			<div id="bande_image">
				<img src="../template/base/compte/icones/commande.png" alt="inf_commande" />
			</div>
			<div id="bande_titre">Mes devis</div>
			<div id="bande_sous_titre">
			 <?php
				if($nb_com!=0)
				{echo"Cliquez sur le num&eacute;ro d'un devis pour voir le d&eacute;tail";}			
				else{echo"Vous n'avez pas de devis sur notre site";}
			 ?>
			</div>
		</div>
		<div id="bande_rubrique_d">
		<a href="account.php" title="Retourner à mon compte"><img src="../template/base/compte/bouton_retour.png" alt="retour" /></a>
		</div>
		<div style="clear:both;"></div>

	 <?php
	if($nb_com!=0)
		{
	?>
	<div id="info_com_gene">
		Vous avez <?php echo $nb_com;?> devis sur notre site
	</div>
	<div style="clear:both;"></div>


	<div id="commande_barre_info">
		<div class="item_num_com" style="width:70px;"><strong>N&deg; devis</strong></div>
		<div class="item_date_com" style="width:230px;"><strong>Date de cr&eacute;ation</strong></div>
		<div class="item_date_com" style="width:230px;"><strong>Date d'expiration</strong></div>
		<div class="item_montant_com" style="width:100px;"><strong>Montant TTC</strong></div>
		<div class="item_montant_com" style="width:100px;"><strong>Devis</strong></div>
		<div class="item_suivi_com" style="width:220px;"><strong>Action</strong></div>
		<div style="clear:both;"></div>
	</div>
	<?php

		$i=0;
		while($res=tep_db_fetch_array($req_order)) {
			if($i % 2==0){
				$classe=1;
			} else {
				$classe=2;
			}
			
			$timestamp_devis1 = strtotime($res['date_purchased']);
			$date_devis1 = html_entity_decode(date_fr($timestamp_devis1));
				
			$timestamp_devis2 = strtotime($res['orders_date_fin_devis']);
			$date_devis2 = html_entity_decode(date_fr($timestamp_devis2));
			
			echo'<div  class="ligne_commade_info'.$classe.'">
					 <div class="item_num_com" style="width:70px;"><a href="devis_details.php?id_com='.$res['od'].'" title="Voir le d&eacute;tail de cette commande">'.$res['od'].'</a></div>
					 <div class="item_date_com" style="width:230px;">'.$date_devis1.'</div>
					 <div class="item_date_com" style="width:230px;">'.$date_devis2.'</div>
					 <div class="item_montant_com" style="width:100px; align:right"><strong>'.$res['total'].' &euro;</strong></div>
					 
					 <div class="item_facture_com" style="width:100px;">
					 <a href="generation_pdf/facture.php?id_com='.$res['od'].'&cli='.md5($res['customers_id']).'" target="_blank" title="Votre Devis Proforma au format pdf">
					 <img src="../template/base/icones/icone_pdf.png" alt="facture pdf" border="none"/>
					 </a>
					 </div>
					 
					 <div class="item_suivi_com2" style="width:220px;">';
						$today = date("Y-m-d H:i:s");
						$nbjours = round((strtotime($today) - strtotime($res['orders_date_fin_devis']))/(60*60*24)-1);
						if ($nbjours<0) {
							echo'<a href="devis_details.php?id_com='.$res['od'].'#moyen_paiement">Valider ce devis</a>';
						} else { 
							echo 'Date d&eacute;pass&eacute;e Contactez <br>nous au '. TEL_NORMAL;
						}
					 echo'</div>
				   <div style="clear:both;"></div>';
				   
				echo '</div>';
				
				$i++;
		}
	} else {
	?>    
		<div style="background-image:url(../template/base/compte/image_no-cmd.png); width:716px; height:176px;"></div><br><br><br>
		<div style="clear:both;"></div>    
	<?php 
	} 
	?>
</div>
<?php // if($_SERVER['REMOTE_ADDR']== '78.245.140.130') printr($_SESSION);
} else { 
	header('Location: connexion.php');
}

require("../includes/footer.php");
require("../includes/page_bottom.php"); 
?>
