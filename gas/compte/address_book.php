<?php
/* Page permettant au client de pouvoir creer, modifier et supprimer les adresses associ�ss � sont compte */
// Notice: Undefined variable: _POST_civi in /var/www/vhosts/generalarmystore.fr/httpdocs/gas/compte/address_book.php on line 96
// Notice: Undefined index: default_address in /var/www/vhosts/generalarmystore.fr/httpdocs/gas/compte/address_book.php on line 164


$nom_page = "address_book";
   
require("../includes/page_top.php");

include("fonction/session.php");
include("fonction/address_book.php");

if(!empty($_SESSION['customer_id'])) {
	
	require("../includes/meta_head.php");
	require("../includes/header.php"); ?>
	
	<div id="corps">
		<div id="bande_rubrique_g">
			<div id="bande_image"><img src="../template/base/compte/icones/cahier.png" alt="inf_cahier" /></div>
			<div id="bande_titre">Gestion de votre carnet d'adresses</div>
			<div id="bande_sous_titre">Vous permet de cr&eacute;er, de modifier ou d'effacer une adresse.</div>
		</div>
		<div id="bande_rubrique_d"><a href="account.php" title="Retourner � mon compte"><img src="../template/base/compte/bouton_retour.png" alt="retour" /></a></div>
		<div class="clear"></div>		
		
		<?php
		if ($session_customer_id == 0) {
			echo '<hr>$_POST : ';
			print_r($_POST);
			echo '<hr>';
			echo '$_SESSION : ';
			print_r($_SESSION);
			echo '<hr>';
		}
		
		$_POST['action'] = !empty($_POST['action']) ? $_POST['action'] : '';
		if ($_POST['action'] == 'supprimer_adresse') {
			// ici on met la requete de suppression en BDD $_POST['id_address']
			echo supprimer_adresse($_POST['address_book_id']);		
		}
		
$titre_address = (!empty($_POST['titre_address'])) ? strtoupper(format_inscription($_POST['titre_address'])) : '';
$nom_address = (!empty($_POST['nom_address'])) ? strtoupper(format_inscription($_POST['nom_address'])) : '';
$prenom_address = (!empty($_POST['prenom_address'])) ? format_inscription(ucwords(strtolower($_POST['prenom_address']))) : '';
$societe_address = (!empty($_POST['societe_address'])) ? strtoupper(format_inscription($_POST['societe_address'])) : '';
$a_address = (!empty($_POST['a_address'])) ? ucwords(strtolower(format_inscription($_POST['a_address']))) : '';
$ap_address = (!empty($_POST['ap_address'])) ? ucwords(strtolower(format_inscription($_POST['ap_address']))) : '';
$app_address = (!empty($_POST['app_address'])) ? ucwords(strtolower(format_inscription($_POST['app_address']))) : '';
$appp_address = (!empty($_POST['appp_address'])) ? ucwords(strtolower(format_inscription($_POST['appp_address']))) : '';	
$cp_address = (!empty($_POST['cp_address'])) ? strtoupper(format_inscription($_POST['cp_address'])) : '';
$ville_address = (!empty($_POST['ville_address'])) ? strtoupper(format_inscription($_POST['ville_address'])) : '';
		
		?>
		
		<div>		
			<?php 
			// Si l'action c'est ajouter on affiche la zone d'ajout
			if ($_POST['action'] == 'ajouter') { 						
			?>
				<div style="float:right; margin:0 210px 5px 0;">
					<span class="obligatoire">* les informations obligatoires</span>
				</div>
				<div style="clear:both;"></div>
				<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>"> 
					
					<div style="width:100%; float:left; margin-top:20px; margin-right:0px;">
						
						<div class="titre_box-contenu">Ajouter une nouvelle adresse</div>
						<div class="box-contenu">

								<div class="conteneur_desc_item">
									<div class="item_i">Titre de cette adresse :</div>
									<div class="item_i">Civilit� :</div>
									<div class="item_i">Nom :</div>
									<div class="item_i">Pr�nom :</div>
									<div class="item_i">Soci�t� - Administration - Etc... :</div>
									<div class="item_i">Appt - Etage - Couloir - Esc. :</div>
									<div class="item_i">Entr�e - B�t. - Imm. - R�s. :</div>
									<div class="item_i">N� + Voie (rue, avenue, bvd,�) :</div>
									<div class="item_i">Boite Postale - Lieu Dit :</div>
									<div class="item_i">Code postal :</div>
									<div class="item_i">Ville :</div>
									<div class="item_i">Pays :</div>
								</div>
								
								<div class="conteneur_composant_item">
									<div class="item_i">
										<input type="text" name="titre_address" value="<?php echo $titre_address; ?>" style="text-transform:Uppercase;" size="25" maxlength="31" required="required" />
										&nbsp;<span class="obligatoire">*</span>
									</div>
									<div class="item_i">
										<?php $_POST['civi'] = isset($_POST['civi']) ? $_POST['civi'] : NULL; // ajout THIERRY 24/03/2020 ?>
										&nbsp;&nbsp;&nbsp;
										<input type="radio" name="civi" value="m" <?php if ($_POST['civi']=='m') echo 'checked="checked"'; ?> /><span style="color:#000;">Mr</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="civi" value="f" <?php if ($_POST['civi']=='f') echo 'checked="checked"'; ?> /><span style="color:#000;">Mme</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="civi" value="d" <?php if ($_POST['civi']=='d') echo 'checked="checked"'; ?> /><span style="color:#000;">Mlle</span>&nbsp;&nbsp;&nbsp;&nbsp;
										<span class="obligatoire">*</span>
									</div>
									<div class="item_i">
										<input type="text" name="nom_address" value="<?php echo $nom_address; ?>" style="text-transform:Uppercase;" size="25" maxlength="32" required="required" />
										&nbsp;<span class="obligatoire">*</span>
									</div>
									<div class="item_i">
										<input type="text" name="prenom_address" value="<?php echo $prenom_address; ?>" style="text-transform:capitalize;" size="25" maxlength="32" required="required" />
										&nbsp;<span class="obligatoire">*</span>
									</div>
									
									<div class="item_i">
										<input type="text" name="societe_address" value="<?php echo $societe_address; ?>" style="text-transform:Uppercase;" size="25" maxlength="32" />
									</div>
									<div class="item_i">
										<input type="text" name="app_address" value="<?php echo $app_address; ?>" size="25" maxlength="35" />
									</div>
									<div class="item_i">
										<input type="text" name="ap_address" value="<?php echo $ap_address; ?>" size="25" maxlength="35" />
									</div>
									<div class="item_i">
										<input type="text" name="a_address" value="<?php echo $a_address; ?>" size="25" maxlength="35" required="required" />
										<span class="obligatoire">*</span>
									</div>
									<div class="item_i">
										<input type="text" name="appp_address" value="<?php echo $appp_address; ?>" size="25" maxlength="35" />
									</div>
									<div class="item_i">
										<input type="text" name="cp_address" value="<?php echo $cp_address; ?>" style="text-transform:Uppercase;" size="25"  maxlength="10" required="required" />
										<span class="obligatoire">*</span>
									</div>
									<div class="item_i">
										<input type="text" name="ville_address" value="<?php echo $ville_address; ?>" style="text-transform:Uppercase;" size="25" maxlength="35" required="required" />
										<span class="obligatoire">*</span>
									</div>
									<div class="item_i">
										<select id="pays" name="pays" style="width:208px;" required="required">  
											<option value="">S�lectionnez un pays</option>
											<?php  
											// S�lection des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
											$req_pays_base=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND
											countries_id IN (1,247,272,274,273,275,271)
											");
											while($res_req_pays_base=tep_db_fetch_array($req_pays_base)){
											?>
												<?php $POST_pays = isset($_POST['pays']) ? $_POST['pays'] : NULL; // ajout THIERRY 24/03/2020 ?>
												<option <?php if($POST_pays==$res_req_pays_base['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res_req_pays_base['countries_id'];?>">
												<?php echo $res_req_pays_base['countries_name'];?>
												</option>
											<?php } ?>
											
											<optgroup label=" -------------------"> </optgroup>
											
											<?php 
											// retrait des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
											$req_pays=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND countries_id NOT IN (1,247,272,274,273,275,271) ORDER BY countries_name");
											while($res=tep_db_fetch_array($req_pays)){
											?>
												<option <?php if($POST_pays==$res['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res['countries_id'];?>">
												<?php echo $res['countries_name'];?>
												</option>
											<?php } ?>
											
										</select> <span class="obligatoire">*</span>
									</div>
								</div>
							<div class="clear"></div>
							<div class="item_i">
								<?php $POST_default_address = isset($_POST['default_address']) ? $_POST['default_address'] : NULL; // ajout THIERRY 24/03/2020 ?>
								<input type="checkbox" name="default_address" <?php if($POST_default_address!='') echo'checked="checked"'; ?> value="da" />D�finir comme adresse par d�faut / facturation<br>
							</div>
						</div>
					
					</div>
					<div class="clear"></div>
					
					<input type="hidden" id="id_client" name="id_client" value="<?php echo $_SESSION['customer_id']; ?>" />
					<input type="hidden" name="action" value="ajout_adresse_ok" />
					<input type="submit" value="Ajouter cette nouvelle adresse" class="button" />
				</form>
		
		<?php 
		// Si l'action c'est editer on affiche la zone d'�dition
		} elseif ($_POST['action'] == 'editer') { 				
		?>
			
			<div style="float:right; margin:0 210px 5px 0;">
				<span class="obligatoire">* les informations obligatoires</span>
			</div>
			<div style="clear:both;"></div>
			<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>"> 
					
				<div style="width:100%; float:left; margin-top:20px; margin-right:0px;">
					
					<div class="titre_box-contenu">Modifier une adresse</div>
					<div class="box-contenu">

							<div class="conteneur_desc_item">
								<div class="item_i">Titre de cette adresse :</div>
								<div class="item_i">Civilit� :</div>
								<div class="item_i">Nom :</div>
								<div class="item_i">Pr�nom :</div>
								<div class="item_i">Soci�t� - Administration - Etc... :</div>
								<div class="item_i">Appt - Etage - Couloir - Esc. :</div>
								<div class="item_i">Entr�e - B�t. - Imm. - R�s. :</div>
								<div class="item_i">N� + Voie (rue, avenue, bvd,�) :</div>
								<div class="item_i">Boite Postale - Lieu Dit :</div>
								<div class="item_i">Code postal :</div>
								<div class="item_i">Ville :</div>
								<div class="item_i">Pays :</div>
							</div>
							
							<div class="conteneur_composant_item">
								<div class="item_i">
									<input type="text" name="titre_address" value="<?php echo $titre_address; ?>" style="text-transform:Uppercase;" size="25" maxlength="31" required="required" />
									&nbsp;<span class="obligatoire">*</span>
								</div>
								<div class="item_i">
									&nbsp;&nbsp;&nbsp;
									<?php $_POST['civi'] = isset($_POST['civi']) ? $_POST['civi'] : ''; // ajout THIERRY 24/03/2020 ?>
									<input type="radio" name="civi" value="m" <?php if($_POST['civi']=='m') echo 'checked="checked"'; ?> /><span style="color:#000;">Mr</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="radio" name="civi" value="f" <?php if($_POST['civi']=='f') echo 'checked="checked"'; ?> /><span style="color:#000;">Mme</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="radio" name="civi" value="d" <?php if($_POST['civi']=='d') echo 'checked="checked"'; ?> /><span style="color:#000;">Mlle</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<span class="obligatoire">*</span>
								</div>
								<div class="item_i">
									<input type="text" name="nom_address" value="<?php echo $nom_address; ?>" style="text-transform:Uppercase;" size="25" maxlength="32" required="required" />
									&nbsp;<span class="obligatoire">*</span>
								</div>
								<div class="item_i">
									<input type="text" name="prenom_address" value="<?php echo $prenom_address; ?>" style="text-transform:capitalize;" size="25" maxlength="32" required="required" />
									&nbsp;<span class="obligatoire">*</span>
								</div>
								
								<div class="item_i">
									<input type="text" name="societe_address" value="<?php echo $societe_address; ?>" style="text-transform:Uppercase;" size="25" maxlength="32" />
								</div>
								<div class="item_i">
									<input type="text" name="app_address" value="<?php echo $app_address; ?>" size="25" maxlength="35" />
								</div>
								<div class="item_i">
									<input type="text" name="ap_address" value="<?php echo $ap_address; ?>" size="25" maxlength="35" />
								</div>
								<div class="item_i">
									<input type="text" name="a_address" value="<?php echo $a_address; ?>" size="25" maxlength="35" required="required" />
									<span class="obligatoire">*</span>
								</div>
								<div class="item_i">
									<input type="text" name="appp_address" value="<?php echo $appp_address; ?>" size="25" maxlength="35" />
								</div>
								<div class="item_i">
									<input type="text" name="cp_address" value="<?php echo $cp_address; ?>" style="text-transform:Uppercase;" size="25"  maxlength="10" required="required" />
									<span class="obligatoire">*</span>
								</div>
								<div class="item_i">
									<input type="text" name="ville_address" value="<?php echo $ville_address; ?>" style="text-transform:Uppercase;" size="25" maxlength="35" required="required" />
									<span class="obligatoire">*</span>
								</div>
								<div class="item_i">
									<select id="pays" name="pays" style="width:208px;" required="required">  
										<option value="">S�lectionnez un pays</option>
										<?php  
										// S�lection des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
										$req_pays_base=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND
										countries_id IN (1,247,272,274,273,275,271)
										");
										while($res_req_pays_base=tep_db_fetch_array($req_pays_base)){
										?>
											<option <?php if($_POST['pays']==$res_req_pays_base['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res_req_pays_base['countries_id'];?>">
											<?php echo $res_req_pays_base['countries_name'];?>
											</option>
										<?php } ?>
										
										<optgroup label=" -------------------"> </optgroup>
										
										<?php 
										// retrait des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
										$req_pays=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND countries_id NOT IN (1,247,272,274,273,275,271) ORDER BY countries_name");
										while($res=tep_db_fetch_array($req_pays)){
										?>
											<option <?php if($_POST['pays']==$res['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res['countries_id'];?>">
											<?php echo $res['countries_name'];?>
											</option>
										<?php } ?>
										
									</select> <span class="obligatoire">*</span>
								</div>
							</div>
						<div class="clear"></div>
						<div class="item_i">
							<input type="checkbox" name="default_address" <?php if($_POST['default_address']==1) echo'checked="checked"'; ?> value="da" />D�finir comme adresse par d�faut / facturation<br>
						</div>
					</div>
					
				</div>
				<div class="clear"></div>
				
				<input type="hidden" id="id_client" name="id_client" value="<?php echo $_SESSION['customer_id']; ?>" />
				<input type="hidden" name="action" value="modif_adresse_ok" />
				<input type="hidden" name="address_book_id" value="<?php echo $_POST['address_book_id']; ?>" />
				<input type="submit" value="Enregistrer les modifications" class="button" />
			</form>
			
		<?php 
		} else { 
		
			if ($_POST['action'] == 'ajout_adresse_ok') { // Si l'action c'est ajout_adresse_ok on enregistre l'adresse
				echo '<div class="box-valide">L\'adresse vient d\'&ecirc;tre ajout&eacute;e &agrave; votre compte.</div>';
				echo adresse_ajout();
			} elseif ($_POST['action'] == 'modif_adresse_ok') { // Si l'action c'est modif_adresse_ok on enregistre l'adresse 
				echo adresse_modif();
				echo '<div class="box-valide">La modification &agrave; &eacute;t&eacute; prise en compte.</div>';
			}
		?>
			
			<div>	
				<div class="block">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" >
						<tr>
							<td><h1 style="margin:5px 0 0 10px;" >Mes adresses de livraison</h1></td>
						</tr> 
					</table>
					<div id="block_address"  style="padding:20px 5px 10px 5px;">
						
						<?php
						$i=1;
						$req=tep_db_query("SELECT * FROM ".TABLE_ADDRESS_BOOK." WHERE customers_id=".$_SESSION['customer_id']." ORDER BY address_book_id DESC");
						while($r=tep_db_fetch_array($req)){
							echo '
							<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#E1DEDD; margin-bottom:10px;-moz-border-radius:5px; -webkit-border-radius:5px; border-radius:5px;">
							<tr>
								<td style="text-align:left; width:92%; padding:10px 5px 10px 20px;">
									<strong>'.$i.'</strong>
									<span style="margin-left:20px;">
										<strong>'.strtoupper($r['entry_address_title']).'</strong>	
									</span>';
									// requete qui permet de trouver l'adresse par defaut du client / adresse facturation
									$adresse_defaut=tep_db_query("	SELECT customers_default_address_id 
														FROM ".TABLE_CUSTOMERS." 
														WHERE customers_default_address_id=".$r['address_book_id']."");
									//$resultat_defaut=tep_db_fetch_array($adresse_defaut);
									$nb=tep_db_num_rows($adresse_defaut);
									
									if ($nb==1) { echo '&nbsp;&nbsp;&nbsp;<font color="red"><strong>Adresse utilis�e pour la facturation</strong></font>'; }	
									
									echo '
									<div style="margin-left:33px; padding-top:10px">';
									if (!empty($r['entry_company'])) echo $r['entry_company']. ' - ';
									echo $r['entry_firstname'].' '.$r['entry_lastname'] .'<br>';	
									echo $r['entry_street_address_3'].' '.$r['entry_suburb'].' '.$r['entry_street_address'].' '.$r['entry_street_address_4'].'<br>';		
									echo $r['entry_postcode'] .' ' .$r['entry_city']. ' - ';
									
									$pays=tep_db_query("	SELECT countries_name
															FROM countries  
															WHERE countries_id =".$r['entry_country_id']."");
									$pays_r=tep_db_fetch_array($pays);
									echo $pays_r['countries_name'].'';
									echo '
									</div>
								</td>
								<td style="text-align:right; width:4%; padding:5px 5px 5px 10px;">';
								
									$requete=tep_db_query("	SELECT * 
															FROM ".TABLE_ADDRESS_BOOK." 
															WHERE address_book_id=".$r['address_book_id']."");
									$result=tep_db_fetch_array($requete);
									
									if ($nb==0) { $defaut=0; } else { $defaut=1; }									
									
									echo '<form method="post" action="'.$_SERVER['PHP_SELF'].'">
										<input type="hidden" name="action" value="editer"/>
										<input type="hidden" name="address_book_id" value="'.$result['address_book_id'].'"/>
										<input type="hidden" name="titre_address" value="'.$result['entry_address_title'].'"/>
										<input type="hidden" name="nom_address" value="'.$result['entry_lastname'].'"/>
										<input type="hidden" name="prenom_address" value="'.$result['entry_firstname'].'"/>
										<input type="hidden" name="societe_address" value="'.$result['entry_company'].'"/>
										<input type="hidden" name="app_address" value="'.$result['entry_street_address_3'].'"/>
										<input type="hidden" name="ap_address" value="'.$result['entry_suburb'].'"/>
										<input type="hidden" name="a_address" value="'.$result['entry_street_address'].'"/>
										<input type="hidden" name="appp_address" value="'.$result['entry_street_address_4'].'"/>
										<input type="hidden" name="cp_address" value="'.$result['entry_postcode'].'"/>
										<input type="hidden" name="ville_address" value="'.$result['entry_city'].'"/>
										<input type="hidden" name="pays" value="'.$result['entry_country_id'].'"/>
										<input type="hidden" name="civi" value="'.$result['entry_gender'].'"/>
										<input type="hidden" name="default_address" value="'.$defaut.'"/>
													
										
										<input type="image" src="../template/base/boutons/editer.png"  value="submit" title="Modifier cette adresse" class="pointer">
										</form></td><td style="text-align:right; width:4%; padding:5px 10px 5px 10px;">';
									$nbz=tep_db_num_rows($req);
									if($nbz>1){
										
										echo '<form method="post" action="'.$_SERVER['PHP_SELF'].'">
										<input type="hidden" name="action" value="supprimer_adresse"/>
										<input type="hidden" name="address_book_id" value="'.$r['address_book_id'].'"/>
										<input type="image" src="../template/base/boutons/icone_supprimer.png"  value="submit" title="Supprimer cette adresse" class="pointer">
										</form>';
									}
									echo'</td>
								</tr></table>';
								$i++;
						 }
						?>
						
						<?php // Si c'est une nouvelle adresse on vide les donn�es de $_POST
						if(isset($_POST['nouvelle'])){ unset($_POST); }
						?>
							
						<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
							<input type="hidden" name="customer_id" value="<?php echo $_SESSION['customer_id']; ?>" />
							<input type="hidden" name="action" value="ajouter"/>
							<input type="submit" value="Ajouter une nouvelle adresse" class="button" />
						</form>
					</div>
					
					<div class="clear"></div>
				</div>  	
				
			</div>
			
		<?php 
		} 
		?>		
		</div>
		<div class="clear"></div>
	</div>



<?php 
} else { header('Location: connexion.php'); }
require("../includes/footer.php");
require("../includes/page_bottom.php");
?>

