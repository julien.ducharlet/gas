<?php
/* Auteur : Sami 
   Page permettant au cleint de voir l'etat actuel de son porte monnaie virtuel*/

$nom_page = "electronic_purse";

require("../includes/page_top.php");

include("fonction/session.php");

if(!empty($_SESSION['customer_id'])) {
	
	require("../includes/meta_head.php");

	$req_purse=tep_db_query("SELECT customers_argent_virtuel FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id']." ");
	$total=tep_db_fetch_array($req_purse);
	
	require("../includes/header.php"); ?>

<div id="corps">
    <div id="bande_rubrique_g">
        <div id="bande_image"><img src="../template/base/compte/icones/porte_monnaie.png" alt="inf_commande" /></div>
        <div id="bande_titre">Mon porte-monnaie virtuel</div>
        <div id="bande_sous_titre">Visualiser le solde de votre porte-monnaie virtuel</div>
    </div>
    <div id="bande_rubrique_d">
    	<a href="account.php" title="Retourner � mon compte"><img src="../template/base/compte/bouton_retour.png" alt="retour" /></a>
    </div>
	<div style="clear:both;"></div>

	<div id="info_com_gene">
    	<div style="background-image:url(../template/base/inscription/fond_information_top.png); width:970px; height:19px;"></div>
        <div style="background-image:url(../template/base/inscription/fond_information_center.png); width:970px;">
        	
            Vous avez <strong><?php echo $total['customers_argent_virtuel'];?></strong> &euro; HT 
							  <?php if(taux_taxe()==1.2) echo "(soit ".format_to_money($total['customers_argent_virtuel']*1.2)." &euro; TTC)" ?>
            � utiliser sur notre site.<br /><br />

			Vous trouverez ci-dessous la raison et la liste des sommes qui ont &eacute;t&eacute; ajout&eacute;es &agrave;
			 votre compte client. <br /><br />

			Vous pouvez utiliser votre solde disponible sur cette page en le s&eacute;lectionnant dans les
		    choix des modes de paiement�!<br /> 
            
        </div>
        <div style="background-image:url(../template/base/inscription/fond_information_bottom.png); width:970px; height:19px;"></div>
    </div>
    
	<div style="clear:both;"></div>

	<div id="commande_barre_info">
        <!--<div class="item_num_com"><strong>N&deg; d'ajout</strong></div>-->
        <div class="item_date_com"><strong>Date</strong></div>
        <div class="item_paiement_com" style="width:560px;"><strong>Raison</strong></div>
        <div class="item_montant_com"><strong>Solde</strong></div>
        <div style="clear:both;"></div>
	</div>
<?php
	   //requete permettant de lister toutes les transactions que le client a eu avec son porte monnaie virtuel
	   $req_purse_monnaie=tep_db_query("Select *
									    From ".TABLE_CUSTOMERS_ARGENT."
						 			    Where customers_id=".$_SESSION['customer_id']."
										ORDER BY date_versement DESC");
		$i=0;		
		while($total_monnaie=tep_db_fetch_array($req_purse_monnaie)){
			if($i % 2==0){
				$classe=1;
			}
			else {
				$classe=2;
			}
			
			echo'	<div  class="ligne_commade_info'.$classe.'">';
			//<div class="item_num_com">'.$total_monnaie['id_versement'].'</div>
			echo'	<div class="item_date_com">'.dat($total_monnaie['date_versement']).'</div>
					<div class="item_paiement_com" style="width:560px; text-align:justify">'.$total_monnaie['raison_client'].'</div>
					<div class="item_montant_com" style="text-align:center;"><strong>';
			        echo $total_monnaie['somme'].' &euro; HT ';
					 if(taux_taxe()==1.2) echo '<br /><br />'.format_to_money($total_monnaie['somme']*1.2).' &euro; TTC';
			echo'</strong></div>
				<div style="clear:both;"></div>
				</div>';
			$i++;
		}
?> 
</div>
<?php  
}
else{ header('Location: connexion.php'); }
require("../includes/footer.php");
require("../includes/page_bottom.php"); 
?>