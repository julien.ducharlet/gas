<?php
/* Page permettant au client de pouvoir creer, modifier et supprimer les adresses associ�ss � sont compte */
   
$nom_page = "address_book";
   
require("../includes/page_top.php");

include("fonction/session.php");
include("fonction/address_book.php");

 

if(!empty($_SESSION['customer_id'])) {
	
	require("../includes/meta_head.php");
	require("../includes/header.php"); ?>

<div id="corps">
<div id="bande_rubrique_g">
        <div id="bande_image"><img src="../template/base/compte/icones/cahier.png" alt="inf_cahier" /></div>
        <div id="bande_titre">Gestion de votre carnet d'adresses</div>
        <div id="bande_sous_titre">Vous permet de cr&eacute;er, de modifier ou d'effacer une adresse.</div>
</div>
<div id="bande_rubrique_d"><a href="account.php" title="Retourner � mon compte"><img src="../template/base/compte/bouton_retour.png" alt="retour" /></a></div>
<div class="clear"></div>
<?php
	 if($_POST['ok_modif']=="ok"){
		$_POST['ok_modif']="passe";
		echo adresse_modif();
     }				 
	elseif($_POST['ok_ajout']=="ok"){
	  $_POST['ok_ajout']="passe";
	  echo adresse_ajout();			
    }
?>

<div style="float:left;">
    <div  class="bordure_haut_verte">
    	<p class="titre_info"><span>Mes adresses de livraison</span></p>
    </div>
    <div class="clear"></div>
    
    <div class="bordure_pixel_vert">
      <div id="block_address"  style="padding:20px 5px 10px 5px;">
      	<input type="hidden" id="id_client" name="id_client" value="<?php echo $_SESSION['customer_id']; ?>" />
		<?php
			 $i=1;
             $req=tep_db_query("select * from ".TABLE_ADDRESS_BOOK." where customers_id=".$_SESSION['customer_id']." order by address_book_id asc");
             while($r=tep_db_fetch_array($req)){
               echo '<div  style="background-color:#d6deb0; height:20px; margin-bottom:10px; text-align:left; padding:10px 5px 10px 10px; " id="blop'.$r['adress_book_id'].'">
			   			<div style="float:left;">
								'.$i.'<span style="margin-left:30px;">
								<strong>'.strtoupper($r['entry_address_title']).'</strong>
								- ';
								if($r['entry_gender']=="m")
								  {echo"M";}
								if($r['entry_gender']=="f")
								  {echo"Mme";}
								if($r['entry_gender']=="d")
								  {echo"Mlle";}
						
						   echo' '.$r['entry_lastname'].'
						   </span>
						</div>
						<div style="float:right;">
							<a onclick="recup_info_modif('.$r['address_book_id'].');" class="pointer" title="&eacute;diter cette adresse"><img src="../template/base/boutons/editer.png" alt="modifier" /></a>';
							$nbz=tep_db_num_rows($req);
							if($nbz>1){
							echo'&nbsp;';
							echo'<a onclick="supprimer('.$r['address_book_id'].');" class="pointer" title="supprimer cette adresse"><img src="../template/base/boutons/icone_supprimer.png" alt="supprimer" /></a>';
							}
						echo'</div>
                    </div>';
					$i++;
             }
         ?>
      </div>
      <div class="clear"></div>
	</div>       
	<div class="bordure_bas_verte"></div>
    <div id="nouvelle" style="display:none;  margin-top:10px;">
    	<?php
		$max_address=tep_db_query("select configuration_value from ".TABLE_CONFIGURATION." where configuration_id=35");
		$nb_max=tep_db_fetch_array($max_address);
		$nb_max_address=$nb_max['configuration_value'];
		
		$nb=tep_db_num_rows($req);
		if($nb<$nb_max_address){
			if(isset($_POST['nouvelle'])){
						unset($_POST);
					}
			?>
			<form name="new_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<input type="hidden" name="nouvelle"/>
            <a href="javascript:document.new_form.submit();" title="ajouter une nouvelle adresse">
              <img src="../template/base/boutons/ajouter_une_adresse.jpg" alt="bouton modifier" />
            </a>
			</form>
         <?php 
		}
		 ?>
    </div>
</div>

<?php
print_r($_POST);

if(!empty($_POST)){	
	$titre_address = format_text($_POST['titre_address']);
	$nom_address = format_text($_POST['nom_address']);
	$prenom_address = format_text($_POST['prenom_address']);
	$societe_address = format_text($_POST['societe_address']);

	$a_address = format_text($_POST['a_address']);
	$ap_address = format_text($_POST['ap_address']);
	$cp_address = format_text($_POST['cp_address']);
	
	$ville_address = format_text($_POST['ville_address']);
} else {
	$titre_address = '';
	$nom_address = '';
	$prenom_address = '';
	$societe_address = '';

	$a_address = '';
	$ap_address = '';
	$cp_address = '';
	
	$ville_address = '';
}
?>

<div  style="float:right;">
    <div  class="bordure_haut_verte">
    	<p class="titre_info"><span>Mes coordonn&eacute;es de livraison</span></p>
    </div>
    <div class="clear"></div>
    
    <div class="bordure_pixel_vert">
      <div  style="padding:20px 27px 0px 25px;">
            <form name="adresse" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                <div style="text-align:left; margin-bottom:10px;">
                		Titre ( ex : au bureau ):<br />
                         <input type="text" id="titre_address" name="titre_address" value="<?php echo $titre_address; ?>" size="55" />
                </div>
                <div style="text-align:left; margin-bottom:10px;">
                	Civilit&eacute; 
                    <input type="radio" id="mr" name="civi" value="m"  <?php if($_POST['civi']=='m') echo 'checked="checked"'; ?> />Mr
                    <input type="radio" id="mme" name="civi" value="f" <?php if($_POST['civi']=='f') echo 'checked="checked"'; ?> />Mme
                    <input type="radio" id="mlle" name="civi" value="d"  <?php if($_POST['civi']=='d') echo 'checked="checked"'; ?> />Mlle
                    <div class="clear"></div>
                 </div>
                <div style="text-align:left; margin-bottom:10px;">
                	<div style="float:left;">
                    	Nom :<br /> <input type="text" id="nom_address" name="nom_address" value="<?php echo $nom_address; ?>" />
                    </div>
                    <div style="float:right;">
                    	Prenom :<br /> <input type="text" id="prenom_address" name="prenom_address" value="<?php echo $prenom_address; ?>" />
                    </div>
                    <div class="clear"></div>
                 </div>
                 
                <div style="text-align:left; margin-bottom:10px;">
                	Societe :<br /> <input type="text" id="societe_address" name="societe_address" value="<?php echo $societe_address; ?>" size="55" />
                </div>
                <div style="text-align:left; margin-bottom:10px;">
                	Adresse :<br /> <input type="text" id="a_address" maxlength="31" name="a_address" value="<?php echo $a_address; ?>" size="55" />
                </div>
                <div style="text-align:left; margin-bottom:10px;">
                		Complement d'adresse :<br /> <input type="text" id="ap_address" maxlength="31" name="ap_address" value="<?php echo $ap_address; ?>" size="55" />
                </div>
                
                <div style="text-align:left; margin-bottom:10px;">
                    <div style="float:left;">
                    	Code postal :<br /> <input type="text" id="cp_address" name="cp_address" value="<?php echo $cp_address; ?>" />
                    </div>
                    <div style="float:right;">
                    	Ville :<br /> <input type="text" id="ville_address" name="ville_address" value="<?php echo $ville_address; ?>" />
                    </div>
                    <div class="clear"></div>
                </div>
                <div style="text-align:left; margin-bottom:10px;">
                    
                    <div style="float:left;">Pays :<br />
                    	<select id="pays" name="pays" style="max-width:400px;">
                            <option value="pays">pays</option>
                   			 <?php  
                            $req_pays=tep_db_query("Select countries_id, countries_name From ".TABLE_COUNTRIES." order by countries_name");
                            while($res=tep_db_fetch_array($req_pays)){
                   			 ?>
                            <option <?php if($_POST['pays']==$res['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res['countries_id'];?>">
									<?php echo $res['countries_name'];?>
                            </option>
                     		<?php
                              }
                     		?>
                          </select>
                    </div>
                    <div class="clear"></div>
                </div>
				
				<div style="text-align:left; margin-bottom:10px;">
                    <div style="float:left;">
                    	<input type="checkbox" id="default_address" name="default_address" <?php if($_POST['default_address']!='') echo'checked="checked"'; ?>   value="da" />D&eacute;finir comme adresse par d&eacute;faut<br>
                    </div>
                    <div class="clear"></div>
                </div>
                
                <?php
						if($_POST['ok_ajout']=='passe'){
						?>
                        <script type="text/javascript">reset_ajout();</script>
                        <script type="text/javascript">recharge();</script>
						<?php 
						}
			 	 ?>
                 
                
                <div class="clear"></div>
                	<input type="hidden" id="id_address" name="id_address" value="<?php echo $_POST['id_address']; ?>" />
                 <div id="modif" style="display:none;">
                 	<input type="hidden" id="ok_modif" name="ok_modif" value="" />
                    <a href="#" onclick="ok_modif();" title="enregistrer les modfications">
                    		<img src="../template/base/boutons/enregistrer_les_modifications.jpg" alt="Enregistrer les modifications" />
                    </a>
                 </div>
                 <div id="ajouter" style="display:block;">
                 <?php
                 $nb=tep_db_num_rows($req);
				if($nb<$nb_max_address){
				?>
                 	<input type="hidden" id="ok_ajout" name="ok_ajout" value="" />
                    <a class="pointer" onclick="ok_ajout();" title="enregistrer votre nouvelle adresse">
                    		<img src="../template/base/boutons/enregistrer_adresse.jpg" alt="Enregistrer l'adresse" />
                    </a>
                    
                 <?php
				} else echo"<span style='color:" . COULEUR_28 . ";'>Vous avez d&eacute;pass&eacute; le nombre maximum d'adresses.<br />
							Pour ajouter une adresse vous devez effacer une des adresses d&eacute;j&agrave; existante.</span>";
				?>
                 </div>
                 
                 <?php
						if($_POST['ok_modif']=='passe') {
						?>
						<script type="text/javascript">affiche_modif();</script>
                        <script type="text/javascript">recharge();</script>
						<?php 
						}?>
                 
            </form>
      </div>
      <div class="clear"></div>
    </div>       
    <div class="bordure_bas_verte"></div>
</div>
<div class="clear"></div>
</div>
<?php 

}
else
{ header('Location: login.php'); }
require("../includes/footer.php");
require("../includes/page_bottom.php");
?>

