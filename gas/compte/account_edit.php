<?php
/* Page permattant l'edition des infos (generales et mot de passe) du compte client */

$nom_page = "account_edit";
   
require("../includes/page_top.php");

include("fonction/session.php");

if (!empty($_SESSION['customer_id'])) {
	
	require("../includes/meta_head.php");
	require("../includes/header.php");

	if (empty($_POST['old_mdp'])) { $_POST['old_mdp'] = ''; } 
	if (empty($_POST['new_mdp'])) { $_POST['new_mdp'] = ''; }
	if (empty($_POST['conf_new_mdp'])) { $_POST['conf_new_mdp'] = ''; }
	if (empty($_POST['sms'])) { $_POST['sms'] = ''; }
		
	//fonction permettant d'afficher une div avec un message d'erreur lorsqu'un formulaire est mal renseign�
	function retour_erreur($message){
		return'<br><br><div class="box-erreur">'.$message.'</div>';
	}

	//fonction permettant d'afficher une div avec un message de validation lorsqu'un formulaire est bien renseign�
	function retour_validation($message){
		return'<br><br><div class="box-valide">'.$message.'</div>';
	}

	//fonction permettant de verifier le formulaire de nouveau mot de passe et de le prendre en compte 
	function new_password($old,$new,$new_confirm){
		$ret='';
		$check=true;
		$res = tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id']."");
		$r=tep_db_fetch_array($res);
		$cp=$r['customers_password'];
		if(!tep_validate_password($old, $cp)){
			 $ret.='erreur mot de passe<br />';
			 $check=false;
		}
		if($new==''){
			  $ret.="Vous devez indiquer un nouveau mot de passe <br />";
			  $check=false;
		}
		if($new_confirm==''){
			  $ret.="Vous devez confirmer votre nouveau mot de passe <br />";
			  $check=false;
		}
		if($new!=$new_confirm){
			  $ret.="erreur lors de la confirmation du mot de passe <br />";
			  $check=false;
		}
		if($check==true){
			$new_mdp=tep_encrypt_password($new);
			$exe_req=tep_db_query("update ".TABLE_CUSTOMERS." set customers_password ='".$new_mdp."' where customers_id=".$_SESSION['customer_id']." ");
			$ret.="Votre mot de passe a &eacute;t&eacute; modifi&eacute;";
			return retour_validation($ret);
		}
		return retour_erreur($ret);
	}

	//fonction permettant de faire la validation des informations generales du compte client particulier
	function info_modif(){
		$ret='';
		$formule_polie="Vous devez indiquer votre ";
		$check=true;
		
		if ($_POST['nom_cli']=='') {
			$ret.=$formule_polie."nom<br />";
			$check=false;
		}
		if ($_POST['prenom_cli']=='') {
			$ret.=$formule_polie."prenom<br />";
			$check=false;
		}
		
		if ($_POST['tel_portable_cli']=="" || !preg_match('<^[0-9]*$>', format_tel($_POST['tel_portable_cli']))) {
			$ret.=$formule_polie."num�ro de t�l�phone mobile (valide)<br />";
			$check=false;
		}
		if ($_POST['tel_fixe_cli']!="" && !preg_match('<^[0-9]*$>', format_tel($_POST['tel_fixe_cli']))) {
			$ret.=$formule_polie."num&eacute;ro de t�l�phone fixe (valide)<br />";
			$check=false;
		}
		
		$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
		if(!preg_match($verif,$_POST['mail_cli'])){
			$ret.=$formule_polie."adresse e-mail (valide)<br />";
			$check=false;
		} else {
			$res=tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_email_address='".addslashes($_POST['mail_cli'])."' AND customers_id=".$_SESSION['customer_id']."");
			$r=tep_db_fetch_array($res);
			if ($_POST['mail_cli']!=$r['customers_email_address']) {
				$ress=tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_email_address='".addslashes($_POST['mail_cli'])."'");
				$rs=tep_db_fetch_array($ress);
				if ($_POST['mail_cli']==$rs['customers_email_address']) {
					$ret.="Cette adresse e-mail est d&eacute;j&agrave; utilis&eacute;e<br />";
					$check=false;
				}
			}
		}
		if ($_POST['news']=='') { $newsletter=0; } else { $newsletter=1; }
		
		$mail_cli = strtolower(format_clients($_POST['mail_cli']));
		$nom_cli = strtoupper(format_inscription($_POST['nom_cli']));
		$prenom_cli = ucwords(strtolower(format_inscription($_POST['prenom_cli'])));		
		$tel_portable_cli = format_tel($_POST['tel_portable_cli']);
		$tel_fixe_cli = format_tel($_POST['tel_fixe_cli']);

		
		if($check==true){	
			$modif=tep_db_query("	UPDATE ".TABLE_CUSTOMERS." SET customers_gender='".$_POST['civi']."',
										 customers_firstname='".addslashes($prenom_cli)."',
										 customers_lastname='".addslashes($nom_cli)."',
										 customers_dob='".$_POST['annee']."-".$_POST['mois']."-".$_POST['jour']."',
										 customers_email_address='".addslashes($mail_cli)."',
										 customers_country='".$_POST['pays']."',
										 customers_telephone='".addslashes($tel_portable_cli)."',
										 customers_fixe='".addslashes($tel_fixe_cli)."',
										 customers_newsletter='".$newsletter."'
									WHERE customers_id=".$_SESSION['customer_id']."");
			$ret="Les informations ont &eacute;t&eacute; enregistr&eacute;es";
			return retour_validation($ret);
		}
			return retour_erreur($ret);
	}

	//fonction permettant de faire la validation des informations generales du compte client professionnel
	function info_modif_pro(){
		$ret='';
		$formule_polie="Vous devez indiquer votre ";
		$check=true;
		
		if ($_POST['nom_cli']=='') {
			$ret.=$formule_polie."nom<br>";
			$check=false;
		}
		if ($_POST['prenom_cli']=='') {
			$ret.=$formule_polie."prenom<br>";
			$check=false;
		}
		if ($_POST['societe_cli']=='') {
			$ret.=$formule_polie." le nom de votre soci&eacute;t&eacute; <br />";
			$check=false;
		}
		if ($_POST['tel_portable_cli']=="" || !preg_match('<^[0-9]*$>', format_tel($_POST['tel_portable_cli']))) {
			$ret.=$formule_polie."num�ro de t�l�phone mobile (valide)<br />";
			$check=false;
		}
		if ($_POST['tel_fixe_cli']!="" && !preg_match('<^[0-9]*$>', format_tel($_POST['tel_fixe_cli']))) {
			$ret.=$formule_polie."num&eacute;ro de t�l�phone fixe (valide)<br />";
			$check=false;
		}
		if ($_POST['fax_cli']!="" && !preg_match('<^[0-9]*$>', format_tel($_POST['fax_cli']))) {
			$ret.=$formule_polie."num�ro de t�l�phone mobile (valide)<br />";
			$check=false;
		}	
		$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
		if(!preg_match($verif,$_POST['mail_cli'])){
			$ret.=$formule_polie."adresse e-mail (valide)<br />";
			$check=false;
		} else {
			$res=tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_email_address='".addslashes($_POST['mail_cli'])."' AND customers_id=".$_SESSION['customer_id']."");
			$r=tep_db_fetch_array($res);
			if ($_POST['mail_cli']!=$r['customers_email_address']) {
				$ress=tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_email_address='".addslashes($_POST['mail_cli'])."'");
				$rs=tep_db_fetch_array($ress);
				if ($_POST['mail_cli']==$rs['customers_email_address']) {
					$ret.="Cette adresse e-mail est d&eacute;j&agrave; utilis&eacute;e<br />";
					$check=false;
				}
			}
		}
		if ($_POST['news']=='') { $newsletter=0; } else { $newsletter=1; }
		  
		if($check==true){	
			
			$mail_cli = strtolower(format_clients($_POST['mail_cli']));
			$nom_cli = strtoupper(format_inscription($_POST['nom_cli']));
			$prenom_cli = ucwords(strtolower(format_inscription($_POST['prenom_cli'])));		
			$societe_cli = strtoupper(format_inscription($_POST['societe_cli']));
			$siret_cli = strtoupper(format_inscription($_POST['siret_cli']));
			$tva_cli = strtoupper(format_inscription($_POST['tva_cli']));
			$tel_portable_cli = format_tel($_POST['tel_portable_cli']);
			$tel_fixe_cli = format_tel($_POST['tel_fixe_cli']);
			$fax_cli = format_tel($_POST['fax_cli']);
			
			$modif=tep_db_query("	UPDATE ".TABLE_CUSTOMERS." SET customers_gender='".$_POST['civi']."',
										 customers_firstname='".addslashes($prenom_cli)."',
										 customers_lastname='".addslashes($nom_cli)."',
										 customers_email_address='".addslashes($mail_cli)."',
										 customers_country='".$_POST['pays']."',
										 customers_telephone='".addslashes($tel_portable_cli)."',
										 customers_fixe='".addslashes($tel_fixe_cli)."',
										 customers_company='".addslashes($societe_cli)."',
										 customers_fax='".addslashes($fax_cli)."',
										 customers_newsletter='".$newsletter."'
									WHERE customers_id=".$_SESSION['customer_id']."");
			$ret.="Vos modifications ont bien �t� enregistr�es";
			return retour_validation($ret);
		}
		return retour_erreur($ret);
	}
	?>

	<div id="corps">
		<div id="bande_rubrique_g">
			<div id="bande_image"><img src="../template/base/compte/icones/bonhomme.png" alt="inf_commande" /></div>
			<div id="bande_titre">Informations de mon compte</div>
			<div id="bande_sous_titre">&Eacute;diter les diff&eacute;rentes infomations de votre compte</div>
		</div>
		<div id="bande_rubrique_d"><a href="account.php" title="Retourner � mon compte"><img src="../template/base/compte/bouton_retour.png" alt="retour" /></a></div>
		<div class="clear"></div>
	<?php
	if (isset($_POST['formu'])) {
		echo $res=new_password($_POST['old_mdp'],$_POST['new_mdp'],$_POST['conf_new_mdp']);
	}
	?>
	
	
<? 
//if ($_SESSION['customer_id'] == 3) { 
//print_r($_POST).'<br><br>'; 
//print_r($_SESSION).'<br><br>'; 
?>
	
	<!-- block text -->
	<? // DEBUT ZONE CHANGEMENT MOT DE PASSE ?>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" name="mdp" >
		<div  style="width:100%; margin-top:10px; margin-right:0px;">
			<div class="titre_box-contenu">Changer votre mot de passe</div>
			
			<div class="box-contenu">
				<div class="conteneur_desc_item">
					<div class="item_i">Mot de passe actuel :</div>
					<div class="item_i">Nouveau mot de passe :</div>
					<div class="item_i">Confirmer le mot de passe :</div>
				</div>
				<div class="conteneur_composant_item">
					<div class="item_i">
						<input type="password" name="old_mdp" size="25" value="<?php echo $_POST['old_mdp']; ?>"  />
					</div>
					<div class="item_i">
						<input type="password" name="new_mdp"  size="25" value="<?php echo $_POST['new_mdp']; ?>"  />
					</div>
					<div class="item_i">
						<input type="password" name="conf_new_mdp" size="25" value="<?php echo $_POST['conf_new_mdp']; ?>"  />
					</div>
				</div>
				<?php 
				$post_formu = !empty($_POST['formu']) ? $_POST['formu'] : '';
				if ($post_formu=='ok') { ?>
					<script type="text/javascript">affiche(1);</script>
				<?php } ?>
				<input type="hidden" name="formu" value="ok" /> 
				<div class="clear"></div>
				<div style="margin-top:10px;">
					<a class="button" href="javascript:document.mdp.submit();" title="Valider la modification du mot de passe">
						Changer le mot de passe
					</a>
				</div>
			</div>  
			
		</div>
	</form>
	<? // FIN ZONE CHANGEMENT MOT DE PASSE ?>
	<!-- fin block text -->	
	
	<!-- block text -->
	<? // DEBUT ZONE COORDONNEES ?>
	<?php	
	if(isset($_POST['info'])) {
		if($_SESSION['customers_type']==2 || $_SESSION['customers_type']==3 || $_SESSION['customers_type']==4) { echo $res=info_modif_pro(); } else { echo $res=info_modif(); }        
	}
	
	$res=tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id']."");
	$r=tep_db_fetch_array($res);
	//pour la date de naissance
	$d=$r['customers_dob'];
	//echo '<br><br>' . $r['customers_dob'] . '<br><br>';
	list($an, $mois, $j) = split('-', $d);
	list($jour,$h) = split(' ', $j);
	?>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>#error" name="info">	
		<div  style="width:100%; margin-top:10px; margin-right:0px;">
			<div class="titre_box-contenu">Vos coordonn�es</div>
			
			<div class="box-contenu">
				<div class="conteneur_desc_item">
					<div class="item_i">Civilit� :</div>
					<div class="item_i">Nom :</div>
					<div class="item_i">Pr�nom :</div>
					<?php 
					if ($_SESSION['customers_type']==1) {
					?>
						<div class="item_i">Date de Naissance :</div>  
					<?php 
					}
					?>
					<div class="item_i">Adresse email :</div>  
					<div class="item_i">Pays :</div>  
					<div class="item_i">T�l�phone mobile :</div>  
					<div class="item_i">T�l�phone fixe :</div>  
				</div>
				<div class="conteneur_composant_item">
					<div class="item_i">						
						<input type="radio" name="civi" value="m" id="H" <?php if (isset($_POST['info'])) { if ($_POST['civi']=='m') { echo 'checked="checked"'; } } else if ($r['customers_gender']=='m') { echo 'checked="checked"'; } ?> />
						Mr&nbsp;&nbsp;&nbsp;
						<input type="radio" name="civi" value="f" id="MME" <?php if (isset($_POST['info'])) { if ($_POST['civi']=='f') { echo 'checked="checked"'; } } else if ($r['customers_gender']=='f') { echo 'checked="checked"'; } ?> />Mme&nbsp;&nbsp;&nbsp;
						<input type="radio" name="civi" value="fe" id="MLLE" <?php if (isset($_POST['info'])) { if ($_POST['civi']=='d') { echo 'checked="checked"';} } else if ($r['customers_gender']=='d') { echo 'checked="checked"'; } ?> />
						Mlle
					</div>
					<div class="item_i">
						<input type="text" id="nom" name="nom_cli" maxlength="32" size="25" value="<?php if (isset($_POST['info'])) { echo stripslashes($nom_cli); } else { echo stripslashes($r['customers_lastname']); } ?>" style="text-transform:Uppercase;" />
					</div>
					<div class="item_i">
						<input type="text" id="prenom" name="prenom_cli" maxlength="32" size="25" value="<?php if (isset($_POST['info'])) { echo stripslashes($prenom_cli); } else { echo stripslashes($r['customers_firstname']); } ?>" style="text-transform:capitalize;" />
					</div>
					<?php 
					if ($_SESSION['customers_type']==1) {
					?>
						<div class="item_i">
							<select id="jours" name="jour">
							<?php  
							for ($i=1 ; $i<32 ; $i++) {	
							?>
								<option 
								<?php 
								if (isset($_POST['info'])) {
									if ($_POST['jour']==$i) {
										echo 'selected="selected"';
									}
								} else if ($jour==$i) {
									echo 'selected="selected"';
								} 
								?> 
								value="<?php echo $i;?>"><?php echo $i;?>
								</option>
							<?php
							}
							?>
							</select>

							<select id="mois" name="mois">
								<?php  
									$tab=array('janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre');
									for ($i=0 ; $i<count($tab) ; $i++) {	
								?>
									<option 
										<?php 
										if (isset($_POST['info'])) {
											if ($_POST['mois']==$i+1) { 
												echo 'selected="selected"';
											}
										} else if ($mois==$i+1) {
											echo 'selected="selected"'; 
										}
										?> 
										value="<?php echo $i+1;?>"><?php echo $tab[$i]; ?>
									</option>
								<?php
								}
								?>
							</select> 
													 
							<select id="annees" name="annee" > 
								<?php  
								$age_min= date("Y") -14;
								$age_max= date("Y") -90;
								for ($i=$age_min ;$i>$age_max;$i--) {
								?>
									<option 
									<?php 
									if (isset($_POST['info'])) { 
										if($_POST['annee']==$i) { echo 'selected="selected"'; } 
									} else if ($an==$i) { 
										echo 'selected="selected"'; 
									} ?> 
									value="<?php echo $i;?>"><?php echo $i;?>
									</option>
								<?php
								}
								?>
							</select>
						</div>
					<?php 
					}
					?>
					<div class="item_i">
						<input type="text" name="mail_cli" size="25" id="mail" style="text-transform:Lowercase;" value="<?php if (isset($_POST['info'])) { echo $mail_cli; } else { echo $r['customers_email_address']; } ?>" />
					</div>
					<div class="item_i">
						
						<select id="pays" name="pays" style="width:208px;">  
							<?php  
							// S�lection des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
							$req_pays_base=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND
							countries_id IN (1,247,272,274,273,275,271)
							");
							while($res_req_pays_base=tep_db_fetch_array($req_pays_base)){
							?>
								<option <?php if(isset($_POST['info'])) { if($_POST['pays']==$res_req_pays_base['countries_id']) echo'selected="selected"'; } else if ($r['customers_country']==$res_req_pays_base['countries_id']) {  echo 'selected="selected"'; } ?> value="<?php echo $res_req_pays_base['countries_id'];?>">
									<?php echo $res_req_pays_base['countries_name'];?>
								</option>
							<?php } ?>
							
							<optgroup label=" -------------------"> </optgroup>
							
							<?php 
							// retrait des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
							$req_pays=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND countries_id NOT IN (1,247,272,274,273,275,271) ORDER BY countries_name");
							while($res=tep_db_fetch_array($req_pays)){
							?>
								<option <?php if(isset($_POST['info'])) { if($_POST['pays']==$res['countries_id']) echo'selected="selected"'; } else if ($r['customers_country']==$res['countries_id']) {  echo 'selected="selected"'; } ?> value="<?php echo $res['countries_id'];?>">
									<?php echo $res['countries_name'];?>
								</option>
							<?php } ?>
						</select>
						
					</div>
					<div class="item_i">
						<input type="text" name="tel_portable_cli" maxlength="16" size="25" id="telephone_p" value="<?php if (isset($_POST['info'])) { echo $tel_portable_cli; } else { echo $r['customers_telephone']; } ?>"  />
					</div>
					<div class="item_i">
						<input type="text" name="tel_fixe_cli" maxlength="16" size="25" id="telephone_f" value="<?php if (isset($_POST['info'])) { echo $tel_fixe_cli; } else { echo $r['customers_fixe']; } ?>" />
					</div>
				</div>
				<div class="clear"></div>				
			</div>			
		</div>
	
	<? // FIN ZONE COORDONNEES ?>
	<!-- fin block text -->	
	
	<?php 
	if ($_SESSION['customers_type']!=1) {
	?>
		
		<!-- block text -->
		<? // DEBUT ZONE INFORMATIONS PROFESSIONNELLES ?>
		
			<div  style="width:100%; margin-top:10px; margin-right:0px;">
				<div class="titre_box-contenu">Information professionnelles</div>
				
				<div class="box-contenu">
					<div class="conteneur_desc_item">
						<div class="item_i">
							<?php if ($_SESSION['customers_type']==2 || $_SESSION['customers_type']==3) { 
								echo 'Nom de soci�t� :';
							} else if ($_SESSION['customers_type']==4 && !empty($r['customers_company_type']) ) {
								echo 'Nom de l\'administration :';
							} else {
								echo 'Nom de l\'association :';
							}
							?>							
						</div>
						
						<?php
						if ($_SESSION['customers_type']==4 && !empty($r['customers_company_type']) ) {
							echo '<div class="item_i">Type d\'administration :</div>';
						} 
						?>		
						
						<div class="item_i">
							<?php if ($_SESSION['customers_type']==2 || $_SESSION['customers_type']==3) { 
								echo 'Num�ro de siret :';
							} else if ($_SESSION['customers_type']==4 && !empty($r['customers_company_type']) ) {
								echo 'Num�ro de siret :';
							} else {
								echo 'Num�ro RNA (W) :';
							}
							?>								
						</div>
						<?php
						if ($_SESSION['customers_type']==2 || $_SESSION['customers_type']==3) {
							echo '<div class="item_i">Num�ro de TVA :</div>';
						} 
						?>
						<div class="item_i">
							Num�ro de fax :
						</div>
					</div>
					<div class="conteneur_composant_item">						
						<div class="item_i">
							<input type="text" name="societe_cli" size="25" value="<?php if (isset($_POST['info'])) { echo $societe_cli; } else { echo $r['customers_company']; } ?>" style="text-transform:Uppercase;" />
						</div>
						<?php
						if ($_SESSION['customers_type']==4 && !empty($r['customers_company_type']) ) {
							echo '<div class="item_i">
							<input type="text" name="customers_company_type" size="25"  value=" '. $r['customers_company_type'] .'" readonly="readonly" disabled  />
							</div>';
						} 
						?>	
						<div class="item_i">
							<input type="text" name="siret_cli" size="25"  value="<?php if (isset($_POST['info'])) { echo $siret_cli; } else { echo $r['customers_siret']; } ?>" />
						</div>					
						<?php
						if ($_SESSION['customers_type']==2 || $_SESSION['customers_type']==3)  {
							echo '<div class="item_i">
							<input type="text" name="tva_cli" size="25"  value="'. $r['customers_tva'] .'"  style="text-transform:Uppercase;" />
							</div>';
						} 
						?>
						<div class="item_i">
							<input type="text" name="fax_cli" size="25" value="<?php if (isset($_POST['info'])) { echo $fax_cli; } else { echo $r['customers_fax']; } ?>"/>
						</div>	
					</div>
					<div class="clear"></div>
				</div>  
				
			</div>
		
		<? // FIN ZONE INFORMATIONS PROFESSIONNELLES ?>
		<!-- fin block text -->			
	<?php 
	}
	?>
	<!-- block text -->
	<? // DEBUT ZONE NEWSLETTER ?>
	<div style="width:100%; margin-top:20px; margin-bottom:20px; margin-right:0px; float:right;">
		<div class="titre_box-contenu">Notre newsletter</div>
		
		<div class="box-contenu">
		
			<div>
				<div style="width:500px; padding:10px; float:left; text-align:center;">
					<div>Souhaitez vous recevoir notre lettre d'information : 
						<input type="checkbox" name="news" value="news" <?php if (isset($_POST['info'])) { if ($_POST['news']=='news') echo 'checked="checked"'; } else if ($r['customers_newsletter']==1) { echo 'checked="checked"'; } ?> />
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div style="padding:0px 10px 10px 10px; font-size:10px; text-align:left;" >
				<strong>Nous n'envoyons pas plus d'un email par semaine.</strong><br />
				Soucieux de la protection de votre vie priv�e, nous traitons toutes les informations vous concernant avec la plus stricte confidentialit� et nous ne revendons aucune de ces informations � d'autres soci�t�s.
			</div>
		</div>
		<div class="clear"></div>		
	</div>
	<? // FIN ZONE NEWSLETTER ?>
	<!-- fin block text-->
	
	<div style="margin-top:20px;">
		<input type="hidden" name="info" value="ok" /> 
		<a class="button" href="javascript:document.info.submit();" title="Valider la modfication des informations">
			Valider les modifications
		</a>
	</div>
	
	</form>
<?php 
// if ($_SESSION['customer_id'] == 3) { 
//} 
?>
	
</div>
<?php 
} else {
	header('Location: connexion.php');
}
require("../includes/footer.php");
require("../includes/page_bottom.php");
?>

