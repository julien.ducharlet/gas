<?php
/* Page permettant � un utilisateur de se creer un compte BASE */

$nom_page = "inscription-base";

require("../includes/page_top.php");
require("../includes/meta_head.php");
include("fonction/session.php");
include("fonction/inscription-base.php");
require("../includes/header.php");
?>

<div id="corps">
	<div id="bande_rubrique_g">
		<div id="bande_image">
			<img src="../template/base/compte/icones/bonhomme.png" alt="info commande" />
		</div>
		<div id="bande_titre">Inscription pour les Revendeurs</div>
		<div id="bande_sous_titre">
			Cr&eacute;er votre compte 
		</div>
	</div>
	<div id="bande_rubrique_d">
		<a href="../index.php" title="Retour � l'accueil"><img src="../template/base/boutons/bouton_retour-accueil.png" alt="Retour � l'accueil"/></a>
	</div>
	<div style="clear:both;"></div>
	<div style="clear:both;"></div>

	<?php
	if (empty($_POST['civi'])) { $_POST['civi'] = ''; } 
	if (empty($_POST['mail_cli'])) { $_POST['mail_cli'] = ''; }  
	if (empty($_POST['mdp'])) { $_POST['mdp'] = ''; }   
	if (empty($_POST['jour'])) { $_POST['jour'] = ''; } 
	if (empty($_POST['mois'])) { $_POST['mois'] = ''; } 
	if (empty($_POST['annee'])) { $_POST['annee'] = ''; }  
	if (empty($_POST['societe_cli'])) { $_POST['societe_cli'] = ''; }  
	if (empty($_POST['siret_cli'])) { $_POST['siret_cli'] = ''; }  
	if (empty($_POST['tva_cli'])) { $_POST['tva_cli'] = ''; }  
	if (empty($_POST['pays'])) { $_POST['pays'] = ''; } 
	if (empty($_POST['news'])) { $_POST['news'] = ''; }  
	if (empty($_POST['rgpd'])) { $_POST['rgpd'] = ''; } 

	if (isset($_POST['valid_form'])){
		
		if ($_POST['valid_form']=="ok") {
			$ret='';
			//echo verif_pro();
			echo controle_formulaire();
		}
		
		$customers_company_type_libre = format_text(strtoupper($_POST['customers_company_type_libre']));
		$customers_company_type = format_text($_POST['customers_company_type']);
		$nom_cli = format_text(strtoupper($_POST['nom_cli']));
		$prenom_cli = format_clients(ucwords(strtolower($_POST['prenom_cli'])));		
		$societe_cli = format_text(strtoupper($_POST['societe_cli']));
		$siret_cli = format_text(strtoupper($_POST['siret_cli']));
		$tva_cli = format_text(strtoupper($_POST['tva_cli']));		
		$adresse_cli_3 = format_text(ucwords(strtolower($_POST['adresse_cli_3'])));
		$complement_adresse_cli = format_text(ucwords(strtolower($_POST['complement_adresse_cli'])));
		$adresse_cli = format_text(ucwords(strtolower($_POST['adresse_cli'])));
		$adresse_cli_4 = format_text(ucwords(strtolower($_POST['adresse_cli_4'])));		
		$cp_cli = format_text(strtoupper($_POST['cp_cli']));
		$ville_cli = format_text(strtoupper($_POST['ville_cli']));		
		$tel_portable_cli = format_tel($_POST['tel_portable_cli']);
		$tel_fixe_cli = format_tel($_POST['tel_fixe_cli']);
		$fax_cli = format_tel($_POST['fax_cli']);
	} else {
		$customers_company_type = '';
		$customers_company_type_libre = '';		
		$nom_cli = '';
		$prenom_cli = '';
		$societe_cli = '';
		$siret_cli = '';		
		$tva_cli = '';
		$adresse_cli_3 = '';
		$complement_adresse_cli = '';		
		$adresse_cli = '';
		$adresse_cli_4 = '';
		$cp_cli = '';
		$ville_cli = '';
		$tel_portable_cli = '';
		$tel_fixe_cli = '';
		$fax_cli = '';
	}
	?>

	<div style="float:right; margin:0 210px 5px 0;"><span class="obligatoire">* les informations obligatoires</span></div>
	<div style="clear:both;"></div>

	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" name="formu" autocomplete="off" >
		<?php $_GET['panier'] = !empty($_GET['panier']) ? $_GET['panier'] : ''; ?>
		<input type="hidden" name="panier" value="<?php echo $_GET['panier']; ?>" />
		
		<!-- block text -->
		<? // DEBUT ZONE INFOS CONNEXION ?>
		<div  style="width:100%; margin-top:10px; margin-right:0px; float:right;">
			<div class="titre_box-contenu">Vos informations de connexion</div>
			
			<div class="box-contenu">
				<div class="conteneur_desc_item">
					<div class="item_i">Adresse e-mail :</div>
					<div class="item_i">Mot de passe :</div>
				</div>
				<div class="conteneur_composant_item">
					<div class="item_i"><input type="text" name="mail_cli" size="25" id="mail" value="<?php echo $_POST['mail_cli']; ?>" style="text-transform:Lowercase;" autocomplete="address-level4" /> <span class="obligatoire">*</span></div>
					<div class="item_i"><input type="password" name="mdp" id="mdp" size="25" value="<?php echo $_POST['mdp']; ?>"  /> <span class="obligatoire">*</span></div>
				</div>				
				<div class="clear"></div>
			</div>         
		</div>
		<? // FIN ZONE INFOS CONNEXION ?>
		<!-- fin block text -->	
		
		<!-- block text -->
		<div style="width:100%; float:left; margin-top:20px; margin-right:0px;">
				
			<div class="titre_box-contenu">Les informations du responsable</div>

			<div class="box-contenu">

					<div class="conteneur_desc_item">
						<div class="item_i">Civilit� du responsable:</div>
						<div class="item_i">Nom du responsable:</div>
						<div class="item_i">Pr�nom du responsable:</div>
						<div class="item_i">Date de naissance :</div>
					</div>
					
					<div class="conteneur_composant_item">
						<div class="item_i">
							&nbsp;&nbsp;&nbsp;<input type="radio" name="civi" value="m" id="H" <?php if($_POST['civi']=='m') echo 'checked="checked"'; ?> /><span style="color:#000;">M</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="civi" value="f" id="MME" <?php if($_POST['civi']=='f') echo 'checked="checked"'; ?> /><span style="color:#000;">Mme</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="civi" value="d" id="MLLE" <?php if($_POST['civi']=='d') echo 'checked="checked"'; ?> /><span style="color:#000;">Mlle</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="obligatoire">*</span>
						</div>
						<div class="item_i">
							<input type="text" id="nom" name="nom_cli" size="25" value="<?php echo $nom_cli; ?>" style="text-transform:Uppercase;" />&nbsp;<span class="obligatoire">*</span>
						</div>

						<div class="item_i">
							<input type="text" id="prenom" name="prenom_cli" size="25" value="<?php echo $prenom_cli; ?>" style="text-transform:capitalize;" />&nbsp;<span class="obligatoire">*</span>
						</div>
								
						<div class="item_i">
							<select id="jours" name="jour">
								<option  value="jour">jour</option>
								<?php for ($i=1;$i<32;$i++) { ?>
									<option <?php if($_POST['jour']==$i) echo'selected="selected"'; ?> value="<?php echo $i;?>"><?php echo $i;?></option>
								<?php } ?>
							</select>

							<select id="mois" name="mois" >
								<option value="mois">mois</option>
								<?php  
								$tab=array('janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre');
								for($i=0;$i<count($tab);$i++){	
								?>
									<option <?php if($_POST['mois']==$i+1) echo'selected="selected"'; ?> value="<?php echo $i+1;?>"><?php echo $tab[$i];?></option>
								<?php } ?>
							</select>
							
							<select id="annees" name="annee" >
								<option value="annee">ann&eacute;e</option>
								<?php  
								$age_min= date("Y") -14;
								$age_max= date("Y") -80;
								for($i=$age_min ;$i>$age_max;$i--){
								?>
									<option <?php if($_POST['annee']==$i) echo'selected="selected"'; ?> value="<?php echo $i;?>"><?php echo $i;?></option>
								<?php } ?>
							</select>&nbsp;<span class="obligatoire">*</span>
						</div>
					</div>
				<div class="clear"></div>
			</div>
			
		</div>
		<!-- fin block text-->
		
		<!-- block text -->
		<div style=" width:100%; float:left; margin-top:20px; margin-right:0px;">
			<div class="titre_box-contenu">Les informations professionnelles de facturation</div>
			<div class="box-contenu">
		        
				<div class="conteneur_desc_item">
					<div class="item_i">Statut (EI, SARL, SAS, SA, etc�) :</div>
					<div class="item_i">Activit� (ou) Administration  :</div>
					<div class="item_i">Nom de la soci�t� :</div>
					<div class="item_i">Num�ro Siret : </div>
					<div class="item_i">Num�ro TVA intracom :</div>
					
					<div class="item_i">Appt - Etage - Couloir - Esc. :</div>
					<div class="item_i">Entr�e - B�t. - Imm. - R�s. :</div>
					<div class="item_i">N� + Voie (rue, avenue, bvd, ...) :</div>
					<div class="item_i">Boite Postale - Lieu Dit :</div>					
					<div class="item_i">Code postal :</div>
					<div class="item_i">Ville :</div>
					<div class="item_i">Pays :</div>
					<div class="item_i">T�l�phone portable :</div>
					<div class="item_i">T�l�phone fixe :</div>
					<div class="item_i">Num�ro de Fax :</div>
				</div>
				
				<div class="conteneur_composant_item">
					
					<div class="item_i"><input type="text" name="customers_company_type_libre" maxlength="32" size="25" id="customers_company_type_libre" value="<?php echo $customers_company_type_libre;?>" style="text-transform:Uppercase;" />&nbsp;<span class="obligatoire">*</span></div>
					
					<div class="item_i">
					<select name="customers_company_type" style="width:208px;">
						<option value="">S�lectionnez</option>
						<option value="" style="background-color:#999999; font-weight: bold;">PROFESSIONNEL</option>
										
						<option value="Militaire" <? if ($customers_company_type=='Militaire') echo 'selected';?>>
							&nbsp;&nbsp;&nbsp;Militaire
						</option>
						<option value="Gendarme" <? if ($customers_company_type=='Gendarme') echo 'selected';?>>
							&nbsp;&nbsp;&nbsp;Gendarme
						</option>
						<option value="Policier (PN, CRS, Douanier)" <? if ($customers_company_type=='Policier (PN, CRS, Douanier)') echo 'selected';?>>
							&nbsp;&nbsp;&nbsp;Policier (PN, CRS, Douanier)
						</option>
						<option value="Policier (PM-ASVP)" <? if ($customers_company_type=='Policier (PM-ASVP)') echo 'selected';?>>
							&nbsp;&nbsp;&nbsp;Policier (PM-ASVP)
						</option>
						<option value="Agent P�nitentiaire" <? if ($customers_company_type=='Agent P�nitentiaire') echo 'selected';?>>
							&nbsp;&nbsp;&nbsp;Agent P�nitentiaire
						</option>
						<option value="Agent de S�curit�" <? if ($customers_company_type=='Agent de S�curit�') echo 'selected';?>>
							&nbsp;&nbsp;&nbsp;Agent de S�curit�
						</option>
						<option value="Sapeur Pompier" <? if ($customers_company_type=='Sapeur Pompier') echo 'selected';?>>
							&nbsp;&nbsp;&nbsp;Sapeur Pompier
						</option>
						<option value="Autre profession" <? if ($customers_company_type=='Autre profession') echo 'selected';?>>
							&nbsp;&nbsp;&nbsp;Autre profession
						</option>
						
						<option value="" style="background-color:#999999; font-weight: bold;">ADMINISTRATION</option>
						
						<option value="Mairie" <? if ($customers_company_type=='Mairie') echo 'selected';?>>
							&nbsp;&nbsp;Mairie
						</option>
						<option value="Communaut� de Communes" <? if ($customers_company_type=='Communaut� de Communes') echo 'selected';?>>
							&nbsp;&nbsp;Communaut� de Communes
						</option>
						<option value="Communaut� d�Agglom�ration" <? if ($customers_company_type=='Communaut� d�Agglom�ration') echo 'selected';?>>
							&nbsp;&nbsp;Communaut� d�Agglom�ration
						</option>
						<option value="Conseil G�n�ral" <? if ($customers_company_type=='Conseil G�n�ral') echo 'selected';?>>
							&nbsp;&nbsp;Conseil G�n�ral
						</option>
						<option value="Conseil R�gional" <? if ($customers_company_type=='Conseil R�gional') echo 'selected';?>>
							&nbsp;&nbsp;Conseil R�gional
						</option>
						<option value="Minist�re" <? if ($customers_company_type=='Minist�re') echo 'selected';?>>
							&nbsp;&nbsp;Minist�re
						</option>
						<option value="Pr�fecture / Sous-Pr�fecture" <? if ($customers_company_type=='Pr�fecture / Sous-Pr�fecture') echo 'selected';?>>
							&nbsp;&nbsp;Pr�fecture / Sous-Pr�fecture
						</option>
						<option value="Autre administration" <? if ($customers_company_type=='Autre administration') echo 'selected';?>>
							&nbsp;&nbsp;Autre administration
						</option>
					</select>
					&nbsp;<span class="obligatoire">*</span></div>
					
					<div class="item_i"><input type="text" name="societe_cli" maxlength="32" size="25" id="societe" value="<?php echo $societe_cli;?>" style="text-transform:Uppercase;" />&nbsp;<span class="obligatoire">*</span></div>
					
					<div class="item_i"><input type="text" size="25" maxlength="14" name="siret_cli" id="siret" value="<?php echo $siret_cli;?>" />&nbsp;<span class="obligatoire">*</span></div>
					
					<div class="item_i"><input type="text" maxlength="14" size="25" name="tva_cli" id="tva" value="<?php echo $tva_cli;?>"  style="text-transform:Uppercase;" />&nbsp;&nbsp;&nbsp;</div>
					
					
					<!-- Appt - Etage - Couloir - Esc. : -->
					<div class="item_i">
						<input type="text" name="adresse_cli_3" maxlength="35" size="25" id="adresse_cli_3" value="<?php echo $adresse_cli_3; ?>" style="text-transform:capitalize;" />
					</div>
					<!-- Entr�e - B�t. - Imm. - R�s. : (Adresse secondaire) -->	
					<div class="item_i">
						<input type="text" name="complement_adresse_cli" maxlength="35" size="25" id="complement_adresse_p" value="<?php echo $complement_adresse_cli; ?>" style="text-transform:capitalize;" />
					</div>
					<!-- N� + Voie (rue, avenue, bvd, etc�) : (Adresse principale) -->
					<div class="item_i">
						<input type="text" name="adresse_cli" maxlength="35" size="25" id="adresse_cli" value="<?php echo $adresse_cli; ?>" style="text-transform:capitalize;" /> <span class="obligatoire">*</span>
					</div>
					<!-- Boite Postale - Lieu Dit -->
					<div class="item_i">
						<input type="text" name="adresse_cli_4" maxlength="35" size="25" id="adresse_cli_4" value="<?php echo $adresse_cli_4; ?>" style="text-transform:capitalize;" /> 
					</div>					
					
					<div class="item_i">
						<input type="text" name="cp_cli" maxlength="10" size="25" id="cp_cli" value="<?php echo $cp_cli; ?>" style="text-transform:Uppercase;" /> <span class="obligatoire">*</span>
					</div>
					<div class="item_i">
						<input type="text" name="ville_cli" maxlength="35" size="25" id="ville_cli" value="<?php echo $ville_cli; ?>" style="text-transform:Uppercase;" /> <span class="obligatoire">*</span>
					</div>
					<div class="item_i">
						<select id="pays" name="pays" style="width:208px;">  
							<option value="">S�lectionnez un pays</option>
							<?php  
							// S�lection des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
							$req_pays_base=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND
							countries_id IN (1,247,272,274,273,275,271)
							");
							while($res_req_pays_base=tep_db_fetch_array($req_pays_base)){
							?>
								<option <?php if($_POST['pays']==$res_req_pays_base['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res_req_pays_base['countries_id'];?>">
								<?php echo $res_req_pays_base['countries_name'];?>
								</option>
							<?php } ?>
							
							<optgroup label=" -------------------"> </optgroup>
							
							<?php 
							// retrait des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
							$req_pays=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND countries_id NOT IN (1,247,272,274,273,275,271) ORDER BY countries_name");
							while($res=tep_db_fetch_array($req_pays)){
							?>
								<option <?php if($_POST['pays']==$res['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res['countries_id'];?>">
								<?php echo $res['countries_name'];?>
								</option>
							<?php } ?>
							
						</select> <span class="obligatoire">*</span>
					</div>
					<div class="item_i">
						<input type="text" name="tel_portable_cli" maxlength="16" size="25" id="telephone_p" value="<?php echo $tel_portable_cli; ?>" /> <span class="obligatoire">*</span>
					</div>
					<div class="item_i">
						<input type="text" name="tel_fixe_cli" maxlength="16" size="25" id="telephone_f" value="<?php echo $tel_fixe_cli; ?>" />
					</div>
					<div class="item_i">
						<input type="text" name="fax_cli" maxlength="16" size="25" id="fax" value="<?php echo $fax_cli;?>" />
					</div>
				</div>
				<div class="clear"></div>			
			</div>         
		</div>
		<!-- fin block text-->   		
		
		<!-- block text -->
		<? // DEBUT ZONE NEWSLETTER ?>
		<div  style="width:100%; margin-top:20px; margin-right:0px; float:right;">
			<div class="titre_box-contenu">Notre newsletter</div>
			
			<div class="box-contenu">
			
				<div>
					<div style="width:500px; padding:10px; float:left; text-align:center;">
						<div>Souhaitez vous recevoir notre lettre d'information : <input type="checkbox" name="news" value="news" <?php if($_POST['news']!='') echo'checked="checked"'; ?> /></div>
					</div>
					<div class="clear"></div>
				</div>
				<div style="padding:0px 10px 10px 10px; font-size:10px; text-align:left;" >
					<strong>Nous n'envoyons pas plus d'un email par semaine.</strong><br />
					Soucieux de la protection de votre vie priv�e, nous traitons toutes les informations vous concernant avec la plus stricte confidentialit� et nous ne revendons aucune de ces informations � d'autres soci�t�s.
				</div>

			</div>         
			
			<div class="clear"></div>
			
			<div class="box-rgpd">
				<input type="checkbox" name="rgpd" value="rgpd" <?php if($_POST['rgpd']!='') echo'checked="checked"'; ?>>RGPD (R�glementation G�n�rale sur la Protection des Donn�es) : Vous acceptez que les informations recueillies sur ce formulaire sont enregistr�es dans un fichier informatis� par GROUP ARMY STORE / G�N�RAL ARMY STORE pour la gestion de votre compte. Elles sont conserv�es jusqu'� votre demande d'�ffacement et sont uniquement destin�es � la soci�t� GROUP ARMY STORE / G�N�RAL ARMY STORE. 
			</div>
			<input type="hidden" name="valid_form" value="ok" />
			<div style="margin-top:10px;">
				<a class="button" href="javascript:document.formu.submit();" title="Valider votre inscription">
					Valider votre inscription
                </a>
				<!--<button class="button">Click Me</button>-->
				<!--<a href="javascript:document.formu.submit();" title="Valider votre inscription"><img src="../template/base/boutons/bouton_valider_inscription.jpg" alt="bt valider" /></a>-->
			</div>
			<? // FIN ZONE NEWSLETTER ?>
			<!-- fin block text-->
		</div>
		<div class="clear"></div>
		
		
		<div style="padding:20px 10px 0px 20px; font-size:10px; text-align:center;">
			Conform�ment � la loi � informatique et libert�s �, vous pouvez exercer votre droit d'acc�s aux donn�es vous concernant et les faire rectifier en contactant : <br>
			GROUP ARMY STORE / G�N�RAL ARMY STORE - 5 rue H. Bajard - 26260 SAINT DONAT sur L'HERBASSE - T�l�phone : 09 53 31 59 47 - Email : info[at]generalarmystore.fr 
		</div>
	</form>
</div>

<?php 
require("../includes/footer.php");
require("../includes/page_bottom.php");
?>

