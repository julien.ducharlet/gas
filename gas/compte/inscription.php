<?php
/* Page permettant � un client de se connecte � son compte, ou bien a un utilisateur d'avoir acc�s � aux differentes partie des inscriptions */

$nom_page = "inscription";

require("../includes/page_top.php");
require("../includes/meta_head.php");
include("fonction/session.php");
include("fonction/new_password.php");

$erreur="";
if (isset($_REQUEST['connexion'])) { $erreur.=create(); }
if(isset($_POST['logout'])) { destroy(); }

require("../includes/header.php");
?>

<div id="corps_inscription">
	<div class="clear"></div>
	<div id="type_client">
		<!--<div id="titre">
			<h1>Cliquez sur l'image qui vous concerne pour vous inscrire.</h1>
			<div class="clear"></div>
		</div>
		-->
		<div style="padding-top:10px;"><a href="inscription-particulier.php<?php if(isset($_GET['panier'])){ echo "?panier=".$_GET['panier'];} ?>" id="block_img_particulier" title="Inscrivez vous en tant que particulier"></a></div>
		<div><a href="inscription-professionnel.php<?php if(isset($_GET['panier'])){ echo "?panier=".$_GET['panier'];} ?>" id="block_img_professionnel" title="Inscrivez vous en tant que professionnel"></a></div>
		
		<div><a href="inscription-administration.php<?php if(isset($_GET['panier'])){ echo "?panier=".$_GET['panier'];} ?>" id="block_img_administration" title="Inscrivez vous en tant qu'administration"></a></div>
		<div><a href="inscription-association.php<?php if(isset($_GET['panier'])){ echo "?panier=".$_GET['panier'];} ?>" id="block_img_association" title="Inscrivez vous en tant qu'association"></a></div>
		
		<div><a href="inscription-entreprise.php<?php if(isset($_GET['panier'])){ echo "?panier=".$_GET['panier'];} ?>" id="block_img_entreprise" title="Inscrivez vous en tant qu'entreprise"></a></div>
		<div><a href="inscription-revendeur.php<?php if(isset($_GET['panier'])){ echo "?panier=".$_GET['panier'];} ?>" id="block_img_revendeur" title="Inscrivez vous en tant que revendeur"></a></div>
		
	</div>
	
	<div class="clear"></div>

</div>
<?php  
require("../includes/footer.php");
require("../includes/page_bottom.php");

?>