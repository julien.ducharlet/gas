<?php
/* Affichage des options disponnibles dans le compte client */
   
$nom_page = "account";
   
require("../includes/page_top.php");

include("fonction/session.php");
if(!empty($_SESSION['customer_id'])) {
	
	require("../includes/meta_head.php");
	
	//requete permettant de rendre la rubrique porte monnaie virtuel accassible ou pas
	$req_purse=tep_db_query("Select *
						 From ".TABLE_CUSTOMERS_ARGENT."
						 Where customers_id=".$_SESSION['customer_id']."");
	
	require("../includes/header.php");
?>
 
<div id="corps">

	<div id="titre">
		<strong>Bienvenue <? echo $_SESSION['customers_firstname']; ?> dans votre espace perso</strong>
	</div>

	<div id="sous_titre_compte">Vous pourrez consulter votre compte client, modifier toutes vos informations personnelles et profiter de nombreuses autres fonctionnalit&eacute;s</div>

	<div id="conteneur_rub">

		<?php

		$accountBox = array();

		$accountBox[] = '<div class="item" onclick="location.href=\'account_edit.php\';" title="Informations de votre compte">
			<div class="img_rub"><img src="../template/base/compte/icones/bonhomme.png" alt="inf_compte" height="50" width="50" /></div>
			<div class="txt_rub"><strong>Information de votre compte</strong><br />
		Consultez et modifiez vos informations personnelles </div>
		</div>';

		$accountBox[] = '<div class="item" onclick="location.href=\'order.php\';" title="Mes commandes">
			<div class="img_rub"><img src="../template/base/compte/icones/commande.png" alt="inf_commande" height="50" width="50" /></div>
			<div class="txt_rub"><strong>Mes commandes</strong>�<br /> 
		Consultez l�historique de vos pr�c�dents achats et suivez en temps r�el vos  actuelles commandes</div>
		</div>';


		$accountBox[] = '<div class="item" onclick="location.href=\'address_book.php\';" title="Mon carnet d\'adresse">
			<div class="img_rub"><img src="../template/base/compte/icones/cahier.png" alt="inf_cahier" height="50" width="50" /></div>
			<div class="txt_rub"><strong>Mon carnet d�adresse</strong><br/>
		Enregistrez les adresses de livraison suppl�mentaires que vous souhaitez utiliser sur '. NOM_DU_SITE .'</div>
		</div>';

		$tempItem = '<div class="item" ';
			$tempItem .= 'onclick="location.href=\'electronic_purse.php\';" ';
			$tempItem .= 'title="Porte monnaie virtuel">
			<div class="img_rub"><img src="../template/base/compte/icones/porte_monnaie.png" alt="inf_monnaie" height="50" width="50" /></div>
			<div class="txt_rub"><strong>Porte monnaie virtuel</strong><br />
		Visualisez votre cr�dit et faites vos achats en toute simplicit� sur '. NOM_DU_SITE .'</div>
		</div>';
		$accountBox[] = $tempItem;

		$accountBox[] = '<div class="item" onclick="location.href=\'../faq.php\';" title="Les questions les plus fr�quemment pos�es">
			<div class="img_rub"><img src="../template/base/compte/icones/faq.png" alt="inf_faq" height="50" width="50" /></div>
			<div class="txt_rub"><strong>Questions les plus fr�quemment pos�es</strong><br />
		Trouvez les r�ponses � vos questions dans notre FAQ</div>	
		</div>';
		?>
		<!--<div style="clear:both;"></div>-->


		<!--3 ligne
		<div class="item" onclick="location.href='#';"
		title="Sauvegarde de mes paniers">
			<div class="img_rub"><img src="../template/base/compte/icones/panier.png" alt="inf_panier" /></div>
			<div class="txt_rub"><strong>Sauvegarde de mes paniers</strong><br /> 
		Retrouvez le contenu des paniers que vous avez souhait� conserver  </div>
		</div> -->

		<?php
		/*
		if($_SESSION['customers_type']==1 || $_SESSION['customers_type']==2) {
				
			$accountBox[] = '<div class="item" onclick="location.href=\'sponsorship.php\';" title="Offre de parrainage">
				<div class="img_rub"><img src="../template/base/compte/icones/parrainage.png" alt="inf_parrainage" height="50" width="50" /></div>
				<div class="txt_rub"><strong>Offre Parrainage</strong><br />
			Vous appr�ciez le site et son service ? D�couvrez  et b�n�ficiez de nos offres de parrainage </div>
			</div>';
		}
		*/
		?>

		<!--4 ligne
		<div class="item" onclick="location.href='#';"
		title="Mes bons d'achats">
			<div class="img_rub"><img src="../template/base/compte/icones/cadeau.png" alt="inf_panier" /></div>
			<div class="txt_rub"><strong>Mes bons d�achats</strong> <br />
		Retrouvez  les codes de tous vos bons d�achats ��la Boutique des Accessoires��</div>
		</div> -->

		<!--5 ligne
		<div class="item_rub1_g" onclick="location.href='newsletter.php';" title="Newsletter">		
			<div class="img_rub"><img src="../template/base/compte/icones/courrier.png" alt="inf_news" /></div>
			<div class="txt_rub"><strong>Newsletter du site</strong> <br />
		Informez-vous de l�actualit� et des exclusivit�s de la Boutique des Accessoires et b�n�ficiez d�offres adapt�es.</div>	    
		</div> -->

		<?php
		$accountBox[] = '<div class="item" onclick="location.href=\'../contact.php\';" title="Service apr�s vente">
			<div class="img_rub"><img src="../template/base/compte/icones/service_client.png" alt="inf_faq" height="50" width="50" /></div>
			<div class="txt_rub"><strong>Service Apr�s Vente</strong><br />
		Consultez toutes les informations n�cessaires pour contacter notre SAV</div>
		</div>';

		$req = tep_db_query("select customers_id from ".TABLE_ORDERS." where orders_status='21' and customers_id=".$_SESSION['customer_id']."");
		$nb_res = tep_db_num_rows($req);
		if($nb_res > 0){
			$accountBox[] = '<div class="item" onclick="location.href=\'devis.php\';" title="Mes devis">
			<div class="img_rub"><img src="../template/base/compte/icones/commande.png" alt="inf_commande" height="50" width="50" /></div>
			<div class="txt_rub"><strong>Mes devis</strong>�<br />
			Consultez les devis r�alis�s suite � votre demande.</div>
			</div>';
		} 
		for($i=0 ; $i<sizeof($accountBox) ; $i++) {
			echo $accountBox[$i];
		}

		?>
			<div style="clear:both;"></div>
	</div>
</div>
<?php 
} else
{header('Location: connexion.php');}

require("../includes/footer.php");
require("../includes/page_bottom.php");
?>

