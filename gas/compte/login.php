<?php
/* Page permettant � un client de se connecte � son compte, ou bien a un utilisateur d'avoir acc�s � aux differentes parrtie des inscriptions */

$nom_page = "login";

require("../includes/page_top.php");
require("../includes/meta_head.php");
include("fonction/session.php");
include("fonction/new_password.php");
$erreur="";
if (isset($_REQUEST['connexion']))
{$erreur.=create();}

if(isset($_POST['logout']))
{destroy();}

require("../includes/header.php");
?>

<div id="corps_login">

<div id="login"><h1>Veuillez renseigner les champs suivants pour acc&eacute;der &agrave; votre compte</h1></div>
<div class="clear"></div>
<div class="login_form">
	<form id="addressForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    	<input type="hidden" name="panier" value="<?php echo $_GET['panier']; ?>" />
    	<div id="info_adr">Votre adresse e-mail :</div>
        <div class="login_form_text" id="input_adr"><input id="mail" name="mail" value="<?php if(isset($_POST["mail"])){echo $_POST["mail"];}?>" type="text" size="22" /></div>
        <div id="info_mdp">Votre mot de passe : </div>
        <div class="login_form_text" id="input_mdp"><input name="mdp" type="password" value="" size="14" /></div>
        <input type="hidden" name="connexion" value="ok" />
        <div id="bt_log">
        <input src="../template/base/boutons/bouton_connexion.jpg" alt="bouton connexion" class="connexion_submit" type="image">
        </div>
        <br />
        <div id="oubli_pass">
            Mot de passe oubli&eacute; ?
            <a onclick="form_pass();" title="Ouvrir le fromulaire pour l'oubli de mot de passe">Cliquez ici</a>
        </div>
        <div id="oubli_pass_close">
        	Mot de passe oubli&eacute; ?
            <a onclick="document.getElementById('formulaire_oubli').style.display='none';
            					 document.getElementById('oubli_pass').style.display='block';
                                 document.getElementById('oubli_pass_close').style.display='none';"
                                 title="Fermer le fromulaire pour l'oubli de mot de passe">Fermer</a>
        </div>
        <div class="clear"></div>
        <div id="erreur_log"><strong><?php echo $erreur; ?></strong></div>
     </form>
</div>

<div id="formulaire_oubli">
	
    <div id="info_pass">
    	<div id="img_oubli"></div>
    	<div id="info_oubli">Si vous avez oubli&eacute; votre mot de passe, vous pouvez indiquer ci-contre l'adresse e-mail que vous avez utilis&eacute;e pour cr&eacute;er votre compte. Vous recevrez alors un e-mail contenant vos identifiants. <br /><br />Si cette adresse e-mail a chang&eacute;, contactez notre <a href="../contact.php" title="Contatez notre support technique">support technique.</a></div>
	</div>
    
	<div id="formu">
       	<div id="field_oubli">
           <div id="field">
        	<FIELDSET id="field_oubli"><LEGEND> Je veux r&eacute;cup&eacute;rer mes informations</LEGEND>
            <div id="info_adr_oubli">Adresse email :</div>
            <div id="input_oubli"><input id="email" type="text" value="" size="20" /></div>
            <div><img onclick="rech();" src="../template/base/compte/boutons/bouton_envoyer.jpg" alt="bouton erreur" title="Recevoir votre mot de passe" /></div>
            </FIELDSET>
           </div>
        </div>
        <div class="clear"></div>
        <div id="erreur"></div>
		
    </div>
</div>

<div class="clear"></div>
<div id="type_client">
	<div id="info_nouv_compte">
        <h1>Pas encore client sur notre site ? Veuillez choisir votre statut</h1>
        <div class="clear"></div>
    </div>
    <div><a href="create_account_part.php<?php if(isset($_GET['panier'])){ echo "?panier=".$_GET['panier'];} ?>" id="block_img_part" title="Inscrivez vous en tant que particulier"></a></div>
    <div><a href="create_account_pro.php<?php if(isset($_GET['panier'])){ echo "?panier=".$_GET['panier'];} ?>" id="block_img_pro" title="Inscrivez vous en tant que professionnel"></a></div>
    
</div>
<br  /><br />
<div class="clear"></div>

</div>
<?php  
require("../includes/footer.php");
require("../includes/page_bottom.php");

?>