<?php
/* Page permattant l'edition des infos (generales et mot de passe) du compte client */

$nom_page = "account_edit";
   
require("../includes/page_top.php");

include("fonction/session.php");

if (!empty($_SESSION['customer_id'])) {
	
	require("../includes/meta_head.php");
	require("../includes/header.php");

	if (empty($_POST['old_mdp'])) { $_POST['old_mdp'] = ''; } 
	if (empty($_POST['new_mdp'])) { $_POST['new_mdp'] = ''; }
	if (empty($_POST['conf_new_mdp'])) { $_POST['conf_new_mdp'] = ''; }
	if (empty($_POST['sms'])) { $_POST['sms'] = ''; }
	
	
	$mail_cli = strtolower(format_clients($_POST['mail_cli']));
	$nom_cli = strtoupper(format_inscription($_POST['nom_cli']));
	$prenom_cli = ucwords(strtolower(format_inscription($_POST['prenom_cli'])));		
	$societe_cli = strtoupper(format_inscription($_POST['societe']));
	$siret_cli = strtoupper(format_inscription($_POST['siret_cli']));
	$tva_cli = strtoupper(format_inscription($_POST['tva_cli']));
	$tel_portable_cli = format_tel($_POST['tel_portable_cli']);
	$tel_fixe_cli = format_tel($_POST['tel_fixe_cli']);
	$fax_cli = format_tel($_POST['fax_cli']);
		
	//fonction permettant d'afficher une div avec un message d'erreur lorsqu'un formulaire est mal renseign�
	function retour_erreur($message){
		return'<div class="haut_erreur"></div>
			   <div class="milieu_erreur">'.$message.'</div>
			   <div class="clear"></div>  
			   <div class="bas_erreur"></div>';
	}

	//fonction permettant d'afficher une div avec un message de validation lorsqu'un formulaire est bien renseign�
	function retour_validation($message){
		return'<div id="bordure_mdp_haut_verte" style="background-color:#8CCB7C;"></div>
			   <div id="bordure_mdp_milieu_vert" style="background-color:#8CCB7C;">'.$message.'</div>
			   <div class="clear"></div>  
			   <div id="bordure_mdp_bas_verte" style="background-color:#8CCB7C;"></div>';
	}

	//fonction permettant de verifier le formulaire de nouveau mot de passe et de le prendre en compte 
	function new_password($old,$new,$new_confirm){
		$ret='';
		$check=true;
		$res = tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id']."");
		$r=tep_db_fetch_array($res);
		$cp=$r['customers_password'];
		if(!tep_validate_password($old, $cp)){
			 $ret.='erreur mot de passe<br />';
			 $check=false;
		}
		if($new==''){
			  $ret.="Vous devez indiquer un nouveau mot de passe <br />";
			  $check=false;
		}
		if($new_confirm==''){
			  $ret.="Vous devez confirmer votre nouveau mot de passe <br />";
			  $check=false;
		}
		if($new!=$new_confirm){
			  $ret.="erreur lors de la confirmation du mot de passe <br />";
			  $check=false;
		}
		if($check==true){
			$new_mdp=tep_encrypt_password($new);
			$exe_req=tep_db_query("update ".TABLE_CUSTOMERS." set customers_password ='".$new_mdp."' where customers_id=".$_SESSION['customer_id']." ");
			$ret.="Votre mot de passe a &eacute;t&eacute; modifi&eacute;";
			return retour_validation($ret);
		}
		return retour_erreur($ret);
	}

	//fonction permettant de faire la validation des informations generales du compte client particulier
	function info_modif(){
		$ret='';
		$formule_polie="Vous devez indiquer votre ";
		$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
		$check=true;
		if ($_POST['civi']=='') {
			$ret.=$formule_polie."civilit&eacute; <br />";
			$check=false;
		}
		if ($nom_cli=='') {
			$ret.=$formule_polie."nom<br />";
			$check=false;
		}
		if ($prenom_cli=='') {
			$ret.=$formule_polie."prenom<br />";
			$check=false;
		}
		/*
		if ($_POST['jour']=='jour') {
			$ret.=$formule_polie."jour de naissance<br />";
			$check=false;
		}
		if ($_POST['mois']=='mois') {
			$ret.=$formule_polie."mois de naissance<br />";
			$check=false;
		}
		if ($_POST['annee']=='annee') {
			$ret.=$formule_polie."ann&eacute;e de naissance<br />";
			$check=false;
		}
		*/
		if ($_POST['pays']=='pays') {
			$ret.=$formule_polie."pays<br />";
			$check=false;
		}
		if ($tel_portable_cli=="" || !preg_match('<^[0-9]*$>', format_tel($tel_portable_cli))) {
			$ret.=$formule_polie."votre num�ro de t�l�phone mobile (valide)<br />";
			$check=false;
		}
		if (!preg_match($verif,$mail_cli)) {
			$ret.=$formule_polie."adresse e-mail (valide)<br />";
			$check=false;
		} else {
			$res=tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".addslashes($mail_cli)."' and customers_id=".$_SESSION['customer_id']."");
			$r=tep_db_fetch_array($res);
			if ($mail_cli!=$r['customers_email_address']) {
				$ress=tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".addslashes($mail_cli)."'");
				$rs=tep_db_fetch_array($ress);
				if ($mail_cli==$rs['customers_email_address']) {
					$ret.="Cette adresse e-mail est d&eacute;j&agrave; utilis&eacute;e<br />";
					$check=false;
				}
			}
		}
		if ($_POST['news']=='') { $newsletter=0; } else { $newsletter=1; }
		if ($_POST['sms']=='') { $sms=0; } else { $sms=1; }
		  
		if($check==true){	
			$modif=tep_db_query("	UPDATE ".TABLE_CUSTOMERS." SET customers_gender='".$_POST['civi']."',
										 customers_firstname='".addslashes($prenom_cli)."',
										 customers_lastname='".addslashes($nom_cli)."',
										 customers_dob='".$_POST['annee']."-".$_POST['mois']."-".$_POST['jour']."',
										 customers_email_address='".addslashes($mail_cli)."',
										 customers_country='".$_POST['pays']."',
										 customers_telephone='".addslashes($tel_portable_cli)."',
										 customers_fixe='".addslashes($tel_fixe_cli)."',
										 customers_newsletter='".$newsletter."',
										 customers_news_sms='".$sms."'
									WHERE customers_id=".$_SESSION['customer_id']."");
			$ret="Les informations ont &eacute;t&eacute; enregistr&eacute;es";
			return retour_validation($ret);
		}
			return retour_erreur($ret);
	}

	//fonction permettant de faire la validation des informations generales du compte client professionnel
	function info_modif_pro(){
		$ret='';
		$formule_polie="Vous devez indiquer votre ";
		$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
		$check=true;
		if ($_POST['civi']=='') {
			$ret.=$formule_polie."civilit&eacute; <br />";
			$check=false;
		}
		if ($nom_cli=='') {
			$ret.=$formule_polie."nom<br />";
			$check=false;
		}
		if ($prenom_cli=='') {
			$ret.=$formule_polie."prenom<br />";
			$check=false;
		}
		/*
		if ($_POST['jour']=='jour') {
			$ret.=$formule_polie."jour de naissance<br />";
			$check=false;
		}
		if ($_POST['mois']=='mois') {
			$ret.=$formule_polie."mois de naissance<br />";
			$check=false;
		}
		if ($_POST['annee']=='annee') {
			$ret.=$formule_polie."ann&eacute;e de naissance<br />";
			$check=false;
		}
		*/
		if ($_POST['pays']=='pays') {
			$ret.=$formule_polie."pays<br />";
			$check=false;
		}
		if ($societe_cli=='') {
			$ret.=$formule_polie." le nom de votre soci&eacute;t&eacute; <br />";
			$check=false;
		}	  
		if ($fax_cli!='') {
			if(preg_match("#^0[0-68]([-. ]?[0-9]{2}){4}$#", $fax_cli)==false) {
				$ret.=$formule_polie."num&eacute;ro de fax (valide)<br />";
				$check=false;
			}
		}		
		if(!preg_match($verif,$mail_cli)){
			$ret.=$formule_polie."adresse e-mail (valide)<br />";
			$check=false;
		} else {
			$res=tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".addslashes($mail_cli)."' and customers_id=".$_SESSION['customer_id']."");
			$r=tep_db_fetch_array($res);
			if ($mail_cli!=$r['customers_email_address']) {
				$ress=tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".addslashes($mail_cli)."'");
				$rs=tep_db_fetch_array($ress);
				if ($mail_cli==$rs['customers_email_address']) {
					$ret.="Cette adresse e-mail est d&eacute;j&agrave; utilis&eacute;e<br />";
					$check=false;
				}
			}
		}
		if ($_POST['news']=='') { $newsletter=0; } else { $newsletter=1; }
		if ($_POST['sms']=='') { $sms=0; } else { $sms=1; }
		  
		if($check==true){	
			$modif=tep_db_query("	UPDATE ".TABLE_CUSTOMERS." SET customers_gender='".$_POST['civi']."',
										 customers_firstname='".addslashes($prenom_cli)."',
										 customers_lastname='".addslashes($nom_cli)."',
										 customers_dob='".$_POST['annee']."-".$_POST['mois']."-".$_POST['jour']."',
										 customers_email_address='".addslashes($mail_cli)."',
										 customers_country='".$_POST['pays']."',
										 customers_telephone='".addslashes($tel_portable_cli)."',
										 customers_fixe='".addslashes($tel_fixe_cli)."',
										 customers_newsletter='".$newsletter."',
										 customers_news_sms='".$sms."',
										 customers_company='".addslashes($societe_cli)."',
										 customers_fax='".addslashes($fax_cli)."'
									WHERE customers_id=".$_SESSION['customer_id']."");
			$ret.="modification r&eacute;alis&eacute;e";
			return retour_validation($ret);
		}
		return retour_erreur($ret);
	}
	?>

	<div id="corps">
		<div id="bande_rubrique_g">
			<div id="bande_image"><img src="../template/base/compte/icones/bonhomme.png" alt="inf_commande" /></div>
			<div id="bande_titre">Informations de mon compte</div>
			<div id="bande_sous_titre">&Eacute;diter les diff&eacute;rentes infomations de votre compte</div>
		</div>
		<div id="bande_rubrique_d"><a href="account.php" title="Retourner � mon compte"><img src="../template/base/compte/bouton_retour.png" alt="retour" /></a></div>
		<div class="clear"></div>
	<?php
	if (isset($_POST['formu'])) {
		echo $res=new_password($_POST['old_mdp'],$_POST['new_mdp'],$_POST['conf_new_mdp']);
	 }
	?>
	
	
<? if ($_SESSION['customer_id'] == 3) { ?> 
	
	<!-- block text -->
	<? // DEBUT ZONE CHANGEMENT MOT DE PASSE ?>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" name="mdp" >
		<div  style="width:100%; margin-top:10px; margin-right:0px;">
			<div class="titre_box-contenu">Changer votre mot de passe</div>
			
			<div class="box-contenu">
				<div class="conteneur_desc_item">
					<div class="item_i">Mot de passe actuel :</div>
					<div class="item_i">Nouveau mot de passe :</div>
					<div class="item_i">Confirmer le mot de passe :</div>
				</div>
				<div class="conteneur_composant_item">
					<div class="item_i">
						<input type="password" name="old_mdp" size="25" value="<?php echo $_POST['old_mdp']; ?>"  />
					</div>
					<div class="item_i">
						<input type="password" name="new_mdp"  size="25" value="<?php echo $_POST['new_mdp']; ?>"  />
					</div>
					<div class="item_i">
						<input type="password" name="conf_new_mdp" size="25" value="<?php echo $_POST['conf_new_mdp']; ?>"  />
					</div>
				</div>
				<?php if ($_POST['formu']=='ok') { ?>
					<script type="text/javascript">affiche(1);</script>
				<?php } ?>
				<input type="hidden" name="formu" value="ok" /> 
				<div class="clear"></div>
				<div style="margin-top:10px;">
					<a class="button" href="javascript:document.mdp.submit();" title="Valider la modification du mot de passe">
						Changer le mot de passe
					</a>
				</div>
			</div>  
			
		</div>
	</form>
	<? // FIN ZONE CHANGEMENT MOT DE PASSE ?>
	<!-- fin block text -->	
	
	
	<!-- block text -->
	<? // DEBUT ZONE COORDONNEES ?>
	<?php
	if(isset($_POST['info'])) {
		if($_SESSION['customers_type']==2 || $_SESSION['customers_type']==3 || $_SESSION['customers_type']==4) { echo $res=info_modif_pro(); } else { echo $res=info_modif(); }        
	}
	$res=tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id']."");
	$r=tep_db_fetch_array($res);
	//pour la date de naissance
	$d=$r['customers_dob'];
	list($an, $mois, $j) = split('-', $d);
	list($jour,$h) = split(' ', $j);
	?>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>#error" name="info">
		<div  style="width:100%; margin-top:10px; margin-right:0px;">
			<div class="titre_box-contenu">Vos coordonn�es</div>
			
			<div class="box-contenu">
				<div class="conteneur_desc_item">
					<div class="item_i">Civilit� :</div>
					<div class="item_i">Nom :</div>
					<div class="item_i">Pr�nom :</div>
					<?php 
					if ($_SESSION['customers_type']==1) {
					?>
						<div class="item_i">Date de Naissance :</div>  
					<?php 
					}
					?>
					<div class="item_i">Adresse email :</div>  
					<div class="item_i">Pays :</div>  
					<div class="item_i">T�l�phone mobile</div>  
					<div class="item_i">T�l�phone fixe</div>  
				</div>
				<div class="conteneur_composant_item">
					<div class="item_i">						
						<input type="radio" name="civi" value="m" id="H" <?php if (isset($_POST['info'])) { if ($_POST['civi']=='m') { echo 'checked="checked"'; } } else if ($r['customers_gender']=='m') { echo 'checked="checked"'; } ?> />
						Mr&nbsp;&nbsp;&nbsp;
						<input type="radio" name="civi" value="f" id="MME" <?php if (isset($_POST['info'])) { if ($_POST['civi']=='f') { echo 'checked="checked"'; } } else if ($r['customers_gender']=='f') { echo 'checked="checked"'; } ?> />Mme&nbsp;&nbsp;&nbsp;
						<input type="radio" name="civi" value="fe" id="MLLE" <?php if (isset($_POST['info'])) { if ($_POST['civi']=='d') { echo 'checked="checked"';} } else if ($r['customers_gender']=='d') { echo 'checked="checked"'; } ?> />
						Mlle
					</div>
					<div class="item_i">
						<input type="text" id="nom" name="nom_cli" maxlength="32" size="25" value="<?php if (isset($_POST['info'])) { echo stripslashes($nom_cli); } else { echo stripslashes($r['customers_lastname']); } ?>" style="text-transform:Uppercase;" />
					</div>
					<div class="item_i">
						<input type="text" id="prenom" name="prenom_cli" maxlength="32" size="25" value="<?php if (isset($_POST['info'])) { echo stripslashes($prenom_cli); } else { echo stripslashes($r['customers_firstname']); } ?>" style="text-transform:capitalize;" />
					</div>
					<?php 
					if ($_SESSION['customers_type']==1) {
					?>
						<div class="item_i">
							DOB uniquement au particulier et professionnel statut 1
						</div>
					<?php 
					}
					?>
					<div class="item_i">
						<input type="text" name="mail_cli" size="25" id="mail" style="text-transform:Lowercase;" value="<?php if (isset($_POST['info'])) { echo $mail_cli; } else { echo $r['customers_email_address']; } ?>" />
					</div>
					<div class="item_i">
						
						<select id="pays" name="pays" style="width:208px;">  
							<?php  
							// S�lection des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
							$req_pays_base=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND
							countries_id IN (1,247,272,274,273,275,271)
							");
							while($res_req_pays_base=tep_db_fetch_array($req_pays_base)){
							?>
								<option <?php if(isset($_POST['info'])) { if($_POST['pays']==$res_req_pays_base['countries_id']) echo'selected="selected"'; } else if ($r['customers_country']==$res_req_pays_base['countries_id']) {  echo 'selected="selected"'; } ?> value="<?php echo $res_req_pays_base['countries_id'];?>">
									<?php echo $res_req_pays_base['countries_name'];?>
								</option>
							<?php } ?>
							
							<optgroup label=" -------------------"> </optgroup>
							
							<?php 
							// retrait des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
							$req_pays=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND countries_id NOT IN (1,247,272,274,273,275,271) ORDER BY countries_name");
							while($res=tep_db_fetch_array($req_pays)){
							?>
								<option <?php if(isset($_POST['info'])) { if($_POST['pays']==$res['countries_id']) echo'selected="selected"'; } else if ($r['customers_country']==$res['countries_id']) {  echo 'selected="selected"'; } ?> value="<?php echo $res['countries_id'];?>">
									<?php echo $res['countries_name'];?>
								</option>
							<?php } ?>
						</select>
						
					</div>
					<div class="item_i">
						<input type="text" name="tel_portable_cli" maxlength="16" size="25" id="telephone_p" value="<?php if (isset($_POST['info'])) { echo $tel_portable_cli; } else { echo $r['customers_telephone']; } ?>"  />
					</div>
					<div class="item_i">
						<input type="text" name="tel_fixe_cli" maxlength="16" size="25" id="telephone_f" value="<?php if (isset($_POST['info'])) { echo $tel_fixe_cli; } else { echo $r['customers_fixe']; } ?>" />
					</div>
				</div>
				<div class="clear"></div>				
			</div>			
		</div>
	</form>
	<? // FIN ZONE COORDONNEES ?>
	<!-- fin block text -->	
	
	<?php 
	if ($_SESSION['customers_type']!=1) {
	?>
		
		<!-- block text -->
		<? // DEBUT ZONE CHANGEMENT MOT DE PASSE ?>
		
			<div  style="width:100%; margin-top:10px; margin-right:0px;">
				<div class="titre_box-contenu">Information professionnelles</div>
				
				<div class="box-contenu">
					<div class="conteneur_desc_item">
						<div class="item_i">Nom de soci�t�</div>
						<div class="item_i">Num�ro de siret</div>
						<div class="item_i">Num�ro de TVA :</div>
						<div class="item_i">Num�ro de fax :</div>
					</div>
					<div class="conteneur_composant_item">						
						<div class="item_i">
							<input type="text" name="societe" size="25" value="<?php if (isset($_POST['info'])) { echo $societe_cli; } else { echo $r['customers_company']; } ?>" />
						</div>
						<div class="item_i">
							<input type="text" name="siret_cli" size="25"  value="<?php if (isset($_POST['info'])) { echo $siret_cli; } else { echo $r['customers_siret']; } ?>" readonly="readonly" />
						</div>
						<div class="item_i">
							<input type="text" name="fax" size="25" value="<?php if (isset($_POST['info'])) { echo $fax_cli; } else { echo $r['customers_fax']; } ?>"/>
						</div>						
						<div class="item_i">
							<input type="text" name="tva_cli" size="25" value="<?php if (isset($_POST['info'])) { echo $tva_cli; } else { echo $r['customers_tva']; } ?>" readonly="readonly" />
						</div>				
					</div>
					<div class="clear"></div>
				</div>  
				
			</div>
		
		<? // FIN ZONE CHANGEMENT MOT DE PASSE ?>
		<!-- fin block text -->	
		
		<!-- block text -->
		<? // DEBUT ZONE NEWSLETTER ?>
		<div  style="width:100%; margin-top:20px; margin-right:0px; float:right;">
			<div class="titre_box-contenu">Notre newsletter</div>
			
			<div class="box-contenu">
			
				<div>
					<div style="width:500px; padding:10px; float:left; text-align:center;">
						<div>Souhaitez vous recevoir notre lettre d'information : 
							<input type="checkbox" name="news" value="news" <?php if (isset($_POST['info'])) { if ($_POST['news']=='news') echo 'checked="checked"'; } else if ($r['customers_newsletter']==1) { echo 'checked="checked"'; } ?> />
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div style="padding:0px 10px 10px 10px; font-size:10px; text-align:left;" >
					<strong>Nous n'envoyons pas plus d'un email par semaine.</strong><br />
					Soucieux de la protection de votre vie priv�e, nous traitons toutes les informations vous concernant avec la plus stricte confidentialit� et nous ne revendons aucune de ces informations � d'autres soci�t�s.
				</div>

			</div>         
			
			<div class="clear"></div>
			
			<div class="box-rgpd">
				<input type="checkbox" name="rgpd" value="rgpd" <?php if($_POST['rgpd']!='') echo'checked="checked"'; ?>>RGPD (R�glementation G�n�rale sur la Protection des Donn�es) : Vous acceptez que les informations recueillies sur ce formulaire sont enregistr�es dans un fichier informatis� par GROUP ARMY STORE / G�N�RAL ARMY STORE pour la gestion de votre compte. Elles sont conserv�es jusqu'� votre demande d'�ffacement et sont uniquement destin�es � la soci�t� GROUP ARMY STORE / G�N�RAL ARMY STORE. 
			</div>
			<input type="hidden" name="info"  value="ok" /> 
			<div style="margin-top:10px;">				
				<a class="button" href="javascript:document.info.submit();" title="Valider la modfication des informations">
					Valider les modifications
				</a>
			</div>
		</div>
		<? // FIN ZONE NEWSLETTER ?>
		<!-- fin block text-->
		
	<?php 
	}
	?>
	
<? 
} 
?>
	
	<div class="clear"></div>
	<?php
	if(isset($_POST['info'])) {
		if($_SESSION['customers_type']==2) { echo $res=info_modif_pro(); } else { echo $res=info_modif(); }        
	}
	?>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>#error" name="info">

	<div id="block2" style="display:block; margin-top:20px;">
	<?php
	$res=tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id']."");
	$r=tep_db_fetch_array($res);
	//pour la date de naissance
	$d=$r['customers_dob'];
	list($an, $mois, $j) = split('-', $d);
	list($jour,$h) = split(' ', $j);
	?>
	<!-- block text -->
	<div style=" width:100%; float:left;  margin-right:0px;">
		 <div class="bordure_haut_verte">
			<p class="titre_info"><span>Mes coordonn&eacute;es</span></p>
		 </div>
		 <div class="bordure_pixel_vert" style="height:260px;">
			  <div style="padding:5px 0px 5px 20px;">
				<div class="conteneur_desc_item" style="width:150px;">
				   <div class="item_i">Civilit� :</div>
				   <div class="item_i">Nom :</div>
				   <div class="item_i">Pr�nom :</div>
				   <div class="item_i">Date de naissance :</div>
				   <div class="item_i">Adresse email :</div>                            
				   <div class="item_i">Pays :</div>
				   <div class="item_i">T�l�phone portable :</div>
				   <div class="item_i">T�l�phone fixe :</div>
				</div>
				<div class="conteneur_composant_item" style="width:280px;">
				   <div class="item_i">
					<input type="radio" name="civi" value="m" id="H" <?php if (isset($_POST['info'])) { if ($_POST['civi']=='m') { echo 'checked="checked"'; } } else if ($r['customers_gender']=='m') { echo 'checked="checked"'; } ?> />
					Mr&nbsp;&nbsp;&nbsp;
					<input type="radio" name="civi" value="f" id="MME" <?php if (isset($_POST['info'])) { if ($_POST['civi']=='f') { echo 'checked="checked"'; } } else if ($r['customers_gender']=='f') { echo 'checked="checked"'; } ?> />Mme&nbsp;&nbsp;&nbsp;
					<input type="radio" name="civi" value="fe" id="MLLE" <?php if (isset($_POST['info'])) { if ($_POST['civi']=='d') { echo 'checked="checked"';} } else if ($r['customers_gender']=='d') { echo 'checked="checked"'; } ?> />
					Mlle
				 </div>
				 <div class="item_i">
					<input type="text" id="nom" name="nom_cli" size="25" value="<?php if(isset($_POST['info']))
																			  {echo stripslashes($nom_cli);}
																			   else echo stripslashes($r['customers_lastname']);
																		?>" />
				 </div>
				 <div class="item_i">
				   <input type="text" id="prenom" name="prenom_cli" size="25" value="<?php if (isset($_POST['info'])) { echo stripslashes($prenom_cli); } else { echo stripslashes($r['customers_firstname']); } ?>" />
				 </div>
				 <div class="item_i">
				   <select id="jours" name="jour">
					  <option  value="jour">jour</option>
					  <?php  
						for($i=1;$i<32;$i++)
						  {	
					  ?>
					   <option <?php if(isset($_POST['info']))
									{
									  if($_POST['jour']==$i) 
									  echo 'selected="selected"';
									}
									else if($jour==$i)
									echo 'selected="selected"';
								?> value="<?php echo $i;?>"><?php echo $i;?>
					   </option>
					  <?php
						  }
					  ?>
				   </select>
				   
				   <select id="mois" name="mois" >
					   <option value="mois">mois</option>
						<?php  
						   $tab=array('janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre');
						   for($i=0;$i<count($tab);$i++)
							  {	
						 ?>
						<option <?php if(isset($_POST['info']))
										{
										  if($_POST['mois']==$i+1) 
										   echo 'selected="selected"';
										}
									  else if($mois==$i+1)
										 echo 'selected="selected"'; ?> value="<?php echo $i+1;?>"><?php echo $tab[$i];?>
					   </option>
					   <?php
							}
					   ?>
				   </select> 
											 
				   <select id="annees" name="annee" >
					  <option value="annee">ann&eacute;e</option>   
					  <?php  
						  $age_min= date("Y") -14;
						  $age_max= date("Y") -80;
						  for($i=$age_min ;$i>$age_max;$i--)
						   {
					   ?>
								<option <?php if(isset($_POST['info'])) { if($_POST['annee']==$i) { echo 'selected="selected"'; } } else if ($an==$i) { echo 'selected="selected"'; } ?> value="<?php echo $i;?>"><?php echo $i;?>
								</option>
							<?php
							}
							?>
							</select>
						</div>
						<div class="item_i">
							<input type="text" name="mail_cli" size="25" id="mail" value="<?php if (isset($_POST['info'])) { echo $mail_cli; } else { echo $r['customers_email_address']; } ?>" />
						</div>
						<div class="item_i">
							<select id="pays" name="pays" style="width:208px;">  
								<option value="">Choisissez un pays</option>   
								<?php  
								$req_pays=tep_db_query("Select countries_id, countries_name From ".TABLE_COUNTRIES." order by countries_name");
								while($res=tep_db_fetch_array($req_pays)) {
									?>
									<option <?php if(isset($_POST['info'])) { 
												if($_POST['pays']==$res['countries_id']) { echo 'selected="selected"'; }
											} else if ($r['customers_country']==$res['countries_id']) {
												echo 'selected="selected"';
											}
											?> value="<?php echo $res['countries_id'];?>">
										<?php echo $res['countries_name'];?>
									</option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="item_i">
							<input type="text" name="tel_portable_cli" size="25" id="telephone_p" value="<?php if (isset($_POST['info'])) { echo $tel_portable_cli; } else { echo $r['customers_telephone']; } ?>"  />
						</div>
						<div class="item_i">
							<input type="text" name="tel_fixe_cli" size="25" id="telephone_f" value="<?php if (isset($_POST['info'])) { echo $tel_fixe_cli; } else { echo $r['customers_fixe']; } ?>" />
						</div>
					</div>
				</div>
				<div class="clear"></div>                                                    
			</div>         
			<div class="bordure_bas_verte"></div>
		</div>
		<!-- fin block text-->
		
		
		
		
		<!-- block text -->
		<div style="width:100%; float:left; margin-right:0px; margin-top:20px;">
			<?php 
			if ($_SESSION['customers_type']==2) {
				echo '	<div class="bordure_haut_verte">
							<p class="titre_info"><span>Informations Professionelles</span></p> 
						</div>';  
			} else {
				echo '	<div class="bordure_haut_verte">
							<p class="titre_info"><span>Souhaitez vous recevoir</span></p> 
						</div>';    
			} 
			?>
			<div class="bordure_pixel_vert">
				<?php 
				if($_SESSION['customers_type']==2) {
				?> 
				<div style="padding:10px 15px 10px 20px;">
					<div class="conteneur_desc_item" style="width:200px; height:130px;">
						<div class="item_i">Nom de la soci&eacute;t&eacute; :</div>
						<div class="item_i">Num&eacute;ro de fax :</div>
						<div class="item_i">Num&eacute;ro de siret :</div>
						<div class="item_i">Num&eacute;ro de TVA :</div>
					</div>
					<div class="conteneur_desc_item" style="width:150px;">    
						<div class="item_i">
							<div class="item_i">
								<input type="text" name="societe" size="25" value="<?php if(isset($_POST['info'])) { echo $societe_cli; } else { echo $r['customers_company']; }?>" />
							</div>
							<div class="item_i">
								<input type="text" name="fax" size="25" value="<?php if(isset($_POST['info'])) { echo $fax_cli; } else { echo $r['customers_fax'];} ?>"/>
							</div>
							<div class="item_i">
								<input type="text" name="siret_cli" size="25"  value="<?php if(isset($_POST['info'])) { echo $siret_cli; } else { echo $r['customers_siret']; } ?>" readonly="readonly" />
							</div>
							<div class="item_i">
								<input type="text" name="tva_cli" size="25" value="<?php if(isset($_POST['info'])) { echo $tva_cli; } else { echo $r['customers_tva']; } ?>" readonly="readonly" /></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				<div class="clear"></div>
				<div class="bordure_pixel_vert2">&nbsp;</div>			
			
			<?php 
			}
			
			if ($_SESSION['customers_type']==2) {
				echo '<p class="titre_info"><span>Souhaitez vous recevoir</span></p>';
			}
			?>
			<div style="padding:10px 0px 10px 20px;">
				<div class="conteneur_desc_item" style="width:170px;">
				<div class="item_i">Notre lettre d'information :</div>
				<?php 
				if (SMS_NOTIFICATION == '1') {
					echo '<div class="item_i">Des SMS de notifications :</div>';
				}
			?>
		   </div>
		   <div class="conteneur_composant_item2" style="width:270px;">
			 <div class="item_i">
			   <div style="float:left;">
				<input type="checkbox" name="news" value="news" <?php if (isset($_POST['info'])) { if ($_POST['news']=='news') echo 'checked="checked"'; } else if ($r['customers_newsletter']==1) { echo 'checked="checked"'; } ?> />
				</div>
				<div class="mentions" style="font-size:10px; color:<?php echo COULEUR_7; ?>; width:230px; height:30px; float:left;">
					Un email par semaine maximum.
				</div>
			  </div>
			 
			</div>
			<div class="clear"></div>
		  </div>                    
		 </div>         
		 <div class="bordure_bas_verte" style="margin-bottom:10px;"></div>
			<input type="hidden" name="info"  value="ok" /> 
				<a href="javascript:document.info.submit();" title="Valider la modfication des informations"><img src="../template/base/boutons/bouton_valider.jpg" alt="bouton valider" /></a>
		</div>
	   <!-- fin block text-->
		<div class="clear"></div>
	</div>
	</form>
	</div>
<?php 
} else {
	header('Location: connexion.php');
}
require("../includes/footer.php");
require("../includes/page_bottom.php");
?>

