<?php
/* Page permettant au client de parrainer des filleuls */
   
$nom_page = "sponsorship";
require("../includes/page_top.php");
include("fonction/session.php");

if(!empty($_SESSION['customer_id'])){
	
	if ($_REQUEST['suppr_filleul'] != '') {
		tep_db_query("DELETE FROM " . TABLE_PARRAINAGE . " WHERE id_parrainage = '" . $_REQUEST['suppr_filleul'] . "' AND id_parrain = '" . $_SESSION['customer_id'] . "'");
		header('Location: ' . $_SERVER['PHP_SELF'] . '#filleuls');
	}
	
	require("../includes/meta_head.php");
	
	require("../includes/header.php"); ?>
	
	
	<div id="corps">
		 
		<div id="bande_rubrique_g">
			<div id="bande_image">
				<img src="../template/base/compte/icones/parrainage.png" alt="inf_commande" />
			</div>
			<div id="bande_titre">Offre parrainage</div>
			<div id="bande_sous_titre">Parrainer des amis et profiter de nombreux avantages</div>
		</div>   
		<div id="bande_rubrique_d"><a href="account.php" title="Retourner � mon compte"><img src="../template/base/compte/bouton_retour.png" alt="retour" /></a></div>
		<div class="clear"></div>
		
		<div style="text-align:left;">
		
			<div style="height:50px; margin-bottom:15px; width:100%;">
				<div style="float:left;"><img src="../template/base/compte/icone_parrainage/iconeparrainage2.png" alt="parrainage" /></div>
				<div style="float:left; background-color:#B7B700; margin-top:10px; height:22px; width:920px; padding-top:8px;">
					<span style="margin-left:10px;">Parrainer de nouveaux filleuls</span>
				</div>
			</div>
			<div class="clear"></div>
			
			<div>&#9658;&nbsp; <a href="../faq.php?Faq_Fam=78" style="text-decoration:none; color:<?php echo COULEUR_1; ?>;">Qu'est ce que le parrainage ?</a></div>
			<div>&#9658;&nbsp; <a href="../faq.php?Faq_Fam=78&Faq_id=63" style="text-decoration:none; color:<?php echo COULEUR_1; ?>;">Conditions g&eacute;n&eacute;rale d'utilisation</a></div>
			<div>&#9658;&nbsp; <a href="../faq.php?Faq_Fam=78&Faq_id=64" style="text-decoration:none; color:<?php echo COULEUR_1; ?>;">En tant que parrain tu ne feras point ...</a></div>
			
			
			<?php
	
	$today = date("Y-m-d H:i:s");
	
	if($_POST['envoi_p']=='parrainer') {
		
		$err = '';
		$verif = true;
		
		for($i=0 ; $i<5 ; $i++) {
			
			$verif_mail = "!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
			
			if((strlen($_POST['mail'. $i])>0 || strlen($_POST['nom'. $i])>0) && preg_match($verif_mail, $_POST['mail'. $i])) {
			
				if(strlen($_POST['nom'. $i])>0 && strlen($_POST['mail'. $i])>0) {
					
					$verif_email_filleul_exist_client = tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['mail'.$i]."'");
					$nb_res_cust = tep_db_num_rows($verif_email_filleul_exist_client);
					
					if($nb_res_cust!=0) {
						
						$err .= "L'adresse : ".$_POST['mail'.$i]." fait d&eacute;j&agrave; partie des clients.<br />";
						$verif = false;
					}
					else {
					
					$verif_email_filleul_exist = tep_db_query("select * from ".TABLE_PARRAINAGE." where mail_filleul='".$_POST['mail'.$i]."'");
					$nb_res = tep_db_num_rows($verif_email_filleul_exist);				
					
						if($nb_res != 0) {
							$err .= "L'adresse : ".$_POST['mail'.$i]." fait d&eacute;j&agrave; l'objet d'une demande de parrainage.<br />";
							$verif = false;
						}
					}
				}
				elseif(strlen($_POST['mail'. $i]) == 0) {
					
					$err .= 'Veuillez rentrer une adresse mail.<br />';
					$verif = false;
				}
				elseif(strlen($_POST['nom'. $i]) == 0) {
					
					$err .= 'Veuillez remplir le champ Nom / Pr�nom.<br />';
					$verif = false;
				}
			}
			elseif(strlen($_POST['nom'. $i])>0 && strlen($_POST['mail'. $i])==0) {
				
				$err .= 'Veuillez rentrer une adresse mail.<br />';
				$verif = false;
			}
			elseif(strlen($_POST['mail'. $i])>0) {
				
				$err .= 'Veuillez rentrer une adresse mail valide.<br />';
				$verif = false;
			}
		}
		
		
		
		if(!$verif) {
			
			echo ' <div class="haut_erreur" style="margin-top:20px;"></div>
				   <div class="milieu_erreur" style="text-align:center;">'.$err.'</div>
				   <div class="clear"></div>  
				   <div class="bas_erreur"></div>';		
		}
		else{
			for($i=0;$i<5;$i++){
				if(strlen($_POST['nom'.$i])>0 && strlen($_POST['mail'.$i])>0) {
					
						$req_info_parrain=tep_db_query("select customers_type, customers_email_address, customers_gender, customers_firstname, customers_lastname from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['customer_id']."");
						$res_info=tep_db_fetch_array($req_info_parrain);
					
						$sujet=$res_info['customers_lastname']." ".$res_info['customers_firstname']." vous propose des avantages";
						$message_defaut="Bonjour<br /><br/>
										".strtoupper($res_info['customers_lastname'])." ".ucwords($res_info['customers_firstname'])." aimerait vous parrainer et
										vous faire d�couvrir le site intrenet : www.GENERALARMYSTORE.fr<br /><br />
										En passant votre premi�re commande sur notre site internet, vous profiterez d'un cadeau qui sera automatiquement ajout� � cette
										derni�re et ceci gr�ce � votre ami.<br /><br />
										Pour pouvoir profiter des avantages du parrainage il faut vous inscrire sur le site:  www.generalarmystore.fr en ";
						$message_defaut2="<br /><br />
										 Pour plus d'informations, vous pouvez consulter dans  la foire aux questions la rubrique correspondant au parrainage
										 en <a href='https://www.generalarmystore.fr/gas/faq.php?Faq_Fam=78' style='color:#aab41d; font-weight:bold;' >cliquant ici</a>
										 <br /><br/>
										 L��quipe de GENERAL ARMY STORE vous souhaite une tr�s bonne visite sur sa boutique.";
						
						if($res_info['customers_type']==1) {
							
							$url_parrainage = "<a href='https://www.generalarmystore.fr/gas/compte/create_account_part.php?email_parrain=".$res_info['customers_email_address']."' style='color:#aab41d; font-weight:bold;'>Cliquant ici</a>";
						}
						else {
							
							$url_parrainage = "<a href='https://www.generalarmystore.fr/gas/compte/create_account_part.php?email_parrain=".$res_info['customers_email_address']."' style='color:#aab41d; font-weight:bold;'>Cliquant ici</a>";
						}
						
						
						if($_POST['message']=='') {
							
							$mail=$_POST['mail'.$i];	
							$mess='email defaut bda';
							$messa=$message_defaut.$url_parrainage.$message_defaut2;
							mail_client($mail,$sujet,$messa);
						}
						else {
							
							$mail=$_POST['mail'.$i];
							$mess=$_POST['message'];
							$messa=$message_defaut.$url_parrainage.$message_defaut2."<br /><br />Message de votre ami : <br />".stripslashes($mess);
							mail_client($mail,$sujet,$messa);
						}
						
						
						$req_parrainage=tep_db_query("insert into ".TABLE_PARRAINAGE." (id_parrain, nom_filleul, mail_filleul, message_envoye, date_envoi, statut) values (".$_SESSION['customer_id'].", '".addslashes($_POST['nom'.$i])."', '".addslashes($_POST['mail'.$i])."', '".$mess."', '".$today."', 'non lu') ");
				}
			}
		unset($_POST);
		}
		
		
	}
	
	?>
	
			<div style="display:block; margin-top:15px; float:left;">
					<div  id="bordure_mdp_haut_verte">
						<p class="titre_info"><span>Comment parrainer ?</span></p>
					</div> 
					<div id="bordure_mdp_milieu_vert">
						<div style="padding:0px 0px 0px 40px;"> 	
								<div>Pour parrainer un ou plusieurs filleuls veuillez remplir les champs ci dessous.</div>
								<div style="float:left;">
									<div style="float:left; width:200px; height:30px; margin-left:20px; margin-top:15px; text-align:center;">
									Nom, pr&eacute;nom et/ou surnom</div>
									<div style="float:left; width:200px; height:30px;  margin-left:50px; margin-top:15px; text-align:center;">
											Adresse Email de votre filleul
									</div>
									<div class="clear"></div>
									<form action="sponsorship.php" method="post" name="p">                                
									<div style="float:left; width:200px; height:30px; margin-left:20px; text-align:center;">
										<input type="text" id="nom_filleul0" name="nom0" value="<?php echo $_POST['nom0']; ?>" size="30"  />
									</div>
									<div style="float:left; width:200px; height:30px; margin-left:50px; text-align:center;">
										<input type="text" id="mail_filleul0" name="mail0" value="<?php echo $_POST['mail0']; ?>" size="30"  />
									</div>
									<div class="clear"></div>
									<div style="float:left; width:200px; height:30px; margin-left:20px; text-align:center;">
										<input type="text" id="nom_filleul1" name="nom1" value="<?php echo $_POST['nom1']; ?>" size="30"  />
									</div>
									<div style="float:left; width:200px; height:30px; margin-left:50px; text-align:center;">
										<input type="text" id="mail_filleul1" name="mail1" value="<?php echo $_POST['mail1']; ?>" size="30"  />
									</div>
									<div class="clear"></div>
									<div style="float:left; width:200px; height:30px; margin-left:20px; text-align:center;">
										<input type="text" id="nom_filleul2" name="nom2" value="<?php echo $_POST['nom2']; ?>" size="30"  />
									</div>
									<div style="float:left; width:200px; height:30px; margin-left:50px; text-align:center;">
										<input type="text" id="mail_filleul2" name="mail2" value="<?php echo $_POST['mail2']; ?>" size="30"  />
									</div>
									<div class="clear"></div>
									<div style="float:left; width:200px; height:30px; margin-left:20px; text-align:center;">
										<input type="text" id="nom_filleul3" name="nom3" value="<?php echo $_POST['nom3']; ?>" size="30"  />
									</div>
									<div style="float:left; width:200px; height:30px; margin-left:50px; text-align:center;">
										<input type="text" id="mail_filleul3" name="mail3" value="<?php echo $_POST['mail3']; ?>" size="30"  />
									</div>
									<div class="clear"></div>
									<div style="float:left; width:200px; height:30px; margin-left:20px; text-align:center;">
										<input type="text" id="nom_filleul4" name="nom4" value="<?php echo $_POST['nom4']; ?>" size="30"  />
									</div>
									<div style="float:left; width:200px; height:30px; margin-left:50px; text-align:center;">
										<input type="text" id="mail_filleul4" name="mail4" value="<?php echo $_POST['mail4']; ?>" size="30"  />
									</div>
									<div class="clear"></div>
									<input type="hidden" value="parrainer" name="envoi_p"  />
									</form>
								</div>
								<div style="float:right; margin-top:-30px; margin-right:20px; height:40px; padding-top:20px;">
								<img src="../template/base/compte/programme_de_parrainage.png" alt="programme parrainage" title="programme parrainage"  />
								</div>
								<div class="clear"></div>
								
								<div style="margin-top:20px; width:520px; text-align:center;">
								<a href="javascript:document.p.submit();"><img src="../template/base/compte/boutons/bouton_envoyer-mon-parrainage.png" alt="valider parrainage" style="cursor:pointer;" /></a>
								<!-- <input type="button" id="valider_parain" name="valider" value="OK" onclick="envoi_parrainage();" />--></div>
								<div style="margin-top:10px; width:520px; text-align:center;">Pour parrainer plus de 50 filleuls � la fois,<br /> merci de nous contacter et nous le ferons pour vous.</div>
								<div style="margin-top:20px;">Vous avez la possibilit&eacute; d'envoyer un message personalis&eacute; en cochant cette case <input type="checkbox" id="check_text_parrain" onclick="affiche_zone();" /> sinon un message automatique sera envoy&eacute;</div>
								<div id="zone" style="display:none;"><br /><textarea id="text_parrain" name="message" rows="5" style="width:95%;"></textarea></div>
								</form>
						</div>
					</div>
					<div id="bordure_mdp_bas_verte"></div> 
				</div>
				<div class="clear"></div>
			
			 <div style="height:50px; margin-top:25px; width:100%;">
				<div style="float:left;"><img src="../template/base/compte/icone_parrainage/iconeparainage.png" alt="parrainage" /></div>
				<div style="float:left; background-color:#B7B700; margin-top:10px; height:22px; width:920px; padding-top:8px;">
					<span style="margin-left:10px;">Vos filleuls</span>
				</div>
			</div>
			<div class="clear"></div>
			
			<div id="filleul">
				<?php
					$req_filleul=tep_db_query("select * from ".TABLE_PARRAINAGE." where id_parrain=".$_SESSION['customer_id']."");
					$nb_filleul=tep_db_num_rows($req_filleul);
					
					if($nb_filleul==0) {
						
						echo "<div style='width:100%; color:red; font-size:15px; margin-top:10px; margin-bottom:15px; text-align:center;'>Vous n'avez parrain&eacute; encore personne</div>";
					}
					else{
				?>            
				<div style="float:left; border-color:<?php echo COULEUR_25; ?>; border-width:0px 0px 2px 0px; border-style:solid; height:25px; width:150px; text-align:center;">Filleul</div>
				<div style="float:left; border-color:<?php echo COULEUR_25; ?>; border-width:0px 0px 2px 0px; border-style:solid; height:25px; width:300px; text-align:center;">Etat</div>
				<div style="float:left; border-color:<?php echo COULEUR_25; ?>; border-width:0px 0px 2px 0px; border-style:solid; height:25px; width:200px; text-align:center;">Date d'envoi</div>
				<div style="float:left; border-color:<?php echo COULEUR_25; ?>; border-width:0px 0px 2px 0px; border-style:solid; height:25px; width:270px; text-align:center;">Relancer un filleul</div>
				<div style="float:left; border-color:<?php echo COULEUR_25; ?>; border-width:0px 0px 2px 0px; border-style:solid; height:25px; width:50px; text-align:center;">Annuler</div>
				<div class="clear"></div>            
				<?php
				
				while($res_filleul=tep_db_fetch_array($req_filleul)) { ?>

				<div style="margin-top:15px;">
					<div style="float:left; background-color:<?php echo COULEUR_24; ?>; padding-top:10px; height:25px; width:150px; text-align:center;">
						<?php echo $res_filleul['nom_filleul']; ?>
					</div>
					<div style="float:left; background-color:<?php echo COULEUR_24; ?>; padding-top:10px; height:25px; width:300px; text-align:center;">
						<?php
						if($res_filleul['statut']=='non lu') {
							
							echo 'N\'est pas encore inscrit'; 
						}
						elseif($res_filleul['statut']=='inscrit') {
							
							echo "Filleul inscrit ";     
						}
						elseif($res_filleul['statut']=='achete') {
							
							echo "Achat effectu�";
						}						
						?>
					</div>
					<div style="float:left; background-color:<?php echo COULEUR_24; ?>; padding-top:10px; height:25px; width:200px; text-align:center;">
						<?php echo dat($res_filleul['date_envoi']);?>
					</div>
					
					<div id="filleuls" style="float:left; background-color:<?php echo COULEUR_24; ?>; padding-top:10px; height:25px; width:270px; text-align:center;">
					<?php

						if($res_filleul['statut']=='non lu') {
							
							$today = date("Y-m-d H:i:s");
							$nbjours = round((strtotime($today) - strtotime($res_filleul['date_envoi']))/(60*60*24)-1);

							if($nbjours<0) {
								
								$nbjours=0;
							}
							elseif($nbjours>=15) {
								
								echo '<div style="margin-top:-3px;"><img style="cursor: pointer;" src="../template/base/boutons/bouton_relancer.png" alt="relancer filleul" onclick="relance_parrainage('.$res_filleul['id_parrainage'].');"/></div>';
							}
							else {
								
								echo "Vous pouvez le relancer dans ".(15-$nbjours)." jours";	
							}
						}
						elseif($res_filleul['statut']=='inscrit') {

							$req_info_filleul_date=tep_db_query("select customers_id from ".TABLE_CUSTOMERS." where customers_email_address='".$res_filleul['mail_filleul']."'");
							$res_info=tep_db_fetch_array($req_info_filleul_date);
							
							$num_filleul=$res_info['customers_id'];
							$date_filleul_inscrit=tep_db_query("select customers_info_date_account_created from ".TABLE_CUSTOMERS_INFO." where customers_info_id=".$num_filleul."");
							$res_date=tep_db_fetch_array($date_filleul_inscrit);
							
							echo "inscrit le ".dat($res_date['customers_info_date_account_created']);                        
						}
						elseif($res_filleul['statut']=='achete') {

							$req_info_filleul_date=tep_db_query("select customers_id from ".TABLE_CUSTOMERS." where customers_email_address='".$res_filleul['mail_filleul']."'");
							$res_info=tep_db_fetch_array($req_info_filleul_date);
							
							$num_filleul=$res_info['customers_id'];
							$date_filleul_achat=tep_db_query("select date_purchased from ".TABLE_ORDERS." where customers_id=".$num_filleul."");
							$res_date=tep_db_fetch_array($date_filleul_achat);
							
							echo "Achat effectu� le ".dat($res_date['date_purchased']);
						}
						?>
					</div>
					<div style="float:left; background-color:<?php echo COULEUR_24; ?>; padding-top:5px; padding-bottom:5px; height:25px; width:50px; text-align:center;">
					<?php                    
						if($res_filleul['statut']=='non lu') {
						?>
							<a href="<?php echo $_SERVER['PHP_SELF']; ?>?suppr_filleul=<?php echo $res_filleul['id_parrainage']; ?>"><img src="../template/base/panier/icone_supprimer.png" alt="image annuler" height="25"/></a>
						<?php
						}
					?>
					</div>
					<div class="clear"></div>
				</div>
				<?php
						}
				?>
				<div class="clear"></div>
				<div style="margin-top:15px; margin-left:15px;">
					Si votre filleul ne se manifeste pas et n'a pas accept&eacute; votre offre de parrainage vous avez la possibilit&eacute; de le relancer une fois tout les 15 jours.
				</div>
					  
				<div style="display:block; margin-top:15px; float:left;">
					<div  id="bordure_mdp_haut_verte">
						<p class="titre_info"><span>Comment relancer un filleul?</span></p>
					</div> 
					<div id="bordure_mdp_milieu_vert">
						<div style="padding:0px 0px 0px 40px;"> 	
							<div>&#9658;&nbsp; Cliquez sur le bouton "Relancer" correspondant au filleul souhait&eacute;</div>
							<div>&#9658;&nbsp; Un message de relance lui sera automatiquement envoy&eacute;</div>
							<div>&#9658;&nbsp; Vous serez pr&eacute;venu par mail quand la premi&egrave;re commande du filleul aura &eacute;t&eacute; enregistr&eacute;e</div>
						</div>
					</div>
					<div id="bordure_mdp_bas_verte"></div>
				</div>
				<div class="clear"></div>
			<?php } ?>
			</div>
		</div>
	</div>
	<?php  
}
else {
	
	header('Location: connexion.php');
}

require("../includes/footer.php");
require("../includes/page_bottom.php"); 
?>
