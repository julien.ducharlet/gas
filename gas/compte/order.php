<?php
/* Page listant les commandes du client */
  $nom_page = "order";
   
require("../includes/page_top.php");
include("fonction/session.php");

// Original avant modification
require("../includes/modules/mod_paiement_spplus.php");
// require("../includes/modules/mod_paiement_spplus_reesayer.php");

if(!empty($_SESSION['customer_id'])){
	require("../includes/meta_head.php");

$_POST['valide'] = !empty($_POST['valide']) ? $_POST['valide'] : '';
if ($_POST['valide']=="ok") {
	$siret=SIRET_SPPLUS;
	$id_magasin=ID_MAGASIN_SPPLUS;
	$_SESSION['total_panier']=format_to_money($_POST['montant']);
	echo lien_paiement_spplus($id_magasin,$siret,$_POST['commande']);
}
$req_order=tep_db_query("SELECT 
						o.orders_id as od, 
						o.customers_id, 
						
						o.delivery_name,
						o.delivery_company,
						o.delivery_street_address,
						o.delivery_suburb,
						o.delivery_city,
						o.delivery_postcode,
						o.delivery_country,
						
						date_purchased, 
						payment_method, 
						total, 
						orders_status os, 
						orders_status_name, 
						orders_numero_facture, 
						o.etat_paiement,
						
						c.countries_name,
						c.countries_iso_code_2
						
						FROM 
							".TABLE_ORDERS." o, 
							".TABLE_ORDERS_STATUS." os,
							countries c
						WHERE 
							o.orders_status=os.orders_status_id AND
							o.orders_status<>'21' AND
							o.customers_id=".$_SESSION['customer_id']." AND
							o.delivery_country = c.countries_name
						ORDER BY date_purchased desc");

$nb_com=tep_db_num_rows($req_order);

require("../includes/header.php"); ?>

<div id="corps">

    <div id="bande_rubrique_g">
        <div id="bande_image">
            <img src="../template/base/compte/icones/commande.png" alt="inf_commande" />
        </div>
        <div id="bande_titre">Mes commandes</div>
        <div id="bande_sous_titre">
         <?php
		 	if($nb_com!=0)
			{echo"Cliquez sur le num&eacute;ro d'une commande pour voir le d&eacute;tail";}			
			else{echo"Vous n'avez pas de commandes sur notre site";}
		 ?>
        </div>
    </div>
    <div id="bande_rubrique_d">
    <a href="account.php" title="Retourner � mon compte"><img src="../template/base/compte/bouton_retour.png" alt="retour" /></a>
    </div>
	<div style="clear:both;"></div>

 <?php
if($nb_com!=0)
	{
?>
<div id="info_com_gene"> 
	Vous avez <?php echo $nb_com;?> commande<?php if($nb_com>1) echo's'; ?> sur notre site
</div>
<div style="clear:both;"></div>


<div id="commande_barre_info">
	<div class="item_num_com"><strong>N� commande</strong></div>
    <div class="item_date_com"><strong>Date</strong></div>
    <div class="item_paiement_com"><strong>Paiement</strong></div>
    <div class="item_montant_com"><strong>Montant TTC</strong></div>
    <div class="item_statut_com"><strong>Statut</strong></div>
    <div class="item_facture_com"><strong>Facture</strong></div>
    <div class="item_suivi_com"><strong>Suivi</strong></div>
    <div style="clear:both;"></div>
</div>
<?php

		$i=0;
		while($res=tep_db_fetch_array($req_order)) {
			
			$classe = ($i%2 == 0) ? 1 : 2;
			
			echo'<div  class="ligne_commade_info'. $classe .'">
					 <div class="item_num_com"><a href="order_detail.php?id_com='. $res['od'] .'" title="Voir le d�tail de cette commande">'. $res['od'] .'</a></div>
					 <div class="item_date_com">'. dat($res['date_purchased']). '</div>
					 <div class="item_paiement_com">&nbsp;';
					 
			$mode_reglement = ' '.$res['payment_method'];
					 
			if(strpos($mode_reglement,'que')) echo 'Ch&egrave;que';
			elseif(strpos($mode_reglement,"Carte B")) echo "Carte Bancaire";
			elseif(strpos($mode_reglement,"PayPal")) echo "PayPal";
			elseif(strpos($mode_reglement,"Virement")) echo "Virement Bancaire";
			elseif(strpos($mode_reglement,"Porte")) echo "Porte monnaie virtuel";
			elseif(strpos($mode_reglement,"Mandat Cash")) echo "Mandat Cash";
			elseif(strpos($mode_reglement,"Mandat Administratif")) echo "Mandat Administratif";
            
			elseif($res['payment_method']=="") echo "Attente paiement";
				
					 
			$req_date = tep_db_query("Select date_added from orders_status_history where orders_id=". $res['od'] ." order by date_added desc");
			$res_date = tep_db_fetch_array($req_date);
					
			echo'</div>
			<div class="item_montant_com"><strong>'. $res['total'] .' �</strong></div>
			<div class="item_statut_com">';
					 
				if($res['orders_status_name']=="Colis envoy�") {
					echo'<span class="item_statut_comOK">'. $res['orders_status_name'] .'</span> ('. dat($res_date['date_added']) .')';
				} elseif ($res['orders_status_name']=="Commande Annul�e") {
					echo'<span class="item_statut_comNO">'. $res['orders_status_name'] .'</span>';
				} elseif ($res['orders_status_name']=="En attente paiement CB") {
					echo'<span class="item_statut_comNO">'. $res['orders_status_name'] .'</span>';
				} else{ echo $res['orders_status_name']; }
			echo'</div>
					 <div class="item_facture_com">';
						if($res['orders_numero_facture']!=0) {
							
							 echo'<a href="generation_pdf/facture.php?id_com='. $res['od'] .'&cli='. md5($res['customers_id']) .'" target="_blank" title="Votre facture au format pdf">
							 		<img src="../template/base/icones/icone_pdf.png" alt="facture pdf"   border="none"/>
								  </a>';
						 }
						 else echo'-';
						 
					 echo'</div>
					<div class="item_suivi_com2">';
				
					$req_suivi=tep_db_query("SELECT track_num, track_num2, track_num3, track_num4, track_num5
											FROM orders_status_history
											WHERE  orders_id=".$res['od']."
												AND (track_num <>'' OR track_num2 <>'' OR track_num3<>'' OR track_num4<>'' OR track_num5<>'')");
					$nb_suivf=tep_db_num_rows($req_suivi);
					
					
					   if($nb_suivf==0){
						echo"-";
					   }
					   else{
						while($res_sv=tep_db_fetch_array($req_suivi)) {
							
							 if($res_sv['track_num']!=''){
									// OK
                                 echo '<a href="https://www.laposte.fr/outils/suivre-vos-envois?code='.$res_sv['track_num'].'" target="_blank" title="num&eacute;ro de suivi colissimo">'.$res_sv['track_num'].'</a><br/>';
							 }
							
							if($res_sv['track_num2']!=''){
                                    // OK
									echo '<a href="https://www.laposte.fr/outils/suivre-vos-envois?code='.$res_sv['track_num2'].'" target="_blank" title="num&eacute;ro de suivi courrier">'.$res_sv['track_num2'].'</a><br/>';
								
							}
								
							if($res_sv['track_num3']!=''){
                                    // OK
									echo '<a href="https://www.laposte.fr/outils/suivre-vos-envois?code='.$res_sv['track_num3'].'" target="_blank" title="num&eacute;ro de suivi chronopost">
										'.$res_sv['track_num3'].'
										 </a><br/>';
							}
							
							if($res_sv['track_num4']!='') {
								// TO TEST
								echo '<a href="https://www.dpd.fr/traces_exd_'.$res_sv['track_num4'].'" target="_blank" title="num&eacute;ro de suivi GLS">
										'.$res_sv['track_num4'].'
										 </a><br/>';
							}
							
							if($res_sv['track_num5']!='') {
								// OK
								echo '<a href="https://www.ups.com/WebTracking?loc=fr_fr&requester=ST&trackingNumber='.$res_sv['track_num5'].'" target="_blank" title="num&eacute;ro de suivi UPS">
										'.$res_sv['track_num5'].'</a><br/>';
							}
						}
					}
					
				   echo'</div>
				   <div style="clear:both;"></div>';
				   if($res['os']==27){
					echo'<div style="margin-top:10px; font-weight:bold;font-size:13px;">
						 <form method="post" action"'.$_SERVER['PHP_SELF'].'" name="paybis">';
					echo ' Votre paiement par carte bancaire n\'a pas &eacute;t&eacute; pris en compte ! 
					      <input type="hidden" name="valide" value="ok" />
						  <input type="hidden" name="commande" value="'.$res['od'].'" />
						  <input type="hidden" name="montant" value="'.$res['total'].'" />
						  <a href="javascript:document.forms[\'paybis\'].submit(); " title="Connectez vous">Cliquez ici pour r�essayer</a>';
					echo'</form>
						 </div>';
					}
					
					if($res['os']==36){
					echo'<div style="margin-top:10px; font-weight:bold;font-size:13px;">
						 <form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="paypalbis">';
					echo ' Votre paiement par PayPal n\'a pas &eacute;t&eacute; pris en compte !';
					
					echo '									
<input type="hidden" value="'.$res['total'].'" name="amount" />
<input name="currency_code" type="hidden" value="EUR" />
<input name="shipping" type="hidden" value="0.00" />
<input name="tax" type="hidden" value="0.00" />
<input name="return" type="hidden" value="https://www.generalarmystore.fr/gas/panier_remerciement.php" />
<input name="cancel_return" type="hidden" value="https://www.generalarmystore.fr/gas/compte/order.php" />
<input name="notify_url" type="hidden" value="https://www.generalarmystore.fr/gas/paypal-7dql9apmn7idm5qgdr5u3qposa3i2c7e.php" /> 
<input name="address_override" type="hidden" value="1" />

<input name="first_name" type="hidden" value="'. $res['delivery_company'] .'">
<input name="last_name" type="hidden" value="'. $res['delivery_name'] .'">
<input name="address1" type="hidden" value="'. addslashes($res['delivery_street_address']) .'">
<input name="address2" type="hidden" value="'. addslashes($res['delivery_suburb']) .'">


<input name="city" type="hidden" value="'. $res['delivery_city'] .'">
<input name="zip" type="hidden" value="'. $res['delivery_postcode'] .'">
<input name="country" type="hidden" value="'. $res['countries_iso_code_2'] .'">
		
<input name="cmd" type="hidden" value="_xclick" />
<input name="business" type="hidden" value="compta@generalarmystore.fr" />
<input name="item_name" type="hidden" value="Commande General Army Store '.$res['od'].'" />
<input name="no_note" type="hidden" value="1" />
<input name="lc" type="hidden" value="FR" />
<input name="bn" type="hidden" value="PP-BuyNowBF" />
<input name="custom" type="hidden" value="'.$res['od'].'" />
<a href="javascript:document.forms[\'paypalbis\'].submit(); " title="Payer de nouveau avec PayPal">Cliquez ici pour r�essayer</a>';
							
					echo'</form>
						 </div>';
					}
					
				echo '</div>';
				
				$i++;
		}
}
else{
?>  
    <div style="background-image:url(../template/base/compte/image_no-cmd.png); width:716px; height:176px;"></div><br /><br /><br />
    <div style="clear:both;"></div>    
<?php } ?>
</div>
<?php 
}
else
{ header('Location: connexion.php');}

require("../includes/footer.php");
require("../includes/page_bottom.php"); 
?>
