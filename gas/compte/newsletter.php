<?php
/* Auteur : Sami 
   Page permattant au client d'avoir une newsletter apropri�e � sa demande*/
   
$nom_page = "newsletter";
   
require("../includes/page_top.php");

include("fonction/session.php");

 
if(!empty($_SESSION['customer_id'])) {
	
	require("../includes/meta_head.php");
	
$req_info=tep_db_query("select * from ".TABLE_NEWSLETTER_INFOS." where customers_id=".$_SESSION['customer_id']."");
$res1=tep_db_fetch_array($req_info);

$req_customer=tep_db_query("select customers_newsletter, customers_newsletter_inscription, customers_newsletter_partenaire from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['customer_id']."");
$resu2=tep_db_fetch_array($req_customer);

require("../includes/header.php"); ?>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/compte/fonction/newsletter.js"></script>
<div id="corps">

    <div id="bande_rubrique_g">
        <div id="bande_image">
            <img src="../template/base/compte/icones/courrier.png" alt="inf_commande" />
        </div>
        <div id="bande_titre">Newsletter</div>
        <div id="bande_sous_titre">
         Tenez vous au courant des nouveaut&eacute;s de la boutique des accessoires
        </div>
    </div>
    
    <div id="bande_rubrique_d">
    	<a href="account.php"><img src="../template/base/compte/bouton_retour.png" alt="retour" /></a>
    </div>
	<div style="clear:both;"></div>
                           
    <div style="width:100%; height:100%;"> 
		<form id="pipo" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" name="nwl">
           <?php
		   		if(isset($_POST['valider']) && $_POST['valider']=="ok"){
					 if($_POST['general_news']=='') $news=0; else $news=1;
					 if($_POST['partenariat']=='') $part=0; else $part=1;
					 //if($_POST['abo_news']=='') $abo=1; else $abo=0
					 $abo=1;
					 $req=tep_db_query("update ".TABLE_CUSTOMERS." set customers_newsletter='".$news."',
									   						 customers_newsletter_partenaire='".$part."',
															 customers_newsletter_inscription='".$abo."'
														 where customers_id=".$_SESSION['customer_id']."");
					 
					 /*
					 if($_POST['promos']=='') $promo=0; else $promo=1;
					 if($_POST['news']=='') $nouveaute=0; else $nouveaute=1;*/
					 $promo=0;
					 $nouveaute=0;
					 $frequence=$_POST['freqs']; 
					 
					 $verif_client=tep_db_query("select news_id from ".TABLE_NEWSLETTER_INFOS." where customers_id=".$_SESSION['customer_id']."");
					 $id_verif=tep_db_fetch_array($verif_client);
					 $cli=tep_db_num_rows($verif_client);
					
					 if($cli==0){
						$insert=tep_db_query("insert into ".TABLE_NEWSLETTER_INFOS." (customers_id, frequence_news, promo_news, nouveaute_news) 
																	 values (".$_SESSION['customer_id'].",".$frequence.",'".$promo."','".$nouveaute."')");
					 }
					 else{
						$update=tep_db_query("update ".TABLE_NEWSLETTER_INFOS." set frequence_news=".$frequence.",
																		  promo_news='".$promo."',
																		  nouveaute_news='".$nouveaute."'
																	  where customers_id=".$_SESSION['customer_id']."");
							}
				}
		   	 ?>             
             <div style="display:block; float:left;">
                     <div  id="bordure_mdp_haut_verte"></div> 
                     <div id="bordure_mdp_milieu_vert">
                        <div style="padding:0px 0px 0px 0px;"> 	
                         <span>Recevoir la newsletter g&eacute;n&eacute;rale : <input type="checkbox" id="gene" name="general_news" value="1" <?php if(isset($_POST['valider']))
																															{if($_POST['general_news']=='1') 
																															 echo'checked="checked"';}
																														 else if($resu2['customers_newsletter']==1)
																														echo 'checked="checked"'; ?>/>
                         </span>
                         <span style="margin-left:30px;">                                                                                                                        
                         Recevoir les promotions de nos partenaires : <input type="checkbox" id="part" name="partenariat" value="1" <?php if(isset($_POST['valider']))
																															{if($_POST['partenariat']=='1') 
																															 echo'checked="checked"';}
																														 else if($resu2['customers_newsletter_partenaire']==1)
																														echo 'checked="checked"'; ?> />	
                         </span >
                         <span style="margin-left:30px; font-size:10px; color:<?php echo COULEUR_18; ?>;">Nous envoyons seulement un email tous les 15 jours</span>
                        </div>
                     </div>
                   <div id="bordure_mdp_bas_verte"></div> 
             </div>
             <div class="clear"></div>
             
             <div style="display:block; margin-top:15px; float:left;">
                  <div  id="bordure_mdp_haut_verte"></div> 
                  <div id="bordure_mdp_milieu_vert">
                     <div style="padding:0px 20px 0px 20px;"> 	
                      <div><strong>Indiquez la fr&eacute;quence &agrave; laquelle vous souhaitez recevoir nos emails :</strong></div>
                      <div style=" margin-top:10px; margin-bottom:10px;">
                      <div style="float:left; margin-left:320px; margin-top:2px;">
                     	 <?php $freq=array('Un message par semaine','Un message toutes les deux semaines'); ?>
           				 <select id="freq" name="freqs">
							 <?php for($i=1;$i<count($freq)+1;$i++){ ?>
                                <option <?php
                                     if(isset($_POST['valider'])){
                                      if($_POST['freqs']==$i) 
                                      echo 'selected="selected"';
                                     }
                                      else if($res1['frequence_news']==$i)
                                            echo 'selected="selected"';
                                    ?> value="<?php echo $i; ?>"><?php echo $freq[$i-1]; ?></option>
                             <?php } ?>
                       </select>
                       <input type="hidden" name="valider" value="ok" />
                       </div>
                       <div style="float:left; margin-left:10px;">
                        <!--<input type="submit" value="valider" name="valider"  />-->
                        <img src="../template/base/boutons/bouton_enregistrer_mon_choix.png" style="cursor:pointer;" onclick="document.nwl.submit();" alt="ajouter une selection" title="ajouter une selection a la newsletter" /> 
                       </div>
                       <div class="clear"></div>
                        
                      </div>
                      <div ><hr style="color:#99cc00; width:100%;" /></div>
                      <div><strong>Choisissez les produits que vous poss&eacute;dez, afin d'&ecirc;tre inform&eacute; des derni&egrave;res nouveaut&eacute;es.</strong></div>
                      <div style="margin-top:10px;">
                      <div style="float:left; margin-left:120px; margin-top:2px;">
                       Votre produit :                       
                        <select name="rayon" id="rayon<?php echo $i; ?>" onchange="rayon_fc(<?php echo $i; ?>);">
                        <option value="rayon">Choisissez un rayon</option>
                         <?php
                            $reponse = tep_db_query("SELECT rubrique_name, rubrique_id
                                                     FROM rubrique
													 WHERE rubrique_status='1'
                                                     ORDER BY rubrique_id");
                             while($donnees = tep_db_fetch_array($reponse)){
                          ?>
                           <option value="<?php echo $donnees['rubrique_id']; ?>">
                                <?php echo $donnees['rubrique_name']; ?>
                           </option>
                         <?php } ?>
                        </select>
                        <select name="marque" id="marque<?php echo $i; ?>" onchange="marque_fc(<?php echo $i; ?>);" disabled="disabled">
                        <option value="marque">Choisissez une marque</option>
                        </select>
                        <select name="modele" id="modele<?php echo $i; ?>" disabled="disabled">
                        <option value="modele">Choisissez un mod&egrave;</option>
                        </select>
                        <!--<input type="button" name="envoyer"  onclick="enregistrer_fc(<?php echo $i; ?>);" value="ajouter un produit" />-->
						</div>
                        <div style="float:left; margin-left:10px;">
                        	<img src="../template/base/boutons/bouton_enregistrer_ma_selection.png" style="cursor:pointer;" onclick="enregistrer_fc(<?php echo $i; ?>);" alt="ajouter une selection" title="ajouter une selection a la newsletter" /></div>
                        <div class="clear"></div>
                      </div>          
                     </div>
                  </div>
                <div id="bordure_mdp_bas_verte"></div> 
            </div>
            <div class="clear"></div>
             <!--
             Ne plus jamais recevoir de newsletter de notre part   :
             <input type="checkbox" id="abo" name="abo_news" value="1" <?php if(isset($_POST['valider']))
																			{if($_POST['abo_news']=='1') 
																				 echo'checked="checked"';}
																			 else if($resu2['customers_newsletter_inscription']==0)
																			 echo 'checked="checked"'; ?>/>
            
            <br />
            <br />
            
            
            <br />
            Choix d'envoi : <input type="checkbox" id="promo" name="promos" value="1" <?php if(isset($_POST['valider']))
																							  {if($_POST['promos']=='1') 
																								 echo'checked="checked"';}
																								 else if($res1['promo_news']==1)
																								 echo 'checked="checked"'; ?> />promotions
                                                                                                 
                    <input type="checkbox" id="nouv" name="news" value="1" <?php if(isset($_POST['valider']))
																					{if($_POST['news']=='1') 
																					 echo'checked="checked"';}
																					 else if($res1['nouveaute_news']==1)
																					 echo 'checked="checked"'; ?> />nouveaut&eacute;
      
             
            
            <br />
            <br /> -->       
      </form>    
          
          <div style="width:100%; height:25px; padding-top:10px; border-width:0px 0px 2px 0px; border-style:solid; border-color:#99cc00;">
          	<div style="width:237px; height:10px; float:left;">Rayon</div>
            <div style="width:237px; height:10px; float:left;">Marque</div>
            <div style="width:237px; height:10px; float:left;">Mod&egrave;le</div>
            <div style="width:237px; height:10px; float:left;">Action</div>
            <div class="clear"></div>
      	  </div>
          <div id="block_produit" style=" margin-bottom:30px; width:100%;">
          <?php
			if($res1['news_id']!=''){				
				$req_prod=tep_db_query("Select *
										 From ".TABLE_NEWSLETTER_PRODUCTS."
										 Where newsletter_id=".$res1['news_id']."");
				$nb_prod=tep_db_num_rows($req_prod);
				
				if($nb_prod==0){
					echo"Vous n'avez pas de produit atitr� pour votre newsletter";
				}
				else{				
				$i=1;
				while($res2=tep_db_fetch_array($req_prod))
				{
					
                        $reponse = tep_db_query("SELECT rubrique_name, rubrique_id
                                                 FROM ".TABLE_RAYON."
                                                 WhERE rubrique_id=".$res2['rayon_id']);
                        $donnees = tep_db_fetch_array($reponse);
                      
				?>
                
                <input type="hidden" id="rayon_e<?php echo $i; ?>" value="<?php echo $donnees['rubrique_id']; ?>" />                
                <div style="width:237px; padding-top:15px; border-width:0px 0px 2px 0px; border-style:solid; border-color:#99cc00; background-color:#e3e3e3; height:30px; float:left;"><?php echo $donnees['rubrique_name']; ?></div>
                
                <?php 
					$reponse2 = tep_db_query("SELECT cd.categories_name, cd.categories_id
                                  FROM ".TABLE_CATEGORIES." as c inner join categories_description as cd on c.categories_id=cd.categories_id
                                       inner join categories_rubrique as cr on c.categories_id=cr.categories_id
                                  WHERE c.parent_id=0 AND c.sort_order > 0 AND cr.rubrique_id = " . $res2['rayon_id'] . " 
                                  ORDER BY c.sort_order");
					$verif=false;
                    while(($donnees2 = tep_db_fetch_array($reponse2)) && $verif==false){
						if($res2['marque_id']==$donnees2['categories_id']){
						 $val=$donnees2['categories_name'];
						 $val_id=$res2['marque_id'];
						 $verif=true;
						 }
						 else{
							 $val="";
							 $val_id="";
							 $verif=false;
						 }
					 } 
				?>
                 <input type="hidden"  id="marque_e<?php echo $i; ?>" value="<?php echo $val_id; ?>" />
                 <div style="width:237px; padding-top:15px; border-width:0px 0px 2px 0px; border-style:solid; border-color:#99cc00; height:30px; float:left;"><?php echo $val; ?></div>
                     <?php
                       
                        $modele_query = tep_db_query("SELECT cd.categories_name, cd.categories_id
                                          FROM ".TABLE_CATEGORIES." as c inner join categories_description as cd on c.categories_id=cd.categories_id
                                          WHERE c.parent_id='" . $res2['marque_id'] . "'
                                          ORDER BY cd.categories_name");
                        $verif2=false;
                        while(($modele_data = tep_db_fetch_array($modele_query)) && $verif2==false){
                         if($res2['modele_id']==$modele_data['categories_id']){
                            $value=$modele_data['categories_name'];
                            $value_id=$res2['modele_id'];
                            $verif2=true;
                         }
                         else{
                              $value="";
                              $value_id="";
                              $verif2=false;
                         }
                        }					
                    ?>
               <input type="hidden" id="modele_e<?php echo $i; ?>" value="<?php echo $value_id; ?>" />
               <div style="width:240px; padding-top:15px; border-width:0px 0px 2px 0px; border-style:solid; border-color:#99cc00; background-color:#e3e3e3; height:30px; float:left;"><?php echo $value; ?></div>
                                 
				<div style="width:255px; padding-top:15px; border-width:0px 0px 2px 0px; border-style:solid; border-color:#99cc00; height:30px; float:left;">
                	<!--<input type="button" value="supprimer" onclick="supprime_fc(<?php echo $i; ?>)" />-->
                    <div style="margin-top:-5px;">
                    	<img src="../template/base/boutons/bouton_supprimer.png" style="cursor:pointer;" onclick="supprime_fc(<?php echo $i; ?>)" alt="supprimer une selection" title="supprimer une selection" />
                    </div>
                </div>
                <div class="clear"></div>
				<?php
				$i++;
			 	}				
			   }
			}
			else{
				echo"Vous n'avez pas de produit atitr� pour votre newsletter";
			}
		  ?>
    </div>
    <div style="clear:both;"></div>	
</div>
</div>
<?php  
}
else
{ header('Location: connexion.php');}
require("../includes/footer.php");
require("../includes/page_bottom.php"); 

?>
