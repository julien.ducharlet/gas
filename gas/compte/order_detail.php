<?php
/* Page affichant le detail d'une commande */

$nom_page = "order_detail";
   
require("../includes/page_top.php");

include("fonction/session.php");

if(!empty($_SESSION['customer_id'])) {
	
require("../includes/meta_head.php");

$id_commande = (int)$_GET['id_com'];

$commande_query = tep_db_query("SELECT * 
									FROM ".TABLE_ORDERS." 
									WHERE orders_id = '" . $id_commande . "'");
$commande_data = tep_db_fetch_array($commande_query);

if($_SESSION['customer_id']==$commande_data['customers_id']){
	
/* récupération des articles de la commande */
$articles_query = tep_db_query("SELECT orders_products_id, products_model, products_name, final_price, products_quantity , cpath
									FROM ".TABLE_ORDERS_PRODUCTS." 
									WHERE orders_id = '" . $id_commande . "'");
$nbArticles = tep_db_num_rows($articles_query);

/* récupération des statuts de la commande */
$history_command = tep_db_query("SELECT date_added, comments, track_num, track_num2,  track_num3, track_num4, track_num5, orders_status_name, o.orders_status_id
									FROM ".TABLE_ORDERS_STATUS_HISTORY." os, ".TABLE_ORDERS_STATUS." o
									WHERE os.orders_status_id=o.orders_status_id
										  and orders_id = '" . $id_commande . "'
										  and  os.orders_status_id=4");
$nbcolis_send = tep_db_num_rows($history_command);

require("../includes/header.php"); 

?>
<div id="corps">
<div id="bande_rubrique_g">
   		<div id="bande_image">
            <img src="../template/base/compte/icones/commande.png" alt="inf_commande" />
        </div>
        <div id="bande_titre">Votre commande N&deg; : <span style="color:<?php echo COULEUR_28; ?>;"><strong><?php echo $id_commande;?></strong></span></div>
        <div id="bande_sous_titre">Le d&eacute;tail de la commande</div>
</div>
<div id="bande_rubrique_d">
    <a href="order.php" title="Retourner &agrave; mon compte"><img src="../template/base/boutons/bouton_retour-cmd.png" alt="retour" /></a>
</div>
	<div style="clear:both;"></div>
<!-- entete -->
<div id="block_info">
	<div class="block_bordure_violette"></div>
    <div style=" float:left; text-align:left; margin-top:10px; margin-left:10px; width:85%; font-size:0.9em;">
    	Num&eacute;ro de commande : <span style="color:<?php echo COULEUR_28; ?>;"><strong><?php echo $id_commande;?></strong></span><br><br>
        Commande du :<br>
        <strong>
			<?php 
                $timestamp_commande = strtotime($commande_data['date_purchased']);
                echo $date_commande = html_entity_decode(date_fr($timestamp_commande));
            ?>
        </strong>
        <br><br>
        Mode de r&egrave;glement :
       <strong>
		   <?php
            $mode_reglement = ' '.$commande_data['payment_method'];
			if (strpos($mode_reglement,'que')) {
				echo 'ch&egrave;que';
			} elseif (strpos($mode_reglement,"Carte B")) {
				echo "Carte Bancaire";
			} elseif (strpos($mode_reglement,"PayPal")) {
				echo "Paypal";
			} elseif (strpos($mode_reglement,"Virement")) {
				echo "Virement Bancaire";
			} elseif (strpos($mode_reglement,"Porte")) { 
				echo "<span style='font-size:0.9em;'>Porte monnaie virtuel</span>";
			}
            ?>
        </strong><br>
		<br>
        <?php 
		$id_facture = $commande_data['orders_numero_facture'];
		if ($id_facture!=0) {
		?>
			Facture N&deg;
			<strong>
				<a style=" color:#C33;" href="generation_pdf/facture.php?id_com=<?php echo $id_commande .'&cli='. md5($commande_data['customers_id']); ?>" target="_blank" title="Votre facture au format PDF">
					<?php echo $id_facture; ?>
				</a>
			</strong>
			<br>
			Du :
			<strong>
				<?php
				$timestamp_facture = strtotime($commande_data['orders_date_facture']);
				echo $date_facture = html_entity_decode(date_fr($timestamp_facture));
				?>
			</strong><br>
			<br>
        <?php 
		} 
		?>
    </div>
</div>

<div id="block_info_facturation">
	<div class="block_bordure_verte"></div>
    <div>
    <?php
		echo'<div style="float:left; text-align:left; margin-left:10px; margin-top:10px;"><strong>Adresse de facturation</strong> </div>';
		echo'<div style="float:left; text-align:left; margin-left:20px; margin-top:10px; width:80%;">';
		

		if (!empty($commande_data['customers_company'])) {
			echo $commande_data['customers_company'] . '<br>';
		}		
		echo $commande_data['customers_name'] . '<br>';
		if (!empty($commande_data['customers_street_address_3'])) {
			echo $commande_data['customers_street_address_3']. "<br>";
		}
		if (!empty($commande_data['customers_suburb'])) {
			echo $commande_data['customers_suburb']. "<br>";
		}
		if (!empty($commande_data['customers_street_address'])) {
			echo $commande_data['customers_street_address']. "<br>";
		} 
		if (!empty($commande_data['customers_street_address_4'])) {
			echo $commande_data['customers_street_address_4']. "<br>";
		}
		echo $commande_data['customers_postcode'] . '&nbsp;&nbsp;';
		echo $commande_data['customers_city'] . '<br>';
		echo $commande_data['customers_country'];
		echo'</div><div style="clear:both;"></div>';
	 ?>
    </div>
</div>
<div id="block_info_livraison">
	<div class="block_bordure_violette"></div>
		<div><?php
			echo '<div style="float:left; text-align:left; margin-left:10px; margin-top:10px;"><strong>Adresse de livraison</strong> </div>';
			echo '<div style="float:left; text-align:left; margin-left:20px; margin-top:10px; width:80%;">';
			
			if (!empty($commande_data['delivery_company'])) {
				echo $commande_data['delivery_company'] . '<br>';
			}	
			echo $livraison_nom = $commande_data['delivery_name'] . '<br>';
			if (!empty($commande_data['delivery_street_address_3'])) {
				echo $commande_data['delivery_street_address_3'] . '<br>';
			}
			if (!empty($commande_data['delivery_suburb'])) {
				echo $commande_data['delivery_suburb'] . "<br>";
			}
			if (!empty($commande_data['delivery_street_address'])) {
				echo $commande_data['delivery_street_address'] . '<br>';
			} 
			if (!empty($commande_data['delivery_street_address_4'])) {
				echo $commande_data['delivery_street_address_4'] . '<br>';
			}
			echo $commande_data['delivery_postcode'] . '&nbsp;&nbsp;';
			echo $livraison_ville = $commande_data['delivery_city'] . '<br>';
			echo $livraison_pays = $commande_data['delivery_country'];
			echo'</div><div style="clear:both;"></div>';
			
			$req_id_pays=tep_db_query("select countries_id from countries where countries_name='".$commande_data['delivery_country']."'");
			$res_country_id=tep_db_fetch_array($req_id_pays);
    ?>
    </div>
</div>
<div style="clear:both;"></div>
<!-- fin de l'entete -->

<!-- detail de la commande -->

<div class="petit_titre">Votre commande contient</div>
<div style="clear:both;"></div>
<div class="commande_detail_barre_info">
	<div  style="float:left; width:100px;"><strong>Qt&eacute;</strong></div>
    <div  style="float:left; width:650px; text-align:left;"><strong><?php if($nbArticles>1)echo'Articles'; else echo'Article';?></strong></div>
    <div  style="float:left; width:95px;">
    	<strong>
        Prix Unit. <?php if (parametrage_affiche_prix($res_country_id['countries_id']) == 1.2) echo 'TTC'; else echo 'HT';?>
        </strong>
    </div>
    <div  style="float:left; margin-left:10px; width:95px;">
    	<strong>
    	Prix Total <?php if (parametrage_affiche_prix($res_country_id['countries_id']) == 1.2) echo 'TTC'; else echo 'HT';?>
        </strong>
    </div>
    <div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
<?php
while ($articles_data = tep_db_fetch_array($articles_query)) {
?>
<div class="commande_detail_barre_info" style="background-color:#FFF; color:#000; border-style:solid;  border-color:<?php echo COULEUR_25; ?>; border-width:0px 0px 2px 0px;">
	<div  style="float:left; width:100px;"><strong><?php echo $articles_data['products_quantity']; ?></strong></div>
    <div  style="float:left; width:650px; text-align:left;"><strong>
	<?php 
	echo $articles_data['products_name'];

	$req_option=tep_db_query("select products_options, products_options_values from ".TABLE_ORDERS_PRODUCTS_ATTRIBUTES." where orders_products_id=".$articles_data['orders_products_id']."");
	   $nb_option=tep_db_num_rows($req_option);
	   if($nb_option==1)
	   { 
	   $res_option=tep_db_fetch_array($req_option);
	   echo '<br>'.$res_option['products_options'].' : '.$res_option['products_options_values'];}
	   
	?></strong></div>
    <div  style="float:left; width:95px;"><strong><?php echo format_to_money($articles_data['final_price']*parametrage_affiche_prix($res_country_id['countries_id'])).' &euro;';?></strong></div>
    <div  style="float:left; margin-left:10px; width:95px;">
         <strong><?php echo format_to_money(($articles_data['final_price']*$articles_data['products_quantity'])*parametrage_affiche_prix($res_country_id['countries_id'])).' &euro;';?></strong>
    </div>
    <div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
<?php
}
?>
<div style="float:right; text-align:right; margin-top:30px;">
<?php
	echo 'Sous-Total HT: '.$sous_total = $commande_data['ss_total'].' &euro;';
	echo'<br>';
	$remise = $commande_data['remise']+$commande_data['remise_porte_monnaie'];
	echo 'Frais de livraison HT : '.$fdp = $commande_data['frais_port_client'].' &euro;<br>';
	if (parametrage_affiche_prix($res_country_id['countries_id']) == 1.2) {
		echo 'Dont TVA Fran&ccedil;aise 20% : '.$tva = $commande_data['tva_total'].' &euro;<br>';
	}
	if($remise!=0)
	echo 'Remise HT : '.$remise.' &euro;<br>';
	$total = $commande_data['total'];
	echo 'Total &agrave; Payer : <span style="color:' . COULEUR_28 . '; font-size:14px;"><strong>'. format_to_money($total) .'&euro;</strong></span><br>';
?>
</div>
<div style="clear:both;"></div>
<!-- Fin detail de la commanade -->

<!-- historique commande -->
<div class="petit_titre">Suivi de votre colis</div>
<div style="clear:both;"></div>
<div class="commande_detail_barre_info">
	<div  style="float:left; width:300px; margin-right:35px;"><strong>Date d'exp&eacute;dition</strong></div>
    <div  style="float:left;  width:300px;"><strong>Num&eacute;ros de colis</strong></div>
    <div  style="float:right; width:300px;"><strong>Suivi du colis</strong></div>
    <div style="clear:both;"></div>
</div>

<?php 
if ($nbcolis_send==0) {
?>
	<div class="commande_detail_barre_info" style="background-color:#FFF; color:#000; border-bottom: 2px solid <?php echo COULEUR_25; ?>; padding: 0;">
		<div style="float:left; height:30px; width:100%; border-style:solid;  border-color:#ae69c2; border-width:0px 0px 0px 0px; padding-top: 10px">
			<strong>Pour le moment aucun colis n'a &eacute;t&eacute; envoy&eacute;</strong>
		</div>
		<div style="clear:both;"></div>
	</div>
<?php
} else {
	while ($hist = tep_db_fetch_array($history_command)) {
	?>
		<div class="commande_detail_barre_info" style="background-color:#FFF; color:#000; border-style:solid;  border-color:<?php echo COULEUR_25; ?>; border-width:0px 0px 2px 0px; padding:0px 0px 0px 0px;">
			<div style="float:left; height:30px; width:300px; margin-right:35px; border-right:2px solid <?php echo COULEUR_25; ?>; padding-top:10px;">
				<strong>
					<?php 
					$timestamp_d = strtotime($hist['date_added']);
					echo html_entity_decode(date_fr($timestamp_d)); 
					?>
				</strong>
			</div>
			<div style="float:left; width:300px; padding-top:15px;">
				<strong>
					<?php 
					if ($hist['track_num']!='') {
						echo  $hist['track_num'];
					} else if ($hist['track_num2']!='') {
						echo $hist['track_num2'];
					} else if ($hist['track_num3']!='') {
						echo $hist['track_num3'];
					} else if ($hist['track_num4']!='') {
						echo $hist['track_num4'];
					} else if ($hist['track_num5']!='') {
						echo $hist['track_num5'];
					}
					?>
				</strong>
			</div>
			<div style="float:right; height:30px; width:300px; border-left:2px solid <?php echo COULEUR_25; ?>; padding-top:6px;">
				<strong>             
					<?php
					if ($hist['track_num']!='') {
						echo '<a href="https://www.laposte.fr/outils/suivre-vos-envois?code='.$hist['track_num'].'" target="_blank" target="_blank" title="num&eacute;ro de suivi colissimo">
							<input type="submit" value="Suivre mon colis" class="button-suivi">
						</a>';
					} else if ($hist['track_num2']!='') {
						echo '<a href="https://www.laposte.fr/outils/suivre-vos-envois?code='.$hist['track_num2'].'" target="_blank" target="_blank" title="num&eacute;ro de suivi courrier">
							<input type="submit" value="Suivre mon colis" class="button-suivi">
						</a>';
					} else if ($hist['track_num3']!='') {
						echo '<a href="https://www.laposte.fr/outils/suivre-vos-envois?code='.$hist['track_num3'].'" target="_blank" title="num&eacute;ro de suivi chronopost">
							<input type="submit" value="Suivre mon colis" class="button-suivi">
						</a>';
					}  else if ($hist['track_num4']!='') {
						echo '<a href="https://www.dpd.fr/traces_exd_'.$hist['track_num4'].'" target="_blank" title="num&eacute;ro de suivi DPD">
							<input type="submit" value="Suivre mon colis" class="button-suivi">
						</a>';
					}  else if ($hist['track_num5']!='') {
						echo '<a href="https://www.ups.com/WebTracking?loc=fr_fr&requester=ST&trackingNumber='.$hist['track_num5'].'" target="_blank" title="num&eacute;ro de suivi UPS">
							<input type="submit" value="Suivre mon colis" class="button-suivi">
						</a>';
					}
					?>
				</strong>
			</div>
			<div style="clear:both;"></div>
		</div>
	<?php
	}
}
?>
<!-- Fin historique commande -->

<!-- commentaire commande -->
<?php 
//and comments is not null
$history_command2 = tep_db_query("SELECT date_added, comments, orders_status_name
									FROM ".TABLE_ORDERS_STATUS_HISTORY." os, ".TABLE_ORDERS_STATUS." o
									WHERE os.orders_status_id=o.orders_status_id
										  
										  and orders_id = '" . $id_commande . "' ORDER BY date_added asc");
$nb_comment=tep_db_num_rows($history_command2);
?><br>
<?php
	if($nb_comment==0)
	{
		echo"<br><br>";
	}
	

while ($com = tep_db_fetch_array($history_command2))
{
?>
<div style="background-image:url(../template/base/command_detail/fond_com_top.png); width:956px; height:12px;  background-repeat:no-repeat; margin-top:15px;"></div>
<div style="background-image:url(../template/base/command_detail/fond_com_in.png); width:956px;">
	<div style="float:left; margin-left:20px;">
		<strong>
		<?php $timestamp_d = strtotime($com['date_added']);
            echo "Le ".html_entity_decode(date_fr($timestamp_d));
        ?>
    	</strong>    
    </div>
    <div style="float:right; margin-right:20px;">
        

Le statut de votre commande est : <strong><?php echo $com['orders_status_name'];?></strong>
    </div>
    <div style="clear:both;"></div>
    <?php // On met la condition de si pas vide  :  if ($com['comments'] 
		if ($com['comments'] != NULL) { // Dans le cas ou ce n'est pas VIDE ?>
				
         <div style="border-style:dotted; border-color:#FFF; border-width:2px 0px 0px 0px; margin-left:20px; margin-right:20px; margin-top:10px; padding-top:5px; padding-bottom:20px; text-align:left;">
        Commentaire :
        <img src="../template/base/command_detail/quote_ouvrir.png" alt="guillemet_open" />
        <?php echo nl2br($com['comments']); ?>        
        <img src="../template/base/command_detail/quote_fermer.png" alt="guillemet_close" />
    	</div>        
        
	<?php } // On ferme la condition ?> 
	
	
	</div>
<div style="background-image:url(../template/base/command_detail/fond_com_bottom.png); width:956px; height:12px; background-repeat:no-repeat; margin-bottom:10px;">&nbsp;</div>
<?php
}
?>
<!--Fin commentaire commande -->
</div>
<?php 
}
}
else
{ header('Location: connexion.php');}
require("../includes/footer.php");
require("../includes/page_bottom.php"); 


?>