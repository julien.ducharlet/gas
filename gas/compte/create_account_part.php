<?php
/* Page permettant � un utilisateur de se creer un compte particulier */

$nom_page = "create_account_part";

require("../includes/page_top.php");
require("../includes/meta_head.php");
include("fonction/session.php");
include("fonction/inscription_formulaire.php");
require("../includes/header.php");
?>

<div id="corps">
            <div id="bande_rubrique_g">
                    <div id="bande_image">
                        <img src="../template/base/compte/icones/bonhomme.png" alt="info commande" />
                    </div>
                    <div id="bande_titre">Inscription</div>
                    <div id="bande_sous_titre">
                     Cr&eacute;er votre compte
                    </div>
                </div>
                <div id="bande_rubrique_d">
                <a href="../index.php" title="Retour � l'accueil"><img src="../template/base/boutons/bouton_retour-accueil.png" alt="Retour � l'accueil"/></a>
                </div>
                <div style="clear:both;"></div>
            <div style="clear:both;"></div>
<?php
if (empty($_POST['mail_cli'])) { $_POST['mail_cli'] = ''; } 
if (empty($_POST['civi'])) { $_POST['civi'] = ''; } 
if (empty($_POST['pays'])) { $_POST['pays'] = ''; } 
if (empty($_POST['conf_mail_cli'])) { $_POST['conf_mail_cli'] = ''; } 
if (empty($_POST['email_parrain'])) { $_POST['email_parrain'] = ''; }  
if (empty($_POST['rgpd'])) { $_POST['rgpd'] = ''; } 
if (empty($_POST['jour'])) { $_POST['jour'] = ''; } 
if (empty($_POST['mois'])) { $_POST['mois'] = ''; } 
if (empty($_POST['annee'])) { $_POST['annee'] = ''; }  
if (empty($_POST['mdp'])) { $_POST['mdp'] = ''; }  
if (empty($_POST['conf_mdp'])) { $_POST['conf_mdp'] = ''; }   
if (empty($_POST['news'])) { $_POST['news'] = ''; }  
if (empty($_POST['sms'])) { $_POST['sms'] = ''; }  


if(isset($_POST['valid_form']) && $_POST['valid_form']=="ok"){
	$ret='';
    echo verif();
	
	$nom_cli = format_text($_POST['nom_cli']);
	$prenom_cli = format_text($_POST['prenom_cli']);
	$adresse_cli = format_text($_POST['adresse_cli']);

	$complement_adresse_cli = format_text($_POST['complement_adresse_cli']);
	$cp_cli = format_text($_POST['cp_cli']);
	$ville_cli = format_text($_POST['ville_cli']);
	
	$tel_portable_cli = format_tel($_POST['tel_portable_cli']);
	$tel_fixe_cli = format_tel($_POST['tel_fixe_cli']);
}
else {
	
	$nom_cli = '';
	$prenom_cli = '';
	$adresse_cli = '';

	$complement_adresse_cli = '';
	$cp_cli = '';
	$ville_cli = '';
	
	$tel_portable_cli = '';
	$tel_fixe_cli = '';
}
?>

<div style="float:right; margin-bottom:5px; color:#DF4E02;">* information obligatoire</div>
<div style="clear:both;"></div>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" name="formu">
	<input type="hidden" name="panier" value="<?php echo $_GET['panier']; ?>" />

  <!-- block text -->
            <div  style=" width:464px; float:left; background-color:#999; margin-right:0px;">
                <div  class="bordure_haut_verte">
                	<p class="titre_info"><span>Mes coordonn&eacute;es</span></p>
                </div>
                
                <div class="bordure_pixel_vert">
                    <div style="padding:0 0 5px 20px; height:350px;">
        
                        <div class="conteneur_desc_item" style="width:150px;">
                            <div class="item_i">Civilit&eacute; :</div>
                            <div class="item_i">Nom :</div>
                            <div class="item_i">Pr&eacute;nom :</div>
                            <div class="item_i">Date de naissance :</div>
                            <div class="item_i">Adresse :</div>
                            <div class="item_i">Compl�ment :</div>
                            <div class="item_i">Code postal :</div>
                            <div class="item_i">Ville :</div>
                            <div class="item_i">Pays :</div>
                            <div class="item_i">T&eacute;l&eacute;phone portable :</div>
                            <div class="item_i">T&eacute;l&eacute;phone fixe :</div>
                        </div>
                        
                        <div class="conteneur_composant_item" style="width:285px; color:#DF4E02;">
                            <div class="item_i">
                                <input type="radio" name="civi" value="m" id="H" <?php if($_POST['civi']=='m') echo 'checked="checked"'; ?> /><span style="color:#000;">M</span>&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="civi" value="f" id="MME" <?php if($_POST['civi']=='f') echo 'checked="checked"'; ?> /><span style="color:#000;">Mme</span>&nbsp;&nbsp;&nbsp;
								<input type="radio" name="civi" value="d" id="MLLE" <?php if($_POST['civi']=='d') echo 'checked="checked"'; ?> /><span style="color:#000;">Mlle</span>&nbsp;&nbsp;*
                            </div>
                            <div class="item_i">
                                <input type="text" id="nom" name="nom_cli" size="30" value="<?php echo $nom_cli; ?>" />&nbsp;*
                            </div>
                            <div class="item_i">
                                <input type="text" id="prenom" name="prenom_cli" size="30" value="<?php echo $prenom_cli; ?>" />&nbsp;*
                            </div>
							<div class="item_i">
                                <select id="jours" name="jour">
                                    <option  value="jour">jour</option>
									<?php for($i=1;$i<32;$i++){?>
                                    		<option <?php if($_POST['jour']==$i) echo'selected="selected"'; ?> value="<?php echo $i;?>"><?php echo $i;?></option>
                                     <?php } ?>
                                </select>

                                <select id="mois" name="mois" >
                                	<option value="mois">mois</option>
									<?php  
										$tab=array('janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre');
										for($i=0;$i<count($tab);$i++){	
                            		?>
                                    <option <?php if($_POST['mois']==$i+1) echo'selected="selected"'; ?> value="<?php echo $i+1;?>"><?php echo $tab[$i];?></option>
                             		<?php } ?>
                                </select>
                                      
                                <select id="annees" name="annee" >
                                	<option value="annee">ann&eacute;e</option>
									<?php  
										$age_min= date("Y") -14;
										$age_max= date("Y") -80;
										for($i=$age_min ;$i>$age_max;$i--){
                                    ?>
                                    		<option <?php if($_POST['annee']==$i) echo'selected="selected"'; ?> value="<?php echo $i;?>"><?php echo $i;?></option>
                             			<?php } ?>
                                </select>&nbsp;*
							</div>
                                <div class="item_i">
                                <input type="text" name="adresse_cli" maxlength="31" size="30" id="adresse_cli" value="<?php echo $adresse_cli; ?>" />&nbsp;&nbsp;*
                                </div>
                                <div class="item_i">
                                <input type="text" name="complement_adresse_cli" maxlength="31" size="30" id="complement_adresse_p" value="<?php echo $complement_adresse_cli; ?>"  />&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                                <div class="item_i">
                                <input type="text" name="cp_cli" size="30" id="cp_cli" value="<?php echo $cp_cli; ?>" maxlength="5" />&nbsp;&nbsp;*
                                </div>
                                <div class="item_i">
                                <input type="text" name="ville_cli" size="30" id="ville_cli" value="<?php echo $ville_cli; ?>" />&nbsp;&nbsp;*
                                </div>
                                <div class="item_i">
                                <select id="pays" name="pays" style="width:208px;">  
                                        <option value="pays">Veuillez choisir un pays</option>
                                <?php  
                                        $req_pays=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 ORDER BY countries_name");
                                        while($res=tep_db_fetch_array($req_pays)) {
                                ?>
                                        <option <?php if($_POST['pays']==$res['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res['countries_id'];?>">
                                                <?php echo $res['countries_name'];?>
                                        </option>
                                 <?php } ?>
                                 </select>&nbsp;*
								</div>
                                <div class="item_i">
                                <input type="text" name="tel_portable_cli" size="30" id="telephone_p" value="<?php echo $tel_portable_cli; ?>" />&nbsp;&nbsp;*
                                </div>
                                <div class="item_i">
                                <input type="text" name="tel_fixe_cli" size="30" id="telephone_f" value="<?php echo $tel_fixe_cli; ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                        </div>
                        </div>
						<div class="clear"></div> 
                 	</div>
                <div class="bordure_bas_verte"></div>
            </div>
        <!-- fin block text-->
        
    <!-- block text -->
            <div  style="width:464px; margin-top:0px; margin-right:0px; float:right;">
                <div  class="bordure_haut_verte">
                <p class="titre_info"><span>Mes informations de connexion</span></p>                
                </div>
                <div class="bordure_pixel_vert">
                    <div style="padding:0 15px 5px 20px;">
                        <div class="conteneur_desc_item" style="width:230px; height:130px;">
                                <div class="item_i">Adresse email	 :</div>
                                <div class="item_i">Confirmation de votre adresse email	 :</div>
                                <div class="item_i">Mot de passe	 :</div>
                                <div class="item_i">Confirmation de votre mot de passe	 :</div>
								<?php if(!empty($_REQUEST['email_parrain'])) { ?>
                                    <div class="item_i">Adresse email de votre parrain	 :</div>
                                <?php } ?>
                        </div>
						
                        <div class="conteneur_composant_item" style="width:170px; color:#DF4E02;">
                                <div class="item_i"><input type="text" name="mail_cli" size="17" id="mail" value="<?php echo $_POST['mail_cli']; ?>"  />&nbsp;*</div>
                                <div class="item_i"><input type="text" name="conf_mail_cli" size="17" id="conf_mail" value="<?php echo $_POST['conf_mail_cli']; ?>"  />&nbsp;*</div>
                                <div class="item_i"><input type="password" name="mdp" size="17" id="mdp" value="<?php echo $_POST['mdp']; ?>"  />&nbsp;*</div>
                                <div class="item_i"><input type="password" name="conf_mdp" size="17" id="conf_mdp" value="<?php echo $_POST['conf_mdp']; ?>" />&nbsp;*</div>
								<?php if(!empty($_REQUEST['email_parrain'])) { ?>
                                	<div class="item_i"><input type="text" name="email_parrain" size="17" id="email_parrain" value="<?php echo $_REQUEST['email_parrain']; ?>" />&nbsp;&nbsp;&nbsp;</div>
                                <?php } ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>

                    <div class="bordure_pixel_vert2">&nbsp;</div>                    
                    <p class="titre_info"><span>Souhaitez vous recevoir</span></p>
                    <div style="padding:10px 0px 0px 10px;">
                    	<div class="conteneur_desc_item" style="width:170px;">
							<div class="item_i">Notre lettre d'information :</div>
                        </div>
                        <div class="conteneur_composant_item2" style="width:270px; color:#DF4E02;" >
							<div class="item_i_right">
								<div style="float:left;"><input type="checkbox" name="news" value="news" <?php if($_POST['news']!='') echo'checked="checked"'; ?> /></div>
								<div class="mentions" style="font-size:10px; width:230px; height:30px; float:left;"></div>
							</div>
                        </div>
                        <div class="clear"></div>
                    </div>
					<div style="padding:0px 10px 0px 10px; font-size:10px; text-align:left;" >
                    	<strong>Nous n'envoyons pas plus d'un email par semaine.</strong><br />
						Soucieux de la protection de votre vie priv�e, nous traitons toutes les informations vous concernant avec la plus stricte confidentialit� et nous ne revendons aucune de ces informations � d'autres soci�t�s.
					</div>
                 </div>         
                <div class="bordure_bas_verte"></div>
				
				<div style="padding:10px 10px 0px 10px; font-size:10px; text-align:left;">
					<input type="checkbox" name="rgpd" value="rgpd" <?php if($_POST['rgpd']!='') echo'checked="checked"'; ?>>RGPD : Vous acceptez que les informations recueillies sur ce formulaire sont enregistr�es dans un fichier informatis� par G�N�RAL ARMY STORE pour la gestion de votre compte. Elles sont conserv�es jusqu'� votre demande d'�ffacement et sont uniquement destin�es � la soci�t� G�N�RAL ARMY STORE. 
				</div>
				
                <input type="hidden" name="valid_form" value="ok" />
                <div style="margin-top:10px;">
                 <a href="javascript:document.formu.submit();" title="Valider votre inscription"><img src="../template/base/boutons/bouton_valider_inscription.jpg" alt="bt valider" /></a>        		  <div class="clear"></div>
			</div>
			
        <!-- fin block text-->
        </div>
        <div class="clear"></div>
		<div style="padding:20px 10px 0px 20px; font-size:10px; text-align:center;">
			Conform�ment � la loi � informatique et libert�s �, vous pouvez exercer votre droit d'acc�s aux donn�es vous concernant et les faire rectifier en contactant : <br>
			G�N�RAL ARMY STORE - 5 rue H. Bajard - 26260 SAINT DONAT sur L'HERBASSE - T�l�phone : 09 53 31 59 47 - Email : info[at]generalarmystore.fr 
		</div>
</form>
	
</div>
<?php 
require("../includes/footer.php");
require("../includes/page_bottom.php");
?>

