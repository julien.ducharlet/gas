<?php 
/*  
   Génération de la facture en .pdf avec FPDF
   Liste des fonctions : https://www.fpdf.org/fr/doc/index.php
*/

/* Initialisations */		
	require("../../includes/configure.php");
	require(ABSOLUTE_DIR . "/gas/includes/fonctions/base_de_donnees.php");
	require(DATABASE_TABLE_DIR);
	require('fpdf.php');
	
	define('EURO', chr(128));
	tep_db_connect();

/* Récupération des données */
	$id_commande = $_GET['id_com'];
	
	$commande_query = tep_db_query("	SELECT * 
										FROM ". TABLE_ORDERS ." 
										WHERE orders_id = '". $id_commande ."'");
	$commande_data = tep_db_fetch_array($commande_query);
	
	$commentaires_query = tep_db_query("SELECT comments 
										FROM ". TABLE_ORDERS_STATUS_HISTORY ." 
										WHERE orders_id = '". $id_commande ."'
										ORDER BY orders_status_history_id ASC");
	$commentaires_data = tep_db_fetch_array($commentaires_query);
	
	if($_REQUEST["cli"] == md5($commande_data["customers_id"])) {
		
		$timestamp_facture = strtotime($commande_data['orders_date_facture']);
		$date_facture = html_entity_decode(date_fr($timestamp_facture));
		
		$id_facture = $commande_data['orders_numero_facture'];
		
		$facture_entreprise = $commande_data['customers_company'];
		if ($facture_entreprise == '')
			$facture_nom = ucwords($commande_data['customers_name']);
		else
			$facture_nom = "\n" . ucwords($commande_data['customers_name']);
		
		$facture_adresse_2 = $commande_data['customers_suburb'];
		if ($facture_adresse_2 == '')
			$facture_adresse_1 = $commande_data['customers_street_address'];
		else
			$facture_adresse_1 = $commande_data['customers_street_address'] . "\n";
		
		$facture_adresse = '';
		if (!empty($commande_data['customers_street_address_3'])) {
			$facture_adresse .= $commande_data['customers_street_address_3']. "\n";
		}
		if (!empty($commande_data['customers_suburb'])) {
			$facture_adresse .= $commande_data['customers_suburb']. "\n";
		}
		if (!empty($commande_data['customers_street_address'])) {
			$facture_adresse .= $commande_data['customers_street_address']. "\n";
		} 
		if (!empty($commande_data['customers_street_address_4'])) {
			$facture_adresse .= $commande_data['customers_street_address_4']. "\n";
		}
	
		$facture_code_postal = $commande_data['customers_postcode'];
		$facture_ville = $commande_data['customers_city'];
		$facture_pays = $commande_data['customers_country'];
		
		$livraison_entreprise = $commande_data['delivery_company'];
		if ($livraison_entreprise == '')
			$livraison_nom = ucwords($commande_data['delivery_name']);
		else
			$livraison_nom = "\n" . ucwords($commande_data['delivery_name']);
				
		
		$livraison_adresse = '';
		if (!empty($commande_data['delivery_street_address_3'])) {
			$livraison_adresse .= $commande_data['delivery_street_address_3']. "\n";
		}
		if (!empty($commande_data['delivery_suburb'])) {
			$livraison_adresse .= $commande_data['delivery_suburb']. "\n";
		}
		if (!empty($commande_data['delivery_street_address'])) {
			$livraison_adresse .= $commande_data['delivery_street_address']. "\n";
		} 
		if (!empty($commande_data['delivery_street_address_4'])) {
			$livraison_adresse .= $commande_data['delivery_street_address_4']. "\n";
		}
		
		$livraison_code_postal = $commande_data['delivery_postcode'];
		$livraison_ville = $commande_data['delivery_city'];
		$livraison_pays = $commande_data['delivery_country'];
		
		$timestamp_commande = strtotime($commande_data['date_purchased']);
		$date_commande = html_entity_decode(date_fr($timestamp_commande));
		
		$timestamp_devis = strtotime($commande_data['orders_date_fin_devis']);
		$date_fin_devis = html_entity_decode(date_fr($timestamp_devis));
		
		$mode_reglement = $commande_data['payment_method'];
		
		$total = $commande_data['total'];
		$sous_total = $commande_data['ss_total'];
		$tva = $commande_data['tva_total'];
		$remise = $commande_data['remise'];
		// Début ajout thierry
		$remise_pourcent = $commande_data['remise_pourcent'];
		$remise_porte_monnaie = $commande_data['remise_porte_monnaie'];
		// Fin ajout thierry
		$fdp = $commande_data['frais_port_client'];
		
	/* récupération des articles de la commande */
		
		$articles_query = tep_db_query("SELECT orders_products_id, products_model, products_name, final_price, products_quantity, products_tax 
										FROM ". TABLE_ORDERS_PRODUCTS ." 
										WHERE orders_id = '". $id_commande ."'");
	
	/* Création de la page */
	
		$PDF = new fpdf();
		$PDF->AddPage();
		$PDF->SetMargins(2, 2);
		//$PDF->SetDisplayMode(fullpage, continuous);
		
	/* Information sur le document  */
		$PDF->SetAuthor('Société GENERAL ARMY STORE', true); // Auteur du document
		
		if($id_facture>0  && $commande_data['total'] < 0) {
			
			$PDF->SetTitle('Avoir '. $id_facture, true); // Définit le titre du document. 
			$PDF->SetSubject('Votre Avoir '. $id_facture .' de la commande '. $id_commande, true); // Définit le sujet du document. 
		}
		elseif($id_facture>0) {
			
			$PDF->SetTitle('Facture '. $id_facture, true); // Définit le titre du document. 
			$PDF->SetSubject('Votre facture '. $id_facture .' de la commande '. $id_commande, true); // Définit le sujet du document. 
		}

	/* Header */
	
		//On change de page automatiquement dès qu'on atteint la limite de XX mm en bas de la page
		$PDF->SetAutoPageBreak(true, 20);
		
	/* Date de la facture */
		if($id_facture>0  && $commande_data['total'] < 0) {
			
			$PDF->SetTextColor(0, 0, 0);
			$PDF->SetFont('Arial', '', 11);
			$PDF->SetXY(101, 9);
			$PDF->Cell(100, 5, 'Avoir du '. $date_facture, 0, 1, 'R');
		
		} elseif($id_facture>0) {
			
			$PDF->SetTextColor(0, 0, 0);
			$PDF->SetFont('Arial', '', 11);
			$PDF->SetXY(101, 7.5);
			$PDF->Cell(100, 5, 'Facture du '. $date_facture, 0, 1, 'R');
		}
		
	/* N° de la facture */
		if($id_facture>0 && $commande_data['total'] < 0) { 
			$facture = html_entity_decode("Avoir N&deg;") . $id_facture;
		} elseif($id_facture>0) { 	
			$facture = html_entity_decode("Facture N&deg; ") . $id_facture;
		} else { 
			$facture = html_entity_decode("Devis");
		}
		
		$PDF->SetXY(101, 24);
		$PDF->SetFont('Arial', '', 14);
		$PDF->SetTextColor(1, 136, 198);  // style=\"color:#0188C6;\"
		$PDF->MultiCell(100, 6, $facture, 0, 'R', 0);
	
	/* Adresses de livraison et de facturation */
		
		//Adresse de facturation
		/* Fond petite zone */
		$PDF->SetXY(10, 38);
		$PDF->SetFillColor(103, 103, 103); //$PDF->SetFillColor(167, 176, 106);
		$PDF->Cell(7, 45, '', 0, 0, 'C', 1);
		
		/* Fond grande zone */
		$PDF->SetX(17);
		$PDF->SetFillColor(189, 189, 189); // $PDF->SetFillColor(193, 202, 123);
		$PDF->Cell(83, 45, '', 0, 0, 'C', 1);
		
		$PDF->SetXY(18, 41);
		$PDF->SetFont('Arial', 'B', 10);
		$PDF->SetTextColor(0, 0, 0);
		$PDF->Cell(69, 4, html_entity_decode('Vendu &agrave; :'), 0, 0, 'L');
		
		$PDF->SetXY(18, 48);
		$PDF->SetFont('Arial', '', 10);
		$PDF->MultiCell(69, 4, $facture_entreprise . $facture_nom ."\n". $facture_adresse . $facture_code_postal ." ". $facture_ville ."\n". $facture_pays, 0, 'L', 0);
		
		//Adresse de Livraison
		/* Fond petite zone */
		$PDF->SetXY(110, 38);
		$PDF->SetFillColor(151, 151, 151); //$PDF->SetFillColor(184, 170, 133);
		$PDF->Cell(7, 45, '', 0, 0, 'C', 1);
		
		/* Fond grande zone */
		$PDF->SetX(117);
		$PDF->SetFillColor(220, 220, 220); //$PDF->SetFillColor(216, 210, 186);
		$PDF->Cell(83, 45, '', 0, 0, 'C', 1);
		
		$PDF->SetXY(118, 41);
		$PDF->SetFont('Arial', 'B', 10);
		$PDF->SetTextColor(0, 0, 0);
		$PDF->Cell(69, 4, html_entity_decode('Livr&eacute; &agrave; :'), 0, 0, 'L');
		
		$PDF->SetXY(118, 48);
		$PDF->SetFont('Arial', '', 10);
		$PDF->MultiCell(69, 4, $livraison_entreprise . $livraison_nom ."\n". $livraison_adresse . $livraison_code_postal ." ". $livraison_ville ."\n". $livraison_pays, 0, 'L', 0);
		
	
	/* Informations de la comande */
		
		/* Informations de la comande */
		
		// Trait supérieur
		$PDF->SetFillColor(1, 136, 198); //$PDF->SetFillColor(136, 140, 7);  
		$PDF->SetXY(10, 88);
		$PDF->Cell(190, 0.2, '', 0, 0, 'C', 1);
		
		if (empty($mode_reglement)) { // s'il n'y a pas de mode de reglement 
			//
			$PDF->SetXY(10, 90);
			$PDF->MultiCell(50,5,html_entity_decode("Num&eacute;ro de commande : \nDate de cr&eacute;ation du devis : \nDate d'expiration du devis : "),0,'L',0);

			// Texte numéro de commande
			$PDF->SetFont('Arial','B',10);
			$PDF->SetTextColor(1, 136, 198); // 220, 34, 19
			$PDF->SetXY(55,90);
			$PDF->Cell(141, 5, $id_commande ,0 ,1 ,'L' ,0);
			
			// Texte autres
			$PDF->SetTextColor(0, 0, 0);		
			$PDF->SetX(55);
			$PDF->Cell(143,5,$date_commande,0,1,'L',0);
			$PDF->SetX(55);
			$PDF->Cell(159,5,$date_fin_devis,0,1,'L',0);
			
		} else {
			//
			$PDF->SetXY(10, 90);
			$PDF->MultiCell(50,5,html_entity_decode("Num&eacute;ro de commande : \nDate de la commande : \nMode de R&egrave;glement :"),0,'L',0);
			
			// Texte numéro de commande
			$PDF->SetFont('Arial','B',10);
			$PDF->SetTextColor(1, 136, 198); // 220, 34, 19
			$PDF->SetXY(55,90); // 49,120 
			$PDF->Cell(141,5,$id_commande,0,1,'L',0);
			
			// Texte autres
			$PDF->SetTextColor(0, 0, 0);		
			$PDF->SetX(55); // 47
			$PDF->Cell(143,5,$date_commande,0,1,'L',0);
			$PDF->SetX(55); // 30
			$PDF->Cell(159,5,$mode_reglement,0,1,'L',0);
			
		}
			
		// Trait inférieur
		/*$PDF->SetXY(10,117);
		$PDF->Cell(190,0.2,'',0,0,'C',1);*/
	
	/* Fin Informations de la commande */
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Détails des articles de la commande
///////////////////////////////////////////////////////////////////////////////////////////////////////////

		/*$PDF->SetFont('Arial', '', 14);
		$PDF->SetXY(10, 142);
		$PDF->MultiCell(50, 5, "Votre commande", 0, 'L', 0);*/
		
		$PDF->SetFillColor(1, 136, 198); //$PDF->SetFillColor(136, 140, 7);  
		
		$PDF->SetXY(10, 107.6);
		$PDF->Cell(190, 0.2, '', 0, 0, 'C', 1);
		
		$PDF->SetFont('Arial', 'B', 7);
		$PDF->SetXY(10, 108);
		$PDF->Cell(7, 5,html_entity_decode("Qt&eacute;"), 0, 0, 'C', 0);
		$PDF->Cell(110, 5,html_entity_decode("D&eacute;signation"), 0, 0, 'C', 0);
		$PDF->Cell(18, 5,"Prix U. HT", 0, 0, 'C', 0);
		$PDF->Cell(18, 5,"Total HT", 0, 0, 'C', 0);
		$PDF->Cell(18, 5,"Prix U. TTC", 0, 0, 'C', 0);
		$PDF->Cell(18, 5,"Total TTC", 0, 1, 'C', 0);
		
		$PDF->SetX(10);
		$PDF->Cell(190, 0.2, '', 0, 0, 'C', 1);
		
		while ($articles_data = tep_db_fetch_array($articles_query)) {
					
			/* récupération des options des articles de la commande */
			$options_query = tep_db_query("SELECT products_options, products_options_values 
											FROM ". TABLE_ORDERS_PRODUCTS_ATTRIBUTES ." 
											WHERE orders_products_id = '". $articles_data['orders_products_id'] ."'");
			$options = tep_db_fetch_array($options_query);
			
			AjoutArticle($articles_data['products_quantity'], $articles_data['products_name'], $articles_data['final_price'], $articles_data['products_tax'], $options['products_options'], $options['products_options_values']);
		}
		
		$PDF->SetFillColor(1, 136, 198); //$PDF->SetFillColor(136, 140, 7);  
		
		$PDF->SetX(10);
		$PDF->Cell(190, 0.2, '', 0, 0, 'C', 1);
		
		if ($remise_pourcent != 0) {
			$PDF->SetX(10);
			$PDF->MultiCell(190, 2, "", 0, 'L', 0);
			
			$PDF->SetTextColor(220, 34, 19);
			$PDF->SetFont('Arial', 'B', 7);
			$PDF->SetX(12);
			$remise_texte = html_entity_decode(round($remise_pourcent,0) ." % de remise ont &eacute;t&eacute; appliqu&eacute;s sur la commande\n");
			$PDF->MultiCell(190, 5, $remise_texte, 0, 'L', 0);
			$PDF->SetTextColor(0, 0, 0);
		}
		
		$PDF->SetFont('Arial', '', 10);
		
		$PDF->SetX(10);
		
		$PDF->Ln(5);
		
		$details = html_entity_decode("Sous-Total HT: ". $sous_total ." ". EURO ."\nFrais de Livraison HT : ". $fdp ." ". EURO ."\n");
				 
		if ($remise != 0)
			$details = $details . html_entity_decode("Bon de r&eacute;duction HT : ". $remise ." ". EURO ."\n");
		
		if ($remise_porte_monnaie != 0)
			$details = $details . html_entity_decode("Porte Monnaie Virtuel HT : ". $remise_porte_monnaie ." ". EURO ."\n");
		
		if ($tva != 0)
			$details = $details . html_entity_decode("TVA Fran&ccedil;aise 20% : ". $tva ." ". EURO ."\n");
		
		
		
		$PDF->MultiCell(199, 5, $details, 0, 'R', 0);
		
		$PDF->SetFont('Arial', '', 11);
		
		$PDF->Cell(182, 5, html_entity_decode("Total &agrave; Payer TTC : "), 0, 0, 'R', 0);
		
		$PDF->SetFont('Arial', 'B', 11);
		$PDF->SetTextColor(1, 136, 198); // 220, 34, 19
		
		$PDF->Cell(17, 5, $total ." ". EURO, 0, 1, 'R', 0);
	
		// affichage du 1er commentaire && si c'est pas un avoir
		if(!empty($commentaires_data['comments']) && $commande_data['total']>0) {
			//saut de ligne pour espacer
			
			$PDF->MultiCell(180, 5, "", 0, 'C', 0);
			
			$PDF->SetX(10);
			$PDF->SetFont('Arial', '', 8);
			$PDF->SetTextColor(0, 0, 0);
			$PDF->MultiCell(190, 4, html_entity_decode($commentaires_data['comments']), 1, 'L', 0);
		}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Choix d'enregistrement sur le serveur ou d'affichage dans le navigateur
///////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//On l'affiche à l'écran
		$PDF->Output();


///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fin du code de la génération de la facture
///////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
	}// FIN if l'id client encodé correspond a ce qui a été passé en param

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction qui met un nombre au format monétaire (15.00 au lieu de 15 ou 12.60 au lieu de 12.6 ou 
// encore 5.65 au lieu de 5.6497)
// Param : $valeur valeur à convertir
///////////////////////////////////////////////////////////////////////////////////////////////////////////

/*Fonction qui met un nombre au format monétaire (15.00 au lieu de 15 ou 12.60 au lieu de 12.6 ou encore 5.65 au lieu de 5.6497)
  Param : $valeur valeur à convertir*/
function FormatToMoney($valeur) {
	$nbDecimales = '';
	if(strpos($valeur,",")){
		$nbDecimales = strlen(strrchr($valeur, ","));
	}
	elseif(strpos($valeur,".")){
		$nbDecimales = strlen(strrchr($valeur, "."));
	}
	
	if ($nbDecimales > 2){
		$valeur = round($valeur, 2);
		$nbDecimales = strlen(strrchr($valeur, "."));
	}
	if ($nbDecimales == 0)
		$valeur = $valeur . ".00";
	else if($nbDecimales == 1)
		$valeur = $valeur . "00";
	else if($nbDecimales == 2)
		$valeur = $valeur . "0";
	
	return $valeur;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction qui ajoute un article au tableau de la commande
///////////////////////////////////////////////////////////////////////////////////////////////////////////

function AjoutArticle($qtte, $nom, $prix, $tax, $options_name, $options_attributes)
{
	global $PDF;
	
	if ($options_name != ''){
		$article_nom = $nom . " (" . $options_name . " : " . $options_attributes . ")";
	} else {
		$article_nom = $nom;
	}
	
	$PDF->SetFillColor(1, 136, 198); //$PDF->SetFillColor(136, 140, 7);  
	
	$PDF->SetX(17);
	$PDF->Cell(0.2, 5, '', 0, 0, 'C', 1);

	$PDF->SetX(127);
	$PDF->Cell(0.2, 5, '', 0, 0, 'C', 1);

	$PDF->SetX(145);
	$PDF->Cell(0.2, 5, '', 0, 0, 'C', 1);

	$PDF->SetX(163);
	$PDF->Cell(0.2, 5, '', 0, 0, 'C', 1);

	$PDF->SetX(182);
	$PDF->Cell(0.2, 5, '', 0, 0, 'C', 1);


	$PDF->SetFont('Arial', '', 7);
	$PDF->SetX(10);
	$PDF->Cell(7, 5, $qtte, 0, 0, 'C', 0);
	$PDF->SetFont('Arial', '', 6);
	$PDF->Cell(110, 5, $article_nom, 0, 0, 'L', 0);
	$PDF->SetFont('Arial', '', 7);
	
	$prix_ht = FormatToMoney($prix);
	$PDF->Cell(18, 5, $prix_ht ." ". EURO, 0, 0, 'C', 0);
	
	$total_ht=$qtte*$prix;
	$total_ht = FormatToMoney($total_ht);
	$PDF->Cell(18, 5, $total_ht ." ". EURO, 0, 0, 'C', 0);
	
	$prix_ttc = FormatToMoney($prix*(1+($tax/100)));
	$PDF->Cell(18, 5, $prix_ttc ." ". EURO, 0, 0, 'C', 0);
	
	$total = FormatToMoney($qtte*$prix*(1+($tax/100)));
	$PDF->Cell(18, 5, $total ." ". EURO, 0, 1, 'C', 0);
	
	return $total;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction qui affiche la date en Francais avec le Jour et le Mois
///////////////////////////////////////////////////////////////////////////////////////////////////////////

function date_fr($timestamp_date)
{
	$mois=array('Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre');
	$jour=array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
	
	$mois_fr = $mois[date('n', $timestamp_date)-1];
	$jour_fr = $jour[date('w', $timestamp_date)];
	
	$date_fr = $jour_fr ." ". date('d', $timestamp_date) ." ". $mois_fr ." ". date('Y', $timestamp_date);
	
	return $date_fr;
}


?>