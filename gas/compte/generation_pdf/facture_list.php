<?php 
/*  
   Génération de la facture en .pdf avec FPDF
   Liste des fonctions : https://www.fpdf.org/fr/doc/index.php
*/

/* Initialisations */
		
	require("../../includes/configure.php");
	require(ABSOLUTE_DIR . "/gas/includes/fonctions/base_de_donnees.php");
	
	require(DATABASE_TABLE_DIR);
	require('fpdf.php');
	
	define('EURO', chr(128));
	
	tep_db_connect();

/* Récupération des données */
	$commandes = explode(' ', $_POST['id_com']);
	
	$PDF = new fpdf();

for($index=0 ; $index<sizeof($commandes)-1 ; $index++) {
	
	$commande_query = tep_db_query("SELECT * 
									FROM ". TABLE_ORDERS ." 
									WHERE orders_id = '". $commandes[$index] ."'");
	$commande_data = tep_db_fetch_array($commande_query);
	
	$commentaires_query = tep_db_query("SELECT comments 
									FROM ". TABLE_ORDERS_STATUS_HISTORY . " 
									WHERE orders_id = '". $commandes[$index] ."'
									ORDER BY orders_status_history_id ASC");
	$commentaires_data = tep_db_fetch_array($commentaires_query);
	
	
	$timestamp_facture = strtotime($commande_data['orders_date_facture']);
	$date_facture = html_entity_decode(date_fr($timestamp_facture));
	
	$id_facture = $commande_data['orders_numero_facture'];
	
	
	$facture_entreprise = $commande_data['customers_company'];
	
	if ($facture_entreprise == '')
		$facture_nom = ucwords($commande_data['customers_name']);
	else
		$facture_nom = "\n". ucwords($commande_data['customers_name']);
	
	
	$facture_adresse_2 = $commande_data['customers_suburb'];
	
	if ($facture_adresse_2 == '')
		$facture_adresse_1 = $commande_data['customers_street_address'];
	else
		$facture_adresse_1 = $commande_data['customers_street_address'] ."\n";

	$facture_code_postal = $commande_data['customers_postcode'];
	$facture_ville = $commande_data['customers_city'];
	$facture_pays = $commande_data['customers_country'];
	
	
	$livraison_entreprise = $commande_data['delivery_company'];
	
	if ($livraison_entreprise == '')
		$livraison_nom = ucwords($commande_data['delivery_name']);
	else
		$livraison_nom = "\n". ucwords($commande_data['delivery_name']);
	
	
	$livraison_adresse_2 = $commande_data['delivery_suburb'];
	
	if ($livraison_adresse_2 == '')
		$livraison_adresse_1 = $commande_data['delivery_street_address'];
	else
		$livraison_adresse_1 = $commande_data['delivery_street_address'] . "\n";
	
	$livraison_code_postal = $commande_data['delivery_postcode'];
	$livraison_ville = $commande_data['delivery_city'];
	$livraison_pays = $commande_data['delivery_country'];
	
	$timestamp_commande = strtotime($commande_data['date_purchased']);
	$date_commande = html_entity_decode(date_fr($timestamp_commande));
	
	$mode_reglement = $commande_data['payment_method'];
	
	$total = $commande_data['total'];
	$sous_total = $commande_data['ss_total'];
	$tva = $commande_data['tva_total'];
	$remise = $commande_data['remise'];
	// Début ajout thierry
	$remise_pourcent = $commande_data['remise_pourcent'];
	$remise_porte_monnaie = $commande_data['remise_porte_monnaie'];
	// Fin ajout thierry
	$fdp = $commande_data['frais_port_client'];
	
/* récupération des articles de la commande */
	
	$articles_query = tep_db_query("SELECT orders_products_id, products_model, products_name, final_price, products_quantity, products_tax 
									FROM ". TABLE_ORDERS_PRODUCTS ." 
									WHERE orders_id = '". $commandes[$index] ."'");

/* Création de la page */

	$PDF->AddPage();
	$PDF->SetMargins(2, 2);
	$PDF->SetDisplayMode(fullpage, continuous);
	
/* Information sur le document  */
	$PDF->SetAuthor('Société GENERALARMYSTORE', true); // Auteur du document
	
	if($id_facture>0  && $commande_data['total']<0) {
		
		$PDF->SetTitle('Avoir '. $id_facture,true); // Définit le titre du document. 
		$PDF->SetSubject('Votre Avoir '. $id_facture .' de la commande '. $commandes[$index],true); // Définit le sujet du document. 
	}
	elseif($id_facture>0) {
		
		$PDF->SetTitle('Facture '. $id_facture,true); // Définit le titre du document. 
		$PDF->SetSubject('Votre facture '. $id_facture .' de la commande '. $commandes[$index],true); // Définit le sujet du document. 
	}

/* Header */

	//On change de page automatiquement dès qu'on atteint la limite de XX mm en bas de la page
	$PDF->SetAutoPageBreak(true, 20);
	
	
/* Date de la facture */
	if($id_facture>0  && $commande_data['total']<0) {
		
		$PDF->SetTextColor(0, 0, 0);
		$PDF->SetFont('Arial', '', 11);
		$PDF->SetXY(105, 4);
		$PDF->Cell(100, 5, 'Avoir du '. $date_facture, 0, 1, 'R');
	}
	elseif($id_facture>0) {
		
		$PDF->SetTextColor(0, 0, 0);
		$PDF->SetFont('Arial', '', 11);
		$PDF->SetXY(105, 4);
		$PDF->Cell(100, 5, 'Facture du '. $date_facture, 0, 1, 'R');
	}
	
/* Id de la facture et adresse du site */
	if($id_facture>0 && $commande_data['total']<0) {
		
		$facture = html_entity_decode("Avoir N&deg;") . $id_facture;
	}
	elseif($id_facture>0) {
		
		$facture = html_entity_decode("Facture N&deg;") . $id_facture;
	}
	else {
		
		$facture = html_entity_decode("Facture PROFORMA");
	}
	
	$PDF->SetXY(0, 55);
	$PDF->SetFont('Arial', '', 14);
	$PDF->SetTextColor(220, 34, 19);
	$PDF->MultiCell(210, 6, $facture . "\n". ADRESSE_SITE, 0, 'C', 0);

/* Adresses de livraison et de facturation */
	
	//Adresse de facturation
	$PDF->SetXY(10, 71);
	$PDF->SetFillColor(167, 176, 106);
	$PDF->Cell(7, 40, '', 0, 0, 'C', 1);
	
	$PDF->SetX(17);
	$PDF->SetFillColor(243, 243, 223);
	$PDF->Cell(83, 40, '', 0, 0, 'C', 1);
	
	$PDF->SetXY(18, 74);
	$PDF->SetFont('Arial', 'B', 10);
	$PDF->SetTextColor(0, 0, 0);
	$PDF->Cell(69, 4, html_entity_decode('Vendu &agrave; :'), 0, 0, 'L');
	
	$PDF->SetXY(22, 81);
	$PDF->SetFont('Arial', '', 10);
	$PDF->MultiCell(69, 4, $facture_entreprise . $facture_nom ."\n". $facture_adresse_1 . $facture_adresse_2 ."\n". $facture_code_postal ." ". $facture_ville ."\n". $facture_pays, 0, 'L', 0);
	
	//Adresse de Livraison
	$PDF->SetXY(110, 71);
	$PDF->SetFillColor(184, 170, 133);
	$PDF->Cell(7, 40, '', 0, 0, 'C', 1);
	
	$PDF->SetX(117);
	$PDF->SetFillColor(216, 210, 186);
	$PDF->Cell(83, 40, '', 0, 0, 'C', 1);
	
	$PDF->SetXY(118, 74);
	$PDF->SetFont('Arial', 'B', 10);
	$PDF->SetTextColor(0, 0, 0);
	$PDF->Cell(69, 4, html_entity_decode('Livr&eacute; &agrave; :'), 0, 0, 'L');
	
	$PDF->SetXY(122, 81);
	$PDF->SetFont('Arial', '', 10);
	$PDF->MultiCell(69, 4, $livraison_entreprise . $livraison_nom ."\n". $livraison_adresse_1 . $livraison_adresse_2 ."\n". $livraison_code_postal ." ". $livraison_ville ."\n". $livraison_pays, 0, 'L', 0);
	
	
/* Informations de la comande */
	
	// Ligne violette de DEBUT
	$PDF->SetFillColor(131, 67, 143);
	$PDF->SetXY(10, 118);
	$PDF->Cell(190, 0.2, '', 0, 0, 'C', 1);
	
	// Texte "Numéro de commande :", "Date de la commande :" et "Règlement :"
	$PDF->SetXY(10, 120);
	$PDF->MultiCell(50, 5, html_entity_decode("Num&eacute;ro de commande :\nDate de la commande :\nR&egrave;glement :"), 0, 'L', 0);
	
	// Texte indiquant le numéro de la CMD
	$PDF->SetFont('Arial', 'B', 10);
	$PDF->SetTextColor(220, 34, 19);
	$PDF->SetXY(49, 120);
	$PDF->Cell(141, 5, $commandes[$index], 0, 1, 'L', 0);
			
	// Texte indiquant la date de la CMD
	$PDF->SetTextColor(0, 0, 0);
	$PDF->SetX(47);
	$PDF->Cell(143, 5, $date_commande, 0, 1, 'L', 0);
	
	// Texte indiquant le mode de reglement de la CMD
	$PDF->SetTextColor(0, 0, 0);
	$PDF->SetX(30);
	$PDF->Cell(159, 5, $mode_reglement, 0, 1, 'L', 0);
	
	// Ligne violette de FIN
	$PDF->SetXY(10, 137);
	$PDF->Cell(190, 0.2, '', 0, 0, 'C', 1);
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Détails des articles de la commande
///////////////////////////////////////////////////////////////////////////////////////////////////////////

	$PDF->SetFont('Arial', '', 14);
	$PDF->SetXY(10, 142);
	$PDF->MultiCell(50, 5, "Votre commande", 0, 'L', 0);
	
	$PDF->SetFillColor(136, 140, 7);
	
	$PDF->SetXY(10, 147.6);
	$PDF->Cell(190, 0.2, '', 0, 0, 'C', 1);
	
	$PDF->SetFont('Arial', 'B', 7);
	$PDF->SetXY(10, 148);
	$PDF->Cell(7, 5, html_entity_decode("Qt&eacute;"), 0, 0, 'C', 0);
	$PDF->Cell(110, 5, "Article(s)", 0, 0, 'C', 0);
	$PDF->Cell(18, 5, "Prix U. HT", 0, 0, 'C', 0);
	$PDF->Cell(18, 5, "Total HT", 0, 0, 'C', 0);
	$PDF->Cell(18, 5, "Prix U. TTC", 0, 0, 'C', 0);
	$PDF->Cell(18, 5, "Total TTC", 0, 1, 'C', 0);
	
	$PDF->SetX(10);
	$PDF->Cell(190, 0.2, '', 0, 0, 'C', 1);
	
	while ($articles_data = tep_db_fetch_array($articles_query)) {
				
		/* récupération des options des articles de la commande */
		$options_query = tep_db_query("SELECT products_options, products_options_values 
										FROM ". TABLE_ORDERS_PRODUCTS_ATTRIBUTES ." 
										WHERE orders_products_id = '". $articles_data['orders_products_id'] ."'");
		$options = tep_db_fetch_array($options_query);
		
		AjoutArticle($articles_data['products_quantity'], $articles_data['products_name'], $articles_data['final_price'], $articles_data['products_tax'], $options['products_options'], $options['products_options_values']);
	}
	
	
	
	$PDF->SetFillColor(136, 140, 7);
	
	$PDF->SetX(10);
	$PDF->Cell(190, 0.2, '', 0, 0, 'C', 1);
	
	if ($remise_pourcent != 0) {
		$PDF->SetX(10);
		$PDF->MultiCell(190, 2, "", 0, 'L', 0);
		
		$PDF->SetTextColor(220, 34, 19);
		$PDF->SetFont('Arial', 'B', 7);
		$PDF->SetX(12);
		$remise_texte = html_entity_decode(round($remise_pourcent,0) ." % de remise ont &eacute;t&eacute; appliqu&eacute;s sur la commande\n");
		$PDF->MultiCell(190, 5, $remise_texte, 0, 'L', 0);
		$PDF->SetTextColor(0, 0, 0);
	}
	
	$PDF->SetFont('Arial', '', 10);
	
	$PDF->SetX(10);
	
	$PDF->Ln(5);
	
	$details = html_entity_decode("Sous-Total HT: ". $sous_total ." ". EURO ."\nFrais de Livraison HT : ". $fdp ." ". EURO ."\n");
			 
	if ($remise != 0)
		$details = $details . html_entity_decode("Bon de r&eacute;duction HT : ". $remise ." ". EURO ."\n");
	
	if ($remise_porte_monnaie != 0)
		$details = $details . html_entity_decode("Porte Monnaie Virtuel HT : ". $remise_porte_monnaie ." ". EURO ."\n");
	
	if ($tva != 0)
		$details = $details . html_entity_decode("Dont TVA Fran&ccedil;aise 20% : ". $tva ." ". EURO ."\n");
	
	
	
	$PDF->MultiCell(199, 5, $details, 0, 'R', 0);
	
	$PDF->SetFont('Arial', '', 11);
	
	$PDF->Cell(182, 5, html_entity_decode("Total &agrave; Payer TTC : "), 0, 0, 'R', 0);
	
	$PDF->SetFont('Arial', 'B', 11);
	$PDF->SetTextColor(220, 34, 19);
	
	$PDF->Cell(17, 5, $total ." ". EURO, 0, 1, 'R', 0);

	//affichage des commentaires
	if(!empty($commentaires_data['comments']) && $commande_data['total']>0) {
		//saut de ligne pour espacer
		
		$PDF->MultiCell(180, 20, "", 0, 'C', 0);
		
		$PDF->SetX(20);
		$PDF->SetFont('Arial', '', 8);
		$PDF->SetTextColor(0, 0, 0);
		$PDF->MultiCell(160, 4, html_entity_decode($commentaires_data['comments']), 1, 'L', 0);
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Choix d'enregistrement sur le serveur ou d'affichage dans le navigateur
///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fin du code de la génération de la facture
///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
}

//On l'affiche à l'écran
$PDF->Output();





///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction qui met un nombre au format monétaire (15.00 au lieu de 15 ou 12.60 au lieu de 12.6 ou 
// encore 5.65 au lieu de 5.6497)
// Param : $valeur valeur à convertir
///////////////////////////////////////////////////////////////////////////////////////////////////////////

function FormatToMoney($valeur)
{
	if(strpos($valeur,",")){
		$nbDecimales = strlen(strrchr($valeur, ","));
	}
	elseif(strpos($valeur,".")){
		$nbDecimales = strlen(strrchr($valeur, "."));
	}
	
	if ($nbDecimales > 2){
		$valeur = round($valeur, 2);
		$nbDecimales = strlen(strrchr($valeur, "."));
	}
	if ($nbDecimales == 0)
		$valeur = $valeur . ".00";
	else if($nbDecimales == 1)
		$valeur = $valeur . "00";
	else if($nbDecimales == 2)
		$valeur = $valeur . "0";
	
	return $valeur;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction qui ajoute un article au tableau de la commande
///////////////////////////////////////////////////////////////////////////////////////////////////////////

function AjoutArticle($qtte, $nom, $prix, $tax, $options_name, $options_attributes)
{
	global $PDF;
	
	if ($options_name != ''){
		$article_nom = $nom . " (" . $options_name . " : " . $options_attributes . ")";
	} else {
		$article_nom = $nom;
	}
	
	$PDF->SetFillColor(136, 140, 7);
	
	$PDF->SetX(17);
	$PDF->Cell(0.2,5,'',0,0,'C',1);

	$PDF->SetX(127);
	$PDF->Cell(0.2,5,'',0,0,'C',1);

	$PDF->SetX(145);
	$PDF->Cell(0.2,5,'',0,0,'C',1);

	$PDF->SetX(163);
	$PDF->Cell(0.2,5,'',0,0,'C',1);

	$PDF->SetX(182);
	$PDF->Cell(0.2,5,'',0,0,'C',1);


	$PDF->SetFont('Arial','',7);
	$PDF->SetX(10);
	$PDF->Cell(7,5,$qtte,0,0,'C',0);
	$PDF->SetFont('Arial','',6);
	$PDF->Cell(110,5,$article_nom,0,0,'L',0);
	$PDF->SetFont('Arial','',7);
	
	$prix_ht = FormatToMoney($prix);
	$PDF->Cell(18,5,$prix_ht . " " . EURO,0,0,'C',0);
	
	$total_ht=$qtte*$prix;
	$total_ht = FormatToMoney($total_ht);
	$PDF->Cell(18,5,$total_ht . " " . EURO,0,0,'C',0);
	
	$prix_ttc = FormatToMoney($prix*(1+($tax/100)));
	$PDF->Cell(18,5,$prix_ttc . " " . EURO,0,0,'C',0);
	
	$total = FormatToMoney($qtte*$prix*(1+($tax/100)));
	$PDF->Cell(18,5,$total . " " . EURO,0,1,'C',0);
	
	return $total;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction qui affiche la date en Francais avec le Jour et le Mois
///////////////////////////////////////////////////////////////////////////////////////////////////////////

function date_fr($timestamp_date)
{
	$mois=array('Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre');
	$jour=array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
	
	$mois_fr = $mois[date('n', $timestamp_date)-1];
	$jour_fr = $jour[date('w', $timestamp_date)];
	
	$date_fr = $jour_fr . " " . date('d', $timestamp_date) . " " . $mois_fr . " " . date('Y', $timestamp_date);
	
	return $date_fr;
}


?>