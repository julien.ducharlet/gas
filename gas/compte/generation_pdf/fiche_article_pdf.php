<?php 
/* */

/* Initialisations */
		
	require("../../includes/configure.php");
	require("../../includes/fonctions/general.php");
	require(ABSOLUTE_DIR . "/gas/includes/fonctions/base_de_donnees.php");
	
	require(DATABASE_TABLE_DIR);
	require('fpdf.php');
	
	/* VARIABLES GLOABLES */
	define('EURO',chr(128));
	define('SOFT_BACKGROUND', '240, 239, 240');
	
	$taxe = taux_taxe();
	$customer_type = 1;
	$products_options = array();
	/* */
	
	tep_db_connect();

/* query client */
if(isset($_GET['cID'])) {
	
	$query_customer = 'select customers_type from '. TABLE_CUSTOMERS .' c where customers_id='. (int)$_GET['cID'];
	$query_customer = tep_db_query($query_customer);
	
	$data_customer = tep_db_fetch_array($query_customer);
	$customer_type = $data_customer['customers_type'];
}

	
/* R�cup�ration des donn�es pour la fiche article */
	$id_article = (int)$_GET['id_article'];
	
	$query_article = 'select p.products_id, products_name, products_bimage, products_description, products_model, products_ref_origine, part_price_by_1, part_price_by_5, part_price_by_10, ';
	$query_article .= 'pro_price_by_1, pro_price_by_5, pro_price_by_10, rev_price_by_1, rev_price_by_5, rev_price_by_10, adm_price_by_1, adm_price_by_5, adm_price_by_10 ';
	$query_article .= 'from '. TABLE_PRODUCTS .' p, '. TABLE_PRODUCTS_DESCRIPTION .' pd, '. TABLE_PRODUCTS_PRICE_BY_QUANTITY .' ppbq ';
	$query_article .= 'where p.products_id=pd.products_id and ppbq.products_id=pd.products_id and p.products_id='. $id_article;
	
	$query_article = tep_db_query($query_article);
	$data_article = tep_db_fetch_array($query_article);
	
	
	/* options du produit */
	$query_options = 'select products_options_name, products_options_values_name as option_name from '. TABLE_PRODUCTS_ATTRIBUTES .' pa,
						'. TABLE_PRODUCTS_OPTIONS_VALUES .' pov,
						'. TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS .' povtopo,
						'. TABLE_PRODUCTS_OPTIONS .' po ';
	$query_options .= 'where pa.options_values_id=pov.products_options_values_id
						and pov.products_options_values_id=povtopo.products_options_values_id
						and povtopo.products_options_id=po.products_options_id
						and options_dispo = \'1\' and products_id='. $id_article .' ';
	$query_options .= 'order by products_options_values_name';
	$query_options = tep_db_query($query_options);
	
	$i = 0;
	
	while($data_options = tep_db_fetch_array($query_options)) {
		
		if($i++ ==0) $type_option = $data_options['products_options_name'];
		
		$products_options[] = $data_options['option_name'];
	}
	
	/* v�rfiier s'il y a une promotion pour ce produit */
	$req_promo = tep_db_query("select specials_new_products_price, expires_date from ". TABLE_SPECIALS ." where products_id=". $id_article ." and (expires_date >NOW() or expires_date='0000-00-00 00:00:00')");
	$res_promo = tep_db_fetch_array($req_promo);
	$article_in_promo = (tep_db_num_rows($req_promo) == 0) ? false : true;
	
	switch($customer_type) {
		
		case 1 : //particulier
			$prix_by_1 = $data_article['part_price_by_1'];
			$prix_by_5 = $data_article['part_price_by_5'];
			$prix_by_10 = $data_article['part_price_by_10'];
			
			$type = 'particuliers';
			break;
			
		case 2 : //professionnel
			$prix_by_1 = $data_article['pro_price_by_1'];
			$prix_by_5 = $data_article['pro_price_by_5'];
			$prix_by_10 = $data_article['pro_price_by_10'];
			
			$type = 'professionnels';
			break;
		
		case 3 : //revendeur
			$prix_by_1 = $data_article['rev_price_by_1'];
			$prix_by_5 = $data_article['rev_price_by_5'];
			$prix_by_10 = $data_article['rev_price_by_10'];
			
			$type = 'revendeurs';
			break;
			
		case 4 : //administration
			$prix_by_1 = $data_article['adm_price_by_1'];
			$prix_by_5 = $data_article['adm_price_by_5'];
			$prix_by_10 = $data_article['adm_price_by_10'];
			
			$type = 'administrations';
			break;
		
		default : 
			$prix_by_1 = $data_article['part_price_by_1'];
			$prix_by_5 = $data_article['part_price_by_5'];
			$prix_by_10 = $data_article['part_price_by_10'];
			break;
	}

/* Cr�ation de la page */

//$PDF = new PDF_HTML();
$PDF = new fpdf();
$PDF->AddPage();
$PDF->SetMargins(2,2);
$PDF->SetDisplayMode(fullpage,continuous);

$PDF->SetAuthor('GeneralArmyStore',true);
/* HEADER */

	//On change de page automatiquement d�s qu'on atteint la limite de 1.0cm en bas de la page
	$PDF->SetAutoPageBreak(true , 10);
	
	//Ajout du logo
	/*$PDF->Image("./logo.jpg", 10, 5, 32, 32, '');
	
	//$PDF->Image("./head_facture.jpg", 42, 5, 100, 32);
	
	//image adresse du site
	//$PDF->Image("./head_facture_gris.jpg", 42, 5, 83, 32, '');
	
	//ligne verte en dessous du logo
	$PDF->SetFont('Arial','',11);
	$PDF->SetFillColor(220, 34, 19);
	$PDF->SetXY(0,40);
	$PDF->MultiCell(220,0.4,'',0,'R',1);
/* FIN HEADER */







/* Date de la facture */

	
	$PDF->SetTextColor(0, 0, 0);
	$PDF->SetFont('Arial','',11);
	$PDF->SetXY(105,4);
	$PDF->Cell(100,5,'Fiche du '. dat(date("Y-m-d")), 0, 1,'R');
	
	//nom article
	$PDF->SetXY(0,55);
	$PDF->SetFillColor(SOFT_BACKGROUND);
	$PDF->Cell(220, 7, html_entity_decode($data_article['products_name']), 0, 0, 'C', 1);
	
	
	
	/* IMAGE DE L'ARTICLE */
	$PDF->Image("../../images/products_pictures/normale/". $data_article['products_bimage'], 20, 67, 75, 75, '');
		
	
	
	
	
	
/* BOX DE DROITE */
	//Adresse de Livraison
	$PDF->SetXY(110, 67);
	/*$PDF->SetFillColor(184, 170, 133);
	$PDF->Cell(7,40,'',0,0,'C',1);*/

	$PDF->SetX(117);
	$PDF->SetFillColor(SOFT_BACKGROUND);
	$PDF->Cell(83,80,'',0,0,'C',1);

	$PDF->SetXY(118, 87);
	$PDF->SetFont('Arial','B',10);
	$PDF->SetTextColor(0,0,0);

	$PDF->SetXY(122, 70);
	$PDF->SetFont('Arial','',10);
	
	if($article_in_promo) {
		
		$text_right_box = "Prix promotionnel : ". format_to_money($res_promo['specials_new_products_price']*$taxe) . EURO;
		
		if(empty($res_promo['expires_date']) || $res_promo['expires_date']!='0000-00-00 00:00:00') {
			
			$text_right_box .= "\nValable jusqu'au ". dat($res_promo['expires_date']);
		}
		
	}
	else {
		
		$text_right_box = 'Prix pour les '. $type ."\n\n";
		
		if($prix_by_1==0) {
			
			$text_right_box .= "Prix unitaire : ". format_to_money($data_article['products_price']*$taxe) ." ". EURO ." \nPrix par 5 : ". format_to_money($data_article['products_price']*$taxe) ." ". EURO ." \nPrix par 10 : ". format_to_money($data_article['products_price']*$taxe) ." ". EURO;
		}
		else {
			
			$text_right_box .= "Prix unitaire : ". format_to_money($prix_by_1*$taxe) ." ". EURO ." \nPrix par 5 : ". format_to_money($prix_by_5*$taxe) ." ". EURO ." \nPrix par 10 : ". format_to_money($prix_by_10*$taxe) ." ". EURO;
		}
	}
	
	//$text_right_box .= html_entity_decode("\n\nR&eacute;f&eacute;rence : ". ((!empty($data_article['products_ref_origine'])) ? $data_article['products_ref_origine'] : $data_article['products_model']));
	$text_right_box .= html_entity_decode("\n\nPour retrouver cet article sur notre boutique, vous devez taper la r&eacute;f&eacute;rence suivante dans notre moteur de recherche : \n". $data_article['products_ref_origine']);
	
	$PDF->MultiCell(69, 4, $text_right_box, 0, 'C', 0);
	
	
	
	
/* DESCRIPTION */
	$description = "Description de l'article : \n";
	$description .= _substr(ereg_replace("<[^>]*>", "", $data_article['products_description']), 580);
	
	// barre rouge
	$PDF->SetFillColor(220, 34, 19);
	$PDF->SetXY(10,155);
	$PDF->Cell(190,0.2,'',0,0,'C',1);
	
	//cadre
	$PDF->SetXY(10, 157);
	$PDF->SetFillColor(SOFT_BACKGROUND);
	$PDF->Cell(190, 91, '', 0, 1, 'L', 1);
	
	//texte
	$PDF->SetXY(10,160);
	$PDF->MultiCell(190, 5, $description, 0, 'L', 0);
	
	
/* CADRE BLANC*/
	$PDF->SetFillColor(255, 255, 255);
	$PDF->SetXY(10, 226.3);
	$PDF->Cell(190, 30,'', 0, 0, 'C', 1);

	
	
	
	/* LISTE DES OPTIONS */
	if(sizeof($products_options) > 0) {
		
		$options = $type_option ." disponibles : \n";
		$options .= implode(" | ", $products_options);
		
		$PDF->SetFillColor(136, 140, 7);
	
		$PDF->SetXY(10,230);
		$PDF->Cell(190,0.2,'',0,0,'C',1);
		
		$PDF->SetXY(10,230.3);
		$PDF->SetFillColor(SOFT_BACKGROUND);
		$PDF->Cell(190, 35, '', 0, 1, 'L', 1);
		
		$PDF->SetXY(10,230);
		$PDF->MultiCell(190, 5, $options, 0, 'L', 0);
	}
	
	
	

/* Footer */

	//S�parateur corps/pied de page
/*
	$PDF->SetFillColor(136, 140, 7);

	$PDF->SetXY(0,-24.8);
	$PDF->MultiCell(210,0.4,'',0,'R',1);

	//Pied de page
	$PDF->SetXY(0,-20);
	$PDF->SetFont('Arial','',8);
	$PDF->SetTextColor(0, 0, 0);
	$PDF->MultiCell(210,4,html_entity_decode("General Army Store - 21, Rue Pasteur - Entr&eacute;e rue H. Bajard - 26260 - St Donat sur l'Herbasse\nRCS 447 513 342 - Siret 44751334200033 - Soci&eacute;t&eacute; au capital de 7500" . EURO . " - TVA intracommunautaire FR 92 447513342 - A.P.E. 4771Z"),0,'C',0);
*/
	//On cr�e un fichier . avec la facture
	//$PDF->Output("facture.", "F");
	
	//On l'affiche � l'�cran
	$PDF->Output();
	
	//-------------------------------- Fin du code de la g�n�ration de la facture --------------------------------//








/*Fonction qui met un nombre au format mon�taire (15.00 au lieu de 15 ou 12.60 au lieu de 12.6 ou encore 5.65 au lieu de 5.6497)
  Param : $valeur valeur � convertir*/
function FormatToMoney($valeur)
{
	if(strpos($valeur,",")){
		$nbDecimales = strlen(strrchr($valeur, ","));
	}
	elseif(strpos($valeur,".")){
		$nbDecimales = strlen(strrchr($valeur, "."));
	}
	
	if ($nbDecimales > 2){
		$valeur = round($valeur, 2);
		$nbDecimales = strlen(strrchr($valeur, "."));
	}
	if ($nbDecimales == 0)
		$valeur = $valeur . ".00";
	else if($nbDecimales == 1)
		$valeur = $valeur . "00";
	else if($nbDecimales == 2)
		$valeur = $valeur . "0";
	
	return $valeur;
}

/*Fonction qui ajoute un article au tableau de la commande
  Params : */
function AjoutArticle($qtte, $nom, $prix, $tax, $options_name, $options_attributes)
{
	global $PDF;
	
	if ($options_name != ''){
		$article_nom = $nom . " (" . $options_name . " : " . $options_attributes . ")";
	} else {
		$article_nom = $nom;
	}
	
	$PDF->SetFillColor(136, 140, 7);
	
	$PDF->SetX(17);
	$PDF->Cell(0.2,5,'',0,0,'C',1);

	$PDF->SetX(127);
	$PDF->Cell(0.2,5,'',0,0,'C',1);

	$PDF->SetX(145);
	$PDF->Cell(0.2,5,'',0,0,'C',1);

	$PDF->SetX(163);
	$PDF->Cell(0.2,5,'',0,0,'C',1);

	$PDF->SetX(182);
	$PDF->Cell(0.2,5,'',0,0,'C',1);


	$PDF->SetFont('Arial','',7);
	$PDF->SetX(10);
	$PDF->Cell(7,5,$qtte,0,0,'C',0);
	$PDF->SetFont('Arial','',6);
	$PDF->Cell(110,5,$article_nom,0,0,'L',0);
	$PDF->SetFont('Arial','',7);
	
	$prix_ht = FormatToMoney($prix);
	$PDF->Cell(18,5,$prix_ht . " " . EURO,0,0,'C',0);
	
	$total_ht=$qtte*$prix;
	$total_ht = FormatToMoney($total_ht);
	$PDF->Cell(18,5,$total_ht . " " . EURO,0,0,'C',0);
	
	$prix_ttc = FormatToMoney($prix*(1+($tax/100)));
	$PDF->Cell(18,5,$prix_ttc . " " . EURO,0,0,'C',0);
	
	$total = FormatToMoney($qtte*$prix*(1+($tax/100)));
	$PDF->Cell(18,5,$total . " " . EURO,0,1,'C',0);
	
	return $total;
}
?>