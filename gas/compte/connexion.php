<?php
/* Page permettant � un client de se connecte � son compte, ou bien a un utilisateur d'avoir acc�s � aux differentes partie des inscriptions */

$nom_page = "inscription-particulier";

require("../includes/page_top.php");
require("../includes/meta_head.php");
include("fonction/session.php");
include("fonction/new_password.php");

$erreur="";
if (isset($_REQUEST['connexion'])) { $erreur.=create(); }
if (isset($_REQUEST['mdp_oublie'])) { $erreur.=verif_mail($_POST['mail_mdp_oublie']); }
if (isset($_POST['logout'])) { destroy(); }
require("../includes/header.php");
?>

<div id="corps">
	<div id="bande_rubrique_g">
		<div id="bande_image">
			<img src="../template/base/compte/icones/bonhomme.png" alt="info commande" />
		</div>
		<div id="bande_titre">Connexion � votre compte</div>
		<div id="bande_sous_titre"></div>
	</div>
	<div style="clear:both;"></div>

	
	<?php 
	if (!empty($erreur)) { echo $erreur; }
	?>
	
	<!-- block text -->
	<? // DEBUT ZONE INSCRIPTION ?>
	<div  style="width:100%; margin-top:10px; margin-right:0px; float:right;">
		<div class="titre_box-contenu">Vous n'avez pas encore de compte sur notre site ?</div>
		<div class="box-contenu">
			<div style="padding-top:10px;">
				<div class="item_i">Cliquez sur le bouton ci-dessous pour vous inscrire.</div>
			</div>			
			<div class="clear"></div>
			<div style="margin-top:10px;">
				<a href="inscription.php" class="button" title="Connexion">
					Je cr�e mon compte
				</a>
				<!--<button class="button">Click Me</button>-->
				<!--<a href="javascript:document.formu.submit();" title="Valider votre inscription"><img src="../template/base/boutons/bouton_valider_inscription.jpg" alt="bt valider" /></a>-->
			</div>
		</div>
	</div>
	<? // FIN ZONE INSCRIPTION ?>
	<!-- fin block text -->
	
	
	<!-- block text -->
	<? // DEBUT ZONE INFOS CONNEXION ?>
	<? $get_panier = !empty($_GET['panier']) ? $_GET['panier'] : ''; ?>
	<form id="addressForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<input type="hidden" name="panier" value="<?php echo $get_panier; ?>" />
		<div  style="width:100%; margin-top:20px; margin-right:0px; float:right;">
			<div class="titre_box-contenu">Saisissez vos informations de connexion</div>
			<div class="box-contenu">
				<div class="conteneur_desc_item" style="padding-top:10px;">
					<div class="item_i">Adresse e-mail :</div>
					<div class="item_i">Mot de passe :</div>
				</div>
				<div class="conteneur_composant_item"  style="padding-top:10px;">
					<div class="item_i">
						<input type="text" name="mail" size="25" id="mail" value="<?php if(isset($_POST["mail"])){echo $_POST["mail"];}?>" style="text-transform:Lowercase;" autocomplete="address-level4" /> 
					</div>
					<div class="item_i">
						<? $post_mdp = !empty($_POST['mdp']) ? $_POST['mdp'] : ''; ?>
						<input type="password" name="mdp" id="mdp" size="25" value="<?php echo $post_mdp; ?>"  />
						<input type="hidden" name="connexion" value="ok" />
					</div>
				</div>				
				<div class="clear"></div>
				<div style="margin-top:10px;">
					<a class="button" onclick="document.getElementById('addressForm').submit();" title="Connexion">
						Connexion
					</a>
					<!--<button class="button">Click Me</button>-->
					<!--<a href="javascript:document.formu.submit();" title="Valider votre inscription"><img src="../template/base/boutons/bouton_valider_inscription.jpg" alt="bt valider" /></a>-->
				</div>
			</div>
		</div>
		<? // FIN ZONE INFOS CONNEXION ?>
		<!-- fin block text -->	
	</form>
	

	
	<!-- block text -->
	<? // DEBUT ZONE MOT DE PASSE OUBLIE ?>
	<div style="width:100%; margin-top:20px; margin-right:0px; float:right;">
		<div class="titre_box-contenu">Vous avez oubli� votre mot de passe ?</div>
		<form id="MotDePasseOublie" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<div class="box-contenu">
			<div style="padding-top:20px;">
				Veuillez indiquer ci-dessous l'adresse e-mail utilis�e lors de la cr�ation <br>de votre compte pour recevoir un nouveau mot de passe.<br />
			</div>
			<div class="conteneur_desc_item">
				<div class="item_i">Adresse e-mail :</div>
			</div>
			<div class="conteneur_composant_item">
				<div class="item_i">
					<? $post_mail_mdp_oublie = !empty($_POST['mail_mdp_oublie']) ? $_POST['mail_mdp_oublie'] : ''; ?>
					<input type="text" name="mail_mdp_oublie" size="25" id="email" value="<?php echo $post_mail_mdp_oublie; ?>" style="text-transform:Lowercase;" autocomplete="address-level4" />
					<input type="hidden" name="mdp_oublie" value="ok" />
				</div>
			</div>				
			<div class="clear"></div>
			<div style="padding-top:10px;">
				<!--<a class="button" onclick="rech();" title="Demander un nouveau mot de passe">-->
				<a class="button" onclick="document.getElementById('MotDePasseOublie').submit();" title="Connexion">
					Demander un nouveau mot de passe
				</a>
			</div>
			<div style="padding-top:10px;">
				Si vous avez chang� d'adresse e-mail, contactez notre <a href="../contact.php" title="Contatez notre service client">service client</a>.
			</div>
		</div>
		</form>
		<br><br><br>		
	</div>
	<? // FIN ZONE MOT DE PASSE OUBLIE ?>
	<!-- fin block text -->		

	<div class="clear"></div>
	
</div>
<?php  
require("../includes/footer.php");
require("../includes/page_bottom.php");

?>