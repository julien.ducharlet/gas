<?php
/* Page affichant les details d'un devis */

$nom_page = "devis_details";

require("../includes/page_top.php");
include("fonction/session.php");
require("fonction/devis_traitement.php");

if(!empty($_SESSION['customer_id'])) {

	require("../includes/meta_head.php");
	
	$id_commande = (int)$_GET['id_com'];
	$_SESSION['id_devis'] = (int)$_GET['id_com'];
	
	$commande_query = tep_db_query("SELECT * FROM ".TABLE_ORDERS." WHERE orders_id = '" . $id_commande . "'");
	$commande_data = tep_db_fetch_array($commande_query);
	
	if ($_SESSION['customer_id']==$commande_data['customers_id']) {
		
		/* récupération des articles du devis */
		$articles_query = tep_db_query("SELECT orders_products_id, products_model, products_name, final_price, products_quantity , cpath FROM ".TABLE_ORDERS_PRODUCTS." WHERE orders_id = '" . $id_commande . "'");
		$nbArticles = tep_db_num_rows($articles_query);
		
		// QUEL UTILITE ONT LES LIGNES SUIVANTES CAR UTILE UNIQUEMENT DANS orders_detail.php ???
		$history_command = tep_db_query("SELECT date_added, comments, track_num, track_num2,  track_num3, track_num4, orders_status_name, o.orders_status_id FROM ".TABLE_ORDERS_STATUS_HISTORY." os, ".TABLE_ORDERS_STATUS." o WHERE os.orders_status_id=o.orders_status_id AND orders_id = '" . $id_commande . "' AND os.orders_status_id=4");
		$nbcolis_send = tep_db_num_rows($history_command);
		
		require("../includes/header.php");

		if(isset($_POST['valider'])) {
			
			if(($_SESSION['devis_moyen_paiement']=='4' && !empty($_SESSION['remise_panier_devis']) && $_POST['montant']==0) || $_SESSION['devis_moyen_paiement']!='4') {
				$_SESSION['total_panier'] = $_POST['montant'];
				devis_traitement();
			}
		}
		?>

		<SCRIPT type="text/javascript" src="fonction/devis.js"></script>
		<div id="corps">
			<div id="bande_rubrique_g">
				<div id="bande_image">
					<img src="../template/base/compte/icones/commande.png" alt="inf_commande" />
				</div>
				<div id="bande_titre">Votre devis N&deg; : <span style="color:#e64d21;"><strong><?php echo $id_commande;?></strong></span></div>
				<div id="bande_sous_titre">Le d&eacute;tail du devis</div>
			</div>
			<div id="bande_rubrique_d">
				<a href="devis.php" title="Retourner &agrave; mon compte"><img src="../template/base/compte/boutons/bouton_retour-devis.png" alt="retour" /></a>
			</div>
			<div style="clear:both;"></div>
			<!-- entete -->
			<div id="block_info">
				<div class="block_bordure_violette"></div>
				<div style=" float:left; text-align:left; margin-top:10px; margin-left:10px; width:85%; font-size:0.9em;">
					Num&eacute;ro de devis : <span style="color:#e64d21;"><strong><?php echo $id_commande;?></strong></span><br/><br/>
					Devis du :
					<strong>
						<?php 
							$timestamp_commande = strtotime($commande_data['date_purchased']);
							echo   $date_commande = html_entity_decode(date_fr($timestamp_commande));
						?>
					</strong>
					<br/><br/>
					<?php 
					$id_facture = $commande_data['orders_numero_facture'];
					if($id_facture!=0)
					{
					?>
					Devis N&deg;
					<strong>
						   <a style="text-decoration:none; color:#C33;" href="generation_pdf/facture.php?id_com=<?php echo $id_commande; ?>" target="_blank" title="Votre devis au format PDF ">
								<?php echo $id_facture; ?>
						  </a>
					</strong>
					<br/><br/>
					Du :
					<strong>
						<?php
						$timestamp_facture = strtotime($commande_data['orders_date_facture']);
						echo $date_facture = html_entity_decode(date_fr($timestamp_facture));
						?>
					</strong><br/><br/>
					<?php } ?>
				</div>
			</div>

			<div id="block_info_facturation">
				<div class="block_bordure_verte"></div>
				<div>
				<?php
					echo'<div style="float:left; text-align:left; margin-left:10px; margin-top:10px;"><strong>Adresse de facturation</strong> </div>';
					echo'<div style="float:left; text-align:left; margin-left:20px; margin-top:10px; width:80%;">';
								
					if ($commande_data['customers_company'] != '') 
						echo $commande_data['customers_company'].'<br>'; // Societe
					if ($commande_data['customers_name'] != '') 
						echo $commande_data['customers_name'].'<br>'; // Nom + Prénom
					
					if ($commande_data['customers_street_address_3'] != '') 
						echo  $commande_data['customers_street_address_3'].'<br>'; //Adresse 1
					if ($commande_data['customers_suburb'] != '') 
						echo  $commande_data['customers_suburb'].'<br>'; //Adresse 2
					if ($commande_data['customers_street_address'] != '') 
						echo $commande_data['customers_street_address'].'<br>'; //Adresse 3
					if ($commande_data['customers_street_address_4'] != '') 
						echo  $commande_data['customers_street_address_4'].'<br>'; //Adresse 4	
					
					echo $commande_data['customers_postcode'].'&nbsp;&nbsp;'.$commande_data['customers_city'].'<br>';
					echo $commande_data['customers_country'];		
		
					echo'</div><div style="clear:both;"></div>';
				 ?>
				</div>
			</div>
			<div id="block_info_livraison">
				<div class="block_bordure_violette"></div>
				  <div><?php
						echo'<div style="float:left; text-align:left; margin-left:10px; margin-top:10px;"><strong>Adresse de livraison</strong> </div>';
						echo'<div style="float:left; text-align:left; margin-left:20px; margin-top:10px; width:80%;">';
						
						$facture_entreprise = $commande_data['delivery_company'];
						
						if ($commande_data['delivery_company'] != '') 
						echo $commande_data['delivery_company'].'<br>'; // Societe
					if ($commande_data['delivery_name'] != '') 
						echo $commande_data['delivery_name'].'<br>'; // Nom + Prénom
					
					if ($commande_data['delivery_street_address_3'] != '') 
						echo  $commande_data['delivery_street_address_3'].'<br>'; //Adresse 1
					if ($commande_data['delivery_suburb'] != '') 
						echo  $commande_data['delivery_suburb'].'<br>'; //Adresse 2
					if ($commande_data['delivery_street_address'] != '') 
						echo $commande_data['delivery_street_address'].'<br>'; //Adresse 3
					if ($commande_data['delivery_street_address_4'] != '') 
						echo  $commande_data['delivery_street_address_4'].'<br>'; //Adresse 4	
					
					echo $commande_data['delivery_postcode'].'&nbsp;&nbsp;'.$commande_data['delivery_city'].'<br>';
					echo $commande_data['delivery_country'];		
						
						echo'</div><div style="clear:both;"></div>';
						
						$req_id_pays=tep_db_query("select countries_id from countries where countries_name='".$commande_data['delivery_country']."'");
						$res_country_id=tep_db_fetch_array($req_id_pays);
				?>
				</div>
			</div>
			<div style="clear:both;"></div>
			<!-- fin de l'entete -->

			<!-- detail de la commande -->
			<div class="petit_titre">Votre devis contient</div>
			<div style="clear:both;"></div>
			<div class="commande_detail_barre_info">
				<div  style="float:left; width:100px;"><strong>Qt&eacute;</strong></div>
				<div  style="float:left; width:650px; text-align:left;"><strong><?php if($nbArticles>1)echo'Articles'; else echo'Article';?></strong></div>
				<div  style="float:left; width:95px;">
					<strong>
					Prix Unit. <?php if (parametrage_affiche_prix($res_country_id['countries_id']) == 1.2) echo 'TTC'; else echo 'HT';?>
					</strong>
				</div>
				<div  style="float:left; margin-left:10px; width:95px;">
					<strong>
					Prix Total <?php if (parametrage_affiche_prix($res_country_id['countries_id']) == 1.2) echo 'TTC'; else echo 'HT';?>
					</strong>
				</div>
				<div style="clear:both;"></div>
			</div>
			<div style="clear:both;"></div>
			<?php
			while ($articles_data = tep_db_fetch_array($articles_query)) {
			?>
			<div class="commande_detail_barre_info" style="background-color:#FFF; color:#000; border-style:solid;  border-color:#ae69c2; border-width:0px 0px 2px 0px;">
				<div  style="float:left; width:100px;"><strong><?php echo $articles_data['products_quantity']; ?></strong></div>
				<div  style="float:left; width:650px; text-align:left;"><strong>
				<?php 
				echo $articles_data['products_name'];
				$req_option=tep_db_query("select products_options, products_options_values from ".TABLE_ORDERS_PRODUCTS_ATTRIBUTES." where orders_products_id=".$articles_data['orders_products_id']."");
				   $nb_option=tep_db_num_rows($req_option);
				   if($nb_option==1) {
					   $res_option=tep_db_fetch_array($req_option);
					   echo '<br />'.$res_option['products_options'].' : '.$res_option['products_options_values'];
				   }
				   
				?></strong></div>
				<div  style="float:left; width:95px;"><strong><?php echo format_to_money($articles_data['final_price']*parametrage_affiche_prix($res_country_id['countries_id'])).' &euro;';?></strong></div>
				<div  style="float:left; margin-left:10px; width:95px;">
					 <strong><?php echo format_to_money(($articles_data['final_price']*$articles_data['products_quantity'])*parametrage_affiche_prix($res_country_id['countries_id'])).' &euro;';?></strong>
				</div>
				<div style="clear:both;"></div>
			</div>
			<div style="clear:both;"></div>
			<?php
			}
			?>
			<div style="float:right; text-align:right; margin-top:30px;">
			<?php
				$sous_total = $commande_data['ss_total'];
				$_SESSION['ss_total_devis'] = $sous_total;
				
				echo 'Sous-Total HT: '. $sous_total .' &euro;';
				echo'<br />';
				if((isset($_SESSION['devis_moyen_paiement']) && !empty($_SESSION['devis_moyen_paiement'])) && $_SESSION['devis_moyen_paiement']==4) {
					
					$somme_av = tep_db_query("select customers_argent_virtuel from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['customer_id']."");
					$res_av = tep_db_fetch_array($somme_av);
							
					if($res_av['customers_argent_virtuel'] > $commande_data['total']){
						
						$remise = $commande_data['ss_total']+$commande_data['frais_port_client'];
						$_SESSION['remise_panier_devis'] = $remise;
					}
					else{
						
						$remise = $res_av['customers_argent_virtuel'];
						$_SESSION['remise_panier_devis'] = $remise;
					}
				}
				elseif(!empty($_SESSION['remise_panier_devis'])) {
					
					$remise = $_SESSION['remise_panier_devis'];
				}
				else {
					
					$remise = 0;
					$_SESSION['remise_panier_devis'] = 0;
				}
				
				echo 'Frais de livraison HT : '.$fdp = $commande_data['frais_port_client'].' &euro;<br />';
				
				if($remise!=0) echo 'Remise HT : '.$remise.' &euro;<br />';
				
				if (parametrage_affiche_prix($res_country_id['countries_id']) == 1.2) {
					echo 'TVA Fran&ccedil;aise 20% : '.$_SESSION['tva_panier_devis']=$tva = format_to_money(($sous_total+$fdp-$remise)*0.2).' &euro;<br />';
				}
				else $tva = 0;
				
				$total = $commande_data['ss_total']+$commande_data['frais_port_client']-$remise+$tva;
				$_SESSION['total_panier_devis'] = $total;
				
				echo 'Total &agrave; Payer : <span style="color:#e64d21; font-size:14px;"><strong>'. format_to_money($total) .'&euro;</strong></span><br />';
			?>
			</div>
			<div style="clear:both;"></div>

			<?php 
			$today = date("Y-m-d H:i:s");
			$nbjours = round((strtotime($today) - strtotime($commande_data['orders_date_fin_devis']))/(60*60*24)-1);
			if ($nbjours<0) {
				?>											
				<!-- Zone de Paiment du DEVIS -->
				<div class="petit_titre"><strong>S&eacute;lectionnez le mode de r&egrave;glement que vous souhaitez utiliser : </strong></div>
				<div style="clear:both;"></div>
				<div id="moyen_paiement" >

					<?php 
					$i=0;
					$moyen_p = tep_db_query("SELECT * FROM ".TABLE_MODULE_PAIEMENT." WHERE paiement_visible='1' AND  paiement_montant_max>".$commande_data['total']." AND paiement_id != 5 AND paiement_id != 8 ORDER BY paiement_ordre_affichage");
					while($res_paiement=tep_db_fetch_array($moyen_p)) {
						
						if($res_paiement['paiement_id']==4) {
							$somme_av=tep_db_query("select customers_argent_virtuel from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['customer_id']."");
							$res_av=tep_db_fetch_array($somme_av);
						}
                        else {
                            $res_av = 0;
                        }
						
						//if($res_paiement['paiement_id']==4 && !empty($_SESSION['remise_panier_devis']))    {							
						//} else {
							
							
							
							
							echo '<div style="';
                            // JULIEN 12/03
                            // add condition (isset($_SESSION['montant_panier_admin']) 
							if(
							($res_av['customers_argent_virtuel']<=0 && $res_paiement['paiement_id']==4 )
							|| 
							($res_paiement['paiement_id']==4 && (isset($_SESSION['montant_panier_admin']) && $_SESSION['montant_panier_admin']>0) && $_SESSION['remise_porte_monnaie_admin']>0)
							||
							($_SESSION['customers_type']!=4 && $res_paiement['paiement_id']==7)
							) echo ' display:none; ';
							
							echo 'height:50px; background-image:url(../template/base/images_paiement/fond_moyen_de_paiement.gif); background-repeat:no-repeat; margin-top:10px; ">
									  
							<div style="float:left; width:250px; text-decoration:underline; text-align:left; height:30px; color:#8e2eaa; font-size:12px; margin-left:15px; margin-top:15px; "><strong>Paiement par '.$res_paiement['paiement_nom'].'</strong>';
							echo'</div>';
							  
							echo '<div style="float:left; margin-left:10px; margin-top:18px;  font-size:11px;  color:#788403;">';
								if($res_av['customers_argent_virtuel']>0 && $res_paiement['paiement_id']==4){
									  echo "Utilisez les <span style='color:#df4e02;'>".($res_av['customers_argent_virtuel'])." &euro;</span> que vous avez dans votre porte monnaie virtuel";
								} else { echo $res_paiement['paiement_description']; }
							echo'</div>					  
							<div style="float:right; margin-top:15px; margin-right:25px;">
								<input ';
								if((isset($_SESSION['devis_moyen_paiement']) && !empty($_SESSION['devis_moyen_paiement'])) && $_SESSION['devis_moyen_paiement']==$res_paiement['paiement_id']) 
									 {echo'checked="checked"';}
									 
								echo ' type="radio" id="paiement'.$i.'" name="supri" onclick="paiement('.$res_paiement['paiement_id'].','.$_GET['id_com'].');" />
							</div>
							
							<div style="float:right; margin-top:5px; margin-right:10px;">
								<img src="../template/base/images_paiement/'.$res_paiement['paiement_img'].'" />
							</div>
								  
						  </div>';
						  $i++;
						  
						  
						  
						//}
					}
					echo'<input type="hidden" id="compteur" name="compt" value="'.$i.'" />';
				?>
				</div>

					<?php
					if((isset($_SESSION['devis_moyen_paiement']) && !empty($_SESSION['devis_moyen_paiement']))){
						?>
						<div style="float:left; margin-top:10px;">
										<input type="checkbox" id="cgv" name="cgv_b"  />
										<strong>J'ai lu et accept&eacute; les <a href="../conditions_generales.php#article_2">Conditions g&eacute;n&eacute;rales de vente !</a></strong>
						</div>
						<div style="margin-top:40px;">
						  <form name="valider_devis" action="devis_details.php?id_com=<?php echo $_GET['id_com']; ?>" method="post">
							<input type="hidden" name="valider" value="oui" />
							<input type="hidden" name="montant" value="<?php echo $total; ?>" />
							<img src="../template/base/panier/bouton_finir-cmd.png" title="finaliser ma commande" alt="finaliser commande" style="cursor:pointer;" onclick="validation()"/>
						   </form>
						</div>
						<?php 
					}

			} else {
				echo '<br /><br /><span style="size:18px; color:red;";><strong>La date d\'expiration du devis &agrave; &eacute;t&eacute; atteinte. Merci de nous contacter au '. TEL_NORMAL .' pour plus d\'information.</strong></span><br /><br />';
			}
			?>
		</div>
	<?php 
	}  // FIN if($_SESSION['customer_id']==$commande_data['customers_id']){
		
} else { // Si le client n'est plus connecté retour à la page de login
	header('Location: connexion.php');
}
require("../includes/footer.php");
require("../includes/page_bottom.php"); 


?>