<?php
/* Page informant au client que sa deconnexion a bien �t� prise en compte */
   
$nom_page = "logout";
   
require("../includes/page_top.php");
require("../includes/meta_head.php");
require("../includes/languages/french/logout.php");
include("fonction/session.php");
include("fonction/new_password.php");


//destruction de toute les variables session utilis�es lors de la connexion au compte client 
if ($_GET['logout'] == "true") {
	unset($_SESSION['master']);
	unset($_SESSION['commentaire_commande']);
	unset($_SESSION['customer_id']);
	unset($_SESSION['customers_firstname']);
	unset($_SESSION['customers_type']);
	unset($_SESSION['customers_compatibilite']);
	unset($_SESSION['customers_vip']);
	unset($_SESSION['customer_country_id']);
	unset($_SESSION['frais_livraison']);
	unset($_SESSION['adresse_livraison']);
	unset($_SESSION['mode_paiement']);
	unset($_SESSION['poids_commande']);
	unset($_SESSION['fdp_panier']);
	unset($_SESSION['tvap_panier']);
	unset($_SESSION['ss_total_panier']);
	unset($_SESSION['tva_total_panier']);
	unset($_SESSION['total_panier']);
	unset($_SESSION['code_promo']);
	unset($_SESSION['coupon_frais_de_port']);
	unset($_SESSION['coupon_affectation_remise']);
	unset($_SESSION['type_reduc']); 
	unset($_SESSION['montant_reduc']);
	unset($_SESSION['coupon_minimum_order']);
	unset($_SESSION['coupon_date_deb']);
	unset($_SESSION['coupon_date_fin']);
	unset($_SESSION['parrainage_kdo']);
	unset($_SESSION['remise_coupon']);
	unset($_SESSION['remise_par_coupon']);
	unset($_SESSION['remise_porte_monnaie']);
	unset($_SESSION['id_devis']);
	unset($_SESSION['devis_moyen_paiement']);
	unset($_SESSION['ss_total_devis']);
	unset($_SESSION['remise_panier_devis']);
	unset($_SESSION['tva_panier_devis']);
	unset($_SESSION['total_panier_devis']);
	unset($_SESSION['bda_id_client']);
}

require("../includes/header.php");

?>

<div id="corps">
<div id="deconnexion">
<div class="titre">D&eacute;connexion de votre compte</div>
            <div id="img_logout"></div>
    <!-- block text -->
            <div id="conteneur_text_logout">
                <div  class="bordure_haut_verte_logout"></div>
                <div class="bordure_pixel_vert_logout">
                    <div id="text_logout"> 
                        <?php echo TEXT_INFORMATION; ?>
                    </div>
                    <div class="clear"></div>
                 </div>         
                <div class="bordure_bas_verte_logout"></div>
            </div>
            <div class="clear"></div>
        <!-- fin block text-->
</div>
</div>
<?php  
require("../includes/footer.php");
require("../includes/page_bottom.php");

?>