<?php
/* Auteur : Sami 
   Page regroupant les fonctions qui permettent de faire les actions presentent dans la page account.edit*/

function new_password($old,$new,$new_confirm){
	
	$ret='';
	$check=true;
	$res = tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id']."");
	$r=tep_db_fetch_array($res);
	$cp=$r['customers_password'];
	if(!tep_validate_password($old, $cp))
	{
		 $ret.='erreur mot de passe<br />';
		 $check=false;
	}
	if($new=='')
	  {
		  $ret.="Vous devez indiquer un nouveau mot de passe <br />";
		  $check=false;
	  }
	if($new_confirm=='')
	  {
		  $ret.="Vous devez confirmer votre nouveau mot de passe <br />";
		  $check=false;
	  }
   if($new!=$new_confirm)
	  {
		  $ret.="erreur lors de la confirmation du mot de passe <br />";
		  $check=false;
	  }
	  
	if($check==true)
	{
		$new_mdp=tep_encrypt_password($new);
		$exe_req=tep_db_query("update ".TABLE_CUSTOMERS." set customers_password ='".$new_mdp."' where customers_id=".$_SESSION['customer_id']." ");
		//maj client info
					$today = date("Y-m-d H:i:s");
					$customers_info=tep_db_query("update customers_info
												 set customers_info_date_account_last_modified='".$today."'
											     where customers_info_id=".$_SESSION['customer_id']."");
		return $ret.="valide";
	}
	
	return $ret;
}


function info_modif(){
	
	$formule_polie="Vous devez indiquer votre ";
	$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
	$check=true;
	  if($_POST['civi']=='')
	   {
	     $ret.=$formule_polie."civilit&eacute; <br />";
		 $check=false;
	  }
	  if($_POST['nom_cli']=='')
	  {
		  $ret.=$formule_polie."nom<br />";
		  $check=false;
	  }
	  
	   if($_POST['prenom_cli']=='')
	  {
		  $ret.=$formule_polie."prenom<br />";
		  $check=false;
	  }
	    if($_POST['jour']=='jour')
	  {
		  $ret.=$formule_polie."jour de naissance<br />";
		  $check=false;
	  }
	  
	  if($_POST['mois']=='mois')
	  {
		  $ret.=$formule_polie."mois de naissance<br />";
		  $check=false;
	  }
	  if($_POST['annee']=='annee')
	  {
		  $ret.=$formule_polie."ann&eacute;e de naissance<br />";
		  $check=false;
	  }
	  if($_POST['pays']=='pays')
	  {
		  $ret.=$formule_polie."pays<br />";
		  $check=false;
	  }
	   if(preg_match("#^0[0-68]([-. ]?[0-9]{2}){4}$#", $_POST['tel_portable_cli'])==false)
	  {
		  $ret.=$formule_polie."numero de mobile (valide)<br />";
		  $check=false;
	  }
	  if($_POST['tel_fixe_cli']!='')
	  {
		  if(preg_match("#^0[0-68]([-. ]?[0-9]{2}){4}$#", $_POST['tel_fixe_cli'])==false)
		  {
			  $ret.=$formule_polie."numero de fixe (valide)<br />";
			  $check=false;
		  }
	  }
	  if(!preg_match($verif,$_POST['mail_cli']))
	  {
		  $ret.=$formule_polie."adresse e-mail (valide)<br />";
		  $check=false;
	  }
	  
	  else{
		  
		  $res=tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['mail_cli']."' and customers_id=".$_SESSION['customer_id']."");
		  $r=tep_db_fetch_array($res);
		  
		  if($_POST['mail_cli']!=$r['customers_email_address'])
		  {
			  $ress=tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['mail_cli']."'");
		 	  $rs=tep_db_fetch_array($ress);
			 
			 if($_POST['mail_cli']==$rs['customers_email_address'])
		  		{
			  		$ret.="Cette adresse e-mail est d&eacute;j&agrave; utilis&eacute;e<br />";
		 	  		$check=false;
				}
		  }
		  
	  }
	  if($_POST['news']=='')
	  { $newsletter=0;}
	  else{ $newsletter=1;}
	  if($_POST['sms']=='')
	  {$sms=0;}
	  else{$sms=1;}
	  
	  if($check==true)
	  {	
	    $modif=tep_db_query("UPDATE ".TABLE_CUSTOMERS." SET customers_gender='".addslashes($_POST['civi'])."',
									 customers_firstname='".addslashes($_POST['prenom_cli'])."',
									 customers_lastname='".addslashes($_POST['nom_cli'])."',
									 customers_dob='".$_POST['annee']."-".$_POST['mois']."-".$_POST['jour']."',
									 customers_email_address='".addslashes($_POST['mail_cli'])."',
									 customers_country='".$_POST['pays']."',
									 customers_telephone='".addslashes($_POST['tel_portable_cli'])."',
									 customers_fixe='".addslashes($_POST['tel_fixe_cli'])."',
									 customers_newsletter='".$newsletter."',
									 customers_news_sms='".$sms."'
								WHERE customers_id=".$_SESSION['customer_id']."");
		//maj client info
					$today = date("Y-m-d H:i:s");
					$customers_info=tep_db_query("update ".TABLE_CUSTOMERS_INFO."
												 set customers_info_date_account_last_modified='".$today."'
											     where customers_info_id=".$_SESSION['customer_id'].")");
	  	return $ret.='valide';
	  }
	   return $ret;
}



function info_modif_pro(){
	
	$formule_polie="Vous devez indiquer votre ";
	$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
	$check=true;
	  if($_POST['civi']=='')
	   {
	     $ret.=$formule_polie."civilit&eacute; <br />";
		 $check=false;
	  }
	  if($_POST['nom_cli']=='')
	  {
		  $ret.=$formule_polie."nom<br />";
		  $check=false;
	  }
	  
	   if($_POST['prenom_cli']=='')
	  {
		  $ret.=$formule_polie."prenom<br />";
		  $check=false;
	  }
	    if($_POST['jour']=='jour')
	  {
		  $ret.=$formule_polie."jour de naissance<br />";
		  $check=false;
	  }
	  
	  if($_POST['mois']=='mois')
	  {
		  $ret.=$formule_polie."mois de naissance<br />";
		  $check=false;
	  }
	  if($_POST['annee']=='annee')
	  {
		  $ret.=$formule_polie."ann&eacute;e de naissance<br />";
		  $check=false;
	  }
	  if($_POST['pays']=='pays')
	  {
		  $ret.=$formule_polie."pays<br />";
		  $check=false;
	  }
	   if(preg_match("#^0[0-68]([-. ]?[0-9]{2}){4}$#", $_POST['tel_portable_cli'])==false)
	  {
		  $ret.=$formule_polie."numero de mobile (valide)<br />";
		  $check=false;
	  }
	  if($_POST['tel_fixe_cli']!='')
	  {
		  if(preg_match("#^0[0-68]([-. ]?[0-9]{2}){4}$#", $_POST['tel_fixe_cli'])==false)
		  {
			  $ret.=$formule_polie."numero de fixe (valide)<br />";
			  $check=false;
		  }
	  }
	  
	  if($_POST['societe']=='')
	   {
	     $ret.=$formule_polie." le nom de votre soci&eacute;t&eacute; <br />";
		 $check=false;
	  }	  
	  if($_POST['fax']!='')
	  {
		  if(preg_match("#^0[0-68]([-. ]?[0-9]{2}){4}$#", $_POST['fax'])==false)
		  {
			  $ret.=$formule_polie."num&eacute;ro de fax (valide)<br />";
			  $check=false;
		  }
	  }
	  
	  if(!preg_match($verif,$_POST['mail_cli']))
	  {
		  $ret.=$formule_polie."adresse e-mail (valide)<br />";
		  $check=false;
	  }
	  
	  else{
		  
		  $res=tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['mail_cli']."' and customers_id=".$_SESSION['customer_id']."");
		  $r=tep_db_fetch_array($res);
		  
		  if($_POST['mail_cli']!=$r['customers_email_address'])
		  {
			  $ress=tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['mail_cli']."'");
		 	  $rs=tep_db_fetch_array($ress);
			 
			 if($_POST['mail_cli']==$rs['customers_email_address'])
		  		{
			  		$ret.="Cette adresse e-mail est d&eacute;j&agrave; utilis&eacute;e<br />";
		 	  		$check=false;
				}
		  }
		  
	  }
	  if($_POST['news']=='')
	  { $newsletter=0;}
	  else{ $newsletter=1; }
	  if($_POST['sms']=='')
	  { $sms=0;}
	  else{$sms=1;}
	  
	  if($check==true)
	  {	
	    $modif=tep_db_query("UPDATE ".TABLE_CUSTOMERS." SET customers_gender='".$_POST['civi']."',
									 customers_firstname='".addslashes($_POST['prenom_cli'])."',
									 customers_lastname='".addslashes($_POST['nom_cli'])."',
									 customers_dob='".$_POST['annee']."-".$_POST['mois']."-".$_POST['jour']."',
									 customers_email_address='".addslashes($_POST['mail_cli'])."',
									 customers_country='".$_POST['pays']."',
									 customers_telephone='".addslashes($_POST['tel_portable_cli'])."',
									 customers_fixe='".addslashes($_POST['tel_fixe_cli'])."',
									 customers_newsletter='".$newsletter."',
									 customers_news_sms='".$sms."',
									 customers_company='".addslashes($_POST['societe'])."',
									 customers_fax='".addslashes($_POST['fax'])."'
								WHERE customers_id=".$_SESSION['customer_id']."");
		//maj client info
					$today = date("Y-m-d H:i:s");
					$customers_info=tep_db_query("update ".TABLE_CUSTOMERS_INFO."
												 set customers_info_date_account_last_modified='".$today."'
											     where customers_info_id=".$_SESSION['customer_id']."");
												 
	  	return $ret.='valide';
	  }
	   return $ret;
}




?>