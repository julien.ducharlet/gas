/* Auteur : Sami
   Page servant � la prise en compte des param�tres pour le paiement de devis dans le compte client*/
   
function paiement(id,id_com){
	
	var req;
		
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");

		req.onreadystatechange = function()
		{
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
					  //alert(req.responseText);
					  document.location.href="devis_details.php?id_com="+id_com;
				}
				else
				{alert("ERRROOOOOOOOOOOOORRRRR");}
			}
		};
		req.open("GET", "fonction/fonctions_ajax/devis.php?devis="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
		
}


function validation() {
	
	var checkBox = document.getElementById("cgv");
	var paiementSelected = false;
	
	for(i=0 ; i<document.getElementById('compteur').value ; i++) {
		
		if(document.getElementById('paiement'+ i).checked) paiementSelected = true;
	}
	
	if(!checkBox.checked || !paiementSelected) {
		
		if(!paiementSelected) alert('Vous devez choisir un moyen de paiement');
		else alert("Vous devez accepter les conditions g�n�rales de vente");
	}
	else document.valider_devis.submit();
}