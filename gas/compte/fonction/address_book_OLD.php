<?php
/* Page regroupant les fonctions qui permettent de faire les actions presentent dans la page adresse_book.php */
function adresse_modif(){
	$formule_polie="Vous devez indiquer votre ";
	$check=true;
	  if($_POST['titre_address']=='')
	   {
	     $ret.=$formule_polie."titre (...) <br />";
		 $check=false;
	  }
	  if($_POST['civi']=='')
	   {
	     $ret.=$formule_polie."civilit&eacute; <br />";
		 $check=false;
	  }
	  if($_POST['nom_address']=='')
	  {
		  $ret.=$formule_polie."nom<br />";
		  $check=false;
	  }
	  if($_POST['prenom_address']=='')
	  {
		  $ret.=$formule_polie."prenom<br />";
		  $check=false;
	  }	  
	  if($_POST['a_address']=='')
	  {
		  $ret.=$formule_polie." adresse<br />";
		  $check=false;
	  }
	  if(preg_match("#^[0-9]{4}[0-9]?$#", $_POST['cp_address'])==false)
	  {
		  $ret.=$formule_polie." code postal<br />";
		  $check=false;
	  }
	  if($_POST['ville_address']=='')
	  {
		  $ret.=$formule_polie." ville<br />";
		  $check=false;
	  }
	  if($_POST['pays']=='pays')
	  {
		  $ret.=$formule_polie."pays<br />";
		  $check=false;
	  }
	  
	  if ($check==true) {
		  
	  	if ($_POST['default_address']!='') {
			$mod=tep_db_query("update ".TABLE_CUSTOMERS." set customers_default_address_id=".$_POST['id_address']." where customers_id=".$_SESSION['customer_id']."");
			$_SESSION['customer_country_id']=$_POST['pays'];
		}
		  
		$titre_address = format_text($_POST['titre_address']);
		$societe_address = format_text($_POST['societe_address']);
		$prenom_address = format_text($_POST['prenom_address']);
		$nom_address = format_text($_POST['nom_address']);
		  
		$a_address = format_text($_POST['a_address']);
		$ap_address = format_text($_POST['ap_address']);
		
		$cp_address = format_text($_POST['cp_address']);
		$ville_address = format_text($_POST['ville_address']);
		  
		$modif=tep_db_query("UPDATE ".TABLE_ADDRESS_BOOK." set 
							entry_address_title='". $titre_address ."',
							entry_gender='". $_POST['civi'] ."',
							entry_company='". $societe_address ."',
							entry_firstname='". $prenom_address ."',
							entry_lastname='". $nom_address ."',
							entry_street_address='". $a_address ."',
							entry_suburb='". $ap_address ."',
							entry_postcode='". $cp_address ."',
							entry_city='". $ville_address ."',
							entry_country_id='".$_POST['pays']."'
							where address_book_id=".$_POST['id_address']."");
		
		//maj client info
		$today = date("Y-m-d H:i:s");
		$customers_info=tep_db_query("update ".TABLE_CUSTOMERS_INFO."
												 set customers_info_date_account_last_modified='".$today."'
											     where customers_info_id=".$_SESSION['customer_id']."");	  	
		
		return $ret.='<div id="bordure_mdp_haut_verte" style="background-color:#8CCB7C;"></div>
					  <div id="bordure_mdp_milieu_vert" style="background-color:#8CCB7C;">modification r&eacute;alis&eacute;e</div>
					  <div class="clear"></div>  
					  <div id="bordure_mdp_bas_verte" style="background-color:#8CCB7C;"></div>';
	  }
	   else return '<div class="haut_erreur"></div>
					<div class="milieu_erreur">'.$ret.'</div>
					<div class="clear"></div>  
					<div class="bas_erreur"></div>';
}

function adresse_ajout(){
	
	$formule_polie="Vous devez indiquer votre ";
	$check=true;
	  if($_POST['titre_address']=='')
	   {
	     $ret.=$formule_polie."le nom de l'adresse<br />";
		 $check=false;
	  }
	  if($_POST['civi']=='')
	   {
	     $ret.=$formule_polie."civilit&eacute; <br />";
		 $check=false;
	  }
	  if($_POST['nom_address']=='')
	  {
		  $ret.=$formule_polie."nom<br />";
		  $check=false;
	  }
	  if($_POST['prenom_address']=='')
	  {
		  $ret.=$formule_polie."prenom<br />";
		  $check=false;
	  }	  
	  if($_POST['a_address']=='')
	  {
		  $ret.=$formule_polie." adresse<br />";
		  $check=false;
	  }
	  if(preg_match("#^[0-9]{4}[0-9]?$#", $_POST['cp_address'])==false)
	  {
		  $ret.=$formule_polie." code postal<br />";
		  $check=false;
	  }
	  if($_POST['ville_address']=='')
	  {
		  $ret.=$formule_polie." ville<br />";
		  $check=false;
	  }
	  if($_POST['pays']=='pays')
	  {
		  $ret.=$formule_polie."pays<br />";
		  $check=false;
	  }
	  
	  if($check==true)
	  {	
	    $modif=tep_db_query("insert into ".TABLE_ADDRESS_BOOK." 
													 (customers_id,
													  entry_address_title,
													  entry_gender,
													  entry_company,
													  entry_firstname,
													  entry_lastname,
													  entry_street_address,
													  entry_suburb,
													  entry_postcode,
													  entry_city,
													  entry_country_id)
												values
													(".$_SESSION['customer_id'].",
													 '".format_text($_POST['titre_address'])."',
													 '".format_text($_POST['civi'])."',
													 '".format_text($_POST['societe_address'])."',
													 '".format_text($_POST['prenom_address'])."',
													 '".format_text($_POST['nom_address'])."',
													 '".format_text($_POST['a_address'])."',
													 '".format_text($_POST['ap_address'])."',
													 ".format_text($_POST['cp_address']).",
													 '".format_text($_POST['ville_address'])."',
													 ".$_POST['pays'].")");
	  	
		//maj client info
		$today = date("Y-m-d H:i:s");
		$customers_info=tep_db_query("update ".TABLE_CUSTOMERS_INFO."
												 set customers_info_date_account_last_modified='".$today."'
											     where customers_info_id=".$_SESSION['customer_id']."");
					
	    if($_POST['default_address']!='')
		  {
			  //$up1=tep_db_query("update ".TABLE_ADDRESS_BOOK." set address_default='0' where address_default='1' and customers_id=".$_SESSION['customer_id']."");
			  
			  $req=tep_db_query("select max(address_book_id) as max from ".TABLE_ADDRESS_BOOK." where customers_id=".$_SESSION['customer_id']."");
			  $res=tep_db_fetch_array($req);
			  
			  //$up2=tep_db_query("update ".TABLE_ADDRESS_BOOK." set address_default='1' where address_book_id=".$res['max']."");
			  
			  $mod=tep_db_query("update ".TABLE_CUSTOMERS." set customers_default_address_id=".$res['max']." where customers_id=".$_SESSION['customer_id']."");
			  $_SESSION['customer_country_id']=$_POST['pays'];
			  
		  }
		  
		return $ret.='<div id="bordure_mdp_haut_verte" style="background-color:#8CCB7C;"></div>
					  <div id="bordure_mdp_milieu_vert" style="background-color:#8CCB7C;">Votre adresse vient d&ecirc;tre ajout&eacute;e dans votre compte.</div>
					  <div class="clear"></div>  
					  <div id="bordure_mdp_bas_verte" style="background-color:#8CCB7C;"></div>';
	   }
	   else return '<div class="haut_erreur"></div>
					<div class="milieu_erreur">'.$ret.'</div>
					<div class="clear"></div>  
					<div class="bas_erreur"></div>';	  
}
?>