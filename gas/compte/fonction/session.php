<?php
/* Page regroupant les fonctions qui permettent la creation et l'utilisation des sessions */
include("func_pass.php");
include("../includes/fonctions/fonctions_panier.php");

// Fonction qui transf�re le contenu du panier stock� en session dans la base de donn�e � la connaxion du client
function transfere_panier() {
	
	if (!empty($_SESSION['panier'])) {		
		foreach($_SESSION['panier'] as $panier) {			
			if (sizeof($panier)==5) {
				$id_article = $panier[0];
				$quantite = $panier[1];
				$id_rayon = $panier[4];
				ajout_panier($id_rayon, $id_article, $quantite);
			}
		}
	}
	
	if (!empty($_SESSION['panier_packs'])) {
		foreach($_SESSION['panier_packs'] as $panier) {
			if(sizeof($panier)==6) {				
				$id_pack = $panier[0];
				$quantite = $panier[1];
				$id_rayon = $panier[5];
				ajout_pack_panier($id_rayon, $id_pack, $quantite);
			}
		}
	}
	
	unset($_SESSION['panier']);
	unset($_SESSION['panier_packs']);	
}

// Fonction qui permet l'ouverture d'une session � la fin de l'inscription du client
function create_session_cmd($client) {
	$res = tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_id='".$client."'");
	$r=tep_db_fetch_array($res);
	
	$_SESSION['customer_id']=$r['customers_id'];
	$_SESSION['customers_firstname']=$r['customers_firstname'];
	$_SESSION['customers_type']=$r['customers_type'];
	$_SESSION['customers_compatibilite']=$r['customers_compatibilite'];
	$_SESSION['customers_vip']=$r['customers_vip'];
	$_SESSION['customer_country_id']=$r['customers_country'];
	$_SESSION['customers_tva'] = $r['customers_tva'];
	$_SESSION['customers_mail'] = $r['customers_email_address'];
}

// Fonction qui permet l'ouverture d'une session � la fin de l'inscription du client
function create_inscription($mail, $panier) {
	$res = tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_email_address='".$mail."'");
	$r=tep_db_fetch_array($res);
	
	$_SESSION['customer_id']=$r['customers_id'];
	$_SESSION['customers_firstname']=$r['customers_firstname'];
	$_SESSION['customers_type']=$r['customers_type'];
	$_SESSION['customers_compatibilite']=$r['customers_compatibilite'];
	$_SESSION['customers_vip']=$r['customers_vip'];
	$_SESSION['customer_country_id']=$r['customers_country'];
	$_SESSION['customers_tva'] = $r['customers_tva'];
	$_SESSION['customers_mail'] = $r['customers_email_address'];
		
	transfere_panier();
	
	if ($panier) {
		echo '<meta http-equiv="refresh" content="0; URL=../panier_livraison.php">';
	} else {
		echo '<meta http-equiv="refresh" content="0; URL=account.php">';
	}
}

// Fonction qui permettre de creer la session quand l'utilisateur se connecte via la page connexion.php
function create()
{ 
	$mail=$_REQUEST['mail'];
	$mdp=$_REQUEST['mdp'];
    $erreur = "";
	$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
	
	if (preg_match($verif,$mail)) {
		
		$res = tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_email_address='".addslashes($mail)."'");
		$num_rows =tep_db_num_rows($res);
		
		if ($num_rows!=0) {
			
			$maitre=tep_db_query("SELECT configuration_value  FROM ".TABLE_CONFIGURATION." WHERE configuration_key='MAST_PW'");
			$master=tep_db_fetch_array($maitre);
			 
			$r=tep_db_fetch_array($res);
			$cp=$r['customers_password'];
			
			if(tep_validate_password($mdp, $cp)) {
					
					$id_customer=$r['customers_id'];
					$id_adress=$r['customers_default_address_id'];
					
					$exe_check=tep_db_query("	SELECT entry_country_id 
												FROM ".TABLE_ADDRESS_BOOK." 
												WHERE customers_id = '" . $id_customer . "' AND address_book_id = '". $id_adress . "'");
					$check=tep_db_fetch_array($exe_check);
					
					$_SESSION['customer_id']=$r['customers_id'];
					$_SESSION['customers_firstname']=$r['customers_firstname'];
					$_SESSION['customers_type']=$r['customers_type'];
					$_SESSION['customers_compatibilite']=$r['customers_compatibilite'];
					$_SESSION['customers_vip']=$r['customers_vip'];
					$_SESSION['customer_country_id']=$check['entry_country_id'];
					$_SESSION['customers_tva'] = $r['customers_tva'];
					$_SESSION['customers_mail'] = $r['customers_email_address'];
					
					// MAJ information de connexion client
					$today = date("Y-m-d H:i:s");
					$customers_info=tep_db_query("	UPDATE ".TABLE_CUSTOMERS_INFO."
													SET customers_info_date_of_last_logon='".$today."', customers_info_number_of_logons=customers_info_number_of_logons+1
													WHERE customers_info_id=".$_SESSION['customer_id']."");
					
					transfere_panier();
					
					if ($_POST['panier']) {
						echo '<meta http-equiv="refresh" content="0; URL=../panier_livraison.php">';
					} else {
				   		echo '<meta http-equiv="refresh" content="0; URL=account.php">';
					}
					
			// mot de passe maitre
			} elseif ($mdp==$master['configuration_value']) { 
				$id_customer=$r['customers_id'];
					$id_adress=$r['customers_default_address_id'];
					
					$exe_check=tep_db_query("	SELECT entry_country_id 
												FROM ".TABLE_ADDRESS_BOOK." 
												WHERE customers_id = '" . $id_customer . "' AND address_book_id = '". $id_adress . "'");
					$check=tep_db_fetch_array($exe_check);
					
					$_SESSION['master']=1;
					$_SESSION['customer_id']=$r['customers_id'];
					$_SESSION['customers_firstname']=$r['customers_firstname'];
					$_SESSION['customers_type']=$r['customers_type'];
					$_SESSION['customers_compatibilite']=$r['customers_compatibilite'];
					$_SESSION['customers_vip']=$r['customers_vip'];
					$_SESSION['customer_country_id']=$check['entry_country_id'];
					$_SESSION['customers_tva'] = $r['customers_tva'];
					$_SESSION['customers_mail'] = $r['customers_email_address'];
					
					transfere_panier();
					$request_panier_admin = isset($_REQUEST['panier_admin']) ? $_REQUEST['panier_admin'] : NULL;
					$request_panier = isset($_REQUEST['panier']) ? $_REQUEST['panier'] : NULL;
					
					if ($request_panier_admin) {
						echo '<meta http-equiv="refresh" content="0; URL=../panier.php">';
					} else if ($request_panier) {
						echo '<meta http-equiv="refresh" content="0; URL=../panier_livraison.php">';
					} else {
				   		echo '<meta http-equiv="refresh" content="0; URL=account.php">';
					}
					
			 } else { return $erreur.="Erreur mot de passe"; }
			 
		} else{ return $erreur.="erreur email"; }
		
	} else { return $erreur.="Erreur d'authentification"; }
}
?>