<?php 
/* Page regroupant les fonctions qui permettent de faire les inscriptions pour les ASSOCIATION */

function sujet_message() {
	return 'Bienvenue sur le site Group Army Store / G�n�ral Army Store';
}

function corps_message($nom,$civilite,$societe,$nb_produit,$nb_reference) {
	return '<p style="font-size: 11px; font-family: Verdana,Arial,Helvetica,sans-serif; color: rgb(0, 0, 0);">
			Bonjour <span style="font-weight: bold; color: rgb(170, 180, 29);">'.$societe.'</span>,<br>
			<br>
			<br>
			Nous vous souhaitons la bienvenue chez <span style="font-weight: bold;">Group Army Store / G�n�ral Army Store</span>.<br>
			<br>
			<br>
			Vous avez d�sormais acc�s � tous nos services parmi lesquels :<br>
			<br>
			- L\'expertise et l\'exp�rience d\'une <span style="font-weight: bold;">�quipe de professionnels</span> � votre �coute et � votre service <span style="font-weight: bold;">depuis 2003</span><br>
			- Un catalogue de <span style="font-weight: bold;">'.$nb_produit.' produits</span> d�clin�s en <span style="font-weight: bold;">'.$nb_reference.' r�f�rences</span> s�lectionn�es parmi environ <span style="font-weight: bold;">150 marques</span> de renom<br>
			- Des <span style="font-weight: bold;">fiches produits compl�tes</span> : descriptifs d�taill�s, photos, vid�os, vues � 360�, manuels utilisateurs, notes et avis de clients<br>
			- Des <span style="font-weight: bold;">tarifs comp�titifs</span> et adapt�s avec des <span style="font-weight: bold;">remises quantitatives</span> � partir de 5 pi�ces d�une m�me r�f�rence<br>
			- Des <span style="font-weight: bold;">moyens de paiement 100% s�curis�s</span> : CB (avec 3D Secure), Paypal, Ch�que, Virement<br>
			- Des d�lais de <span style="font-weight: bold;">livraisons rapides</span><br>
			- Un <span style="font-weight: bold;">Service Client</span> et un <span style="font-weight: bold;">Service Apr�s-Vente</span> jug�s <span style="font-weight: bold;">comp�tents</span> par nos clients et <span style="font-weight: bold;">disponibles</span> 5 jours sur 7 (8h / jour)<br>
			- Et bien plus encore�<br>
			<br>
			<br>
			'.SIGNATURE_MAIL.'
			</p>';
}

function controle_formulaire() {
	$ret='<strong>Pour finaliser votre inscription, veuillez : </strong><br><br>';
	
	$formule_polie="Indiquer ";
	$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
	$check=true;
	
	// CONTROLE DES INFORMATIONS DE CONNEXION
	$mail_cli = format_clients(strtolower($_POST['mail_cli']));
	if (!preg_match($verif,$mail_cli)) {
		$ret.=$formule_polie."une adresse e-mail (valide)<br>";
		$check=false;
	} else {
	  	$res=tep_db_query("SELECT customers_id FROM ".TABLE_CUSTOMERS." WHERE customers_email_address='".$mail_cli."'");
		$num_rows =tep_db_num_rows($res);
		if ($num_rows!=0) { $ret.="Cette adresse e-mail est d�j� utilis�e<br>"; $check=false; }
	}	
	if ($_POST['mdp']=='') { $ret.=$formule_polie."votre mot de passe <br>"; $check=false; }
	
	// CONTROLE DES INFORMATIONS DU RESPONSABLE
	
	if (empty($_POST['civi'])) { $ret.=$formule_polie."la civilit� du responsable<br>"; $check=false; }	
	$nom_cli = strtoupper(format_inscription($_POST['nom_cli']));
	if (empty($nom_cli)) { $ret.=$formule_polie."le nom du responsable<br>"; $check=false; }
	$prenom_cli = ucwords(strtolower(format_inscription($_POST['prenom_cli'])));
	if (empty($prenom_cli)) { $ret.=$formule_polie."le pr�nom du responsable<br>"; $check=false; }
	
	// CONTROLE DES INFORMATIONS DE FACTURATION	
	
	$societe_cli = strtoupper(format_inscription($_POST['societe_cli'])); // VARIABLE
	if (empty($societe_cli)){ $ret.=$formule_polie."le nom de l'association<br>"; $check=false; } 
	
	$siret_cli = strtoupper(format_inscription($_POST['siret_cli'])); // VARIABLE
	if (preg_match("#^[0-9]{9}[0-9]?$#", $siret_cli)==false) { $ret.=$formule_polie."num�ro de R�pertoire Nationnal des Associations  (9 chiffres sans le W)<br>"; $check=false; } // VARIABLE
	
	$adresse_cli = ucwords(strtolower(format_inscription($_POST['adresse_cli'])));
	if (empty($adresse_cli)) { $ret.=$formule_polie."une adresse physique<br>"; $check=false; }
	$cp_cli = strtoupper(format_inscription($_POST['cp_cli']));
	if (!ctype_alnum($cp_cli)) { $ret.=$formule_polie."un code postal (Uniquement des chiffres et des lettres sans espace)<br>"; $check=false; }
	$ville_cli = strtoupper(format_inscription($_POST['ville_cli']));
	if (empty($ville_cli)) { $ret.=$formule_polie."une ville<br>"; $check=false; }
	
	if (empty($_POST['pays'])) { $ret.= "S�lectionnez un pays<br>"; $check=false; }
	
	$tel_portable_cli = format_tel($_POST['tel_portable_cli']);
	if (!ctype_digit($tel_portable_cli)) { $ret.=$formule_polie."un num�ro de t�l�phone mobile valide (uniquement des chiffres)<br>"; $check=false; }	
	$tel_fixe_cli = format_tel($_POST['tel_fixe_cli']);
	if (!empty($tel_fixe_cli)) { if (!ctype_digit($tel_fixe_cli)) { $ret.=$formule_polie."un num�ro de t�l�phone fixe valide (uniquement des chiffres)<br>"; $check=false; } }	
	$fax_cli = format_tel($_POST['fax_cli']); 
	if (!empty($fax_cli)) { if (!ctype_digit($fax_cli)) { $ret.=$formule_polie."un num�ro de fax valide (uniquement des chiffres)<br>"; $check=false; } } // VARIABLE
	  
	if ($_POST['news']=='') { $newsletter=0; } else { $newsletter=1; }
	if ($_POST['rgpd']=='') { $ret.="Cocher et accepter la RGPD<br>"; $check=false; }
	
// SI LES CONTROLES SONT OK	
	
	if ($check==true) {
		
		// TYPE DE CLIENT A L'ENREGISTREMENT VARIABLE
		$customers_type=4;
		$customers_company_type='Association';
			  	
		// ZONE SANS CONTROLE
		$adresse_cli_3 = ucwords(strtolower(format_inscription($_POST['adresse_cli_3'])));
		$complement_adresse_cli = ucwords(strtolower(format_inscription($_POST['complement_adresse_cli'])));
		$adresse_cli_4 = ucwords(strtolower(format_inscription($_POST['adresse_cli_4'])));		
		
		$mdp=tep_encrypt_password($_POST['mdp']);
		$exe_req=tep_db_query("INSERT INTO ".TABLE_CUSTOMERS." (
													customers_gender,
													customers_firstname,
													customers_lastname,
													customers_email_address, 
													customers_telephone,
													customers_fixe,
													customers_password,
													customers_newsletter,
													customers_type,
													customers_country,
													customers_company,
													customers_fax,
													customers_siret,
													customers_company_type
													
												) VALUES (
													'".$_POST['civi']."',
													'".$prenom_cli."',
													'".$nom_cli."',
													'".$mail_cli."',
													'".$tel_portable_cli."',
													'".$tel_fixe_cli."',
													'".$mdp."',
													'".$newsletter."',
													'".$customers_type."',
													".$_POST['pays'].",
													'".$societe_cli."',
													'".$fax_cli."',
													'".$siret_cli."',
													'".$customers_company_type."'
												)"
							);
			
		$req_num_cli=tep_db_query("SELECT customers_id FROM ".TABLE_CUSTOMERS." WHERE customers_email_address='".$mail_cli."' ");
		$res=tep_db_fetch_array($req_num_cli);
		$num=$res['customers_id'];
		  
		$today = date("Y-m-d H:i:s");
		  
		$customers_info=tep_db_query("INSERT INTO ".TABLE_CUSTOMERS_INFO." (customers_info_id, customers_info_date_account_created) VALUES (".$num.", '".$today."')");
		  
		$address=tep_db_query("INSERT INTO ".TABLE_ADDRESS_BOOK." 
										(customers_id,
										 entry_gender,
										 entry_company,
										 entry_firstname, 
										 entry_lastname,
										 entry_street_address,
										 entry_street_address_3,
										 entry_street_address_4,
										 entry_suburb,
										 entry_postcode,
										 entry_city,
										 entry_country_id)
										 VALUES (".$num.",
										'".$_POST['civi']."',
										'".$societe_cli."',
										'".$prenom_cli."',
										'".$nom_cli."',
										'".$adresse_cli."',
										'".$adresse_cli_3."',
										'".$adresse_cli_4."',
										'".$complement_adresse_cli."',
										'".$cp_cli."',
										'".$ville_cli."',
										".$_POST['pays'].")");
		  
		$adresse_num=tep_db_query("SELECT address_book_id FROM ".TABLE_ADDRESS_BOOK." WHERE customers_id=".$num."");
		$res_add_num=tep_db_fetch_array($adresse_num);
		$num_add=$res_add_num['address_book_id'];
		  
		$address_defaut_cli=tep_db_query("UPDATE ".TABLE_CUSTOMERS." SET customers_default_address_id=".$num_add." WHERE customers_id=".$num."");
		
		$ret.=create_inscription($mail_cli, $_POST['panier']);
	
		if ($_POST['civi']=="m") { $civilite="M"; } 
		else if ($_POST['civi']=="f") { $civilite="Mle"; } 
		else if ($_POST['civi']=="d") { $civilite="Mlle"; }
		
		// requete pour r�cup�rer le nombre de produit visible sur le site pour le mail
		$requete_nb_produit=tep_db_query("SELECT products_id FROM products WHERE products_status=1");
		$nb_produit = tep_db_num_rows($requete_nb_produit);

		// requete pour r�cup�rer le nombre d'option produit visible sur le site pour le mail
		$requete_nb_reference=tep_db_query("SELECT products_attributes_id FROM products_attributes WHERE options_dispo='1'");
		$nb_reference = tep_db_num_rows($requete_nb_reference);

		$sujet=sujet_message();
		$message=corps_message($nom_cli,$civilite,$societe_cli,$nb_produit,$nb_reference);
		mail_client($mail_cli,$sujet,$message);
		// FAIRE EMAIL SUR INFO@GENERALARMYSTORE.FR
		// mail_client('info@generalarmystore.fr',$sujet,$message);
	
	} else { 
		//echo '<div class="haut_erreur"></div><div class="milieu_erreur">'.$ret.'</div><div class="clear"></div><div class="bas_erreur"></div>';
		echo '<div class="box-erreur">'.$ret.'</div>';
	}
}


?>