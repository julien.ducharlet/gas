<?php
/* Page permettant d'�ffectuer les enregistrements en base lors du paiement d'un devis */
   
//require("../includes/modules/mod_paiement_spplus_reesayer.php");
require("../includes/modules/mod_paiement_spplus.php");
// JULIEN 12/03
//require("../includes/configure_spplus.php");
function devis_traitement(){
		
	//Variables pour le paiement
	####Carte Bleue####
	$ID_MAGASIN=ID_MAGASIN_SPPLUS;
	$MAIL=MAIL_SPPLUS;
	$MAIL_TECHNIQUE=MAIL_TECHNIQUE_SPPLUS;
	$SIRET=SIRET_SPPLUS;
	
	$today = date("Y-m-d H:i:s");
		
	$req=tep_db_query("SELECT * FROM orders WHERE orders_id=".$_SESSION['id_devis']."");
	$res=tep_db_fetch_array($req);
	
	//calcul des montant si il y a application d'une remise porte monnaie virtuel
	$montant_ht = $res['ss_total'];
	$port_ht = $res['frais_port_client'];
	$remise = $res['remise_port_monnaie'];
	$tva_total = $res['tva_total'];
	$tva_port = $res['tva_port'];
	
	
	if(isset($_SESSION['remise_panier_devis']) && !empty($_SESSION['remise_panier_devis'])){
		
		$remise = $_SESSION['remise_panier_devis'];
		
		if ($_SESSION['total_panier_devis']==0) {
			$tva_total = 0;
			$tva_port = 0;
		} elseif (($montant_ht-$_SESSION['total_panier_devis'])>0) {// comment le ss-total peut-il etre sup�rieur au total ?!!!!!!
			$ss_total2 = ($montant_ht-$_SESSION['total_panier_devis']);
			$port_ht = $res['frais_port_client'];
			$tva_port = $res['tva_port'];
			$tva_total = round(($ss_total2+$port_ht)*0.2,2);				
		} else {
			$ss_total2 = ($_SESSION['total_panier_devis']-$montant_ht);
			$port_ht = round($port_ht,2);
			$tva_port = round($port_ht*0.2,2);
			$tva_total = $tva_port;
		}
	}
	
	$moyen_p=tep_db_query("SELECT * FROM module_paiement WHERE paiement_id=".$_SESSION['devis_moyen_paiement']."");
	$res_paiement=tep_db_fetch_array($moyen_p);
	$statut_commande=$res_paiement['paiement_statut'];
	
	if (isset($_SESSION['remise_panier_devis']) && !empty($_SESSION['remise_panier_devis'])) {
		$marge=$res['marge']-$_SESSION['remise_panier_devis'];
	 
		$orders_status_history = tep_db_query("
												INSERT INTO 
													orders_status_history 
													(
														orders_id, 
														orders_status_id, 
														date_added, 
														customer_notified, 
														comments
													) VALUES (
														".$_SESSION['id_devis'].", 
														".$statut_commande.", 
														'".$today."', 
														1, 
														'Utilisation du porte monnaie virtuel' 
													)
											");

		$somme_av=tep_db_query("SELECT customers_argent_virtuel FROM customers WHERE customers_id=".$_SESSION['customer_id']."");
		$res_av=tep_db_fetch_array($somme_av);

		$nouv_m=$res_av['customers_argent_virtuel']-$_SESSION['remise_panier_devis'];
		$r='Suite � la commande : '.$_SESSION['id_devis'].' votre porte-monnaie viruel a �t� debit� de '.$_SESSION['remise_panier_devis'].' �';

		$update_cli_av=tep_db_query("
										UPDATE 
											customers 
										SET 
											customers_argent_virtuel=".$nouv_m." 
										WHERE 
											customers_id=".$_SESSION['customer_id']."
									");
		$inset_cli = tep_db_query("
										INSERT INTO 
											customer_argent_virtuel 
											(
												customers_id, 
												somme, 
												raison, 
												raison_client, 
												date_versement, 
												user_id
											) VALUES (
												'".$_SESSION['customer_id']."', 
												'".$_SESSION['remise_panier_devis']."', 
												'".$r."', 
												'".$r."',
												'".$today."',
												999
											)
								");

	} else {
		$marge=$res['marge'];
		// Requete insertion d'un nouveau statut � la commande
		$orders_status_history = tep_db_query("
												INSERT INTO 
													orders_status_history
													(
														orders_id, 
														orders_status_id, 
														date_added, 
														customer_notified
													) VALUES (
														".$_SESSION['id_devis'].", 
														".$statut_commande.", 
														'".$today."', 
														1
													)
											");	
	}
	
	// MAJ de la commande
	$orders_maj=tep_db_query("	
									UPDATE 
										orders 
									SET 
										payment_method='".$res_paiement['paiement_nom']."', 
										orders_status=".$statut_commande.", 
										remise_porte_monnaie=".$_SESSION['remise_panier_devis'].", 
										total=".$_SESSION['total_panier_devis'].", 
										tva_total=".$tva_total.", 
										frais_port_client=".$port_ht.", 
										tva_port=".$tva_port.",  
										marge=".$marge." 
									WHERE 
										orders_id=".$_SESSION['id_devis']."
							");
		  
	// MAJ de la date de derniere commande du client 
	$customers_info=tep_db_query("
									UPDATE 
										customers_info 
									SET 
										customers_info_date_of_last_order='".$today."' 
									WHERE 
										customers_info_id=".$_SESSION['customer_id']."
								");
		
	//MAJ des STOCKS VIRTUEL des articles (champ : products_quantity)
	$req_prod=tep_db_query("	
								SELECT 
									op.orders_products_id, 
									op.products_id, 
									op.products_model, 
									op.products_name, 
									op.products_quantity, 
									opa.products_options_values 
								FROM 
									orders_products op 
								LEFT JOIN 
									orders_products_attributes opa 
								ON 
									opa.orders_products_id = op.orders_products_id
								WHERE op.orders_id=".$_SESSION['id_devis']."
							");
							
	while($res_p=tep_db_fetch_array($req_prod)){
		
		if ($res_p['products_options_values'] != NULL) {
			
			$recherche_options_query = "
										SELECT 
											products_options_values_id
										FROM 
											products_options_values
										WHERE 
											products_options_values_name = '".$res_p['products_options_values']."'";
			$recherche_options_response = tep_db_query($recherche_options_query);
			$recherche_options_data = tep_db_fetch_array($recherche_options_response);
			$maj_stock_options = tep_db_query("
										UPDATE 
											products_attributes 
										SET 
											options_quantity=options_quantity-".$res_p['products_quantity']." 
										WHERE 
											products_id=".$res_p['products_id']." 
										AND 
											options_values_id = ". $recherche_options_data['products_options_values_id']."
										");
			
			//alerte de stock sp�cifique pour une option
	  		$alert_stock_options = '	
										SELECT 
											options_quantity, 
											options_quantity_min, 
											products_options_name, 
											products_options_values_name 
										FROM 
											products_attributes pa, 
											products_options po, 
											products_options_values pov
										WHERE 
											pa.options_id=po.products_options_id
										AND 
											pa.options_values_id=pov.products_options_values_id
										AND 
											products_id=\''. $res_p['products_id'] .'\'
										AND 
											options_values_id = \''. $recherche_options_data['products_options_values_id'] .'\'
									';
			$alert_stock_options = tep_db_query($alert_stock_options);
			$res_alert = tep_db_fetch_array($alert_stock_options);
			
			if ($res_alert['options_quantity']<=$res_alert['options_quantity_min']) {
				mail_stock_alert($res_p['products_id'], $res_p['products_name'], $res_p['products_model'] .' - '. $res_alert['products_options_name'] .' => '. $res_alert['products_options_values_name']);
			}
		}
		
		$products_quantity = tep_db_query("UPDATE ".TABLE_PRODUCTS." SET products_quantity=products_quantity-".$res_p['products_quantity']." WHERE products_id=".$res_p['products_id']."");
		
		///mail alert stock
		$alert_stock = tep_db_query("SELECT products_quantity_min, products_quantity FROM ".TABLE_PRODUCTS." WHERE products_id=".$res_p['products_id']."");
		$res_alert = tep_db_fetch_array($alert_stock);
		
		if($res_alert['products_quantity']<=$res_alert['products_quantity_min']) {
			mail_stock_alert($res_p['products_id'], $res_p['products_name'], $res_p['products_model']);
		}
	}
	
	//mail client
	 $mail=$res['customers_email_address'];
	 $sujet="Traitement de la commande";	 
	 $mess=mail_contenu_commande($_SESSION['id_devis']);
	 
	 //mail admin
	 $mess_admin=mail_contenu_commande_admin($_SESSION['id_devis']);
	 
	 
	if($_SESSION['devis_moyen_paiement']==1){
		echo lien_paiement_spplus($ID_MAGASIN,$SIRET,$_SESSION['id_devis']);
	} else {
		 mail_cmd_admin($sujet,$mess_admin);
		 mail_client_commande($mail,$sujet,$mess);	
		 
		 $_SESSION['id_devis']='';
		 $_SESSION['remise_panier_devis']='';
		 $_SESSION['total_panier_devis']='';
		 $_SESSION['devis_moyen_paiement']='';
		 
		 echo '<meta http-equiv="refresh" content="0; URL=../panier_remerciement.php">';		 
	}
}
?>