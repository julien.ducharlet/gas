/* Javascript qui est utilis�e pour la page adress_book.php */

function ok_ajout(){
	document.getElementById("ok_ajout").value="ok";
	document.adresse.submit();
}

function ok_modif(){
	document.getElementById("ok_modif").value="ok";
	document.adresse.submit();
}

function reset_ajout(){
	document.adresse.reset();
	document.getElementById("ajouter").style.display="block";
	document.getElementById("modif").style.display="none";
	document.getElementById("nouvelle").style.display="none";
}


function affiche_modif(){
	document.getElementById("ajouter").style.display="none";
	document.getElementById("modif").style.display="block";
	document.getElementById("nouvelle").style.display="block";
}

function recup_info_modif(id){
		var req;
		
		affiche_modif();
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");

		req.onreadystatechange = function()
		{
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
                    var str;
                    var rep=req.responseText;
					
                    str=rep.split("�|�");
					document.getElementById("id_address").value=str[0];
					document.getElementById("titre_address").value=str[1];
					document.getElementById("prenom_address").value=str[2];
                    document.getElementById("nom_address").value=str[3];
					document.getElementById("societe_address").value=str[4];
					document.getElementById("a_address").value=str[5];
					document.getElementById("ap_address").value=str[6];
					document.getElementById("cp_address").value=str[7];
					document.getElementById("ville_address").value=str[8];
				
					var selectBox = document.getElementById("pays");
                    for (var i=0; i<selectBox.options.length; i++)

                    {
                        if(selectBox.options[i].value==str[9])
                         {selectBox.options[i].selected = true;}

                   }
				   
				   var checkBox = document.getElementById("default_address");
				   if(str[10]==0)
                     {checkBox.checked = false;}
					 else {checkBox.checked = true;}
					
					
				  if(str[11]==1)
				  { document.getElementById("mr").checked=true;}
				  if(str[11]==2)
				  { document.getElementById("mme").checked=true;}
				  if(str[11]==3)
				  { document.getElementById("mlle").checked=true;}
				  
				}
				else
				{//alert(req.statusText);
				}
			}
		};
		req.open("GET", "fonction/fonctions_ajax/info_adresse.php?id_adresse="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	
}


function recharge(){
		
		var req;
		
		affiche_modif();
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");

		req.onreadystatechange = function()
		{
			
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{ document.getElementById("block_address").innerHTML=req.responseText;}
				else
				{document.getElementById("block_address").innerHTML="Error: returned status code " + req.status + " " + req.statusText;}
			}
		};
		req.open("GET", "fonction/fonctions_ajax/info_adresse.php?refresh=oui", true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}


function supprimer(id_address){
	
	    check = confirm('�tes-vous s�r de vouloir supprimer cette adresse ?')
		if(check==true) { 
		
		var req;
		
		affiche_modif();
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");

		req.onreadystatechange = function()
		{
			
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
				  recharge();
				  reset_ajout();
				  location.href='address_book.php';
				}
				else
				{alert(req.statusText);}
			}
		};
		req.open("GET", "fonction/fonctions_ajax/info_adresse.php?delete="+id_address, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);		
		}
}


