<?php
/* Page regroupant les fonctions qui permettent l'envoi d'un nouveau mot de passe au client */
   

//Genere une chaine de carecteres al�atoir qui servira au nouveau mot de passe
function genere_password()
{    
	//longueur du mot de passe voulue
	$size=10;
	 // Initialisation des caract�res utilisables
	$characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
	
	for($i=0;$i<$size;$i++)
	{ $password .= ($i%2) ? strtoupper($characters[array_rand($characters)]) : $characters[array_rand($characters)]; }
	 return $password;
} 

//Envoi un nouveau mot de passe au client
function verif_mail($m) {
	
	$email=$m;
	$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
	if(preg_match($verif,$email)) {
				
		$req=tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_email_address='".$email."'");
		$r=tep_db_fetch_array($req);
		
		$customers_reset_password = $r['customers_reset_password'];
		if ($customers_reset_password == 0) {
			$num_rows =tep_db_num_rows($req);
			if ($num_rows!=0) { 
				
				$nom=$r['customers_lastname'];
				if ($r['customers_gender']=="m") { $civilite="M"; }
				else if ($r['customers_gender']=="f") { $civilite="Mle"; }
				else if ($r['customers_gender']=="d") { $civilite="Mlle"; }
				  
				$mdp=genere_password();
				$new_mdp=tep_encrypt_password($mdp);
				$exe_req=tep_db_query("update customers set customers_password ='".$new_mdp."' where customers_email_address='".$email."' ");
				$maj_customers_reset_password=tep_db_query("update customers set customers_reset_password =1 where customers_email_address='".$email."' ");
				 
				$sujet=sujet_message();
				$message=corps_message($nom,$civilite,$mdp);
				mail_client($email,$sujet,$message);
				return $erreur.='<div class="box-valide">Vous allez recevoir un e-mail avec un mot de passe provisoire. Merci de patienter.</div>';
				  
			} else { 
				return $erreur.='<div class="box-erreur">L\'adresse e-mail n\'a pas �t� trouv&eacute;e.</div>'; 
			}
		
		} else {
			return $erreur.='<div class="box-erreur">Votre demande de nouveau mot de passe � d�j� �t� prise en compte.<br>Merci de patienter et de consulter votre boite e-mail dans quelques minutes.</div>';
		}
		
	} else { 
		return $erreur.='<div class="box-erreur">Veuillez renseigner une adresse e-mail valide.</div>'; 
	}

}

function sujet_message()
{return 'Nouveau mot de passe';}

function corps_message($nom,$civilite,$mdp)
{
	return ' <p style="font-size: 11px; font-family: Verdana,Arial,Helvetica,sans-serif; color:#000000;">
				Bonjour '.$civilite.' <span style="font-weight: bold; color:#AAB41D;">'. $nom .'</span>,
				<br>
				<br>
				Vous venez de nous signaler la perte de vos identifiants pour vous connecter � notre site.<br>
				<br>
				Suite � cela, nous vous envoyons un nouveau mot de passe qui est le suivant : <strong>'. $mdp .'</strong><br>
				<br>
				Pour des raisons de s�curit�, il vous sera demand� de le changer le plus rapidement possible. 
				<br>
				Vous pouvez, pour cela, vous rendre sur votre compte en cliquant <a style="color:#AAB41D; font-weight:bold;" href="'. WEBSITE . BASE_DIR .'/compte/connexion.php">ici</a>.<br>
				<br>
				'. SIGNATURE_MAIL .'
			</p>';
}
?>
