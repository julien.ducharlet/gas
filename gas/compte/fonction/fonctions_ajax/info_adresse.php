<?php
/* Page qui est utilis�e lors de l'appel ajax pour la page address.book.php */
   
session_start();
require("../../../includes/configure.php");
require("../../../includes/fonctions/base_de_donnees.php");
require("../../../includes/fonctions/general.php");
require(DATABASE_TABLE_DIR);

tep_db_connect();

$id = (int)$_GET['id_adresse'];
$refresh = (int)$_GET['refresh'];
$delete = (int)$_GET['delete'];

//affiche les infos quand l'utilisateur clique sur l'icone modifier
if($id!='')
{
	$req=tep_db_query("SELECT * FROM ".TABLE_ADDRESS_BOOK." WHERE address_book_id=".$id."");
	$r=tep_db_fetch_array($req);
	
	$def=tep_db_query("SELECT customers_default_address_id FROM ".TABLE_CUSTOMERS." WHERE customers_default_address_id=".$id."");
	$d=tep_db_fetch_array($def);
	$nb=tep_db_num_rows($def);
	
	if($nb==0) {
		$defaut=0;
	} else { 
		$defaut=1;
	}
	
	if($r['entry_gender']=="m")
	{$gender=1;}
	if($r['entry_gender']=="f")
	{$gender=2;}
	if($r['entry_gender']=="d")
	{$gender=3;}
	
	echo $r['address_book_id'].'�|�'.stripslashes($r['entry_address_title']).'�|�'.stripslashes($r['entry_firstname']).'�|�'.stripslashes($r['entry_lastname']).'�|�'.stripslashes($r['entry_company']).'�|�'.stripslashes($r['entry_street_address_3']).'�|�'.stripslashes($r['entry_suburb']).'�|�'.stripslashes($r['entry_street_address']).'�|�'.stripslashes($r['entry_street_address_4']).'�|�'.$r['entry_postcode'].'�|�'.$r['entry_city'].'�|�'.$r['entry_country_id'].'�|�'.$defaut.'�|�'.$gender;
}

//refraichi les infos des adresses apr�s aujout ou suppression
if($refresh!='')
{
	$i=1;
	$req=tep_db_query("SELECT * FROM ".TABLE_ADDRESS_BOOK." WHERE customers_id='".$_SESSION['customer_id']."' ORDER BY address_book_id asc");
             while($r=tep_db_fetch_array($req))
             {
               echo '<div style="background-color:#d6deb0; height:20px; margin-bottom:10px; text-align:left; padding:10px 5px 10px 10px; " id="blop'.$r['adress_book_id'].'">
			   			<div style="float:left;">
								'.$i.'<span style="margin-left:30px;">
								<strong>'.strtoupper($r['entry_address_title']).'</strong>
								- ';
								if($r['entry_gender']=="m")
								  {echo"M";}
								if($r['entry_gender']=="f")
								  {echo"Mme";}
								if($r['entry_gender']=="d")
								  {echo"Mlle";}
						
						   echo' '.$r['entry_lastname'].'
						   </span>
						</div>
						<div style="float:right;">
							<a href="#" onclick="recup_info_modif('.$r['address_book_id'].');"><img src="../template/base/boutons/editer.png" alt="modifier" /></a>';
							$nbz=tep_db_num_rows($req);
							if($nbz>1){
							echo'&nbsp;';
							echo'<a href="#" onclick="supprimer('.$r['address_book_id'].');"><img src="../template/base/boutons/icone_supprimer.png" alt="supprimer" /></a>';
							}
						echo'</div>
                    </div>';
					$i++;
             }
}

//on efface l'adresse dont l'id est sp�cifi�
if($delete!='')
{
	$verif=tep_db_query("SELECT customers_default_address_id FROM ".TABLE_CUSTOMERS." WHERE customers_default_address_id=".$delete."");
	$resultat_verif=tep_db_num_rows($verif);
	
	$del=tep_db_query("DELETE FROM ".TABLE_ADDRESS_BOOK." WHERE address_book_id=".$delete."");
	
	if($resultat_verif == 1)
	{
		$req=tep_db_query("SELECT max(address_book_id) AS max FROM ".TABLE_ADDRESS_BOOK." WHERE customers_id=".$_SESSION['customer_id']."");
		$res=tep_db_fetch_array($req);
			  
		$up3=tep_db_query("UPDATE ".TABLE_CUSTOMERS." SET customers_default_address_id=".$res['max']." WHERE customers_id=".$_SESSION['customer_id']."");				
	}
}
?>