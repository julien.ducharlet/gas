<?php
/* Auteur : Sami 
   Page qui est utilis�e lors de l'appel ajax pour la page newsletter.php*/
   
session_start();

require("../../../includes/configure.php");
require("../../../includes/fonctions/base_de_donnees.php");
require("../../../includes/fonctions/general.php");
require(DATABASE_TABLE_DIR);

tep_db_connect();

$id_rayon = (int)$_GET['rayon'];
$id_marque = (int)$_GET['marque'];
$id_ligne = (int)$_GET['ligne'];

//insertion
$rayon = (int)$_GET['r'];
$marque = (int)$_GET['m'];
$modele = (int)$_GET['mo'];
$abonnement=$_GET['abo'];
$general=$_GET['gene'];
$partenaire=$_GET['part'];
$frequence=$_GET['freq'];
$promo=$_GET['promo'];
$nouveaute=$_GET['nouv'];

//suppression
$rayon_supr = (int)$_GET['r_supr'];
$marque_supr = (int)$_GET['m_supr'];
$modele_supr = (int)$_GET['mo_supr'];

//rechargement
$refresh=$_GET['recharge'];

//met a jour la partie concernant la liste des produits 
function refresh(){

			$req_info=tep_db_query("select * from newsletter_infos where customers_id=". (int)$_SESSION['customer_id']);
			$res1=tep_db_fetch_array($req_info);
			
			if($res1['news_id']!=''){				
				$req_prod=tep_db_query("Select *
										 From ".TABLE_NEWSLETTER_PRODUCTS."
										 Where newsletter_id=".$res1['news_id']."");
				$nb_prod=tep_db_num_rows($req_prod);
				if($nb_prod==0){
					echo"Vous n'avez pas de produit atitr� pour votre newsletter";
				}
				else{				
				$i=1;
				while($res2=tep_db_fetch_array($req_prod))
				{
					
                        $reponse = tep_db_query("SELECT rubrique_name, rubrique_id
                                                 FROM ".TABLE_RAYON."
                                                 WhERE rubrique_id=".$res2['rayon_id']);
                        $donnees = tep_db_fetch_array($reponse);
                      
				
                	echo'<input type="hidden" id="rayon_e'.$i.'" value="'.$donnees['rubrique_id'].'" />';
                 
					echo'<div style="width:237px; padding-top:15px; border-width:0px 0px 2px 0px; border-style:solid; border-color:#99cc00; background-color:#e3e3e3; height:30px; float:left;">'.$donnees['rubrique_name'].'</div>';
				
					$reponse2 = tep_db_query("SELECT cd.categories_name, cd.categories_id
                                  FROM ".TABLE_CATEGORIES." as c inner join categories_description as cd on c.categories_id=cd.categories_id
                                       inner join categories_rubrique as cr on c.categories_id=cr.categories_id
                                  WHERE c.parent_id=0 AND c.sort_order > 0 AND cr.rubrique_id = " . $res2['rayon_id'] . " 
                                  ORDER BY c.sort_order");
                    $verif=false;
                    while(($donnees2 = tep_db_fetch_array($reponse2)) && $verif==false){
						if($res2['marque_id']==$donnees2['categories_id']){
						 $val=$donnees2['categories_name'];
						 $val_id=$res2['marque_id'];
						 $verif=true;
						 }
						 else{
							 $val="";
							 $val_id="";
							 $verif=false;
						 }
					 } 
					echo ' <input type="hidden"  id="marque_e'.$i.'" value="'.$val_id.'" />';
					echo'<div style="width:237px; padding-top:15px; border-width:0px 0px 2px 0px; border-style:solid; border-color:#99cc00; height:30px; float:left;">'. $val.'</div>';
					
					$modele_query = tep_db_query("SELECT cd.categories_name, cd.categories_id
                                      FROM ".TABLE_CATEGORIES." as c inner join categories_description as cd on c.categories_id=cd.categories_id
                                      WHERE c.parent_id='" . $res2['marque_id'] . "'
                                      ORDER BY cd.categories_name");
                    $verif2=false;
                    while(($modele_data = tep_db_fetch_array($modele_query)) && $verif2==false){
					 if($res2['modele_id']==$modele_data['categories_id']){
						$value=$modele_data['categories_name']; 
						$value_id=$res2['modele_id'];
						$verif2=true;
					 }
					 else{
						  $value="";
						  $value_id="";
						  $verif2=false;
					 }
					}		
				echo'<input type="hidden" id="modele_e'.$i.'" value="'.$value_id.'" />';
				echo'<div style="width:240px; padding-top:15px; border-width:0px 0px 2px 0px; border-style:solid; border-color:#99cc00; background-color:#e3e3e3; height:30px; float:left;">'.$value.'</div>';
				
				echo'<div style="width:255px; padding-top:15px; border-width:0px 0px 2px 0px; border-style:solid; border-color:#99cc00; height:30px; float:left;">
                	<!--<input type="button" value="supprimer" onclick="supprime_fc('.$i.')" />-->
					<div style="margin-top:-5px;"><img src="../template/base/boutons/bouton_supprimer.png" style="cursor:pointer;" onclick="supprime_fc('.$i.')" alt="supprimer une selection" title="supprimer une selection" /></div>
                </div>
				<br />';				
				$i++;
			 	}
			   }
			}
}

//permet de supprimer un des produits dans la liste
if($rayon_supr!='')
{ 
	$recup_id_client=tep_db_query("select news_id from newsletter_infos where customers_id=".$_SESSION['customer_id']."");
	$id_news=tep_db_fetch_array($recup_id_client);
	
	if($marque_supr=='') $marque_supr=0;
	if($modele_supr=='') $modele_supr=0;
   	$delete=tep_db_query("delete from newsletter_products where newsletter_id=".$id_news['news_id']."
														 and rayon_id=".$rayon_supr."
														 and marque_id=".$marque_supr."
														 and modele_id=".$modele_supr."");
   refresh();
}

//permet d'inserer un produit
if($rayon!='')
{   
	$req=tep_db_query("update customers set customers_newsletter='".$general."',
									   		customers_newsletter_partenaire='".$partenaire."',
											customers_newsletter_inscription='".$abonnement."'
									 where customers_id=".$_SESSION['customer_id']."");
	
	$verif_client=tep_db_query("select news_id from newsletter_infos where customers_id=".$_SESSION['customer_id']."");
	$id_verif=tep_db_fetch_array($verif_client);
	$cli=tep_db_num_rows($verif_client);
	
	if($cli==0)
	{
		$insert=tep_db_query("insert into newsletter_infos (customers_id, frequence_news, promo_news, nouveaute_news) 
													 values (".$_SESSION['customer_id'].",".$frequence.",'".$promo."','".$nouveaute."')");
	}
	else{
		$update=tep_db_query("update newsletter_infos set frequence_news=".$frequence.",
							 							  promo_news='".$promo."',
														  nouveaute_news='".$nouveaute."'
													  where customers_id=".$_SESSION['customer_id']."");
	}
	
   	if($marque=='marque') $marque=0;
	if($modele=='modele') $modele=0;
	
	$recup_id_client=tep_db_query("select news_id from newsletter_infos where customers_id=".$_SESSION['customer_id']."");
	$id_news=tep_db_fetch_array($recup_id_client);
	
	$verif=tep_db_query("select newsletter_id from newsletter_products  where newsletter_id=".$id_news['news_id']."
														 and rayon_id=".$rayon."
														 and marque_id=".$marque."
														 and modele_id=".$modele."");
	$nb=tep_db_num_rows($verif);
	if($nb==0)
	{
	$req=tep_db_query("insert into newsletter_products (newsletter_id, rayon_id, marque_id, modele_id) 
													  value (".$id_news['news_id'].",".$rayon.",".$marque.",".$modele.")");
	refresh();	
	}
	else echo'Attention';

}

//recupere les marques
if($id_rayon!='')
{
	$reponse2 = tep_db_query("SELECT cd.categories_name, cd.categories_id
                              FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id
							  	   inner join categories_rubrique as cr on c.categories_id=cr.categories_id
                              WHERE c.parent_id=0 AND c.sort_order > 0 AND cr.rubrique_id = " . $id_rayon . " 
                              ORDER BY c.sort_order");
	echo'<option value="marque">Choisissez une marque</option>';
    while($donnees2 = tep_db_fetch_array($reponse2))
	{echo"<option value=".$donnees2['categories_id'].">".$donnees2['categories_name']."</option>";}
}

//recupere les modeles
if($id_marque!='')
{
	$modele_query = tep_db_query("SELECT cd.categories_name, cd.categories_id
                                  FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id
                                  WHERE c.parent_id='" . $id_marque . "'
                                  ORDER BY cd.categories_name");
	echo'<option value="modele">Choisissez un mod&egrave;le</option>';
    while($modele_data = tep_db_fetch_array($modele_query))
	{echo'<option value='.$modele_data['categories_id'].'>'.$modele_data['categories_name'].'</option>';}
}

//recupere les rayons
if($id_ligne!='')
{
	$reponse = tep_db_query("SELECT rubrique_name, rubrique_id
                             	 FROM rubrique
                            	 ORDER BY rubrique_id");
	echo'<option value="rayon">Choisissez un rayon</option>';
    while($donnees = tep_db_fetch_array($reponse))
	{echo'<option value='.$donnees['rubrique_id'].'>'.$donnees['rubrique_name'].'</option>';}
}


?>