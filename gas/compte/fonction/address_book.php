<?php
/* Page regroupant les fonctions qui permettent de faire les actions presentent dans la page adresse_book.php */

function adresse_modif(){
	$post_default_address = !empty($_POST['default_address']) ? $_POST['default_address'] : '';
	if ($post_default_address != '') {
		$mod=tep_db_query("	UPDATE ". TABLE_CUSTOMERS ." 
							SET customers_default_address_id=".$_POST['address_book_id']." 
							WHERE customers_id=".$_SESSION['customer_id']."");
		$_SESSION['customer_country_id']=$_POST['pays'];
	}
	  
	$titre_address = strtoupper(format_inscription($_POST['titre_address']));
	$societe_address = strtoupper(format_inscription($_POST['societe_address']));
	$nom_address = strtoupper(format_inscription($_POST['nom_address']));
	$prenom_address = format_inscription(ucwords(strtolower($_POST['prenom_address'])));
	$app_address = ucwords(strtolower(format_inscription($_POST['app_address'])));
	$ap_address = ucwords(strtolower(format_inscription($_POST['ap_address'])));
	$a_address = ucwords(strtolower(format_inscription($_POST['a_address'])));
	$appp_address = ucwords(strtolower(format_inscription($_POST['appp_address'])));
	$cp_address = strtoupper(format_inscription($_POST['cp_address']));
	$ville_address = strtoupper(format_inscription($_POST['ville_address']));
	
	$_POST['civi'] = isset($_POST['civi']) ? $_POST['civi'] : ''; // ajout THIERRY 24/03/2020 
	
	$modif=tep_db_query("	UPDATE ".TABLE_ADDRESS_BOOK." SET 
								entry_address_title='". $titre_address ."',
								entry_gender='". $_POST['civi'] ."',
								entry_company='". $societe_address ."',
								entry_firstname='". $prenom_address ."',
								entry_lastname='". $nom_address ."',
								entry_street_address_3='". $app_address ."',
								entry_suburb='". $ap_address ."',
								entry_street_address='". $a_address ."',							
								entry_street_address_4='". $appp_address ."',
								entry_postcode='". $cp_address ."',
								entry_city='". $ville_address ."',
								entry_country_id='".$_POST['pays']."'
							WHERE address_book_id=".$_POST['address_book_id']."");
	
	//maj client info
	$today = date("Y-m-d H:i:s");
	$customers_info=tep_db_query("	UPDATE ".TABLE_CUSTOMERS_INFO."
									SET customers_info_date_account_last_modified='".$today."'
									WHERE customers_info_id=".$_SESSION['customer_id']."");	  	
}

function adresse_ajout(){

	$titre_address = strtoupper(format_inscription($_POST['titre_address']));
	$societe_address = strtoupper(format_inscription($_POST['societe_address']));
	$nom_address = strtoupper(format_inscription($_POST['nom_address']));
	$prenom_address = format_inscription(ucwords(strtolower($_POST['prenom_address'])));
	$app_address = ucwords(strtolower(format_inscription($_POST['app_address'])));
	$ap_address = ucwords(strtolower(format_inscription($_POST['ap_address'])));
	$a_address = ucwords(strtolower(format_inscription($_POST['a_address'])));
	$appp_address = ucwords(strtolower(format_inscription($_POST['appp_address'])));
	$cp_address = strtoupper(format_inscription($_POST['cp_address']));
	$ville_address = strtoupper(format_inscription($_POST['ville_address']));
	
	$_POST['civi'] = isset($_POST['civi']) ? $_POST['civi'] : ''; // ajout THIERRY 24/03/2020
	
	$modif=tep_db_query("INSERT INTO ".TABLE_ADDRESS_BOOK." 
									(customers_id,
									entry_address_title,
									entry_gender,
									entry_company,
									entry_firstname,
									entry_lastname,
									entry_street_address_3,
									entry_suburb,
									entry_street_address,
									entry_street_address_4,
									entry_postcode,
									entry_city,
									entry_country_id)
								VALUES
									(".$_SESSION['customer_id'].",
									'".$titre_address."',
									'".$_POST['civi']."',
									'".$societe_address."',
									'".$prenom_address."',
									'".$nom_address."',
									'".$app_address."',
									'".$ap_address."',
									'".$a_address."',
									'".$appp_address."',
									'".$cp_address."',
									'".$ville_address."',
									".$_POST['pays'].")");
	
	// MAJ de la date de derniere MAJ du compte client
	$today = date("Y-m-d H:i:s");
	$customers_info=tep_db_query("	UPDATE ".TABLE_CUSTOMERS_INFO."
									SET customers_info_date_account_last_modified='".$today."'
									WHERE customers_info_id=".$_SESSION['customer_id']."");
	
	$POST_default_address = isset($_POST['default_address']) ? $_POST['default_address'] : ''; // ajout THIERRY 24/03/2020 
	
	if ($POST_default_address!='') { // si c'est l'adresse par defaut 
		$req=tep_db_query("	SELECT max(address_book_id) AS max 
							FROM ".TABLE_ADDRESS_BOOK." 
							WHERE customers_id=".$_SESSION['customer_id']."");
		$res=tep_db_fetch_array($req);
		
		$mod=tep_db_query("	UPDATE ".TABLE_CUSTOMERS." SET customers_default_address_id=".$res['max']." WHERE customers_id=".$_SESSION['customer_id']."");
		$_SESSION['customer_country_id']=$_POST['pays'];
	}		
}

function supprimer_adresse($id){
	
	$del=tep_db_query("DELETE FROM ".TABLE_ADDRESS_BOOK." WHERE address_book_id=".$id."");
		
	$verif=tep_db_query("SELECT customers_default_address_id FROM ".TABLE_CUSTOMERS." WHERE customers_default_address_id=".$id."");
	$resultat_verif=tep_db_num_rows($verif);		
		
	if ($resultat_verif == 1) {
		$req=tep_db_query("SELECT max(address_book_id) AS max FROM ".TABLE_ADDRESS_BOOK." WHERE customers_id=".$_SESSION['customer_id']."");
		$res=tep_db_fetch_array($req);
				  
		$up3=tep_db_query("UPDATE ".TABLE_CUSTOMERS." SET customers_default_address_id=".$res['max']." WHERE customers_id=".$_SESSION['customer_id']."");				
	}
	
	echo '<div class="box-valide">L\'adresse a bien &eacute;t&eacute; supprim&eacute;e.</div>';
	
}

?>