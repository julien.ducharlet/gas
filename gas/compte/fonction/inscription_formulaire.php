<?php 
/* Auteur : Sami 
   Page regroupant les fonctions qui permettent de faire les inscriptions pour la creation des comptes*/

function sujet_message()
{
	return 'Bienvenue sur le site ' . NOM_DU_SITE . '.';
}

function corps_message_particulier($nom,$civilite)
{
	return '<p style="font-size: 11px; font-family: Verdana,Arial,Helvetica,sans-serif; color: rgb(0, 0, 0);">
						Bonjour '.$civilite.' <span style="font-weight: bold; color: rgb(170, 180, 29);">'.$nom.'</span>,
						<br />
						<br />
						nous vous souhaitons la bienvenue sur le site ' . NOM_DU_SITE . '.
						<br />
						<br />
						Vous avez maintenant acc�s � tous nos services parmi lesquels :<br />
						- Une m�thode de paiement 100% s�curis�e !<br />
						- Des d�lais de livraison respectable et surtout respect�s !<br />
						- Une garantie satisfait ou rembours�* !<br />
						<br />
						<br />
						Pour obtenir de l\'aide sur n\'importe lequel de nos services,<br />
						<a style="color:#aab41d; font-weight:bold;" href="'. WEBSITE . BASE_DIR .'/contact.php">cliquez ici</a>
						et remplissez correctement le formulaire afin que nous puissions traiter<br />
						efficacement vos questions. 
						<br />
						<br />
						<br />
						Vous pouvez �galement nous contacter par t�l�phone au
						<span style="color:#aab41d;">' . TEL_NORMAL .'</span>.
						<br />
						<br />
						<br />
						Cordialement,
						<br />
						Le service client.
						<br />
						</p>';
}

function corps_message_pro($nom,$civilite)
{
	return '<p style="font-size: 11px; font-family: Verdana,Arial,Helvetica,sans-serif; color: rgb(0, 0, 0);">
						Bonjour '.$civilite.' <span style="font-weight: bold; color: rgb(170, 180, 29);">'.$nom.'</span>,
						<br />
						<br />
						nous vous souhaitons la bienvenue sur le site ' . NOM_DU_SITE . '.
						<br />
						<br />
						<br />
						Vous avez maintenant acc�s � tous nos services parmi lesquels :<br />
						- Une m�thode de paiement 100% s�curis�e !<br />
						- Des d�lais de livraison respectable et surtout respect�s !<br />
						<br />
						Pour obtenir de l\'aide sur n\'importe lequel de nos services,<br />
						<a style="color:#aab41d; font-weight:bold;" href="'. WEBSITE . BASE_DIR .'/contact.php">cliquez ici</a>
						et remplissez correctement le formulaire afin que nous puissions traiter<br />
						efficacement vos questions. 
						<br />
						<br />
						<br />
						<br />
						Vous pouvez �galement nous contacter par t�l�phone au <span style="color:#aab41d;">' . TEL_PRO .'</span>.
						<br />
						<br />
						<br />
						Cordialement,<br />
						<br />
						Le Service client�le professionnelle
						<br />
						</p>';
}

function verif()
{
	$ret='<strong><u>Erreur � corriger pour finaliser votre inscription :</u></strong><br><br>';
	
	$today = date("Y-m-d H:i:s");
	$formule_polie="Vous devez indiquer ";
	$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
	$check=true;
		if($_POST['civi']=='') {
			$ret.=$formule_polie."votre civilit�<br />";
			$check=false;
		}
		
	  if($_POST['nom_cli']=='')
	  {
		  $ret.=$formule_polie."votre nom<br />";
		  $check=false;
	  }
	  
	   if($_POST['prenom_cli']=='')
	  {
		  $ret.=$formule_polie."votre pr�nom<br />";
		  $check=false;
	  }
	    if($_POST['jour']=='jour')
	  {
		  $ret.=$formule_polie."votre jour de naissance<br />";
		  $check=false;
	  }
	  
	  if($_POST['mois']=='mois')
	  {
		  $ret.=$formule_polie."votre mois de naissance<br />";
		  $check=false;
	  }
	  if($_POST['annee']=='annee')
	  {
		  $ret.=$formule_polie."votre ann�e de naissance<br />";
		  $check=false;
	  }
	   if ($_POST['tel_portable_cli'] == "" || !preg_match('<^[0-9]*$>', format_tel($_POST['tel_portable_cli'])))
	  {
		  $ret.=$formule_polie."votre num�ro de t�l�phone mobile (valide)<br />";
		  $check=false;
	  }
	  if($_POST['adresse_cli']=='')
	  {
			  $ret.=$formule_polie."votre adresse<br />";
			  $check=false;
		  
	  }
	  if($_POST['ville_cli']=='')
	  {
			  $ret.=$formule_polie."votre ville<br />";
			  $check=false;
		  
	  }
	  if(preg_match("#^[0-9]{4}[0-9]?$#", $_POST['cp_cli'])==false)
	  {
			  $ret.=$formule_polie."votre code postal<br />";
			  $check=false;
		  
	  }	  
	  /*if($_POST['tel_fixe_cli']!='')
	  {
		  if(preg_match("#^0[1-9]([-. ]?[0-9]{2}){4}$#", $_POST['tel_fixe_cli'])==false)
		  {
			  $ret.=$formule_polie."numero de fixe (valide)<br />";
			  $check=false;
		  }
	  }*/
	   if($_POST['pays']=='pays')
	  {
		  $ret.=$formule_polie."votre pays<br />";
		  $check=false;
	  }
	  if(!preg_match($verif,$_POST['mail_cli']))
	  {
		  $ret.=$formule_polie."votre adresse e-mail (valide)<br />";
		  $check=false;
	  }
	  else{
		  
		  $res=tep_db_query("select customers_id from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['mail_cli']."'");
		  $num_rows =tep_db_num_rows($res);
		  if($num_rows!=0)
		  {
			  $ret.="Cette adresse e-mail est d&eacute;j&agrave; utilis&eacute;e<br />";
		 	  $check=false;
		  }
		  
	  }
	  if($_POST['email_parrain']!=''){
		  
		   $res0=tep_db_query("select id_parrainage 
								   from ".TABLE_PARRAINAGE."
								   where mail_filleul='".$_POST['mail_cli']."'");
		   
		   $res=tep_db_query("select customers_id from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['email_parrain']."'");
		   $req_res=tep_db_fetch_array($res);
		   $num_rows =tep_db_num_rows($res);
		   $num_rows0 =tep_db_num_rows($res0);
		   
		   if($num_rows0!=0 && $num_rows!=0){
			   $req_verif=tep_db_query("select *
									   from ".TABLE_PARRAINAGE."
									   where id_parrain=".$req_res['customers_id']."
									   and mail_filleul='".$_POST['mail_cli']."'
									   and statut='non lu' ");
			   $res_nb=tep_db_num_rows($req_verif);
			   if($res_nb==0){			   
			   $ret.="D&eacute;sol&eacute; mais votre adresse fait d&eacute;j&agrave; l'objet d'un parrainage<br />";
			   $check=false;
			   }
		   }
		   else{			
			  if($num_rows==0)
			  {
				  $ret.="D&eacute;sol&eacute; mais l'adresse de votre parrain nous est inconnue<br />";
				  $check=false;
			  }
		   }
	  }
	  
	  if(!preg_match($verif,$_POST['conf_mail_cli']))
	  {
		  $ret.="Vous devez confirmer votre adresse e-mail (valide)<br />";
		  $check=false;
	  }
	  if($_POST['conf_mail_cli']!=$_POST['mail_cli'])
	  {
		  $ret.="Erreur lors de la confirmation de l'adresse e-mail <br />";
		  $check=false;
	  }
	  if($_POST['mdp']=='')
	  {
		  $ret.=$formule_polie."votre mot de passe <br />";
		  $check=false;
	  }
	  if($_POST['conf_mdp']=='')
	  {
		  $ret.="Vous devez confirmer votre mot de passe <br />";
		  $check=false;
	  }
	  if($_POST['conf_mdp']!=$_POST['mdp'])
	  {
		  $ret.="Erreur lors de la confirmation du mot de passe <br />";
		  $check=false;
	  }
		if($_POST['news']=='') {
			$newsletter=0;
		} else {
			$newsletter=1;
		}
		if($_POST['sms']=='') {
			$sms=0;
		} else {
			$sms=1;
		}
		if($_POST['rgpd']=='') {
			$ret.="Vous devez cocher et accepter la RGPD<br />";
			$check=false;
		}
	  
	  if($check==true) {
			
			$nom_cli = format_text($_POST['nom_cli']);
			$prenom_cli = format_text($_POST['prenom_cli']);
			$adresse_cli = format_text($_POST['adresse_cli']);
		
			$complement_adresse_cli = format_text($_POST['complement_adresse_cli']);
			$cp_cli = format_text($_POST['cp_cli']);
			$ville_cli = format_text($_POST['ville_cli']);
			
			$tel_portable_cli = format_tel($_POST['tel_portable_cli']);
			$tel_fixe_cli = format_tel($_POST['tel_fixe_cli']);
			
	  		$mdp=tep_encrypt_password($_POST['mdp']);
			
			$exe_req=tep_db_query("insert into ".TABLE_CUSTOMERS." (customers_gender,
														  customers_firstname,
														  customers_lastname,
														  customers_dob, 
														  customers_email_address, 
														  customers_telephone,
														  customers_fixe,
														  customers_password,
														  customers_newsletter,
														  customers_news_sms,
														  customers_type,
														  customers_country) 
									  
								  				values ('".$_POST['civi']."',
														'".$prenom_cli."',
														'".$nom_cli."',														
														'".$_POST['annee']."-".$_POST['mois']."-".$_POST['jour']."',
														'".$_POST['mail_cli']."',
														'".$tel_portable_cli."',
														'".$tel_fixe_cli."',
														'".$mdp."',
														'".$newsletter."',
														'".$sms."',
														1,
														".$_POST['pays'].")");
			
			
			
		  $req_num_cli=tep_db_query("select customers_id from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['mail_cli']."' ");
		  $res=tep_db_fetch_array($req_num_cli);
		  $num=$res['customers_id'];
		  
		  
		  
		  $customers_info=tep_db_query("insert into ".TABLE_CUSTOMERS_INFO." (customers_info_id, customers_info_date_account_created) values (".$num.", '".$today."')");
		  
		  $address=tep_db_query("insert into ".TABLE_ADDRESS_BOOK." 
										(customers_id,
										 entry_gender,
										 entry_firstname,
										 entry_lastname,
										 entry_street_address,
										 entry_suburb,
										 entry_postcode,
										 entry_city,
										 entry_country_id)
										 value (".$num.",
										'".$_POST['civi']."',
										'".$prenom_cli."',
										'".$nom_cli."',
										'".$adresse_cli."',
										'".$complement_adresse_cli."',
										'".$cp_cli."',
										'".$ville_cli."',
										".$_POST['pays'].")");
		  
		  $adresse_num=tep_db_query("select address_book_id from ".TABLE_ADDRESS_BOOK." where customers_id=".$num."");
		  $res_add_num=tep_db_fetch_array($adresse_num);
		  $num_add=$res_add_num['address_book_id'];
		  
		  $address_defaut_cli=tep_db_query("update ".TABLE_CUSTOMERS." set customers_default_address_id=".$num_add." where customers_id=".$num."");
		 
			
		  $ret.=create_inscription($_POST['mail_cli'], $_POST['panier']);
		  
		  if($_POST['civi']=="m")
		  {
		  $civilite="M";
		  }
		  if($_POST['civi']=="f")
		  {
		  $civilite="Mle";
		  }
		  if($_POST['civi']=="d")
		  {
		  $civilite="Mlle";
		  }
		  
		  $sujet=sujet_message();
		  $message=corps_message_particulier($_POST['nom_cli'],$civilite);
		  mail_client($_POST['mail_cli'],$sujet,$message);
	  }
	  else return '<div class="haut_erreur"></div>
					<div class="milieu_erreur">'.$ret.'</div>
					<div class="clear"></div>  
				<div class="bas_erreur"></div>';
	
	
}


function verif_pro()
{
	$ret='<strong><u>Erreur � corriger pour finaliser votre inscription :</u></strong><br><br>';
	
	$formule_polie="Vous devez indiquer ";
	$verif="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
	$check=true;
	if($_POST['type_activite']==''){
		$ret.=$formule_polie."votre type d'activit� <br />";
		$check=false;
	}
	if($_POST['civi']==''){
		$ret.=$formule_polie."la civilit� du responsable<br />";
		$check=false;
	}
	if($_POST['nom_cli']==''){
		$ret.=$formule_polie."le nom du responsable<br />";
		$check=false;
	}
	if($_POST['prenom_cli']==''){
		$ret.=$formule_polie."le pr�nom du responsable<br />";
		$check=false;
	}
	if($_POST['jour']=='jour'){
		$ret.=$formule_polie."votre jour de naissance<br />";
		$check=false;
	}
	  
	if($_POST['mois']=='mois'){
		$ret.=$formule_polie."votre mois de naissance<br />";
		$check=false;
	}
	if($_POST['annee']=='annee'){
		$ret.=$formule_polie."votre ann�e de naissance<br />";
		$check=false;
	}
	if($_POST['type_activite']=='societe'){
		if($_POST['societe_cli']==''){
			$ret.=$formule_polie."votre nom de soci�t�<br />";
			$check=false;
		}
		$customers_type=2;
	}
	if($_POST['type_activite']=='administration'){
		if($_POST['societe_cli']==''){
			$ret.=$formule_polie."le nom de votre organisme<br />";
			$check=false;
		}
		$customers_type=4;
	}
	if($_POST['adresse_cli']==''){
		$ret.=$formule_polie."une adresse<br />";
		$check=false;  
	}
	if(preg_match("#^[0-9]{4}[0-9]?$#", $_POST['cp_cli'])==false){
		$ret.=$formule_polie."un code postal<br />";
		$check=false;  
	}
	if($_POST['ville_cli']==''){
		$ret.=$formule_polie."une ville<br />";
		$check=false;  
	}
	if($_POST['pays']=='pays'){
		$ret.=$formule_polie."un pays<br />";
		$check=false;
	}
	if(!preg_match("#^0[6-7]([-. ]?[0-9]{2}){4}$#", $_POST['tel_portable_cli'])){
		$ret.=$formule_polie."un num�ro de t�l�phone mobile (valide)<br />";
		$check=false;
	}
	if($_POST['tel_fixe_cli']!=''){
		if(preg_match("#^0[1-9]([-. ]?[0-9]{2}){4}$#", $_POST['tel_fixe_cli'])==false){
			$ret.=$formule_polie."un num�ro de t�l�phone fixe (valide)<br />";
			$check=false;
		}
	}
	if(!preg_match($verif,$_POST['mail_cli'])){
		$ret.=$formule_polie."une adresse e-mail (valide)<br />";
		$check=false;
	}
	else{
	  	$res=tep_db_query("select customers_id from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['mail_cli']."'");
		$num_rows =tep_db_num_rows($res);
		if($num_rows!=0){
			$ret.="Cette adresse e-mail est d�j� utilis�e<br />";
		 	$check=false;
		}
	}
	if($_POST['email_parrain']!=''){
		$res0=tep_db_query("select id_parrainage from ".TABLE_PARRAINAGE." where mail_filleul='".$_POST['mail_cli']."'");
		$res=tep_db_query("select customers_id from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['email_parrain']."'");
		$req_res=tep_db_fetch_array($res);
		$num_rows =tep_db_num_rows($res);
		$num_rows0 =tep_db_num_rows($res0);
		   
		if($num_rows0!=0 && $num_rows!=0){
			$req_verif=tep_db_query("select * from ".TABLE_PARRAINAGE." where id_parrain=".$req_res['customers_id']." and mail_filleul='".$_POST['mail_cli']."' and statut='non lu' ");
			$res_nb=tep_db_num_rows($req_verif);
			if($res_nb==0){			   
				$ret.="D�sol� mais votre adresse fait d�j� l'objet d'un parrainage<br />";
			   	$check=false;
			}
		}
		else{			
			if($num_rows==0){
				$ret.="D�sol� mais l'adresse de votre parrain est inconnue<br />";
				$check=false;
			}
		}
	}
	if(!preg_match($verif,$_POST['conf_mail_cli'])){
		$ret.="Vous devez confirmer votre adresse e-mail (valide)<br />";
		$check=false;
	}
	if($_POST['conf_mail_cli']!=$_POST['mail_cli']){
		$ret.="Erreur lors de la confirmation de l'adresse e-mail <br />";
		$check=false;
	}
	if($_POST['mdp']==''){
		$ret.=$formule_polie."votre mot de passe <br />";
		$check=false;
	}
	if($_POST['conf_mdp']==''){
		$ret.="Vous devez confirmer votre mot de passe <br />";
		$check=false;
	}
	if($_POST['conf_mdp']!=$_POST['mdp']){
		$ret.="Erreur lors de la confirmation du mot de passe <br />";
		$check=false;
	}
	
	if($_POST['fax_cli']!=''){
		if(preg_match("#^0[0-68]([-. ]?[0-9]{2}){4}$#", $_POST['fax_cli'])==false){
			$ret.=$formule_polie."votre num�ro de fax (valide)<br />";
			$check=false;
		}
	}
	if($_POST['pays']!='pays'){
	  	// verification du numero TVA et siret
	  	$req_pays_tva=tep_db_query("select countries_tva_intracom from ".TABLE_COUNTRIES." where countries_id=".$_POST['pays']."");
	  	$res_tva=tep_db_fetch_array($req_pays_tva);
	}
	
	//mis en commentaire et passer a true par ludo => a priori ca posait des soucis pour les administrations et certaines entreprises qui n'ont pas de num de tva
	$verif_tva_cli=true;
	/*if($res_tva['countries_tva_intracom']=='1'){
		if($_POST['siret_cli']!=''){
			$verif_tva_cli=true;
		 	if( strlen($_POST['siret_cli'])<5){
				$ret.=$formule_polie." num&eacute;ro de siret (5 caract&eacute;res minimum) <br />";
			 	$check=false;
			 	$verif_tva_cli=false;
			}
			if($_POST['tva_cli']==''){
				$ret.="Votre numero de tva  n'est pas valide<br />";
				$check=false;
				$verif_tva_cli=false;
			}
		}
		if(strlen($_POST['tva_cli'])<7){
			$ret.="Votre numero de tva  n'est pas valide<br />";
			$check=false;
			$verif_tva_cli=false;
				 
			if(empty($_POST['siret_cli'])){
				$ret.=$formule_polie." num&eacute;ro de siret (5 caract&eacute;res minimum) <br />";
				$check=false;
				$verif_tva_cli=false;
			}
		}
	}*/
	  
	if($_POST['news']==''){
		$newsletter=0;
	}
	else{
		$newsletter=1;
	}
	if($_POST['sms']==''){
		$sms=0;
	}
	else{
		$sms=1;
	}
	if($_POST['rgpd']=='') {
		$ret.="Vous devez cocher et accepter la RGPD<br />";
		$check=false;
	}
	
	if($check==true){
		if($verif_tva_cli==true){
			$verif_tva_intra=1;
	  	}
	  	else $verif_tva_intra=0;
	  	
		//Suppression des espaces dans le num�ro de TVA et SIRET
		$tva_cli = format_tva($_POST['tva_cli']);
		$siret_cli = format_text($_POST['siret_cli']);
		
		$nom_cli = format_text($_POST['nom_cli']);
		$prenom_cli = format_text($_POST['prenom_cli']);
		$adresse_cli = format_text($_POST['adresse_cli']);
	
		$complement_adresse_cli = format_text($_POST['complement_adresse_cli']);
		$cp_cli = format_text($_POST['cp_cli']);
		$ville_cli = format_text($_POST['ville_cli']);
		
		$societe_cli = format_text($_POST['societe_cli']);
		
		$tel_portable_cli = format_tel($_POST['tel_portable_cli']);
		$tel_fixe_cli = format_tel($_POST['tel_fixe_cli']);
		$fax_cli = format_tel($_POST['fax_cli']);
		
		
		$mdp=tep_encrypt_password($_POST['mdp']);
		$exe_req=tep_db_query("insert into ".TABLE_CUSTOMERS." (customers_gender,
															  customers_firstname,
															  customers_lastname,
															  customers_dob, 
															  customers_email_address, 
															  customers_telephone,
															  customers_fixe,
															  customers_password,
															  customers_newsletter,
															  customers_news_sms,
															  customers_type,
															  customers_country,
															  customers_company,
															  customers_fax,
															  customers_siret,
															  customers_tva,
															  customers_tva_verif) 
									  
								  				values ('".$_POST['civi']."',
														'".$prenom_cli."',
														'".$nom_cli."',
														'".$_POST['annee']."-".$_POST['mois']."-".$_POST['jour']."',
														'".$_POST['mail_cli']."',
														'".$tel_portable_cli."',
														'".$tel_fixe_cli."',
														'".$mdp."',
														'".$newsletter."',
														'".$sms."',
														'".$customers_type."',
														".$_POST['pays'].",
														'".$societe_cli."',
														'".$fax_cli."',
														'".$siret_cli."',
														'".$tva_cli."',
														'".$verif_tva_intra."')");
		  
		
			
			
		  $req_num_cli=tep_db_query("select customers_id from ".TABLE_CUSTOMERS." where customers_email_address='".$_POST['mail_cli']."' ");
		  $res=tep_db_fetch_array($req_num_cli);
		  $num=$res['customers_id'];
		  
		  $today = date("Y-m-d H:i:s");
		  
		  $customers_info=tep_db_query("insert into ".TABLE_CUSTOMERS_INFO." (customers_info_id, customers_info_date_account_created) values (".$num.", '".$today."')");
		  
		  $address=tep_db_query("insert into ".TABLE_ADDRESS_BOOK." 
										(customers_id,
										 entry_gender,
										 entry_company,
										 entry_firstname,
										 entry_lastname,
										 entry_street_address,
										 entry_suburb,
										 entry_postcode,
										 entry_city,
										 entry_country_id)
										 values (".$num.",
										'".$_POST['civi']."',
										'".$societe_cli."',
										'".$prenom_cli."',
										'".$nom_cli."',
										'".$adresse_cli."',
										'".$complement_adresse_cli."',
										'".$cp_cli."',
										'".$ville_cli."',
										".$_POST['pays'].")");
		  
		  
		  
		  $adresse_num=tep_db_query("select address_book_id from ".TABLE_ADDRESS_BOOK." where customers_id=".$num."");
		  $res_add_num=tep_db_fetch_array($adresse_num);
		  $num_add=$res_add_num['address_book_id'];
		  
		  $address_defaut_cli=tep_db_query("update ".TABLE_CUSTOMERS." set customers_default_address_id=".$num_add." where customers_id=".$num."");
		  
		  
												
		  $ret.=create_inscription($_POST['mail_cli'], $_POST['panier']);
		  
		  if($_POST['civi']=="m")
		  {
		  $civilite="M";
		  }
		  if($_POST['civi']=="f")
		  {
		  $civilite="Mle";
		  }
		  if($_POST['civi']=="d")
		  {
		  $civilite="Mlle";
		  }
		  
		  $sujet=sujet_message();
		  $message=corps_message_pro($_POST['nom_cli'],$civilite);
		  mail_client($_POST['mail_cli'],$sujet,$message);
	  }
	else return '<div class="haut_erreur"></div>
					<div class="milieu_erreur">'.$ret.'</div>
					<div class="clear"></div>  
				<div class="bas_erreur"></div>';
	  

}


function tep_get_tva_intracom_array() {
    $intracom_array = array('AT'=>'AT',    //Austria
            'BE'=>'BE',    //Belgium
            'DK'=>'DK',    //Denmark
            'FI'=>'FI',    //Finland
            'FR'=>'FR',    //France
            'FX'=>'FR',    //France m�tropolitaine
            'DE'=>'DE',    //Germany
            'GR'=>'EL',    //Greece
            'IE'=>'IE',    //Irland
            'IT'=>'IT',    //Italy
            'LU'=>'LU',    //Luxembourg
            'NL'=>'NL',    //Netherlands
            'PT'=>'PT',    //Portugal
            'ES'=>'ES',    //Spain
            'SE'=>'SE',    //Sweden
            'GB'=>'GB',    //United Kingdom
            'CY'=>'CY',    //Cyprus
            'EE'=>'EE',    //Estonia
            'HU'=>'HU',    //Hungary
            'LV'=>'LV',    //Latvia
            'LT'=>'LT',    //Lithuania
            'MT'=>'MT',    //Malta
            'PL'=>'PL',    //Poland
            'SK'=>'SK',    //Slovakia
            'CZ'=>'CZ',    //Czech Republic
            'SI'=>'SI');   //Slovania
    return $intracom_array;
}

function tep_verif_tva($num_tva){
    $prefix = substr($num_tva, 0, 2);
    if (array_search($prefix, tep_get_tva_intracom_array() ) === false) {
        return 'false';
    }
    $tva = substr($num_tva, 2);    
    $monfd = file_get_contents('https://ec.europa.eu/taxation_customs/vies/viesquer.do?ms='.$prefix.'&iso='.$prefix.'&vat='.$tva, r);
        if ( eregi("invalid VAT number", $monfd) ) {
            return 'false';
        } elseif ( eregi("valid VAT number", $monfd) ){
            return 'true';
        } else {
            $myVerif = 'no_verif';
        }
    return $myVerif;
}

?>