/* Auteur : Sami 
   Page servant pour la page des newsletter*/

//Retourne les rayons
function rayon_fc(id){
	var rayon = document.getElementById("rayon"+id).value;
		
	var req;
	
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");

		req.onreadystatechange = function()
		{
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
					document.getElementById("marque"+id).disabled=false;
					document.getElementById("modele"+id).disabled=true;
					document.getElementById("modele"+id).innerHTML="<option value='modele'>Choisissez un mod�le</option>";
					document.getElementById("marque"+id).innerHTML=req.responseText;
				}
				else
				{
					document.getElementById("marque"+id).innerHTML="Error: returned status code " + req.status + " " + req.statusText;
					
				}
			}
		};
		req.open("GET", "fonction/fonctions_ajax/newsletter.php?rayon="+rayon, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	
	
}

//Retourne les marques
function marque_fc(id){
	var marque = document.getElementById("marque"+id).value;
			
	var req;
	
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");

		req.onreadystatechange = function()
		{
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
					document.getElementById("modele"+id).disabled=false;
					document.getElementById("modele"+id).innerHTML=req.responseText;					
				}
				else
				{ document.getElementById("modele"+id).innerHTML="Error: returned status code " + req.status + " " + req.statusText; }
			}
		};
		req.open("GET", "fonction/fonctions_ajax/newsletter.php?marque="+marque, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	
}

//permet d'enregistrer les choix du client 'les parties comment�es serviront surement pour la suite)
function enregistrer_fc(id){
	var frequence=document.getElementById("freq").value;
	
	var Abo=1;
	/*var abonnement=document.getElementById("abo");
	 if(abonnement.checked==true) Abo=0; else Abo=1;*/
	 
	var Gene
	var general=document.getElementById("gene");
	 if(general.checked==true) Gene=1; else Gene=0;
	 
	var Part
	var partenaire=document.getElementById("part");
	 if(partenaire.checked==true) Part=1; else Part=0;
	 
	var Promo = 0;
	/*var promotion=document.getElementById("promo");
	 if(promotion.checked==true) Promo=1; else Promo=0;*/
	 
	var Nouv = 0;
	/*var nouveaute=document.getElementById("nouv");
	 if(nouveaute.checked==true) Nouv=1; else Nouv=0;*/
		
	var rayon=document.getElementById("rayon"+id).value;
	var marque=document.getElementById("marque"+id).value;
	var modele=document.getElementById("modele"+id).value;
	
	if(rayon=='rayon')
	{ alert("Vous devez au moins sp�cifier un rayon"); }
	else{
	var req;
	
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");

		req.onreadystatechange = function()
		{
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
					var rep=req.responseText;
					if(rep=="Attention")
					{alert("Vous avez d�j� enregstr� ce choix pour la newsletter");}
					else{
						 alert("Votre choix a bien �t� pris en compte");
						 document.getElementById("block_produit").innerHTML=rep;}					
				}
				else
				{ document.getElementById("block_produit").innerHTML="Error: returned status code " + req.status + " " + req.statusText; }
			}
		};
		req.open("GET", "fonction/fonctions_ajax/newsletter.php?r="+rayon+"&m="+marque+"&mo="+modele+"&abo="+Abo+"&gene="+Gene+"&part="+Part+"&freq="+frequence+"&promo="+Promo+"&nouv="+Nouv, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	}
}

//Permet de supprimer un des choix du client
function supprime_fc(id){
	var rayon=document.getElementById("rayon_e"+id).value;
	var marque=document.getElementById("marque_e"+id).value;
	var modele=document.getElementById("modele_e"+id).value;
	
	if(rayon=='rayon')
	{ alert("attention");}
	
	else
   {
	var req;
	
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");

		req.onreadystatechange = function()
		{
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{ document.getElementById("block_produit").innerHTML=req.responseText; }
				else
				{ document.getElementById("block_produit").innerHTML="Error: returned status code " + req.status + " " + req.statusText; }
			}
		};
		req.open("GET", "fonction/fonctions_ajax/newsletter.php?r_supr="+rayon+"&m_supr="+marque+"&mo_supr="+modele, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	 }		
}