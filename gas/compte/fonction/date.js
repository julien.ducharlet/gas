/* Auteur : Paul 
   Cr�ation d'un menu en fonction du nombre de jour dans un mois donn�*/

function selectionne() {
	// On r�cup�re les infos sur la date
	var jour = document.getElementById("jours").value;
	var mois = document.getElementById("mois").value;
	if (mois == 'mois')
		mois = '';
	var annee = document.getElementById("annees").value;
	if (annee == 'annee')
		annee = '';
	
    var req;
	
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject("Msxml2.XMLHTTP");

		req.onreadystatechange = function()
		{
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
					// Si c'est bon on r�cup�re le menu
					document.getElementById("jours").innerHTML=req.responseText;
				}
				else
				{
					// Sinon on affiche le message d'erreur
					document.getElementById("jours").innerHTML="Error: returned status code " + req.status + " " + req.statusText;
				}
			}
		};
		req.open("GET", "fonction/fonctions_ajax/menu_jour.php?jour="+jour+"&mois="+mois+"&annee="+annee, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}