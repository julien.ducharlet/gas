<?php
/* Auteur : Sami 
   Fonctions specifiques � l'obtention d'un nouveau mot de passe recuper�es de osCommerce*/
   
function tep_not_null($value) {
	
		if (is_array($value)) {
			if (sizeof($value) > 0){
				return true;
			}
		  	else {return false;}
		}
		else {
		  	if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {
			  	return true;
			}
		  else {return false;}
	  	}
} 

// Return a random value
  function tep_rand($min = null, $max = null) {
    static $seeded;

    if (!$seeded) {
      mt_srand((double)microtime()*1000000);
      $seeded = true;
    }

    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }
  

//Fonction permettant de comparer le mot de passe pr�sent en base et le mot de passe saisie par le client lors de sa connexion ou autre
  function tep_validate_password($plain, $encrypted) {
	
    if (tep_not_null($plain) && tep_not_null($encrypted)) {
// split apart the hash / salt
      $stack = explode(':', $encrypted);
		
      if (sizeof($stack) != 2) return false;

      if (md5($stack[1] . $plain) == $stack[0]) {
        return true;
      }
    }
    return false;
  }
 
//Fonction permettant de creer et crypter un mot de passe
  function tep_encrypt_password($plain) {
    $password = '';

    for ($i=0; $i<10; $i++) {
      $password .= tep_rand();
    }
    $salt = substr(md5($password), 0, 2);
    $password = md5($salt . $plain) . ':' . $salt;
    return $password;
  }
?>