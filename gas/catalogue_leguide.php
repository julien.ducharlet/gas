<?php


	require('includes/page_top.php');
	
	Header( 'Content-Type: text/xml' );
	
	define(DELAI_LVRAISON, '3-15 jours');
	define(ZONE_LIVRAISON, '1');
	
	$i = 1;
	$price_is_reduced = false;
	$tab_livraison = array();
	$tab_fdp_id = 0;
	
	$query_tab_fdp = 'select poids_livraison, prix_livraison from '. TABLE_MODULE_LIVRAISON .' where zone_livraison_id=1 order by poids_livraison desc';
	$query_tab_fdp = tep_db_query($query_tab_fdp);
	
	while($data = tep_db_fetch_array($query_tab_fdp)) {
		
		$tab_livraison[] = array('poids' => $data['poids_livraison'], 'prix' => $data['prix_livraison']);
	}
	
	//print_r($tab_livraison);
	
	$query = 'select * from '. TABLE_PRODUCTS .' p, '. TABLE_PRODUCTS_DESCRIPTION .' pd, '. TABLE_MANUFACTURERS .' m, ';
	$query .= TABLE_PRODUCTS_TO_CATEGORIES .' ptc, '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd ';
	$query .= 'where p.products_id=pd.products_id and p.manufacturers_id=m.manufacturers_id ';
	$query .= 'and p.products_id=ptc.products_id and ptc.categories_id=c.categories_id and c.categories_id=cd.categories_id and products_status=1 ';
	$query .= 'order by ptc.categories_id';
	
	$query = tep_db_query($query);
	
	echo '<?xml version="1.0" encoding="ISO-8859-1"?>'; 
 	
	echo "\n";
  // Déclaration du catalogue. Il est recommandé d'indiquer correctement la langue et le fuseau horaire GMT.
  	echo '<catalogue lang="FR" date="'.  date('Y-m-d H:i'). '" GMT="+1" version="2.0">';
	
	while($data = tep_db_fetch_array($query)) {
		
		/* initialisation des variables */
		$cPath = array();
		$unique_id = array();
		$categorie = array();
		
		
		
		$cPath[] = $data['products_id'] .'-'. bda_product_name_transform($data['products_url'], $data['categories_id']);
		$unique_id[] = $data['products_id'];
		//$categorie[] = bda_product_name_transform($data['products_url'], $data['categories_id']);
		$cat = $data['categories_id'];
		
		while($cat > 0) {
			
			$query_cat = 'select c1.categories_id, c1.parent_id, categories_name, categories_url from '. TABLE_CATEGORIES .' c1, '. TABLE_CATEGORIES .' c2, '. TABLE_CATEGORIES_DESCRIPTION .' cd ';
			$query_cat .= 'where c1.categories_id=c2.categories_id and c1.categories_id=cd.categories_id and c1.categories_id='. $cat;
			
			$query_cat = tep_db_query($query_cat);
			
			$data_cat = tep_db_fetch_array($query_cat);
			
			$cPath[] = $data_cat['categories_id'] .'-'. $data_cat['categories_url'];
			$unique_id[] = $data_cat['categories_id'];
			$categorie[] = $data_cat['categories_name'];
			$cat = $data_cat['parent_id'];
		}
		
		$query_rubrique = 'select r.rubrique_id, rubrique_url from '. TABLE_CATEGORIES_RAYON .' cr, '. TABLE_RAYON  .' r ';
		$query_rubrique .= 'where cr.rubrique_id=r.rubrique_id and categories_id='. $data['categories_id'];
		
		$query_rubrique = tep_db_query($query_rubrique);
		$data_rubrique = tep_db_fetch_array($query_rubrique);
		
		$cPath[] = $data_rubrique['rubrique_id'] .'-'. $data_rubrique['rubrique_url'];
		$unique_id[] = $data_rubrique['rubrique_id'];
		
		$cPath = array_reverse($cPath);
		$cPath = implode('/', $cPath);
		
		$categorie = array_reverse($categorie);
		$categorie = implode(' > ', $categorie);
		
		//$unique_id = array_reverse($unique_id);
		$unique_id = implode('', $unique_id);
		
		$cPath = encode_url($cPath);
		/* fin du calcul des chemins */
		
		
		$query_promo = 'select specials_new_products_price as promo_price from '. TABLE_SPECIALS .' ';
		$query_promo .= 'where products_id='. $data['products_id'] .' and (expires_date=\'0000-00-00 00:00:00\' or expires_date<NOW())';
		$query_promo = tep_db_query($query_promo);
		
		if(tep_db_num_rows($query_promo) > 0) {
			
			$data_promo = tep_db_fetch_array($query_promo);
			$prix_vente = $data_promo['promo_price'];
			
			$price_is_reduced = true;
		}
		else {
			// pas de promo, on vérifie si le produit est en vente flash
			$query_vente_flash = 'select prix_vente from '. TABLE_PRODUCTS_VENTES_FLASH .' where products_id='. $data['products_id'] .' and debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash=\'0000-00-00\')';
			$query_vente_flash = tep_db_query($query_vente_flash);
			
			if(tep_db_num_rows($query_vente_flash) > 0) {
				
				$data_vente_flash = tep_db_fetch_array($query_vente_flash);
				$prix_vente = $data_vente_flash['prix_vente'];
				
				$price_is_reduced = true;
			}
			else {
				
				$prix_vente = $data['products_price'];
			}
		}
		
		
		echo "\n\t";
		echo '<product place="'. $i++ .'">';
		
			echo "\n\t\t";
			//  Prendre le champ "family_client_id" et trouver le texte dans la table "family_client" champ "family_client_name"
			echo '<categorie><![CDATA['. $categorie .']]></categorie>';
			echo "\n\t\t";
			// Prendre l'ID (products_id) + ajouter le CPATH pour ne pas avoir de probleme avec les articles dupliqués
			echo '<identifiant_unique><![CDATA['. $unique_id .']]></identifiant_unique>';
			echo "\n\t\t";
			// Prendre le champ "products_name" dans la table "products_description" (utiliser la fonction qui traduit les [@] [#] [°] ...)
			echo '<titre><![CDATA['. bda_product_name_transform($data['products_name'], $data['categories_id']) .']]></titre>';
			echo "\n\t\t";
			// Prendre le champ "products_description" dans la table "products_description" (utiliser la fonction qui traduit les [@] [#] [°] ...)
			echo '<description><![CDATA['. bda_product_name_transform($data['products_description'], $data['categories_id']) .']]></description>';
			echo "\n\t\t";
			// Si pas en promo, ni en vente flash mettre le prix de vente normal champs "products_price"
			echo '<prix currency="EUR">'. str_replace('.', ',', format_to_money($prix_vente*1.2)) .'</prix>';
			echo "\n\t\t";
			// Utiliser l'url rewriting
			echo '<url_produit><![CDATA['. WEBSITE . BASE_DIR .'/'. $cPath .'.html]]></url_produit>';
			echo "\n\t\t";
			// Prendre le champ "products_bimage" avec devant en dur 
			echo '<url_image><![CDATA['. WEBSITE . BASE_DIR .'/images/products_pictures/normale/'. $data['products_bimage'] .']]></url_image>';
			echo "\n\t\t";
			// Faire le calcul en fonction de "products_weight" et de la table "module_livraison" uniquement pour la france (zone_livraison_id=1 pour BDA).
			
			while($tab_livraison[$tab_fdp_id++]['poids'] > $data['products_weight']) {
				//echo $tab_livraison[$tab_fdp_id]['poids'] .' - '. $data['products_weight'] .' =>'. $tab_livraison[$tab_fdp_id]['prix'] .'<br>';
				//boucle qui s'incrémente : passe au poids suivant
			}
			
			
			//echo '<frais_de_livraison>'. str_replace('.', ',', format_to_money($tab_livraison[--$tab_fdp_id]['prix']*1.2)) .'</frais_de_livraison>'; 
			echo '<frais_de_livraison>7</frais_de_livraison>'; 
			echo "\n\t\t";
			// Toujours mettre disponible mais faire un controle dans la boucle pour que si le champs "products_status" est = à 0 on ne génère pas l'article.
			echo '<disponibilite>1</disponibilite>';
			echo "\n\t\t";
			// Pour BDA "24-48 heures" et pour GAS "3-15 jours"
			echo '<delai_de_livraison>'. DELAI_LVRAISON .'</delai_de_livraison>';
			echo "\n\t\t";
			// prendre le champs "products_garantie"
			echo '<garantie>'. $data['products_garantie'] .' mois</garantie>';
			echo "\n\t\t";
			// Prendre le champ "products_model"
			echo '<reference_modele><![CDATA['. $data['products_model'] .']]></reference_modele>';  // on mets le champ ""
			echo "\n\t\t";
			// Prendre le champ "products_ecotaxe"
			echo '<D3E>'. str_replace('.', ',', $data['products_ecotaxe']) .'</D3E>';
			echo "\n\t\t";
			// 
			echo '<marque><![CDATA['. $data['manufacturers_name'] .']]></marque>';
			echo "\n\t\t";
			// Prendre le champ "products_code_barre"
			if(!empty($data['products_code_barre'])) {
				
				echo '<ean>'. $data['products_code_barre'] .'</ean>';
				echo "\n\t\t";
			}
			
			if($price_is_reduced) {
				
				echo '<prix_barre currency="EUR">'. str_replace('.', ',', format_to_money($data['products_price']*1.2)) .'</prix_barre>'; // enfonction de products_pricce uniquement si prix promo ou vente flash
				echo "\n\t\t";
			}
			// 
			echo '<devise>EUR</devise>';  // EUR on ne touche plus
			echo "\n\t\t";
			//
			echo '<type_promotion>0</type_promotion>';
			echo "\n\t\t";
			// 
			echo '<occasion>0</occasion>'; // 0 on ne touche plus car nous ne vendons que du neuf = 0
			echo "\n\t";
			// 
			//echo '<url_mobile>'. WEBSITE .'/'. $data['products_garantie'] .'</url_mobile>';  // A voir si on fait un dev pour les mobiles
			//echo "\n\t";
	
		echo '</product>';
	}
  
  echo "\n";
  echo '</catalogue>'; 