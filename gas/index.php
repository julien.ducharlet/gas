<?php
/* Page d'accueil du site */
   
$nom_page = "accueil";

require("includes/page_top.php");
require("includes/meta/index.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">
	<?php include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_fil_ariane.php"); ?>

	<div id="accueil" >
    	<div class="menu">
          	<div class="menu_top"></div>
            <div class="menu_middle">
                <ul class="menu_liste">
                    <?php
						$premier = true;
                        // R�cup�ration du nom des marques en pr�cisant un sort_order != 0 pour masquer les cat�gories cach�es et >0 pour ne pas afficher les cat�gories g�n�riques (cartes_m�moire, etc...)
                        $reponse2 = tep_db_query("SELECT cd.categories_name, cd.categories_url, cd.categories_id, r.rubrique_url, cr.rubrique_id, cd.balise_title_lien_texte, categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, categories_status_adm, categories_status_vip
                                                 FROM " . TABLE_CATEGORIES . " as c 
												 inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on c.categories_id=cd.categories_id 
												 inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id 
												 inner join " . TABLE_RAYON . " as r on cr.rubrique_id=r.rubrique_id
                                                 WHERE c.parent_id='0' AND c.sort_order > 0 AND cr.rubrique_id = 1 and c.categories_status='1'
                                                 ORDER BY c.sort_order, cd.categories_name ASC");
                        while($donnees2 = tep_db_fetch_array($reponse2)){
                            if(affiche_info($session_customer_id, $session_customers_type, $session_customers_vip, $donnees2['categories_status_tous'], $donnees2['categories_status_client'], $donnees2['categories_status_rev'], $donnees2['categories_status_pro'], $donnees2['categories_status_adm'], $donnees2['categories_status_vip'])){
								if ($premier) {
									echo "<li style='border-top: 1px dashed " . COULEUR_10 . ";'><a href=\"" . url('marque', array('id_rayon' => $donnees2['rubrique_id'], 'nom_rayon' => $donnees2['rubrique_url'], 'id_marque' => $donnees2['categories_id'], 'url_marque' => $donnees2['categories_url'])) . "\" title=\"".$donnees2["balise_title_lien_texte"]."\">" . $donnees2['categories_name'] . "</a></li>";
									$premier = false;
								} else {
									echo "<li><a href=\"" . url('marque', array('id_rayon' => $donnees2['rubrique_id'], 'nom_rayon' => $donnees2['rubrique_url'], 'id_marque' => $donnees2['categories_id'], 'url_marque' => $donnees2['categories_url'])) . "\" title=\"".$donnees2["balise_title_lien_texte"]."\">" . $donnees2['categories_name'] . "</a></li>";
								}
							}
						}
                    ?>
                </ul>
            </div>
            <div class="menu_bottom"></div>
			<br />
			
            
			<?php // AVIS VERIFIES #7C7972 ?>
			<meta class="netreviewsWidget" id="netreviewsWidgetNum14786" data-jsurl="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/widget01-14786_script.js"/><script src="//cl.avis-verifies.com/fr/widget4/widget01.min.js"></script>
			<?php // End AVIS VERIFIES ?>
				
			<br />
			<!--<br />
			<div class="menu_top"></div>
            <div class="menu_middle" style="text-align:center;">
			
				<a href="details_pour_administration.php"><img src="<?php echo BASE_DIR; ?>/template/base/accueil/bouton_administrtation.png" alt="Avantages pour les Administrations"/></a><br />
				<br />
				<a href="details_pour_pro.php"><img src="<?php echo BASE_DIR; ?>/template/base/accueil/bouton_revendeur.png" alt="Avantages pour les revendeurs"/></a><br />
                			
            </div>
            <div class="menu_bottom"></div>
			-->
			<br />
			<div>
				<a href="../gas/faq.php?Faq_Fam=76&Faq_id=69" title="Group Army Store vous livre aussi en Secteur postal Militaire">
					<img src="<?php echo BASE_DIR; ?>/template/base/menu/livraison_sp-armee_accueil.png" width="200" alt="Group Army Store vous livre aussi en Secteur postal Militaire" />
				</a>
			</div>
			<br />
			<div>
				<a href="../gas/faq.php?Faq_Fam=75" title="Les modes de paiement propos�s par Group Army Store">
					<img src="<?php echo BASE_DIR; ?>/template/base/menu/modes_de_paiement.jpg" width="200" alt="les modes de paiement de Group Army Store" />
				</a>
			</div>
			
        </div>
		
		<?php // ZONE DE TEST DEBUT ?>
		<?php //if(isset($_SESSION) && ($_SESSION['customer_id']==3 || $_SESSION['customer_id']==9535)) {
				
			$query = 'select * from '. TABLE_PRODUCTS_VENTES_FLASH .' where fin_vente_flash >NOW() or fin_vente_flash=\'0000-00-00\'';
			$query = tep_db_query($query);
			
			if(tep_db_num_rows($query) > 0 && $_SESSION['customers_type']!=3) { ?>
			<div class="bandeau"  style="height:117px; text-align:center">
				<a href="<?php echo BASE_DIR; ?>/ventes_flash.php"><img src="<?php echo BASE_DIR; ?>/template/base/index/banniere_vente_flash.gif" width="724" height="117" /></a>
			</div>
			
		<?php } ?>
		<?php // ZONE DE TEST FIN ?>		
		
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="<?php echo BASE_DIR; ?>/css/slider.css" >
		<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/slider.js"></script>
		<script>
			$(document).ready(function(){
				$('.slider').bxSlider({
					auto: true,
					pager: true,
					slideWidth: 700, /* Largeur du slide */
					pause: 3000, /* 3 secondes entre les images */
					autoHover: true, /* se met du slide en pause au survole */
					autoHover: true, /* se met du slide en pause au survole */
					autoControls: false, /* Affiche  */
					touchEnabled: false
				});
			});
		</script>
		<div class="texte_index"><?php include("includes/languages/french/define_slider.php"); ?></div>
		<div class="texte_index"><?php include("includes/languages/french/define_mainpage.php"); ?></div>
		<div class="zone_defilement_logo_marque" align="center"><?php include("includes/modules/defilement_logos_marques.php"); ?></div>
		
		<div class="texte_avisverifies">
			<iframe src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2aproduit_all_index.html" style="border:none;width: 100%;"  ></iframe>
		</div>
		
		<br />
		<br />
				
        <!--<div class="top_ten_accueil">
	        <?php //include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_top_ten.php"); ?>
        </div>-->
    </div>
    
    <div style="clear:both;"></div>
</div>


<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>