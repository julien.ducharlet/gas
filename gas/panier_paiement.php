<?php 
/* Page permettant de choisir son moyen de paiement */ 
 
$nom_page = "panier";    
require("includes/page_top.php"); 
 
// Inclusion des fonctions d'affichage du panier 
require("includes/fonctions/fonctions_panier.php"); 
 
if (empty($_SESSION['customer_id']) || !isset($_SESSION['adresse_livraison'])) { ?> 
	<meta http-equiv="refresh" content="0; URL='<?php echo BASE_DIR; ?>/index.php'"> <?php 
} else { 
	$query=tep_db_query("select * from ".TABLE_CUSTOMERS_BASKET." where customers_id=". $_SESSION['customer_id']); 
	$nb_redirect=tep_db_num_rows($query); 
	 
	while($data = tep_db_fetch_array($query)) { 
		if($data['customers_basket_quantity'] <= 0) { 
			tep_db_query('delete from '. TABLE_CUSTOMERS_BASKET .' where customers_id='. $_SESSION['customer_id'] .' and customers_basket_id='. $data['customers_basket_id']); 
		} 
	} 
 
	if ($nb_redirect==0) { ?> 
		<meta http-equiv="refresh" content="0; URL='<?php echo BASE_DIR; ?>/index.php'"> <?php 
	} else { 
		require("includes/meta/panier_paiement.php"); 
		require("includes/meta_head.php"); 
		require("includes/header.php"); 
		include("includes/fonctions/fonctions_coupon.php"); 
		prix_livraison(); 
		 
		if (isset($_POST['montant'])) { 
			$montant=$_POST['montant']; 
			$code=$_POST['promotion']; 
			englob_coupon($code,$montant); 
		} elseif (isset($_SESSION['coupon_affectation_remise'])  && !empty($_SESSION['coupon_affectation_remise'])) { 
			englob_coupon($_SESSION['code_promo'],format_to_money(sous_total_panier($_SESSION['customer_id'])));    
		} 
		?> 
 
	<?php 
	// SEULEMENT POUR THIERRY 
	/*if ($_SESSION['customer_id']==3) { 
		print_r($_SESSION); 
		echo '<br><br>'; 
		echo 'Customers_type : '; 
		echo $_SESSION['customers_type'];  
		echo '<br><br>'; 
	}*/
	?> 
	 
	<style type="text/css"> 
		.container { display: block; position: relative; padding-left: 25px; margin-bottom: 12px; cursor: pointer; font-size: 22px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; } 
		.container input { position: absolute; opacity: 0; cursor: pointer; height: 0; width: 0;} 
		.checkmark { position: absolute; top: 0; left: 0; height: 15px; width: 15px; background-color: #eee; border-radius: 50%; border: solid 2px #808080;} 
		.container:hover input ~ .checkmark { background-color: #ccc; } 
		.container input:checked ~ .checkmark { background-color: #EEEEEE; /*#2196F3*/ } 
		.checkmark:after { content: ""; position: absolute; display: none; } 
		.container input:checked ~ .checkmark:after { display: block; } 
		.container .checkmark:after { top: 3px; left: 3px; width: 9px; height: 9px; border-radius: 50%; background: #676767; } 
	</style> 
 
	<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/panier_paiement.js"></script> 
	<div id="corps"> 
		<div id="panier"> 
			<div class="titre" style="margin-bottom:10px;">Choisissez le mode de paiement de votre commande</div> 
            <div style="margin-bottom:10px;" >Le contenu de votre commande : <a href="panier.php">modifier</a></div> 
             
			<!-- Tableau contenant les articles choisis par le client --> 
			<?php 
			$panier_query = tep_db_query("	SELECT cb.products_id, cb.customers_basket_quantity, cb.id_rayon 
											FROM ".TABLE_CUSTOMERS_BASKET." as cb 
											WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' and cb.pack_id = ''"); 
			$i = 0; 
			$nb_articles = tep_db_num_rows($panier_query); 
			if ($nb_articles > 0) { 
				?> 
				<table id="table_articles"> 
					<thead> 
						<tr> 
							<th colspan="2">Articles</th> 
							<th>Prix Unitaire</th> 
							<th>Quantit&eacute;</th> 
							<th>Prix total</th> 
						</tr> 
					</thead> 
					<tbody> 
						<?php 
                        $articles_sous_total_HT = 0;
						while($panier_data = tep_db_fetch_array($panier_query)){ 
							if (!empty($panier_data['products_id'])) { 
								$id_complet = $panier_data['products_id']; 
								$i++; 
								 
								// On rÃ¯Â¿Â½cupÃ¯Â¿Â½re les infos sur les options
                                if (isset($panier_data['products_id'])) {
								    $article_options = explode("{", $panier_data['products_id']); 
                                }
								if (isset($article_options[1])) {
                                    $options = explode("}", $article_options[1]); 
                                }
                                if ((isset($options[0])) && (isset($options[1]))) {
								    $option_details = infos_options($options[0], $options[1]); 
                                }
                                else {
                                    $option_details = "";
                                }
								// On rÃ¯Â¿Â½cupÃ¯Â¿Â½re les infos sur l'article 
								$article = explode("_", $article_options[0]); 
								$quantite = $panier_data['customers_basket_quantity']; 
								$article_details = infos_article($article, $quantite); 
								 
								$today = date("Y-m-d H:i:s"); 
									$req_promo=tep_db_query("select specials_new_products_price, expires_date from specials where products_id=".$article_details['id_article']." and (expires_date >'".$today."' or expires_date='0000-00-00 00:00:00')"); 
									$res_promo=tep_db_fetch_array($req_promo); 
									$nb_row=tep_db_num_rows($req_promo); 
									if($nb_row!=0){ 
										$articles_sous_total_HT += $res_promo['specials_new_products_price']*$quantite; 
									} else { 
										$articles_sous_total_HT += $article_details['prix_ht']*$quantite;  
									} 
 
								// On affiche les articles du panier 
								affiche_article_panier($nb_articles, $id_complet, $panier_data['id_rayon'], $article_details, $option_details, $quantite, $i, false, ""); 
							} 
						} 
					} 
					?> 
					</tbody> 
				</table> 
				<?php 
				$pack_sous_total_HT = 0; 
				$packs_query = tep_db_query("	SELECT cb.pack_id, cb.customers_basket_quantity, cb.id_rayon 
												FROM ".TABLE_CUSTOMERS_BASKET." as cb 
												WHERE  
												cb.customers_id = '" . $_SESSION['customer_id'] . "'  
												AND  
												cb.pack_id <> '' 
											"); 
				$nb_packs = tep_db_num_rows($packs_query); 
				if ($nb_packs > 0) { ?> 
				<!-- Tableau contenant les Packs choisis par le client --> 
				<table id="table_packs"> 
					<thead> 
						<tr> 
							<th colspan="2">Packs</th> 
							<th>Prix Normal</th> 
							<th>Prix du Pack</th> 
							<th>Economie R&eacute;alis&eacute;e</th> 
							<th>Quantit&eacute;</th> 
							<th>Prix total</th> 
						</tr> 
					</thead> 
					<tbody> 
                 
                <?php					 
					while ($packs_data = tep_db_fetch_array($packs_query)) { 
						$id_complet = $packs_data['pack_id']; 
						$i++; 
						 
						// On rÃ¯Â¿Â½cupÃ¯Â¿Â½re les infos sur les options 
						$pack_options = explode("{", $id_complet); 
						$options = explode("}", $pack_options[1]); 
						$option_details = infos_options($options[0], $options[1]); 
						 
						// On rÃ¯Â¿Â½cupÃ¯Â¿Â½re les infos sur le pack 
						$pack = explode("_", $pack_options[0]); 
						$pack_details = info_pack($pack, $option_details); 
						 
						// On rÃ¯Â¿Â½cupÃ¯Â¿Â½re des infos complÃ¯Â¿Â½mentaires 
						$quantite = $packs_data['customers_basket_quantity']; 
						$pack_sous_total_HT += $pack_details['prix_ht']*$quantite; 
						 
						//On affiche le pack et la liste de ses articles 
						affiche_pack_panier($id_complet, $packs_data['id_rayon'], $pack_details, $quantite, $i, false); 
					} 
				} 
                ?> 
                </tbody> 
            </table>      
        <div id="totaux"> 
         
           <?php $sous_total_total = $articles_sous_total_HT + $pack_sous_total_HT; ?> 
           Sous-total HT : <strong><span id="ss_total_ht"><?php echo format_to_money($sous_total_total); ?></span> &euro;</strong><br /> 
           Frais de livraison HT : 
            <strong><span id="tva"><?php if(taux_taxe()==1.2) echo $port=format_to_money($_SESSION['frais_livraison']/1.2); 
		   								 else echo $port=format_to_money($_SESSION['frais_livraison']); 
								   ?> 
                    </span> &euro; 
             </strong><br /> 
           <?php 
            $remise = 0;
		     //si on a une remise par coupon 
			 if( isset($_SESSION['coupon_affectation_remise'])  && !empty($_SESSION['coupon_affectation_remise']) ) 
				{ 
					echo "Remise HT : <strong>"; 
					//si la remise s'applique au frais de port 
					if($_SESSION['coupon_frais_de_port']=='1'){  
						echo $_SESSION['remise_coupon']=$remise=format_to_money($_SESSION['frais_livraison']);} 
					else{ 
						//si la remise s'applique en pourcentage  
						if($_SESSION['type_reduc']=='P'){ 
							echo $_SESSION['remise_coupon']=$remise=format_to_money(($_SESSION['montant_reduc']*$sous_total_total)/100); 
						} 
						//si la remise s'applique avec un montant fixe 
						else{ 
							echo $_SESSION['remise_coupon']=$remise=format_to_money($_SESSION['montant_reduc']); 
						} 
					} 
					echo " &euro;</strong><br />"; 
				} 
			?> 
            TVA 20% :  
			<strong> 
			<span id="tva"> 
			<?php 	  
			if (taux_taxe() == 1.2){ 
				if ((isset ($_SESSION['coupon_frais_de_port'])) && ($_SESSION['coupon_frais_de_port']=='1')){  
					$tva_exacte = $sous_total_total * 0.2; 
					$tva = correction_tva($tva_exacte); 
					$tva = $tva['arrondie']; 
					echo $tva; 
				} else { 
					$tva_exacte = (($sous_total_total-$remise) * 0.2)+($_SESSION['frais_livraison']-($_SESSION['frais_livraison']/1.2));				 
					$tva = correction_tva($tva_exacte); 
					$tva = $tva['arrondie']; 
					echo format_to_money($tva); 
				} 
			} else { 
				echo format_to_money(0); 
			} 
			?> 
			</span> &euro; 
			</strong> 
			<br /> 
            <span class="total"> 
            	<?php  
				if ((isset($_SESSION['coupon_frais_de_port'])) && ($_SESSION['coupon_frais_de_port']=='1')) { 
				   $TOTAL=$sous_total_total+ $tva;  
				} 
				else $TOTAL=$port + $sous_total_total - $remise + $tva; 
				 
				if($TOTAL>0) $Total=$TOTAL; else $Total=0; 
				 
				?> 
            	Total TTC : <strong><span id="total"><?php echo format_to_money($Total); ?></span> &euro;</strong> 
            </span> 
        </div> 
         
        <div class="titre" style="margin-bottom:10px;">Votre code promotionnel</div> 
        <div style="margin-bottom:10px;"> 
            <span style="font-size:14px; font-weight:bold; color:#F00;"><?php  
                 
                if (isset($_SESSION['coupon_affectation_remise'])  && !empty($_SESSION['coupon_affectation_remise'])) { 
                    message_coupon(englob_coupon($_SESSION['code_promo'],format_to_money(sous_total_panier($_SESSION['customer_id'])))); 
                    echo '<br><br>'; 
                } else {	 
                    if (isset($_POST['montant'])) { 
                        $montant=$_POST['montant']; 
                        $code=$_POST['promotion']; 
                        message_coupon(englob_coupon($code,$montant)); 
                        echo '<br><br>'; 
                    } 
            ?> 
			</span> 
			<form method="post" name="coupon" action="panier_paiement.php"> 
				<div style="float:left; margin:5px;">Si vous disposez d'un code promotionnel, veuillez l'inscrire ici :</div> 
				<div style="float:left; margin-left:5px;"><input type="text" id="promo" name="promotion" /></div> 
				<div style="float:left; margin-left:10px;"><input type="hidden" name="montant" value="<?php echo $sous_total_total;?>" /></div> 
				<div style="float:left; margin-left:10px;"><img style="cursor:pointer;" onclick="javascript:document.coupon.submit();" src="template/base/boutons/bouton_code_promo.png" title="appliquer le code promo" alt="appliquer le code promo" /></div> 
				<div class="clear"></div> 
			</form> 
            <?php  
			}  
			?> 
        </div> 
                     
        <div class="titre" style="margin-bottom:10px; margin-top:20px;">Les m&eacute;thodes de paiement</div> 
       	<div>Veuillez selectionner la m&eacute;thode de paiement que vous souhaitez utiliser pour cette commande.</div> 
        <div> 
		 
			<?php 
			$i=0; 
			$moyen_p=tep_db_query("SELECT * FROM ".TABLE_MODULE_PAIEMENT." WHERE paiement_visible='1' AND paiement_montant_max>".$TOTAL." ORDER BY paiement_ordre_affichage"); 
			while($res_paiement=tep_db_fetch_array($moyen_p)) { 
				if ($res_paiement['paiement_id']==4) { 
					$somme_av=tep_db_query("SELECT customers_argent_virtuel FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id'].""); 
					$res_av=tep_db_fetch_array($somme_av); 
				} 
				 
				echo '<div style="';  
				 
				// Si le porte monnaie virtuel est <= 0 && que le mode de paiement est porte monnaie (ID4) on n'affiche pas 
				if (((isset($res_av['customers_argent_virtuel'])) && ($res_av['customers_argent_virtuel'] <=0)) && ($res_paiement['paiement_id']==4)) echo ' display:none; '; 
				// Si le client n'est pas une administration && que le mode de paiement est DEVIS (ID8) on n'affiche pas 
				if ($_SESSION['customers_type']!=4 && ($res_paiement['paiement_id']==8 || $res_paiement['paiement_id']==7)) echo ' display:none; ';		 
				 
					echo 'height:50px; background-image:url(template/base/images_paiement/fond_moyen_de_paiement.gif); background-repeat:no-repeat; margin-top:10px; "> 
						 
					<div style="float:left; width:250px; text-decoration:underline;  height:30px; color:' . COULEUR_29 . '; font-size:12px; margin-left:15px; margin-top:15px; ">'; 
					 
					if ($res_paiement['paiement_id']==8) { 
						echo '<label for="paiement'.$i.'"><strong>Cr�ation d\'un devis</strong></label>'; 
					} else { 
						//echo '<strong>Paiement par '.$res_paiement['paiement_nom'].'</strong>'; 
						echo '<label for="paiement'.$i.'"><strong>Paiement par '.$res_paiement['paiement_nom'].'</strong></label>'; 
					} 
					echo'</div>'; 
					 
					echo '<div style="float:left; margin-top:18px; font-size:11px; color:#788403;">'; 
						if ((isset($res_av['customers_argent_virtuel'])) && ($res_av['customers_argent_virtuel']>0) && ($res_paiement['paiement_id']==4)) { 
							echo '<label for="paiement'.$i.'">Utilisez les <span style="color:#df4e02;">'.format_to_money($res_av['customers_argent_virtuel']).' &euro;</span>'; 
							if (taux_taxe()==1.2) { 
								echo '<span style="font-size:8px;"><strong>('.format_to_money($res_av['customers_argent_virtuel']*1.2).' &euro; TTC)</strong></span>'; 
							} 
							echo ' que vous avez dans votre porte-monnaie virtuel</label>'; 
						} else {  
							echo '<label for="paiement'.$i.'">'. $res_paiement['paiement_description'] . '</label>'; 
						} 
					echo'</div> 
					 
					 
					<div style="float:right; margin-top:15px; margin-right:25px;"> 
						<label class="container"><input '; 
							if((isset($_SESSION['mode_paiement']) && !empty($_SESSION['mode_paiement'])) && $_SESSION['mode_paiement']==$res_paiement['paiement_id']) { 
								echo'checked="checked"'; 
							} else if ($res_paiement['paiement_id']==1) { 
								//echo'checked="checked"'; 
							} 
							  
							echo ' type="radio" id="paiement'.$i.'" name="supri" onclick="paiement('.$res_paiement['paiement_id'].');" /> 
							<span class="checkmark"></span> 
						</label> 
					</div> 
					  
					<div style="float:right; margin-top:5px; margin-right:10px; width:150px;"> 
						<label for="paiement'.$i.'"><img src="template/base/images_paiement/'.$res_paiement['paiement_img'].'" /></label> 
					</div> 
			  </div>'; 
			  $i++; 
			} 
			echo'<input type="hidden" id="compteur" name="compt" value="'.$i.'" />'; 
            ?> 
			</div> 
			 
			<?php if (isset($_SESSION['master']) && ($_SESSION['master']==1)) { ?> 
			<div style="margin-top:10px;"> 
				<strong>Ajouter des commentaires au sujet de la commande</strong><br /> 
				<form method="post" name="commentaire" action="panier_recapitulatif.php"> 
					<textarea name="commentaire_commande" rows="3" style="width:100%;" ><?php if (isset($_SESSION['commentaire_commande'])) {echo $_SESSION['commentaire_commande'];} ?></textarea> 
				</form> 
			</div> 
			<?php } ?> 
				  
			<div style="margin-top:10px; height:50px;"> 
				 
				<div style="float:left; width:700px;"> 
					<input type="checkbox" id="cgv" name="cgv_b"  /> 
					<strong>J'ai lu et j'accepte les <a href="conditions_generales.php" target=_blank">Conditions G�n�rales de Vente</a> et j'accepte que les informations recueillies pour cette commande soient enregistr�es et conserv�es conform��ment � la r�glementation RGPD <a href="conditions_generales.php#article_10" target=_blank">(en savoir plus)</a>. </strong> 
				</div> 
				   
				<div id="continue" style=" float:right;"> 
					<?php if(isset($_SESSION['master']) && ($_SESSION['master']==1)) $mod=1; else $mod=0;?> 
					<a href="#totaux"><img src="template/base/panier/bouton_finir-cmd.png" title="Finaliser ma commande" onclick="continue_procedure('<?php echo $mod; ?>');"  /></a> 
				</div> 
				<div class="clear"></div> 
			</div> 
         
		</div> 
 </div> 
<?php 
	require("includes/footer.php");  
	require("includes/page_bottom.php"); 
	} 
} 
?>