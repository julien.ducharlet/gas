<?php
/* Auteur : Paul 
   Affiche le formulaire de contact du site et s'occupe des traitements d'envoi*/
   
$nom_page = "contact";

require("includes/page_top.php");
require("includes/meta/contact.php");
require("includes/meta_head.php");
require("includes/header.php");

$message = "<span class='info'>Les champs marqu�s d'une * sont obligatoires</span>";
// RegEx permettant de v�rifier la validit� d'un mail
$verif_mail = "!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
$success = false;

if (isset($_POST['message'])) {
	$_POST['message'] = stripslashes($_POST['message']);
	
	if ($_POST['name'] == '' || $_POST['email'] == '' || $_POST['telephone'] == '') {
		$message = "<span class='error'>Vous n'avez pas rempli tous les champs obligatoires !</span>";
	} else if (!preg_match($verif_mail, $_POST['email'])) {
		$message = "<span class='error'>Votre adresse email n'est pas valide !</span>";
	} else if ($_POST['destinataire'] == '') {
		$message = "<span class='error'>Vous n'avez pas pr�cis� le destinataire de votre message !</span>";
	} else if ($_POST['message'] == '') {
		$message = "<span class='error'>Votre message est vide !</span>";
	} else {
		$message = "<span class='success'>Message correctement envoy� � nos services, nous llons treaiter votre demande rapidement !</span>";
		$success = true;
		
		mail_contact($_POST['destinataire'], $_POST['email'], $_POST['num_commande'], $_POST['telephone'], $_POST['sujet'], $_POST['message']);		
	}
}
?>

<div id="corps">
    <div id="contact">
        <h1>Si vous avez une question, n'h�sitez pas � nous contacter !</h1>
        
        <div id="image_contact"><img src="template/base/contact/photo_operatrice.jpg" alt="Contactez-nous" /></div>
		
        <div id="texte_contact">
            <span class="souligne">Nos coordonn�es</span> :<br />
			<br />
            <strong>GENERAL ARMY STORE</strong><br />
			<a href="https://www.google.fr/maps/place/General+Army+Store/@45.1219626,4.9798604,17z/data=!3m1!4b1!4m5!3m4!1s0x47f5492ef25b38b3:0xac5f165d4eda5aa1!8m2!3d45.1219588!4d4.9820491" style="color:#3388cc;text-decoration:none" target="_blank">Entr�e 5 rue H. Bajard<br />
			26260 SAINT DONAT sur L'HERBASSE</a><br />
			<br />
            Service commercial ouvert du lundi au jeudi de 9h00 � 12h30 et de 14h00 � 18h30 et le vendredi de 9h00 � 12h30 et de 14h00 � 17h30 joignable au <span class="stronger">09 53 31 59 47</span><br />
			<br />
			<br />
            <span class="stronger">Avant de poser une question</span>, merci de v�rifier si la r�ponse n'est pas d�j� pr�sente dans notre FAQ<br /><br />
            <a href="faq.php"><img src="template/base/contact/bouton_faq.png" alt="Consulter la FAQ" /></a><br /><br />
            <?php echo TEMPS_REPONSE; ?>
        </div>
        
        <div style="clear:both;"></div>
        
        <div id="formulaire_contact">
        	<?php if ($success) { ?>
            	<div class="message_success">
                	Votre message a �t� envoy� avec succ�s !<br /><br />
                    <a href="index.php"><img src="template/base/contact/bouton_retour-accueil.png" alt="retour Accueil" /></a>
                </div>
            <?php } else { ?>
            <form action="" method="post">
                <div class="libelles">
                    Votre Nom et Pr�nom : <br />
                    Votre Adresse email : <br />
                    Num�ro de T�l�phone : <br />
                    Num�ro de commande : <br />
                </div>
                <div class="coordonnees">
                    <input type="text" name="name" value="<?php $POST_name = isset($_POST['name']) ? $_POST['name'] : ""; echo $POST_name; ?>" size="25" />*<br />
                    <input type="text" name="email" value="<?php $POST_email = isset($_POST['email']) ? $_POST['email'] : ""; echo $POST_email; ?>" size="25" />*<br />
                    <input type="text" name="telephone" value="<?php $POST_telephone = isset($_POST['telephone']) ? $_POST['telephone'] : ""; echo $POST_telephone; ?>" size="25" />*<br />
                    <input type="text" name="num_commande" value="<?php $POST_num_commande = isset($_POST['num_commande']) ? $_POST['num_commande'] : ""; echo $POST_num_commande; ?>" size="25" />&nbsp;<br />
                </div>
                <div class="destinataire">
                    <strong>Envoyer le message � notre : </strong><br /><br />
                    <input type="radio" name="destinataire" id="particuliers" value="particuliers" <?php $POST_destinataire = isset($_POST['destinataire']) ? $_POST['destinataire'] : NULL; if ($POST_destinataire == 'particuliers') echo 'checked="checked"'; ?>/><label for="particuliers">Service Client pour les particuliers</label><br />
                    <input type="radio" name="destinataire" id="professionnels" value="professionnels" <?php if ($POST_destinataire == 'professionnels') echo 'checked="checked"'; ?>/><label for="professionnels">Service Client pour les professionnels</label><br />
                    <input type="radio" name="destinataire" id="achats" value="achats" <?php if ($POST_destinataire == 'achats') echo 'checked="checked"'; ?>/><label for="achats">Vous �tes un grossiste / Revendeur</label>
                </div>
                <div class="message">
                    <select name="sujet">
                        <option value="J'ai une question sur un article" <?php $POST_sujet = isset($_POST['sujet']) ? $_POST['sujet'] : NULL; if ($POST_sujet == "J'ai une question sur un article") echo 'selected="selected"'; ?>>J'ai une question sur un article</option>
                        <option value="Je n'arrive pas � m'inscrire" <?php if ($POST_sujet == "Je n'arrive pas � m'inscrire") echo 'selected="selected"'; ?>>Je n'arrive pas � m'inscrire</option>
                        <option value="Je n'arrive pas � me connecter" <?php if ($POST_sujet == "Je n'arrive pas � me connecter") echo 'selected="selected"'; ?>>Je n'arrive pas � me connecter</option>
                        <option value="J'ai un probl�me sur une Commande" <?php if ($POST_sujet == "J'ai un probl�me sur une Commande") echo 'selected="selected"'; ?>>J'ai un probl�me sur une Commande</option>
                        <option value="J'ai un probl�me de livraison" <?php if ($POST_sujet == "J'ai un probl�me de livraison") echo 'selected="selected"'; ?>>J'ai un probl�me de livraison</option>
                        <option value="Je souhaite retourner un article" <?php if ($POST_sujet == "Je souhaite retourner un article") echo 'selected="selected"'; ?>>Je souhaite retourner un article</option>
                        <option value="Je souhaite proposer des articles" <?php if ($POST_sujet == "Je souhaite proposer des articles") echo 'selected="selected"'; ?>>Je souhaite proposer des articles</option>
                        <option value="Autre..." <?php if ($POST_sujet == "Autre...") echo 'selected="selected"'; ?>>Autre...</option>
                    </select><br />
                    <textarea name="message" rows="4" cols="35"><?php $POST_message = isset($_POST['message']) ? $_POST['message'] : ""; echo $POST_message; ?></textarea>
                </div>
                
                <div style="clear:both;"></div>
                
                <div class="valider"><p><?php echo $message; ?></p><input type="image" src="template/base/contact/bouton_envoyer.jpg" alt="Envoyer" /></div>
            </form>
            <?php 
			} 
			?>
        </div>		
        <br>        
		<?php // AVIS VERIFIES #7C7972 ?>
		<div style="margin:5px; padding:10px; background-color:#ffffff; border-top:1px dashed grey;">		
			<iframe id='AV_widget_iframe' frameBorder="0" width="100%" height="100%" src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2ahorizontal_index.html"></iframe>
		</div>
		<?php // End AVIS VERIFIES ?>
		
    </div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>