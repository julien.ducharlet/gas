<?php
/*Affiche les infos de l'article s�lectionn�*/
$nom_page = "produits_fiches";

require("includes/page_top.php");

//date du jour 
$today = date("Y-m-d H:i:s");
// initialisation de variable
$PACK='';

switch($_SESSION['customers_type']) {
	
	case '1' :
		$query_custom_type = ' and categories_status_client=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
		break;
	
	case '2' :
		$query_custom_type = ' and categories_status_pro=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_pro=\'1\'';
		break;
	
	case '3' :
		$query_custom_type = ' and categories_status_rev=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_rev=\'1\'';
		break;
	
	case '4' :
		$query_custom_type = ' and categories_status_adm=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_adm=\'1\'';
		break;
	
	default :
		$query_custom_type = ' and categories_status_client=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
		break;
}

//verifie URL
$query_url="SELECT cd.categories_id as id_modele, cd.categories_name as nom_modele, cd.categories_url as url_modele, rub.rubrique_name, rub.rubrique_url, cd2.categories_name as nom_marque, 
				   cd2.categories_url as url_marque, pd.products_name as nom_produit, pd.products_baseline, pd.products_url as url_article,
				   pd.products_head_title_tag, pd.products_head_desc_tag, pd.products_head_keywords_tag
            FROM " . TABLE_CATEGORIES . " as c
			inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
			inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
			inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
			inner join " . TABLE_RAYON . " as rub on (rub.rubrique_id=cr.rubrique_id ". $query_rubrique_custom_type .")
			inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " as pc on c.categories_id=pc.categories_id
			inner join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on pd.products_id=pc.products_id
			inner join " . TABLE_PRODUCTS . " as p on p.products_id=pd.products_id
			
            WHERE c.parent_id='". (int)$_REQUEST['marque'] ."'
			AND cd.categories_id='". (int)$_REQUEST['modele'] ."'
			AND cr.rubrique_id='". (int)$_REQUEST['rayon'] ."'
			AND pd.products_id='". (int)$_REQUEST['produit'] ."'
			AND c.categories_status='1'
			AND products_status='1'". $query_custom_type;

$res_url=tep_db_query($query_url);
if(tep_db_num_rows($res_url)!=1){
	$query_url_modele="	SELECT cd.categories_baseline, cd.categories_name as nom_modele, cd.categories_url as url_modele, rub.rubrique_name, rub.rubrique_url, cd2.categories_name as nom_marque, 
					   	cd2.categories_url as url_marque, cd.categories_htc_title_tag, cd.categories_htc_desc_tag, cd.categories_htc_keywords_tag
						FROM " . TABLE_CATEGORIES . " as c 	inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
												   			inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
												   			inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
												   			inner join " . TABLE_RAYON . " as rub on (rub.rubrique_id=cr.rubrique_id ". $query_rubrique_custom_type .")
						WHERE c.parent_id='". (int)$_REQUEST['marque'] ."' AND cd.categories_id='". (int)$_REQUEST['modele'] ."' AND cr.rubrique_id='". (int)$_REQUEST['rayon'] ."'";
	
	$res_url_modele=tep_db_query($query_url_modele);
	if(tep_db_num_rows($res_url_modele)!=1){
		redirect(404,url('erreur',array()));
	}
	else{
		$row_url_modele = tep_db_fetch_array($res_url_modele);
		$url_clean_modele = url("modele",array(
											   'id_rayon' => (int)$_REQUEST["rayon"], 
											   'nom_rayon' => $row_url_modele["rubrique_url"], 
											   'id_marque' => (int)$_REQUEST["marque"], 
											   'url_marque' => $row_url_modele["url_marque"], 
											   'id_modele' => (int)$_REQUEST["modele"], 
											   'url_modele' => $row_url_modele["url_modele"]));
		redirect(301,$url_clean_modele);
	}/**/
}
else{
	$row_url = tep_db_fetch_array($res_url);
	$baseline=$row_url["products_baseline"];
	$url_clean = url("article",array(
									 'id_rayon' => (int)$_REQUEST["rayon"], 
									 'nom_rayon' => $row_url["rubrique_url"], 
									 'id_marque' => (int)$_REQUEST["marque"], 
									 'url_marque' => $row_url["url_marque"], 
									 'id_modele' => (int)$_REQUEST["modele"], 
									 'url_modele' => $row_url["url_modele"], 
									 'id_article' => (int)$_REQUEST["produit"], 
									 'url_article' => bda_product_name_transform($row_url["url_article"],$row_url["id_modele"])));
	if($_SERVER['REQUEST_URI']!=$url_clean){
		redirect(301,$url_clean);
	}

	require("includes/meta/produits_fiches.php");
	require("includes/meta_head.php");
	require("includes/header.php");
}?>
<? // Utilis� pour le zoom sur les images ?>
<script src="<?php echo BASE_DIR; ?>/includes/js/mz-packed.js" type="text/javascript"></script>
<? // utile uniquement pour les ventes flash ?>
<script src="<?php echo BASE_DIR; ?>/includes/js/produits_liste.js" type="text/javascript"></script>

<script type="text/javascript" language="javascript">
	// Affichage des onglets de la fiche des articles
	function affiche_onglet(id) {
		arrLinkId = new Array('_0','_1','_2','_3','_4','_5','_6');
		intNbLink = new Number(arrLinkId.length);
		arrClassLink = new Array('current','ghost');
		strContent = new String()
		for (i=0; i<intNbLink; i++) {
			if (document.getElementById(arrLinkId[i]) != null) {
				strContent = "onglet"+arrLinkId[i];
				if ( arrLinkId[i] == id ) {
					document.getElementById(arrLinkId[i]).className = arrClassLink[0];
					document.getElementById(strContent).className = 'on content';
				} else {
					document.getElementById(arrLinkId[i]).className = arrClassLink[1];
					document.getElementById(strContent).className = 'off content';
				}
			}
		}	
	}
</script>

<div id="corps">
	<input type="hidden" id="base_dir" value="<?php echo BASE_DIR . "/"; ?>" />
    <div id="fiche_articles">
	<div itemscope itemtype="https://schema.org/Product" >
	<?php
        include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_fil_ariane.php");
        
        $id_produit = (int)$_GET['produit'];
        
        $produit_query = tep_db_query("SELECT pd.products_name, pd.products_url, pd.products_description, 
									  p.products_model, p.products_price, p.products_bimage, p.products_quantity, products_video_1, products_video_1_largeur, products_video_1_hauteur, products_video_2, products_video_2_largeur, products_video_2_hauteur, products_vue_360, p.products_ecotaxe, p.products_video, p.products_video_preview, p.products_ref_origine, p.products_garantie, p.products_code_barre, p.products_status, p.products_date_available, p.products_manage_stock, p.products_note,
									  m.manufacturers_name, m.manufacturers_id, m.manufacturers_image, m.manufacturers_url, 
									  ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1,
									  ppbq.part_price_by_5, ppbq.pro_price_by_5, ppbq.rev_price_by_5, ppbq.adm_price_by_5,
									  ppbq.part_price_by_10, ppbq.pro_price_by_10, ppbq.rev_price_by_10, ppbq.adm_price_by_10,
									  s.specials_new_products_price,
									  debut_vente_flash, fin_vente_flash, pvf.prix_vente
                                       FROM " . TABLE_PRODUCTS_DESCRIPTION . " as pd 
									   inner join " . TABLE_PRODUCTS . " as p on pd.products_id = p.products_id
									   left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id
									   left join ". TABLE_SPECIALS ." as s on (p.products_id=s.products_id and (expires_date>NOW() or expires_date='0000-00-00 00:00:00'))
									   left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash='0000-00-00')))
									   left join " . TABLE_MANUFACTURERS . " as m on p.manufacturers_id = m.manufacturers_id
                                       WHERE pd.products_id = " . $id_produit);
		
        $produit_data = tep_db_fetch_array($produit_query);
        
        $video_article = $produit_data['products_video'];
        $apercu_video_article = $produit_data['products_video_preview'];
        
        $nom_produit = $produit_data['products_name'];
        $nom_produit = bda_product_name_transform($nom_produit, $id_modele);
		
        $url_produit = $produit_data['products_url'];
        $url_produit = bda_product_name_transform($url_produit, $id_modele);
        
        $nom_fabriquant = $produit_data['manufacturers_name'];
        $nom_fabriquant = preg_replace('/Accessoires /', '', $nom_fabriquant);
        $nom_fabriquant = preg_replace('/ Origine/', '', $nom_fabriquant);
        
        $id_fabriquant = $produit_data['manufacturers_id'];
        
        $description_produit = $produit_data['products_description'];
        $description_produit = bda_product_name_transform($description_produit, $id_modele);
		
		$disponibilite = $produit_data['products_date_available'];
		
		$sipromo = $produit_data['specials_new_products_price'];
		
		$garantie_article = $produit_data['products_garantie'];
		$reference_origine_article = $produit_data['products_ref_origine'];
		$code_barre_article = $produit_data['products_code_barre'];
        
        $produit_image_query = tep_db_query("SELECT products_bimage
                                             FROM " . TABLE_PRODUCTS_IMAGE . "
                                             WHERE products_id = " . $id_produit . " AND products_bimage <> ''
                                             order by products_bimage
											 LIMIT 0,4");
    ?>
        <?
        // AFFICHE LA DATE DE REAPPROVISONNEMENT D'UN ARTICLE INDISPONIBLE !
		if ($disponibilite > date('Y-m-d H:i:s')) {
			echo '<div id="disponibilite">';
			echo 'L\'article sera disponible � partir du ' . dat($disponibilite) . '<br /><br />';
			echo '</div>';
		}
		$prix = calculate_price_for_product($produit_data);
		?>		
		
		<h1 id="nom_produit" ><?php echo $nom_produit; ?></h1>
		
		<div style="display:none;">
		
			<span style="display:none;" itemprop="productID"><?php echo $id_produit; ?></span>
			<span style="display:none;" itemprop="sku"><?php echo $id_produit; ?></span>
			<span style="display:none;" itemprop="name"><?php echo $nom_produit; ?></span>
			<span style="display:none;" itemprop="image">https://www.generalarmystore.fr/gas/images/products_pictures/normale/<?php echo $produit_data['products_bimage']; ?></span>			
			<div style="display:none;" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
			
				<? if($prix['promo'] > 0) { ?>
				<span style="display:none;" itemprop="price"><?php echo $prix['promo']; ?></span>
				<? } else { ?>
				<span style="display:none;" itemprop="price"><?php echo $prix['normal']; ?></span>
				<? } ?>
				<span style="display:none;" itemprop="priceCurrency">EUR</span>
				<span style="display:none;"><link itemprop="availability" href="https://schema.org/InStock" />in_stock</span>
				
				<span style="display:none;" itemprop="priceValidUntil">2050-12-31</span>
				<span style="display:none;" itemprop="seller">General Army Store</span>
				<span style="display:none;" itemprop="url">https://www.generalarmystore.fr<?php echo $url_clean; ?></span>
				</span>
			</div>
			<?php 
			// AVIS VERIFIES Note globale du produit 
			$fichier_avis_verifies_note_globale = 'http://cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/AWS/PRODUCT_API/AVERAGE/' . $id_produit . '.xml';
			$file = @fopen($fichier_avis_verifies_note_globale, 'r'); 
			if ($file) {
				$xmlgeneral = simplexml_load_file($fichier_avis_verifies_note_globale); 
				foreach($xmlgeneral as $avisgeneral){	
					// G�n�re les informations global des avis des produits pour google
					echo '<div itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">';
					echo '<meta itemprop="ratingValue" content="' . $avisgeneral->rate . '" />';
					echo '<meta itemprop="bestRating" content="5" />';
					echo '<meta itemprop="reviewCount" content="' . $avisgeneral->count . '" />';
					echo '</div>';
				}
				fclose($file);
			} 
			// END AVIS VERIFIES Note globale du produit 	
			?>
			
			<span style="display:none;" itemprop="brand"><?php echo $produit_data['manufacturers_name']; ?></span>
			<span style="display:none;" itemprop="gtin13"><?php echo $code_barre_article; ?></span>
			<span style="display:none;" itemprop="url">https://www.generalarmystore.fr<?php echo $url_clean; ?></span>
		</div>
		
		
        <div id="image_article">
            <!-- IMAGE DE BASE -->
            <a href="<?php echo BASE_DIR; ?>/images/products_pictures/grande/<?php echo $produit_data['products_bimage']; ?>" class="MagicZoom" ondblclick="window.open('<?php echo BASE_DIR; ?>/images/products_pictures/normale/<?php echo $produit_data['products_bimage']; ?>')" id="zoom1" rel="zoom-width:350px; zoom-height:350px; opacity:30; thumb-change:mouseover;"><img src="<?php echo BASE_DIR; ?>/images/products_pictures/normale/<?php echo $produit_data['products_bimage']; ?>" alt="<?php echo strip_tags($nom_produit);?> 0" width="350" height="350"/></a>
            
            <!-- ajout du chargement de l'animation Magic Zoom -->
            <img class="MagicZoomLoading" src="<?php echo BASE_DIR; ?>/images/products_pictures/ajax-loader.gif" alt="Chargement en cours, merci de patienter"/>
            
            <div class="miniatures">
                <a href="<?php echo BASE_DIR; ?>/images/products_pictures/grande/<?php echo $produit_data['products_bimage']; ?>" rel="zoom1" rev="<?php echo BASE_DIR; ?>/images/products_pictures/normale/<?php echo $produit_data['products_bimage']; ?>"><img src="<?php echo BASE_DIR; ?>/images/products_pictures/micro/<?php echo $produit_data['products_bimage']; ?>" alt="<?php echo strip_tags($nom_produit);?> 0" width="53" height="53"/></a>
                <?php
				$i=0;		
                while ($produit_image_data = tep_db_fetch_array($produit_image_query)) {
                $i++
				?>
                    <a href="<?php echo BASE_DIR .'/images/products_pictures/grande/'. $produit_image_data['products_bimage'] .'" rel="zoom1" rev="'. BASE_DIR .'/images/products_pictures/normale/'. $produit_image_data['products_bimage'] .'"><img src="'. BASE_DIR .'/images/products_pictures/micro/'. $produit_image_data['products_bimage'] .'" alt="'. strip_tags($nom_produit) . ' ' . $i .'" width="53" height="53"/></a>';
				} ?>
				
				<?php if (!empty($produit_data['products_vue_360'])) { ?>
                <a href="#details_article" onclick="affiche_onglet('_6')"><img id="image_video" src="<?php echo BASE_DIR; ?>/template/base/boutons/bouton_360.png" alt="Vid�o" /></a>
                <?php } ?>                
            </div>
        </div>
        
        <div id="description_article">
        	<!-- Affichage du logo de la marque -->
            <div class="marque">
				<a href="<?php echo BASE_DIR; ?>/marques.php?marque=<?php echo $produit_data['manufacturers_id']; ?>">
                	<img src="<?php echo BASE_DIR; ?>/images/marques_pictures/<?php echo $produit_data['manufacturers_image']; ?>" alt="Logo <?php echo $nom_fabriquant; ?>" />
				</a>
            </div>
            <a href="#details_article" onclick="affiche_onglet('_0')"><img id="lien_description" src="<?php echo BASE_DIR; ?>/template/base/fiche_article/bouton_description_article.jpg" alt="Description" /></a>
			
			<?php if ($_SESSION['customers_type']==3 || (GESTION_ONGLET_PRIX_PAR_QUANTITE == 1 && ($prix['promo']==0 && $prix['flash']==0))) { ?>
			<a href="#details_article" onclick="affiche_onglet('_5')"><img id="lien_description" src="<?php echo BASE_DIR; ?>/template/base/fiche_article/bouton_remise_quantite.jpg" alt="Quantit�" /></a>
			<?php } ?>
			           
			 <a id="question_button" class="pointer" style="text-decoration:underline;"><img id="lien_description" src="<?php echo BASE_DIR; ?>/template/base/fiche_article/bouton_poser_une_question.jpg" alt="Poser une question" /></a>	 
			
            <?php
			/*
				products_manage_stock les diff�rentes possibilit�s
					1 - tout				- Accepter les commandes (Vente hors stock)
					2 - sur_cmd				- Accepter les commandes (Sur commande uniquement)
					3 - destock_visible		- Refuser les commandes (Destockage)
					4 - destock_cache		- Refuser les commandes (Destockage Invisible)
					5 - destock_cache_qte	- Refuser les commandes (Qt�s visibles)
				
				Si un des produit du pack (products_id - products_id_1 - products_id_2)
				est 
				products_manage_stock = destock_visible 
				ou
				products_manage_stock = destock_cache 
				ou
				products_manage_stock = destock_cache_qte 
				et 
				products_quantity <=0	
				
			*/
			
			$pack_requete=" SELECT 	
								p.products_id, 
								p.products_quantity,
								p.products_manage_stock,
								pack.pack_id, 
								pack.pack_remise, 
								pack.products_id , 
								pack.products_id_1, 
								pack.products_id_2, 
								pack.pack_products_quantity
							FROM
								products_packs pack,
								products p								
							WHERE  
								pack.products_id='".$id_produit."'
								AND
								pack.pack_status = 1  
								AND
								pack.products_id=p.products_id	
								AND
								((p.products_manage_stock IN ('destock_visible', 'destock_cache', 'destock_cache_qte') AND (p.products_quantity >=1)) OR (p.products_manage_stock IN ('tout', 'sur_cmd')))
								AND
								(
									(
									pack.pack_products_quantity=2 AND pack.products_id_1 = (SELECT p1.products_id FROM products p1 WHERE p1.products_id = pack.products_id_1 AND ((p1.products_manage_stock IN ('destock_visible', 'destock_cache', 'destock_cache_qte') AND (p1.products_quantity >=1)) OR (p1.products_manage_stock IN ('tout', 'sur_cmd'))))
									)
									OR
									(
									pack.pack_products_quantity=3 AND pack.products_id_1 = (SELECT p1.products_id FROM products p1 WHERE p1.products_id = pack.products_id_1 AND ((p1.products_manage_stock IN ('destock_visible', 'destock_cache', 'destock_cache_qte') AND (p1.products_quantity >=1)) OR (p1.products_manage_stock IN ('tout', 'sur_cmd')))) 
									AND 			
									(pack.products_id_2 = (SELECT p2.products_id FROM products p2 WHERE p2.products_id = pack.products_id_2 AND ((p2.products_manage_stock IN ('destock_visible', 'destock_cache', 'destock_cache_qte') AND (p2.products_quantity >=1)) OR (p2.products_manage_stock IN ('tout', 'sur_cmd')))))
									)
								)
								
								
							ORDER BY 
								pack.pack_id";			
			
			$packs_query = tep_db_query($pack_requete);					
			
			// on controle le nombre de pack soit >0 
            if ( (tep_db_num_rows($packs_query)>0) && (empty($sipromo)) && ($_SESSION['customers_type']==1 OR $_SESSION['customers_type']==2) ) {
			?>
                <a href="#details_article" onclick="affiche_onglet('_2')"><img id="lien_pack" src="<?php echo BASE_DIR; ?>/template/base/fiche_article/bouton_les-packs.jpg" alt="Packs" /></a>
            <?php } 
			// Bouton vid�o
			if (!empty($video_article) && !empty($apercu_video_article)) { ?>
                <a href="#details_article" onclick="affiche_onglet('_1')"><img id="lien_video" src="<?php echo BASE_DIR; ?>/template/base/fiche_article/bouton_video.jpg" alt="Vid�o" /></a>
            <?php } 
		
			
			// AVIS VERIFIES Note globale du produit 
			$fichier_avis_verifies_note_globale = 'http://cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/AWS/PRODUCT_API/AVERAGE/' . $id_produit . '.xml';
			$file = @fopen($fichier_avis_verifies_note_globale, 'r'); 
			if ($file) {			
				$xmlgeneral = simplexml_load_file($fichier_avis_verifies_note_globale); 
				echo '<div style="text-align:center; padding-top:10px;">';
				foreach($xmlgeneral as $avisgeneral){
					echo '<a href="#details_article" onclick="affiche_onglet(\'_3\')" title="Lire les avis des clients" style="text-decoration:none; color: #0188C6;">';
					
					// Fonction d'affichage des notes en im�age avec demi note
					//echo '<span itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">';
					echo avis_verifies_complet($avisgeneral->rate); 
					//echo '<span itemprop="ratingValue" style="display:none;">'. $avisgeneral->rate .'</span>';
					//echo '<span itemprop="bestRating" style="display:none;">5</span>';
					echo '<br>Il y a ' . $avisgeneral->count . ' avis sur ce produit';
					echo '</span>';
					echo '</a>';					
				}			
				echo '</div>';
				fclose($file);
			} 
			
			// END AVIS VERIFIES Note globale du produit 		
			?>			
        </div>
        
        <div id="actions_article">
        	<!-- Affichage du prix de l'article -->
            <div class="prix">
                <?php 
				if ($prix['flash'] > 0) {
					$date_debut = convert_date_vente_flash($produit_data['debut_vente_flash']);
					$date_fin = convert_date_vente_flash($produit_data['fin_vente_flash']);
					$decompte = temps_restant($produit_data['fin_vente_flash']);
					?>
					<img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/image_vente-flash.png" alt="Vente Flash" /><br>
					<br>
					<?php
					echo '<span id="compteur">'. show_temps_restant($decompte['jour'], $decompte['heure'], $decompte['min'], $decompte['sec'], 'short') .'</span><br /><br />';
					echo "<span style='font-size:15px; font-weight: bold;'>". round((1-($prix['flash']/$prix['normal']))*100,0) ."% d'�conomie</span><br /><br />";
					?>
					<script type="text/javascript">
						var timer = setInterval("decompte('compteur')", 1000);
					</script>
				<?php
				} elseif ($produit_data['products_manage_stock'] == 'destock_visible') {
				?>
					<img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/image_destockage.png" alt="Destockage" /><br /><br />
					<?php
					if ($produit_data['products_quantity'] > 0) {
						echo '<span style="font-size:15px; font-weight: bold;">Il en reste <span id="qte_product">'. $produit_data['products_quantity'] .'</span> en stock</span><br /><br />';
					} else {
						echo "<span style='font-size:15px; font-weight: bold;'>Article &eacute;puis&eacute;</span><br /><br />";
					}
				
				// Ajout Thierry 22/05/2018 pour nouveau statut produit 5 - Refuser les commandes (Destockage Invisible avec qt�)
				} elseif ($produit_data['products_manage_stock'] == 'destock_cache_qte') {
					  	  
					if ($prix['promo'] > 0) { ?>
						<img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/image_promotion.png" alt="Promotion" /><br /><br />
						<?php
						echo "<span style='font-size:15px; font-weight: bold;'>".round((1-($prix['promo']/$prix['normal']))*100,0)."% d'�conomie</span><br /><br />";
					}
						  
					if ($produit_data['products_quantity'] > 0) {
						echo '<span style="font-size:15px; font-weight: bold;">Il y a <span id="qte_product">'. $produit_data['products_quantity'] .'</span> pi�ce(s) en stock</span><br /><br />';
					} else {
						echo "<span style='font-size:15px; font-weight: bold;'>Article &eacute;puis&eacute;</span><br /><br />";
					}
					  
				} elseif($prix['promo'] > 0) { ?>
						
					<img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/image_promotion.png" alt="Promotion" /><br /><br />
					<?php
					echo "<span style='font-size:15px; font-weight: bold;'>".round((1-($prix['promo']/$prix['normal']))*100,0)."% d'�conomie</span><br /><br />";
				}
					  
				if ($prix['type'] == 'HT') { 
					echo '<span style="font-size: 1.2em; color: ' . COULEUR_30 . '; font-weight: bold;">Prix HT</span><br /><br />';
				} else { 
					echo '<span style="font-size: 1.2em; color: #0188C6; font-weight: bold;">Prix TTC</span><br /><br />';
				}
				if($prix['flash'] > 0){						  
					prix2img((string)format_to_money($prix['flash']), '42');?><br /><?php
					echo "<strong>Au lieu de : </strong><span style='text-decoration:line-through; font-size:15px;'>".format_to_money($prix['normal'])." Euros</span><br />";
	
				} elseif ($prix['promo'] > 0) {
						  
					prix2img((string)format_to_money($prix['promo']), '42');?><br /><?php
					echo "<strong>Au lieu de : </strong><span style='text-decoration:line-through; font-size:15px;'>".format_to_money($prix['normal'])." Euros</span><br />";
						 
				} else {
					if (format_to_money($prix['normal']) == 0) { 
						echo '<a id="question_button_prix" class="pointer" style="text-decoration:underline;"><img src="https://www.generalarmystore.fr/gas/template/base/fiche_article/bouton_sur_demande.png" alt="Poser une question"></a>';												
					} else {
						prix2img((string)format_to_money($prix['normal']), '42') .'<br />';
					}		  
				}
				?>
			</div>
			<!-- Affichage (ou pas) de l'ecotaxe -->
			<div class="ecotaxe">
                <?php                    
				$ecotaxe = format_to_money($produit_data['products_ecotaxe']);
				if ($ecotaxe != '0.00') {
					echo '<img src="' . BASE_DIR . '/images/ecotaxe.png" alt=" " /><p>Le prix comprend l\'Ecotaxe de ' . $ecotaxe . '�</p>';
				}
				?>
			</div>
			           
			<?php
				if ($produit_data['products_quantity']>0) {
					$show_basket = true;
				} elseif ($produit_data['products_manage_stock']=='destock_visible' || $produit_data['products_manage_stock']=='destock_cache' || $produit_data['products_manage_stock']=='destock_cache_qte') {
					$show_basket = false;
				} else {
					$show_basket = true;
				}
				
				if ($show_basket) {
					?>
					<div class="quantite">
                    	<?php	
						$query_count_opt_hidden = '	SELECT count(*) as total
													FROM '. TABLE_PRODUCTS_ATTRIBUTES .'
													WHERE products_id = \''. $id_produit .'\'
								 
													HAVING total= (SELECT count(*)
													FROM '. TABLE_PRODUCTS_ATTRIBUTES .'
													WHERE products_id = \''. $id_produit .'\'
													AND options_dispo = \'0\')';
						$query_count_opt_hidden = tep_db_query($query_count_opt_hidden);
						
						$data_count_opt = tep_db_fetch_array($query_count_opt_hidden);
						
						if ($data_count_opt['total']>0) {
							echo '<div style="margin-top:40px;padding 0 5px;">Cet article est momentan�ment indisponible et est en cours de r�approvisionnement</div>';
						} else {
							
							?>
							<div class="qtte_texte">
								<div style="float:left; padding-left:14px;">Quantit� :</div>
							<div>
							
							<input type="image" src="<?php echo BASE_DIR; ?>/template/base/fiche_article/plus.jpg" value="+" onclick="plus('quantite_fiche_<?php echo $id_produit . "_" . $id_marque . "_" . $id_modele; ?>','<?php echo (get_destock($produit_data['products_manage_stock'])) ? $produit_data['products_quantity'] : 0;?>');" />
							
							<input id="quantite_fiche_<?php echo $id_produit . "_" . $id_marque . "_" . $id_modele; ?>" type="text" value="1" size="1" style="text-align: center; background-color: #FFFFFF;" disabled="disabled"/>
							
							<input type="image" src="<?php echo BASE_DIR; ?>/template/base/fiche_article/moins.jpg" value="-" onclick="moins('quantite_fiche_<?php echo $id_produit . "_" . $id_marque . "_" . $id_modele; ?>');" /></div>
							</div>
							<!-- Affichage du bouton d'ajout au panier -->
							<?php							
							echo '<img class="pointer add_product_to_basket" src="' . BASE_DIR . '/template/base/fiche_article/icone_panier.png" alt="Ajouter au panier"/></a>';
							
							if ($produit_data['products_manage_stock']=='sur_cmd') {
								echo '<div style="margin-top:40px; color:red; font-weight:bold; ">Article vendu uniquement sur commande.<br>D�lai de 1 � 4 semaines.</div>';
							}
						}
						?>
					</div> 
					<?php
				  }
				  else {
					  echo '<br>Cet article �tant actuellement en rupture chez tous nos fournisseurs, il est impossible de le commander pour le moment.<br><br>Nous en sommes d�sol�s et vous invitons � revenir ult�rieurement v�rifier sa disponibilit�.';
				  }
				  ?>
		<br />
		</div>
			
		<?
        // AFFICHE LA DATE DE REAPPROVISONNEMENT D'UN ARTICLE INDISPONIBLE !
		if ($disponibilite > date('Y-m-d H:i:s') && $prix['promo']==0) {
			echo '<div align="center">';
			echo 'Disponible le ' . dat($disponibilite) . '<br />dans nos entrepots *';
			echo '</div>';
		}
		?>
        
        <div style="clear:both;"></div>
        
        <div id="message_pro">
        	<?php
           
			$i = 0;
			$query = 'select * from '. TABLE_PRODUCTS_LOGOS .' pl, '. TABLE_PRODUCTS_TO_LOGOS .' ptl where pl.logo_id=ptl.logo_id and products_id='. $id_produit .' order by pl.logo_name limit 0,6';
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				?>
                <div style="float:left; margin-right:5px; margin-top:-7px;" >
                	<a href="<?php echo BASE_DIR .'/produits_logos_details.php#logo_'. $data['logo_id']; ?>"><img id="logo_<?php echo $data['logo_id']; ?>" 
                    src="<?php echo BASE_DIR .'/images/type_products_pictures/'. $data['logo_name']; ?>" 
					alt="<?php echo $data['logo_name']; ?>" width="53" height="53" />
					</a>
               	</div>
                <?php
			}
			?>
        </div>
        
        <div style="clear:both;"></div>
        
        <!-- Affichage des onglets et de leur contenu -->
        <div id="details_article">
            <ul id="onglets">
                <li class="onglet0">
                    <a href="#details_article" id="_0" class="current" onclick="affiche_onglet(this.id)"><img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/onglet_description.gif" alt="Description" /></a>
                </li>
                <li class="onglet1">
                	<!-- Si une vid�o est disponible on affiche l'onglet vid�o -->
                    <?php if (!empty($produit_data['products_video_1'])) { ?>
                    <a href="#details_article" id="_1" class="ghost" onclick="affiche_onglet(this.id)"><img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/onglet_video.gif" alt="Vid�os" /></a>
                    <?php } ?>
                </li>
				<li class="onglet6">
                	<!-- Si une vue � 360� est disponible on affiche l'onglet -->
                    <?php if (!empty($produit_data['products_vue_360']))  { ?>
                    <a href="#details_article" id="_6" class="ghost" onclick="affiche_onglet(this.id)"><img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/onglet_vue-360.gif" alt="Vues � 360�" /></a>
                    <?php } ?>
                </li>
                <li class="onglet2">
                	
                    <?php									
					// on controle le nombre de pack soit >0 
                    if ( (tep_db_num_rows($packs_query)>0) && (empty($sipromo)) && ($_SESSION['customers_type']==1 OR $_SESSION['customers_type']==2) ) {
					?>
                    <a href="#details_article" id="_2" class="ghost" onclick="affiche_onglet(this.id)"><img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/onglet_pack.gif" alt="Les Packs" /></a>
                    <?php } ?>
                </li>
                <li class="onglet3">
                    <a href="#details_article" id="_3" class="ghost" onclick="affiche_onglet(this.id)"><div class="avis">&nbsp;</div></a>
                </li>
                <li class="onglet4">
                	<!-- Si c'est un professionel on affiche l'onglet des compatibilit�s -->
                    <?php // Thierry modification du 19/04/2018 pour controler que la variable ne soit pas vide ?>
					<?php if (!empty($_SESSION['customers_compatibilite'])  &&  ($_SESSION['customers_compatibilite'] == 1)) { ?>
                        <a href="#details_article" id="_4" class="ghost" onclick="affiche_onglet(this.id)"><img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/onglet_compatibilites.gif" alt="Compatibilit�" /></a>
                    <?php } ?>
                </li>
                <li class="onglet5">
                	<?php if ($_SESSION['customers_type']==3 || (GESTION_ONGLET_PRIX_PAR_QUANTITE == 1 && ($prix['promo']==0 && $prix['flash']==0))) { ?>
                        <a href="#details_article" id="_5" class="ghost" onclick="affiche_onglet(this.id)"><img src="<?php echo BASE_DIR; ?>/template/base/fiche_article/onglet_quantite.gif" alt="Prix par quantite" /></a>
					<?php } ?>
                </li>
            </ul>
            
            <div id="onglet_0" class="on content">
            	<?php 
					$infos_options_query = tep_db_query("SELECT pov.products_options_values_name, po.products_options_name, pa.options_id, pa.options_values_id , options_quantity
														 FROM " . TABLE_PRODUCTS_ATTRIBUTES . " pa, 
														 " . TABLE_PRODUCTS_OPTIONS . " po, 
														 " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov 
														 WHERE pa.products_id = '" . $id_produit . "' AND pa.options_values_id = pov.products_options_values_id AND pa.options_id = po.products_options_id AND pa.options_dispo = '1' 
														 ORDER BY pa.products_options_sort_order");
				?> 
	            <h2><?php echo $nom_produit; ?></h2>
                
				<?php 
				$products_options = array();
				if (tep_db_num_rows($infos_options_query) != 0) { ?>
                    <div style="width: 24%; float: right;">
                        <?php 
							
							$i = 0;
							$products_options = array();
                            $premier = true;
							
                            while ($infos_options = tep_db_fetch_array($infos_options_query)) {
								
                                if ($premier) {
									
									$products_options['options_name'] = $infos_options['products_options_name'];

									
                                    echo "<h2 style='font-size: 1.1em;'>" . $infos_options['products_options_name'] . "</h2><p>";
                                    $premier = false;
                                }
								
								$products_options['options_values_name'][$i] = $infos_options['products_options_values_name'];
								$products_options['options_values_id'][$i] = $infos_options['options_values_id'];
								$products_options['options_quantity'][$i] = $infos_options['options_quantity'];
								$products_options['options_id'][$i++] = $infos_options['options_id'];
								
                                echo "&nbsp;&nbsp;-&nbsp;&nbsp;" . $infos_options['products_options_values_name'] . "<br />";
                            }
                            echo "<br /><br /><a href=\"https://www.generalarmystore.fr/gas/tableau_des_tailles.php\" target=\"blank\" style=\"color:#0188C6;\"> Tableau des tailles</a></p>";
                        ?>
                    </div>
				<?php } ?>
                
            	<div <?php if (tep_db_num_rows($infos_options_query) != 0) { echo 'style="width: 72%; float: left; padding-right: 20px; border-right: 1px solid black;"'; } ?>>
                    <?php if (!empty($reference_origine_article)) { ?>
                    <p>R�f�rence d'origine : <?php echo $reference_origine_article; ?></p>
                    <?php } ?>
                    <script type="text/javascript"> (tarteaucitron.job = tarteaucitron.job || []).push('youtube'); </script>
					<script type="text/javascript"> (tarteaucitron.job = tarteaucitron.job || []).push('youtubeplaylist'); </script>
                    <p itemprop="description"><?php echo $description_produit; ?></p>
                    
                    <?php if ($garantie_article != 0 && !empty($garantie_article)) { ?>
                    <p>Garantie : <?php echo $garantie_article; ?></p>
                    <?php } ?>
                    
                    <?php if (!empty($code_barre_article)) { ?>
                    <p>Code Barre : <?php echo $code_barre_article; ?></p>
                    <?php } ?>
				</div>
                
                <div style="clear: both;"></div>
            </div>        
			
            <div id="onglet_1" class="off content">
                <br>                
				<CENTER><div class="youtube_player" videoID="<?php echo $produit_data['products_video_1']; ?>" width="<?php if ($produit_data['products_video_1_largeur'] == 0 )  { echo '940'; } else { echo $produit_data['products_video_1_largeur']; } ?>" height="<?php if ($produit_data['products_video_1_hauteur'] == 0 )  { echo '658'; } else { echo $produit_data['products_video_1_hauteur']; } ?>" theme="dark" rel="0" controls="1" showinfo="1" autoplay="0"></div></CENTER>
				<br>
				<?php if (!empty($produit_data['products_video_2'])) { ?>
				<CENTER><div class="youtube_player" videoID="<?php echo $produit_data['products_video_2']; ?>" width="<?php if ($produit_data['products_video_2_largeur'] == 0 )  { echo '940'; } else { echo $produit_data['products_video_2_largeur']; } ?>" height="<?php if ($produit_data['products_video_2_hauteur'] == 0 )  { echo '658'; } else { echo $produit_data['products_video_2_hauteur']; } ?>" theme="dark" rel="0" controls="1" showinfo="1" autoplay="0"></div></CENTER>				
				
				<?php } ?>
								
            </div>
            <div id="onglet_6" class="off content">
				<div id="container">
				<center>
					<iframe width="940" height="680" src="https://www.generalarmystore.fr/gas/telechargement/Vue_360/<? echo $produit_data['products_vue_360']; ?>/Preview.html" frameborder="0" allowfullscreen></iframe>
					</center>
				</div>
            </div>
			
            <?php if (tep_db_num_rows($packs_query) > 0) { ?>
                <div id="onglet_2" class="off content">
                    <table>
                        <tr>
                            <th>LES PACKS PROMO</th>
                            <th>Prix Normal</th>
                            <th>Prix du pack</th>
                            <th>Economie R�alis�e</th>
                            <th>&nbsp;</th>
                        </tr>
    
                        <?php
                        $i = 0;
						$taxe = taux_taxe();
                        while ($packs_data = tep_db_fetch_array($packs_query)) {
							
                            $image = "";
							$texte = "";
							$prix_pack = 0;
							$pack_id = $packs_data['pack_id'];
                            $noms_articles = "";
							
							get_info_produit($packs_data['products_id']);
							$PACK[$pack_id]['product'][] = get_info_produit($packs_data['products_id_1'],true);
							
							if($packs_data['products_id_2']>0) {
								$PACK[$pack_id]['product'][] = get_info_produit($packs_data['products_id_2'],true);
							}
							
							$remise = $packs_data['pack_remise'];
                            $prix_normal_ttc = format_to_money($prix_pack*$taxe);
                            $prix_pack_ttc = (string)format_to_money(($prix_pack-$remise)*$taxe);
							$prix_remise_ttc = (string)format_to_money($remise*$taxe);
							
							//echo '<tr><td class="designation_pack" colspan="5"> </td></tr>';
							
							
							echo '<tr><td class="designation_pack">';
							
							echo '<br />'. $image ."<br />". $texte .'</td>';
							echo '<td class="prix_normal">';
							prix2img($prix_normal_ttc, "micro");
							echo '</td><td class="prix_pack">';
							prix2img($prix_pack_ttc, "micro");
							echo '</td><td class="economie">';
							prix2img($prix_remise_ttc, "micro");
							$noms_articles = substr($noms_articles,2);
							
							
							$PACK[$pack_id]['remise'] = $prix_remise_ttc;
							
							//echo '</td><td><a href="' . BASE_DIR . '/includes/lightboxes/ajout_panier.php?parent=\'fiche\'&amp;nom=' . addslashes($noms_articles) . '&amp;pack=' . $pack_id . '&amp;economie=' . $prix_remise_ttc . '&amp;' . tep_get_all_get_params() . '" class="lbOn"><img src="' . BASE_DIR . '/template/base/liste_produits/icone_panier_grand.png" alt="Ajouter au panier"  /></a></td></tr>';
							echo '</td><td><img class="pointer add_pack_to_basket" id="pack_'. $pack_id .'_to_basket" src="' . BASE_DIR . '/template/base/liste_produits/icone_panier_grand.png" alt="Ajouter au panier"  /></td></tr>';
							
							$i++;
						}
                        ?>
                    </table>
                </div>
            <?php } ?>

            <div id="onglet_3" class="off content">
                <!--<h1>L'avis des clients</h1>-->
										
					<?php 
					// AVIS VERIFIES Liste des commentaires du produit
					
					// Recup�ration des avis sur le produit et lecture du XML
					$fichier_avis_verifies_avis_produits = 'http://cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/AWS/PRODUCT_API/REVIEWS/' . $id_produit . '.xml';
								
					$file = @fopen($fichier_avis_verifies_avis_produits, 'r'); 
					if ($file) {	
						$xml = simplexml_load_file($fichier_avis_verifies_avis_produits); 
					} else {
						$xml = '';
					}
					
					if (empty($xml)) { 
					echo '<div style="padding:20px 10px 30px 10px; border-bottom: 1px dashed #999999;"><strong>Il n\'y a pas encore d\'avis sur ce produit</strong></div>';
					echo '<div style="padding:20px 10px 0 10px; margin:10px;  background-color:#ffffff;">En attendant, vous pouvez d�couvrir les notes et commentaires de nos clients sur d\'autres produits :<br><br>';
					echo '<iframe src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2aproduit_all_index.html" style="border:none;width: 100%;"  ></iframe><br><br></div>
					';
					} else { 
					
						
						$fichier_avis_verifies_note_globale = 'http://cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/AWS/PRODUCT_API/AVERAGE/' . $id_produit . '.xml';
						$xmlgeneral = simplexml_load_file($fichier_avis_verifies_note_globale); 
						
						foreach($xmlgeneral as $avisgeneral){
							echo '	<div style="width:100%; text-align: center; height:70px; padding-top:20px;">
										<div style="width:50%; float: left;">
										<img src="https://www.generalarmystore.fr/gas/images/etoiles/logo-avis-verifies.png" alt="Logo Avis V�rifi�s" width="250px" heigth="54px" style="vertical-align:middle;">
									</div>
									<div style="width:50%; margin-left:50%; text-align:center;">';
									
							echo '<strong>Ce produit � re�u ' . $avisgeneral->count . ' avis de nos clients<br>
										La note moyenne est de ' . number_format($avisgeneral->rate,2) . ' sur 5</strong><br>';
							// Fonction d'affichage des notes en image avec demi note
							echo avis_verifies_complet($avisgeneral->rate);
							
							echo '	</div>
							</div>';							
						}									
						
						foreach($xml as $avis){
							
						
												
							echo '<div style="border-top:1px dashed #999999; padding:10px;margin-top:10px;color:#666666;" itemprop="reviews" itemscope itemtype="https://schema.org/Review">';
							// Fonction d'affichage des �toiles en fonction de la note
							echo avis_verifies($avis->rate);
							
							//echo 'Note de <strong>'.$avis->rate . '/5</strong> - ';	
							echo '&nbsp&nbspAvis d�pos� le ' . date("d-m-Y", strtotime($avis->review_date)) . ' par <span itemprop="name">' . $avis->firstname . ' ' . $avis->lastname . '</span><meta itemprop="author" content="'. $avis->firstname . ' ' . $avis->lastname .'" /><br>';
							echo '<div style="color:#000000; padding:10px; font-size:13px; background-color:#ffffff; margin-top:10px; "><span itemprop="reviewBody">' . utf8_decode($avis->review) . '</span></div>';
							
							// A RETIRER QUAND LES COMMENTAIRES PRODUITS SERONT DANS L'ORDRE
							$nombre_de_reponses = count($avis->moderation->exchange);
							$nombre_de_reponse = $nombre_de_reponses-1; 
							
							if (!empty($avis->moderation)) {
							echo '<div style="margin:10px 0 0 33px;"><strong>R�ponse de G�n�ral Army Store le ' . date("d-m-Y", strtotime($avis->moderation->exchange->date)) . ' :</strong> </div>';
							echo '<div style="color:#000000; padding:10px; font-size:13px; background-color:#ffffff; margin:10px 0 0 30px; ">';
							// REMPLACER "$nombre_de_reponse" par "0" QUAND LES COMMENTAIRES PRODUITS SERONT DANS L'ORDRE
							echo utf8_decode($avis->moderation->exchange[$nombre_de_reponse]->comment) . '</div>';
							
							}
							echo '</div>';
						}
					}
					// END AVIS VERIFIES Liste des commentaires et des r�ponses du produit 
					?>				
            </div>
            <?php // Thierry modification du 19/04/2018 pour controler que la variable ne soit pas vide ?>
			<?php if (!empty($_SESSION['customers_compatibilite'])  &&  ($_SESSION['customers_compatibilite'] == 1)) { ?>
            <div id="onglet_4" class="off content">
                <h1>Compatibilit�s</h1>
                <table style="width: 100%">
                    <?php
                    $premier = true; $i = 0;
                    $compatibilite_query = tep_db_query("SELECT ptc.categories_id, cd.categories_name, cd.categories_url, cd_marque.categories_id as id_marque, cd_marque.categories_name as nom_marque, cd_marque.categories_url as url_marque, c.parent_id
                                                 FROM " . TABLE_PRODUCTS_TO_CATEGORIES . " ptc, " . TABLE_CATEGORIES_DESCRIPTION . " cd, " . TABLE_CATEGORIES_DESCRIPTION . " cd_marque, " . TABLE_CATEGORIES . " c
                                                 WHERE ptc.products_id = " . $id_produit . " AND ptc.categories_id = cd.categories_id AND cd.categories_id = c.categories_id AND c.parent_id = cd_marque.categories_id
                                                 ORDER BY cd_marque.categories_name ASC, cd.categories_name ASC");
                    while ($compatibilite_data = tep_db_fetch_array($compatibilite_query)) {
                        $i++;
                        if ($premier) {
                            echo '<tr><td style="font-weight: bold; font-size: 1.2em; background-color: white;" colspan="4">' . $compatibilite_data['nom_marque'] . '</td></tr><tr>';
                            $marque_old = $compatibilite_data['nom_marque'];
                            $premier = false;
                            $i--;
                        }
                        
                        if ($marque_old != $compatibilite_data['nom_marque']) {
                            echo '</tr><tr><td style="font-weight: bold; font-size: 1.2em; background-color: white; line-height: 20px;" colspan="4">' . $compatibilite_data['nom_marque'] . '</td></tr><tr>';
                            $marque_old = $compatibilite_data['nom_marque'];
                            $i = 0;
                        }
                        
                        if ($i % 4 == 0) {
                            echo '</tr>';
                        }
						$url_clean_modele = url("modele",array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $compatibilite_data["id_marque"], 'url_marque' => $compatibilite_data["url_marque"], 'id_modele' => $compatibilite_data["categories_id"], 'url_modele' => $compatibilite_data["categories_url"]));
                        echo  '<td><a href="'.$url_clean_modele.'">' . $compatibilite_data['categories_name'] . '</a></td>';
                    }
                    ?>
                </table>
            </div>
            <?php } ?>
            
            <div id="onglet_5" class="off content">
				<?php 
                    $prix_par_qte_query = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS_PRICE_BY_QUANTITY . " WHERE products_id = '" . $id_produit . "'");
                    $prix_par_qte = tep_db_fetch_array($prix_par_qte_query);
                    
                    if($_SESSION['customers_type'] == 1) {
						
                        $nom = 'particuliers';
						
						$prix_par_1 = $produit_data['part_price_by_1'];
                        $prix_par_5 = $produit_data['part_price_by_5'];
                        $prix_par_10 = $produit_data['part_price_by_10'];
                    }
					elseif($_SESSION['customers_type'] == 2) {
						
                        $nom = 'professionnels';
						
						$prix_par_1 = $produit_data['pro_price_by_1'];
                        $prix_par_5 = $produit_data['pro_price_by_5'];
                        $prix_par_10 = $produit_data['pro_price_by_10'];
                    }
					elseif($_SESSION['customers_type'] == 3) {
						
                        $nom = 'revendeurs';
						
						$prix_par_1 = $produit_data['rev_price_by_1'];
                        $prix_par_5 = $produit_data['rev_price_by_5'];
                        $prix_par_10 = $produit_data['rev_price_by_10'];
                    }
					elseif($_SESSION['customers_type'] == 4) {
						
                        $nom = 'administrations';
						
						$prix_par_1 = $produit_data['adm_price_by_1'];
                        $prix_par_5 = $produit_data['adm_price_by_5'];
                        $prix_par_10 = $produit_data['adm_price_by_10'];
                    }
                ?>
                <h1>Prix � l'unit� par quantit�s pour les <?php echo $nom; ?></h1>
                <div style="margin: 15px; margin-right: 0px; width: 48%; float: left;">
                    <table style="color:#000000;font-size:12px;width: 100%;">
                        <tr>
                            <td style="border: 1px solid #CCC; text-align: right;">Pour l'achat de 1 article</td>
                            <td style="border: 1px solid #CCC; text-align: center;"><?php echo money_format("%i", $prix_par_1); ?> � HT</td>
                            <td style="border: 1px solid #CCC; text-align: center;"><?php echo money_format("%i", $prix_par_1*1.2); ?> � TTC</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #CCC; text-align: right;">Pour l'achat de 5 � 9 articles</td>
                            <td style="border: 1px solid #CCC; text-align: center;"><?php echo money_format("%i", $prix_par_5); ?> � HT</td>
                            <td style="border: 1px solid #CCC; text-align: center;"><?php echo money_format("%i", $prix_par_5*1.2); ?> � TTC</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #CCC; text-align: right;">Pour l'achat de 10 articles et plus</td>
                            <td style="border: 1px solid #CCC; text-align: center;"><?php echo money_format("%i", $prix_par_10); ?> � HT</td>
                            <td style="border: 1px solid #CCC; text-align: center;"><?php echo money_format("%i", $prix_par_10*1.2); ?> � TTC</td>
                        </tr>
                    </table>
                </div>
                
                <div style="margin: 15px; margin-left: 0px; width: 48%; float: right;">
                	Si vous avez un besoin sup�rieur � <strong style="color:#738648;">25 articles</strong> d'une m�me r�f�rence, veuillez nous contacter soit par t�l�phone au <strong style="color:#738648;"><?php echo TEL_NORMAL; ?></strong> soit en vous rendant sur la page "<a href="<?php echo BASE_DIR; ?>/contact.php" target="_blank" style="color:#738648; text-decoration: none; font-weight: bold;">Contactez-nous</a>".
                </div>
                
                <div style="clear: both;"></div>
            </div>
			
            <div>
            <?php
			include("includes/modules/egalement_achete.php");
            ?>
            
            </div>
            
            <?php 
				$url_social=WEBSITE.$url_clean;
				echo '<br />Vous avez trouv� ce produit moins cher ailleurs ? Faites nous le savoir en <a id="moins_cher_ailleurs_button" class="pointer" style="color:#0188C6;"><strong>cliquant ici</strong></a><br />';
			?>
            
            <br />
			<strong>Vous appr�ciez ce produit ? Faites le savoir !</strong><br /><br />
			<!-- AddThis Button BEGIN -->
			<!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5abcce51c6c502b6"></script>-->
			<script type="text/javascript">
				tarteaucitron.user.addthisPubId = 'ra-5abcce51c6c502b6';
				(tarteaucitron.job = tarteaucitron.job || []).push('addthis');
			</script>
			<div class="addthis_inline_share_toolbox addthis_sharing_toolbox"></div>
			<!-- AddThis Button END -->          
			
        </div>
    </div>
    </div>
</div>

<div id="overlay"></div>

<div id="lightbox">
	<div style="float:right;"><img class="close_lightbox pointer" src="<?php echo BASE_DIR; ?>/template/base/boutons/icone_fermer.png" title="fermer"/></div>
	<br>
	<div id="lightbox_product">
		<h1>Vous venez d'ajouter ce produit � votre panier</h1>
       	<p style="font-size:18px; margin-top:-5px; margin-bottom:12px;"><?php echo bda_product_name_transform($produit_data['products_name'], $id_modele); ?></p>
	</div>
	<?php 
	if (tep_db_num_rows($packs_query) > 0) { 
	?>
		<div id="lightbox_pack">
			<?php		
			foreach($PACK as $pack_id => $key) {
				
				echo '<div id="pack_'. $pack_id .'" style="display:none">';
						
					echo '<h1>Vous venez d\'ajouter ce pack � votre panier</h1>';
					// produit principal
					echo '<p>'. bda_product_name_transform($produit_data['products_name'], $id_modele);
					for($i=0 ; $i<sizeof($key['product']) ; $i++) {
						
						echo '<br />';
						echo '<img src="' . BASE_DIR . '/template/base/fiche_article/plus_pack.png" alt="+" width="15"/>';
						echo '<br />'. $key['product'][$i];
					}
					echo '</p>';
					?>
					<h1 style="color:#DF4E02;">Vous avez �conomis� : <?php echo $key['remise']; ?> Euros !</h1>
				</div>
			<?php
			}
			?>
		</div>
	<?php 
	} 
	
	if (tep_db_num_rows($infos_options_query) != 0) { ?>
		<div id="options">
			<?php //echo $products_options['options_name']; ?>
			<?php //echo sizeof($products_options['options_values_name']); ?>
			<table>
				<tr>
				<?php
				for ($i=0; $i<sizeof($products_options['options_values_name']); $i++) {
					if ($i%4==0 && $i!=0) {
						echo '</tr><tr>';
					}
					echo '<td>';
						echo ($products_options['options_quantity'][$i]<=0)
							? '<input type="radio" disabled="disabled" name="product_options" value="{'. $products_options['options_id'][$i] .'}'. $products_options['options_values_id'][$i] .'" />'
							: '<input type="radio" name="product_options" value="{'. $products_options['options_id'][$i] .'}'. $products_options['options_values_id'][$i] .'" />';
						echo ($products_options['options_quantity'][$i]<=0) ? '<s>'. $products_options['options_values_name'][$i] .'</s>' : $products_options['options_values_name'][$i];
						if(($produit_data['products_manage_stock']=="destock_visible") || ($produit_data['products_manage_stock']=="destock_cache_qte")) {
							
							echo ($products_options['options_quantity'][$i]<=0) ? '&nbsp;(0)' : '&nbsp;('. $products_options['options_quantity'][$i] .')';
						}
					echo '</td>';
				}
				?>
				</tr>
			</table>
		</div>
	<?php 
	} 
	?>
    
	<div id="lightbox_moins_cher" style="display:none;padding:15px;">
		<form id="form_moins_cher" method="post">
			<div class="clear"></div>
			
			<div style="text-align:left; margin-bottom:10px;">
				Lien de la page du site proposant le produit moins cher : <input type="text" id="url_product" name="url_product" value="" size="65" />
			</div>		
			<div style="text-align:left; margin-bottom:10px;">
				<div>
					Prix affich� : <input type="text" id="prix_moins_cher" name="prix_moins_cher" value="" />
				</div>
			</div>         
			<div style="text-align:left; margin-bottom:10px;">
				<div>
					Votre adresse email : <input type="text" id="ad_mail" name="ad_mail" value="<?php if(isset($_SESSION) && !empty($_SESSION['customer_id'])) echo $_SESSION['customers_mail']; ?>" size="40"/>
				</div>
			</div>
			
			<?php 
			if (isset($_SESSION) && empty($_SESSION['customer_id'])) { 
				dsp_crypt2(0,1);
			?>
				<div style="text-align:left; margin-top:5px;">Recopier le code :<br><input type="text" id="code" name="code"></div>
			<?php 
			} 
			?>
			<input type="hidden" id="current_url" name="current_url" value="<?php echo WEBSITE . $_SERVER['REQUEST_URI']; ?>"/>
			<input class="send_mail_moins_cher" type="button" value="Envoyer ma demande" style="background-color: #555555; margin-top:10px; border: none; color: white; padding: 10px 24px; text-align: center; text-decoration: none; display: inline-block; font-size: 12px; font-weight:bold; border-radius: 8px;">
			<div id="response_send_mail" style="color:#8C9A09;display:none;"></div>
		</form>
	</div>
    
	<div id="lightbox_question" style="display:none;padding:15px;">
		<form id="form_question" method="post">
			<div class="clear"></div>
			<div style="text-align:left; margin-bottom:10px;">Merci de nous indiquer : </div>
			<div style="text-align:left; margin-bottom:10px;">
				Votre Nom : <input id="custom_name" name="custom_name" value="">&nbsp;&nbsp;&nbsp;
				Votre num�ro de T�l�phone : <input id="custom_tel" name="custom_tel" value="">
			</div>
			 
			<div style="text-align:left; margin-bottom:10px;">
				<div>
					 Votre email : <input type="text" id="custom_mail" name="custom_mail" value="<?php if(isset($_SESSION) && !empty($_SESSION['customer_id'])) echo $_SESSION['customers_mail']; ?>" size="40"/>
				</div>
			</div>
			 
			<div style="text-align:left; margin-bottom:10px;">
				et de poser votre question � propos de l'article :<br />
				<textarea id="custom_question" name="custom_question" cols="110" rows="6"></textarea>
			</div>
			 
			<?php 
			if (isset($_SESSION) && empty($_SESSION['customer_id'])) { 
			dsp_crypt(0,1);
			?>
				<div style="text-align:left; margin-top:5px;">Recopier le code : <br><input type="text" id="code" name="code"></div>
			<?php 
			} 
			?>
			
			<input type="hidden" id="current_url_2" name="current_url" value="<?php echo WEBSITE . $_SERVER['REQUEST_URI']; ?>"/>
			<input type="hidden" id="current_product" name="current_product" value="<?php echo $nom_produit; ?>"/>
			<input class="send_question" type="button" value="Envoyer ma question" style="background-color: #555555; margin-top:10px; border: none; color: white; padding: 10px 24px; text-align: center; text-decoration: none; display: inline-block; font-size: 12px; font-weight:bold; border-radius: 8px;">
		</form>
	</div>
    
    <div class="images product_images">
        <img class="product_back_to_product" alt="Continuer mes achats" src="<?php echo BASE_DIR;?>/template/base/ajout_panier_lb/bouton_continuer-achats.png"/>
        <img class="product_validate_basket" alt="Valider mon panier" src="<?php echo BASE_DIR;?>/template/base/ajout_panier_lb/bouton_valider-panier.png"/>
    </div>
    
    <div class="images pack_images">
        <img class="pack_back_to_product" alt="Continuer mes achats" src="<?php echo BASE_DIR;?>/template/base/ajout_panier_lb/bouton_continuer-achats.png" height="49" width="182"/>
        <img class="pack_validate_basket" alt="Valider mon panier" src="<?php echo BASE_DIR;?>/template/base/ajout_panier_lb/bouton_valider-panier.png" height="49" width="182"/>
    </div>
</div>

<!-- affichage des boutons une fois la page charg�e
<script type="text/javascript">affiche_boutons_js();</script> -->

<?php
require("includes/footer.php");
require("includes/page_bottom.php");

function get_info_produit($products_id,$avec_plus=false){
	global $image, $texte, $prix_pack, $noms_articles;
	
	$query_url="SELECT cd.categories_id as id_modele, cd.categories_name as nom_modele, cd.categories_url as url_modele, 
					   rub.rubrique_id, rub.rubrique_name, 
					   cd2.categories_name as nom_marque, cd2.categories_id as id_marque, cd2.categories_url as url_marque, 
					   pd.products_id, pd.products_url as url_article, p.products_bimage, p.products_price
            FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
			  				     			   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
								 			   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
								 			   inner join " . TABLE_RAYON . " as rub on rub.rubrique_id=cr.rubrique_id
											   inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " as pc on c.categories_id=pc.categories_id
											   inner join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on pd.products_id=pc.products_id
											   inner join " . TABLE_PRODUCTS . " as p on p.products_id=pd.products_id
            WHERE pd.products_id='".$products_id."'";
	$res_produit_data=tep_db_query($query_url);
	$row_produit_data=tep_db_fetch_array($res_produit_data);

	$nom_produit=bda_product_name_transform($row_produit_data['url_article'], $row_produit_data['id_modele']);
	if($avec_plus){	
		$image .= ' <img src="' . BASE_DIR . '/template/base/fiche_article/plus_pack.png" alt="+" width="15"/> ';
	}
	$image .= '<img src="' . BASE_DIR . '/images/products_pictures/mini/' . $row_produit_data['products_bimage'] . '" alt="Image" />';
	$url_clean = url("article",array('id_rayon' => $row_produit_data["rubrique_id"], 'nom_rayon' => $row_produit_data["rubrique_name"], 'id_marque' => $row_produit_data["id_marque"], 'url_marque' => $row_produit_data["url_marque"], 'id_modele' => $row_produit_data["id_modele"], 'url_modele' => $row_produit_data["url_modele"], 'id_article' => $row_produit_data["products_id"], 'url_article' => bda_product_name_transform($row_produit_data["url_article"],$row_produit_data["id_modele"])));
	
	if($avec_plus){	
		$texte .= ' <img src="' . BASE_DIR . '/template/base/fiche_article/plus_pack.png" alt="+" width="15"/><br> ';
	}
	$texte .= "<a href=\"" . $url_clean. "\">" . $nom_produit . "</a>";
	$noms_articles .= '$$' . $nom_produit;
	$prix_pack += $row_produit_data['products_price'];
	
	return $nom_produit;
}
?>