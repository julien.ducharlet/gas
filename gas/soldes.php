<?php
/* Page affichant la liste des articles en destockage ou en solde. L'affichage peut se faire sous 3 formes : 'Images', 'Icones' et 'Liste'*/

$nom_page = "produits_liste";
   
require("includes/page_top.php");

//date du jour 
$today = date("Y-m-d H:i:s");

require("includes/meta/soldes.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/produits_liste.js"></script>

<?php
		
// Si le type de vue n'a jamais �t� d�fini on met par d�faut la vue "images"
if (!isset($_SESSION['type_vue']))
	$_SESSION['type_vue'] = 'images';

?>

<div id="corps">
	<input type="hidden" id="base_dir" value="<?php echo BASE_DIR . "/"; ?>" />
    

    
    <div id="produits">
        <?php
		
		
			$nb_articles_par_page = 200;
			
			if (!isset($_REQUEST["page"])){
				$page = 1;
			} else {
				$page = $_REQUEST["page"];
			}
			
			$premier_article_page = ($page-1) * $nb_articles_par_page;

			$products_query = tep_db_query("SELECT p.products_price, p.products_image, p.products_bimage, p.products_sort_order, p.products_date_added, p.products_quantity_min,
										   pd.products_id, pd.products_name, pd.products_url, pd.balise_title_lien_image, pd.balise_title_lien_texte,
										   ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1,
										   r.rubrique_url, r.rubrique_name, r.rubrique_id,
										   cd.categories_id, cd.categories_name, cd.categories_url,
										   cd2.categories_id as id_marque, cd2.categories_url as url_marque, cd2.categories_name as nom_marque,
										   s.specials_new_products_price, s.expires_date
											FROM  " . TABLE_PRODUCTS . " p
											left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id
											left join " . TABLE_SPECIALS . " s on (s.products_id = p.products_id and (expires_date >'".$today."' or expires_date='0000-00-00 00:00:00')),										
											" . TABLE_PRODUCTS_DESCRIPTION . " pd, 
											" . TABLE_CATEGORIES . " c, 
											" . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, 
											" . TABLE_CATEGORIES_DESCRIPTION . " cd, 
											" . TABLE_CATEGORIES_DESCRIPTION . " cd2, 
											" . TABLE_RAYON . " r, 
											" . TABLE_CATEGORIES_RAYON . " cr 
											WHERE p.products_status = '1' and p.products_id = pd.products_id and p.products_id = p2c.products_id and p2c.categories_id = c.categories_id and cd.categories_id=c.categories_id and c.categories_id = cr.categories_id and cr.rubrique_id = r.rubrique_id and c.parent_id = cd2.categories_id and p.products_manage_stock = 'destock_visible'
											GROUP BY pd.products_id
											ORDER BY products_name DESC");
		?>
        
        <div class="nbr_produits">Voici les <strong><?php echo tep_db_num_rows($products_query); ?></strong> articles en destockages ou en soldes.</div>
        
        <div class="type_affichage"><div class="texte">Cliquez sur une des ic�nes pour modifier l'affichage des articles :</div><div id="liste" onclick="change_type(this.id)"></div><div id="icones" onclick="change_type(this.id)"></div><div id="images" onclick="change_type(this.id)"></div></div>
 
        <div style="clear:both;"></div>
        
        
        
        <div id="articles_images">
	        <table>
    	        <tr>
                <?php
                    $i = 0; $j = 0;
					$nb_articles_ligne = 6;
					$est_affiche = true;
                    while($products_data = tep_db_fetch_array($products_query)){
						$j++;
						
						
						$est_affiche = ($j > $premier_article_page && $j <= $premier_article_page + $nb_articles_par_page);
												
						if ($est_affiche) {
							$i++;
							
							$id_rayon = $products_data['rubrique_id'];
							$nom_rayon = $products_data['rubrique_name'];
							$url_rayon = $products_data['rubrique_url'];
							$id_modele = $products_data['categories_id'];
							$nom_modele = $products_data['categories_name'];
							$url_modele = $products_data['categories_url'];
							$id_marque = $products_data['id_marque'];
							$nom_marque = $products_data['nom_marque'];
							$url_marque = $products_data['url_marque'];
														
							$products_name = bda_product_name_transform($products_data['products_name'], $id_modele);
							$products_url = bda_product_name_transform($products_data['products_url'], $id_modele);
							
							$prix = calculate_price_for_product($products_data);
							
							$url = url('article', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele, 'url_modele' => $url_modele, 'id_article' => $products_data['products_id'], 'url_article' => $products_url));
							?>

                                	<td class="article" <?php if ($i % $nb_articles_ligne == 1) echo 'style="border-left: none;"'; ?> onclick="location.href='<?php echo $url; ?>';">
                                        <div class="image_article">
                                            <a href="<?php echo $url; ?>" title="Solde sur <?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?> - Soldes - destockage en lot"><img src="<?php echo BASE_DIR . "/images/products_pictures/petite/" . $products_data['products_bimage']; ?>" alt="<?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?>" /></a>
                                           
                                        </div>
                                        <div class="nom_article">
                                            <a href="<?php echo $url; ?>" title="destockage <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?> destockages et soldes en lot"><?php echo $products_name; ?></a>
											
                                        </div>
                                        <div class="prix_article">
                                            <?php
											
											  ($prix['promo'] > 0) ? prix2img($prix['promo'], 'moyen') :  prix2img($prix['normal'], 'moyen');
											  
											?>
                                        </div>
                                	</td>
								<?php 
									if ($i % $nb_articles_ligne == 0) {
										?>
                                			</tr><tr>
                                        <?php
									}
								?>
							<?php
						}
                    }
                
                ?>
				</tr>
            </table>
        </div>
        
        
        
        <div id="articles_icones">
            <table>
                <tr>
                    <th>&nbsp;</th>
                    <th>&nbsp;Nom</th>
                    <th>&nbsp;Prix TTC</th>
                    <th>Quantit�</th>
                    <th>&nbsp;</th>
                </tr>
                <?php
                if (tep_db_num_rows($products_query) > 0) {
                    $i = 0; $j = 0;
					tep_db_data_seek($products_query, 0);
					$est_affiche = true;
                    while($products_data = tep_db_fetch_array($products_query)){
						$j++;
						
						$est_affiche = ($j > $premier_article_page && $j <= $premier_article_page + $nb_articles_par_page);
												
						if ($est_affiche) {
							$i++;
							$products_name = bda_product_name_transform($products_data['products_name'], $id_modele);
							$products_url = bda_product_name_transform($products_data['products_url'], $id_modele);
							
							$prix = calculate_price_for_product($products_data);
							
							$url = url('article', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele, 'url_modele' => $url_modele, 'id_article' => $products_data['products_id'], 'url_article' => $products_url));
							?>
							<tr class="<?php if (($i % 2) == 0) {echo 'fond_1';} else {echo 'fond_2';} ?>">
								<td class="image_article"><a href="<?php echo $url; ?>" title="<?php echo $products_data['balise_title_lien_texte'];?>tttt" ><img src="<?php echo BASE_DIR . "/images/products_pictures/petite/" . $products_data['products_bimage']; ?>" title="Accessoire <?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);  ?>" alt="<?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?>" /></a></td>
								<td class="nom" onclick="location.href='<?php echo $url; ?>';"><?php echo "<a href=\"" . $url . "\" title=\"".bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele)."\">" . $products_name . "</a>"; ?></td>
								<td class="prix">
									<?php
									  
									  if($prix['promo'] > 0) {
										  
										  echo "<span style='color:#8e2eaa; font-size:13px;'><strong>Promo</strong></span>";
										  echo "<br /><span style='text-decoration:line-through;'>". format_to_money($prix['normal']) ." &euro;</span>";
										  echo "<br />". format_to_money($prix['promo']) ." &euro;";
									  }
									  else echo $prix['normal'] ." �";
									?>
                                </td>
								<td class="quantite"><input id="quantite_icones_<?php echo $products_data['products_id'] . "_" . $id_marque . "_" . $id_modele; ?>" type="text" size="2" /></td>
								<td class="ajout_panier"><a href="<?php echo BASE_DIR; ?>/includes/lightboxes/ajout_panier.php?parent='icones'&amp;nom=<?php echo $products_name; ?>&amp;produit=<?php echo $products_data['products_id']; ?>&amp;<?php echo tep_get_all_get_params(); ?>" class="lbOn"><img src="<?php echo BASE_DIR; ?>/template/base/liste_produits/icone_panier_grand.png" title="achat <?php echo bda_product_name_transform($products_data['products_name'], $id_modele); ?>" alt="Achat <?php echo bda_product_name_transform($products_data['products_name'], $id_modele); ?> "  /></a></td>
							</tr>
							<?php
						}
					}
                } else {
                ?>
                <tr>
                    <td colspan="5" style="text-align: center; background-color:#CCC; font-weight: bold;">
					<br />
					<br />
					<strong>Il n'y a encore aucun article list� dans cette cat�gorie.<br />
					<br />
					Si vous cherchez un article bien pr�cis, vous pouvez nous contacter et nous nous efforcerons de vous le trouver !<br />
					<br />
					<br />
					</strong>
					</td>
                </tr>
                <?php
                }
                ?>
            </table>
        </div>
        
        <div id="articles_liste">
            <table>
                <tr>
                	<th>&nbsp;</th>
                    <th>&nbsp;Nom</th>
                    <th>&nbsp;Prix TTC</th>
                    <th>Quantit�</th>
                    <th>&nbsp;</th>
                </tr>
                <?php
                if (tep_db_num_rows($products_query) > 0) {
                    $i = 0; $j = 0;
					tep_db_data_seek($products_query, 0);
					$est_affiche = true;
                    while($products_data = tep_db_fetch_array($products_query)){
						$j++;
						
						$est_affiche = ($j > $premier_article_page && $j <= $premier_article_page + $nb_articles_par_page);
												
						if ($est_affiche) {
							$i++;
							$products_name = bda_product_name_transform($products_data['products_name'], $id_modele);
							$products_url = bda_product_name_transform($products_data['products_url'], $id_modele);
							
							$prix = calculate_price_for_product($products_data);
							
							$url = url('article', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele, 'url_modele' => $url_modele, 'id_article' => $products_data['products_id'], 'url_article' => $products_url));
							?>
							<tr class="<?php if (($i % 2) == 0) {echo 'fond_1';} else {echo 'fond_2';} ?>">
								<td class="icone_apercu" onmouseover="document.getElementById('div_image_article_<?php echo $i; ?>').style.display = 'block'; document.getElementById('div_image_article_<?php echo $i; ?>').style.backgroundImage = 'url(<?php echo BASE_DIR . "/images/products_pictures/normale/" . $products_data['products_bimage']; ?>)';" onmouseout="document.getElementById('div_image_article_<?php echo $i; ?>').style.display = 'none';"><a href="<?php echo $url; ?>" title="<?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?>"><img src="<?php echo BASE_DIR; ?>/template/base/liste_produits/icone_apercu_article.png" title="Accessoires <?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);  ?>" alt="<?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?>"/></a></td>
								<td class="nom" onclick="location.href='<?php echo $url; ?>';"><?php echo "<a href=\"" . $url . "\" title=\"vendre ".bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele)."\">" . $products_name . "</a>"; ?></td>
								<td class="prix">
									<?php
									
									  if($prix['promo'] > 0) {
										  
										  echo "<span style='color:#8e2eaa; font-size:13px;'><strong>Promo</strong></span>";
										  echo "<br /><span style='text-decoration:line-through;'>". format_to_money($prix['normal']) ." &euro;</span>";
										  echo "<br />".format_to_money($prix['promo'])." &euro;";
									  }
									  else echo $prix['normal'] . " �";
									?>
                                </td>
								<td class="quantite"><input id="quantite_liste_<?php echo $products_data['products_id'] . "_" . $id_marque . "_" . $id_modele; ?>" type="text" size="2" /></td>
								<td class="ajout_panier"><a href="<?php echo BASE_DIR; ?>/includes/lightboxes/ajout_panier.php?parent='liste'&amp;nom=<?php echo $products_name; ?>&amp;produit=<?php echo $products_data['products_id']; ?>&amp;<?php echo tep_get_all_get_params(); ?>" class="lbOn"><img src="<?php echo BASE_DIR; ?>/template/base/liste_produits/icone_panier_petit.png" title="Acheter <?php echo bda_product_name_transform($products_data['products_name'], $id_modele); ?>" alt="acheter <?php echo bda_product_name_transform($products_data['products_name'], $id_modele); ?>"  /></a></td>
							</tr>
							<tr><td class="td_image_article"><div id="div_image_article_<?php echo $i; ?>" class="grande_image_article"></div></td></tr>
							<?php
						}
                    }
                } else {
                ?>
                <tr>
                    <td colspan="5" style="text-align: center; background-color:#CCC; font-weight: bold;">
						<br />
						<br />
						<strong>Il n'y a encore aucun article list� dans cette cat�gorie.<br />
						<br />
						Si vous cherchez un article bien pr�cis, vous pouvez nous contacter et nous nous efforcerons de vous le trouver !<br />
						<br />
						<br />
						</strong>
					</td>
                </tr>
                <?php
                }
                ?>
            </table>
		</div>
		
        <div style="clear:both;"></div>
        
        <div>
            <?php
				$nb_articles = tep_db_num_rows($products_query);
				$nb_pages = ceil($nb_articles / $nb_articles_par_page);
				
				if ($nb_articles > $nb_articles_par_page) {
					echo "Page : ";
					
					for ($i = 1; $i <= $nb_pages; $i++) {
						echo "<a href='" . $_SERVER['PHP_SELF'] . "?page=" . $i ."'>" . $i . "</a> ";
					}
				}
			?>
        </div>
		
        
	</div>
</div>

<!-- Div ou la requ�te AJAX va �crire l'affectation de variable de session -->
<div id="affect_session_var"></div>

<!-- On affiche la bonne div correspondant au type de vue choisie -->
<script type="text/javascript">change_type('<?php echo $_SESSION['type_vue']; ?>');</script>

<?php 
require("includes/footer.php");
require("includes/page_bottom.php");
?>