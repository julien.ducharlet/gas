<?php
/* Auteur : Paul
   Page pour les infos sur le recrutement*/
	 
// Pour CSS dynamique dans le Dossier CSS (pensez a cr�er un fichier CSS)
$nom_page = "tableau_des_tailles";

require("includes/page_top.php");
//require("includes/meta/.php"); // Pour ajouter des balises META particulieres a une page 
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">
	<div id="recrutement">
    	<div class="titre">Tableau de correspondance des tailles</div>
        
        <!--<div class="image_droite"></div> -->
        
		<div class="texte_intro">
			<table width="940" border="0" align="left" cellpadding="2" cellspacing="0">
				<!--<tr>
					<td align="center"><strong>Tableau de correspondance des tailles</strong></td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr> -->
				<tr width="100%">
					<td>
						<table width="100%" border="1" cellpadding="0" cellspacing="0">
							<tr>
								<td height="25" colspan="8" align="center" bgcolor="#666633"><strong><font size="2" color="white">Chapeaux, Bobs, Casquettes</font></strong></td>
							</tr>
							<tr>
								<td align="center">Taille en Chiffres</td>
								<td align="center">55</td>
								<td align="center">56</td>
								<td align="center">57</td>
								<td align="center">58</td>
								<td align="center">59</td>
								<td align="center">60</td>
								<td align="center">61</td>
							</tr>
							<tr>
								<td align="center">Taille en Lettres</td>
								<td align="center">XS</td>
								<td align="center">S</td>
								<td align="center">M</td>
								<td align="center">L</td>
								<td align="center">XL</td>
								<td align="center">XXL</td>
								<td align="center">XXXL</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<td height="25" colspan="9" align="center" bgcolor="#666633"><strong><font size="2" color="white">Parkas, Vestes, Blousons</font></strong></td>
							</tr>
							<tr>
								<td align="center">Taille</td>
								<td align="center">37/38</td>
								<td align="center">39/40</td>
								<td align="center">41/42</td>
								<td align="center">43/44</td>
								<td align="center">45/46</td>
								<td align="center">48/50</td>
								<td align="center">52/54</td>
								<td align="center">56/58</td>
							</tr>
							<tr>
								<td align="center">Poitrine (en cm)</td>
								<td align="center">84/88</td>
								<td align="center">89/96</td>
								<td align="center">97/104</td>
								<td align="center">105/112</td>
								<td align="center">113/120</td>
								<td align="center">121/128</td>
								<td align="center">129/136</td>
								<td align="center">137/144</td>
							</tr>
							<tr>
								<td align="center">Taille Militaire</td>
								<td align="center">88</td>
								<td align="center">96</td>
								<td align="center">104</td>
								<td align="center">112</td>
								<td align="center">120</td>
								<td align="center">128</td>
								<td align="center">136</td>
								<td align="center">144</td>
							</tr>
							<tr>
								<td align="center">Taille en Chiffres</td>
								<td align="center">1</td>
								<td align="center">2</td>
								<td align="center">3</td>
								<td align="center">4</td>
								<td align="center">5</td>
								<td align="center">6</td>
								<td align="center">7</td>
								<td align="center">8</td>
							</tr>
							<tr>
								<td align="center">Taille en Lettres</td>
								<td align="center">S</td>
								<td align="center">M</td>
								<td align="center">L</td>
								<td align="center">XL</td>
								<td align="center">XXL</td>
								<td align="center">XXXL</td>
								<td align="center">XXXXL</td>
								<td align="center">XXXXXL</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<td height="25" colspan="10" align="center" bgcolor="#666633"><strong><font size="2" color="white">Parkas,  Blousons</font></strong></td>
							</tr>
							<tr>
								<td align="center">Poitrine (en cm)</td>
								<td align="center">92</td>
								<td align="center">96</td>
								<td align="center">100</td>
								<td align="center">104</td>
								<td align="center">108</td>
								<td align="center">112</td>
								<td align="center">116</td>
								<td align="center">120</td>
								<td align="center">124</td>
							</tr>
							<tr>
								<td align="center">Taille Civile</td>
								<td align="center">46</td>
								<td align="center">48</td>
								<td align="center">50</td>
								<td align="center">52</td>
								<td align="center">54</td>
								<td align="center">56</td>
								<td align="center">58</td>
								<td align="center">60</td>
								<td align="center">62</td>
							</tr>
							<tr>
								<td align="center">Taille en Lettres</td>
								<td align="center">XS</td>
								<td align="center">S</td>
								<td align="center">M</td>
								<td align="center">L</td>
								<td align="center">XL</td>
								<td align="center">XXL</td>
								<td align="center">XXXL</td>
								<td align="center">XXXXL</td>
								<td align="center">XXXXXL</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<td height="25" colspan="9" align="center" bgcolor="#666633"><strong><font size="2" color="white">Sweat-shirts, Polos, Tee-shirts, Chemises, Chemisettes Arm&eacute;e</font></strong></td>
							</tr>
							<tr>
								<td align="center">Taille</td>
								<td align="center">37/38</td>
								<td align="center">39/40</td>
								<td align="center">41/42</td>
								<td align="center">43/44</td>
								<td align="center">45/46</td>
								<td align="center">47/48</td>
								<td align="center">49/50</td>
								<td align="center">51/52</td>
							</tr>
							<tr>
								<td align="center">Taille en Chiffres</td>
								<td align="center">1</td>
								<td align="center">2</td>
								<td align="center">3</td>
								<td align="center">4</td>
								<td align="center">5</td>
								<td align="center">6</td>
								<td align="center">7</td>
								<td align="center">8</td>
							</tr>
							<tr>
								<td align="center">Taille en Lettres</td>
								<td align="center">S</td>
								<td align="center">M</td>
								<td align="center">L</td>
								<td align="center">XL</td>
								<td align="center">XXL</td>
								<td align="center">XXXL</td>
								<td align="center">XXXXL</td>
								<td align="center">XXXXXL</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<td height="25" colspan="9" align="center" bgcolor="#666633"><font size="2" color="white"><strong>Pantalons, Bermudas, Shorts</strong></strong></td>
							</tr>
							<tr>
								<td align="center">Ceinture (cm)</td>
								<td align="center">64/68</td>
								<td align="center">69/72</td>
								<td align="center">73/76</td>
								<td align="center">77/80</td>
								<td align="center">81/84</td>
								<td align="center">85/88</td>
								<td align="center">89/92</td>
								<td align="center">93/96</td>
							</tr>
							<tr>
								<td align="center">Taille Civile</td>
								<td align="center">34</td>
								<td align="center">36</td>
								<td align="center">38</td>
								<td align="center">40</td>
								<td align="center">42</td>
								<td align="center">44</td>
								<td align="center">46</td>
								<td align="center">48</td>
							</tr>
							<tr>
								<td align="center">Taille Militaire</td>
								<td align="center">68</td>
								<td align="center">72</td>
								<td align="center">76</td>
								<td align="center">80</td>
								<td align="center">84</td>
								<td align="center">88</td>
								<td align="center">92</td>
								<td align="center">96</td>
							</tr>
							<tr>
								<td align="center">Taille en Lettres</td>
								<td align="center">XS</td>
								<td align="center">XS</td>
								<td align="center">S</td>
								<td align="center">S</td>
								<td align="center">M</td>
								<td align="center">M</td>
								<td align="center">L</td>
								<td align="center">L</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">Ceinture (cm)</td>
								<td align="center">97/100</td>
								<td align="center">101/104</td>
								<td align="center">105/108</td>
								<td align="center">109/112</td>
								<td align="center">113/116</td>
								<td align="center">117/120</td>
								<td align="center">121/128</td>
								<td align="center">129/136</td>
							</tr>
							<tr>
								<td align="center">Taille Civile</td>
								<td align="center">50</td>
								<td align="center">52</td>
								<td align="center">54</td>
								<td align="center">56</td>
								<td align="center">58</td>
								<td align="center">60</td>
								<td align="center">64</td>
								<td align="center">68</td>
							</tr>
							<tr>
								<td align="center">Taille Militaire</td>
								<td align="center">100</td>
								<td align="center">104</td>
								<td align="center">108</td>
								<td align="center">112</td>
								<td align="center">116</td>
								<td align="center">120</td>
								<td align="center">128</td>
								<td align="center">136</td>
							</tr>
							<tr>
								<td align="center">Taille en Lettres</td>
								<td align="center">XL</td>
								<td align="center">XL</td>
								<td align="center">XXL</td>
								<td align="center">XXL</td>
								<td align="center">XXXL</td>
								<td align="center">XXXL</td>
								<td align="center">XXXXL</td>
								<td align="center">XXXXL</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<td height="25" colspan="13" align="center" bgcolor="#666633"><strong><font size="2" color="white">Chaussures</font></strong></td>
							</tr>
							<tr>
								<td align="center">Taille</td>
								<td align="center">23</td>
								<td align="center">23.6</td>
								<td align="center">24.3</td>
								<td align="center">25</td>
								<td align="center">25.6</td>
								<td align="center">26.3</td>
								<td align="center">27</td>
								<td align="center">27.6</td>
								<td align="center">28.3</td>
								<td align="center">29</td>
								<td align="center">29.6</td>
								<td align="center">30.3</td>
							</tr>
							<tr>
								<td align="center">Taille en Chiffres</td>
								<td align="center">36</td>
								<td align="center">37</td>
								<td align="center">38</td>
								<td align="center">39</td>
								<td align="center">40</td>
								<td align="center">41</td>
								<td align="center">42</td>
								<td align="center">43</td>
								<td align="center">44</td>
								<td align="center">45</td>
								<td align="center">46</td>
								<td align="center">47</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<td height="25" colspan="8" align="center" bgcolor="#666633"><strong><font size="2" color="white">Gants</font></strong></td>
							</tr>
							<tr>
								<td align="center">Taille en Chiffres</td>
								<td align="center">6</td>
								<td align="center">7</td>
								<td align="center">8</td>
								<td align="center">9</td>
								<td align="center">10</td>
								<td align="center">11</td>
								<td align="center">12</td>
							</tr>
							<tr>
								<td align="center">Taille en Lettres</td>
								<td align="center">XS</td>
								<td align="center">S</td>
								<td align="center">M</td>
								<td align="center">L</td>
								<td align="center">XL</td>
								<td align="center">XXL</td>
								<td align="center">XXXL</td>
							</tr>
						</table>
					</td>
				</tr>
				
			</table>

        </div>
        
        <div style="clear:both;"></div>
        
    </div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>