<?php
/* Auteur : Paul 
   Affichage du panier du client*/

$nom_page = "panier";
   
require("includes/page_top.php");
require("includes/meta/panier.php");
require("includes/meta_head.php");
require("includes/header.php");

// Inclusion des fonctions d'affichage du panier
require("includes/fonctions/fonctions_panier.php");
?>

<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/panier.js"></script>

<div id="corps">
	<div id="panier">
    	<div class="titre">Qu'y a-t-il dans mon panier ?</div>
        <?php if(isset($_SESSION['parrainage_kdo']) && $_SESSION['parrainage_kdo']==1){?>
                    <div style="margin-top:10px; margin-bottom:10px; font-size:16px; color:#8e2eaa;" >
                    	<strong>Gr&acirc;ce au parrainage un cadeau vous sera livr&eacute; avec votre premi&egrave;re commande</strong>
                    </div>
        <?php }?>
        <div class="contenu">
        	<!-- Tableau contenant les articles choisis par le client -->
            <table id="table_articles">
                <thead>
                    <tr>
                        <th colspan="2">Articles</th>
                        <th>Prix Unitaire</th>
                        <th>Quantit�</th>
                        <th>Prix total</th>
                        <th style="border-right: none;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <?php
				// Si l'utilisateur est logu�
				if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] != '') {
					
					prix_livraison();
					
					// On r�cup�re les infos sur son panier
					$panier_query = tep_db_query("SELECT cb.products_id, cb.customers_basket_quantity, cb.id_rayon 
												  FROM " . TABLE_CUSTOMERS_BASKET . " as cb
												  WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' and cb.pack_id = ''");
					
					$i = 0;
					$nb_articles = tep_db_num_rows($panier_query);
					
					if ($nb_articles > 0) {
						
						while($panier_data = tep_db_fetch_array($panier_query)) {
							
							if(!empty($panier_data['products_id'])){
							$id_complet = $panier_data['products_id'];
							$i++;
							
							// On r�cup�re les infos sur les options
							$article_options = explode("{", $panier_data['products_id']);
							$options = explode("}", $article_options[1]);
							$option_details = infos_options($options[0], $options[1]);
							
							// On r�cup�re les infos sur l'article
							$article = explode("_", $article_options[0]);
							$quantite = $panier_data['customers_basket_quantity'];
							$article_details = infos_article($article, $quantite);
							
							// On r�cup�re des infos compl�mentaires
							//$promo = promo_product($article_details['id_article']);
							
							
							$articles_sous_total_HT += $article_details['prix_ht']*$quantite;
							
								// On affiche les articles du panier
								affiche_article_panier($nb_articles, $id_complet, $panier_data['id_rayon'], $article_details, $option_details, $quantite, $i, true);
								
							}
						}
					} else {
						?>
						<tr>
							<td colspan="6">Aucun Produit</td>
						</tr>
						<?php
					}
				// Si l'utilisateur n'est pas logu�
				} else {
					$i = 0;
					$nb_articles = count($_SESSION['panier']);
					
					if ($nb_articles > 0) {
						foreach ($_SESSION['panier'] as $article) {	
							$quantite = $article[1];
							if ($quantite > 0) {
								$i++;
								$id_complet = $article[0];
								
								$id_rayon = $article[4];
								
								// On r�cup�re les infos sur les options
								$article_options = explode("{", $article[0]);
								$options = explode("}", $article_options[1]);
								$option_details = infos_options($options[0], $options[1]);
								
								// On r�cup�re les infos sur l'article
								$info_article = explode("_", $article_options[0]);
								$article_details = infos_article($info_article, $quantite);
								
								// On r�cup�re des infos compl�mentaires
								/*$today = date("Y-m-d H:i:s");
									$req_promo=tep_db_query("select specials_new_products_price, expires_date from specials where products_id=".$article_details['id_article']." and (expires_date >'".$today."' or expires_date='0000-00-00 00:00:00')");
									$res_promo=tep_db_fetch_array($req_promo);
									$nb_row=tep_db_num_rows($req_promo);*/
									
								$articles_sous_total_HT += $article_details['prix_ht']*$quantite;
								
								// On affiche les articles du panier
								affiche_article_panier($nb_articles, $id_complet, $id_rayon, $article_details, $option_details, $quantite, $i, true);
							}
						}
					} else {
						?>
						<tr>
							<td colspan="6">Aucun Article dans le Panier</td>
						</tr>
						<?php
					}
				}
                ?>
                </tbody>
            </table>
            
            <!-- Tableau contenant les Packs choisis par le client -->
            <table id="table_packs">
                <thead>
                    <tr>
                        <th colspan="2">Packs</th>
                        <th>Prix Normal</th>
                        <th>Prix du Pack</th>
                        <th>Economie R�alis�e</th>
                        <th>Quantit�</th>
                        <th>Prix total</th>
                        <th style="border-right: none;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                
                <?php
					$pack_sous_total_HT = 0;
					
					//Si l'utilisateur est logu�
                    if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] != '') {
						// On r�cup�re les infos sur ses packs
                        $packs_query = tep_db_query("SELECT cb.pack_id, cb.customers_basket_quantity, cb.id_rayon 
                                                      FROM " . TABLE_CUSTOMERS_BASKET . " as cb
                                                      WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' and cb.pack_id <> ''");
						
                        $nb_packs = tep_db_num_rows($packs_query);
						
                        if ($nb_packs > 0) {
                            while($packs_data = tep_db_fetch_array($packs_query)) {
								
								if(!empty($packs_data['pack_id'])){
								
									$id_rayon = $packs_data['id_rayon'];
									
									$rayon_query = tep_db_query("SELECT rubrique_name  
																 FROM " . TABLE_RAYON . "
																 WHERE rubrique_id = '" . $id_rayon . "'");
									$rayon_data = tep_db_fetch_array($rayon_query);
									$nom_rayon = $rayon_data['rubrique_name'];
	
									$id_complet = $packs_data['pack_id'];
									$i++;
									
									// On r�cup�re les infos sur les options
									$pack_options = explode("{", $id_complet);
									$options = explode("}", $pack_options[1]);
									$option_details = infos_options($options[0], $options[1]);
									
									// On r�cup�re les infos sur le pack
									$pack = explode("_", $pack_options[0]);
									$pack_details = info_pack($pack, $option_details);
									
									// On r�cup�re des infos compl�mentaires
									$quantite = $packs_data['customers_basket_quantity'];
									$pack_sous_total_HT += $pack_details['prix_ht']*$quantite;
									
									$pack_details['liste_articles'] = str_replace('id_rayon_a_remplir', $id_rayon, $pack_details['liste_articles']);
									$pack_details['liste_articles'] = str_replace('nom_rayon_a_remplir', $nom_rayon, $pack_details['liste_articles']);
									
									//On affiche le pack et la liste de ses articles
									affiche_pack_panier($id_complet, $id_rayon, $pack_details, $quantite, $i, true);
								}
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="8">Aucun Pack dans le panier</td>
                            </tr>
                            <?php
                        }
					// Si l'utilisateur n'est pas logu�
                    } else {
						$j = 0;
                        $nb_packs = count($_SESSION['panier_packs']);
                        if ($nb_packs > 0) {
                            foreach ($_SESSION['panier_packs'] as $session_pack) {	
                                $quantite = $session_pack[1];
                                if ($quantite > 0) {
									$i++; $j++;
									$id_complet = $session_pack[0];
																	
									$id_rayon = $session_pack[5];
								
									$rayon_query = tep_db_query("SELECT rubrique_name  
																 FROM " . TABLE_RAYON . "
																 WHERE rubrique_id = '" . $id_rayon . "'");
									$rayon_data = tep_db_fetch_array($rayon_query);
									$nom_rayon = $rayon_data['rubrique_name'];
									
									// On r�cup�re les infos sur les options
									$pack_options = explode("{", $id_complet);
									$options = explode("}", $pack_options[1]);
									$option_details = infos_options($options[0], $options[1]);
									
									// On r�cup�re les infos sur le pack
									$pack = explode("_", $pack_options[0]);
									$pack_details = info_pack($pack, $option_details);
									
									// On r�cup�re des infos compl�mentaires
									$pack_sous_total_HT += $pack_details['prix_ht']*$quantite;
									
									$pack_details['liste_articles'] = str_replace('id_rayon_a_remplir', $id_rayon, $pack_details['liste_articles']);
									$pack_details['liste_articles'] = str_replace('nom_rayon_a_remplir', $nom_rayon, $pack_details['liste_articles']);
									
									//On affiche le pack et la liste de ses articles
									affiche_pack_panier($id_complet, $id_rayon, $pack_details, $quantite, $i, true);
                                }
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="8">Aucun Pack dans le Panier</td>
                            </tr>
                            <?php
                        }
                    }
                ?>
                </tbody>
            </table>
        </div>
        
        <div id="test"></div>
        
       <div id="totaux">
        	<?php $sous_total_total = $articles_sous_total_HT + $pack_sous_total_HT;
			if($sous_total_total<0){$sous_total_total=0;}?>
        	Sous-total HT : <strong><span id="ss_total_ht"><?php echo format_to_money($sous_total_total); ?></span> �</strong><br />
            <?php
            $taxe = taux_taxe();
			if ($taxe != 1) { ?>
	            TVA 20% : <strong><span id="tva"><?php echo format_to_money($sous_total_total * ($taxe-1)); ?></span> �</strong><br />
            <?php } ?>
            <span class="total">Total TTC : <strong><span id="total"><?php echo format_to_money($sous_total_total * $taxe); ?></span> �</strong></span>
        </div>
        
        <div class="actions">
        	<!-- Si on a au moins un pack ou au moins un article on peut comtinuer sinon l'image ne s'affiche pas -->
        	<?php if ($nb_articles > 0 || $nb_packs > 0) { ?>
            <div class="poursuivre" id="bouton_poursuivre" onclick="location.href='panier_livraison.php';"><img height="48" width="182" src="<?php echo BASE_DIR; ?>/template/base/panier/bouton_finir-cmd.png" alt="finaliser la commande"/></div>
            <?php } ?>
        </div>
        <br>
		<div>
			<table id="table_packs">
                <tr><td></td></tr>
			</table><br>
			
			<?php // AVIS VERIFIES #7C7972 ?>
			<div style="margin:5px; padding:10px; background-color:#ffffff; border-top:1px dashed grey;">		
				<iframe id='AV_widget_iframe' frameBorder="0" width="100%" height="100%" src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2ahorizontal_index.html"></iframe>
			</div>
			<?php // End AVIS VERIFIES ?>
	
		</div>
    </div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>