<?php
/* Auteur : Paul
   Page pour les infos sur le recrutement*/
   
$nom_page = "recrutement";

require("includes/page_top.php");
require("includes/meta/recrutement.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">
	<div id="recrutement">
    	<div class="titre">Vous �tes int�ress� par notre entreprise ?</div>
        
        <div class="image_droite"></div>
        
        <div class="texte_intro">
        	Bas� � Montpellier (34), nous sommes r�guli�rement � la recherche de jeunes talents dans le domaine du webdesign, du graphisme, de l'illustration, et de la programmation.<br /><br />
            Nous somme aussi � la recherche de personnes travaillant comme freelance donc n'h�sitez pas � nous contacter, nous prendrons le temps d'�tudier attentivement chaque proposition.
        </div>
        
        <div class="offres_emploi">
        	<span class="sous_titre">Nos offres d'emploi :</span><br /><br />
            Nous n'avons pas de recrutement sp�cifique pour le moment !<br /><br />
            N'h�sitez pas � nous envoyer vos CV et lettre de motivation par email � cette adresse&nbsp;: <img src="template/base/mail.php" alt="" /><br /><br />
            Nous demandons � voir ce que vous savez faire donc les liens de sites ou des travaux que vous avez r�alis�s seront indispensables pour que nous puissions etudier votre candidature s�rieusement.
        </div>
        
        <div class="offres_stage">
        	<span class="sous_titre">Nos offres de stage :</span><br /><br />
            Aucune offre pr�cise n�est � pourvoir mais n�h�sitez pas � postuler de mani�re spontan�e !<br /><br />
            Nous sommes r�guli�rement enchant�s d�accueillir des stagiaires dans ces domaines :
			<ul class="liste_1">
            	<li>Graphisme</li>
                <li>Illustration</li>
                <li>Webmaster</li>
            </ul>
            <ul class="liste_2">
                <li>Marketing</li>
                <li>Programmation</li>
                <li>Comptabilit�</li>
            </ul>
            Envoyez vos demandes par e-mail � cette adresse : <img src="template/base/mail.php" alt="" />
        </div>
        
        <div style="clear:both;"></div>
        
    </div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>