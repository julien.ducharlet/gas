<?php
/* Page affichant les articles d'une marque */

$nom_page = "marque";
require("includes/page_top.php");

//verifie URL
$query_url= "SELECT manufacturers_id, manufacturers_name, manufacturers_url 
			FROM  ".TABLE_MANUFACTURERS." 
			WHERE manufacturers_id=". (int)$_REQUEST["marque"] ;
$res_url=tep_db_query($query_url);

if (tep_db_num_rows($res_url)!=1) { // Si l'URL n'existe pas on redirige sur une page 404
	redirect(404,url('erreur',array()));
} else { // Sinon
	$row_url = tep_db_fetch_array($res_url); // Resultat de la requete
	// On fabrique l'URL qu'on assigne a la variable $url_clean
	$url_clean = url("marques",array('id_marques' => (int)$_REQUEST["marque"], 'nom_marques' => $row_url["manufacturers_url"]));
	
	//################ POUR MOI
	//if ($_SESSION['customer_id']==3) echo $_SERVER['REQUEST_URI'];
	//if ($_SESSION['customer_id']==3) echo '<br>'.$url_clean;
	
	/*if ($_SERVER['REQUEST_URI'] != $url_clean) {
		redirect(301,$url_clean);
	}*/
	
	require("includes/meta/marques.php");
	require("includes/meta_head.php");
	require("includes/header.php");
	require("css/background_page_marques.php");
}
//date du jour 
$today = date("Y-m-d H:i:s"); 
?>

<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/produits_liste.js"></script>
<script type="text/javascript"> (tarteaucitron.job = tarteaucitron.job || []).push('youtube'); </script>
<script type="text/javascript"> (tarteaucitron.job = tarteaucitron.job || []).push('youtubeplaylist'); </script>


<div id="corps">
	<input type="hidden" id="base_dir" value="<?php echo BASE_DIR . "/"; ?>" />
	
    <div id="produits">
        <?php		
			$nb_articles_par_page = 60;
			
			if (!isset($_REQUEST["page"])){
				$page = 1;
			} else {
				$page = (int)$_REQUEST["page"];
			}
			
			$premier_article_page = ($page-1) * $nb_articles_par_page;
		
			$option_manufacturers = '';
			$manufacturers_image = '';
			$manufacturers_id = (isset($_GET['marque']) && !empty($_GET['marque'])) ? (int)$_GET['marque'] : 0;
			
			$i = 0;
			
			$query_url_manufacturers = 'SELECT 
										manufacturers_id, 
										manufacturers_url 
									FROM '. TABLE_MANUFACTURERS .' 
									WHERE manufacturers_id = "'. $_GET["marque"].'"';
			$query_url_manufacturers = tep_db_query($query_url_manufacturers);	
			$data_url_manufacturers = tep_db_fetch_array($query_url_manufacturers);
			//echo $data_url_manufacturers['manufacturers_url'];
			//$idm = $data_url_manufacturers['manufacturers_id'];
			
			$query_manufacturers = 'SELECT 
										manufacturers_id, 
										manufacturers_name, 
										manufacturers_image, 
										manufacturers_url 
									FROM '. TABLE_MANUFACTURERS .' 
									ORDER BY manufacturers_name';
			$query_manufacturers = tep_db_query($query_manufacturers );
			
			while($data = tep_db_fetch_array($query_manufacturers )) {
				
				$query_count = '	SELECT count(*) AS total 
									FROM '. TABLE_PRODUCTS .' p 
									WHERE products_status=1 AND 
									manufacturers_id='. $data['manufacturers_id'];
				$query_count = tep_db_query($query_count);
				
				$data_count = tep_db_fetch_array($query_count);
				
				if($data_count['total'] > 0) {
					
					if($manufacturers_id==0 && $i++==0) $manufacturers_id = $data['manufacturers_id'];
					
					$option_manufacturers .= ($data['manufacturers_id'] == $manufacturers_id) 
						? '<option value="'. $data['manufacturers_id'] .'" selected="selected">'. $data['manufacturers_name'] .' ('. $data_count['total'] .')</option>'
						: '<option value="'. $data['manufacturers_id'] .'">'. $data['manufacturers_name'] .' ('. $data_count['total'] .')</option>';
					
				}
				// MARQUE QUI DEFILE - On affiche les logo des marques qui ont plus de XX produits
				if($data_count['total'] > 10) {
					
					if ($manufacturers_id==0 && $i++==0) { $manufacturers_id = $data['manufacturers_id']; }
					
					$manufacturers_image .= '<a href="/gas/marques.php?di='.$data['manufacturers_id'].'" title="Les articles de la marque '.$data['manufacturers_name'].'">'. $data['manufacturers_name'] .'</a> -  ';
				}
			}
			
			$products_query = tep_db_query("SELECT 
											s.specials_new_products_price, 
											s.expires_date, 
											pd.products_id, 
											pd.products_name, 
											pd.products_url, 
											pd.balise_title_lien_image, 
											pd.balise_title_lien_texte,
											p.products_price, 
											p.products_bimage, 
											p.products_sort_order, 
											p.products_date_added, 
											p.products_manage_stock, 
											p.products_note, 
											r.rubrique_url, 
											r.rubrique_name, 
											r.rubrique_id,
											cd.categories_id, cd.categories_name, cd.categories_url,
											cd2.categories_id as id_marque, cd2.categories_url as url_marque, cd2.categories_name as nom_marque,
											ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1,
											s.specials_new_products_price, s.expires_date,
											pvf.prix_vente,
											m.manufacturers_url
										   
											FROM  " . TABLE_PRODUCTS . " p
											left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id
											left join " . TABLE_SPECIALS . " s on (s.products_id = p.products_id and (expires_date>NOW() or expires_date='0000-00-00 00:00:00'))
											left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash='0000-00-00'))),
											". TABLE_MANUFACTURERS ." m, 
											" . TABLE_PRODUCTS_DESCRIPTION . " pd,
											" . TABLE_CATEGORIES . " c, 
											" . TABLE_PRODUCTS_TO_CATEGORIES . " p2c,
											" . TABLE_CATEGORIES_DESCRIPTION . " cd,
											" . TABLE_CATEGORIES_DESCRIPTION . " cd2,
											" . TABLE_RAYON . " r,
											" . TABLE_CATEGORIES_RAYON . " cr 
											WHERE 
											p.products_status = '1' AND 
											p.products_id = pd.products_id AND 
											p.products_id = p2c.products_id AND 
											p2c.categories_id = c.categories_id AND cd.categories_id=c.categories_id AND 
											c.categories_id = cr.categories_id AND 
											cr.rubrique_id = r.rubrique_id AND 
											c.parent_id = cd2.categories_id AND p.manufacturers_id=m.manufacturers_id AND 
											m.manufacturers_id=". $manufacturers_id ." 
											GROUP BY pd.products_id
											ORDER BY products_date_added");
			
			$req_meta_manufacturers = " SELECT * FROM ". TABLE_MANUFACTURERS . " WHERE manufacturers_id= " .$manufacturers_id . "";
			$res_meta_manufacturers = tep_db_query($req_meta_manufacturers);
			$data = tep_db_fetch_array($res_meta_manufacturers );
		?>
        
        <div class="nbr_produits">
			<strong>Selectionnez une marque :</strong> <select onchange="document.location.href='?marque='+ this.options[this.options.selectedIndex].value"><?php echo $option_manufacturers; ?></select>
		</div>
		<? // $data['manufacturers_url'] ?>
 
        <div style="clear:both;"></div>	
		
        <div id="articles_images">
	        <table>
				<tr>
					<td style="padding:15px;">
						<h1>
							<font style="font-size:18px;">
								Boutique de la marque <?php echo $data['manufacturers_name']; ?>
							</font>
							<br>
							<?php if (tep_db_num_rows($products_query)>=2) { echo '('. tep_db_num_rows($products_query) . ' articles)'; } ?> 
						</h1>
					</td>
					<td width="150px">
						<img src="../gas/images/marques_pictures/<?php echo $data['manufacturers_image']; ?>">
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:justify;">
						<br>
						<?php echo $data['manufacturers_description']; ?>
						<br><br>
					</td>
				</tr>
			</table>
			<hr>
			<table>
    	        <tr>
                <?php
                    $i = 0; $j = 0;
					$nb_articles_ligne = 5;
					$est_affiche = true;
                    while($products_data = tep_db_fetch_array($products_query)){
						$j++;
						
						$prix = calculate_price_for_product($products_data);
						
						$est_affiche = ($j > $premier_article_page && $j <= $premier_article_page + $nb_articles_par_page);
						
						if ($est_affiche) {
							$i++;
							
							$id_rayon = $products_data['rubrique_id'];
							$nom_rayon = $products_data['rubrique_name'];
							$url_rayon = $products_data['rubrique_url'];
							$id_modele = $products_data['categories_id'];
							$nom_modele = $products_data['categories_name'];
							$url_modele = $products_data['categories_url'];
							$id_marque = $products_data['id_marque'];
							$nom_marque = $products_data['nom_marque'];
							$url_marque = $products_data['url_marque'];
														
							$products_name = bda_product_name_transform($products_data['products_name'], $id_modele);
							$products_url = bda_product_name_transform($products_data['products_url'], $id_modele);
							
							$url = url('article', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele, 'url_modele' => $url_modele, 'id_article' => $products_data['products_id'], 'url_article' => $products_url));
							
							?>

							<td class="article" <?php if ($i % $nb_articles_ligne == 1) echo 'style="border-left: none;"'; ?> onclick="location.href='<?php echo $url; ?>';">
								<div class="image_article">
									<a href="<?php echo $url; ?>" title="<?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele) . ' / Boutique ' . $data['manufacturers_name'];?>"><img src="<?php echo BASE_DIR . "/images/products_pictures/petite/" . $products_data['products_bimage']; ?>" alt="Vente <?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele) . ' / Magasin ' . $data['manufacturers_name'];?>" width="140px" height="140px"/></a>
									<?php if($prix['flash'] > 0){ ?>
										<img class="image_article_filigrane" src="<?php echo BASE_DIR . "/template/base/liste_produits/vente-flash.png"; ?>" alt="Vente Flash <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>" title="Vente flash sur <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele) . ' / Boutique ' . $data['manufacturers_name'];?>" />
									<?php } elseif ($products_data['products_manage_stock'] == 'destock_visible') { ?>
										<img class="image_article_filigrane" src="<?php echo BASE_DIR . "/template/base/liste_produits/destockage.png"; ?>" alt="destockage <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>" title="Fin de serie <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele) . ' / Boutique ' . $data['manufacturers_name'];?>" />
									<?php } elseif($prix['promo'] > 0){ ?>
										<img class="image_article_filigrane" src="<?php echo BASE_DIR . "/template/base/liste_produits/promotion.png"; ?>" alt="Promotion <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?>" title="Solde sur <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele) . ' / Boutique ' . $data['manufacturers_name'];?>" />
									<?php } ?>
								</div>
								<div class="nom_article">
									<a href="<?php echo $url; ?>" title="Acheter <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele) . ' / Magasin ' . $data['manufacturers_name'];?>">
										<?php echo $products_name; ?>
									</a>
								</div>
								<div class="prix_article">
									<?php
									if (show_price($prix) != 0) { 
										prix2img(show_price($prix), 'moyen');
									}
									?>
								</div>
								<span><?php if($prix['type']=='HT') echo 'Hors Taxes'; ?></span>
								<?php // AVIS VERIFIES										
								if ($products_data['products_note'] != 0 ) {
									$note = $products_data['products_note'];
									
									echo '<div text-align="center" style="padding-top:10px;">';
									// Fonction d'affichage des notes en image avec demi note
									echo avis_verifies_complet($note);
									echo '</div>';
								} 
								// END AVIS VERIFIES
								?>									
							</td>
							<?php 
							if ($i % $nb_articles_ligne == 0) {
								echo '</tr>';
								echo '<tr>';
							}
						}
                    }
                ?>
				</tr>
            </table>
        </div>
		
        <div style="clear:both;"></div>
        
        <div class="pagination">
            <?php
				$nb_articles = tep_db_num_rows($products_query);
				$nb_pages = ceil($nb_articles / $nb_articles_par_page);
				
				if ($nb_articles > $nb_articles_par_page) {
					if ($nb_pages >= 2) {
						$page_precedente=$page-1;
						$page_suivante=$page+1;
						if ($page!=1) {
							echo "<a href='" . $_SERVER['PHP_SELF'] . "?marque=". $manufacturers_id ."&page=". $page_precedente ."' title='Page pr�c�dente'><strong>&laquo;</strong></a>";
						}
						for ($i = 1; $i <= $nb_pages; $i++) {
							if ($i==$page) {
								echo "<a href='" . $_SERVER['PHP_SELF'] . "?marque=". $manufacturers_id ."&page=" . $i ."' title='page ".$i."' class='active'>" . $i . "</a>";
							} else {
								echo "<a href='" . $_SERVER['PHP_SELF'] . "?marque=". $manufacturers_id ."&page=" . $i ."' title='page ".$i."'><strong>" . $i . "</strong></a>";
							}
						}
						if ($page!=$nb_pages ) {
							echo "<a href='" . $_SERVER['PHP_SELF'] . "?marque=". $manufacturers_id ."&page=". $page_suivante ."' title='Page suivante'><strong>&raquo;</strong></a>";
						}
					}
				}
			?>
        </div>		
		
		<div style="padding-top:30px;"><strong>Toutes les marques disponibles chez Group Army Store :</strong></div>
        <div style="clear:both;"></div>
		<div class="zone_defilement_logo_marque" align="center"><?php include("includes/modules/defilement_logos_marques_marques.php"); ?></div>
		
        
	</div>
</div>

<!-- Div ou la requ�te AJAX va �crire l'affectation de variable de session -->
<div id="affect_session_var"></div>
<div id="overlay"></div>
<div id="lightbox"></div>

<?php 
require("includes/footer.php");
require("includes/page_bottom.php");
?>