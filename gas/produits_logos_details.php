<?php
/* Auteur : Paul
   Page pour les infos sur le recrutement*/
	 
// Pour CSS dynamique dans le Dossier CSS (pensez a cr�er un fichier CSS)
$nom_page = "tableau_des_tailles";

require("includes/page_top.php");
//require("includes/meta/.php"); // Pour ajouter des balises META particulieres a une page 
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">
	<div id="recrutement">
    	<div class="titre">Informations des logos</div>
        
        <!--<div class="image_droite"></div> -->
        
		<div class="texte_intro">
			
            <?php
           
			$i = 0;
			$query = 'select * from '. TABLE_PRODUCTS_LOGOS .' order by logo_name';
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				?>
                <div style="float:left; margin-right:15px; margin-bottom:25px;">
                	<img id="logo_<?php echo $data['logo_id']; ?>" src="<?php echo BASE_DIR .'/images/type_products_pictures/'. $data['logo_name']; ?>" alt="<?php echo $data['logo_name']; ?>" />
					
               	</div>
                <div style="margin:0 15px 25px; text-align:justify;"><?php echo $data['logo_hover']; ?></div><div style="clear:both;"></div>
                <?php
			}
			?>
            
        </div>
        
        <div style="clear:both;"></div>
        
    </div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>