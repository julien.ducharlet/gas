<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  if (!isset($HTTP_GET_VARS['products_id'])) {
    tep_redirect(tep_href_link(FILENAME_DEFAULT));
  }
  
  
  //from front.php
  $nrReviews->page = (int)$HTTP_POST_VARS['p'];
  $nrReviews->sorting = $HTTP_POST_VARS['sort'];
  $maxPages = $nrReviews->getNewMaxPage();
  echo '<div style="display:none" maxPages="'.$maxPages.'">'; echo '</div>';
  include_once DIR_NET_REVIEWS.'pagination.php';
?>

