<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../../includes/configure_css.php");
?>

/* Feuille de style des fichiers : devis_details.php, order_detail.php */

#block_info{
	background-color:<?php echo COULEUR_24; ?>;
	float:left;
	width:300px;
	height:170px;
	margin-right:35px;
}

#block_info_facturation{
	background-color:<?php echo COULEUR_26; ?>; 
	float:left; 
	width:300px; 
	height:170px;
}

#block_info_livraison{
	background-color:<?php echo COULEUR_24; ?>;
	float:right; 
	width:300px;
	height:170px;
}

.block_bordure_violette{
	width:10%;
	height:100%;
	background-color:<?php echo COULEUR_6; ?>;
	float:left;
}

.block_bordure_verte{
	width:10%;
	height:100%;
	background-color:<?php echo COULEUR_27; ?>;
	float:left;
}

.petit_titre{
	margin-bottom:5px;
	margin-top:30px; 
	float:left;
	color:<?php echo COULEUR_7; ?>;
	font-size:12px;
}

.commande_detail_barre_info{
	color:<?php echo COULEUR_16; ?>;
	background-color:<?php echo COULEUR_25; ?>;
	padding-top:5px;
	padding-bottom:5px;
}