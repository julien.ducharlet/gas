<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../../includes/configure_css.php");
?>

/* Feuille de style des pade d'inscription */

.titre_box-contenu {
	width:550px; 
	margin-left:10px;
	padding:10px;
	text-align:left;
	font-weight: bold;
}

.box-contenu {
	width:550px;
	padding: 0 15px 15px 15px;
	background-color: #E1DEDD; /* BDA : Gris clair + */
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
}

.box-rgpd {
	width:550px;
	padding:10px 10px 0px 10px; 
	font-size:11px; 
	text-align:left;
}

/* Zone de gauche qui d'�crit les champs */
.conteneur_desc_item {
	width:210px;
	float:left;
	text-align:right;
}

.item_i {
	height:20px;
	margin-top:10px;
    line-height:23px;
}

.item_i_right {
	height:25px;
	margin-top:10px;
}

/* Zone de droite avec les champs */
.conteneur_composant_item {
	width:320px;
	float:right;
	text-align:left;
}

.conteneur_composant_item2 {
	float:right;
	text-align:justify;
}

#block_address {
	padding:20px 5px 10px 5px;
}

.ligne_info {
	background-color:#DF4E02;
	height:20px;
	margin-bottom:10px;
	text-align:left;
	padding:10px 5px 10px 10px;
}

