<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../../includes/configure_css.php");
?>

#corps_inscription {
		width:990px;
		background-color:<?php echo COULEUR_16; ?>;
		color:<?php echo COULEUR_10; ?>;
		padding-top:15px;
}

#type_client{ height:280px; }

#titre {
	margin-left:30px;
} 

#titre h1{
	float:left;
	font-size:16px;
	color: #666666; 
}

#block_img_particulier{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/01-inscription-particulier.jpg);
	width:450px; 
	height:250px; 
	margin: 5px 0 20px 30px;
	float:left;
	display:block;
}

#block_img_professionnel{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/02-inscription-professionnel.jpg);
	width:450px; 
	height:250px;
	margin: 5px 30px 20px 0;
	float: right;
}

#block_img_administration {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/03-inscription-administration.jpg);
	width:450px; 
	height:250px; 
	margin: 5px 0 20px 30px; 
	float:left;
	display:block;
}

#block_img_association {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/04-inscription-association.jpg);
	width:450px; 
	height:250px;	
	margin: 5px 30px 20px 0;
	float: right;
}

#block_img_entreprise{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/05-inscription-entreprise.jpg);
	width:450px; 
	height:250px; 
	margin: 5px 0 30px 30px; 
	float:left;
	display:block;
}

#block_img_revendeur{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/06-inscription-revendeur.jpg);
	width:450px; 
	height:250px;
	margin: 5px 30px 30px 0;
	float: right;
}
