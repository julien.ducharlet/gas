<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../../includes/configure_css.php");
?>

/* Feuille de style des fichiers : devis.php, electronic_purse.php, order.php */

#info_com_gene{
	margin-top:15px;
	margin-bottom:15px;
	float:left;
}

#commande_barre_info{
	color:<?php echo COULEUR_16; ?>;
	/*height:22px;*/
	background-color:<?php echo COULEUR_2; ?>;
	padding-top:5px;
	padding-bottom:5px;
}

.item_num_com{
	 float: left;
	 margin-left:10px; 
	 width:100px;
}

.item_date_com{
	float: left;
	/*margin-left:30px;*/
	width:180px;
}

.item_paiement_com{
	float: left; 
	/*margin-left:10px;*/
	width:150px;
}

.item_montant_com{
	float: left;
	/*margin-left:10px;*/
	width:110px;
}

.item_statut_comOK{
	color:<?php echo COULEUR_20; ?>;
}

.item_statut_comNO{
	color:<?php echo COULEUR_9; ?>;
}

.item_statut_com{
	float: left;
	/*margin-left:30px;*/
	width:200px;
}

.item_facture_com{
	 float: left;
	 /*margin-left:30px;*/
	 width:55px;
}

.item_suivi_com{
	 float: left;
	 /*margin-left:5px;*/
	 width:145px;
}

.item_suivi_com2{
	 float: left;
	 /*margin-left:5px;*/
	 width:145px;
}

.ligne_commade_info1{
    padding-top:10px;
    padding-bottom:10px;
    border-bottom: 1px solid <?php echo COULEUR_2; ?>;
    padding-bottom:inherit;
    background-color:<?php echo COULEUR_14; ?>;
}

.ligne_commade_info1 a{
    color:<?php echo COULEUR_8; ?>;
    font-weight:bold;
}

.ligne_commade_info1 a:hover{
    color:<?php echo COULEUR_8; ?>;
    font-weight:bold;
    text-decoration:none;
}

.ligne_commade_info2{
    padding-top:10px;
    padding-bottom:10px;
    border-bottom: 1px solid <?php echo COULEUR_2; ?>;
    padding-bottom:inherit;
    background-color:<?php echo COULEUR_13; ?>;
}

.ligne_commade_info2 a{
    color:<?php echo COULEUR_8; ?>;
    font-weight:bold;
}

.ligne_commade_info2 a:hover{
    color:<?php echo COULEUR_8; ?>;
    font-weight:bold;
    text-decoration:none;
}