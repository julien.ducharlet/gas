<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../../includes/configure_css.php");
?>

/*formulaire pour le log*/
#login{
	float:left;
}

#login h1 {	
	margin-top:10px;
	margin-bottom:5px;
	color: <?php echo COULEUR_7; ?>;
	font-size:13px;
	margin-left:20px;
}

.login_form {
	height:50px;
	margin-left:20px;
	margin-right:20px;
	background-color: <?php echo COULEUR_24; ?>;
	border-top: 1px solid <?php echo COULEUR_1; ?>;
}

.login_form a {
	text-decoration: none;
	color: <?php echo COULEUR_1; ?>;
}

.login_form a:hover {
	text-decoration: underline;
}

.login_form_text input{
	background-color:<?php echo COULEUR_23; ?>;
	border-color:<?php echo COULEUR_1; ?>;
}

#bt_log{
	margin-top:10px;
	margin-left:5px;
	float:left;
}

#erreur_log{
	width:200px;
	height:20px;
}

#corps_login {
		width:990px;
		background-color:<?php echo COULEUR_16; ?>;
		color:<?php echo COULEUR_10; ?>;
}

#info_adr{
	margin-top:15px;
	margin-left:15px;
	margin-right:5px;
	margin-bottom:5px;
	float:left;
}

#input_adr{
	margin-top:10px;
	float:left;
}

#info_mdp{
	margin-top:15px;
	margin-left:15px;
	margin-right:5px;
	float:left;
}

#input_mdp{
	margin-top:10px;
	float:left;
}

#oubli_pass{
	margin-left:25px;
	float:left;
}

#oubli_pass_close{
	display:none;
	margin-left:25px;
	float:left;
}

/*formulaire pour l'oubli*/

#formulaire_oubli{
	display:none;
	margin-left:20px;
	margin-right:20px;
	background-color:<?php echo COULEUR_4; ?>; 
	border-bottom: 1px solid <?php echo COULEUR_1; ?>;
	height:110px; 
}

#info_pass{
	width:60%; 
	height:120px; 
	float:left;
}

#info_pass a{
	color:<?php echo COULEUR_8; ?>;
	font-weight:bold;
	text-decoration:none;
}

#img_oubli{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/icone_compte.png);
	width:63px; 
	height:90px;  
	margin-top:10px; 
	margin-left:15px; 
	/*margin-right:5px;*/
	float:left; 
	background-repeat: no-repeat;
}

#info_oubli{
	width:450px;
	margin-left:14px;
	margin-bottom:10px;
	margin-top:20px; 
	text-align:left;
	float: left;
	text-align:justify;	
}

#formu{
	width:40%; 
	height:120px; 
	float:right;
}

#field{
	margin-right:20px;
}

#field_oubli{
	margin-top:15px;
	border-color:<?php echo COULEUR_16; ?>;
}

#field_oubli LEGEND {
	text-align:center;
}

#info_adr_oubli{
	float:left;
	margin-top:5px;
	margin-left:5px; 
	margin-right:10px;
}

#input_oubli{
	float:left;
}

#input_oubli input{
	background-color:<?php echo COULEUR_23; ?>;
	border-color:<?php echo COULEUR_1; ?>;
}

#erreur{
	width:350px;
	height:20px;
	font-weight:bold;
}

/*block pour image type client*/

#type_client{
	height:280px;
}

#block_img_part{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/image_particulier.jpg);
	width:450px; 
	height:241px; 
	margin-top:5px;
	margin-bottom:20px;
	margin-left:20px; 
	float:left;
	display:block;
}

#block_img_pro{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/image_pro.jpg);
	width:450px; 
	height:241px;
	margin-top:5px;
	margin-bottom:20px;
	margin-right:20px;
	float: right;
}

#block_img_particulier{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/01-inscription-particulier.jpg);
	width:450px; 
	height:250px; 
	margin-top:5px;
	margin-bottom:20px;
	margin-left:30px; 
	float:left;
	display:block;
}

#block_img_professionnel{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/02-inscription-professionnel.jpg);
	width:450px; 
	height:250px;
	margin-top:5px;
	margin-bottom:20px;
	margin-right:30px;
	float: right;
}

#block_img_administration {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/03-inscription-administration.jpg);
	width:450px; 
	height:250px; 
	margin-top:5px;
	margin-bottom:20px;
	margin-left:30px; 
	float:left;
	display:block;
}

#block_img_association {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/04-inscription-association.jpg);
	width:450px; 
	height:250px;
	margin-top:5px;
	margin-bottom:20px;
	margin-right:30px;
	float: right;
}

#block_img_entreprise{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/05-inscription-entreprise.jpg);
	width:450px; 
	height:250px; 
	margin-top:5px;
	margin-bottom:20px;
	margin-left:30px; 
	float:left;
	display:block;
}

#block_img_revendeur{
	background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/06-inscription-revendeur.jpg);
	width:450px; 
	height:250px;
	margin-top:5px;
	margin-bottom:20px;
	margin-right:30px;
	float: right;
}


#info_nouv_compte{
	margin-top:10px;
	margin-left:20px;
 
}

#info_nouv_compte h1{
	float:left;
	font-size:16px;
	color: <?php echo COULEUR_7; ?>;
}

