<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../../includes/configure_css.php");
?>

#img_logout{
	float:left;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/logout/fond_deco_gauche.png); 
	width:377px;
	height:267px; 
	margin-top:20px; 
	margin-left:10px;
}

#conteneur_text_logout{
	float:right; 
	margin-top:20px; 
	margin-right:10px;
}

#text_logout{
	padding:0px 20px 0px 20px;
	text-align:left; 
	height:235px;
	color:<?php echo COULEUR_10; ?>;
	font-weight:normal;
}

#deconnexion{
		margin: 5px;
	padding: 0px;
	font-size: 0.9em;
}

#deconnexion .titre{
	width: 100%;
	font-weight: bold;
	text-align:left;
	font-size:1.4em;
	color: <?php echo COULEUR_7; ?>;
	border-bottom: 1px solid <?php echo COULEUR_1; ?>;
	padding-bottom: 2px;
}

.bordure_haut_verte_logout{
	width:528px;
	height:16px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/logout/fond_deco_top.png);
	background-color:<?php echo COULEUR_16; ?>;
	background-repeat: no-repeat;
}

.bordure_pixel_vert_logout{
	width:528px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/logout/fond_deco_in.png);
}

.bordure_bas_verte_logout{	
	width:528px;
	height:16px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/logout/fond_deco_bottom.png);
	background-color:<?php echo COULEUR_16; ?>;
	background-repeat: no-repeat;
}