<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../../includes/configure_css.php");
?>

#faq_no_select{
	background-color: #E9E9E9;
	height:60px;
	margin-bottom:10px;
	padding-top:50px;
}

#conteneur_faq{
	width: auto;
}

#conteneur_faq a{
	color:<?php echo COULEUR_11; ?>;
	text-decoration:none;
}

#conteneur_faq img{
	border:none;
}

#select_faq{
	height:10px;
	text-align:center;
	font-size:14px;
	font-weight:bold;
	margin-bottom:30px;
	color: #000000;
}

#qa_categ{
	background-color:<?php echo COULEUR_16; ?>; 
	width:300px;
	height:30px;
	float:left;
	text-align:left; 
	font-size:14px;
	font-weight:bold;
}

#imgs_deploy {
	background-color:<?php echo COULEUR_16; ?>;
	width:300px; 
	height:35px;
	float:right; 
	text-align:right;
}

.pixel {
	width:10px;
	height:10px;
}

.img_cat {
	float:left;
	margin-top:5px;
	margin-bottom:5px;
	margin-right:10px;
}

.desc_cat {
	float:left;
	width:160px;
	margin-right:30px;
	height:30px; 
	margin-top:5px;
	margin-bottom:5px;
	text-align:left; 
	padding-top:10px; 
	font-size:10px;
}

.cnt_quet {
	border: 1px dotted <?php echo COULEUR_10; ?>; 
	padding: 5px;
	background-color: #E9E9E9; <?php //echo COULEUR_15; ?> 
	margin-bottom: 5px; 
	text-align:left;
}

.cnt_quet2 {
	padding: 5px;
	margin-bottom: 5px;
	text-align:left;
}

.img_loupe {
	float:left;
	margin-right:10px;
}

.question_faq {
	 margin-top:5px;
}