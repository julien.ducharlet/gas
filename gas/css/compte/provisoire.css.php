<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../../includes/configure_css.php");
?>

/* Feuille de style des fichiers : account_edit.php, adress_book.php, create_account_part/pro.php, sponsorship.php */

p.titre_info{
	margin-top:-0.4em;
	margin-left:30px;
	float:left;
}

p.titre_info span
{
	padding:0 10px;
	background-color:<?php echo COULEUR_14; ?>;
	color:<?php echo COULEUR_1; ?>;
	position: absolute;
	border: 1px solid <?php echo COULEUR_2; ?>;
	width:225px;
	text-align:left;
}

.bordure_haut_verte{
	width:464px; 
	height:14px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_inscr_top.gif); 
	background-color:<?php echo COULEUR_14; ?>; 
	background-repeat: no-repeat;
}

.bordure_pixel_vert{
	width:464px; 
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_inscr_in.gif);
	background-color:<?php echo COULEUR_14; ?>;
}

.bordure_pixel_vert2{
	width:464px; 
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_inscr_in.gif);
	background-color:<?php echo COULEUR_16; ?>;
}

.bordure_bas_verte{
	width:464px; 
	height:14px; 
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_inscr_bottom.gif);
	background-color:<?php echo COULEUR_14; ?>;
	background-repeat: no-repeat;
}

.conteneur_desc_item{
	float:left;
	text-align:left;
}

.item_i{
	margin-top:10px;
	height:20px;
    line-height:23px;
}

.item_i_right{
	margin-top:10px;
    height:25px;
}

.conteneur_composant_item{
	float:right;
}

.conteneur_composant_item2{
	float:right;
	text-align:justify;
}

#block_address{
	padding:20px 5px 10px 5px;
}

.ligne_info{
	background-color:<?php echo COULEUR_22; ?>;
	height:20px;
	margin-bottom:10px;
	text-align:left;
	padding:10px 5px 10px 10px;
}