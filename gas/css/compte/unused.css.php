<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../../includes/configure_css.php");
?>

/* CSS Document */

/* Feuille de style contenant toutes les classes non utilisées dans les pages de la partie compte */

/* Aucune occurence */

	/* Login.css */
	
	.login_button {
		background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/boutons/bouton_connexion.jpg);
		width: 79px;
		height: 20px;
		border: none;
		vertical-align: bottom;
	}
	
	#fermer_oubli{
		width:18px; 
		height:18px;
		float:right; 
		margin-top:16px;
		background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/fleche_fermer_pwd2.png);
		background-color:#F0F;
	}
	
	.oubli_form_text input{
		background-color:<?php echo COULEUR_23; ?>;
		border-color:<?php echo COULEUR_1; ?>;
	}
	
	.oubli_button { 
		background-image: url(<?php echo BASE_DIR; ?>/template/base/compte/boutons/bouton_envoyer.jpg);
		width: 79px;
		height: 20px;
		border: none;
		vertical-align: bottom;
	}

	/* FAQ */
	
	#titre_faq{
		height:30px;
		text-align:left; 
		font-size:14px; 
		font-weight:bold;
		color:#09F;
	}
	
	/* Divers */
	
	#bande_rubrique{
		float:left;
		margin-bottom:15px;
	}
	
	/* Logout */
	
	#titre_logout {
		color: <?php echo COULEUR_7; ?>;
		font-weight: bold;
		text-align: left;
		font-size: 1.2em;
		margin-top:0px;
		margin-bottom:0px;
		height:325px;
	}

	#titre_logout .content {
		border-top: 1px solid <?php echo COULEUR_1; ?>;
		padding: 5px;
		height: 160px;
		border-bottom-width:0px;
		font-size:11px;
	}