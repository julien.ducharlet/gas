<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#panier {
	margin: 5px;
	padding: 0px;
	text-align: left;
	font-size: 12px;
}

#panier table {
	width: 100%;
	border-collapse: collapse;
	margin-top: 1px;
	margin-bottom: 1px;
	border-bottom: 2px solid <?php echo COULEUR_4; ?>;
}

#panier th {
	color: <?php echo COULEUR_16; ?>;
	background-color: <?php echo COULEUR_4; ?>; 
	line-height: 20px;
	border: 1px solid <?php echo COULEUR_16; ?>;
	text-align: center;
}

#panier td {
	border-top: 1px solid <?php echo COULEUR_18; ?>;
	line-height: 30px;
}

#panier td.modele {
	text-align: center;
}

#panier td.article {
	text-align: left;
	padding-left: 15px;
	line-height: 15px;
}

#panier td.article a {
	color: <?php echo COULEUR_10; ?>;
	font-weight: bold;
}

#panier td.article a .options {
	font-size: 0.8em;
	font-style: italic;
	font-weight: normal;
	text-decoration: none;
}

#panier td.image_article {
	text-align: center;
	width: 53px;
}

#panier td.prix {
	text-align: center;
	font-weight: bold;
	width: 95px;
}

#panier td.prix_remise {
	text-align: center;
	font-weight: bold;
	width: 130px;
	color: <?php echo COULEUR_7; ?>;
}

#panier td.quantite {
	text-align: center;
	width: 75px;
}

#panier td.quantite .quantite_2 {
	width: 55px;
	margin: auto;
}

#panier td.quantite .quantite_2 input {
	text-align: center;
	float: left;
}

#panier td.quantite .signe {
	float: right;
	padding-top: 1px;
	line-height: 10px;
}

#panier td.prix_total {
	text-align: center;
	font-weight: bold;
	width: 75px;
}

#panier td.supprimer {
	text-align: center;
	width: 25px;
	cursor: pointer;
}

#panier .titre {
	width: 100%;
	font-size: 1.3em;
	font-weight: bold;
	border-bottom: 1px solid <?php echo COULEUR_4; ?>;
	color: <?php echo COULEUR_4; ?>; <?php //echo COULEUR_7; ?>
	padding-bottom: 2px;
}

#panier #totaux {
	text-align: right;
	margin-top: 5px;
}

#panier #totaux .total {
	font-size: 13px;
	font-weight: bold;
}

#panier #totaux .total strong {
	font-size: 1.4em;
	font-weight: bolder;
	color: <?php echo COULEUR_7; ?>;
}

#panier .actions .sauver {
	width: 182px;
	height: 48px;
	margin: auto;
	margin-top: 15px;
	margin-bottom: 20px;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/panier/bouton_sauvegarder.png);
	cursor: pointer;
}

#panier .actions .poursuivre {
	width: 182px;
	height: 48px;
	/*float: right;*/
	margin: auto;
	margin-top: 15px;
	margin-bottom: 20px;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/panier/bouton_finir-cmd.png);
	cursor: pointer;
}

#table_fdp th {
	color: #FFFFFF;
	background-color: <?php echo COULEUR_4; ?>; <? //  ?>
	line-height: 20px;
	border: 1px solid <?php echo COULEUR_16; ?>;
	text-align: center;
}