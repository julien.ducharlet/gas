<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#marques {
	margin: 5px;
	padding: 0px;
}

#marques .telephone {
	height: 190px;
	width: 115px;
	margin: 10px 2px;
	padding: 2px 10px;
	float: left;
	text-align: center;
	cursor: pointer;
}

#marques .telephone a {
	color: #676767;
	text-decoration: none;
	font-weight: bold;
	font-size: 12px;
}

#marques .telephone {
	margin: 0px;
	padding: 10px;
}

#marques .telephone .nom_marque {
	font-size: 10px;
	font-weight: normal;
}

#marques .telephone:hover {
	background-color: #CCCCCC; <?php // echo COULEUR_5; ?>
}