<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#tel_plus_populaires {
	color: <?php echo COULEUR_7; ?>;
	font-weight: bold;
	text-align: left;
	margin: 20px 0px;
	font-size: 1.2em;
}

#tel_plus_populaires .telephone {
	margin: 0px;
	padding: 0px;
	font-size: 0.9em;
}

#tel_plus_populaires .telephone .nom_marque {
	font-size: 0.8em;
	font-weight: normal;
}

#tel_plus_populaires .liste {
	border-top: 1px solid <?php echo COULEUR_1; ?>;
	padding: 5px;
	color: <?php echo COULEUR_1; ?>;
   	border-bottom: 1px solid <?php echo COULEUR_1; ?>;
}

#tel_plus_populaires .liste .telephone {
	width: 100px;
	margin: 10px 2px;
	padding: 2px 0px;
	float: left;
	text-align: center;
	cursor: pointer;
}

#tel_plus_populaires .liste .telephone:hover {
	background-color: <?php echo COULEUR_5; ?>;
}

#tel_plus_populaires .liste img {
	border: none;
}

#tel_plus_populaires .liste a {
	color: <?php echo COULEUR_1; ?>;
}