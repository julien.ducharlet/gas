<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#rayons {
	margin: 10px;
	padding: 0px;
}

#rayons .recherche_marque a, #rayons .recherche_marque a:visited{
	text-decoration: none;
    color:#676767; 
}

#rayons .recherche_marque {
	color: #676767; <?php // echo COULEUR_7; ?>
	font-weight: bold;
	text-align: center;
	margin-top: 20px;
	margin-bottom: 20px;
	font-size: 19px;
}

#rayons .gauche {
	text-align: left;
}

#rayons .centre {
	text-align: center;
}

#rayons .recherche_marque .liste {	
	/*border: 1px solid #676767;*/
	/*background-color: #efefef; */<?php //echo COULEUR_16; ?>
	/*background-image: url(/gas/template/base/rayons/fond_marques.jpg);*/
	background-repeat: repeat-x;
	padding: 0px;	
}



#rayons .recherche_marque .liste img {
	border: none;
	margin: 7px;
}