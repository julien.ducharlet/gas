<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

.clear{ clear:both; }

#titre {
	width:100%; 
	height:20%;
	text-align:left;
	color:<?php echo COULEUR_17; ?>;
	font-size:16px;
}

#bande_rubrique_g {
	width:605px;
	height:50px;
	float:left;
	margin-bottom:30px;
}

#bande_image {
	width:55px;
	height:48px; 
	float:left;
}

#bande_titre {
	width:500px; 
	height:18px;
	float:left; 
	text-align:left;
	color:<?php echo COULEUR_17; ?>;
	font-size:16px;
	margin-top:5px;
}

#bande_sous_titre {
	width:500px;
	height:16px;
	float:left;
	text-align:left;
}

#bande_rubrique_d {
	float:right;
	width:182px; 
	text-align:right;
}





/* A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER */
/* A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER */
/* A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER */
/* A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER */
/* A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER */
/* A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER */
/* A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER */
/* A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER A SUPPRIMER */


.haut_erreur {
	width:970px;
	height:19px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_erreur_top.png);
	background-repeat: no-repeat;
}

.milieu_erreur {
	color:<?php echo COULEUR_16; ?>;
	width:970px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_erreur_in.png);
}

.bas_erreur {
	width:970px;
	height:19px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_erreur_bottom.png);
	background-repeat: no-repeat;
	margin-bottom:20px;
}

#bordure_mdp_haut_verte {
	background-color:<?php echo COULEUR_14; ?>; 
	width:970px;
	height:19px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_mdp_top_gif.gif);
	background-repeat: no-repeat;
	background-color:<?php echo COULEUR_14; ?>; 
}

#bordure_mdp_milieu_vert {
	background-color:<?php echo COULEUR_14; ?>; 
	width:970px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_mdp_in_gif.gif);
	
}

#bordure_mdp_bas_verte{
	
	background-color:<?php echo COULEUR_14; ?>; 
	width:970px;
	height:19px;
	background-image:url(<?php echo BASE_DIR; ?>/template/base/inscription/fond_mdp_bottom_gif.gif);
	background-repeat: no-repeat;
	margin-bottom:10px;
}