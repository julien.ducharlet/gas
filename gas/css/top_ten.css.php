<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#top_ten {
	width: 460px;
	float: right;
	color: <?php echo COULEUR_7; ?>;
	font-weight: bold;
	text-align: left;
	font-size: 1.2em;
}

#top_ten table {
	width: 100%;
	border-collapse: collapse;
	font-size: 0.8em;
}

#top_ten tr:hover {
	background-color: #CCCCCC ; <?php //echo COULEUR_22; ?>
}

#top_ten td {
	border-bottom: 1px solid <?php echo COULEUR_12; ?>;
	line-height: 20px
}

#top_ten .numero {
	width: 15px;
	color: <?php echo COULEUR_7; ?>;
	text-align: right;
}

#top_ten a {
	color: <?php echo COULEUR_18; ?>;
	text-decoration: none;
}

#last_ten {
	width: 460px;
	float: left;
	color: <?php echo COULEUR_7; ?>;
	font-weight: bold;
	text-align: left;
	font-size: 1.2em;
}

#last_ten table {
	width: 100%;
	border-collapse: collapse;
	font-size: 0.8em;
}

#last_ten tr:hover {
	background-color: #CCCCCC ; <?php //echo COULEUR_5; ?>
}

#last_ten td {
	border-bottom: 1px solid <?php echo COULEUR_12; ?>;
	line-height: 20px;
}

#last_ten .numero {
	width: 15px;
	color: <?php echo COULEUR_7; ?>;
	text-align: right;
}

#last_ten a {
	color: #0188C6; <?php //echo COULEUR_1; ?>
	text-decoration: none;
}