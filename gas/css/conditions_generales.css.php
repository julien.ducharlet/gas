<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#conditions_generales {
	margin: 5px;
	padding: 0px;
	text-align: left;
}

#conditions_generales a {
	color: #0188C6; <?php //echo COULEUR_18; ?>
}

#conditions_generales .titre {
	width: 100%;
	font-size: 1.2em;
	font-weight: bold;
	border-bottom: 1px solid #474747;
	color: #474747; 
	padding-bottom: 2px;
}

#conditions_generales .coordonnees_1 {
	font-weight: bold;
	text-decoration: underline;
	float: left;
}

#conditions_generales .coordonnees_2 {
	float: left;
	margin-left: 10px;
}

#conditions_generales .articles {
	background-color: #BDBDBD;
	padding: 3px;
	margin-top: 20px;
	margin-bottom: 20px;
	font-weight: bolder;
}

#conditions_generales .sous_articles {
	text-decoration: underline;
}

#conditions_generales .centre {
	text-align: center;
}

#conditions_generales .souligne {
	text-decoration: underline;
}

#conditions_generales #sommaire {
	line-height: 20px;
	margin: 20px 10px;
}

#conditions_generales #sommaire .sommaire_1 {
	width: 50%;
	float: left;
	margin-bottom: 20px;
}

#conditions_generales #sommaire .sommaire_2 {
	width: 50%;
	float: right;
}

#conditions_generales #sommaire .som_sous_art {
	padding-left: 30px;
}