<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#qui_sommes_nous {
	margin: 5px;
	padding: 0px;
	text-align: left;
	font-size: 0.9em;
}

#qui_sommes_nous .titre {
	width: 100%;
	font-size: 1.3em;
	font-weight: bold;
	border-bottom: 1px solid #474747;
	color: <?php echo COULEUR_7; ?>; 
	padding-bottom: 2px;
}

#qui_sommes_nous .texte_presentation {
	width: 663px;
	height: 339px;
	float: left;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/qui_sommes_nous/fond_description.png);
	margin: 10px 0px;
	padding: 10px;
	
}

#qui_sommes_nous .image_droite_1 {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/qui_sommes_nous/image_stock.png);
	float: right;
	width: 253px;
	height: 171px;
	margin: 10px 0px;
}

#qui_sommes_nous .image_droite_2 {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/qui_sommes_nous/image_bureau.png);
	float: right;
	width: 253px;
	height: 171px;
	margin: 7px 0px;
}

#qui_sommes_nous .liens dt {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/qui_sommes_nous/puce_fleche.png);
	background-repeat: no-repeat;
	background-position: left center;
	padding-left: 10px;
	color: <?php echo COULEUR_1; ?>;
	font-weight: bold;
}

#qui_sommes_nous .liens dt a {
	color: <?php echo COULEUR_1; ?>;
	font-weight: bold;
	text-decoration: none;
}

#qui_sommes_nous .liens dd {
	margin-left: 20px;
	margin-bottom: 10px;
}