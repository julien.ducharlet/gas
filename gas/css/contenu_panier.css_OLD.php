<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#banniere .nb_articles_panier {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/banniere/header_cadre_panier.png);
	background-repeat: no-repeat;
	margin: 13px;
	float: right;
	width: 209px;
	height: 135px;
	cursor: pointer;
}

#banniere .nb_articles_panier .image_panier {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/banniere/header_icone_panier.png);
	background-repeat: no-repeat;
	float: left;
	margin: 5px;
	width: 68px;
	height: 96px;
}

#banniere .nb_articles_panier .titre {
	margin-top: 5px;
	color: <?php echo COULEUR_18; ?>;
	font-style: italic;
	font-weight: bold;
}

#banniere .nb_articles_panier .texte {
	margin-top: 10px;
	font-size: 0.9em;
	color: <?php echo COULEUR_18; ?>;
	font-style: italic;
	font-weight: normal;
}

#banniere .nb_articles_panier .texte strong {
	font-weight: bolder;
	font-size: 1.5em;
}

#banniere .nb_articles_panier .texte .aucun_article {
	font-size: 1.1em;
	font-weight: bold;
	padding-right: 5px;
}