<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#accueil {
	margin: 5px;
	padding: 0px;
	text-align: left;
}

#accueil .menu {
	float: left;
	width: 200px;
}

#accueil .menu_top {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/index/menu_accueil_top.png);
	width: 200px;
	height: 19px;
}

#accueil .menu_bottom {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/index/menu_accueil_bottom.png);
	width: 200px;
	height: 19px;
}

#accueil .menu_middle {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/index/menu_accueil_center.png);
	background-repeat: repeat-y;
}

#accueil .menu_liste {
	padding: 0px;
	margin: 0px;
	padding-left: 25px;
	margin-right: 25px;
	/*border-top: 1px dashed <?php echo COULEUR_10; ?>;*/
	font-weight: bold;
	font-size:10px;
	line-height: 20px;
	list-style-image: url(<?php echo BASE_DIR; ?>/template/base/index/puce.gif);
    list-style-position: inside;
}

#accueil .menu_liste li {
	border-bottom: 1px dashed <?php echo COULEUR_10; ?>;
}

#accueil .menu a {
	text-decoration: none;
	color: <?php echo COULEUR_7; ?>;
}

#accueil .menu a:hover {
	text-decoration: underline;
}

#accueil .bandeau {
	width: 740px;
	height: 200px;
	float: right;
	margin-bottom: 20px;
}

#accueil .texte_index {
	float: right;
	margin-bottom: 20px;
    margin-right: 10px;
    width: 710px;
    background-color: <?php echo COULEUR_24; ?>;
    border: 1px solid black;
    padding: 5px;
}

#accueil .zone_defilement_logo_marque {
	float: right;
	margin-bottom: 20px;
    margin-right: 10px;
    width: 710px;
    background-color: <?php echo COULEUR_16; ?>;
    border: 1px solid black;
    padding: 5px;
}

#accueil .texte_avisverifies {
	float: right;
    
    width: 100%; /* 710px */
    padding: 20px 0 20px 10px;
	border-top: 1px dashed #999999;
}

#accueil .top_ten_accueil {
	font-size: 0.8em;
	width: 740px;
	float: right;
}

#accueil .top_ten_accueil #last_ten {
	width: 48%;
	margin: 5px;
}

#accueil .top_ten_accueil #top_ten {
	width: 48%;
	margin: 5px;
}