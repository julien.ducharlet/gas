<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#infos_livraison {
	margin: 5px;
	padding: 0px;
	text-align: left;
	font-size: 0.9em;
	line-height: 14px;
}

#infos_livraison .titre {
	width: 100%;
	font-weight: bold;
	font-size: 1.4em;
	color: <?php echo COULEUR_7; ?>;
	border-bottom: 1px solid <?php echo COULEUR_1; ?>;
	padding-bottom: 5px;
	margin-bottom: 10px;
}

#infos_livraison .image_fourgonnette {
	float: right;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/infos_livraison/photo_voiture_poste.png);
	width: 381px;
	height: 294px;
}

#infos_livraison .zone_1 {
	float: left;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/infos_livraison/cadre_texte.png);
	padding: 10px;
	width: 534px;
	height: 285px;
}

#infos_livraison .zone_2_top {
	margin-top: 20px;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/infos_livraison/cadre_tableau_haut.png);
	width: 957px;
	height: 13px;
}

#infos_livraison .zone_2_corps {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/infos_livraison/cadre_tableau_millieu.png);
	background-repeat: repeat-y;
	text-align: center;
	width: 957px;
	padding: 0px 14px 0px 14px;
}

#infos_livraison .zone_2_bottom {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/infos_livraison/cadre_tableau_bas.png);
	width: 957px;
	height: 14px;
}

#infos_livraison .zone_2_corps table {
	width: 97%;
	margin-top: 10px;
	border-collapse: collapse;
}

#infos_livraison .zone_2_corps td {
	border: 1px solid <?php echo COULEUR_18; ?>;
}

#infos_livraison .zone_2_corps td.titre_tableau {
	text-align: center;
	font-weight: bold;
	width: 90px;
}
