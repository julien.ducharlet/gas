<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?> 

#vente_flash {
	margin: 5px;
	padding: 0px;
}

#vente_flash h2 {
	color:#738648;
	margin-top:20px;
	text-align:left;
    text-decoration:underline;
}

#vente_flash table {
	background-image:url("<?php echo BASE_DIR; ?>/template/base/ventes_flash/fond_ventes_flash.jpg");
    background-repeat:no-repeat;
	margin:5px 0;
    height:200px;
    width:960px;
}

#vente_flash table tr td{padding:0px;}

#vente_flash span.relief{
	color:#738648;
    font-weight:bold;
}


#vente_flash .texte_recherche {
	font-size: 1em;
	color: <?php echo COULEUR_1; ?>;
}

#vente_flash .valeur_recherche {
	font-size: 1.2em;
	color: <?php echo COULEUR_7; ?>;
}

#vente_flash .nom_produit {
    font-size:18px;
    font-weight:bold;
    margin-top:10px;
}

#vente_flash .couleur_flash {
	color: <?php echo COULEUR_1; ?>;
}

#vente_flash .prix_flash {
	color: <?php echo COULEUR_9; ?>;
}

#vente_flash .nbr_vente_flash {
	float: left;
	font-size: 0.9em;
	margin-top: 10px;
}

#vente_flash .type_affichage {
	float: right;
	line-height: 26px;
	width: 510px;
}

#vente_flash .type_affichage .texte{
	float: left;
	font-size: 0.9em;
}

#vente_flash .type_affichage #liste {
	width: 39px;
	height: 26px;
	float: right;
	cursor: pointer;
}

#vente_flash .type_affichage #icones {
	width: 39px;
	height: 26px;
	float: right;
	cursor: pointer;
}

#vente_flash .type_affichage #images {
	width: 39px;
	height: 26px;
	float: right;
	cursor: pointer;
}

#vente_flash table {
	width: 100%;
}

#vente_flash .fond_2 td {
	background-color: <?php echo COULEUR_6; ?>;
}

#vente_flash .fond_1 td {
	background-color: <?php echo COULEUR_24; ?>;
}


#vente_flash td {
	border-left: 1px solid <?php echo COULEUR_16; ?>;
}



