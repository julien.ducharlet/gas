<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>
/* Caractéristiques générales de la page */

body {
	width: 990px;
	margin: auto;
	padding: 0px;
	background-color: #9E9E9E; <?php //echo COULEUR_FOND_PAGE; ?>
	color: <?php echo COULEUR_16; ?>; 
	font: 12px Verdana,Arial,Helvetica,sans-serif;
}

span.obligatoire{
	color:#f75663;
	font-weight: bold;
}

input[type="text"] {
	margin:0px;
}

img {
	border: none;
}

img, div { behavior: url(/gas/includes/png_fix/iepngfix.htc) }
	
	
	/* Header de la page contenant les drapeaux et les liens */
	#header {
		height: 10px;
		width: 970px;
		margin: 0px;
	}

	
	.header_liens {
		width: 390px;
		text-align: right;
		float: right;
		padding: 12px 10px 5px 10px;
	}
	
	.header_liens a {
		color: <?php echo COULEUR_16; ?>;
	}


/* Sous-Header avec le menu de recherche et le numéro de téléphone */
#sous-header {
	width: 990px;
	height: 40px;
	/*background-color: #7C7972; */<?php // AVANT ?>
	background-color: #777777;  <?php // APRES ?>
	border-top: 2px solid #FFFFFF;
	border-bottom: 2px solid #FFFFFF;
}

.header_recherche {
	float: left;
	width: 320px; /* 400 */
	margin: 8px 0px 0px 5px;
}

.header_promos_news {
	float: left;
	width: 300px; 
	margin: 12px 0px 0px -25px;
}

.recherche_submit {
	width: 33px;
	height: 22px;
	border: none;
	margin-bottom: -6px;
}

#sous-header form {
	padding: 0px;
	margin: 0px;
}

	/* Bannière et logo */
	#background_banniere {
		position: absolute;
		width: 100%;
		left: 0px;
		height:162px;
		<? //background-image: url(/gas/template/base/background_header_01.jpg); ?>
		z-index: -5;
	}
	
	#banniere {
		width:990px;
		height:162px;
		max-height: 162px;
		background-color:#656565;
		background-image:url(/gas/template/base/background_header_03.jpg);
	}
	
	#banniere .banniere {
		float: left;
		margin-left: 35px; /* 60 */
		margin-top: 15px; /* 15 */
	}
	
	.logo {
		float: left;
		margin: 15px 0px 0px 13px;
	}
	
	/* Logo du site */

	#logo-gas {
		background-image: url(/gas/template/base/logo_group-army-store_03-N.png);
		width: 210px; /*182*/
		height: 130px;
	}

	#logo-gas:hover {
		background-image: url(/gas/template/base/logo_group-army-store_03-O.png);
	}


/* Menu et Sous-menus */
#div_menu {
	width:990px;
	height: 38px;
	background-image:url(/gas/template/base/onglet-01.jpg);
}

.menu-general {
	float: left;
	/*width: 141px;*/
	height: 38px;
	padding: 0px 20px;
	margin: 0px;
	list-style : none;
    border-right: 1px solid #98908E;
}

.pour_ie {
	float: left;
	width: 1px;
}

.menu-general:hover {
	background-image: url(/gas/template/base/onglet_over-01.jpg);
}

.menu-general a {
	display: block;
	margin-top: 8px;
	text-decoration: none;
	font-weight: bold;
	color: <?php echo COULEUR_16; ?>;
}

.fond_onglets {
	float: right;
	height: 38px;
	background-color: <?php echo COULEUR_10; ?>;
	background-image: url(/gas/template/base/background_menu.jpg);
}

.sous-menu {
	position: absolute;
	width: 200px;
	display: none;
	clear: both;
	margin: 15px 0px 0px -20px;
	padding: 0px;
	text-align: left;
	background-color: #979797; <?php //echo COULEUR_2; #9E9E9E ?>
	border: 1px solid #474747; <?php //echo COULEUR_25; ?>
	border-top: none;
	z-index: 9999;
}

.sous-menu a {
	border-bottom: 1px dashed #676767; <?php //echo COULEUR_3; #656565 ?>
	margin: 0px;
	padding: 5px 0px;
	font-size: 0.8em;
}

.sous-menu a:hover {
	background-color: #656565; <?php //echo COULEUR_3; ?>
}


/* Contenu général de la page */
	#corps {
		width:970px;
		background-color:<?php echo COULEUR_16; ?>;
		color:<?php echo COULEUR_10; ?>;
		padding: 10px;
	}
	
	#corps h1 {
		margin: 0px;
		font-size: 1em;
	}

/* Gestion de la pagination */

.pagination {
	display: inline-block;
}

.pagination a {
	/*border-radius: 5px;*/
	color: black;
	float: left;
	padding: 8px 16px;
	font-size: 14px;
	text-decoration: none;
	transition: background-color .3s;
	border: 1px solid #E9E9E9;
}

.pagination a.active {
	background-color: #0188C6;
	color: white;
	border: 1px solid #0188C6;
}

.pagination a:hover:not(.active) { background-color: #E9E9E9; }
	
/* Pied de page avec paiment sécurisé, etc... */

#page_bottom {
	background-color:#FFFFFF;
}

#footer {
	height:49px;
	margin: 0px;
    margin: auto;
	padding: 0px;
	background-color:#676767;
	background-image:url(/gas/template/base/background_footer_03.jpg);
}

#footer .bloc_texte {
	width: 940px;
    margin: auto;
}

#footer .texte_footer {
	color: #FFFFFF; 
	font-weight: bold;
	font-size: 1.3em;
	margin-top: 16px;
	margin-left: 10px;
	float: left;
}

#footer .icone_paiement {
	background-image:url(/gas/template/base/footer_icons/icone_cadenas.png);
	width: 42px;
	height: 42px;
	margin-left: 10px;
	margin-right: 30px;
	margin-top: 4px;
	float: left;
}

#footer .icone_satisfait {
	background-image:url(/gas/template/base/footer_icons/icone_euro.png);
	width: 42px;
	height: 42px;
	margin-left: 10px;
	margin-right: 30px;
	margin-top: 4px;
	float: left;
}

#footer .icone_expedition {
	background-image:url(/gas/template/base/footer_icons/icone_livraison.png);
	width: 42px;
	height: 42px;
	margin-left: 10px;
	margin-top: 4px;
	float: left;
}

#background_footer {
	position: absolute;
	left: 0px;
	width: 100%;
	height: 49px;
	<? // background-image: url(/gas/template/base/background_footer_01.jpg); ?>
	z-index: -5;
}

#footer img {
	vertical-align: bottom;
}

	/* Pied de page avec les liens et les logos des cartes */
	#footer_2 {
		width: 990px;
		height: 40px;
		background-color: <?php echo COULEUR_16; ?>;
		color: <?php echo COULEUR_10; ?>;
	}
	
	#footer_liens {
		margin-top: 12px;
		margin-left: 12px;
		float: left;
	}
	
	a.footer_liens {
		color: <?php echo COULEUR_11; ?>;
	}
	
	.icones_cartes {
		background-image:url(/gas/template/base/footer_icons/icone_cb.jpg);
        background-repeat: no-repeat;
		width: 146px; /*109*/
		height: 31px;
		margin-top: 2px;
		margin-right: 7px;
		float: right;
	}

#copyright {
	
	margin: 0px;
	color: #FFFFFF; 
    text-align: center;
	padding: 10px 0 10px 0;
	background-color: #979797;
}

#copyright a {
	color: #FFFFFF;
}

#referencement {
	text-align:justify; 
	font-size: 10px;
	color: <?php echo COULEUR_12; ?>;
	text-decoration: none;
	margin-top:10px;
}

#referencement  a {
	color: <?php echo COULEUR_12; ?>;
	text-decoration: none;
}

#fil {
	border-bottom: 1px dotted <?php echo COULEUR_13; ?>;
	text-align: left;
	padding: 5px;
	margin-bottom: 10px;
}

#fil a {
	color: <?php echo COULEUR_TEXTE_FIL_ARIANE; ?>;
	text-decoration: none;
}

#fil a:hover { text-decoration: underline; }

p.legend { margin-top:-0.8em; margin-left:10px; float:left; }
p.legend span { padding:0 10px; background-color:<?php echo COULEUR_16; ?>; position: absolute; }

div.fieldnotset { border: 1px solid #979797; }

.clear { clear:both; }

.mod_manque {
	height: 26px;
	line-height: 26px;
	/*background-image:url(/gas/template/base/liste_modeles/fond_notice.jpg);*/
    background-color: <?php echo COULEUR_25; ?>;
	text-align: center;
	color: <?php echo COULEUR_16; ?>;
	font-size: 1.2em;
	margin: 10px 0px;
}

.mod_manque a { color: <?php echo COULEUR_16; ?>; }

#cadre_promo { width: 650px; float: left; }
#cadre_recherche { width: 295px; float: right; padding: 0px; }
#cadre_recherche fieldset {	margin: 0px; }
#cadre_recherche select { margin: 5px; width: 270px; }

.pointer{cursor:pointer;}
.center{text-align:center;}

/* style des message d'erreur ou de validation */
.box-erreur {
	width:950px;
	color:#FFFFFF;
	font-weight: bold;
	margin:0 0 10px 0;
	padding: 10px 10px 10px 10px;
	background-color: #f75663; 	
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:7px;
}
.box-valide {
	width:950px;
	color:#ffffff;
	font-weight: bold;
	padding: 10px 10px 10px 10px;
	background-color: #5d9e29; 
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:7px;
}

/* Nouveau bouton dans l'inscription */
.button {
    display: inline-block;
    margin: 0.75rem;
    padding: 0.75rem 1.5rem;
    border: none;
    border-radius: 0.1875rem;
    outline: none;
    background-color: #0188c6; /* 709E6F tomato */
    color: white;
    font-family: inherit;
    font-size: 14px;
    font-weight: 600;
    line-height: 1.5rem;
    text-decoration: none;
    text-align: center;
    cursor: pointer;
    transition: all 150ms ease-out;
}

.button-suivi {
    display: inline-block;
    padding: 0.1rem 1.5rem;
    border: none;
    border-radius: 0.1875rem;
    outline: none;
    background-color: #0188c6;
    color: white;
    font-family: inherit;
    font-size: 12px;
    font-weight: 600;
    line-height: 1.5rem;
    text-decoration: none;
    text-align: center;
    cursor: pointer;
    transition: all 150ms ease-out;
}
