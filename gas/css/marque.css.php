<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php"); 
?>

#produits {
	margin: 5px;
	padding: 0px;
}

#produits .texte_recherche {
	font-size: 1em;
	color: <?php echo COULEUR_1; ?>;
}

#produits .valeur_recherche {
	font-size: 1.2em;
	color: <?php echo COULEUR_7; ?>;
}



#produits i {
	color: #76A96E;
}


#produits .nbr_produits {
	float: center;
	font-size: 1em;
	color: #FFFFFF;
	margin: 0px 0 10px 0;
	padding: 10px 0 10px 0;
	background-color: #676767;
}

#produits .type_affichage {
	float: right;
	line-height: 26px;
	width: 510px;
}

#produits .type_affichage .texte{
	float: left;
	font-size: 0.9em;
}

#produits .type_affichage #liste {
	width: 39px;
	height: 26px;
	float: right;
	cursor: pointer;
}

#produits .type_affichage #icones {
	width: 39px;
	height: 26px;
	float: right;
	cursor: pointer;
}

#produits .type_affichage #images {
	width: 39px;
	height: 26px;
	float: right;
	cursor: pointer;
}

#produits table {
	width: 100%;
	border-collapse: collapse;
}

#produits .fond_2 td {
	background-color: <?php echo COULEUR_6; ?>;
}

#produits .fond_1 td {
	background-color: <?php echo COULEUR_24; ?>;
}

#produits th {
	border: 1px dotted <?php echo COULEUR_10; ?>;
	border-top: none;
	border-left: none;
}

#produits th a{
	color: <?php echo COULEUR_10; ?>;
	text-decoration: none;
}

#produits th a:hover{
	text-decoration: underline;
}

#produits td {
	border-left: 1px solid <?php echo COULEUR_16; ?>;
	text-align: center;
}

#produits #articles_images {
	/*display: none;*/
	margin: 5px 0px;
	padding: 5px 0px;
	/*border-top: 2px solid #474747;*/ <?php // echo COULEUR_1; ?>
	/*border-bottom: 2px solid #474747; */<?php // echo COULEUR_1; ?>
}

#produits #articles_images td.article {
	width: 138px;
	padding: 10px;
	border-left: 1px dotted <?php echo COULEUR_10; ?>;
	border-bottom: 1px dotted <?php echo COULEUR_10; ?>;
	cursor: pointer;
    vertical-align: top;
}

#produits #articles_images .article .image_article img {
	border: none;
}

#produits #articles_images .article .image_article_filigrane {
	border: 1px solid black;
	margin-top: -100px;
}

#produits #articles_images .article .nom_article {
	color: <?php echo COULEUR_1; ?>;
	margin-top: 10px;
	margin-bottom: 10px;
    min-height:56px;
}

#produits #articles_images .article .nom_article a {
	color: #0188C6; <?php //echo COULEUR_1; ?>
	text-decoration: none;
}

#produits #articles_icones {
	display: none;
	border-top: 1px solid <?php echo COULEUR_1; ?>;
	border-bottom: 2px solid <?php echo COULEUR_1; ?>;
	margin: 5px 0px;
	padding: 5px 0px;
}

#produits #articles_icones .ajout_panier {
	width: 49px;
	cursor: pointer;
}

#produits #articles_icones .image_article {
	background-color: <?php echo COULEUR_16; ?>;
	width: 100px;
	height: 100px;
	text-align: center;
	border: 1px solid <?php echo COULEUR_10; ?>;
	border-left: none;
}

#produits #articles_icones .quantite {
	text-align: center;
	width: 80px;
}

#produits #articles_icones .prix {
	text-align: center;
	width: 80px;
}

#produits #articles_icones .nom {
	cursor: pointer;
}

#produits #articles_icones .nom a {
	color: <?php echo COULEUR_10; ?>;
	text-decoration: none;
	padding-left: 20px;
}

#produits #articles_liste {
	display: none;
	border-top: 1px solid <?php echo COULEUR_1; ?>;
	border-bottom: 2px solid <?php echo COULEUR_1; ?>;
	margin: 5px 0px;
	padding: 5px 0px;
}

#produits #articles_liste .icone_apercu {
	width: 30px;
	text-align: center;
	cursor: pointer;
}

#produits #articles_liste .td_image_article {
	border: none;
	height: 0px;
}

#produits #articles_liste .td_image_article .grande_image_article {
	width: 350px;
	height: 350px;
	background-color: <?php echo COULEUR_16; ?>;
	border: 1px dotted <?php echo COULEUR_10; ?>;
	display: none;
	position: absolute;
}

#produits #articles_liste .ajout_panier {
	width: 36px;
	cursor: pointer;
}

#produits #articles_liste .quantite {
	text-align: center;
	width: 80px;
}

#produits #articles_liste .prix {
	text-align: center;
	width: 80px;
}

#produits #articles_liste .nom {
	cursor: pointer;
}

#produits #articles_liste .nom a {
	color: <?php echo COULEUR_10; ?>;
	text-decoration: none;
	padding-left: 20px;
}

#produits .filtres {
	width: 924px;
	height: 65px;
	line-height: 65px;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/liste_produits/fond_filtres.png);
}

#produits .filtres select {
	margin-right: 30px;
}

#produits .titre_filtres {
	text-align: left;
	margin-bottom: 10px;
	margin-left: 30px;
}

#produits .titre_filtres strong {
	color: <?php echo COULEUR_7; ?>;
}