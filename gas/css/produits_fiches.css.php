<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

/* CSS de la fiche des articles */
#fiche_articles {
	text-align: left;
}

#nom_produit {
	font-weight: bold;
	font-size: 1.5em;
	text-align: left;
	padding-left: 10px;
	padding-top: 5px;
}

#disponibilite {
	font-weight: bold;
	color:#F00;
	font-size: 1em;
	text-align: center;
	background-color:#688970;
	padding-top: 10px;
}

#image_article {
	float: left;
	margin-left:2px;
    margin-right:5px;
    margin-top:5px;
    margin-bottom:5px;
    width: 350px;
}

#image_article img, #message_pro img {
	border: 1px solid <?php echo COULEUR_13; ?>;
}

#image_article .miniatures {
	width: 354px;
}

#image_article .miniatures img {
	float: left;
	margin: 2px;
}

#image_article .miniatures #image_video {
	float: right;
}

.miniatures {
	margin-top: 3px;
	float: left;
}

#description_article {
	width: 350px;
	height: 350px;
	border: 1px solid <?php echo COULEUR_13; ?>;
	float: left;
	margin-left:5px;
    margin-right:2px;
    margin-top:5px;
    margin-bottom:5px;
}

#description_article .marque {
	text-align: center;
	border-bottom: 1px dotted <?php echo COULEUR_13; ?>;
	padding: 0px;
}

#description_article .marque img {
	border: none;
    margin: 10px 0px;
}

#lien_description {
	margin: 5px 5px 5px;
    
}
#lien_pack {
	margin: 5px;
}
#lien_video {
	margin: 5px;
}

#actions_article {
	float: right;
	text-align: center;
	width: 240px;
}

#actions_article .ecotaxe {
	color: <?php echo COULEUR_20; ?>;
	font-weight: bold;
	font-size: 0.8em;
	margin: 0px;
	padding: 0px;
	margin-top: 3px;
}

#actions_article .ecotaxe img {
	float: left;
	margin: 0px;
	padding: 0px;
}

#actions_article .ecotaxe p {
	margin: 0px;
	padding: 0px;
	padding-top: 5px;
}

#actions_article .quantite {
	padding: 10px 5px;
    margin-top: 20px;
	height: 140px;
    width: 229px;
    background-image: url(<?php echo BASE_DIR; ?>/template/base/fiche_article/cadre_ajouter_au_panier-01.png);
	background-repeat: no-repeat;
}

#actions_article .qtte_texte {
	clear:both;
	color: #474747; <?php //echo COULEUR_1; ?>
	font-weight: bold;
	font-size: 1.2em;
    height:25px;
    margin-bottom:20px;
    margin-left: 5px;
    margin-right:17px;
    margin-top: 5px;
    
}

#actions_article .quantite input {
	float: right;
}

#message_pro {
	height: 44px;
	margin-top: -55px;
	margin-left: 365px;
}

#message_pro .image_pro {
	width: 40px;
	height: 44px;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/fiche_article/icone_pro.png);
	float: left;
}

#message_pro .texte_pro {
	text-align: left;
	font-size: 0.8em;
	padding-top: 5px;
}

#details_article #onglets {
	width: 970px;
	height: 40px;
	padding: 0px;
	margin: 0px;
	margin-top: 10px;
	font-weight: bold;
}

#details_article #onglets .avis {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/fiche_article/onglet_avis.gif);
	background-repeat: no-repeat;
	width: 176px;
	height: 40px;
}

#details_article #onglets .compatibilite {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/fiche_article/onglet_description.gif);
	background-repeat: no-repeat;
	width: 145px;
	height: 40px;
}

#details_article #onglets li {
	float: left;
	display: inline;
	margin: 0px;
}

#details_article #onglets li a {
	text-align: center;
	margin: 0px;
	padding: 0px;
	display: block;
	text-decoration: none;
}

#onglets li img:hover {
	filter:alpha(opacity=70);
	-moz-opacity: .7;
}

#details_article .content {
	border: 1px solid <?php echo COULEUR_12; ?>;
	background-color: <?php echo COULEUR_15; ?>;
	margin: 0px;
	margin-top: -1px;
	padding: 5px;
}

#details_article h1 {
	text-align: left;
	font-weight: bold;
	font-size: 1.1em;
}

#details_article p {
	text-align: left;
}

#details_article a.current {
	filter:alpha(opacity=100);
	-moz-opacity:1;
	-khtml-opacity: 1;
	opacity: 1;
}
#details_article a.ghost { 
	filter:alpha(opacity=70); <?php // defaut 40  ?>
	-moz-opacity:0.7; <?php // defaut 0.4  ?>
	-khtml-opacity: 0.7;
	opacity: 0.7;
}
#details_article .on {
	display: block;
}
#details_article .off {
	display: none;
}

#onglet_2 table {
	border-collapse: collapse;
}

#onglet_2 th {
	text-align: center;
	color: <?php echo COULEUR_7; ?>;
}

#onglet_2 td {
	border-top: 1px solid <?php echo COULEUR_1; ?>;
	padding: 5px;
}

#onglet_2 td.designation_pack {
	padding: 15px;
}

#onglet_2 td.designation_pack a {
	color: <?php echo COULEUR_18; ?>;
	text-decoration: none;
	font-weight: bolder;
}

#onglet_2 td.designation_pack a:hover {
	text-decoration: underline;
}

#onglet_2 td.prix_normal {
	width: 10%;
	text-align: center;
}

#onglet_2 td.prix_pack {
	width: 10%;
	text-align: center;
}

#onglet_2 td.economie {
	width: 9%;
	text-align: center;
}

#onglet_0 h2 {
	font-size: 1.2em;
}


.logo_marque {
	height: 75px;
	text-align: center;
	border-bottom: 1px dotted <?php echo COULEUR_13; ?>;
	padding: 0px;
}

.logo_marque img {
	border: none;
}

.description_marque {
	border: 1px dashed <?php echo COULEUR_10; ?>;
	margin: 10px;
	padding: 5px;
	height: 300px;
}

#plus_pack {
	margin: 10px;
	margin-bottom: 21px;
}

/* CSS Pour magicZoom */
/* CSS class for zoomed area */
.MagicZoomBigImageCont {
	border:			1px solid <?php echo COULEUR_19; ?>;
}

/* Header look and feel CSS class */
/* header is shown if "title" attribute is present in the <A> tag */
.MagicZoomHeader {
	font:			10px Tahoma, Verdana, Arial, sans-serif;
	color:			<?php echo COULEUR_16; ?>;
	background:		<?php echo COULEUR_19; ?>;
	text-align:     center !important; 
}


/* CSS class for small looking glass square under mouse */
.MagicZoomPup {
	border: 		1px solid #aaa;
	background: <?php echo COULEUR_1; ?>;
}

/* CSS style for loading animation box */
.MagicZoomLoading {
	text-align:		center;
	background: 	<?php echo COULEUR_16; ?>;
	color:			<?php echo COULEUR_11; ?>;
	border: 		1px solid <?php echo COULEUR_13; ?>;
	padding:		3px 3px 3px 3px !important;
	display: 		none; /* do not edit this line please */
}

/* CSS style for gif image in the loading animation box */
.MagicZoomLoading img {
	padding-top:	3px !important;
}


.article {
	width: 138px;
	padding: 10px;
	border-left: 1px dotted <?php echo COULEUR_10; ?>;
	cursor: pointer;
    vertical-align: top;
    
    text-align:center;
}

.article .nom_article {
	color: <?php echo COULEUR_1; ?>;
    font-size:12px;
	margin-top: 10px;
	margin-bottom: 10px;
    min-height:56px;
}

.article .nom_article a {
	color: #0188C6; <?php //echo COULEUR_1; ?>
	text-decoration: none;
}

/*ventes-flash*/
span.relief{
	color:#FF0000;
    font-weight:bold;
}
#compteur {
	font-size:9px;
}


