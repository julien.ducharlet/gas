<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#banniere .nb_articles_panier {
	margin: 15px 13px 13px 13px; 
	float: right;
	width: 210px; 
	cursor: pointer;
	border: 2px solid #FFFFFF; /* NEW */
	background: rgba(204, 204, 204, 0.2); 
}

/* TITRE PANIER */
#banniere .nb_articles_panier .titre {
	font-size: 1.3em;
	font-weight: bold;
	color:#FFFFFF;	 
	padding: 12px 0 12px 0;
	border-bottom: 2px solid #FFFFFF; 
}

/* TEXTE PANIER */
#banniere .nb_articles_panier .texte {
	font-size: 1em;
	color:#FFFFFF;
	font-weight: normal;
	padding: 12px 0 12px 0;
}

/* TEXTE RECHERCHE */
#banniere .nb_articles_panier .recherche {
	font-size: 1em;
	color:#FFFFFF;
	padding: 10px 0px 10px 0px;
	border-top: 2px solid #FFFFFF;
}

/* LOUPE BOUTON */
.new_recherche_submit {
	width: 22px;
	height: 22px;
	border: none;
	margin-bottom: -6px;
}
