<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>


/* - - - - - - - - - - - - - - - - - - - - -

Title : Lightbox CSS
Author : Kevin Hale
URL : https://particletree.com/features/lightbox-gone-wild/

Created : January 13, 2006
Modified : February 1, 2006

- - - - - - - - - - - - - - - - - - - - - */

#lightbox{
	display:none;
	position: absolute;
	top:230px; <?php // 50% ?>
	left:50%;
	z-index:9999;
	width:940px; <?php // 940px ?>
	/*height:180px;*/
	margin:-200px 0 0 -470px; <?php // -200px 0 0 -220px ?>
	border:3px solid #4B5F4F; <?php // #404042 gris?>
	background:#ECEAEA; <?php // #FDFCE9 ?>
	text-align:left;
	overflow:auto;
}
#lightbox[id]{
	position:fixed;
}

#overlay{
	display:none;
	position:absolute;
	top:0;
	left:0;
	width:100%;
	height:100%;
	z-index:5000;
	background-color:<?php echo COULEUR_10; ?>;
	-moz-opacity: 0.6;
	opacity:.60;
	filter: alpha(opacity=80);
}
#overlay[id]{
	position:fixed;
}

#lightbox.done #lbLoadMessage{
	display:none;
}
#lightbox.done #lbContent{
	display:block;
}
#lightbox.loading #lbContent{
	display:none;
}
#lightbox.loading #lbLoadMessage{
	display:block;
}


#lightbox {
	color: <?php echo COULEUR_10; ?>;
}

#lightbox h1 {
	color: <?php echo COULEUR_18; ?>;
	font-size: 1.2em;
	text-align: center;
}

#lightbox #lightbox_product p, #lightbox #lightbox_pack p {
	color: <?php echo COULEUR_10; ?>;
	font-weight: bolder;
	font-size: 1em;
	text-align: center;
	margin: 30px 0px;
}

#lightbox .images {
	text-align: center;
	margin-bottom: 5px;
}

#lightbox .images img {
	margin: 10px;
	cursor: pointer;
}

#lightbox #options {
	margin: 5px;
	border-bottom: 1px dotted <?php echo COULEUR_13; ?>;
	border-top: 1px dotted <?php echo COULEUR_13; ?>;
	padding-top: 10px;
	padding-bottom: 10px;
	font-weight: bold;
	text-align: center;
	background-color: <?php echo COULEUR_16; ?>;
}

#lightbox #options table{
	padding-top: 15px;
	margin: auto;
}

#lightbox #options td{
	color: #55582C; <?php // echo COULEUR_1; ?>
	font-weight: bold;
	text-align: left;
}

#lightbox_ajout_adresse {
	padding:10px;
}

#lightbox_ajout_adresse p {
	color: <?php echo COULEUR_10; ?>;
	font-weight: bolder;
	font-size: 1em;
	text-align: left;
	margin: 5px 0px 2px;
    float:left;
    min-width:70px;
}

#lightbox_ajout_adresse .row {
	margin:7px 0px;
}