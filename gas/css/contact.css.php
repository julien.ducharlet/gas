<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#contact {
	margin: 5px;
	padding: 0px;
}

#contact h1 {
	width: 100%;
	font-size: 1.2em;
	text-align: left;
	color: <?php echo COULEUR_7; ?>;
	border-bottom: 1px solid <?php echo COULEUR_4; ?>;
	margin-bottom: 10px;
	padding-bottom: 2px;
}

#contact #image_contact {
	float: left;
	height: 255px;
	width: 255px;
}

#contact #texte_contact {
	float: right;
	width: 675px;
	text-align: left;
}

#contact #texte_contact .stronger {
	font-weight: bold;
	color: <?php echo COULEUR_7; ?>;
}

#contact #texte_contact .souligne {
	text-decoration: underline;
}

#contact #formulaire_contact {
	width: 100%;
	height: 164px;
	width: 960px;
	margin-top: 20px;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/contact/fond_formulaire.png);
	background-repeat: no-repeat;
	text-align: left;
}

#contact #formulaire_contact .libelles {
	float: left;
	margin-top: 15px;
	margin-left: 15px;
	line-height: 30px;
	font-size: 0.9em;
}

#contact #formulaire_contact .coordonnees {
	float: left;
	margin-top: 10px;
	line-height: 31px;
	color: <?php echo COULEUR_7; ?>;
}

#contact #formulaire_contact .coordonnees input {
	border: 1px solid <?php echo COULEUR_4; ?>;
	font-size: 0.9em;
	margin: 6px;
}

#contact #formulaire_contact .destinataire {
	float: left;
	margin: 10px 25px;
	line-height: 15px;
	font-size: 0.9em;
}

#contact #formulaire_contact .destinataire strong {
	font-size: 1.3em;
}

#contact #formulaire_contact .message {
	float: right;
	margin-top: 15px;
	margin-right: 15px;
}

#contact #formulaire_contact .message textarea {
	border: 1px solid <?php echo COULEUR_4; ?>;
}

#contact #formulaire_contact .message select {
	width: 301px;
}

#contact #formulaire_contact .valider {
	height: 20px;
	margin: 0px 10px;
}

#contact #formulaire_contact .valider p {
	float: left;
	margin: 0px 5px;
	padding: 0px;
	padding-top: 10px;
	font-size: 0.8em;
	color: <?php echo COULEUR_7; ?>;
}

#contact #formulaire_contact .valider .info {
	color: <?php echo COULEUR_7; ?>;
}

#contact #formulaire_contact .valider .error {
	color: <?php echo COULEUR_9; ?>;
	font-weight: bolder;
	padding-left: 20px;
	padding-bottom: 3px;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/contact/icone_erreur.png);
	background-repeat: no-repeat;
}

#contact #formulaire_contact .valider .success {
	color: <?php echo COULEUR_18; ?>;
	padding-left: 20px;
	padding-bottom: 3px;
	background-image: url(<?php echo BASE_DIR; ?>/template/base/contact/icone_ok.png);
	background-repeat: no-repeat;
}

#contact #formulaire_contact .valider input {
	float: right;
	margin-right: 5px;
}

#contact #formulaire_contact .message_success {
	margin: auto;
	padding-top: 30px;
	text-align: center;
	color: <?php echo COULEUR_20; ?>;
	font-size: 1.5em;
}