<?php
header("Content-type: text/css; charset=iso-8859-1");
require("../includes/configure_css.php");
?>

#recrutement {
	margin: 5px;
	padding: 0px;
	text-align: left;
	font-size: 0.8em;
	line-height: 15px;
}

#recrutement .titre {
	width: 100%;
	font-weight: bold;
	font-size: 1.4em;
	color: <?php echo COULEUR_7; ?>;
	border-bottom: 1px solid <?php echo COULEUR_1; ?>;
	padding-bottom: 5px;
}

#recrutement .sous_titre {
	font-weight: bold;
	text-decoration: underline;
}

#recrutement .texte_intro {
	width: 496px;
	float: left;
	padding: 10px;
	margin-bottom: 10px;
}

#recrutement .offres_emploi {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/recrutement/fond_recrutement-emploi.png);
	background-repeat: no-repeat;
	padding: 10px;
	width: 496px;
	height: 149px;
	float: left;
	margin-bottom: 10px;
}

#recrutement .offres_stage {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/recrutement/fond_recrutement-stage.png);
	background-repeat: no-repeat;
	padding: 10px;
	width: 496px;
	height: 162px;
	float: left;
	line-height: 13px;
}

#recrutement .offres_stage .liste_1 {
	float: left;
	width: 200px;
}

#recrutement .offres_stage .liste_2 {
	float: right;
	width: 200px;
}

#recrutement .image_droite {
	background-image: url(<?php echo BASE_DIR; ?>/template/base/recrutement/image_pros.jpg);
	float: right;
	width: 408px;
	height: 335px;
}