<?php
/* Auteur : Paul 
   Affiche les conditions g�n�rales de vente*/

$nom_page = "conditions_generales";
   
require("includes/page_top.php");
require("includes/meta/conditions_generales.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">
	<div id="conditions_generales">
    	<div class="titre">Sommaire</div>
        
        <div id="sommaire">
            <div class="sommaire_1">
                <a href="#article_1">Article 1 : Champ d'application</a><br />
                <a href="#article_2">Article 2 : Commandes</a><br />
                <a href="#article_3">Article 3 : Disponibilit� des articles</a><br />
                <a href="#article_4">Article 4 : Prix</a><br />
                <a href="#article_5">Article 5 : Frais de port</a><br />
                <a href="#article_6">Article 6 : Paiement</a><br />
                <a href="#article_7">Article 7 : Livraison</a><br />
                <a href="#article_8">Article 8 : Garantie : Satisfait, �change ou rembours�</a><br />
            </div>
            <div class="sommaire_2">
                <a href="#article_9">Article 9 : Transfert de propri�t� - Transfert de Risques</a><br />
                <a href="#article_10">Article 10 : Droit d'acc�s et de rectification</a><br />
                <a class="som_sous_art" href="#article_101">Article 10.1 - D�ontologie</a><br />
                <a class="som_sous_art" href="#article_102">Article 10.2 - Les cookies</a><br />
                <a class="som_sous_art" href="#article_103">Article 10.3 - Vos donn�es personnelles</a><br />
                <a href="#article_11">Article 11 : Responsabilit�</a><br />
                <a href="#article_12">Article 12 : Droit Applicable</a><br />
                <a href="#article_13">Article 13 : Acceptation de l'acheteur</a><br />
            </div>
        </div>
        
        <div style="clear:both;"></div>
  
    	<div class="titre">CONDITIONS GENERALES DE VENTE</div>
        <p>Le site internet www.GeneralArmyStore.fr est exploit� par la SARL GENERALARMYSTORE immatricul� au RCS de ROMANS sous le num�ro 447513342 et dont voici ses coordonn�es :</p>
        
        <div class="coordonnees_1">
            Nom commercial :<br />
			Soci�t� :<br />
            Adresse :<br />
			<br />
			Code postal :<br />
			Ville :<br />
            Email :<br /> 
            T�l�phone :<br />
            Siret :<br />
            TVA Intracom :<br />
			<br />
			Resposable de la publication :<br />
			Correspondant informatique et libert�s :<br />
			<br />
			H�bergement du site : 
        </div>
        <div class="coordonnees_2">
  			www.GeneralArmyStore.fr<br />
			SARL GENERALARMYSTORE<br />
            Angle des rues Pasteur et Bajard<br />
			entr�e 5 rue H. Bajard<br />
			26260 <br />
			SAINT DONAT sur L'HERBASSE <br />
			info[at]generalarmystore.fr <br />
            <?php echo TEL_NORMAL; ?><br />
            447 513 342 00033 <br />
            FR92 447513342 <br />
			<br />
			Mr Boris DETOMA<br />
			Mr Boris DETOMA<br />
			<br />
			SAS Heberg-24 - 806 avenue des Plans - 06270 Villeneuve Loubet - Siren : 799409289
        </div>
        
        <div style="clear:both;"></div>
  	 
		<div class="articles" id="article_1">ARTICLE 1 : CHAMP D'APPLICATION</div>
		<p>Les pr�sentes conditions g�n�rales de vente s'appliquent � toutes les ventes conclues par le biais de ce site Internet ou �ffectu�es � l'adresse de la soci�t�.</p>

		<div class="articles" id="article_2">ARTICLE 2 : COMMANDES</div>
		<p>
        <span class="souligne">Vous pouvez passer vos commandes :</span><br /><br />
		- sur Internet : www.generalarmystore.fr<br />
        - par courrier : G�N�RAL ARMY STORE - 5 rue H. Bajard - 26260 SAINT DONAT sur L'HERBASSE - FRANCE<br />
		<br />
		<strong>Attention </strong>: le d�lai de traitement des commandes pass�es par courrier est plus cons�quent que celui des commandes pass�es directement sur le site Internet.
 		</p>
        
		<div class="articles" id="article_3">ARTICLE 3 : DISPONIBILITE DES ARTICLES</div>
		<p>
        Pour r�aliser notre vitrine et nos catalogues de vente, nous travaillons avec nos propres bases de donn�es, enrichies d'apr�s les informations communiqu�es par nos fournisseurs. Les �ventuelles ruptures de stocks ne sauraient en cons�quence nous �tre imput�es. Chaque client est inform� par courrier ou sur la facture des produits non disponibles.<br />
		<br />
		<strong>Important </strong>: Si vous nous passez une pr� commande, nous ne pourrons en aucun cas �tre tenus pour responsables d'un �ventuel retard dans les d�lais de livraison lesquels d�pendent de la disponibilit� chez nos fournisseurs.<br />
		<br />
        Des articles peuvent parfois �tre propos�s � la vente en pr� commande sur le site internet G�N�RAL ARMY STORE. Ils sont exp�di�s d�s leur r�ception dans notre entrep�t.<br /> 
		G�N�RAL ARMY STORE ne pourra �tre tenu responsable d'un �ventuel retard de commercialisation.  
        </p>
 
		<div class="articles" id="article_4">ARTICLE 4 : PRIX</div>
		<p>
        Les prix indiqu�s en euro sont r�put�s nets, hors frais de port. Ceux exprim�s en devises autres que l'euro sont donn�s � titre purement indicatif. Ils sont modifiables � tout moment sans pr�avis, sachant que les articles seront factur�s sur la base en vigueur � l'enregistrement de la commande.<br />
		<br />
        Toutes les commandes sont factur�es en euros et payables en euros. Pour toute livraison ext�rieure � la France M�tropolitaine, les �ventuels frais de douanes ou taxes locales restent � la charge du destinataire.
        </p>
 
		<div class="articles" id="article_5">ARTICLE 5 : FRAIS DE PORT</div>
		<p>
        Les frais de port comprennent une participation aux frais de  pr�paration et d'emballage plus les co�ts d'affranchissement. Ils sont  forfaitaires, compos�s d'une partie fixe et d'une partie en fonction du nombre d'articles command�s. Seuls, la zone g�ographique et le mode d'exp�dition choisi par vous-m�me en font varier le montant.<br />
		<br />
  		Pensez � regrouper tous vos articles sur une seule commande. Nous ne pourrons  regrouper deux commandes distinctes et les frais de port vous seront factur�s pour chacune d'elle. 
        </p>
 
		<div class="articles" id="article_6">ARTICLE 6 : PAIEMENT</div>
		<p>
        Vous disposez de plusieurs moyens de paiement offrant un maximum de garanties et de s�curit�.<br /><br />
		<span class="souligne">Vous pouvez r�gler :</span><br />
		<br />
		<strong>- Par CARTE BANCAIRE</strong> (CB, e-Carte Bleue, Maestro, Mastercard, Visa, Visa Electron) :<br />
		En indiquant directement dans les zones pr�vues � cet effet (saisie s�curis�e par cryptage SSL sur le module de paiement SystemPay de la CAISSE EPARGNE), le num�ro de votre carte sans espace entre les chiffres, sa date de validit� et le cryptogramme de s�curit� situ� au dos de la carte.<br />
		Ce syst�me est enti�rement s�curis� et b�n�ficie de la fonction <a href="https://fr.wikipedia.org/wiki/3-D_Secure" target="_blank">3DSecure</a>.<br />
        <strong>Attention</strong> : Pour toute commande sup�rieure � 350 Euros, obligation de paiement par virement bancaire. <br />
		<br />		
		<strong>- Par CHEQUE en Euros</strong>, compensable dans une banque fran�aise :<br />
		Etablir votre ch�que � l'ordre de G�N�RAL ARMY STORE et l'envoyer � l'adresse G�N�RAL ARMY STORE - 5 rue H. Bajard - 26260 SAINT DONAT sur L'HERBASSE - FRANCE.<br />
		L'encaissement du ch�que est r�alis� � la r�ception de celui-ci.<br />
		Il faut obligatoirement noter le num�ro de la commande au dos du ch�que pour �viter tout retard dans le traitement et l'exp�dition de votre commande.<br />
        <strong>Attention</strong> : Pour toute commande sup�rieure � 350 Euros, obligation de paiement par virement bancaire.<br />
		<br />			
		<strong>- Par VIREMENT </strong>:<br />
		Il faut obligatoirement que le num�ro de la commande soit indiqu� dans l'intitul� du virement pour �viter tout retard dans l'exp�dition de votre commande.<br />
		<br />			
		<strong>- Par MANDAT ADMINISTRATIF </strong>(R�serv� aux administrations) :<br />
		Il faut obligatoirement nous envoyer un bon de commande administratif, sur lequel doit figurer le num�ro du devis pr�alablement �tabli (sauf dans le cas d'une commande pass�e directement sur notre site Internet), pour valider la commande.<br />
		</p>		

		<div class="articles" id="article_7">ARTICLE 7 : LIVRAISON</div>
		<p>
        Que vous commandiez de France ou de l'�tranger, nous vous livrons � domicile. Le d�lai de livraison comprend le temps de pr�paration du colis plus le temps d'acheminement.<br />
		<br />
		Nos d�lais de livraison d�pendent de la disponibilit� et du lieu de stockage des articles choisis.<br />
		Toutefois, presque 90% des commandes de nos clients quittent notre entrep�t le jour m�me ou le lendemain de leur commande.<br />
		<br />
		Le d�lai de livraison pour la France M�tropolitaine (+ Corse + Monaco) est g�n�ralement compris entre 2 et 12 jours ouvr�s. Le d�lai moyen �tant de moins de 4 jours ouvr�s.<br />
		N�anmoins, ce d�lai ne constitue pas un d�lai de rigueur et nous ne pourrons pas voir notre responsabilit� engag�e en cas de retard de livraison ou de rupture de stock chez un fournisseur.<br />
		<br />
		Attention : si vous choisissez de r�gler par ch�que ou virement bancaire, la commande ne sera trait�e qu'� r�ception de votre r�glement et les d�lais courront en cons�quence. <br />
 		</p>
        
		<div class="articles" id="article_8">ARTICLE 8 : GARANTIE : SATISFAIT, ECHANGE OU REMBOURSE.</div>
		
		<p><u>- Qualit� produits :</u><br />
		<br />
		Les articles commercialis�s par G�N�RAL ARMY STORE sont neufs et garantis contre tout d�faut except� les quelques articles d'occasion dont l'�tat est mentionn� dans la description. Ils sont identiques � ceux du commerce traditionnel.<br />
		S'il arrivait qu'un article soit d�fectueux ou non conforme, nous nous engageons � vous l'�changer ou le rembourser, sous r�serve qu'il nous soit retourn� dans son emballage d'origine et accompagn� de la facture GENERAL ARMY STORE correspondante dans les 14 jours suivant la r�ception du colis � l'adresse :<br />
		<br >
		G�N�RAL ARMY STORE - 5 rue H. Bajard - 26260 SAINT DONAT sur L'HERBASSE - FRANCE<br />
		<br />
		Les frais de retour et de r�exp�dition restent int�gralement � votre charge et l'�change sera mis en oeuvre imm�diatement. En cas de remboursement, nous garantissons qu'il est effectu� sur votre Carte Bancaire, par Ch�que, ou sur votre Porte-Monnaie Virtuel (choix � pr�ciser) dans un d�lai inf�rieur ou �gal � 30 jours suivant la date de r�ception du colis retourn�. Aucun envoi en contre-remboursement ne sera accept�, quel qu'en soit le motif.<br />
		<br >
		<br />
		<u>- Service Apr�s Vente :</u><br />
		<br />
		Le site internet GENERAL ARMY STORE garantit les articles vendus contre les d�fauts de mati�re ou de fabrication pendant un an � compter de la date de livraison. Pour pouvoir b�n�ficier de la garantie sur vos achats, il convient imp�rativement de conserver la facture d'achat originale.<br />
		<br />
		Tout retour d'article au titre de la garantie pr�cit�e doit faire l'objet d'un accord pr�alable de GENERAL ARMY STORE. A cette fin, l'acheteur prendra contact avec le service apr�s-vente en formulant sa demande � l'adresse suivante : info[at]generalarmystore.fr<br />
		<br />
		<strong>Aucun retour ne sera accept� sans autorisation pr�alable de notre part.</strong><br />
		<br />
		La garantie consiste, selon notre choix, soit au remplacement, soit � l'�change, soit � la r�paration des produits, dont vous avez la charge de nous les faire parvenir � vos frais.<br />
		<br />
		Tout produit incomplet, ab�m�, endommag� et/ou dont l'emballage d'origine aura �t� d�t�rior�, ne sera ni repris ni �chang� dans le cadre de la garantie. Afin de conna�tre les d�marches � suivre concernant le service apr�s vente pour tout probl�me ou de panne sur un produit, contactez nous � cette adresse : info [@] generalarmystore.fr.<br />
		<br />
		En tout �tat de cause, les garanties ne couvrent pas : le remplacement des consommables, l'utilisation anormale et non conforme des produits, les d�fauts et leurs cons�quences dus � l'intervention de l'acheteur ou d'un r�parateur non agr�� par GENERAL ARMY STORE ou encore par une modification du produit non pr�vue ni sp�cifi�e par le vendeur ou le fabricant. Les d�fauts et leurs cons�quences li�es � l'utilisation non conforme � l'usage auquel le produit est destin� (utilisation professionnelle, collective), les produits faisant l'objet d'un contrat d'assistance et de maintenance sp�cifiques, les produits personnalis�s ou fabriqu�s sp�cifiquement � la demande expresse et exclusive du client.<br />
		<br />
		Si, � l'occasion d'un retour pour un disfonctionnement du mat�riel, le SAV de la soci�t� constate le bon fonctionnement du produit, le client assumera les frais de r�exp�dition de la marchandise ainsi qu'une facturation forfaitaire de 45 Euros � r�gler pr�alablement � la r�exp�dition. <br />
		<br />
		<br />
		<u>- Qualit� de service :</u><br />
		<br />
		Vous b�n�ficiez d'un droit de retour quelle qu'en soit la raison. Vous pouvez nous retourner un article dans les 14 jours suivant la r�ception de votre colis (dans son emballage cellophane d'origine imp�rativement et avec la facture correspondante). Les frais d'envoi et de retour restent alors � votre charge.<br />
		Attention : cette garantie ne s'applique pas aux produits imm�diatement reproductibles descell�s.<br />
		Votre article vous sera donc �chang� ou rembours� dans un d�lai inf�rieur ou �gal � 30 jours suivant la date de r�ception du colis retourn�, sous r�serve qu'il soit intact et sans trace d'utilisation.
        </p>
 
		<div class="articles" id="article_9">ARTICLE 9 : TRANSFERT DE PROPRIETE - TRANSFERT DE RISQUES</div>
		<p>
		Le transfert de propri�t� des produits 
        au profit de l'acqu�reur, ne sera r�alis� qu'apr�s 
        complet paiement du prix par ce dernier, et ce quelle que soit la date 
        de livraison desdits produits. <br />
		En revanche, le transfert des risques de perte et 
        de d�t�rioration des produits sera r�alis� 
        d�s livraison et r�ception desdits produits par l'Acqu�reur. 
		</p>
 
		<div class="articles" id="article_10">ARTICLE 10 : DROIT D'ACCES ET DE RECTIFICATION</div>
		
		<div class="sous_articles" id="article_101">10.1 - D�ontologie :</div>
		<p>Soucieuse de la protection de votre vie priv�e, G�N�RAL ARMY STORE traite toutes les informations vous concernant avec la plus stricte confidentialit�. Lors de vos achats, nous ne vous demandons que les informations indispensables pour un traitement de qualit� et un suivi attentif de votre commande. Ces donn�es saisies en lignes sont enregistr�es sur un serveur s�curis�. 
		</p><br />
		
		<div class="sous_articles" id="article_102">10.2 - Les cookies :</div>
		<p>
        Les cookies enregistrent certaines informations qui sont stock�es dans la m�moire de votre disque dur.<br />
		En aucun cas, les cookies ne contiennent des informations confidentielles comme votre nom, votre num�ro de carte, etc�<br />
		Ils permettent :<br />
		- de garder en m�moire certains �l�ments utilies � la navigation sur notre site comme les articles que vous aviez s�lectionn�s lors de vos pr�c�dentes visites ou les pages que vous aviez visit�es<br />
		- d'autoriser le fonctionnement dfe certaines fonctionnalit�s comme, par exemple, la lecture des vid�os de pr�sentation des produits<br />
		En outre, d�s votre arriv�e sur notre site, un message d'alerte vous demande si vous souhaitez accepter les cookies, ce que vous pouvez bien entendu accepter ou refuser.		
		</p><br />
		
		<div class="sous_articles" id="article_103">10.3 - Vos donn�es personnelles :</div>
		<p>		
		Conform�ment � la loi Informatique et Libert�s du 6 janvier 1978 et au r�glement europ�en sur la protection des donn�es (RGPD) entr� en vigueur le 25 mai 2018, vous disposez d'un droit d'acc�s et de rectification des donn�es vous concernant.<br />
		Par notre interm�diaire, outre les informatives relatives au suivi de vos commandes, vous pouvez �tre amen�s � recevoir par e-mail des informations relatives � nos offres. Si vous ne le souhaitez pas, il suffit de nous en informer (en nous indiquant vos nom, pr�nom, adresse et e-mail) en nous contactant :<br />
		<br />
		- Par mail : info[at]generalarmystore.fr<br />
		- Par courrier : G�N�RAL ARMY STORE - 5 rue H. Bajard - 26260 SAINT DONAT sur L'HERBASSE - FRANCE<br />
		<br />
		De la m�me fa�on, vous pouvez �galement nous demander la suppression d�finitive de vos donn�es personnelles, ce qui entrainera automatiquement la suppression de votre compte.<br />
		D�s lors que nous aurons proc�d� � sa suppression, vous ne pourrez plus acc�der � vos factures. Nous vous conseillons donc de les suvegarder avant d'effectuer votre demande.<br />
		Cependant, par obligation l�gale et pour des raisons fiscales et comptables, les �ventuelles commandes et factures pr�c�demment enregistr�es sur votre compte resteront stock�es sur nos serveurs informatiques pendant une dur�e incompressible de 10 ans malgr� la suppression du compte.<br />
		<br />
		Vos donn�es personnelles sont stock�es sur le serveur s�curis� de G�N�RAL ARMY STORE situ� � Sophia-Antipolis (France) et administr� par la soci�t� HEBERG-24.<br />
		Les donn�es bancaires sont stock�es sur les serveurs de la banque CAISSE EPARGNE (SystemPay) situ�s en France.
		</p>
 
		<div class="articles" id="article_11">ARTICLE 11 : RESPONSABILITE</div>
		<p>
        Les titres et descriptions pr�sents dans les bases de donn�es de G�N�RAL ARMY STORE ont �t� saisis d'apr�s les informations communiqu�es par les fabricants et les �diteurs.<br />
		G�N�RAL ARMY STORE n'est pas responsable du contenu des descriptions et ne pourra voir sa responsabilit� engag�e � l'�gard de l'acheteur. Les produits propos�s r�pondent � la l�gislation fran�aise en vigueur.<br />
		G�N�RAL ARMY STORE d�cline toute responsabilit� si l'article livr� ne respecte pas la l�gislation du pays de livraison.<br />
		G�N�RAL ARMY STORE se d�gage de toute responsabilit� en cas d'inex�cution du contrat due � un cas de force majeure (gr�ve, inondation, incendie...).<br />
		G�N�RAL ARMY STORE se d�gage de toute responsabilit� quant au contenu des sites sur lesquels des liens hypertextes peuvent renvoyer � partir de son propre site. 
		</p>
		
		<div class="articles" id="article_12">ARTICLE 12 : DROIT APPLICABLE</div>
		<p>
		Toutes les clauses figurant dans les pr�sentes conditions g�n�rales de vente, ainsi que toutes les op�rations d'achat et de vente qui y sont vis�es, seront soumises au droit fran�ais, les tribunaux comp�tents seront ceux de VALENCE (26 - Dr�me), tribunaux dont d�pend le si�ge social de G�N�RAL ARMY STORE o� est form� le contrat entre les parties. 
		</p>
		
		<div class="articles" id="article_13">ARTICLE 13 : ACCEPTATION DE L'ACHETEUR</div>
		<p>
        Les pr�sentes conditions g�n�rales de vente ainsi que les tarifs sont express�ment agr��s et accept�s par l'Acheteur, qui d�clare et reconna�t en avoir une parfaite connaissance, et renonce, de ce fait, � se 
        pr�valoir de tout document contradictoire et, notamment, ses propres conditions g�n�rales d'achat, l'acte d'achat entra�nant acceptation des pr�sentes conditions g�n�rales de vente.<br /><br />
		Les pr�sentes conditions sont modifiables � tout moment sans pr�avis et consultable sur cette page.
        </p>
    </div>
</div>

<?php
require("includes/footer.php"); 
require("includes/page_bottom.php");
?>