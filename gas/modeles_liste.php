<?php
/* Affiche les categories de second niveau pour une categorie de 1er niveau */

$nom_page = "modeles_liste";
require("includes/page_top.php");

switch($_SESSION['customers_type']) {
	case '1' :
		$query_custom_type = ' and categories_status_client=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
		break;
	case '2' :
		$query_custom_type = ' and categories_status_pro=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_pro=\'1\'';
		break;
	case '3' :
		$query_custom_type = ' and categories_status_rev=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_rev=\'1\'';
		break;
	case '4' :
		$query_custom_type = ' and categories_status_adm=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_adm=\'1\'';
		break;
	default :
		$query_custom_type = ' and categories_status_client=\'1\'';
		$query_rubrique_custom_type = ' and rayon_status_client=\'1\'';
		break;
}

$query_url="SELECT cd.categories_name, cd.categories_url, rub.rubrique_name, rub.rubrique_url, cd.categories_htc_title_tag, cd.categories_htc_desc_tag, cd.categories_htc_keywords_tag, cd.categories_baseline   
		FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
							 inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
							 inner join ".TABLE_RAYON." as rub on (rub.rubrique_id=cr.rubrique_id ". $query_rubrique_custom_type .")
		
		WHERE cd.categories_id='".$_REQUEST['marque']."' AND cr.rubrique_id='".$_REQUEST['rayon'] ."'
		AND c.categories_status = '1' ". $query_custom_type ."
								   ORDER BY c.sort_order, cd.categories_name ASC";
			
$res_url=tep_db_query($query_url);
if(tep_db_num_rows($res_url)!=1){
	redirect(404,url('erreur',array()));
} else {
	$row_url = tep_db_fetch_array($res_url);
	$baseline=$row_url["categories_baseline"];
	$url_clean = url("marque",array('id_rayon' => $_REQUEST["rayon"], 'nom_rayon' => $row_url["rubrique_url"], 'id_marque' => $_REQUEST["marque"], 'url_marque' => $row_url["categories_url"]));
	
	if ($_SERVER['REQUEST_URI']!=$url_clean) {
		redirect(301,$url_clean);
	}
	
	//################ POUR MOI
	//if ($_SESSION['customer_id']==3) echo $_SERVER['REQUEST_URI'];
	//if ($_SESSION['customer_id']==3) echo '<br>'.$url_clean.'<br>';
	//if ($session_customer_id==3) print_r($_REQUEST);
	
	require("includes/meta/modeles_liste.php");
	require("includes/meta_head.php");
	require("includes/header.php");
	require("css/background_pages.php");
}


//Permet d'initialiser le nombre de rayon par ligne (affichage)
//$nombre_family_par_ligne=8; // 9
?>



<div id="corps">
	<?php 
		// Affichage du fil d'ariane :
		// Le module se charge aussi de d�clarer et d'assigner une valeur aux variables $id_rayon, $nom_rayon, $id_marque, $nom_marque
		include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_fil_ariane.php"); 
	?>
    
	<div id="marques">
		<?php
        include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_recherche_avancee.php");
		
		// On r�cup�re les infos sur le dernier niveau de cat�gories
		$modele_query = tep_db_query("	SELECT 
											cd.categories_name, 
											cd.categories_url, 
											cd.categories_id, 
											cd.balise_title_lien_image, 
											cd.balise_title_lien_texte, 
											c.sort_order, 
											c.categories_image2, 
											categories_status_tous, 
											categories_status_client, 
											categories_status_rev, 
											categories_status_pro, 
											categories_status_adm, 
											categories_status_vip
										FROM 
											" . TABLE_CATEGORIES . " AS c INNER JOIN " . TABLE_CATEGORIES_DESCRIPTION . " AS cd ON c.categories_id=cd.categories_id
										WHERE 
											c.parent_id='" . $id_marque . "' and 
											c.categories_status='1' ". $query_custom_type ."
										ORDER BY c.sort_order, cd.categories_name");
		
		?>
       	<div class="mod_manque">S�lectionnez une des sous-cat�gories propos�es ci-dessous :</div>
        <?php
        if (tep_db_num_rows($modele_query) > 0) {
			
			$row = 0;
			//$taille = 0;
			$div_cat = '';
			
            while ($modele_data = tep_db_fetch_array($modele_query)) {					
					if (affiche_info(	$session_customer_id , 
										$session_customers_type, 
										$session_customers_vip,
										$modele_data['categories_status_tous'], 
										$modele_data['categories_status_client'], 
										$modele_data['categories_status_rev'], 
										$modele_data['categories_status_pro'], 
										$modele_data['categories_status_adm'], 
										$modele_data['categories_status_vip']
									)
								){
						$url = url('modele', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $modele_data['categories_id'], 'url_modele' => $modele_data['categories_url']));
						
						
						$pathToImage = BASE_DIR . '/images/categories_pictures/' . $modele_data['categories_image2'];
						$size = getImageProperty(ABSOLUTE_DIR . $pathToImage);
						
						$div_cat .= '<div class="telephone" style="max-width:'. $size['width'] .'px">';
							//$div_cat .= '<a href="'. $url .'" title="'. $modele_data['balise_title_lien_texte'].' '.$url_marque.' '.$modele_data['categories_name'] .'">';
							$div_cat .= '<a href="'. $url .'" title="'. $modele_data['balise_title_lien_image'].' - '. $row_url['categories_name'] .'">';
							
							//$div_cat .= '<img src="'. BASE_DIR .'/images/categories_pictures/'. $modele_data['categories_image2'] .'" alt="'. $modele_data['balise_title_lien_image'].' '.$url_marque.' '.$modele_data['categories_name'] .'" height="100px" style="border: solid 1px #979797; margin-bottom: 10px;"/>';
							$div_cat .= '<img src="'. BASE_DIR .'/images/categories_pictures/'. $modele_data['categories_image2'] .'" alt="'. $modele_data['balise_title_lien_image'].'" height="100px" style="border: solid 1px #979797; margin-bottom: 10px;"/>';
							$div_cat .= '</a><br />';
							//$div_cat .=  '<a href="'. $url .'" title="'. $modele_data['balise_title_lien_texte'].' '.$url_marque.' '.$modele_data['categories_name'] .'">';
							$div_cat .=  '<a href="'. $url .'" title="'. $modele_data['balise_title_lien_texte'].'">';
								$div_cat .= '<span class="nom_marque">'. $nom_marque .'</span>';
							$div_cat .= '<br>'. $modele_data['categories_name'] .'</a>';
						$div_cat .= '</div>';
						
						$row++;
					}
            }
        } else {
            echo "Aucun mod�le";
        }
        ?>
        
        <div class="contener" style="width: 960px; margin: auto;">
			<?php echo $div_cat; ?>
        	<div style="clear: both;"></div>
        </div>
        
    	<div class="mod_manque">&nbsp;</div>
	</div>
    
    <?php include(ABSOLUTE_DIR . BASE_DIR . "/includes/modules/mod_top_ten.php"); ?>
    
	
	<?php // AVIS VERIFIES #7C7972 ?>
	<div style="margin:20px 5px 5px 5px; padding:10px; background-color:#ffffff; border-top:1px dashed grey;">		
		<iframe id='AV_widget_iframe' frameBorder="0" width="100%" height="100%" src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2ahorizontal_index.html"></iframe>
	</div>
	<?php // End AVIS VERIFIES ?>
	
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>