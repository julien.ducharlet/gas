<?php
/* Page affichant la liste des nouveaux produits */

$nom_page = "nouveautes";
   
require("includes/page_top.php");
require("includes/meta/nouveautes.php");
require("includes/meta_head.php");
require("includes/header.php");

//date du jour 
$today = date("Y-m-d H:i:s");
?>
<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/produits_liste.js"></script>

<div id="corps">
	<input type="hidden" id="base_dir" value="<?php echo BASE_DIR . "/"; ?>" />
	    
    <div id="produits">
        <?php		
			$nb_articles_par_page = 60;
			
			if (!isset($_REQUEST["page"])){
				$page = 1;
			} else {
				$page = (int)$_REQUEST["page"];
			}
			
			$premier_article_page = ($page-1) * $nb_articles_par_page;

			$products_query = tep_db_query("SELECT p.products_price, p.products_bimage, p.products_sort_order, p.products_date_added,p.products_note,
										   pd.products_id, pd.products_name, pd.products_url, pd.balise_title_lien_image, pd.balise_title_lien_texte,
										   ppbq.part_price_by_1, ppbq.pro_price_by_1, ppbq.rev_price_by_1, ppbq.adm_price_by_1,
										   r.rubrique_url, r.rubrique_name, r.rubrique_id,
										   cd.categories_id, cd.categories_name, cd.categories_url,
										   cd2.categories_id as id_marque, cd2.categories_url as url_marque, cd2.categories_name as nom_marque,
										   s.specials_new_products_price, s.expires_date, pvf.prix_vente
											FROM  " . TABLE_PRODUCTS . " p
											left join ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." ppbq on p.products_id=ppbq.products_id
											left join " . TABLE_SPECIALS . " s on (s.products_id = p.products_id and (expires_date >NOW() or expires_date='0000-00-00 00:00:00'))
											left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash='0000-00-00'))),
											" . TABLE_PRODUCTS_DESCRIPTION . " pd,
											" . TABLE_CATEGORIES . " c,
											" . TABLE_PRODUCTS_TO_CATEGORIES . " p2c,
											" . TABLE_CATEGORIES_DESCRIPTION . " cd,
											" . TABLE_CATEGORIES_DESCRIPTION . " cd2,
											" . TABLE_RAYON . " r,
											" . TABLE_CATEGORIES_RAYON . " cr
											WHERE p.products_status = '1' and p.products_id = pd.products_id and p.products_id = p2c.products_id and p2c.categories_id = c.categories_id and cd.categories_id=c.categories_id and c.categories_id = cr.categories_id and cr.rubrique_id = r.rubrique_id and c.parent_id = cd2.categories_id 
											GROUP BY pd.products_id
											ORDER BY p.products_date_added DESC
											
											LIMIT 300");
		?>
        
		<div style="background-color:#676767; color:#FFFFFF; padding:10px;">
			<h1>D�couvrez d�s � pr�sent les  <strong><?php echo tep_db_num_rows($products_query); ?></strong> derni�res nouveaut�s que nous avons s�lectionn�es pour vous.</h1>
		</div>

        <div style="clear:both;"></div>
        
        <div id="articles_images">
	        <table>
    	        <tr>
                <?php
                    $i = 0; $j = 0;
					$nb_articles_ligne = 5;
					$est_affiche = true;
					
                    while($products_data = tep_db_fetch_array($products_query)){
						$j++;
						
						$prix = calculate_price_for_product($products_data);
						
						$est_affiche = ($j > $premier_article_page && $j <= $premier_article_page + $nb_articles_par_page);
												
						if ($est_affiche) {
							$i++;
							
							$id_rayon = $products_data['rubrique_id'];
							$nom_rayon = $products_data['rubrique_name'];
							$url_rayon = $products_data['rubrique_url'];
							$id_modele = $products_data['categories_id'];
							$nom_modele = $products_data['categories_name'];
							$url_modele = $products_data['categories_url'];
							$id_marque = $products_data['id_marque'];
							$nom_marque = $products_data['nom_marque'];
							$url_marque = $products_data['url_marque'];
														
							$products_name = bda_product_name_transform($products_data['products_name'], $id_modele);
							$products_url = bda_product_name_transform($products_data['products_url'], $id_modele);
							
							$url = url('article', array('id_rayon' => $id_rayon, 'nom_rayon' => $url_rayon, 'id_marque' => $id_marque, 'url_marque' => $url_marque, 'id_modele' => $id_modele, 'url_modele' => $url_modele, 'id_article' => $products_data['products_id'], 'url_article' => $products_url));
							?>

                                	<td class="article" <?php if ($i % $nb_articles_ligne == 1) echo 'style="border-left: none;"'; ?> onclick="location.href='<?php echo $url; ?>';">
                                        <div class="image_article">
                                            <a href="<?php echo $url; ?>" title="Nouveaut� <?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?> - Nouveau">
												<img src="<?php echo BASE_DIR . "/images/products_pictures/petite/" . $products_data['products_bimage']; ?>" alt="nouveau <?php echo bda_product_name_transform($products_data['balise_title_lien_image'], $id_modele);?> nouveaut�" width="140px" height="140px" />
											</a>
                                            
                                        </div>
                                        <div class="nom_article">
                                            <a href="<?php echo $url; ?>" title="Nouveaut� <?php echo bda_product_name_transform($products_data['balise_title_lien_texte'], $id_modele);?> - Nouveaux accessoires"><?php echo $products_name; ?></a>
											
                                        </div>
                                        <div class="prix_article">
                                            <?php 
											if (show_price($prix) != 0) { 
												prix2img(show_price($prix), 'moyen');
											}
											?>
                                        </div>
                                        <span><?php if($prix['type']=='HT') echo 'Hors Taxes'; ?></span>
										<?php // AVIS VERIFIES										
										if ($products_data['products_note'] != 0 ) {
											$note = $products_data['products_note'];
											
											echo '<div text-align="center" style="padding-top:10px;">';
											// Fonction d'affichage des notes en image avec demi note
											echo avis_verifies_complet($note);
											echo '</div>';
										} 
										// END AVIS VERIFIES
										?>
                                	</td>
								<?php 
									if ($i % $nb_articles_ligne == 0) {
										?>
                                			</tr><tr>
                                        <?php
									}
									
									
								?>
							<?php
						}
                    }
                
                ?>
				</tr>
            </table>
        </div>
        
        <div style="clear:both;"></div>
        
        <div class="pagination">
            <?php
				$nb_articles = tep_db_num_rows($products_query);
				$nb_pages = ceil($nb_articles / $nb_articles_par_page);
				
				if ($nb_articles > $nb_articles_par_page) {
					if ($nb_pages >= 2) {
						//echo "Pages disponibles : ";
						$page_precedente=$page-1;
						$page_suivante=$page+1;
						if ($page!=1) {
							echo "<a href='" . $_SERVER['PHP_SELF'] . "?page=". $page_precedente ."' title='Page pr�c�dente'><strong>&laquo;</strong></a>";
						}
						for ($i = 1; $i <= $nb_pages; $i++) {
							if ($i==$page) {
								echo "<a href='" . $_SERVER['PHP_SELF'] . "?page=" . $i ."' title='page ".$i."' class='active'>" . $i . "</a>";
							} else {
								echo "<a href='" . $_SERVER['PHP_SELF'] . "?page=" . $i ."' title='page ".$i."'><strong>" . $i . "</strong></a>";
							}
						}
						if ($page!=$nb_pages ) {
							echo "<a href='" . $_SERVER['PHP_SELF'] . "?page=". $page_suivante ."' title='Page suivante'><strong>&raquo;</strong></a>";
						}
						
					}
				}
			?>
        </div>
		
        
	</div>
</div>

<!-- Div ou la requ�te AJAX va �crire l'affectation de variable de session -->
<div id="affect_session_var"></div>
<div id="overlay"></div>
<div id="lightbox"></div>
<!-- On affiche la bonne div correspondant au type de vue choisie -->
<script type="text/javascript">change_type('<?php echo $_SESSION['type_vue']; ?>');</script>

<?php 
require("includes/footer.php");
require("includes/page_bottom.php");
?>