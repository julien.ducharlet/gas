<?php
/* Page : qui_sommes_nous.php */
   
$nom_page = "qui_sommes_nous";

require("includes/page_top.php");
require("includes/meta/qui_sommes_nous.php");
require("includes/meta_head.php");
require("includes/header.php");
require("includes/languages/french/qui_sommes_nous.php");
?>

<div id="corps">
	<div id="qui_sommes_nous">
        <div class="titre">Qui sommes-nous ?</div>
        
        <div class="image_droite_1"></div>
        
        <div class="texte_presentation">
			<?php echo TEXT_INFORMATION;?>
		</div>
        
        <div class="image_droite_2"></div>
        <div style="clear:both;"></div>
        
        <div class="liens">
	        <dl>
            	<dt><a href="faq.php">INFORMATIONS (FAQ)</a></dt>
                <dd>Les r�ponses aux questions les plus pos�es.</dd>
                <dt><a href="contact.php">SERVICE APRES-VENTE</a></dt>
                <dd>Si vous avez un probl�me avec un de nos produits, identifiez-vous sur le site et rendez-vous dans votre espace client � la section SAV.</dd>
                <dt><a href="contact.php">NOUS CONTACTER</a></dt>
                <dd>Tous les moyens de nous joindre.</dd>
                <dt><a href="conditions_generales.php">CONDITIONS DE VENTE</a></dt>
                <dd>&nbsp;</dd>
			</dl>
        </div>
	</div>
	<?php // AVIS VERIFIES #7C7972 ?>
	<div style="margin:5px; padding:10px; background-color:#ffffff; border-top:1px dashed grey;">		
		<iframe id='AV_widget_iframe' frameBorder="0" width="100%" height="100%" src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2ahorizontal_index.html"></iframe>
	</div>	
	<?php // End AVIS VERIFIES ?>
	
</div>

<?php require("includes/footer.php"); 
	  require("includes/page_bottom.php"); ?>