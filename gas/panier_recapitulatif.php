<?php 
/* Page permettant de faire un recapitulatif de la commande */ 
 
$nom_page = "panier"; 
$nb_redirect = 0; 
 
$session_remise_porte_monnaie = !empty($_SESSION['remise_porte_monnaie']) ? $_SESSION['remise_porte_monnaie'] : ''; 
 
 
require("includes/page_top.php"); 
unset($_SESSION['remise_panier']); 
 
// Si la session "mode_paiement" est vide on redirige vers la page du panier  
if (empty($_SESSION['mode_paiement'])) { 
	header("Location:panier_paiement.php"); 
} 
 
// Inclusion des fonctions d'affichage du panier 
require("includes/fonctions/fonctions_panier.php"); 
require("includes/meta/panier_recapitulatif.php"); 
require("includes/meta_head.php"); 
 
include("includes/fonctions/fonctions_coupon.php"); 
include("panier_traitement.php"); 
 
// si la session existe et que le champ de session "customers_id" n'est pas vide  
if (isset($_SESSION) && !empty($_SESSION['customer_id'])) {	 
	$req_redirect = tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS_BASKET." WHERE customers_id=". $_SESSION['customer_id']); 
	$nb_redirect = tep_db_num_rows($req_redirect); 
} 
 
if ($nb_redirect==0) {  
    echo '<meta http-equiv="refresh" content="0; URL=\'https://www.generalarmystore.fr/gas/compte/order.php\'">';  
} else { 
 
	if ((isset($_POST['commentaire_commande'])) && ($_POST['commentaire_commande']!='')) { 
		$_SESSION['commentaire_commande'] = $_POST['commentaire_commande']; 
	} 
	 
	if ((isset($_POST['panier_action'])) && ($_POST['panier_action']=='ok')) { 
		traitement_commande(); 
	} else { 
	 
		if (empty($_SESSION['customer_id']) || !isset($_SESSION['adresse_livraison'])) { 
			?> 
			<meta http-equiv="refresh" content="0; URL='<?php echo BASE_DIR; ?>/index.php'"> 
			<?php 
		} else { 
			require("includes/header.php"); 
 
		?> 
 
<?php 
// SEULEMENT POUR THIERRY 
/*if ($_SESSION['customer_id']==3) { 
	print_r($_SESSION); 
	echo '<br><br>'; 
	echo 'Customers_type : '; 
	echo $_SESSION['customers_type'];  
	echo '<br><br>'; 
}	 */
?> 
			 
			<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/panier_paiement.js"></script> 
			<div id="corps"> 
				<div id="panier"> 
					<div class="titre" style="margin-bottom:10px;">R&eacute;capitulatif de votre commande</div> 
					<div style="margin-bottom:10px;" >Le contenu de votre commande : <a href="panier.php">modifier</a></div> 
					<?php if(isset($_SESSION['parrainage_kdo']) && $_SESSION['parrainage_kdo']==1){?> 
                    <div style="margin-top:10px; margin-bottom:10px; font-size:16px; color:#8e2eaa;" > 
                    	<strong>Gr&acirc;ce au parrainage un cadeau vous sera livr&eacute; avec votre premi&egrave;re commande</strong> 
                    </div> 
                    <?php }?> 
				   <!-- Tableau contenant les articles choisis par le client --> 
				   <?php 
				   	 
					prix_livraison(); 
					 
					$panier_query = tep_db_query("SELECT cb.products_id, cb.customers_basket_quantity, cb.id_rayon 
												  FROM ".TABLE_CUSTOMERS_BASKET." as cb 
												  WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' AND cb.pack_id = ''"); 
					 $i = 0; 
					 $nb_articles = tep_db_num_rows($panier_query); 
					 if ($nb_articles > 0) { 
				    
				   ?> 
					<table id="table_articles"> 
						<thead> 
							<tr> 
								<th colspan="2">Articles</th> 
								<th>Prix Unitaire</th> 
								<th>Quantit&eacute;</th> 
								<th>Prix total</th> 
							</tr> 
						</thead> 
						<tbody> 
						<?php 
                            $articles_sous_total_HT = 0; 
                            $option_details = ""; 
							while($panier_data = tep_db_fetch_array($panier_query)){ 
								if(!empty($panier_data['products_id'])){ 
									$id_complet = $panier_data['products_id']; 
									$i++; 
									 
                                     
                                    if (isset($panier_data)) { 
								        $article_options = explode("{", $panier_data['products_id']); 
                                    } 
                                    if (isset($article_options[1])) { 
                                        $options = explode("}", $article_options[1]); 
                                    } 
                                    if ((isset($options[0])) && (isset($options[1]))) { 
								        $option_details = infos_options($options[0], $options[1]); 
                                    } 
									 
									// On r?cup?re les infos sur l'article 
									$article = explode("_", $article_options[0]); 
									$quantite = $panier_data['customers_basket_quantity']; 
									$article_details = infos_article($article, $quantite); 
									 
									// On r?cup?re des infos compl?mentaires 
									 
									$articles_sous_total_HT += $article_details['prix_ht']*$quantite; 
										 
										// On affiche les articles du panier 
										affiche_article_panier($nb_articles, $id_complet, $panier_data['id_rayon'], $article_details, $option_details, $quantite, $i, false, ""); 
									} 
								} 
							}  
						?> 
						</tbody> 
					</table> 
					 
					<?php 
					$pack_sous_total_HT = 0; 
					$packs_query = tep_db_query("SELECT cb.pack_id, cb.customers_basket_quantity, cb.id_rayon 
												FROM ".TABLE_CUSTOMERS_BASKET." as cb 
												WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' AND cb.pack_id <> ''"); 
					$nb_packs = tep_db_num_rows($packs_query); 
					if ($nb_packs > 0) { 
					?> 
						<!-- Tableau contenant les Packs choisis par le client --> 
						<table id="table_packs"> 
							<thead> 
								<tr> 
									<th colspan="2">Packs</th> 
									<th>Prix Normal</th> 
									<th>Prix du Pack</th> 
									<th>Economie R&eacute;alis&eacute;e</th> 
									<th>Quantit&eacute;</th> 
									<th>Prix total</th> 
								</tr> 
							</thead> 
							<tbody> 
						 
							<?php							 
							while($packs_data = tep_db_fetch_array($packs_query)){ 
								$id_complet = $packs_data['pack_id']; 
								$i++; 
								// On r?cup?re les infos sur les options 
								$pack_options = explode("{", $id_complet); 
								$options = explode("}", $pack_options[1]); 
								$option_details = infos_options($options[0], $options[1]); 
								// On r?cup?re les infos sur le pack 
								$pack = explode("_", $pack_options[0]); 
								$pack_details = info_pack($pack, $option_details); 
								// On r?cup?re des infos compl?mentaires 
								$quantite = $packs_data['customers_basket_quantity']; 
								$pack_sous_total_HT += $pack_details['prix_ht']*$quantite; 
								//On affiche le pack et la liste de ses articles 
								affiche_pack_panier($id_complet, $packs_data['id_rayon'], $pack_details, $quantite, $i, false); 
							} 
					} 
					?> 
							</tbody> 
						</table>      
						 
				<div id="totaux"> 
					<?php $sous_total_total = $articles_sous_total_HT + $pack_sous_total_HT; ?> 
					Sous-total HT : <strong><span id="ss_total_ht"><?php echo format_to_money($sous_total_total); ?></span> &euro;</strong><br /> 
					Frais de livraison HT : 
                    <strong><span id="tva"><?php if (taux_taxe()==1.2)  { echo format_to_money($_SESSION['frais_livraison']/1.2); 
					} else { echo format_to_money($_SESSION['frais_livraison']); } ?> 
                    </span> &euro;</strong><br /> 
					<?php 
                    $r0 = 0; 
                    $r1 = 0; 
                    $r2 = 0; 
					$remise_fdp = (taux_taxe()==1.2) 
						? round($_SESSION['frais_livraison']/1.2, 2) 
						: round($_SESSION['frais_livraison'], 2); 
					 
					if($_SESSION['mode_paiement']==4 ) { 
						 
						$somme_av = tep_db_query("SELECT customers_argent_virtuel FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id'].""); 
						$res_av = tep_db_fetch_array($somme_av); 
						 
						if(isset($_SESSION['coupon_affectation_remise'])  && !empty($_SESSION['coupon_affectation_remise'])) 
						{ 
							$r0 = (englob_coupon($_SESSION['code_promo'],format_to_money(sous_total_panier($_SESSION['customer_id'])))>97) 
								? $_SESSION['remise_coupon'] 
								: 0; 
						} 
						 
						$r1 = (($sous_total_total+$remise_fdp)<$res_av['customers_argent_virtuel'])  
							? ($sous_total_total+$remise_fdp-$r0) 
							: $res_av['customers_argent_virtuel']; 
						 
						 
					} else { 
						$r1 = $session_remise_porte_monnaie; 
					} 
					  
					if(isset($_SESSION['coupon_affectation_remise'])  && !empty($_SESSION['coupon_affectation_remise'])) { 
						 
						$r2 = (englob_coupon($_SESSION['code_promo'],format_to_money(sous_total_panier($_SESSION['customer_id'])))>97)  
							 
							? $_SESSION['remise_coupon'] 
							: 0; 
					} 
					  
					 
					$remise=round($r1+$r2,2); 
					if($remise!=0) 
					{ 
						echo 'remise HT : <strong><span id="ss_total_ht">'.format_to_money($remise).'</span> &euro;</strong><br />'; 
					} 
					?> 
					TVA 20% : <strong><span id="tva"> 
					<?php 
						if (taux_taxe() == 1.2) { 
							if((($sous_total_total-$remise+$remise_fdp)*0.2)>0) { 
								if ((isset ($_SESSION['coupon_frais_de_port'])) && ($_SESSION['coupon_frais_de_port']=='1')){  
									$tva_exacte = $sous_total_total * 0.2; 
									$tva = correction_tva($tva_exacte); 
									$tva = $tva['arrondie']; 
								} else { 
									$tva_exacte = (($sous_total_total-$remise) * 0.2)+($_SESSION['frais_livraison']-($_SESSION['frais_livraison']/1.2)); 
									$tva = correction_tva($tva_exacte); 
									$tva = $tva['arrondie']; 
								} 
							} else { 
								$tva=0; 
							} 
							 
							echo format_to_money($tva); 
						} else { 
							echo format_to_money(0); 
						} 
                    ?> 
					</span> &euro;</strong><br /> 
					 
					<span class="total"> 
                     
						<?php 			 
                        $res_av_customers_argent_virtuel = isset($res_av['customers_argent_virtuel']) ? $res_av['customers_argent_virtuel'] : 0; 
						//echo round($sous_total_total,3)."-".$remise."+".$remise_fdp."*". taux_taxe(); 
						//echo "<br />".($sous_total_total-$remise+$remise_fdp)*taux_taxe(); 
						if ((isset ($_SESSION['coupon_frais_de_port'])) && ($_SESSION['coupon_frais_de_port']=='1')){ 
						   $TOTAL=(($sous_total_total-$res_av_customers_argent_virtuel)+ $tva);  
						} 
						else $TOTAL=((round($sous_total_total,3)-$remise+$remise_fdp) + $tva); 
						 
						if($TOTAL>0) $Total=$TOTAL; else $Total=0; 
						 
						?> 
						Total TTC : <strong><span id="total"><?php echo format_to_money($Total); ?></span> &euro;</strong> 
					</span> 
				</div> 
				<?php 
                    $SESSION_remise_coupon = isset($_SESSION['remise_coupon']) ? $_SESSION['remise_coupon'] : 0; 
					$info_delivery=tep_db_query("SELECT *  
											   FROM ".TABLE_ADDRESS_BOOK."  
											   WHERE address_book_id=".$_SESSION['adresse_livraison'].""); 
					$res2=tep_db_fetch_array($info_delivery); 
					if(parametrage_affiche_prix($res2['entry_country_id'])==1.2) {	 
						$fdp= round($_SESSION['frais_livraison'],2); 
												 
						$remise_c=round($SESSION_remise_coupon,2); 
						$porte_monnaie=round(($remise-$SESSION_remise_coupon),2); 
						 
						if(taux_taxe()==1.2){$port=round($fdp/1.2,2);} else $port=round($fdp,2); 
						if($Total==0) $tva_fdp=0; 
						else $tva_fdp = round($fdp-($_SESSION['frais_livraison']/1.2),2); 
						//echo "<br/> tva frais de port : ".$tva_fdp; 
						$tva_total_p=round($tva,2); 
						$ss_total_p=round($sous_total_total,2); 
						$total=round($Total,2); 
						//target 
						$_SESSION['remise_panier_av']=$remise; 
						//target 
						 
						$_SESSION['remise_par_coupon']=$remise_c; 
						$_SESSION['remise_porte_monnaie']=$porte_monnaie; 
						$_SESSION['fdp_panier'] = $port; 
						$_SESSION['tvap_panier'] = $tva_fdp; 
						$_SESSION['ss_total_panier'] = $ss_total_p; 
						$_SESSION['tva_total_panier'] = $tva_total_p; 
						$_SESSION['total_panier'] =$total; 
					 
					} else {	 
												 
						$fdp= round($_SESSION['frais_livraison'],2); 
						 
						$remise_c=round($_SESSION['remise_coupon'],2); 
						$porte_monnaie=round(($remise-$_SESSION['remise_coupon']),2); 
						 
						if(taux_taxe()==1.2){$port=round($fdp/1.2,2);} else $port=round($fdp,2); 
						$tva_fdp = round(0,2); 
						$tva_total_p=round(0,2); 
						$ss_total_p=round($sous_total_total,2); 
						$total=round($Total,2); 
						 
						$_SESSION['remise_par_coupon']=$remise_c; 
						$_SESSION['remise_porte_monnaie']=$porte_monnaie; 
						$_SESSION['fdp_panier'] = $port; 
						$_SESSION['tvap_panier'] = $tva_fdp; 
						$_SESSION['ss_total_panier'] = $ss_total_p; 
						$_SESSION['tva_total_panier'] = $tva_total_p; 
						$_SESSION['total_panier'] =$total;	 
					} 
				?> 
				 
				<div class="titre" style="margin-bottom:10px;">Votre adresse de livraison</div> 
				<?php 
					$req=tep_db_query("SELECT * FROM ". TABLE_ADDRESS_BOOK ." WHERE address_book_id=".$_SESSION['adresse_livraison']); 
					$r=tep_db_fetch_array($req); 
					echo '<div>'; 
												echo strtoupper($r['entry_company']).'<br>'; 
												echo $r['entry_lastname'].' '.$r['entry_firstname'].'<br>'; 
												if (!empty($r['entry_street_address_3'])) { 
													echo $r['entry_street_address_3'].'<br>'; 
												} 
												if (!empty($r['entry_suburb'])) { 
													echo $r['entry_suburb'].'<br>'; 
												} 
												echo $r['entry_street_address'].'<br>'; 
												if (!empty($r['entry_street_address_4'])) { 
													echo $r['entry_street_address_4'].'<br>'; 
												} 
												echo $r['entry_postcode'].' '.$r['entry_city'].'<br>'; 
												$pays=tep_db_query("SELECT countries_name 
																		   FROM countries   
																		   WHERE countries_id =".$r['entry_country_id'].""); 
												$pays_r=tep_db_fetch_array($pays); 
												echo strtoupper($pays_r['countries_name']).''; 
												  
				 echo '<br><br><a href="panier_livraison.php">modifier l\'adresse</a>';								 
				 echo '</div>'; 
				?> 
				<?php 
				// SI CE N'EST PAS UN DEVIS ON AFFICHE  
				if ($_SESSION['mode_paiement']!=8) { 
				?> 
					<div class="titre" style="margin-bottom:10px; margin-top:20px;">M&eacute;thode de paiement</div> 
					<div> 
					<?php 
					 
						if($_SESSION['mode_paiement']==4 && $Total>0) { 
							echo '<span style="font-size:14px; font-weight:bold; color:#F00;"> Vous avez utilis&eacute; la totalit&eacute; de l\'argent disponible dans votre porte-monnaie virtuel.<br />Il vous faut maintenant choisir une m&eacute;thode de paiement pour payer le reste du montant de votre commande </span>'; 
						} else {  
							$moyen_p=tep_db_query("	SELECT *  
													FROM ".TABLE_MODULE_PAIEMENT."  
													WHERE paiement_id=".$_SESSION['mode_paiement']); 
										 
							$res_paiement=tep_db_fetch_array($moyen_p); 
							if ($res_paiement['paiement_id']!=8) {  
								echo 'Paiement par '.$res_paiement['paiement_nom']; 
							} else {  
								echo 'Cr?ation d\'un devis'; 
							} 
							echo ' <a href="panier_paiement.php">modifier</a>'; 
						} 
											 
					?> 
					</div> 
					<div style="margin-top:20px;"> 
					<?php 
					echo $res_paiement['paiement_details']; 
					 
					if($_SESSION['mode_paiement']==4 && $Total>0) { 
						 
						$i = 0; 
						$moyen_p = tep_db_query("SELECT * FROM ".TABLE_MODULE_PAIEMENT." WHERE paiement_visible='1' AND paiement_montant_max>".$TOTAL." ORDER BY paiement_ordre_affichage"); 
						 
						while($res_paiement=tep_db_fetch_array($moyen_p)) { 
							 
							if($res_paiement['paiement_id']==4){ 
								$somme_av=tep_db_query("SELECT customers_argent_virtuel FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['customer_id'].""); 
								$res_av=tep_db_fetch_array($somme_av); 
							} 
							 
							 
							 
							if($res_paiement['paiement_id']!=4) { 
								echo '<div style="';  
				 
								// Si mode de paiement == porte monnaie (ID4) ou Paypal (ID5) on ne les affichent pas 
								if ($res_paiement['paiement_id']==4 || $res_paiement['paiement_id']==5) echo ' display:none; '; 
								 
								// Si le client n'est pas une administration && que le mode de paiement est DEVIS (ID8) on n'affiche pas 
								if ($_SESSION['customers_type']!=4 && ($res_paiement['paiement_id']==8 || $res_paiement['paiement_id']==7))echo ' display:none; ';	 
								 
								echo 'height:50px; background-image:url(template/base/images_paiement/fond_moyen_de_paiement.gif); background-repeat:no-repeat; margin-top:10px;"> 
										   
									  <div style="float:left; width:250px; text-decoration:underline;  height:30px; color:#8e2eaa; font-size:12px; margin-left:15px; margin-top:15px; "><strong>Paiement par '.$res_paiement['paiement_nom'].'</strong>'; 
									  echo'</div>'; 
									   
									  echo '<div style="float:left; margin-top:18px;  font-size:11px;  color:#788403;"> 
										  '; 
										  if($res_av['customers_argent_virtuel']>0 && $res_paiement['paiement_id']==4){ 
											  echo "Utilisez les <span style='color:#df4e02;'>".format_to_money($res_av['customers_argent_virtuel'])." &euro;</span> que vous avez dans votre porte monnaie virtuel"; 
										  } 
										  else echo $res_paiement['paiement_description']; 
										   
										  echo' 
									  </div> 
									   
									   
									  <div style="float:right; margin-top:15px; margin-right:25px;"> 
										  <input '; 
										  if((isset($_SESSION['mode_paiement']) && !empty($_SESSION['mode_paiement'])) && $_SESSION['mode_paiement']==$res_paiement['paiement_id'])  
											 {echo'checked="checked"';} 
											  
										 echo ' type="radio" id="paiement'.$i.'" name="supri" onclick="paiement('.$res_paiement['paiement_id'].');" /> 
									 </div> 
									 
									 <div style="float:right; margin-top:5px; margin-right:10px;"> 
										<img src="template/base/images_paiement/'.$res_paiement['paiement_img'].'" /> 
									 </div> 
									   
							  </div>'; 
							  $i++; 
							} 
						} 
						echo'<input type="hidden" id="compteur" name="compt" value="'.$i.'" />'; 
					} 
					 
					?> 
					</div> 
				<?php } ?> 
			</div> 
			<div style="margin-top:20px; float:right;"> 
			<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" name="panier_traitement"> 
			  
				<input type="hidden" name="panier_action" value="ok" /> 
                <input type="checkbox" id="cgv" name="cgv_b" checked="checked" style="display:none;" /> 
				<?php 
				if($_SESSION['mode_paiement']==4 && $Total>0) { // 2?me v?rification du moyen de paiement 
					$mod = ($_SESSION['master']==1) ? 1 : 0; 
					echo '<a href="#totaux"><img src="template/base/panier/Terminer_ma_commande_big.png" title="Terminer ma commande" onclick="continue_procedure(\''. $mod .'\');"/></a>'; 
				 
				} elseif ($_SESSION['mode_paiement']==5 && $Total>0) { 
					// permet de n'afficher le bouton paypal qu'au compte titiandtiti@free.fr 
					// || ($_SESSION['customer_id']==73) 
					//if (($_SESSION['customer_id']==3)|| ($_SESSION['customer_id']==73)) { 
						echo '<a href="javascript:document.panier_traitement.submit();"><img src="template/base/panier/Terminer_ma_commande_big.png" title="Terminer ma commande"  /></a>';						 
					/*} else { 
						echo '<font color="red"><strong>PAIEMENT PAR PAYPAL<br>NON DISPONIBLE<br>POUR LE MOMENT</strong></font>'; 
					}*/ 
				 
				} else {  
					echo '<a href="javascript:document.panier_traitement.submit();"><img src="template/base/panier/Terminer_ma_commande_big.png" title="Terminer ma commande"  /></a>'; 
				} 
				?> 
			</form> 
			</div> 
			<div class="clear"></div> 
		 </div> 
		<?php //echo $_SERVER['REMOTE_ADDR']; 
		require("includes/footer.php"); 
		require("includes/page_bottom.php"); 
		} 
	} 
} 
?>