<?php
/*  */
	 
$nom_page = "partenaires";

require("includes/page_top.php");
//require("includes/meta/recrutement.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">
	<div id="recrutement">
    	<div class="titre">Vous �tes int�ress� pour devenir partenaire de notre boutique ?</div>
        
        <div class="image_droite"></div>
        
		<div class="texte_intro">
        	Nous sommes toujours � la recherche de partenaires de qualit� pouvant nous apporter de nouveaux clients ou travailler en partenariat avec nous via le web ou via des boutiques.
        </div>
        
         <div class="offres_stage">
        	<span class="sous_titre">Nos partenaires :</span><br />
            <ul class="liste_1">
            	<li><a href="https://www.boutiquedesaccessoires.fr" target="_blank" title="Accessoires hitech">Boutique Des Accessoires</a></li>
				<li><a href="https://www.Country-Cowboy.com" target="_blank">Country Cowboy</a></li>
				<li><a href="https://www.dentsdestars.com" target="_blank">Dents de stars</a></li>
				<li><a href="https://www.mtp34.com" target="_blank">MTP34 - Cr�ation de site internet</a></li>
				<li><a href="https://airsoftteamelite42.forumgratuit.fr/forum" target="_blank">Air Soft Elite</a></li>
                <li><a href="https://www.beargrylls.fr" target="_blank">Bear Grills</a></li>
                <br />
                <li>Vous ici ...</li>
            </ul>
        </div>
        
        <div style="clear:both;"></div>
        
    </div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>