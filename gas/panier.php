<?php
/* Affichage du panier du client */
$nom_page = "panier";

/*
if (empty($_SESSION['panier'])) {
	$_SESSION['panier']='';
}
if (empty($_SESSION['panier_packs'])) {
	$_SESSION['panier_packs']='';
}*/
   
require("includes/page_top.php");
require("includes/meta/panier.php");
require("includes/meta_head.php");
require("includes/header.php");

// Inclusion des fonctions d'affichage du panier
require("includes/fonctions/fonctions_panier.php");
?>

<?php
// SEULEMENT POUR THIERRY
/*if ($session_customer_id ==3 || $_SERVER['REMOTE_ADDR'] == '88.191.106.176') {
	print_r($_SESSION);
	//echo '<br>';
	echo '<br><br>';
	//echo 'Customers_type : ';
	//echo $_SESSION['customers_type']; 
	print_r($_POST);
	echo '<br><br>';

}*/	

if ((isset($_SESSION['customer_id'])) && (empty($_SESSION['customer_id']))) {
	//echo 'Coucou';
}
?>

<script type="text/javascript" src="<?php echo BASE_DIR; ?>/includes/js/panier.js"></script>

<div id="corps">
	<div id="panier">
    	<div class="titre">Qu'y a-t-il dans mon panier ?</div>
        <?php if(isset($_SESSION['parrainage_kdo']) && $_SESSION['parrainage_kdo']==1){ ?>
				<div style="margin-top:10px; margin-bottom:10px; font-size:16px; color:#8e2eaa;" >
					<strong>Gr&acirc;ce au parrainage un cadeau vous sera livr&eacute; avec votre premi&egrave;re commande</strong>
				</div>
        <?php } ?>
        <div class="contenu">
        	<!-- Tableau contenant les articles choisis par le client -->
            <table id="table_articles">
                <thead>
                    <tr>
                        <th colspan="2">Articles</th>
                        <th>Prix Unitaire</th>
                        <th>Quantit�</th>
                        <th>Prix total</th>
                        <th style="border-right: none;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <?php
				$poids_produits = 0;
				$sous_total_total = '';
				$articles_sous_total_HT = '';
				$pack_sous_total_HT = '';
				
				// Si l'utilisateur est logu�
				if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] != '') {
					
					prix_livraison();
					
					// On r�cup�re les infos sur son panier
					$panier_query = tep_db_query("SELECT cb.products_id, cb.customers_basket_quantity, cb.id_rayon 
												  FROM " . TABLE_CUSTOMERS_BASKET . " as cb
												  WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' AND cb.pack_id = ''");
					
					$i = 0;
					$nb_articles = tep_db_num_rows($panier_query);
					
					if ($nb_articles > 0) {
						
						while ($panier_data = tep_db_fetch_array($panier_query)) {
							
							if (!empty($panier_data['products_id'])) {
								$id_complet = $panier_data['products_id'];
								$i++;
								
								// On r�cup�re les infos sur les options
								$article_options = explode("{", $panier_data['products_id']);
								
								// On controle que le produit a une option 
								if (isset($article_options[1])) {									
									$controle_option = strpos($id_complet, '{'); 
									if ($controle_option>=1) {
										$options = explode("}", $article_options[1]);
										$option_details = infos_options($options[0], $options[1]);
									}
								} else {
									$option_details = '';
								}
															
								// On r�cup�re les infos sur l'article
								$article = explode("_", $article_options[0]);
								$quantite = $panier_data['customers_basket_quantity'];
								$article_details = infos_article($article, $quantite);
								
								// On r�cup�re des infos compl�mentaires
								//$promo = promo_product($article_details['id_article']);								
								
								$articles_sous_total_HT += $article_details['prix_ht']*$quantite;
							
								// On recup�re l'ID du produit
								$id_article_for_weight = explode("_", $id_complet);
								// On recherche le poids du produit
								$recup_poids_query = tep_db_query("	SELECT products_weight FROM products 
																	WHERE products_id = '" . $id_article_for_weight[0] . "'");
								$recup_poids_data = tep_db_fetch_array($recup_poids_query);
								// On calcule le poids total du produit avec la quantit� dans le panier
								$poids_produits += ($recup_poids_data['products_weight'] * $quantite);
								
								// On affiche les articles du panier
								affiche_article_panier($nb_articles, $id_complet, $panier_data['id_rayon'], $article_details, $option_details, $quantite, $i, true, $poids_produits);
								
							}
						}
					} else {
						?>
						<tr>
							<td colspan="6">Aucun Produit</td>
						</tr>
						<?php
					}
				// Si l'utilisateur n'est pas logu�
				} else {
					$i = 0;
					if (!empty($_SESSION['panier'])) {
						$nb_articles = count($_SESSION['panier']);
					} else {
						$nb_articles = 0;
					}
					
					
					if ($nb_articles > 0) {
						foreach ($_SESSION['panier'] as $article) {	
							$quantite = $article[1];
							if ($quantite > 0) {
								$i++;
								$id_complet = $article[0];
								
								$id_rayon = $article[4];
								
								// On r�cup�re les infos sur les options
								$article_options = explode("{", $article[0]);
								
								// On controle que le produit a une option 
								if (isset($article_options[1])) {
									$controle_option = strpos($id_complet, '{'); 
									if ($controle_option>=1) {
										$options = explode("}", $article_options[1]);
										$option_details = infos_options($options[0], $options[1]);
									}
								} else {
									$option_details = '';
								}
								
								
								// On r�cup�re les infos sur l'article
								$info_article = explode("_", $article_options[0]);
								$article_details = infos_article($info_article, $quantite);
								
								// On r�cup�re des infos compl�mentaires
								/*$today = date("Y-m-d H:i:s");
									$req_promo=tep_db_query("select specials_new_products_price, expires_date from specials where products_id=".$article_details['id_article']." and (expires_date >'".$today."' or expires_date='0000-00-00 00:00:00')");
									$res_promo=tep_db_fetch_array($req_promo);
									$nb_row=tep_db_num_rows($req_promo);*/
									
								$articles_sous_total_HT += $article_details['prix_ht']*$quantite;
								
								// On recup�re l'ID du produit
								$id_article_for_weight = explode("_", $id_complet);
								// On recherche le poids du produit
								$recup_poids_query = tep_db_query("	SELECT products_weight FROM products 
																	WHERE products_id = '" . $id_article_for_weight[0] . "'");
								$recup_poids_data = tep_db_fetch_array($recup_poids_query);
								// On calcule le poids total du produit avec la quantit� dans le panier
								$poids_produits += ($recup_poids_data['products_weight'] * $quantite);
								
								// On affiche les articles du panier
								affiche_article_panier($nb_articles, $id_complet, $id_rayon, $article_details, $option_details, $quantite, $i, true, $poids_produits);								
								
							}
						}
					} else {
						?>
						<tr>
							<td colspan="6">Aucun Article dans le Panier</td>
						</tr>
						<?php
					}
				}
                ?>
                </tbody>
            </table>
            
            <!-- Tableau contenant les Packs choisis par le client -->
            <table id="table_packs">
                <thead>
                    <tr>
                        <th colspan="2">Packs</th>
                        <th>Prix Normal</th>
                        <th>Prix du Pack</th>
                        <th>Economie R�alis�e</th>
                        <th>Quantit�</th>
                        <th>Prix total</th>
                        <th style="border-right: none;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
					
                
                <?php
					$pack_sous_total_HT = 0;
					
					//Si l'utilisateur est logu�
                    if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] != '') {
						// On r�cup�re les infos sur ses packs
                        $packs_query = tep_db_query("SELECT cb.pack_id, cb.customers_basket_quantity, cb.id_rayon 
                                                      FROM " . TABLE_CUSTOMERS_BASKET . " as cb
                                                      WHERE cb.customers_id = '" . $_SESSION['customer_id'] . "' and cb.pack_id <> ''");
						
                        $nb_packs = tep_db_num_rows($packs_query);
						
                        if ($nb_packs > 0) {
                            while($packs_data = tep_db_fetch_array($packs_query)) {
								
								if(!empty($packs_data['pack_id'])){
								
									$id_rayon = $packs_data['id_rayon'];
									
									$rayon_query = tep_db_query("SELECT rubrique_name  
																 FROM " . TABLE_RAYON . "
																 WHERE rubrique_id = '" . $id_rayon . "'");
									$rayon_data = tep_db_fetch_array($rayon_query);
									$nom_rayon = $rayon_data['rubrique_name'];
	
									$id_complet = $packs_data['pack_id'];
									$i++;
									
									// On r�cup�re les infos sur les options
									$pack_options = explode("{", $id_complet);
									$options = explode("}", $pack_options[1]);
									$option_details = infos_options($options[0], $options[1]);
									
									// On r�cup�re les infos sur le pack
									$pack = explode("_", $pack_options[0]);
									$pack_details = info_pack($pack, $option_details);
									
									// On r�cup�re des infos compl�mentaires
									$quantite = $packs_data['customers_basket_quantity'];
									$pack_sous_total_HT += $pack_details['prix_ht']*$quantite;
									
									$pack_details['liste_articles'] = str_replace('id_rayon_a_remplir', $id_rayon, $pack_details['liste_articles']);
									$pack_details['liste_articles'] = str_replace('nom_rayon_a_remplir', $nom_rayon, $pack_details['liste_articles']);
									
									//On affiche le pack et la liste de ses articles
									affiche_pack_panier($id_complet, $id_rayon, $pack_details, $quantite, $i, true);
									
									// On recup�re l'ID du produit
									$pack_id_article_for_weight = explode("_", $id_complet);
									// On recherche les produits du pack
									$produits_du_pack_query = tep_db_query("	SELECT *  
																				FROM products_packs
																				WHERE pack_id = '" . $pack_id_article_for_weight[0] . "'");
									$produits_du_pack_data = tep_db_fetch_array($produits_du_pack_query);
									
									// On recherche le poids des produits du pack
									$recup_poids_query_pack = tep_db_query("	SELECT SUM(products_weight) AS pack_weight
																		FROM products 
																		WHERE 
																			products_id = '" . $produits_du_pack_data['products_id'] . "' 
																		OR
																			products_id = '" . $produits_du_pack_data['products_id_1'] . "'
																		OR
																			products_id = '" . $produits_du_pack_data['products_id_2'] . "'
																		");
									$recup_poids_data_pack = tep_db_fetch_array($recup_poids_query_pack);
									// On recup�re le poids total du pack calcul� dans la requ�te
									$poids_produits += $recup_poids_data_pack['pack_weight'];
									
								}
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="8">Aucun Pack dans le panier</td>
                            </tr>
                            <?php
                        }
					// Si l'utilisateur n'est pas logu�
                    } else {
						$j = 0;
						
						if (!empty($_SESSION['panier_packs'])) {
							$nb_packs = count($_SESSION['panier_packs']);
						} else {
							$nb_packs = 0;
						}
						
                        if ($nb_packs > 0) {
                            foreach ($_SESSION['panier_packs'] as $session_pack) {	
                                $quantite = $session_pack[1];
                                if ($quantite > 0) {
									$i++; $j++;
									$id_complet = $session_pack[0];
																	
									$id_rayon = $session_pack[5];
								
									$rayon_query = tep_db_query("SELECT rubrique_name  
																 FROM " . TABLE_RAYON . "
																 WHERE rubrique_id = '" . $id_rayon . "'");
									$rayon_data = tep_db_fetch_array($rayon_query);
									$nom_rayon = $rayon_data['rubrique_name'];
									
									// On r�cup�re les infos sur les options
									$pack_options = explode("{", $id_complet);
									$options = explode("}", $pack_options[1]);
									$option_details = infos_options($options[0], $options[1]);
									
									// On r�cup�re les infos sur le pack
									$pack = explode("_", $pack_options[0]);
									$pack_details = info_pack($pack, $option_details);
									
									// On r�cup�re des infos compl�mentaires
									$pack_sous_total_HT += $pack_details['prix_ht']*$quantite;
									
									$pack_details['liste_articles'] = str_replace('id_rayon_a_remplir', $id_rayon, $pack_details['liste_articles']);
									$pack_details['liste_articles'] = str_replace('nom_rayon_a_remplir', $nom_rayon, $pack_details['liste_articles']);
									
									//On affiche le pack et la liste de ses articles
									affiche_pack_panier($id_complet, $id_rayon, $pack_details, $quantite, $i, true);
									
									// On recup�re l'ID du produit
									$pack_id_article_for_weight = explode("_", $id_complet);
									// On recherche les produits du pack
									$produits_du_pack_query = tep_db_query("	SELECT *  
																				FROM products_packs
																				WHERE pack_id = '" . $pack_id_article_for_weight[0] . "'");
									$produits_du_pack_data = tep_db_fetch_array($produits_du_pack_query);
									
									// On recherche le poids des produits du pack
									$recup_poids_query_pack = tep_db_query("	SELECT SUM(products_weight) AS pack_weight
																		FROM products 
																		WHERE 
																			products_id = '" . $produits_du_pack_data['products_id'] . "' 
																		OR
																			products_id = '" . $produits_du_pack_data['products_id_1'] . "'
																		OR
																			products_id = '" . $produits_du_pack_data['products_id_2'] . "'
																		");
									$recup_poids_data_pack = tep_db_fetch_array($recup_poids_query_pack);
									// On recup�re le poids total du pack calcul� dans la requ�te
									$poids_produits += $recup_poids_data_pack['pack_weight'];	
                                }
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="8">Aucun Pack dans le Panier</td>
                            </tr>
                            <?php
                        }
                    }
                ?>
                </tbody>
            </table>
        </div>
        
        <div id="test"></div>
        
		<div id="totaux">
			<?php 			
			$sous_total_total = $articles_sous_total_HT + $pack_sous_total_HT;
			if($sous_total_total<0){$sous_total_total=0;}?>
        	Sous-total HT : <strong><span id="ss_total_ht"><?php echo format_to_money($sous_total_total); ?></span> �</strong><br />
            <?php
            $taxe = taux_taxe();
			if ($taxe != 1) { ?>
	            TVA : <strong><span id="tva"><?php echo format_to_money($sous_total_total * ($taxe-1)); ?></span> �</strong><br />
            <?php } ?>
            <span class="total">Total TTC : <strong><span id="total"><?php echo format_to_money($sous_total_total * $taxe); ?></span> �</strong></span>        	
        </div>
        
		
		<?php 
		// $_SESSION['customer_id']==3 ||
		//if ($_SERVER['REMOTE_ADDR'] == '86.210.112.38' || $_SERVER['REMOTE_ADDR'] == '78.245.149.37') { 
		
		if ($nb_articles > 0 || $nb_packs > 0) {
		?>
		<br>
		<br>
		<br>
		<div class="titre">Quels vont �tre mes frais de port ?</div>
		<div class="contenu" style="text-align:left;">
			<form action="panier.php" method="post">
				<input type="hidden" name="demande_fdp" value="ok">
					<table id="table_fdp">
						<thead>
							<tr>
								<th>S�lectionnez une destination</th>
								<th>Poids</th>
								<th>Calculez</th>
								<th>Prix HT</th>
								<th>Prix TTC</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select id="pays" name="pays" style="width:500px;">  
										<?php
										$_POST['pays'] = !empty($_POST['pays']) ? $_POST['pays'] : '';
										// S�lection des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
										$req_pays_base=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND
										countries_id IN (1,247,272,274,273,275,271)
										");
										while($res_req_pays_base=tep_db_fetch_array($req_pays_base)){
										?>
											<option <?php if($_POST['pays']==$res_req_pays_base['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res_req_pays_base['countries_id'];?>">
											<?php echo $res_req_pays_base['countries_name'];?>
											</option>
										<?php } ?>
										
										<optgroup label=" -------------------"> </optgroup>
										
										<?php 
										// retrait des pays : France, Guadeloupe, Guyane, Martinique, R�union, Belgique et Suisse
										$req_pays=tep_db_query("SELECT countries_id, countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id_zone_livraison != 0 AND countries_id NOT IN (1,247,272,274,273,275,271) ORDER BY countries_name");
										while($res=tep_db_fetch_array($req_pays)){
										?>
											<option <?php if($_POST['pays']==$res['countries_id']) echo'selected="selected"'; ?> value="<?php echo $res['countries_id'];?>">
											<?php echo $res['countries_name'];?>
											</option>
										<?php } ?>
										
									</select>
								</td>
								<td class="prix"><?php if (!empty($poids_produits)) { echo $poids_produits . ' Kg'; }?></td>
								<td class="prix"><button class="button" type="submit" value="Ok">Ok </button> </td>
								
								<?php
								$_POST['demande_fdp'] = !empty($_POST['demande_fdp']) ? $_POST['demande_fdp'] : '';
								if ($_POST['demande_fdp']=='ok') {
									if ($poids_produits >= 120) {
										echo '<td class="prix" colspan="2">Contactez-nous</td>';
									} else {
										$prix_livraison_par_adresse_HT = number_format((prix_livraison_par_adresse($_POST['pays'],$poids_produits)/1.2),2);
										$prix_livraison_par_adresse_TTC = number_format(prix_livraison_par_adresse($_POST['pays'],$poids_produits), 2);
										echo '<td class="prix">' . $prix_livraison_par_adresse_HT .' �</td>';
										echo '<td class="prix">' . $prix_livraison_par_adresse_TTC . ' � </td>';
									}
								} else {
									echo '<td class="prix">XXX</td><td class="prix">XXX</td>';
								}
								?>							
							</tr>
						</tbody>
					</table>
			
			</form>		
		</div>
		<br>
		<br>
		<?php
		} // if ($nb_articles > 0 || $nb_packs > 0) {
		//} // if ($_SESSION['customer_id']==3 || $_SERVER['REMOTE_ADDR'] == '86.210.112.38') { 
		?>
		
        <div class="actions">
        	<!-- Si on a au moins un pack ou au moins un article on peut continuer sinon l'image ne s'affiche pas -->
        	<?php 
			if ($nb_articles > 0 || $nb_packs > 0) { ?>
				<div class="poursuivre" id="bouton_poursuivre" onclick="location.href='panier_livraison.php';"><img height="48" width="182" src="<?php echo BASE_DIR; ?>/template/base/panier/bouton_finir-cmd.png" alt="finaliser la commande"/></div>
				<?php 
				if ($_SESSION['customers_type']==4) { 
					echo '<div style="text-align:center; font-size:14px;">Cliquez sur le bouton ci-dessus pour passer votre commande ou cr�er votre devis</div>';
				} 
			} ?>
        </div>
        <br>		
		<br>
		<div>
			<br>
			<?php // AVIS VERIFIES #7C7972 ?>
			<div style="margin:5px; padding:30px; background-color:#ffffff; border-top:1px dashed grey;">		
				<iframe id='AV_widget_iframe' frameBorder="0" width="100%" height="100%" src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2ahorizontal_index.html"></iframe>
			</div>
			<?php // End AVIS VERIFIES ?>
	
		</div>
    </div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>