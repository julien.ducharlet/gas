/*global tarteaucitron */
tarteaucitron.lang = {
    "adblock": "Bonjour, notre site joue la transparence et vous donne le choix des services tiers &aacute; activer.",
    "adblock_call": "Nous vous invitons &aacute; d&eacute;sactiver votre adblocker. Vous pourrez ensuite d&eacute;finir les autorisations et restrictions relatives aux cookies.",
    "reload": "Recharger la page",
    
    "alertBigScroll": "En continuant de d&eacute;filer,",
    "alertBigClick": "En poursuivant votre navigation,",
    "alertBig": "vous acceptez l'utilisation de services tiers pouvant installer des cookies",
    
    "alertBigPrivacy": "Notre site utilise des cookies et vous donne le contr&ocirc;le sur ce que vous souhaitez activer",
    "alertSmall": "Gestion des services",
    "acceptAll": "OK, tout accepter",
    "personalize": "Personnaliser",
    "close": "Fermer",

    "all": "Pr&eacute;f&eacute;rence pour tous les services",

    "info": "Protection de votre vie priv&eacute;e",
    "disclaimer": "Certaines fonctionnalit&eacute;s de ce site (partage de contenus sur les r&eacute;seaux sociaux, lecture de vid&eacute;os) s'appuient sur des services propos&eacute;s par des sites tiers. Ces fonctionnalit&eacute;s d&eacute;posent des cookies permettant notamment &aacute; ces sites de tracer votre navigation. Ces cookies ne sont d&eacute;pos&eacute;s que si vous donnez votre accord. Vous pouvez vous informer sur la nature des cookies d&eacute;pos&eacute;s, les accepter ou les refuser soit globalement pour l'ensemble du site et l'ensemble des services, soit service par service.",
    "allow": "Autoriser",
    "deny": "Interdire",
    "noCookie": "Ce service ne d&eacute;pose aucun cookie.",
    "useCookie": "Ce service peut d&eacute;poser",
    "useCookieCurrent": "Ce service a d&eacute;pos&eacute;",
    "useNoCookie": "Ce service n'a d&eacute;pos&eacute; aucun cookie.",
    "more": "En savoir plus",
    "source": "Voir le site officiel",
    "credit": "Gestion des cookies par tarteaucitron.js",
    
    "fallback": "est d&eacute;sactiv&eacute;. Autorisez le d&eacute;pot de cookies pour acc&eacute;der au contenu.",

    "ads": {
        "title": "R&eacute;gies publicitaires",
        "details": "Les r&eacute;gies publicitaires permettent de g&eacute;n&eacute;rer des revenus en commercialisant les espaces publicitaires du site."
    },
    "analytic": {
        "title": "Mesure d'audience",
        "details": "Les services de mesure d'audience permettent de g&eacute;n&eacute;rer des statistiques de fr&eacute;quentation utiles &aacute; l'am&eacute;lioration du site."
    },
    "social": {
        "title": "R&eacute;seaux sociaux",
        "details": "Les r&eacute;seaux sociaux permettent d'am&eacute;liorer la convivialit&eacute; du site et aident &aacute; sa promotion via les partages."
    },
    "video": {
        "title": "Vid&eacute;os",
        "details": "Les services de partage de vid&eacute;o permettent d'enrichir le site de contenu multim&eacute;dia et augmentent sa visibilit&eacute;."
    },
    "comment": {
        "title": "Commentaires",
        "details": "Les gestionnaires de commentaires facilitent le d&eacute;p&ocirc;t de vos commentaires et luttent contre le spam."
    },
    "support": {
        "title": "Support",
        "details": "Les services de support vous permettent d'entrer en contact avec l'&eacute;quipe du site et d'aider &aacute; son am&eacute;lioration."
    },
    "api": {
        "title": "APIs",
        "details": "Les APIs permettent de charger des scripts : g&eacute;olocalisation, moteurs de recherche, traductions, ..."
    }
};
