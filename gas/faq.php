<?php
/* Affichage de la FAQ */
$nom_page = "faq";

require("includes/page_top.php");
require("includes/meta/faq.php");
require("includes/meta_head.php");
require("includes/header.php"); 
if(isset($_POST['logout']))
{destroy();}

//Permet d'initialiser le nombre de famille par ligne (affichage)
$nombre_family_par_ligne=4;

?>
<div id="corps">
<div id="conteneur_faq">
	<div id="titre"><strong>Les Questions les plus Fr&eacute;quentes</strong></div>
    <div id="select_faq"><hr />S&eacute;lectionnez une Cat&eacute;gorie</div>
    <div id="menu_faq">
     <?php 
		//requete pour l'affichage les familles de la faq
		$resultat_requete_faq=tep_db_query("SELECT faq_family_id, faq_name_family, faq_family_img FROM ".TABLE_FAQ_FAMILY." WHERE visible=1 ORDER BY v_order");						
		$row=0;
		while($FAQ_FAMILY=tep_db_fetch_array($resultat_requete_faq))
		{
			echo '	<div class="img_cat">
						<a href="faq.php?Faq_Fam='.$FAQ_FAMILY['faq_family_id'].'">
							<img src="' . BASE_DIR . '/template/base/icones_faq_family/iconesFAQ/'.$FAQ_FAMILY['faq_family_img'].'"  border="0" alt="" />
						</a>
					</div>
					<div class="desc_cat">
						<a href="faq.php?Faq_Fam='.$FAQ_FAMILY['faq_family_id'].'" title="'.$FAQ_FAMILY['faq_name_family'].'">'.$FAQ_FAMILY['faq_name_family'].'</a>
					</div>';
					$row++;
					if ((($row / $nombre_family_par_ligne) == floor($row / $nombre_family_par_ligne))) { 
						echo'<div style="clear:both;"></div>'; 
					}
		}
	?>   
    </div>
    <div style="clear:both;"><hr /></div>
    <div id="qa_categ_faq">
   <?php 
   		//permet d'afficher le contenu des questions reponses en fonction de la famille choisie ( la premiere Q/R etant affich�e par defaut 
		if($_GET['Faq_Fam']!='')
		{
		 $ID_FAM = (int)$_GET['Faq_Fam'];
		 $resultat_requete_nom=tep_db_query("SELECT faq_name_family FROM ".TABLE_FAQ_FAMILY." WHERE faq_family_id=".$ID_FAM);
		 $NAME=tep_db_fetch_array($resultat_requete_nom);
		 echo' <div id="qa_categ">'.$NAME['faq_name_family'].'</div>
			   <div id="imgs_deploy">
				 <a href="javascript:expandAll();"  title="tout ouvrir">
					<img src="' . BASE_DIR . '/template/base/icones_faq_family/ouvrir.png" border="0" alt="img ouvre tout" />
			     </a>
				 <a href="javascript:collapseAll();"   title="tout fermer">
					<img src="' . BASE_DIR . '/template/base/icones_faq_family/fermer.png" border="0" alt="img ferme tout" />
				 </a>
			   </div>
			<div style="clear:both;"></div>';
		$get_faq_id = isset($_GET['Faq_id']) ? $_GET['Faq_id'] : NULL;
		if ($get_faq_id!='') { 
			$FIRST_ID['faq_id']=(int)$_GET['Faq_id'];
		} else {
			$resultat_faq_first=tep_db_query("SELECT MIN(v_order) as FIRST FROM ".TABLE_FAQ." WHERE visible=1 AND faq_family_id=".$ID_FAM); 
			$FIRST=tep_db_fetch_array($resultat_faq_first);
			if($FIRST['FIRST']!=''){
				$resultat_faq_first_id=tep_db_query("SELECT faq_id FROM ".TABLE_FAQ." WHERE visible=1 AND faq_family_id=".$ID_FAM." AND v_order=".$FIRST['FIRST']); 
				$FIRST_ID=tep_db_fetch_array($resultat_faq_first_id);
			}
		}
								
		$resultat_faq_question=tep_db_query("SELECT * FROM ".TABLE_FAQ." WHERE visible=1 AND faq_family_id=".$ID_FAM." order by v_order");
		while($FAQ_QUES=tep_db_fetch_array($resultat_faq_question)) {
			
			if ($FAQ_QUES['faq_id']==$FIRST_ID['faq_id']) {
				echo'<div id="cnt_'.$FAQ_QUES['faq_id'].'"  class="cnt_quet">
					<div class="img_loupe">
					  <a href="javascript:showHide('.$FAQ_QUES['faq_id'].');"  title="fermer/ouvrir">
						<img src="' . BASE_DIR . '/template/base/icones_faq_family/logo-loupe-moins.gif" alt="img loupe ferme" id="cnt_icon_'.$FAQ_QUES['faq_id'].'" />													                      </a>
					</div>
					<div class="question_faq">
					   <a href="javascript:showHide('.$FAQ_QUES['faq_id'].');" title="fermer/ouvrir">
						<strong>'.$FAQ_QUES['question'].'</strong>
					  </a>
					  <a href="faq.php?Faq_Fam='.$ID_FAM.'&amp;Faq_id='.$FAQ_QUES['faq_id'].'">
						<img class="pixel" src="' . BASE_DIR . '/template/base/icones_faq_family/pixel_trans.gif" alt="pixel invisible"/>	
					  </a>
					</div>
					<div id="cnt_desc_'.$FAQ_QUES['faq_id'].'" style=" padding: 5px;">
						<p><i>'.$FAQ_QUES['answer'].'</i><br /></p>
					</div>
				 </div>';
			 } else { 
				echo'<div id="cnt_'.$FAQ_QUES['faq_id'].'" class="cnt_quet2">
						<div class="img_loupe">
							<a href="javascript:showHide('.$FAQ_QUES['faq_id'].');"  title="ouvrir/fermer">
								<img src="' . BASE_DIR . '/template/base/icones_faq_family/logo-loupe-plus.gif" alt="ouvrir" id="cnt_icon_'.$FAQ_QUES['faq_id'].'" />							
							</a>
						</div>
						<div class="question_faq">
							<a href="javascript:showHide('.$FAQ_QUES['faq_id'].');"  title="fermer/ouvrir"><strong>'.$FAQ_QUES['question'].'</strong></a>							
							<a href="faq.php?Faq_Fam='.$ID_FAM.'&amp;Faq_id='.$FAQ_QUES['faq_id'].'">
								<img class="pixel" src="' . BASE_DIR . '/template/base/icones_faq_family/pixel_trans.gif" alt="pixel invisible"/>
							</a>	
						</div>
						<div id="cnt_desc_'.$FAQ_QUES['faq_id'].'" style="display: none; padding: 5px;" >
							<p><i>'.$FAQ_QUES['answer'].'</i><br /></p>
						</div>
					</div>';
				}
		 }				
		} else { 
			echo '	<br />
					<div id="faq_no_select">
						Pour visualiser les questions et les r&eacute;ponses, vous devez selectionner une cat&eacute;gorie ci-dessus !
					</div>'; 
			}
  ?>  
  </div>
</div>

<?php // AVIS VERIFIES #7C7972 ?>
<div style="margin:15px 5px 5px 5px; padding:20px 10px 10px 10px; background-color:#FFFFFF; border-top:1px dashed grey;">		
	<iframe id='AV_widget_iframe' frameBorder="0" width="100%" height="100%" src="//cl.avis-verifies.com/fr/cache/c/2/a/c2aefb07-1640-9f64-294e-c74d06179a2a/widget4/c2aefb07-1640-9f64-294e-c74d06179a2ahorizontal_index.html"></iframe>
</div>
<?php // End AVIS VERIFIES ?>

</div>
<?php  
require("includes/footer.php");
require("includes/page_bottom.php"); ?>