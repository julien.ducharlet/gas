<?php
 
//$nom_page = "partenaires";
$nom_page = "details_pour_administration";

require("includes/page_top.php");
//require("includes/meta/details_pour_pro.php");
require("includes/meta_head.php");
require("includes/header.php");
?>

<div id="corps">
	<div id="recrutement">
    	<div class="titre">Les Avantages pour les Administrations</div>
        
        <!--<div class="image_droite"></div> -->
        
		<div class="texte_intro">
        	Vous repr�sentez une administration (Mairie, Conseil G�n�ral, Conseil R�gional, Collectivit� Locale, Minist�re, Groupement de Gendarmerie, Direction de la S�curit� Publique, SDIS, etc�), nos produits vous int�ressent et vous souhaitez r�gler vos achats par Mandat Administratif, ouvrez un compte au nom de votre administration.
            <strong>ATTENTION : en aucun cas cela vous dispensera d'envoyer les ordres d'achats pour les commandes devant �tre r�gl�es par Mandats Administratifs.</strong>
            <br />
			<br />
			<u>Vous devez imp�rativement nous faire parvenir ces ordres d'achats soit :</u>
			<br />
			-	par courrier � l�adresse suivante : G�n�ral Army Store � 21, rue Pasteur � Entr�e rue H. Bajard � 26260 Saint Donat sur l�Herbasse.<br />
			-	pax Fax au : 09 58 31 59 47<br />
			-	par e-mail � : info@generalarmystore.fr<br />
			<br />
			Votre compte <strong>Administration</strong> sera activ� dans les 2 jours ouvr�s suivant la r�ception de vos documents.<br />
			Vous aurez alors acc�s � toute notre gamme de produits, dont certains sont sp�cialement r�serv�s aux administrations.<br />
			Vous aurez ainsi la possibilit� de passer vos commandes directement en ligne gr�ce � votre <strong>acc�s priv� et s�curis�</strong> par un identifiant et un mot de passe personnels.<br />
			<br />
        </div>
        
         <div class="offres_stage">
		 	<u><strong>Les Avantages du compte Administration :</strong></u><br />
			<br />
			&bull;&nbsp;&nbsp;Passer vos commandes en ligne : facilit� et gain de temps dans le traitement<br />
			&bull;&nbsp;&nbsp;Avoir acc�s � des produits r�serv�s exclusivement aux administrations ayant ouvert un compte sur le site<br />
			&bull;&nbsp;&nbsp;B�n�ficier instantan�ment des remises quantitatives appliqu�es automatiquement par le site pour chaque r�f�rence<br />
			&bull;&nbsp;&nbsp;Pouvoir faire �tablir des devis pour des commandes sp�cifiques (pour des quantit�s exceptionnelles par exemple)<br />
			&bull;&nbsp;&nbsp;Consulter l�historique de vos commandes<br />
			&bull;&nbsp;&nbsp;Imprimer vos factures en autant d�exemplaires que n�cessaire<br />
			&bull;&nbsp;&nbsp;Suivre l�actualit� G�n�ral Army Store en temps r�el<br />
			<br />
        </div>
		<div class="inscription">
		 	<a href="<?php echo BASE_DIR; ?>/compte/create_account_pro.php" style="color:#F00">S'inscrire maintenant en tant qu'Administration</a><br /><br />
        </div>
        
        <div style="clear:both;"></div>
        
    </div>
</div>

<?php
require("includes/footer.php");
require("includes/page_bottom.php");
?>