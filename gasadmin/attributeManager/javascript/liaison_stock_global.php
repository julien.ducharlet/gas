<?php
/*  Page AJAX by Paul
	cette page permet de lier le module des options au module du stock global de l'article */
	
require("../../includes/configure.php");
require("../../includes/functions/general.php");
require("../../includes/functions/database.php");

tep_db_connect();

$recherche_options = tep_db_query("SELECT SUM(options_quantity_reel) as qte_reel, SUM(options_quantity) as qte FROM products_attributes WHERE products_id = " . $_GET['id_article'] . " AND options_values_id <> " . $_GET['optionValueId']);
//echo "SELECT SUM(options_quantity_reel) as qte_reel, SUM(options_quantity) as qte FROM products_attributes WHERE products_id = " . $_GET['id_article'] . " AND options_values_id <> " . $_GET['optionValueId'];
$quantite_totale = tep_db_fetch_array($recherche_options);


$total_reel = $quantite_totale['qte_reel'] + $_GET['quantity_reel'];
$total_virtuel = $quantite_totale['qte'] + $_GET['quantity'];
//echo "UPDATE products SET products_quantity = " . $total_virtuel . ", products_quantity_reel = " . $total_reel . " WHERE products_id = " . $_GET['id_article'];
tep_db_query("UPDATE products SET products_quantity = " . $total_virtuel . ", products_quantity_reel = " . $total_reel . " WHERE products_id = " . $_GET['id_article']);

echo $total_reel . "_" . $total_virtuel;

?>