<?php
/*
  $Id: margin_report2.php,v 2.56 2004/08/18 18:50:51 chaicka Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2004 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  //require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_MARGIN_REPORT);
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  switch($_GET['report_id']) {
	case 'weekly':
	  $weekday_query = mysql_query("SELECT weekday(now()) as weekday");
	  $weekday = mysql_fetch_array($weekday_query);
	  $day = 6+($weekday['weekday'] - 6);
	  //echo $day;
		switch($day) {
		  case '0':
			$date = mysql_query("SELECT curdate() - INTERVAL 1 DAY as time");
			break;
		  case '1':
			$date = mysql_query("SELECT curdate() - INTERVAL 2 DAY as time");
			break;
		  case '2':
			$date = mysql_query("SELECT curdate() - INTERVAL 3 DAY as time");
			break;
		  case '3':
			$date = mysql_query("SELECT curdate() - INTERVAL 4 DAY as time");
			break;
		  case '4':
			$date = mysql_query("SELECT curdate() - INTERVAL 5 DAY as time");
			break;
		  case '5':
			$date = mysql_query("SELECT curdate() - INTERVAL 6 DAY as time");
			break;
		  case '6':
			$date = mysql_query("SELECT curdate() as time");
			break;
		}
	  $d = mysql_fetch_array($date, MYSQL_ASSOC);
	  $header = 'Ce rapport est depuis le d�but de la semaine : ';  
	  break;
	case 'monthly':
	  $date = mysql_query("SELECT FROM_UNIXTIME(" . strtotime(date("F 1, Y")) . ") as time");
	  $d = mysql_fetch_array($date, MYSQL_ASSOC);
	  $header = 'Ce rapport est depuis le d�but du mois : ';
	  break;
	case 'last_monthly':
	  $le_mois_dernier = mktime(0, 0, 0, date("m")-1, date("d")- (date("d")-1), date("Y")); // Pour recupere le premier jour du mois precedent
	  $date = mysql_query("SELECT FROM_UNIXTIME(" . date($le_mois_dernier) . ") as time");
	  $d = mysql_fetch_array($date, MYSQL_ASSOC);
	  $header = 'Ce rapport est depuis le d�but du mois : ';
	  break;
	case 'quarterly':
	  $quarter_query = mysql_query("SELECT QUARTER(now()) as quarter, year(now()) as year");
	  $quarter = mysql_fetch_array($quarter_query, MYSQL_ASSOC);
		switch($quarter['quarter']) {
		  case '1':
			$d['time'] = $quarter['year'] . '-01-01';
			break;
		  case '2':
			$d['time'] = $quarter['year'] . '-04-01';
			break;
		  case '3':
			$d['time'] = $quarter['year'] . '-07-01';
			break;
		  case '4':
			$d['time'] = $quarter['year'] . '-10-01';
			break;
		}
	  $header = 'Ce rapport est pour le trimestre d�butant le : ';
	  break;
	case 'semiannually':
	  $year_query = mysql_query("SELECT year(now()) as year, month(now()) as month");
	  $year = mysql_fetch_array($year_query, MYSQL_ASSOC);
	  if ($year['month'] >= '7') {
		$d['time'] = $year['year'] . '-07-01';
	  } else {
		$d['time'] = $year['year'] . '-01-01';
	  }
	  $header = 'Ce rapport est pour le semestre d�butant le : ';
	  break;
	case 'annually':
	  $year_query = mysql_query("SELECT year(now()) as year");
	  $year = mysql_fetch_array($year_query, MYSQL_ASSOC);
	  $d['time'] = $year['year'] . '-01-01';
	  $header = 'Ce rapport est depuis le d�but de l\'ann�e : ';
	  break;
		 // cas par defaut quand on arrive sur les marges
	default :
	  $date = mysql_query("SELECT curdate() as time");
	  $d = mysql_fetch_array($date, MYSQL_ASSOC);
	  $header = 'Ce rapport est pour le jour : ';
	  break;
  }

if (isset($_GET['date']) && ($_GET['date'] == 'yes')) {
	  
	$header = 'Ce rapport est pour les dates entre '  . $_GET['sdate'] . ' et ' . $_GET['edate'] . ' : ';
	
	$end_of_query = ' where date_purchased > \'' . $_GET['sdate'] . '\' and date_purchased < \''. $_GET['edate'] .'\'';
}
else {
	
	if ($_GET['report_id'] != '1') {
	
		if($_GET['report_id'] == 'last_monthly') {
			
			$m = date('m');
			$bet = date('Y-'. $m .'-01 00:00:00');
		}
		else {
			
			$bet = date('Y-m-d H:i:s');
		}
			
		$header_date_query = mysql_query("SELECT DATE_FORMAT('" . $d['time'] . "', '%d/%m/%Y ') as date");
		$header_date = mysql_fetch_array($header_date_query, MYSQL_ASSOC);
		$header .= $header_date['date'];
		
		$end_of_query = ' where date_purchased > \'' . $d['time'] . '\' and date_purchased < \''.$bet .'\'';
	}
	else {
		
		$end_of_query = '';
	}
}

$p = array();
$total_price = 0;
$total_cost = 0;
$total_items_sold = 0;
$Nb_commande=0;

$query = 'select count(*) as nb_cmd, sum(ss_total) as ventes_HT, sum(nb_articles) as nb_articles, sum(marge) as marge_HT from '. TABLE_ORDERS . $end_of_query;
$query = tep_db_query($query);
$total = tep_db_fetch_array($query);


//affichage des choix pour les stats et les stats
$page_contents .= '	<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table width="100%" border="0" cellspacing="0" cellpadding="2" style="margin-bottom:10px; margin-top:10px">
															<tr>
																<td align="left" class="main" >' . tep_draw_form('report', FILENAME_MARGIN_REPORT, '', 'get') . 'Afficher : &nbsp;';
																				$options = array();
																				
																				$options[] = array('id' => 'daily', 'text' => 'Aujourd\'hui');
																				$options[] = array('id' => 'weekly', 'text' => 'Cette semaine');
																				$options[] = array('id' => 'monthly', 'text' => 'Ce mois');
																				$options[] = array('id' => 'last_monthly', 'text' => 'Mois Dernier');
																				$options[] = array('id' => 'quarterly', 'text' =>'Par trimestre');
																				$options[] = array('id' => 'semiannually', 'text' => 'Semestriel');
																				$options[] = array('id' => 'annually', 'text' => 'Annuelle');
																			  $page_contents .= tep_draw_pull_down_menu('report_id', $options, (isset($HTTP_GET_VARS['report_id']) ? $HTTP_GET_VARS['report_id'] :                                        '1'), 'onchange="this.form.submit()"');
	
																				$page_contents .= '</form>
																</td>
															</tr>
															<tr>
																<td align=left> 
																	<table bgcolor="#DDDDDD" width="300px">';
																	  $page_contents .= '<tr><td class="main">Nombre de Commandes : ' . $total['nb_cmd'] . '</td></tr>';
																		$page_contents .= '<tr><td class="main">Nombre Articles Vendus : ' . $total['nb_articles'] . '</td></tr>';
																		$page_contents .= '<tr><td class="main">Montant des Ventes HT : ' . $currencies->format($total['ventes_HT']) . '</td></tr>';
																		$page_contents .= '<tr><td class="main">Prix d\'achat Total HT : ' . $currencies->format($total['ventes_HT']-$total['marge_HT']) . '</td></tr>';
																		$page_contents .= '<tr><td class="main">Marge Totale HT : ' . $currencies->format($total['marge_HT']) . '</td></tr>';
																		$page_contents .= '
																	</table>
																</td>
															</tr>';
	
	

	$page_contents .= '<tr>
	                    <td align="left" class="main">
	                      <table bgcolor="#DDDDDD" width="300px">
												   <tr>
													   <td class="main" colspan="2" align="center"><b>' . tep_draw_form('export_to_file_by_date', FILENAME_MARGIN_REPORT, 'get', '') . tep_draw_hidden_field('action', 'export') . tep_draw_hidden_field('date', 'yes') . 'Choisissez les dates<b></td>
													</tr>
												  <tr>
													  <td class="main" align="right">Date de Debut :</td>
														<td><input type="text" name="sdate" id="sdate"><a href="#" onClick="displayCalendar(sdate,\'yyyy-mm-dd\',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a></td>
													</tr>
													<tr>
														<td class="main" align="right">Date de Fin :</td>
														<td><input type="text" name="edate" id="edate"><a href="#" onClick="displayCalendar(edate,\'yyyy-mm-dd\',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a></td>
													</tr>
												  <tr>
														<td align="center" colspan="2"><input type="image" name="submit" src="' . DIR_WS_LANGUAGES . $language . '/images/buttons/button_confirm.gif" alt="Exportation Excel" width="65" height="22"></form></td>
													</tr>
												</table>
											</td>
									 </tr>
								</table>';
	
	
	$contents2 .= '<tr><td bgcolor="#00CCFF" align="right"><font size="3" face="Times New Roman, Times, serif">Nbre de Commandes : </font></td><td align="left" colspan="4" bgcolor="#C0C0C0">' . $Nb_commande . '</td></tr>'.
								'<tr><td bgcolor="#00CCFF" align="right"><font size="3" face="Times New Roman, Times, serif">Nbre Articles Vendus : </font></td><td align="left" colspan="4" bgcolor="#C0C0C0">' . ($total_items_sold)/2 . '</td></tr>'. 
				  '<tr><td bgcolor="#00CCFF" align="right"><font size="3" face="Times New Roman, Times, serif">Montant des Ventes : </font></td><td align="left" colspan="4" bgcolor="#969696">' . ($currencies->format($total_price))/2 . '</td></tr>'.
				  '<tr><td bgcolor="#00CCFF" align="right"><font size="3" face="Times New Roman, Times, serif">Prix d\'achat Total : </font></td><td align="left" colspan="4" bgcolor="#C0C0C0">' . ($currencies->format($total_cost))/2 . '</td></tr>'.
				  '<tr><td bgcolor="#00CCFF" align="right"><font size="3" face="Times New Roman, Times, serif">Marge Totale : </font></td><td align="left" colspan="4" bgcolor="#969696">' . ($currencies->format(($total_price - $total_cost)))/2 . '</td></tr>';

	$csv_data .= "\n\n";
	$csv_data .= "Nbre Articles Vendus \t"  . $Nb_commande . "\n
				 Nbre Articles Vendus \t"  . ($total_items_sold)/2  . "\n
	       Montant des Ventes \t"  . ($currencies->format($total_price))/2 . "\n
				 Prix d\'achat Total \t"  . ($currencies->format($total_cost))/2 . "\n
				 Marge Totale\t"  . ($currencies->format(($total_price - $total_cost)))/2 . "\n";

?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/javascript/dhtmlgoodies_calendar.css"></link>
<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td>
          <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td class="pageHeading"><?php echo $header; ?></td> 
            </tr>
            <tr>
              <td><?php echo $page_contents; ?></td>
            </tr>
          </table>
				</td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>