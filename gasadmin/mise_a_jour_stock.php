<?php
/*
  Page: mise_a_jour_stock.php
*/

  require('includes/application_top.php');

	($row_by_page) ? 
	define('MAX_DISPLAY_ROW_BY_PAGE' , $row_by_page ) : $row_by_page = 50; 
	define('MAX_DISPLAY_ROW_BY_PAGE' , 100 );


// Affichage de la liste des type d'article / des fabricants 
function familys_list(){
        global $manufacturer;

        $familys_query = tep_db_query("select f.family_id, f.family_name from " . TABLE_FAMILY . " f order by f.family_name ASC");
        $return_string = '<select name="manufacturer" onChange="this.form.submit();">';
        $return_string .= '<option value="' . 0 . '">Toutes les familles</option>';
        while($familys = tep_db_fetch_array($familys_query)){
                $return_string .= '<option value="' . $familys['family_id'] . '"';
                if($manufacturer && $familys['family_id'] == $manufacturer) 
												$return_string .= ' SELECTED';
												$return_string .= '>' . $familys['family_name'] . ' - ' . $familys['family_id'] . '</option>';
												
					}
        $return_string .= '</select>';
        return $return_string;
}



##// Uptade database
  switch ($HTTP_GET_VARS['action']) {
    case 'update' :
      $count_update=0;
      $item_updated = array();
	  
	             // Enregistrement de la REFERENCE
                 if($HTTP_POST_VARS['product_new_model']){
                   foreach($HTTP_POST_VARS['product_new_model'] as $id => $new_model) {
                         if (trim($HTTP_POST_VARS['product_new_model'][$id]) != trim($HTTP_POST_VARS['product_old_model'][$id])) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_model='" . $new_model . "', products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				
				// Enregistrement du NOM DE L'ARTICLE
                  if($HTTP_POST_VARS['product_new_name']){
                   foreach($HTTP_POST_VARS['product_new_name'] as $id => $new_name) {
                         if (trim($HTTP_POST_VARS['product_new_name'][$id]) != trim($HTTP_POST_VARS['product_old_name'][$id])) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS_DESCRIPTION . " SET products_name='" . $new_name . "' WHERE products_id=$id and language_id=" . $languages_id);
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				
				// Enregistrement du STOCK REEL
				if($HTTP_POST_VARS['product_new_quantity_reel']){
                   foreach($HTTP_POST_VARS['product_new_quantity_reel'] as $id => $new_quantity_reel) {
                         if ($HTTP_POST_VARS['product_new_quantity_reel'][$id] != $HTTP_POST_VARS['product_old_quantity_reel'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_quantity_reel='".$new_quantity_reel."', products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				
				// Enregistrement du STOCK VIRTUEL
				if($HTTP_POST_VARS['product_new_quantity']){
                   foreach($HTTP_POST_VARS['product_new_quantity'] as $id => $new_quantity) {
                         if ($HTTP_POST_VARS['product_new_quantity'][$id] != $HTTP_POST_VARS['product_old_quantity'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_quantity='".$new_quantity."', products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				
				// Enregistrement du STOCK MINIMUM
				if($HTTP_POST_VARS['product_new_quantity_min']){
                   foreach($HTTP_POST_VARS['product_new_quantity_min'] as $id => $new_quantity_min) {
                         if ($HTTP_POST_VARS['product_new_quantity_min'][$id] != $HTTP_POST_VARS['product_old_quantity_min'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_quantity_min='".$new_quantity_min."', products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				
				// Enregistrement du STOCK IDEAL
				if($HTTP_POST_VARS['product_new_quantity_ideal']){
                   foreach($HTTP_POST_VARS['product_new_quantity_ideal'] as $id => $new_quantity_ideal) {
                         if ($HTTP_POST_VARS['product_new_quantity_ideal'][$id] != $HTTP_POST_VARS['product_old_quantity_ideal'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_quantity_ideal='".$new_quantity_ideal."', products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				
				// Enregistrement du STOCK MAXIMUM
				if($HTTP_POST_VARS['product_new_quantity_max']){
                   foreach($HTTP_POST_VARS['product_new_quantity_max'] as $id => $new_quantity_max) {
                         if ($HTTP_POST_VARS['product_new_quantity_max'][$id] != $HTTP_POST_VARS['product_old_quantity_max'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_quantity_max='".$new_quantity_max."', products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
                
                
     $count_item = array_count_values($item_updated);
     if ($count_item['updated'] > 0) $messageStack->add($count_item['updated'].' '.'produit actualis�!' . " $count_update " . 'Valeur(s) chang�e(s)', 'success');
     break;

     case 'calcul' :
      if ($HTTP_POST_VARS['spec_price']) $preview_global_price = 'true';
     break;
 }

//// explode string parameters from preview product
     if($info_back && $info_back!="-") {
       $infoback = explode('-',$info_back);
       $sort_by = $infoback[0];
       $page =  $infoback[1];
       $current_category_id = $infoback[2];
       $row_by_page = $infoback[3];
       $manufacturer = $infoback[4];
     }


// D�finition du menu d�roulant pour le nombre de resultat � afficher

   $row_bypage_array = array(array());
   for ($i = 50; $i <=500 ; $i=$i+50) {
      $row_bypage_array[] = array('id' => $i,
                                  'text' => $i);
   }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->



<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->

<td width="100%" valign="top">
<table border="0" width="100%" cellspacing="0" cellpadding="2">
	<tr>
		<td>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td class="pageHeading" colspan="3" valign="top">Mise � jour rapide du STOCK</td>
          <td class="pageHeading" align="right">&nbsp;</td>
        </tr>
      </table>
		</td>
</tr>
<tr>
	<td align="center">
		<table width="100%" cellspacing="0" cellpadding="0" border="1" bgcolor="#F3F9FB" bordercolor="#D1E7EF" height="100">
  <tr align="left">
    <td valign="middle">
    	<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td height="5" colspan="5"></td>
				</tr>
				<tr align="center">
					<td class="smalltext"> 
						<?php 
								echo tep_draw_form('row_by_page', FILENAME_MISE_A_JOUR_STOCK, '', 'get'); 
								echo tep_draw_hidden_field( 'manufacturer', $manufacturer); 
								echo tep_draw_hidden_field( 'cPath', $current_category_id);
						?>
          </td>
          <td class="smallText">
						Nombre d'article par page&nbsp;&nbsp;
						<?php echo tep_draw_pull_down_menu('row_by_page', $row_bypage_array, $row_by_page, 'onChange="this.form.submit();"'); ?>
          </td></form>
          	<?php 
								echo tep_draw_form('categorie', FILENAME_MISE_A_JOUR_STOCK, '', 'get'); 
								echo tep_draw_hidden_field( 'row_by_page', $row_by_page); 
								echo tep_draw_hidden_field( 'manufacturer', $manufacturer); ?>
          <td class="smallText" align="center" valign="top">
          	Afficher par CATEGORIE&nbsp;&nbsp;
						<?php echo tep_draw_pull_down_menu('cPath', tep_get_category_tree(), $current_category_id, 'onChange="this.form.submit();"'); ?>
          </td></form>
          	<?php 
								echo tep_draw_form('familys', FILENAME_MISE_A_JOUR_STOCK, '', 'get'); 
								echo tep_draw_hidden_field( 'row_by_page', $row_by_page); 
								echo tep_draw_hidden_field( 'cPath', $current_category_id);?>
          <td class="smallText" align="center" valign="top">
          	Afficher par FAMILLE d'article&nbsp;&nbsp;<?php echo familys_list(); ?>
          </td></form>
						<?php 
            		// echo tep_draw_form('familys', FILENAME_MISE_A_JOUR_STOCK, '', 'get'); 
								// echo tep_draw_hidden_field( 'row_by_page', $row_by_page); 
								// echo tep_draw_hidden_field( 'cPath', $current_category_id);?>
          <!--<td class="smallText" align="center" valign="top">
          	Afficher par MARQUE (A FAIRE) :&nbsp;&nbsp;<?php echo familys_list(); ?>
          </td></form> -->
				</tr>
			</table>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
        <form name="update" method="POST" action="<?php echo "$PHP_SELF?action=update&page=$page&sort_by=$sort_by&cPath=$current_category_id&row_by_page=$row_by_page&manufacturer=$manufacturer"; ?>">
        
        <tr>
        	<td>&nbsp;
						
        	</td>
        </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
					<td valign="top">
						<table border="0" width="100%" cellspacing="0" cellpadding="2">			
							<tr class="dataTableHeadingRow">

<!----------------------------- DEBUT REFERENCE ARTICLE --------------------------------------//-->
								<td class="dataTableHeadingContent" align="left" valign="top">
									<?php 
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"left\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_MISE_A_JOUR_STOCK, 'cPath='. $current_category_id .'&sort_by=p.products_model ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Mod�le' . ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_MISE_A_JOUR_STOCK, 'cPath='. $current_category_id .'&sort_by=p.products_model DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Mod�le' . ' ' . '- De Z � A')."</a>
                  <br />"  .'<br />Mod�le' . "
                  </td>
                  </tr>
                  </table>"; 
                  ?>
                </td>
<!----------------------------- FIN REFERENCE ARTICLE --------------------------------------//-->

<!----------------------------- DEBUT NOM ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
					<?php echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
					<tr class=\"dataTableHeadingRow\">
					<td class=\"dataTableHeadingContent\" align=\"left\" valign=\"middle\">&nbsp;<a href=\"" . tep_href_link( FILENAME_MISE_A_JOUR_STOCK, 'cPath='. $current_category_id .'&sort_by=pd.products_name ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Nom' . '- De A � Z')."</a>
					<a href=\"" . tep_href_link( FILENAME_MISE_A_JOUR_STOCK, 'cPath='. $current_category_id .'&sort_by=pd.products_name DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Nom' . ' ' . '- De Z � A')."</a>
					<br />"  .'<br />Nom' . "
					</td>
					</tr>
					</table>"; 
					?>
                </td>
<!----------------------------- FIN NOM ARTICLE --------------------------------------//-->


<!----------------------------- DEBUT STOCK REEL DE L'ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">			  
						<tr class="dataTableHeadingRow">
							<td class="dataTableHeadingContent" align="center" valign="middle">&nbsp;<br />Stock R�el</td>
						</tr>
					</table>       
                </td>
<!----------------------------- FIN STOCK REEL DE L'ARTICLE --------------------------------------//-->

<!----------------------------- DEBUT STOCK VIRTUEL DE L'ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">			  
						<tr class="dataTableHeadingRow">
							<td class="dataTableHeadingContent" align="center" valign="middle">&nbsp;<br />Stock Virtuel</td>
						</tr>
					</table>       
                </td>
<!----------------------------- FIN STOCK VIRTUEL DE L'ARTICLE --------------------------------------//-->

<!----------------------------- DEBUT STOCK MINIMUM DE L'ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">			  
						<tr class="dataTableHeadingRow">
							<td class="dataTableHeadingContent" align="center" valign="middle">&nbsp;<br />Stock Minimum</td>
						</tr>
					</table>       
                </td>
<!----------------------------- FIN STOCK MINIMUM DE L'ARTICLE --------------------------------------//-->

<!----------------------------- DEBUT STOCK IDEAL DE L'ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">			  
						<tr class="dataTableHeadingRow">
							<td class="dataTableHeadingContent" align="center" valign="middle">&nbsp;<br />Stock Id�al</td>
						</tr>
					</table>       
                </td>
<!----------------------------- FIN STOCK IDEAL DE L'ARTICLE --------------------------------------//-->

<!----------------------------- DEBUT STOCK MAXIMUM DE L'ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">			  
						<tr class="dataTableHeadingRow">
							<td class="dataTableHeadingContent" align="center" valign="middle">&nbsp;<br />Stock Maximum</td>
						</tr>
					</table>       
                </td>
<!----------------------------- FIN STOCK MAXIMUM DE L'ARTICLE --------------------------------------//-->

			
<!----------------------------- DEBUT DE CELLULE VIDE --------------------------------------//-->
								<td class="dataTableHeadingContent" align="center" valign="middle"></td>
<!----------------------------- FIN DE CELLULE VIDE --------------------------------------//-->

<!----------------------------- DEBUT DE CELLULE VIDE --------------------------------------//-->
								<td class="dataTableHeadingContent" align="center" valign="middle"></td>
<!----------------------------- FIN DE CELLULE VIDE --------------------------------------//-->

              </tr>
              <tr class="datatableRow">
<?php
	if ($sort_by && !ereg('order by',$sort_by)) $sort_by = 'order by '.$sort_by ;
    $origin = FILENAME_MISE_A_JOUR_STOCK."?info_back=$sort_by-$page-$current_category_id-$row_by_page-$manufacturer";
    $split_page = $page;
    if ($split_page > 1) $rows = $split_page * 50 - 50; // Nombre identique
	
	$select="select p.products_id, p.products_model, pd.products_name, p.products_quantity_reel, p.products_quantity, p.products_quantity_min, p.products_quantity_ideal, p.products_quantity_max,  p.manufacturers_id, p.family_id ";
	$from = "from  " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION .  " pd ";
	$where="where p.products_id = pd.products_id and pd.language_id = '$languages_id' ";
	if($manufacturer){
		$where.="and p.family_id = " . $manufacturer." ";
    }
  	if ($current_category_id != 0){
    	$from.=", " . TABLE_PRODUCTS_TO_CATEGORIES . " pc ";
		$where.="and p.products_id = pc.products_id and pc.categories_id = '" . $current_category_id . "' ";
  	}
	$products_query_raw=$select.$from.$where.$sort_by;
	
//// page splitter and display each products info
  $products_split = new splitPageResults($split_page, 100, $products_query_raw, $products_query_numrows);
  $products_query = tep_db_query($products_query_raw);
  while ($products = tep_db_fetch_array($products_query)) {
    $rows++;
    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }

echo '<tr> ';


// DEBUT AFFICHAGE REFERENCE DE L'ARTICLE
					echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"35\" name=\"product_new_model[".$products['products_id']."]\" value=\"".$products['products_model']."\"></td>\n";
				
// FIN AFFICHAGE REFERENCE DE L'ARTICLE

// DEBUT DE LA MODIFICATION DU NOM DE L'ARTICLE
			   	echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"40\" name=\"product_new_name[".$products['products_id']."]\" value=\"".str_replace("\"","&quot;",$products['products_name'])."\"></td>\n";
			   
// FIN DE LA MODIFICATION DU NOM DE L'ARTICLE

// DEBUT STOCK REEL DE L'ARTICLE
		echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"16\" name=\"product_new_quantity_reel[".$products['products_id']."]\" value=\"".$products['products_quantity_reel']."\"></td>\n";
// FIN STOCK REEL DE L'ARTICLE

// DEBUT STOCK VIRTUEL DE L'ARTICLE
		echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"16\" name=\"product_new_quantity[".$products['products_id']."]\" value=\"".$products['products_quantity']."\"></td>\n";
// FIN STOCK VIRTUEL DE L'ARTICLE

// DEBUT STOCK MINIMUM DE L'ARTICLE
		echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"16\" name=\"product_new_quantity_min[".$products['products_id']."]\" value=\"".$products['products_quantity_min']."\"></td>\n";
// FIN STOCK MINIMUM DE L'ARTICLE
 
// DEBUT STOCK IDEAL DE L'ARTICLE
		echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"16\" name=\"product_new_quantity_ideal[".$products['products_id']."]\" value=\"".$products['products_quantity_ideal']."\"></td>\n";
// FIN STOCK IDEAL DE L'ARTICLE

// DEBUT STOCK MAXIMUM DE L'ARTICLE
		echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"16\" name=\"product_new_quantity_max[".$products['products_id']."]\" value=\"".$products['products_quantity_max']."\"></td>\n";
// FIN STOCK MAXIMUM DE L'ARTICLE


// DEBUT DE LA PREVISUALISATION 
        if(DISPLAY_PREVIEW == 'true')echo "<td class=\"smallText\" align=\"left\"><a href=\"".tep_href_link (FILENAME_ARTICLE_EDIT, 'pID='.$products['products_id'].'&action=new_product_preview&read=only&sort_by='.$sort_by.'&page='.$split_page.'&origin='.$origin)."\">". tep_image(DIR_WS_IMAGES . 'icon_info.gif', 'produit pr�c�dent') ."</a></td>\n";
                if(DISPLAY_EDIT == 'true')echo "<td class=\"smallText\" align=\"left\"><a href=\"".tep_href_link (FILENAME_ARTICLE_EDIT, 'pID='.$products['products_id'].'&cPath='.$categories_products[0].'&action=new_product')."\" target=\"_blank\">". tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', 'Passez � l\'�dition compl�te') ."</a></td>\n";
// FIN DE LA PREVISUALISATION

//// Hidden parameters for cache old values
        echo tep_draw_hidden_field('product_old_name['.$products['products_id'].'] ',$products['products_name']);
		echo tep_draw_hidden_field('product_old_model['.$products['products_id'].'] ',$products['products_model']);
		echo tep_draw_hidden_field('product_old_quantity_reel['.$products['products_id'].']',$products['products_quantity_reel']);
		echo tep_draw_hidden_field('product_old_quantity['.$products['products_id'].']',$products['products_quantity']);
		echo tep_draw_hidden_field('product_old_quantity_min['.$products['products_id'].']',$products['products_quantity_min']);
        echo tep_draw_hidden_field('product_old_quantity_ideal['.$products['products_id'].']',$products['products_quantity_ideal']);
		echo tep_draw_hidden_field('product_old_quantity_max['.$products['products_id'].']',$products['products_quantity_max']);
        
//// hidden display parameters
        echo tep_draw_hidden_field( 'row_by_page', $row_by_page);
        echo tep_draw_hidden_field( 'sort_by', $sort_by);
        echo tep_draw_hidden_field( 'page', $split_page);
     }
    echo "</table>\n";

?>
          </td>
        </tr>
       </table></td>
      </tr>
<tr>
<td align="right" height="40">
<?php
                //// Affichage des boutons
                echo tep_image_submit('button_update.gif', IMAGE_UPDATE);
              	echo '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_MISE_A_JOUR_STOCK,"row_by_page=$row_by_page") . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>';
?></td>
</tr>
</form>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                <td class="smallText"><?php echo $products_split->display_count($products_query_numrows, 50, $split_page, TEXT_DISPLAY_NUMBER_OF_PRODUCTS);  ?></td>
                <td class="smallText" align="right">
				<?php echo $products_split->display_links($products_query_numrows, 50, MAX_DISPLAY_PAGE_LINKS, $split_page, '&cPath='. $current_category_id .'&sort_by='.$sort_by . '&manufacturer='.$manufacturer.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer); ?>
				
				</td>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->
  </tr>
</table>

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>