<?php
/*  Page by Paul, Requ�te by Ludo
	Update des images des produits pour multi image*/

require('includes/application_top.php');
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>

<script language="javascript">
function verif() {
	if (window.confirm("Voulez-vous vraiment mettre \340 jour la table ?\n\nINTERDICTION FORMELLE de s'en servir sans l'aval de l'administrateur du site !")){
		document.location.href="<?php echo $_SERVER['PHP_SELF']; ?>?lancer=true"
		return true;
	}
}
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<div style="text-align: center;">
    <?php if ($_GET['effectue']) { ?>
    <div style="background-color: #CFC; border: 1px solid #090; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Base de donn&eacute;es mise &agrave; jour avec <strong>succ&egrave;s</strong> !
    </div>
    <?php } else { ?>
    <div style="background-color: #FFC; border: 1px solid #FC0; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Cette page met &agrave; jour la table products_images pour la migration vers le multi-images.
    </div>
    
    <br />
    
    <input type="button" value="Lancer la mise &agrave; jour de la base" onClick="verif();"/>
    <?php } ?>

</div>
<?php
if ($_REQUEST['lancer']) {
	$res=tep_db_query("select products_id, products_bimage from " . TABLE_PRODUCTS);
	while($res && $row=tep_db_fetch_array($res)) {
		$query_insert="insert into ".TABLE_PRODUCTS_IMAGE." (products_id, products_bimage) values ('".$row["products_id"]."', '".$row["products_bimage"]."')";
		//echo $query_insert."<br />";
		//$res_insert=tep_db_query($query_insert);
		$i++;
	}
	?>
	<META HTTP-EQUIV="refresh" content="0;URL='<?php echo $_SERVER['PHP_SELF']; ?>?effectue=true&traites=<?php echo $i; ?>'">
    <?php
}
?>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>