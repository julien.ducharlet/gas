<?php
/*
  $Id: article_logos.php,v 1.55 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

if (tep_not_null($action)) {
	  
    switch ($action) {
		
      case 'insert':
		if ($logo_image = new upload('logo_image', DIR_FS_CATALOG_IMAGES_LOGOS)) {
			
			$query = 'insert into '. TABLE_PRODUCTS_LOGOS .'(logo_name, logo_hover) values(\''.  $logo_image->filename .'\', \''.  $HTTP_POST_VARS['logo_hover'] .'\')';
			tep_db_query($query);
			
			$logo_id = tep_db_insert_id();
		}
		
		tep_redirect(tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . 'logoID=' . $logo_id));
		
		break;
	  	
     case 'save':
	 	
		$sql_data_array = array();
		
        if (isset($HTTP_GET_VARS['logoID'])) $logo_id = tep_db_prepare_input($HTTP_GET_VARS['logoID']);
		
		//ici on fait la requete de mise � jour de l'image si on up une nouvelle image
        if (!empty($_FILES['logo_image']['name']) && $logo_image = new upload('logo_image', DIR_FS_CATALOG_IMAGES_LOGOS)) {
		  //on met a jour la base de donn�e et on upload l'image si le champ logo_name n'est pas vide
		  if($logo_image->filename != "")
		  
			$sql_data_array['logo_name'] = $logo_image->filename;
			
        }
		
		$sql_data_array['logo_hover'] = $HTTP_POST_VARS['logo_hover'];
		
        tep_db_perform(TABLE_PRODUCTS_LOGOS, $sql_data_array, 'update', "logo_id = '" . (int)$logo_id . "'");
		
        tep_redirect(tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . 'logoID=' . $logo_id));
        break;
		
     case 'deleteconfirm':
        $logo_id = tep_db_prepare_input($HTTP_GET_VARS['logoID']);

        if (isset($HTTP_POST_VARS['delete_image']) && ($HTTP_POST_VARS['delete_image'] == 'on')) {
          $logo_query = tep_db_query("select logo_name from " . TABLE_PRODUCTS_LOGOS . " where logo_id = '" . (int)$logo_id . "'");
          $logo_info = tep_db_fetch_array($logo_query);

          $image_location = DIR_FS_CATALOG_IMAGES_LOGOS . $logo_info['logo_name'];

          if (file_exists($image_location)) @unlink($image_location);
        }

		tep_db_query("delete from " . TABLE_PRODUCTS_LOGOS ." where logo_id = '" . (int)$logo_id . "'");
		tep_db_query("delete from " . TABLE_PRODUCTS_TO_LOGOS ." where logo_id = '" . (int)$logo_id . "'");
		
        tep_redirect(tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page']));
        break;
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script language="javascript" src="includes/javascript/jQuery/jquery-ui.js"></script>
<script language="javascript" type="application/javascript">

$(document).ready(function() {
	
	$( ".tablesorter tbody" ).sortable({
		revert: true,
		handle: "img.drag",
		update : function () {
			
			var order = $('.tablesorter tbody').sortable('serialize');
			
			$.ajax({
			type: "POST",
			cache: false,
			url: 'includes/javascript/ajax/crud_logo.php?ajax=1&action=order',
			data: order,
			dataType: "json",
			success: function(data) {
				
				$('#lightbox').hide().queue(function() {
					
					currentRow.fadeTo(500, 1);
					//currentRow.fadeTo(500, 1);
					$.dequeue(this);
				});
					
			}
		});
		}
	});
});

</script>
</head>
<body>
<?php require(DIR_WS_INCLUDES . 'header.php');?>

<div id="global">

    <table id="ribbonHeader">
        <thead>
        <tr>
            <th valign="middle" colspan="2">Gestion des logos</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td width="50%">
                    <a href="<?php echo BASE_DIR; ?>articles_logos2.php">Famille de logos</a>
                </td>
                <td class="selected" width="50%">
                    <a href="<?php echo BASE_DIR; ?>articles_logos2.php">Logos</a>
                </td>
            </tr>
        </tbody>
    </table>
	
    <table class="tablesorter" id="sortable">
    	<thead>
        	<tr>
            	<th></th>
            	<th>Nom</th>
                <th>Survol</th>
                <th>Famille</th>
                <th class="actions">Actions</th>
            </tr>
        </thead>
        <tbody>
        <?php
			
			$query = 'select * from logos order by logos_order';
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				echo '<tr id="logo_'. $data['logos_id'] .'">';
					
					echo '<td valign="middle"><img src="'.DIR_WS_IMAGES  .'draggable.jpg" alt="" title="" class="drag"></td>';
					echo '<td>';
						echo '<img src="'. DIR_WS_CATALOG_IMAGES_LOGOS . $data['logos_name'] .'">';
					echo '</td>';
					echo '<td>'. $data['logos_hover'] .'</td>';
					echo '<td>Famille</td>';
					echo '<td class="">';
						echo '<img src="'. DIR_WS_ICONS .'editer.png">';
					echo '</td>';
				echo '</tr>';
			}
		?>
        </tbody>
    </table>
    
</div>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>

<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
