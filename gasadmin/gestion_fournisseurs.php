<?php
/*
  PAGE: gestion_fournisseurs.php
  Version du 13/06/2007
*/

  require('includes/application_top.php');

  if(isset($_POST['new-valid']))
		{
			$ajouts=0;
			for($i=1;$i<=$_POST['new-lignes'];$i++)
				{
					if(!empty($_POST['new'.$i.'-nom']) && !empty($_POST['new'.$i.'-tva']) && !empty($_POST['new'.$i.'-pay']))
						{	
							$requete_ajout='INSERT INTO `suppliers` ( `suppliers_name` , `suppliers_tva` , `suppliers_country` )
											VALUES 					("'.$_POST['new'.$i.'-nom'].'", 
																	 "'.$_POST['new'.$i.'-tva'].'",
																	 "'.$_POST['new'.$i.'-pay'].'");';
							@mysql_query($requete_ajout);
							$ajouts++;
							echo '<div class="infoBoxHeading">'.$requete_ajout.'</div>';
						}
				}
			
			echo '<META HTTP-EQUIV="Refresh" CONTENT="1">';
			echo '<div class="smallText" align="center">'.$ajouts.' fournisseur(s) ajout&eacute;(s) !</div>';
				
			exit(0);
			
		}

	if(isset($_POST['modif-one']))
		{
			echo '<META HTTP-EQUIV="Refresh" CONTENT="1">';
			echo '<div class="smallText" align="center">Fournisseur '.$_POST['modif-one'].' modifi&eacute; !</div>';
			
			$requete_modif='UPDATE suppliers '
						  .'SET ';
						  
			if(!empty($_POST[$_POST['modif-one'].'-nom'])) { $requete_modif.='	suppliers_name="'.$_POST[$_POST['modif-one'].'-nom'].'", '; }
			if(!empty($_POST[$_POST['modif-one'].'-tva'])||($_POST[$_POST['modif-one'].'-tva']=="0")) { $requete_modif.='	suppliers_tva="'.$_POST[$_POST['modif-one'].'-tva'].'", '; }
			if(!empty($_POST[$_POST['modif-one'].'-pay'])) { $requete_modif.='	suppliers_country="'.$_POST[$_POST['modif-one'].'-pay'].'" '; }
									  
			$requete_modif.='WHERE suppliers_id="'.$_POST['modif-one'].'"';
			echo '<div class="infoBoxHeading">'.$requete_modif.'</div>';
			mysql_query($requete_modif);			
			exit(0);
		}
	
	if(isset($_POST['delete-one']))
		{
			echo '<META HTTP-EQUIV="Refresh" CONTENT="1">';
			echo '<div class="smallText" align="center">Fournisseur '.$_POST['delete-one'].' supprim�&eacute; !</div>';
			
			$requete_delete='DELETE '
						   .'FROM suppliers '
						   .'WHERE suppliers_id="'.$_POST['delete-one'].'"';
			echo '<div class="infoBoxHeading">'.$requete_delete.'</div>';
			mysql_query($requete_delete);			
			exit(0);
		}
  
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title>Gestion des fournisseurs - <?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<div id="popupcalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	  <table border="0" width="100%" cellspacing="0" cellpadding="2">
      	<tr>
          <td width="100%">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
            	<td class="pageHeading">
				  Gestion des Fournisseurs
				</td>
          	  </tr>
        	</table>
		  </td>
        </tr>
	   
	    <tr>
	  	  <td align="left" class="main">	
		  	 Cette page permet d'ajouter, de modifier ou suprimer un fournisseur.<br />
			 <br />
		  </td>
        </tr>
	    <tr>
	  	  <td>
		  
	   		<form name="liste" method="post">
			  <table width="100%" border="0" cellpadding="0" cellspacing="1" class="infoBoxContent">
  				<tr align="center" class="formAreaTitle" bgcolor="#CCCCCC">
    			  <td>Nom</td>
    			  <td>TVA</td>
    			  <td>Pays</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
  				</tr>
  
<?    
  $requete_fournisseurs='SELECT suppliers_id as id, suppliers_name as nom
  						 , suppliers_tva as tva, suppliers_country as pays '
						.'FROM suppliers';
  $reponse_fournisseurs=mysql_query($requete_fournisseurs);
    
  while($fetch=mysql_fetch_object($reponse_fournisseurs))
  	{
	  echo    '<tr class="ligne" align="center">'
	  			
				.'<td><input type="text" name="'.$fetch->id.'-nom" value="'.$fetch->nom.'" size="40"/></td>'
				.'<td><input type="text" name="'.$fetch->id.'-tva" value="'.$fetch->tva.'" maxlength="5" size="5" style="text-align:center;" /></td>'
				.'<td><input type="text" name="'.$fetch->id.'-pay" value="'.$fetch->pays.'" /></td>'
				.'<td width="10%">
					<button type="submit" name="modif-one" value="'.$fetch->id.'" title="Modifier">
					   Sauvegarder<img src="../../admin/images/b_edit.png" title="Modifier" alt="Modifier" height="0" width="0">
					</button>
				  </td>
				  <td width="10%">
					<button type="submit" name="delete-one" value="'.$fetch->id.'" title="Effacer">
					   Effacer<img src="../../admin/images/b_drop.png" title="Effacer" alt="Effacer" height="0" width="0">
					</button>
				  </td>'
			  .'</tr>';
  	}
?>

  				<tr class="formAreaTitle">
    			  <td align="center">
					<img src="../admin/images/pixel_black.gif" width="100%" height="1" />
				  </td>
				  <td colspan="2" align="center">
					Nombre de Fournisseurs � ajouter :
				  </td>
    			  <td align="center">
	    			<img src="../admin/images/pixel_black.gif" width="100%" height="1" />
				  </td>
				  <td align="center">
					<? if (isset($_POST['new-add'])) { echo '<input type="hidden" name="new-lignes" value="'.$_POST['new-cmb'].'" />'; }
					else { echo '<input type="hidden" name="new-lignes" value="1" />'; } ?>
					<input type="text" name="new-cmb" maxlength="2" size="2" />
				  </td>
				  <td align="center">
					<button type="submit" name="new-add" title="Ajout de nouveaux fournisseurs">
					  <b>+</b><img src="../../admin/images/plus.gif" width="0" height="0" />
					</button>
				  </td>
  				</tr>

<?
	if(isset($_POST['new-cmb']) && is_numeric($_POST['new-cmb']))
		{
			$cmb=$_POST['new-cmb'];
		}
	else
		{
			$cmb=1;
		}
	
	for($i=1;$i<=$cmb;$i++)
		{
			echo   '<tr align="center">
						<td><input type="text" name="new'.$i.'-nom" size="40" /></td>
						<td><input type="text" name="new'.$i.'-tva" maxlength="5" size="5" style="text-align:center;" /></td>
						<td><input type="text" name="new'.$i.'-pay" /></td>
						<td></td>
						<td></td>
					</tr>';
		}
?>
					<tr>
					  <td colspan="5" align="center">
					    <br />
						<button type="submit" name="new-valid" title="Ajout de nouveaux fournisseurs">
						  Enregistrer les nouveaux fournisseurs
						  <img src="../../admin/images/button_ok.gif" width="0" height="0" />
						</button>
						<br /><br />
					  </td>
					</tr>
				  </table>
				</form>
		  
       
	   	  	  </td>
      		</tr>
          </table>
    	</td>
  	  </tr>


	</table>
  </td>
</tr>

</table>
</td>
<!-- body_text_eof //-->
</tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
