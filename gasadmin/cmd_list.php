<?php
  require('includes/application_top.php');
  require('includes/functions/date.php');
  require('../' . DIR_WS_SITE . '/includes/fonctions/mail.php');
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();
  
  $Nombre_de_ligne_par_page="110";
  if(isset($HTTP_GET_VARS['nb_resultats']) && !empty ($HTTP_GET_VARS['nb_resultats'])){
  	$Nombre_de_ligne_par_page = tep_db_prepare_input($HTTP_GET_VARS['nb_resultats']);
  }
  
  $orders_statuses = array();
  $orders_status_array = array();
  $orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' ORDER BY orders_status_name ");
  while ($orders_status = tep_db_fetch_array($orders_status_query)) {
    $orders_statuses[] = array('id' => $orders_status['orders_status_id'], 'text' => strip_tags($orders_status['orders_status_name']));
    $orders_status_array[$orders_status['orders_status_id']] = strip_tags($orders_status['orders_status_name']);
  }
  
$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
if (tep_not_null($action)) {
	switch ($action) {
		
		case 'envoi_mail' :
			$oID = tep_db_prepare_input($HTTP_GET_VARS['oID']);
			$invoice_query = tep_db_query("select customers_name, customers_email_address, customers_id, date_purchased from " . TABLE_ORDERS . " where orders_id = '" . $oID . "'");
			$row_invoice = tep_db_fetch_array($invoice_query);					
			$facture= WEBSITE."/".DIR_WS_SITE.'/compte/generation_pdf/facture.php?id_com='.$oID.'&cli='.md5($row_invoice['customers_id']);
			
			$email = 'Bonjour <span style="font-weight: bold; color:#AAB41D;">'. ucwords(strtolower($row_invoice['customers_name'])) .'</span>,<br />';
			$email .= '<hr style="color:#AAB41D;">Num�ro de commande : <span style="font-weight:bold;color:#fe5700;">' . $oID . '</span><br />';
			$email .= 'Date de commande : <span style="font-weight:bold;color: rgb(170, 180, 29);">'. tep_date_long($row_invoice['date_purchased']) .'</span><hr style="color:#AAB41D;"><br />';
			
			$email .= 'Vous pouvez acc�der � votre facture <a href="'. WEBSITE ."/". DIR_WS_SITE .'/compte/generation_pdf/facture.php?id_com='. $oID .'&cli='.md5($row_invoice['customers_id']) .'" style="color:#FE5700;font-weight:bold;">en cliquant ici</a><br /><br />'. SIGNATURE_MAIL;
			
			mail_client_commande($row_invoice['customers_email_address'], "Votre facture ". STORE_NAME, $email);
		
		break;
		
		case 'deleteconfirm':
			$oID = tep_db_prepare_input($HTTP_GET_VARS['oID']);
		
			(estDevis($oID)) ? tep_remove_order($oID, FALSE) : tep_remove_order($oID, TRUE);
		
			tep_redirect(tep_href_link(FILENAME_CMD_LIST, tep_get_all_get_params(array('oID', 'action'))));
		break;
		
		case 'edit':
			tep_redirect(tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params()));
		break;
	}
}

include(DIR_WS_CLASSES . 'order.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/function.js"></script>
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/metadata.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
    // call the tablesorter plugin
    $(".tablesorter").tablesorter();
});
</script>

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<div id="popupcalendar" class="text"></div>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<?php // DEBUT ZONE DES FILTRES DES COMMANDES ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr> 
    <td width="100%" valign="top">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" height="25" style="background-color: #d8d8eb;">
            <tr>
            <td class="smallText" align="left" width="50%">
              <?php echo tep_draw_form('orders', FILENAME_CMD_EDIT, '', 'get'); 
                    echo '<b>&nbsp;&nbsp;ID Commande :&nbsp;&nbsp;</b>'.tep_draw_input_field('oID', '', 'size="12" id="focusorders"') . tep_draw_hidden_field('action', 'edit'); ?>
              </form>
              <script type="text/javascript"> document.orders.focusorders.focus(); </script>
            </td>
            <td class="smallText" align="right" width="50%">
            <?php 
            echo tep_draw_form('Filtres', 'cmd_list.php', '', 'get'); 
            echo '<b>Statut :&nbsp;</b>'.tep_draw_pull_down_menu('status', array_merge(array(array('id' => '', 'text' => 'Toutes les commandes')), $orders_statuses), '', 'onChange="this.form.submit();"').'&nbsp;&nbsp;';
            ?>
            </td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #e1e1f5;">
		<tr>
        	<td align="center">
            	<table width="95%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
						<td align="left" class="smallText" height="40">  
						<?php 	$nb_resultats_array = array();
								$nb_resultats_array[] = array('id' => 100, 'text' => "100");
								$nb_resultats_array[] = array('id' => 200, 'text' => "200");
								$nb_resultats_array[] = array('id' => 300, 'text' => "300");
								$nb_resultats_array[] = array('id' => 500, 'text' => "500");
								$nb_resultats_array[] = array('id' => 1000, 'text' => "1000");
								echo "Nombre de Commande par page :&nbsp;".tep_draw_pull_down_menu('nb_resultats', $nb_resultats_array, '', 'onChange="this.form.submit();"');?>
						</td>
						<td align="left" class="smallText">        
						<?php	$type_client_array = array();
								$type_query = tep_db_query("select type_id, type_name from " . TABLE_CUSTOMERS_TYPE . " order by type_id  ");
								$type_client_array[] = array('id' => "", 'text' => "Tous");
								while ($type_values = tep_db_fetch_array($type_query)) {
								  $type_client_array[] = array('id' => $type_values['type_id'], 'text' => $type_values['type_name']);
								}
								echo "Type de client :&nbsp;".tep_draw_pull_down_menu('type_client', $type_client_array, '', 'onChange="this.form.submit();"');?>
						</td>
						<td align="left" class="smallText" colspan="2">        
						<?php	$zones_array = array();
								$zones_query = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " order by countries_name  ");
								$zones_array[] = array('id' => "", 'text' => "Tous");
								$zones_array[] = array('id' => "NON_FRANCE", 'text' => "Hors de France");
								$zones_array[] = array('id' => "DOM", 'text' => "DOM");
								$zones_array[] = array('id' => "TOM", 'text' => "TOM");
								while ($zones_values = tep_db_fetch_array($zones_query)) {
								  $zones_array[] = array('id' => $zones_values['countries_name'], 'text' => $zones_values['countries_name']);
								}
								echo "Pays de livraison :&nbsp;".tep_draw_pull_down_menu('pays', $zones_array, '', 'onChange="this.form.submit();"');?>
						</td>
						
                    </tr>
					
					
					
                    <tr>
						<td align="left" class="smallText" valign="top" height="40">        
							<?php	echo "Du : ".tep_draw_input_field('date_deb', '', 'size="10" id="focusorders"');?>
							<a href="#" onClick="displayCalendar(Filtres.date_deb,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
							<?php echo "Au&nbsp;".tep_draw_input_field('date_fin', '', 'size="10" id="focusorders"');?>
							<a href="#" onClick="displayCalendar(Filtres.date_fin,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
						</td>
						<td align="left" class="smallText" valign="top">        
							<?php
							echo "Prix de :&nbsp;" . tep_draw_input_field('prix_commande_min', '', 'size="5" id="focusorders"').'&nbsp;';
							echo "�&nbsp;" . tep_draw_input_field('prix_commande_max', '', 'size="5" id="focusorders"').'&nbsp;�';
							?>
						</td>
						<td align="left" class="smallText" valign="top">        
						<?php
								echo "Marge de :&nbsp;" . tep_draw_input_field('marge_commande_min', '', 'size="5" id="focusorders"').'&nbsp;';
								echo "�&nbsp;" . tep_draw_input_field('marge_commande_max', '', 'size="5" id="focusorders"').'&nbsp;�';?>
						</td>
						<td align="left" class="smallText" valign="top"><?php	$avec_facture_array = array();
								$avec_facture_array[] = array('id' => "", 'text' => "Tout type");
								$avec_facture_array[] = array('id' => 1, 'text' => "Sans Facture");
								$avec_facture_array[] = array('id' => 2, 'text' => "Avec Facture");
								echo "Cmd avec facture :&nbsp;".tep_draw_pull_down_menu('cmd_facture', $avec_facture_array, '', 'onChange="this.form.submit();"');?>
						</td>
                    </tr>

					<tr>
						<td align="left" class="smallText" valign="top" height="40">        
						<?php	echo 'Num�ro de Suivi :&nbsp;'.tep_draw_input_field('num_suivi', '', 'size="20" id="focusorders"');?>
						</td>
						<td align="left" class="smallText" valign="top">        
						<?php	echo 'Num�ro de Facture :&nbsp;'.tep_draw_input_field('num_facture', '', 'size="20" id="focusorders"');?>
						</td>
						<td align="left" class="smallText" valign="top">        
						<?php	echo 'Nom soci�t� :&nbsp;'.tep_draw_input_field('customers_company', '', 'size="20" id="focusorders"');?> 
						
						</td>
						<td align="left" class="smallText" valign="top">
							<?php	echo 'Nom client :&nbsp;'.tep_draw_input_field('customers_name', '', 'size="20" id="focusorders"');?>
							<input type=submit value="Go">
							</form>
						</td>
						
                    </tr> 
				</table>
        	</td>
        </tr>
        </table>
        </td>
	  </tr>
	  <?php // FIN ZONE DES FILTRES DES COMMANDES ?>
	  <tr>
		<td>
          <table border="0" width="100%" cellspacing="0" cellpadding="0">
          	<tr><td>&nbsp;</td></tr>
            <tr>
          		<td valign="top">
				<?php echo tep_draw_form('batch_orders', 'OSC_Expeditor_process.php', '', 'post', 'target="_blank"'); ?>
					<table class="tablesorter" cellspacing="0" cellpadding="2" border="0">
              		<thead>
                      <tr>
                      <th class="{sorter:false}" style="text-align:center;"><input type="checkbox" value="Check All" onClick="this.value=checkAll(this.form.list,this.checked)"></th>
                      <th>Type</th>
                      <th>Pays</th>
                      <th>IP cmd</th>
                      <th style="text-align:center;">ID</th>
                      <th>Client</th>
                      <th>Soci�t�</th>
                      <th class="{sorter:'digit'}">Total</th>
                      <th class="{sorter:'digit'}">Marge</th>
                      <th>Date d'achat</th>
                      <th>Mode Paiement</th>
                      <th>Statut</th>
                      <th>Reliquats</th>
                      <th class="{sorter:false}" align="center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
<?php
	
    if(isset($HTTP_GET_VARS['cID']) && !empty($HTTP_GET_VARS['cID'])) {
      	$cID = tep_db_prepare_input($HTTP_GET_VARS['cID']);
      	$where .= " and o.customers_id = '".(int)$cID."'";
    } 
	if(isset($HTTP_GET_VARS['status']) && !empty($HTTP_GET_VARS['status'])) {
    	$status = tep_db_prepare_input($HTTP_GET_VARS['status']);
      	$where .= " and s.orders_status_id = '".(int)$status."'";
    }
	if(isset($HTTP_GET_VARS['num_suivi']) && !empty($HTTP_GET_VARS['num_suivi'])) {
		
		$from = TABLE_ORDERS_STATUS_HISTORY ." osh, ";
		
    	$num_suivi = tep_db_prepare_input($HTTP_GET_VARS['num_suivi']);
      	$where .= " and o.orders_id=osh.orders_id and (track_num = '". $num_suivi ."' or track_num2 = '". $num_suivi ."' or track_num3 = '". $num_suivi ."' or track_num4 = '". $num_suivi ."' or track_num5 = '". $num_suivi ."')";
    }
	if(isset($HTTP_GET_VARS['num_facture']) && !empty($HTTP_GET_VARS['num_facture'])) {
    	$num_facture = tep_db_prepare_input($HTTP_GET_VARS['num_facture']);
      	$where .= " and o.orders_numero_facture like '%".(int)$num_facture."%'";
    }
	if(isset($HTTP_GET_VARS['pays']) && !empty($HTTP_GET_VARS['pays'])) {
		$pays = tep_db_prepare_input($HTTP_GET_VARS['pays']);
      	if($pays=="NON_FRANCE"){
			$where .= " and o.delivery_country<> 'France'";
		}
		elseif($pays=="DOM"){
			$where .= " and (o.delivery_country = 'Martinique' || o.delivery_country = 'Guyane Fran�aise'  || o.delivery_country = 'Guadeloupe' || o.delivery_country = 'R�union' )";
		}
		elseif($pays=="TOM"){
			$where .= " and (o.delivery_country = 'Mayotte' || o.delivery_country = 'Nouvelle Cal�donie'  || o.delivery_country = 'Polyn�sie Fran�aise' || o.delivery_country = 'Saint Pierre et Miquelon' )";
		}
		else{
			$where .= " and o.delivery_country = '".$pays ."'";
		}
	}
	if(isset($HTTP_GET_VARS['date_deb']) && !empty($HTTP_GET_VARS['date_deb'])) {
		$date_deb = tep_db_prepare_input(date_fr_to_bdd($HTTP_GET_VARS['date_deb']));
      	$where .= " and o.date_purchased > '".$date_deb ."'";
	}
	if(isset($HTTP_GET_VARS['date_fin']) && !empty($HTTP_GET_VARS['date_fin'])) {
		$date_fin = tep_db_prepare_input(date_fr_to_bdd($HTTP_GET_VARS['date_fin']));
      	$where .= " and o.date_purchased < '".$date_fin." 23:59:59'";
	}
	if(isset($HTTP_GET_VARS['customers_company']) && !empty($HTTP_GET_VARS['customers_company'])) {
		$customers_company = tep_db_prepare_input($HTTP_GET_VARS['customers_company']);
      	$where .= " and o.customers_company like '%".$customers_company ."%'";
	}
	/**/
	if(isset($HTTP_GET_VARS['customers_name']) && !empty($HTTP_GET_VARS['customers_name'])) {
		$customers_name = tep_db_prepare_input($HTTP_GET_VARS['customers_name']);
      	$where .= " and o.customers_name like '%".$customers_name ."%'";
	}
	/**/
	if(isset($HTTP_GET_VARS['prix_commande_min']) && !empty($HTTP_GET_VARS['prix_commande_min'])) {
		$prix_commande_min = tep_db_prepare_input($HTTP_GET_VARS['prix_commande_min']);
      	$where .= " and o.total > ".$prix_commande_min;
	}
	if(isset($HTTP_GET_VARS['prix_commande_max']) && !empty($HTTP_GET_VARS['prix_commande_max'])) {
		$prix_commande_max = tep_db_prepare_input($HTTP_GET_VARS['prix_commande_max']);
      	$where .= " and o.total < ".$prix_commande_max;
	}
	if(isset($HTTP_GET_VARS['marge_commande_min']) && !empty($HTTP_GET_VARS['marge_commande_min'])) {
		$marge_commande_min = tep_db_prepare_input($HTTP_GET_VARS['marge_commande_min']);
      	$where .= " and o.marge > ".$marge_commande_min;
	}
	if(isset($HTTP_GET_VARS['marge_commande_max']) && !empty($HTTP_GET_VARS['marge_commande_max'])) {
		$marge_commande_max = tep_db_prepare_input($HTTP_GET_VARS['marge_commande_max']);
      	$where .= " and o.marge < ".$marge_commande_max;
	}
	
	if(isset($HTTP_GET_VARS['type_client']) && !empty($HTTP_GET_VARS['type_client'])) {
		$type_client = tep_db_prepare_input($HTTP_GET_VARS['type_client']);
		$where .= " and c.customers_type='" . $type_client . "'";
	}
	if (isset($HTTP_GET_VARS['cmd_facture']) && tep_not_null($HTTP_GET_VARS['cmd_facture'])) {
      	$cmd_facture = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['cmd_facture']));
      	if($cmd_facture==1){//sans facture
			$where .= " and o.orders_numero_facture=0";
		}
		if($cmd_facture==2){//avec facture
			$where .= " and o.orders_numero_facture>0";
		}
    }
	$ordre = " order by o.orders_id DESC";
	
	if (isset($HTTP_GET_VARS['ordre']) && tep_not_null($HTTP_GET_VARS['ordre'])) {
      	$ordre = " order by ".tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['ordre']))." ".tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['type_ordre']));
	}
	
	$orders_query_raw="SELECT o.orders_id, o.customers_id, o.delivery_country, o.origine, o.customers_name, o.customers_company, o.customers_id, o.customer_ip_country, o.customer_ip, o.payment_method, o.total, o.marge, o.remise, o.date_purchased, o.last_modified, o.currency, o.currency_value, o.marge, s.orders_status_name, o.orders_numero_facture, o.orders_status
										 from " . TABLE_ORDERS . " o, " . TABLE_ORDERS_STATUS . " s, ". $from . TABLE_CUSTOMERS . " c 
										 where o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' and o.customers_id = c.customers_id";
										 
	//$orders_query_raw .= " group by o.orders_id ".$ordre;
	$orders_query_raw .= $where . $ordre;
    
	$orders_split = new splitPageResults($HTTP_GET_VARS['page'], $Nombre_de_ligne_par_page, $orders_query_raw, $orders_query_numrows);
    $orders_query = tep_db_query($orders_query_raw);
	
	$style = '';
	// on recupere toutes les commandes de la page
	while ($orders = tep_db_fetch_array($orders_query)) {
			
		// Si une commande est en statut Traitement en cours (ID 1) OU en Reliquat (ID 23) 
		if ($orders['orders_status']=='1' || $orders['orders_status']=='15' || $orders['orders_status']=='23' || $orders['orders_status']=='30') {
			// On fait une requ�te pour compter les commandes d'un client qui sont en statut Traitement en cours (ID 1) OU en Reliquat (ID 23) OU Rupture (ID 15) OU probl�me de livraison (ID 30)
			$query = 'select count(*) as total, orders_status from '. TABLE_ORDERS .'
				where customers_id='. $orders['customers_id'] .'
				and (orders_status=1 or orders_status=23 or orders_status=15 or orders_status=30)
				and orders_id!='. $orders['orders_id'];
			// R�sultat de la requ�te
			$query = tep_db_query($query);
			// On en fait un tableau
			$data = tep_db_fetch_array($query);

			//on applique un style VERT aux num�ros des commande si le client a des commandes en Traitement en cours OU Reliquat OU Rupture 
			$style = ($data['total']>0) ? 'style="font-weight:bold; color:#0C0" ' : '';
			
		} else { 
			// Sinon un applique pas de style
			$style = '';
		}
			
    	if ((!isset($HTTP_GET_VARS['oID']) || (isset($HTTP_GET_VARS['oID']) && ($HTTP_GET_VARS['oID'] == $orders['orders_id']))) && !isset($oInfo)) {
    		$oInfo = new objectInfo($orders);
      	}
      	if (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)) {
        	echo '  <tr id="defaultSelected" class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" >' . "\n";
        	$link = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id . '&action=edit') . '\'"';
	  	} 
	  	else {
        	echo '<tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
        	$link = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('oID')) . 'oID=' . $orders['orders_id'] . '&action=edit') . '\'"';
	  	}
		// Code pour mettre des icones en fontion du TYPE de client
		$customers_type_query = tep_db_query("select customers_id,customers_type from " . TABLE_CUSTOMERS . " where customers_id = '" . $orders["customers_id"] . "'");
		$row_client = mysql_fetch_array($customers_type_query);												
		//if(!isset($_REQUEST["type_client"]) || (isset($_REQUEST["type_client"]) && $_REQUEST["type_client"]== $row_client['customers_type']) ){			
		?>
					  <td class="dataTableContent" align="center">
						<?php echo"<input id='list' name='batch_order_numbers[".$orders['orders_id']."]' type='checkbox' value='".$orders['orders_id']."' />"; ?>
						<?php if (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)){ 
								//echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); 
							  }
							  else { 
								echo '<a href="'.tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('oID')).'oID='.$orders['orders_id']) . '">'. '</a>'; 
							  }?>
					  </td>
                      <td class="dataTableContent" align="center">
                        <?php // Code pour mettre des icones en fontion du TYPE de client
							if($row_client['customers_type'] == '1'){
								echo '<img src="images/icons/icone_particulier.gif" alt="Client Particulier" title="Client Particulier">';
							}
							else if($row_client['customers_type'] == '2'){
								echo '<img src="images/icons/icone_professionnel.gif" alt="Client Professionnel" title="Client Professionnel">';
							}
							else if($row_client['customers_type'] == '3'){
								echo '<img src="images/icons/icone_revendeur.gif" alt="Client Revendeur" title="Client Revendeur">';
							}
							else if($row_client['customers_type'] == '4'){
								echo '<img src="images/icons/icone_administration.gif" alt="Client Administration" title="Client Administration">';
							}?>
                      </td>
                      <td class="dataTableContent" align="center">
						<?php
                        // Code pour mettre des icones en fontion du Pays de livraison 
                        echo get_drapeau($orders['delivery_country']);					
                        ?>
					  </td>
                      <!-- Affichage du pays de la commande d�termin� par l'IP -->
                      <td class="dataTableContent" align="center">
						<?php
                        // Code pour mettre des icones en fontion du Pays de la commande
						if (isset($orders['customer_ip_country']) && $orders['customer_ip_country'] != ''){
							echo "<a href=\"https://en.utrace.de/?query=" . $orders['customer_ip'] . "\" target=\"_blank\">".get_drapeau($orders['customer_ip_country'])."</a>";
						} 
						else {
							echo "<a href=\"https://en.utrace.de/?query=" . $orders['customer_ip'] . "\" target=\"_blank\">&nbsp;</a>";
						}
                        ?>
					  </td>
                      <td <?php echo $link; ?> class="dataTableContent" align="center">
						<?php echo '<a href="'.tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('oID', 'action')).'oID='.$orders['orders_id'].'&action=edit').'" '. $style .'>'.$orders['orders_id'].'</a>&nbsp;'; ?>
					  </td>
					  <td <?php echo $link; ?> class="dataTableContent"><?php echo $orders['customers_name']; ?></td>
					  <td class="dataTableContent">
						<?php echo '<a href="'.tep_href_link(FILENAME_CMD_LIST, tep_get_all_get_params(array('cID'))).'&cID='.$orders['customers_id'].'" target="_blank" title="Voir Toutes les commandes du client">'.$orders['customers_company'].'</a>'; ?>
					  </td>
					  <td <?php echo $link; ?> class="dataTableContent" align="left"><?php echo $orders['total']; ?> � 
 					  	<?php // Code qui affiche le montant du coupon d'une commande.
						if($orders['remise']!=0.00){
							echo '<font color="RED">' . $orders['remise'] .'</font>';
						}?>
					  </td>
                      
                      <?php
					  $marge_couleur = "style=\"color: #000; font-weight: normal;\"";
                      if ($orders["marge"] < $marge_orange)
						  $marge_couleur = "style=\"color: #F60; font-weight: bold;\"";
					  if ($orders["marge"] < $marge_rouge)
						  $marge_couleur = "style=\"color: #F00; font-weight: bold;\"";
					  ?>
                      <td class="dataTableContent" align="left" <?php echo $marge_couleur; ?>><?php echo $orders["marge"];?> �</td>
					  <td <?php echo $link; ?> class="dataTableContent" align="left"><?php echo tep_datetime_short($orders['date_purchased']); ?></td>
					  <td <?php echo $link; ?> class="dataTableContent" align="center">				
						<?php // Code pour mettre des icones en fontion du mode de paiement 
							if(substr_count($orders['payment_method'],"Carte")>0){
								echo "<img src='images/icons/carte_bleue.gif' title='Paiement CB'>";
							}
							else if(substr_count(strtoupper($orders['payment_method']),strtoupper("Ch�que"))>0){
								echo "<img src='images/icons/cheque.gif' title='Paiement Ch�que'>";
							}
							else if(substr_count($orders['payment_method'],"Virement")>0){
								echo "<img src='images/icons/virement.gif' title='Paiement Virement'>";
							}
							else if(substr_count($orders['payment_method'],"PayPal")>0){
								echo "<img src='images/icons/paypal.gif' title='Paiement Paypal'>";
							}
							else if(substr_count($orders['payment_method'],"Esp�ce")>0){
								echo "<img src='images/icons/espece.gif' title='Paiement Esp�ce'>";
							}
							else if(substr_count($orders['payment_method'],"Mandat Cash")>0){
								echo "<img src='images/icons/mandat-cash.gif' title='Paiement Mandat Cash'>";
							}
							else if(substr_count($orders['payment_method'],"Mandat Administratif")>0){
								echo "<img src='images/icons/mandat-administratif.gif' title='Paiement Mandat Administratif'>";
							}
							else if(substr_count($orders['payment_method'],"Porte")>0){
								echo "virtuel";
							}
						?>
					
					  </td>
					  <td <?php echo $link; ?> class="dataTableContent" align="left"><?php echo $orders['orders_status_name']; ?></td>
                      <td <?php echo $link; ?> class="dataTableContent" align="center">
                      	<?php 
							$relicats_query = tep_db_query("SELECT SUM(products_quantity) as qte_totale, SUM(products_quantity_sent) as qte_totale_livree FROM " . TABLE_ORDERS_PRODUCTS . " WHERE orders_id = '" . $orders['orders_id'] . "'");
                      		$relicats = tep_db_fetch_array($relicats_query);
							
							if ($relicats['qte_totale'] != $relicats['qte_totale_livree'] && $relicats['qte_totale_livree'] != 0) {
								echo '<a href="'.tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('oID', 'action')).'oID='.$orders['orders_id'].'&action=edit').'"><strong style="color: red;">R</strong></a>';
							}
						?>
                      &nbsp;</td>
					  <td class="dataTableContent" align="right">
						<?php echo '<a href="'.tep_href_link(FILENAME_CMD_EDIT, 'oID='.$orders['orders_id']).'&action=edit">'.tep_image(DIR_WS_IMAGES.'icons/editer.png', "Editer").'</a>';?>
    					| 
    					<?php // ICONE  POUR LA FACTURE PDF BOUTIQUE DES ACCESSOIRES 
    					echo  '<a href="/'.DIR_WS_SITE.'/compte/generation_pdf/facture.php?id_com='.$orders['orders_id'].'&cli='.md5($orders['customers_id']).'" target="_blank">' . tep_image(DIR_WS_IMAGES . 'icons/facture.gif',"Facture") . '</a>'; 
    					?>
                        |
    					<?php // ICONE POUR envoi mail 
    					echo '<a href="'.tep_href_link(FILENAME_CMD_LIST, tep_get_all_get_params(array('oID', 'action')).'oID='.$orders['orders_id'].'&action=envoi_mail').'" onclick="if (window.confirm(\'Etes vous sur de vouloir envoyer la facture au client?\')){return true;} else {return false;}" title="Envoyer la facture par mail">						'. tep_image(DIR_WS_IMAGES . 'icons/facture_mail.png',"Envoyer la facture par mail") .'</a>'; ?>
                        |
						<?php echo '<a href="cmd_bon_de_livraison.php?oID='.$orders['orders_id'].'" target="_blank">' . tep_image(DIR_WS_IMAGES . 'icons/bon_de_livraison.gif',"Bon de livraison") . '</a>';?>
    					| 
    					<?php // ICONE POUR SUPPRIMER Visible UNIQUEMENT pour le compte administrateur 
							if ($orders['orders_numero_facture'] == 0 || $myAccount["admin_groups_id"]== 1) {
								echo '<a href="'.tep_href_link(FILENAME_CMD_LIST, tep_get_all_get_params(array('oID', 'action')).'oID='.$orders['orders_id'].'&action=deleteconfirm').'" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer la commande : '.$orders['orders_id'].' (DEFINITIVEMENT) ?\')){return true;} else {return false;}">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>';
							} else {
								echo tep_image(DIR_WS_IMAGES . 'icons/supprimer_inactif.png', "Impossible de supprimer");
							}
						?>
					  </td>
					</tr>
		<?php
        $batch_order_numbers[] = $orders['orders_id'];
	}?>
				</tbody>
				</table>
                    
				<table border="0" width="100%" cellspacing="0" cellpadding="2">
					<tr>
						<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
					<tr>
						<td class="smallText"  align="left">
						<?php // les exports sont g�r� dans la page OSC_Expeditor_process.php puis dans la page qui g�n�re la liste ?>
						<b>Action sur commandes s�l�ctionn�es :</b>
						<select size="1" id="Action_orders" name="Action_orders">
							<option value="pavisverifies3j">Export CMD et Produits pour Avis V�rifi�s (5j)</option>
							<option value="pavisverifies10j">Export CMD et Produits pour Avis V�rifi�s (12j)</option>
							<option value="expeditor">Export vers COLISSIMO</option>
							<option value="dymo">Export vers DYMO</option>
							<option value="Liste des articles des cmd">Lister les articles des commandes</option>
							<option value="Liste des articles des cmd fournisseur">Lister les articles des commandes fournisseur</option>
							<option value="MAJ numero colissimo">Mise a Jour numero suivi</option>
							<!--<option value="MAJ numero distingo">Mise a Jour numero distingo</option>
							<option value="MAJ numero chronopost">Mise a Jour numero chronopost</option>
							<option value="MAJ numero dpd">Mise a Jour numero DPD</option>
							<option value="MAJ numero ups">Mise a Jour numero UPS</option>-->
						</select>
						<?php echo tep_image_submit('button_confirm.gif', IMAGE_CONFIRM, 'align="top"');?>
					  </td>
				  	</tr>
				</table>
				</td>
			</tr>
			</form>
    		<tr>
            	<td colspan="12">
                  	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
          			<tr>
                      <td class="smallText" width="33%"><?php echo $orders_split->display_count($orders_query_numrows, $Nombre_de_ligne_par_page, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS); ?></td>
                      <td class="smallText" width="33%" align="center"><?php echo $orders_split->display_links($orders_query_numrows, $Nombre_de_ligne_par_page, MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page'], tep_get_all_get_params(array('page', 'oID', 'action'))); ?></td>
                      <td class="smallText" width="33%">&nbsp;</td>
                  	</tr>
                	</table>
                </td>
            </tr>
            </table></td>
          	</tr>
          </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?><br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>