<?php
require('includes/application_top.php');

// Envoie un mail � un client lors de la demande de mot de passe 
function mail_client($mail,$sujet,$mess){
	$to  = $mail; // Destinataires
	$subject = $sujet; // Sujet
	
	//Corps du Message au format HTML
	$message = '<body>
					<table style="margin:auto; width:800px; border:0;" cellpadding="0" cellspacing="0">
						<tbody>
							<tr>
								<td colspan="2"style="width:800px; height:124px;">
									<a href="'. WEBSITE .'"><img src="'. WEBSITE . BASE_DIR .'/template/base/mail/header800.jpg" alt="Activer l\'affichage des images de votre service email." style="width:800px; border:0; height:121px"></a>
								</td>
							</tr>
							<tr bgcolor="#4f4f4f">
								<td style="color:white; padding-right: 20px;padding-top: 17px;padding-left: 17px; padding-bottom:17px;" bgcolor="#4f4f4f">
				';
	$message.=$mess;
	$message.='					</td>
							</tr>
							<tr>
								<td colspan="2" style="width:800px; height:29px;">
									<img src="'. WEBSITE . BASE_DIR .'/template/base/mail/footer-vert.jpg" style="width:800px; height:29px;" alt="Pensez &agrave; activer l\'affichage des images de votre service mail.">
								</td>
							</tr>
						</tbody>
					</table>
				</body>';
				
     // Destinataire de l'email
     $headers ='From: "General Army Store"<' . MAIL_INFO . '>'."\n";
     $headers .='Reply-To: ' . MAIL_INFO ."\n";
     $headers .='Content-Type: text/html; charset="iso-8859-1"'."\n";
     
     // Fonction php mermettant l'envoi de l'email
     mail($to, $subject, $message, $headers);
}

if (isset($_REQUEST['new_mot_de_passe']) && !empty($_REQUEST['new_mot_de_passe']) && $_REQUEST['new_mot_de_passe'] = $_REQUEST['new_mot_de_passe_confirm']) {
	$mdp_crypte = tep_encrypt_password($_REQUEST['new_mot_de_passe']);
	$nom = ucfirst(strtolower($_REQUEST['customers_lastname']));
	$email = $_REQUEST['customers_email_address'];
	$mdp = $_REQUEST['new_mot_de_passe'];
	
	$exe_req = tep_db_query('UPDATE customers SET customers_password = "' . $mdp_crypte . '" WHERE customers_email_address = "' . $email . '"');
	
	if ($_REQUEST['customers_gender'] == 'm') {
		$gender = "Monsieur";
	} else if ($_REQUEST['customers_gender'] == 'f') {
		$gender = "Madame";
	} else if ($_REQUEST['customers_gender'] == 'd') {
		$gender = "Mademoiselle";
	}
	$sujet = SUJET_NOUVEAU_MDP;
	
	$message = '<p style="font-size: 11px; font-family: Verdana,Arial,Helvetica,sans-serif; color: rgb(255, 255, 255);">
				Bonjour '.$gender.' <span style="font-weight: bold; color: #DC2213;">'.$nom.'</span>,
				<br />
				<br />
				vous venez de nous signaler la perte de vos identifiants pour vous connecter � notre site.
				<br />
				<br />
				Suite � cela, nous vous envoyons un nouveau mot de passe qui est le suivant : <span style="font-weight: bold; color: #DC2213;">'.$mdp.'</span><br />
				<br />
				Pour des raisons de s�curit�, il vous sera demand� de le changer le plus rapidement possible. 
				<br />
				Vous pouvez, pour cela, vous rendre sur votre compte en cliquant <a style="color:#4E87B4;font-weight:bold;" href="'. WEBSITE . BASE_DIR .'/compte/connexion.php">ici</a>.
				<br />
				<br />
				' . SIGNATURE_MAIL . '
				</p>';
	mail_client($email,$sujet,$message);
}

$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
$error = false;
$processed = false;

if(empty($action)){
	$action="edit";
}

if (tep_not_null($action)) {
	switch ($action) {
		case 'update':
			$customers_id = tep_db_prepare_input($HTTP_GET_VARS['cID']);
			$customers_firstname = tep_db_prepare_input($HTTP_POST_VARS['customers_firstname']);
			$customers_lastname = tep_db_prepare_input($HTTP_POST_VARS['customers_lastname']);
			$customers_email_address = tep_db_prepare_input($HTTP_POST_VARS['customers_email_address']);
			$customers_telephone = tep_db_prepare_input(nettoie_tel($HTTP_POST_VARS['customers_telephone']));
			$customers_fixe = tep_db_prepare_input($HTTP_POST_VARS['customers_fixe']);
			$customers_fax = tep_db_prepare_input($HTTP_POST_VARS['customers_fax']);
			$customers_tva = tep_db_prepare_input($HTTP_POST_VARS['customers_tva']);
			$customers_siret = tep_db_prepare_input($HTTP_POST_VARS['customers_siret']);
			$customers_important = tep_db_prepare_input($HTTP_POST_VARS['customers_important']);
			$customers_newsletter = tep_db_prepare_input($HTTP_POST_VARS['customers_newsletter']);
			$customers_news_sms = tep_db_prepare_input($HTTP_POST_VARS['customers_news_sms']);
			$customers_admin = tep_db_prepare_input($HTTP_POST_VARS['customers_admin']);
			$customers_type = tep_db_prepare_input($HTTP_POST_VARS['customers_type']);
			$customers_comment = tep_db_prepare_input($HTTP_POST_VARS['customers_comment']);
			$customers_gender = tep_db_prepare_input($HTTP_POST_VARS['customers_gender']);
			$customers_dob = tep_db_prepare_input($HTTP_POST_VARS['customers_dob']);
			$customers_company = tep_db_prepare_input($HTTP_POST_VARS['entry_company']);
			$customers_company_type = tep_db_prepare_input($HTTP_POST_VARS['customers_company_type']);
			$customers_company_type_libre = tep_db_prepare_input($HTTP_POST_VARS['customers_company_type_libre']);

			$nb_adresse=$_POST['compteur'];
			for($i=0;$i<$nb_adresse;$i++){
				$address_id = tep_db_prepare_input($HTTP_POST_VARS['id_adresse'.$i]);
				$entry_lastname = tep_db_prepare_input($HTTP_POST_VARS['nom'.$i]);
				$entry_firstname = tep_db_prepare_input($HTTP_POST_VARS['prenom'.$i]);
				$entry_company = tep_db_prepare_input($HTTP_POST_VARS['societe'.$i]);
				$entry_address_title = tep_db_prepare_input($HTTP_POST_VARS['titre'.$i]);
				$entry_street_address = tep_db_prepare_input($HTTP_POST_VARS['adresse'.$i]);
				$entry_suburb = tep_db_prepare_input($HTTP_POST_VARS['complement'.$i]);   
				$entry_street_address_3 = tep_db_prepare_input($HTTP_POST_VARS['adresse_3'.$i]);
				$entry_street_address_4 = tep_db_prepare_input($HTTP_POST_VARS['adresse_4'.$i]);
				$entry_postcode = tep_db_prepare_input($HTTP_POST_VARS['codepostal'.$i]);
				$entry_city = tep_db_prepare_input($HTTP_POST_VARS['ville'.$i]);
				$entry_country_id = tep_db_prepare_input($HTTP_POST_VARS['pays'.$i]);
				$default_address = tep_db_prepare_input($_POST['default_address']);
				
				if($entry_suburb==''){ $entry_suburb=NULL; }
				if($entry_company==''){ $entry_company=NULL; }
				
				if($error==false){
					$req=tep_db_query("UPDATE address_book SET 
					entry_address_title='".addslashes($entry_address_title) ."', 
					entry_company='".addslashes($entry_company)."', 
					entry_firstname='".addslashes($entry_firstname)."', 
					entry_lastname='".addslashes($entry_lastname)."', 
					entry_street_address='".addslashes($entry_street_address)."', 
					entry_suburb='".addslashes($entry_suburb)."', 
					entry_street_address_3='".addslashes($entry_street_address_3)."', 
					entry_street_address_4='".addslashes($entry_street_address_4)."', 
					entry_postcode='".$entry_postcode."', 
					entry_city='".addslashes($entry_city)."', 
					entry_country_id='".$entry_country_id."' 
					WHERE 
					address_book_id=".$address_id."");
				}
			}
		
			if (!empty($_POST['titre']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['adresse']) && !empty($_POST['codepostal']) && !empty($_POST['ville'])) {
			$req=tep_db_query("	INSERT INTO ".TABLE_ADDRESS_BOOK." 
								(
									customers_id, 
									entry_address_title, 
									entry_company, 
									entry_firstname, 
									entry_lastname, 
									entry_street_address_3, 
									entry_suburb, 
									entry_street_address, 							
									entry_street_address_4, 
									entry_postcode, 
									entry_city, 
									entry_country_id
								) values (
									".$customers_id.", 
									'".addslashes($_POST['titre']) ."', 
									'".addslashes($_POST['societe'])."',
									'".addslashes($_POST['prenom'])."' , 
									'".addslashes($_POST['nom'])."', 
									'".addslashes($_POST['adresse_3'])."', 
									'".addslashes($_POST['complement'])."',
									'".addslashes($_POST['adresse'])."',									
									'".addslashes($_POST['adresse_4'])."', 
									'".$_POST['codepostal']."', 
									'".addslashes($_POST['ville'])."', 
									'".$_POST['pays'.$i]."')"
								);
			}
		
			if (isset($HTTP_POST_VARS['entry_zone_id'])) $entry_zone_id = tep_db_prepare_input($HTTP_POST_VARS['entry_zone_id']);

			if (strlen($customers_firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
				$error = true;
				$entry_firstname_error = true;
			} else {
				$entry_firstname_error = false;
			}

			if (strlen($customers_lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
				$error = true;
				$entry_lastname_error = true;
			} else {
				$entry_lastname_error = false;
			}

			if (strlen($customers_email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
				$error = true;
				$entry_email_address_error = true;
			} else {
				$entry_email_address_error = false;
			}

			if (!tep_validate_email($customers_email_address)) {
				$error = true;
				$entry_email_address_check_error = true;
			} else {
				$entry_email_address_check_error = false;
			}

			if (strlen($customers_telephone) < 4) {
				$error = true;
				$entry_telephone_error = true;
			} else {
				$entry_telephone_error = false;
			}

			$check_email = tep_db_query("SELECT customers_email_address FROM " . TABLE_CUSTOMERS . " WHERE customers_email_address = '" . tep_db_input($customers_email_address) . "' AND customers_id != '" . (int)$customers_id . "'");
			if (tep_db_num_rows($check_email)) {
				$error = true;
				$entry_email_address_exists = true;
			} else {
				$entry_email_address_exists = false;
			}

			if ($error == false) {		
				$sql_data_array = array(	
											'customers_firstname' => $customers_firstname, 
											'customers_lastname' => $customers_lastname,
											'customers_email_address' => $customers_email_address, 
											'customers_telephone' => $customers_telephone,
											'customers_fixe' => $customers_fixe, 
											'customers_newsletter' => $customers_newsletter, 
											'customers_company' => $entry_company , 
											//'customers_country' => $entry_country_id, 
											//'customers_country' => $customers_country, 
											'customers_fax' => $customers_fax , 
											'customers_tva' => $customers_tva, 
											'customers_siret' => $customers_siret , 
											'customers_important' => $customers_important ,	
											'customers_news_sms' => $customers_news_sms, 
											'customers_admin' => $customers_admin, 
											'customers_type' => $customers_type, 
											'customers_comment' => $customers_comment, 
											'customers_company' => $customers_company, 
											'customers_company_type' => $customers_company_type, 
											'customers_company_type_libre' => $customers_company_type_libre, 
											'customers_default_address_id' => $default_address
										);
				
				if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $customers_gender;
				if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($customers_dob);

				tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "'");
				tep_db_query("UPDATE " . TABLE_CUSTOMERS_INFO . " SET customers_info_date_account_last_modified = now() WHERE customers_info_id = '" . (int)$customers_id . "'");

				if ($entry_zone_id > 0) $entry_state = '';
				tep_redirect(tep_href_link(FILENAME_CLIENT_EDIT, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers_id));

			} else if ($error == true) {
				if ($action != 'new' ) {
					$cInfo = new objectInfo($HTTP_POST_VARS);
					$processed = true;
				}
			}
		break;
		
		case 'suppression_adresse':
			$req=mysql_query("delete from address_book where address_book_id=".$_GET['add_suppr']."");
			//tep_redirect(tep_href_link(FILENAME_CLIENT_EDIT, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $_GET['cID']));
			tep_redirect(tep_href_link(FILENAME_CLIENT_EDIT, 'cID=' . $_GET['cID']));
			// FILENAME_CLIENT_LIST
		break;
		
		default:

			$customers_query = tep_db_query("SELECT c.customers_id, c.customers_country, c.customers_company, c.customers_company_type, c.customers_company_type_libre, c.customers_gender, c.customers_firstname, c.customers_lastname, c.customers_dob, c.customers_fax, c.customers_tva, c.customers_siret, c.customers_important, c.customers_admin, c.customers_email_address, a.entry_company, a.entry_street_address, a.entry_suburb, a.entry_street_address_3, a.entry_street_address_4, a.entry_postcode, a.entry_city, a.entry_state, a.entry_zone_id, a.entry_country_id, c.customers_telephone, c.customers_fixe, c.customers_newsletter, c.customers_news_sms, c.customers_type, c.customers_comment, c.customers_default_address_id 
			FROM " . TABLE_CUSTOMERS . " c 
			LEFT JOIN " . TABLE_ADDRESS_BOOK . " a 
			ON c.customers_default_address_id = a.address_book_id 
			WHERE a.customers_id = c.customers_id and c.customers_id = '" . (int)$HTTP_GET_VARS['cID'] . "'");
			$customers = tep_db_fetch_array($customers_query);
			if ($action == 'edit' && !empty($HTTP_GET_VARS['cID'])) {
				$cInfo = new objectInfo($customers);
			}
    }
}?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript">
	function verif_mdp() {
		var mdp = document.getElementById('ipt_mdp').value;
		var mdp_confirm = document.getElementById('ipt_confirm_mdp').value;
		
		if (mdp != mdp_confirm) {
			document.getElementById('ipt_mdp').style.backgroundColor = 'red';
			document.getElementById('ipt_confirm_mdp').style.backgroundColor = 'red';
		} else {
			document.getElementById('ipt_mdp').style.backgroundColor = 'white';
			document.getElementById('ipt_confirm_mdp').style.backgroundColor = 'white';
		}
	}
</script>
<?php if ($action == 'edit' || $action == 'update') {?>
<script language="javascript"><!--
function check_form() {
	var error = 0;
	var error_message = "<?php echo JS_ERROR; ?>";

	var customers_firstname = document.customers.customers_firstname.value;
	var customers_lastname = document.customers.customers_lastname.value;
	<?php if (ACCOUNT_COMPANY == 'true') echo 'var entry_company = document.customers.entry_company.value;' . "\n"; ?>
	<?php if (ACCOUNT_DOB == 'true') echo 'var customers_dob = document.customers.customers_dob.value;' . "\n"; ?>
	var customers_email_address = document.customers.customers_email_address.value;
	var entry_street_address = document.customers.entry_street_address.value;
	var entry_postcode = document.customers.entry_postcode.value;
	var entry_city = document.customers.entry_city.value;
	var customers_telephone = document.customers.customers_telephone.value;
	var customers_fax = document.customers.customers_fax.value;
	var customers_tva = document.customers.customers_tva.value;
	var customers_siret = document.customers.customers_siret.value;

	<?php 
	if (ACCOUNT_GENDER == 'true') { 
	?>
		if (document.customers.customers_gender[0].checked || document.customers.customers_gender[1].checked) {
			
		} else {
			error_message = error_message + "<?php echo JS_GENDER; ?>";
			error = 1;
		}
	<?php 
	} 
	?>

	if (customers_firstname == "" || customers_firstname.length < <?php echo ENTRY_FIRST_NAME_MIN_LENGTH; ?>) {
		error_message = error_message + "<?php echo JS_FIRST_NAME; ?>";
		error = 1;
	}

	if (customers_lastname == "" || customers_lastname.length < <?php echo ENTRY_LAST_NAME_MIN_LENGTH; ?>) {
		error_message = error_message + "<?php echo JS_LAST_NAME; ?>";
		error = 1;
	}

	if (customers_email_address == "" || customers_email_address.length < <?php echo ENTRY_EMAIL_ADDRESS_MIN_LENGTH; ?>) {
		error_message = error_message + "<?php echo JS_EMAIL_ADDRESS; ?>";
		error = 1;
	}

	if (entry_street_address == "" || entry_street_address.length < <?php echo ENTRY_STREET_ADDRESS_MIN_LENGTH; ?>) {
		error_message = error_message + "<?php echo JS_ADDRESS; ?>";
		error = 1;
	}

	if (entry_postcode == "" || entry_postcode.length < <?php echo ENTRY_POSTCODE_MIN_LENGTH; ?>) {
		error_message = error_message + "<?php echo JS_POST_CODE; ?>";
		error = 1;
	}

	if (entry_city == "" || entry_city.length < <?php echo ENTRY_CITY_MIN_LENGTH; ?>) {
		error_message = error_message + "<?php echo JS_CITY; ?>";
		error = 1;
	}

	<?php 
	if (ACCOUNT_STATE == 'true') {
	?>
		if (document.customers.elements['entry_state'].type != "hidden") {
		if (document.customers.entry_state.value == '' || document.customers.entry_state.value.length < <?php echo ENTRY_STATE_MIN_LENGTH; ?> ) {
			error_message = error_message + "<?php echo JS_STATE; ?>";
			error = 1;
		}
	}
	<?php 
	}
	?>

	if (document.customers.elements['entry_country_id'].type != "hidden") {
		if (document.customers.entry_country_id.value == 0) {
			error_message = error_message + "<?php echo JS_COUNTRY; ?>";
			error = 1;
		}
	}

	if (customers_telephone == "" || customers_telephone.length < <?php echo ENTRY_TELEPHONE_MIN_LENGTH; ?>) {
		error_message = error_message + "<?php echo JS_TELEPHONE; ?>"; error = 1;
	}

	if (error == 1) { 
		alert(error_message); 
		return false; 
	} else { 
		return true; 
	}
}
//--></script>
<?php }?>

</head>
<script language="javascript" src="includes/javascript/ajax/fiche_client.js"></script>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

	<?php
	if ($action == 'edit' || $action == 'update') {
    	$newsletter_array = array(array('id' => '1', 'text' => ENTRY_NEWSLETTER_YES),array('id' => '0', 'text' => ENTRY_NEWSLETTER_NO));
		
		$type_array = array();
		$type_query = tep_db_query("select type_id, type_name from " . TABLE_CUSTOMERS_TYPE . " order by type_id  ");
		while ($type_values = tep_db_fetch_array($type_query)) {
		  $type_array[] = array('id' => $type_values['type_id'], 'text' => $type_values['type_name']);
		}
		$admin_array=array();
		$admin_array[] = array('id' => "", 'text' => "Tous");
		$res_admin=tep_db_query("select admin_lastname, admin_firstname, admin_id from admin  where admin_groups_id<>4 order by admin_lastname, admin_firstname");
		while($row_admin=tep_db_fetch_array($res_admin)){
			$admin_array[]=array('id' => $row_admin["admin_id"], 'text' => $row_admin["admin_lastname"]." ".$row_admin["admin_firstname"]);
		}
		
		$cust_type=$cInfo->customers_type-1;
  //}
  	
	$page="infos";
	include('client_header.php');
?>
	<table border="0" width="100%" cellspacing="2" cellpadding="2">
		<tr>
			<?php
			if ($action == 'edit' && isset($_REQUEST["cID"])) {
				echo tep_draw_form('customers', FILENAME_CLIENT_EDIT, tep_get_all_get_params(array('action')) . 'action=update', 'post', 'onSubmit="return check_form();"') . tep_draw_hidden_field('default_address_id', $cInfo->customers_default_address_id);
			}
			?>
	  
			<td>
				<div style="width:100%; height:200px;">
					<div style="float:left; height:200px; ">
						<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$cInfo->customers_type; ?>" style="width:400px;" >
						<?php if (ACCOUNT_GENDER == 'true') { ?>
							<tr><strong>Donn�es personelles</strong></tr>
							<tr>
								<td class="main">Civilit� :</td>
								<td class="main">
									<?php
									if ($error == true) {
										if ($entry_gender_error == true) {
											echo tep_draw_radio_field('customers_gender', 'm', false, $cInfo->customers_gender) . '&nbsp;&nbsp;Mr&nbsp;&nbsp;' . tep_draw_radio_field('customers_gender', 'f', false, $cInfo->customers_gender) . '&nbsp;&nbsp;Mme&nbsp;&nbsp;' . tep_draw_radio_field('customers_gender', 'd', false, $cInfo->customers_gender) . '&nbsp;&nbsp;Mlle&nbsp;' . ENTRY_GENDER_ERROR;
										} else {
											if ($cInfo->customers_gender == 'm') {
												echo MALE;
											} else if ($cInfo->customers_gender == 'f') {
												echo FEMALE;
											} else {
												echo MLLE;
											}
											echo tep_draw_hidden_field('customers_gender');
										}
									} else {
										echo tep_draw_radio_field('customers_gender', 'm', false, $cInfo->customers_gender) . '&nbsp;&nbsp;Mr&nbsp;&nbsp;' . tep_draw_radio_field('customers_gender', 'f', false, $cInfo->customers_gender) . '&nbsp;&nbsp;Mme&nbsp;&nbsp;' . tep_draw_radio_field('customers_gender', 'd', false, $cInfo->customers_gender) . '&nbsp;&nbsp;Mlle';
									}
									?>
								</td>
							</tr>
						<?php }?>
							<tr>
								<td class="main">Pr�nom :</td>
								<td class="main">
									<?php
									if ($error == true && $entry_firstname_error == false) {
										if ($entry_firstname_error == true) {
											echo tep_draw_input_field('customers_firstname', $cInfo->customers_firstname, 'maxlength="32" size="30"') . '&nbsp;' . ENTRY_FIRST_NAME_ERROR;
										} else {echo $cInfo->customers_firstname . tep_draw_hidden_field('customers_firstname');}
									} else {echo tep_draw_input_field('customers_firstname', $cInfo->customers_firstname, 'maxlength="32" size="30"', true);}?>
								</td>
							</tr>
							<tr>
								<td class="main">Nom :</td>
								<td class="main">
								<?php
								if ($error == true) {
									if ($entry_lastname_error == true) {
										echo tep_draw_input_field('customers_lastname', $cInfo->customers_lastname, 'maxlength="32" size="30"') . '&nbsp;' . ENTRY_LAST_NAME_ERROR;
									} else {
										echo $cInfo->customers_lastname . tep_draw_hidden_field('customers_lastname');
									}
								} else {
									echo tep_draw_input_field('customers_lastname', $cInfo->customers_lastname, 'maxlength="32" size="30"', true);
								}?>
								</td>
							</tr>
							<?php if (ACCOUNT_DOB == 'true') { ?>
							<tr>
								<td class="main">Date de naissance :</td>
								<td class="main">
								<?php
								if ($error == true) {
									if ($entry_date_of_birth_error == true) {
										echo tep_draw_input_field('customers_dob', tep_date_short($cInfo->customers_dob), 'maxlength="10" size="30"') . '&nbsp;' . ENTRY_DATE_OF_BIRTH_ERROR;
									} else { 
										echo $cInfo->customers_dob . tep_draw_hidden_field('customers_dob'); 
									}
								} else {
									echo tep_draw_input_field('customers_dob', tep_date_short($cInfo->customers_dob), 'maxlength="10" size="30"', true);
								} ?>
								</td>
							</tr>
							<?php } ?>
					
							<tr>
								<td class="main">Adresse email :</td>
								<td class="main">
									<?php
									if ($error == true) {
										if ($entry_email_address_error == true) {
											echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96" size="30"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR;
										} elseif ($entry_email_address_check_error == true) {
											echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96" size="30"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_CHECK_ERROR;
										} elseif ($entry_email_address_exists == true) {
											echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96" size="30"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR_EXISTS;
										} else {
											echo $customers_email_address . tep_draw_hidden_field('customers_email_address');
										}
									} else { 
										echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96" size="30"', true);
									} 
									?>
								</td>
							</tr>
							<tr>
								<td class="main">T�l�phone portable :</td>
								<td class="main">
									<?php
									if ($error == true) {
										if ($entry_telephone_error == true) {
											echo tep_draw_input_field('customers_telephone', $cInfo->customers_telephone, 'maxlength="32" size="30"') . '&nbsp;' . ENTRY_TELEPHONE_NUMBER_ERROR;
										} else { 
											echo $cInfo->customers_telephone . tep_draw_hidden_field('customers_telephone');
										}
									} else { 
										echo tep_draw_input_field('customers_telephone', $cInfo->customers_telephone, 'maxlength="32" size="30"', true);
									} ?>
								</td>
							</tr>
							<tr>
								<td class="main">T�l�phone fixe : </td>
								<td class="main">
								<?php
								if ($processed == true) {
									echo $cInfo->customers_fixe . tep_draw_hidden_field('customers_fixe');
								} else { 
									echo tep_draw_input_field('customers_fixe', $cInfo->customers_fixe, 'maxlength="32" size="30"');
								} 
								?>
								</td>
							</tr>
							<tr>
								<td class="main">Num�ro fax :</td>
								<td class="main">
									<?php
									if ($error == true) {
										if ($customers_fax_error == true) {
											echo tep_draw_input_field('customers_fax', $cInfo->customers_fax, 'maxlength="32" size="32"') . '&nbsp;' . CUSTOMERS_FAX_ERROR;
										} else {
											echo $cInfo->customers_fax . tep_draw_hidden_field('customers_fax');
										}
									} else { 
										echo tep_draw_input_field('customers_fax', $cInfo->customers_fax, 'maxlength="32" size="30"');
									}
									?>			
								</td>
							</tr>
						</table>
					</div>
					
					<div style="float:left; width:400px; height:200px; margin-left:10px; " >
						<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$cInfo->customers_type; ?>" style="width:400px;" >
							<tr><strong>Donn�es professionnelles</strong></tr>
							<tr>
								<td class="main">Type de soci�t� :</td>
								<td class="main">
									<?php
									$customers_company_type_libre=$cInfo->customers_company_type_libre;
									echo tep_draw_input_field('customers_company_type_libre', $customers_company_type_libre, 'maxlength="50" size="32"');
									?>			
								</td>
							</tr>
							<tr>
								<td class="main">Nom :</td>
								<td class="main">
									<?php
									$societe_c=$cInfo->customers_company;
									if ($error == true) {
										if ($entry_company_error == true) {
											echo tep_draw_input_field('entry_company', $societe_c, 'maxlength="32" size="32"') . '&nbsp;' . ENTRY_COMPANY_ERROR;
										} else {
											echo $societe_c . tep_draw_hidden_field('entry_company');
										}
									} else {
										echo tep_draw_input_field('entry_company', $societe_c, 'maxlength="32" size="32"');
									}
									?>			
								</td>
							</tr>
							<tr>
								<td class="main">SIRET / RNA:</td>
								<td class="main">
									<?php
									if ($error == true) {
										if ($customers_siret_error == true) {
											echo tep_draw_input_field('customers_siret', $cInfo->customers_siret, 'maxlength="32" size="32"') . '&nbsp;' . CUSTOMERS_SIRET_ERROR;
										} else {
											echo $cInfo->customers_siret . tep_draw_hidden_field('customers_siret'); 
										}
									} else { 
									echo tep_draw_input_field('customers_siret', $cInfo->customers_siret, 'maxlength="32" size="32"');
									} 
									?>			
								</td>
							</tr>							
							<tr>
								<td class="main">TVA intracom :</td>
								<td class="main">
									<?php
									if ($error == true) {
										if ($customers_tva_error == true) {
											echo tep_draw_input_field('customers_tva', $cInfo->customers_tva, 'maxlength="32" size="32"') . '&nbsp;' . CUSTOMERS_TVA_ERROR;
										} else {
											echo $cInfo->customers_tva . tep_draw_hidden_field('customers_tva');
										}
									} else {
										echo tep_draw_input_field('customers_tva', $cInfo->customers_tva, 'maxlength="32" size="32"');
									}
									?>			
								</td>
							</tr>
						</table>
						<br>
						<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$cInfo->customers_type; ?>" style="width:400px;">
							<tr><strong>Newsletter et SMS</strong></tr>
							<tr>
								<td class="main">Bulletin d'informations :</td>
								<td class="main">
									<?php
									if ($processed == true) {
										if ($cInfo->customers_newsletter == '1') {
											echo ENTRY_NEWSLETTER_YES;
										} else {
											echo ENTRY_NEWSLETTER_NO;
										} 
									echo tep_draw_hidden_field('customers_newsletter');
									} else {
										echo tep_draw_pull_down_menu('customers_newsletter', $newsletter_array, (($cInfo->customers_newsletter == '1') ? '1' : '0'));
									}
									?>						
								</td>
							</tr>
							<tr>
								<td class="main">Reception SMS :</td>
								<td class="main">
									<?php
									if ($processed == true) {
										if ($cInfo->customers_news_sms == '1') {
											echo ENTRY_NEWS_SMS_YES;
										} else {
											echo ENTRY_NEWS_SMS_NO;
										}	
										echo tep_draw_hidden_field('customers_news_sms');
									} else {
										echo tep_draw_pull_down_menu('customers_news_sms', $newsletter_array, (($cInfo->customers_news_sms == '1') ? '1' : '0'));
									}
									?>		
								</td>
							</tr>
						</table>
					</div>
					
					<div style="float:left; width:400px; height:200px; margin-left:10px;">
						<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$cInfo->customers_type; ?>" style="width:400px;">
							<tr><strong>Options Type</strong></tr>
							<tr>
								<td class="main">Client de :</td>
								<td class="main">
									<?php
										echo tep_draw_pull_down_menu('customers_admin', $admin_array, $cInfo->customers_admin);
									?>		
								</td>
							</tr>
							<tr>
								<td class="main">Type de client :</td>
								<td class="main">
									<?php
									if ($processed == true) {
										switch($cInfo->customers_type) {
											case 1:echo 'Particulier';
											case 2:echo 'Professionnel';
											case 3:echo 'Revendeur';
											default:echo 'Particulier';
										}
										echo tep_draw_hidden_field('customers_type');
									} else {
										echo tep_draw_pull_down_menu('customers_type', $type_array, $cInfo->customers_type);
									}
									?>		
								</td>
							</tr>
							
							<tr>
								<td class="main">Sous groupe client :</td>
								<td class="main">									
									<select name="customers_company_type">
										<option value="">Pas de sous groupe</option>
										<option value="" style="background-color:#999999; font-weight: bold;">PARTICULIER / PRO</option>
										<? //if ($cInfo->customers_company_type=='Militaire') echo 'selected';?>
										<option value="Militaire" <? if ($cInfo->customers_company_type=='Militaire') echo 'selected';?>>
											&nbsp;&nbsp;&nbsp;Militaire
										</option>
										<option value="Gendarme" <? if ($cInfo->customers_company_type=='Gendarme') echo 'selected';?>>
											&nbsp;&nbsp;&nbsp;Gendarme
										</option>
										<option value="Policier (PN, CRS, Douanier)" <? if ($cInfo->customers_company_type=='Policier (PN, CRS, Douanier)') echo 'selected';?>>
											&nbsp;&nbsp;&nbsp;Policier (PN, CRS, Douanier)
										</option>
										<option value="Policier (PM-ASVP)" <? if ($cInfo->customers_company_type=='Policier (PM-ASVP)') echo 'selected';?>>
											&nbsp;&nbsp;&nbsp;Policier (PM-ASVP)
										</option>
										<option value="Agent P�nitentiaire" <? if ($cInfo->customers_company_type=='Agent P�nitentiaire') echo 'selected';?>>
											&nbsp;&nbsp;&nbsp;Agent P�nitentiaire
										</option>
										<option value="Agent de S�curit�" <? if ($cInfo->customers_company_type=='Agent de S�curit�') echo 'selected';?>>
											&nbsp;&nbsp;&nbsp;Agent de S�curit�
										</option>
										<option value="Sapeur Pompier" <? if ($cInfo->customers_company_type=='Sapeur Pompier') echo 'selected';?>>
											&nbsp;&nbsp;&nbsp;Sapeur Pompier
										</option>
										<option value="Autre profession" <? if ($cInfo->customers_company_type=='Autre profession') echo 'selected';?>>
											&nbsp;&nbsp;&nbsp;Autre profession
										</option>
										
										<option value="" style="background-color:#999999; font-weight: bold;">ADMINISTRATION</option>
										
										<option value="Mairie" <? if ($cInfo->customers_company_type=='Mairie') echo 'selected';?>>
											&nbsp;&nbsp;Mairie
										</option>
										<option value="Communaut� de Communes" <? if ($cInfo->customers_company_type=='Communaut� de Communes') echo 'selected';?>>
											&nbsp;&nbsp;Communaut� de Communes
										</option>
										<option value="Communaut� d�Agglom�ration" <? if ($cInfo->customers_company_type=='Communaut� d�Agglom�ration') echo 'selected';?>>
											&nbsp;&nbsp;Communaut� d�Agglom�ration
										</option>
										<option value="Conseil G�n�ral" <? if ($cInfo->customers_company_type=='Conseil G�n�ral') echo 'selected';?>>
											&nbsp;&nbsp;Conseil G�n�ral
										</option>
										<option value="Conseil R�gional" <? if ($cInfo->customers_company_type=='Conseil R�gional') echo 'selected';?>>
											&nbsp;&nbsp;Conseil R�gional
										</option>
										<option value="Minist�re" <? if ($cInfo->customers_company_type=='Minist�re') echo 'selected';?>>
											&nbsp;&nbsp;Minist�re
										</option>
										<option value="Pr�fecture / Sous-Pr�fecture" <? if ($cInfo->customers_company_type=='Pr�fecture / Sous-Pr�fecture') echo 'selected';?>>
											&nbsp;&nbsp;Pr�fecture / Sous-Pr�fecture
										</option>
										<option value="Association" <? if ($cInfo->customers_company_type=='Association') echo 'selected';?>>
											&nbsp;&nbsp;Association
										</option>
										<option value="Autre administration" <? if ($cInfo->customers_company_type=='Autre administration') echo 'selected';?>>
											&nbsp;&nbsp;Autre administration
										</option>
									</select>	
								</td>
							</tr>
						</table>
						<br>
						<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$cInfo->customers_type; ?>" style="width:400px;">
							<tr><strong>Changement mot de passe</strong></tr>
							<tr>
								<td class="main">Nouveau mot de passe :</td>
								<td class="main">
									<input type="text" value="" name="new_mot_de_passe" id="ipt_mdp" onkeyup='verif_mdp();' />
								</td>
							</tr>
							<tr>
								<td class="main">Confirmation :</td>
								<td class="main">
									<input type="text" value="" name="new_mot_de_passe_confirm" id="ipt_confirm_mdp" onkeyup='verif_mdp();' />
								</td>
							</tr>
							<tr>
								<td colspan='2' style="font-size: 0.8em; font-weight: bold; color: red; text-align: center;">
									Si les champs sont rouge aucune modification ne sera apport�e !
								</td>
							</tr>
						</table>
					</div>
				</div>
              
			  
			  
				<div style="width:100%; margin-top:40px;">
					<div style="float:left; width:1680px; ">
						<strong>Les adresses du client </strong> (id pays : <? echo $customers['customers_country']; ?>)
						
							<?php 
							function nom_pays($id,$i){
								$req=tep_db_query("select countries_id, countries_name from ".TABLE_COUNTRIES."");

								$ret='<select id="country'.$i.'" name="pays'.$i.'" style="width:150px;">';
								while($res=tep_db_fetch_array($req)){
									$ret.= '<option value="'.$res['countries_id'].'" ';
									if($id==$res['countries_id']){
										$ret.= 'selected="selected" ';
									}
									$ret.= ' >'.$res['countries_name'].'</option>';
								}
								$ret.='</select>';
								
								return $ret; 
							}
												
							if(isset($_GET['id_client'])){ $id_client=$_GET['id_client']; }
							if(isset($_GET['cID'])){ $id_client=$_GET['cID']; }

							if ($action == 'edit' && isset($id_client)) {
								$req=tep_db_query("select * from ".TABLE_ADDRESS_BOOK." where customers_id=".$id_client." order by address_book_id asc");
	
					
								$i=0;
							
							while($res=tep_db_fetch_array($req)) { ?>
								
									<input type="hidden" id="id_address<?php echo $i; ?>" name="id_adresse<?php echo $i; ?>" value="<?php echo stripslashes($res["address_book_id"]); ?>" />
							
							
<table style="margin-top:10px;" class="<? echo 'formArea'.$cInfo->customers_type; ?>">
	<tr>
		<td rowspan="2" style="padding-right:20px; padding-left:20px;"><strong><?php echo $i+1;?></strong></td>
		<td>
			<input type="text" size="20" maxlength="32" id="title<?php echo $i; ?>" name="titre<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_address_title"]); ?>" placeholder="Titre adresse"/>
		</td>
		<td>
			<input type="text" size="20" maxlength="32" id="lastname<?php echo $i; ?>" name="nom<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_lastname"]); ?>" placeholder="Nom" />
		</td>
		<td>
			<input type="text" size="35" maxlength="35" id="adresse_3<?php echo $i; ?>" name="adresse_3<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_street_address_3"]); ?>" maxlength="35" placeholder="Appt - Etage - Couloir - Esc.">
		</td>
		<td>
			<input type="text" size="35" maxlength="35" id="adresse<?php echo $i; ?>" name="adresse<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_street_address"]); ?>" maxlength="35" placeholder="N� + Voie (rue, avenue, bvd, etc�)" />
		</td>
		<td>
			<input type="text" size="20" id="postcode<?php echo $i; ?>" name="codepostal<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_postcode"]); ?>" placeholder="Code postal" />
		</td>
		<td>
			<?php echo nom_pays($res["entry_country_id"],$i); ?>
		</td>
		
	</tr>	
	<tr>
		<td>
			<input type="text" size="20"  maxlength="32" id="company<?php echo $i; ?>" name="societe<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_company"]); ?>"  placeholder="Soci�t�" />
		</td>
		<td>
			<input type="text" size="20" maxlength="32" id="firstname<?php echo $i; ?>" name="prenom<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_firstname"]); ?>" placeholder="Pr�nom" />
		</td>									
		<td>
			<input type="text" size="35" maxlength="35" id="suburb<?php echo $i; ?>" name="complement<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_suburb"]); ?>" maxlength="35" placeholder="Entr�e - B�t. - Imm. - R�s." />
		</td>
		<td>
			<input type="text" size="35" maxlength="35" id="adresse_4<?php echo $i; ?>" name="adresse_4<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_street_address_4"]); ?>" maxlength="35" placeholder="Boite Postale - Lieu Dit">
		</td>									
		<td>
			<input type="text" size="20" maxlength="35" id="city<?php echo $i; ?>" name="ville<?php echo $i; ?>" value="<?php echo stripslashes($res["entry_city"]); ?>"  placeholder="Ville" />
		</td>
		
		<td style=" margin-bottom:15px;" class="main">
			<?php
			echo '<input type="radio" name="default_address" '. ( ($res['address_book_id'] == $cInfo->customers_default_address_id) ? 'checked="checked"' : '') .' value="'. $res['address_book_id'] .'">';
						
			echo ($res['address_book_id'] == $cInfo->customers_default_address_id)
			? '<img border="0" title="Supprimer" alt="Supprimer" src="images/icons/supprimer_inactif.png" onclick="alert(\'Vous ne pouvez pas supprimer une adresse qui est par d�faut\')"/>'
			: '<img class="pointer" border="0" title="Supprimer" alt="Supprimer" src="images/icons/supprimer.png" onclick=\'location.href = "client_edit.php?cID='. $id_client .'&action=suppression_adresse&add_suppr='. $res['address_book_id'] .'"\' />';
			echo (($res['address_book_id'] == $cInfo->customers_default_address_id) ? '&nbsp;&nbsp;<span style="color:white; background-color:red;  padding:3px 5px 3px 5px; vertical-align:top;">Facturation</span>&nbsp;&nbsp;' : '');
			?>
			
		</td>
	</tr>
</table>							
									
								<?php
							$i++;
							}
						}
						
					echo "<input type='hidden' id='cpt' name='compteur' value='".$i."' />";
					?><table>
					<tr>		
						<table style="margin-top:10px;">
							<tr>
								<td rowspan="2" style="padding:5px"><strong>NEW</strong></td>
								<td>
									<input type="text" size="20" maxlength="32" id="title" name="titre" value="" placeholder="Titre adresse" />
								</td>
								<td>
									<input type="text" size="20" maxlength="32" id="lastname" name="nom" value="" placeholder="Nom" />
								</td>
								<td>
									<input type="text" size="40" maxlength="35" id="entry_street_address_3" name="adresse_3" value="" maxlength="35" placeholder="Appt - Etage - Couloir - Esc." style="background-color:#DDDDDD" />
								</td>
								<td>
									<input type="text" size="40" maxlength="35" id="adresse" name="adresse" value="" placeholder="N� + Voie (rue, avenue, bvd, etc�)" />
								</td>
								<td>
									<input type="text" size="20" id="postcode" name="codepostal" value="" placeholder="Code postal" />
								</td>
								<td>
									<?php echo nom_pays(0,$i); ?>
								</td>
								
							</tr>
							<tr>
								<td>
									<input type="text" size="20" maxlength="32" id="company" name="societe" value="" placeholder="Soci�t�" style="background-color:#DDDDDD" />
								</td>
								<td>
									<input type="text" size="20" maxlength="32" id="firstname" name="prenom" value="" placeholder="Pr�nom" />
								</td>
								<td>
									<input type="text" size="40" maxlength="35" id="suburb" name="complement" value="" placeholder="Entr�e - B�t. - Imm. - R�s." style="background-color:#DDDDDD"/>
								</td>		
								<td>
									<input type="text" size="40" maxlength="35" id="entry_street_address_4" name="adresse_4" value="" maxlength="35" placeholder="Boite Postale - Lieu Dit" style="background-color:#DDDDDD">
								</td>
								<td>
									<input type="text" size="20" maxlength="35" id="city" name="ville" value="" placeholder="Ville"/>
								</td>
								<td>	</td>
							</tr>
						</table>
					</tr>
				</table>
			</div>
		</div>
		<div style="clear:both;"></div>
		
		
		
						
		<div style="width:100%; margin-top:40px;">
			<div style="float:left; width:100%; margin-left:5px;">
				<strong>Les informations compl�mentaires</strong>
				<table class="<? echo 'formArea'.$cInfo->customers_type; ?>" style="margin-top:10px;">
					<tr>
						<td class="main">Information Importante :</td>
						<td class="main">
							<?php
							echo tep_draw_input_field('customers_important', $cInfo->customers_important, 'size="100" maxlength="73"');
							?>			
						</td>
					</tr>
					<tr>
						<td class="main">Commentaires :</td>
						<td class="main"><?php echo tep_draw_textarea_field('customers_comment', 'soft', '100', '10', $cInfo->customers_comment);?></td>
					</tr>
				</table>                
			</div>
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>
			<div style="text-align:center; width:1300px; margin-top:30px;">
				<input type="submit" value="Mise a jour" />
					<a href="nouvelle_commandeP1.php?cID=<?php echo $id_client; ?>" ><input type="button" value="Passer une commande" /></a>
					<a href="client_list.php" ><input type="button" value="Retour � la liste des clients" /></a>
					<input type="button" value="Se connecter sur le compte du client" onClick="window.open('/<?php echo DIR_WS_SITE;?>/compte/connexion.php?mail=<?php echo $cInfo->customers_email_address; ?>&mdp=azertyuiopqsdfghjklm&connexion=ok');" />
					</form>
				</form>
				<?php }?>
				</form>
			</div>
		</td>
	</tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>