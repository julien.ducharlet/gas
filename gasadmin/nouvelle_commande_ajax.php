<?php 
/*Auteur : Sami 
   Page permettant de faire tous les retour ajax lors du passage d'une commande de l'admin ou de la creation d'un devis*/


require('includes/application_top.php');
session_start();

$$link = mysql_connect("localhost", DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
if ($$link) mysql_select_db(DB_DATABASE);


$adresse = $_GET['adresse'];
$adresse_livraison = $_GET['adresse_livraison'];
$keyword = $_GET['keyword'];
$option = $_GET['option'];
$ajout_produit = $_GET['ajout_produit'];
$mod_qte = $_GET['qte_mod'];
$suppr_prod = $_GET['id_ligne_suppr'];
$moyen_paiement = $_GET['moyen_paiement'];
$recharge_paiement = $_GET['recharge_paiement'];
$devis = $_GET['devis'];

$get_infos = $_GET['get_infos'];
$changement_fdp = $_GET['changement_fdp'];

//affecte une session si on selectionne un devis
if($devis!='') {
	
	$_SESSION['devis_commande_admin'] = ($devis=="oui") ? true : false;
}

//affecte une session lors de la selection d'un moyen de paiement + remise porte monnaie
if($recharge_paiement!='') {
	
	unset($_SESSION['moyen_paiement_panier_admin']);
	unset($_SESSION['remise_porte_monnaie_admin']);
}

//affecte une session lors de la selection d'un moyen de paiement
if($moyen_paiement!='') {
	
	$_SESSION['moyen_paiement_panier_admin']=$moyen_paiement;		
}

//affecte une session lors de la selection de l'adresse de livraison en calculant le poids et le prix
if($adresse_livraison != '') {
	
	$_SESSION['poids_commande_admin']=$poids=$_GET['poids'];
	$_SESSION['montant_ht_commande_admin']=$total=$_GET['montant'];
	
	$req = tep_db_query("select entry_country_id from address_book where address_book_id=".$adresse_livraison."");
	$res = tep_db_fetch_array($req);
	
	$req1 = tep_db_query("select countries_id_zone_livraison from countries where countries_id=".$res['entry_country_id']."");
	$res1 = tep_db_fetch_array($req1);	
	
	$req_a = tep_db_query("select prix_livraison
						 from module_livraison
						 where zone_livraison_id=".$res1['countries_id_zone_livraison']."
						 and poids_livraison >= ".$poids."");
	$res_a = tep_db_fetch_array($req_a);	
	
	$req_zone_tva = tep_db_query("select geo_zone_id from zones_to_geo_zones  where zone_country_id=".$res['entry_country_id']."");
	$tva = tep_db_fetch_array($req_zone_tva);
	
	$prix_livraison = (!$_SESSION['fdp_changes']) ? $res_a['prix_livraison'] : $_SESSION['livraison_commande_admin_exact'];
	
	$M = $prix_livraison+$total;
	
	if($tva['geo_zone_id']==GEO_ZONE_AVEC_TVA) {
		
		$tva = 1;
		$_SESSION['montant_panier_admin'] = $prix_livraison+$total;
	}
	else {
		
		$tva = 1.2;
		$_SESSION['montant_panier_admin'] = ($prix_livraison+$total)*1.2;
	}
	
	$remise = 0;
		
	if($_SESSION['moyen_paiement_panier_admin']==4 && $_SESSION['remise_porte_monnaie_admin']==0) {
		
		$req=tep_db_query("select customers_argent_virtuel from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['id_client_panier']."");
		$res=tep_db_fetch_array($req);
		
		 if($M>$res['customers_argent_virtuel']) {
			 
			 $_SESSION['remise_porte_monnaie_admin'] = $remise=round($res['customers_argent_virtuel'],2);
			 
			 if($tva['geo_zone_id']==GEO_ZONE_AVEC_TVA) {
				 
				$tva = 1;
				$_SESSION['montant_panier_admin'] = round($prix_livraison+$total-$remise,2);
			}
			else {
				$tva=1.2;
				$_SESSION['montant_panier_admin']=round(($prix_livraison+$total-$remise)*1.2,2);
			}
		 }
		 else {
			 $_SESSION['remise_porte_monnaie_admin'] = $remise=round($M,2);
			 
			 if($tva['geo_zone_id']==GEO_ZONE_AVEC_TVA) {
				 
				$tva=1;
				$_SESSION['montant_panier_admin']=round($prix_livraison+$total-$remise,2);
			}
			else {
				$tva=1.2;
				$_SESSION['montant_panier_admin']=round(($prix_livraison+$total-$remise)*1.2,2);
			}
		 }
	}
	else {
		
		if(isset($_SESSION['remise_porte_monnaie_admin']) && !empty($_SESSION['remise_porte_monnaie_admin'])) {
			
			$req = tep_db_query("select customers_argent_virtuel from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['id_client_panier']."");
			$res = tep_db_fetch_array($req);				
			if($tva['geo_zone_id']==GEO_ZONE_AVEC_TVA) {
				
				$tva = 1;
				$_SESSION['montant_panier_admin'] = round($prix_livraison+$total-$remise,2);
			}
			else {
				$tva=1.2;
				$_SESSION['montant_panier_admin'] = round(($prix_livraison+$total-$remise)*1.2,2);
			}
			
			if($M > $res['customers_argent_virtuel']) {
				
				$_SESSION['remise_porte_monnaie_admin'] = $remise = round($res['customers_argent_virtuel'],2);
			}
			else{
				
				$_SESSION['remise_porte_monnaie_admin'] = $remise = round($M,2);
			}
		 }	
		 else $_SESSION['remise_porte_monnaie_admin'] = $remise = 0;
	}
	
	$_SESSION['taux_tva_commande_admin'] = $tva;
	
	if (!$_SESSION['fdp_changes']) {
		
		$_SESSION['livraison_commande_admin'] = round($res_a['prix_livraison'],2);
		$_SESSION['livraison_commande_admin_exact'] = $res_a['prix_livraison'];
	}
		
	echo $tva."!".$_SESSION['livraison_commande_admin_exact']."!".$remise;		
}

//affecte une session lors de la selection d'une adresse de livraison
if($adresse!=''){
	$_SESSION['adresse_commande_admin']=$adresse;
	$_SESSION['fdp_changes'] = false;
}

//permet la recherche de produit
if($keyword!='') {
	
	$req_produit=mysql_query("select p.products_id, pd.products_name, p.products_model 
							 from products p, products_description pd 
							 where p.products_id=pd.products_id and (pd.products_name like '%".$keyword."%' or p.products_model like '%".$keyword."%')");
	$nb_resultat=mysql_num_rows($req_produit);
	if($nb_resultat==0) {
		
		echo "Il n'y a pas de resultat &agrave; votre recherche";
	}
	else {
		
		echo '<select id="liste_produit" size=15 onclick="options_search();">';
		
		while($resultat=mysql_fetch_array($req_produit)) {
			
			echo "<option value=".$resultat['products_id'].">".$resultat['products_id']." *--* ".$resultat['products_name']." *--* ".$resultat['products_model']."</option>";
		}
		echo"</select>";
	}
}

//permet la recherche d'option si le produit en dispose
if($option!='') {
	
	$req_option=mysql_query("select po.products_options_name, po.products_options_id, pov.products_options_values_id, pov.products_options_values_name 
							from products_attributes pa, products_options po, products_options_values pov
							where pov.products_options_values_id=pa.options_values_id
							and pa.options_id=po.products_options_id
							and pa.products_id=". $option ." order by products_options_name");
	$nb_resultat=mysql_num_rows($req_option);
	
	if($nb_resultat==0) {
		
		echo $nb_resultat."$|$".$option;
	}
	else {
		
		echo $nb_resultat."$|$".'<select id="liste_option" size=8 onclick="verif_qte_2()">';
		
		while($res=mysql_fetch_array($req_option)) {
			
			echo "<option value=".$option."*-*".$res['products_options_values_id']."*-*".$res['products_options_id'].">".$res['products_options_values_name']."</option>";	
		}
		echo '</select>';
	}
}

//permet d'ajouter un produit au panier
if($ajout_produit!='') {
	
	$produit_id=$_GET['ajout_produit'];
	$option_id=$_GET['option_id'];
	$option_value_id=$_GET['option_value_id'];
	$qte=$_GET['qte'];
	$cPath = $_GET['cPath'];
	
	if($option_id=='') {
		
		$option_id=0;
		$option_value_id=0;
	}
	
	echo count($_SESSION['panier_admin']['products_id']);
	
	if(count($_SESSION['panier_admin']['products_id'])!=0) {
		
		if($option_id==0) {
			
			$present = false;
			
			for($i=0;$i<=count($_SESSION['panier_admin']['products_id']);$i++) {
				
				if($_SESSION['panier_admin']['products_id'][$i]==$produit_id && $_SESSION['panier_admin']['cPath'][$i]==$cPath) {
					
					echo $_SESSION['panier_admin']['qte'][$i]=$_SESSION['panier_admin']['qte'][$i]+$qte;
					$present=true;
				}
			}
			
			if(!$present) {
				
				echo $_SESSION['panier_admin']['products_id'][$i-1]=$produit_id;
				echo $_SESSION['panier_admin']['option_id'][$i-1]=0;
				echo $_SESSION['panier_admin']['option_values_id'][$i-1]=0;
				echo $_SESSION['panier_admin']['qte'][$i-1]=$qte;
				echo $_SESSION['panier_admin']['cPath'][$i-1]=$cPath;
			}
		}
		else {
			
			$present=false;
			
			for($i=0;$i<=count($_SESSION['panier_admin']['products_id']);$i++) {
				
				if($_SESSION['panier_admin']['products_id'][$i]==$produit_id && $_SESSION['panier_admin']['option_id'][$i]==$option_id && $_SESSION['panier_admin']['option_values_id'][$i]==$option_value_id && $_SESSION['panier_admin']['cPath'][$i]==$cPath) {
					
					echo $_SESSION['panier_admin']['qte'][$i]=$_SESSION['panier_admin']['qte'][$i]+$qte;
					$present=true;
				}					
			}
			if(!$present) {
				
				echo $_SESSION['panier_admin']['products_id'][$i-1]=$produit_id;
				echo $_SESSION['panier_admin']['option_id'][$i-1]=$option_id;
				echo $_SESSION['panier_admin']['option_values_id'][$i-1]=$option_value_id;
				echo $_SESSION['panier_admin']['qte'][$i-1]=$qte;
				echo $_SESSION['panier_admin']['cPath'][$i-1]=$cPath;
			}
		}		
	}
	else {
		
		if($option_id==0) {
			
			echo $_SESSION['panier_admin']['products_id'][0]=$produit_id;
			echo $_SESSION['panier_admin']['option_id'][0]=0;
			echo $_SESSION['panier_admin']['option_values_id'][0]=0;
			echo $_SESSION['panier_admin']['qte'][0]=$qte;
			echo $_SESSION['panier_admin']['cPath'][0]=$cPath;
		}
		else {
			
			echo $_SESSION['panier_admin']['products_id'][0]=$produit_id;
			echo $_SESSION['panier_admin']['option_id'][0]=$option_id;
			echo $_SESSION['panier_admin']['option_values_id'][0]=$option_value_id;
			echo $_SESSION['panier_admin']['qte'][0]=$qte;
			echo $_SESSION['panier_admin']['cPath'][0]=$cPath;
		}
	}
}

//permet de modifier une quantité
if($mod_qte!='') {
	
	$i = $_GET['id_ligne'];
	echo $_SESSION['panier_admin']['qte'][$i]=$mod_qte;
}

//permet de supprimer un produit du panier
if($suppr_prod!='') {

	$nb_occ=count($_SESSION['panier_admin']['products_id'])-1;
	
	if($nb_occ==$suppr_prod) {
		
			unset($_SESSION['panier_admin']['products_id'][$suppr_prod]);
			unset($_SESSION['panier_admin']['option_id'][$suppr_prod]);
			unset($_SESSION['panier_admin']['option_values_id'][$suppr_prod]);
			unset($_SESSION['panier_admin']['qte'][$suppr_prod]);
			unset($_SESSION['panier_admin']['cPath'][$suppr_prod]);
			unset($_SESSION['panier_admin']['products_price'][$suppr_prod]);
	}
	else{
		if($suppr_prod==0){
			$a=0;
		}
		else{ $a=$suppr_prod;}
		
		for($i=$a;$i<=$nb_occ-1;$i++){
			 $_SESSION['panier_admin']['products_id'][$i]=$_SESSION['panier_admin']['products_id'][$i+1];
			 $_SESSION['panier_admin']['option_id'][$i]=$_SESSION['panier_admin']['option_id'][$i+1];
			 $_SESSION['panier_admin']['option_values_id'][$i]=$_SESSION['panier_admin']['option_values_id'][$i+1];
			 $_SESSION['panier_admin']['qte'][$i]=$_SESSION['panier_admin']['qte'][$i+1];
			 $_SESSION['panier_admin']['products_price'][$i]=$_SESSION['panier_admin']['products_price'][$i+1];
			 $_SESSION['panier_admin']['cPath'][$i]=$_SESSION['panier_admin']['cPath'][$i+1];
		}
			unset($_SESSION['panier_admin']['products_id'][$i]);
			unset($_SESSION['panier_admin']['option_id'][$i]);
			unset($_SESSION['panier_admin']['option_values_id'][$i]);
			unset($_SESSION['panier_admin']['qte'][$i]);
			unset($_SESSION['panier_admin']['cPath'][$i]);
			unset($_SESSION['panier_admin']['products_price'][$i]);
	}
}

// AJAX pour les menus déroulants du choix de l'article
$rayon_select = $_GET['rayon_select'];
$marque_select = $_GET['marque_select'];
$modele_select = $_GET['modele_select'];
$article_select = $_GET['article_select'];


if ($rayon_select != '') {
	
	echo '<option value="">Choisissez une marque</option>';
	$reponse_marque = tep_db_query("SELECT cd.categories_name, cd.categories_id, cd.categories_url, cd.balise_title_lien_texte, categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, categories_status_adm, categories_status_vip
										 FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id inner join categories_rubrique as cr on c.categories_id=cr.categories_id
										 WHERE c.parent_id='0' AND cr.rubrique_id = " . $rayon_select . "
										 ORDER BY c.sort_order, cd.categories_name ASC");
	while($donnees_marque = tep_db_fetch_array($reponse_marque)){
		?>
		<option value="<?php echo $donnees_marque['categories_id']; ?>"><?php echo utf8_encode($donnees_marque['categories_name']); ?></option>
		<?php
	}
}

if ($marque_select != '') {
	
	echo '<option value="">Choisissez un mod&egrave;le</option>';
	$reponse_modele = tep_db_query("SELECT cd.categories_name, cd.categories_url, cd.categories_id, cd.balise_title_lien_image, cd.balise_title_lien_texte, 
											 c.categories_image2 ,categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, 
											 categories_status_adm, categories_status_vip
									FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id
									WHERE c.parent_id='" . $marque_select . "'
									ORDER BY cd.categories_name");
	while($donnees_modele = tep_db_fetch_array($reponse_modele)){
		?>
		<option value="<?php echo $donnees_modele['categories_id']; ?>"><?php echo utf8_encode($donnees_modele['categories_name']); ?></option>
		<?php
	}
}

if ($modele_select != '') {
	
	$reponse_nom_categorie = tep_db_query("SELECT categories_name, categories_id
										   FROM categories_description
										   WHERE categories_id = '" . $modele_select . "'");
	$categorie = tep_db_fetch_array($reponse_nom_categorie);
	
	$reponse_article = tep_db_query("SELECT pd.products_id, pd.products_name, pd.products_url, pd.balise_title_lien_image, pd.balise_title_lien_texte, p.products_price, p.products_bimage, p.products_sort_order, p.products_date_added
											FROM " . TABLE_PRODUCTS . " as p, " . TABLE_PRODUCTS_DESCRIPTION . " as pd inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " as ptc on pd.products_id=ptc.products_id
											WHERE ptc.categories_id='" . $modele_select . "' and p.products_id = pd.products_id 
											ORDER BY pd.products_name ASC, p.products_sort_order ASC");
	while($donnees_article = tep_db_fetch_array($reponse_article)){
		?>
		<option value="<?php echo $donnees_article['products_id']; ?>"><?php echo utf8_encode(bda_product_name_transform($donnees_article['products_name'], $categorie['categories_id'])); ?></option>
		<?php
	}
}

if ($article_select != '') {
	
	$req_option = tep_db_query("select po.products_options_name, po.products_options_id, pov.products_options_values_id, pov.products_options_values_name 
							from products_attributes pa, products_options po, products_options_values pov
							where pov.products_options_values_id=pa.options_values_id
							and pa.options_id=po.products_options_id
							and pa.products_id=" . $article_select . " order by products_options_values_name");
	$nb_resultat = mysql_num_rows($req_option);
	
	if ($nb_resultat == 0) {
		
		echo $nb_resultat.'$|$'.$article_select;
	}
	else {
		
		echo $nb_resultat."$|$";
		
		while($res=mysql_fetch_array($req_option)) {
			
			echo "<option value=".$article_select."*-*".$res['products_options_values_id']."*-*".$res['products_options_id'].">".mb_convert_encoding($res['products_options_values_name'],'UTF-8' )."</option>";	
		}
	}
}

if ($get_infos != '') {
	
	$req_article = tep_db_query("select p.products_model, p.products_quantity_reel, p.products_cost, p.products_bimage 
								 from products p 
								 where products_id = ". $get_infos);
	$rep_article = tep_db_fetch_array($req_article);
	
	echo "<img src='../" . BASE_DIR . "/images/products_pictures/normale/" . $rep_article['products_bimage'] . "' alt='Image Article' width='150' height='150' style='border: 1px solid black; margin-bottom: 10px;' /><br /><strong>Référence : </strong><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $rep_article['products_model'] . "<br /><strong>Prix d'achat : </strong><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . format_to_money($rep_article['products_cost']) . " €<br /><strong>Stock Réel : </strong><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $rep_article['products_quantity_reel'];
}

if ($changement_fdp != '') {
	
	$_SESSION['livraison_commande_admin_exact'] = $changement_fdp;
	$_SESSION['livraison_commande_admin'] = $changement_fdp;
	$_SESSION['fdp_changes'] = true;
}
?>
