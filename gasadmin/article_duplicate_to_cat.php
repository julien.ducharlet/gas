<?php
/* Page qui doit permettre de dupliquer des articles dans une categorie */

require('includes/application_top.php');

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

// Si 

//print_r($_POST);

if( isset($_POST) && !empty($_POST) && !empty($_POST['categories_id']) ) {
	
	$products_id = tep_db_prepare_input($_GET['pID']);
	$products_id = explode(" ", $products_id);
	$categories_id = tep_db_prepare_input($_POST['categories_id']);
	
	for($i=0; $i<=sizeof($products_id) ; $i ++) {
	
		for($j=0; $j<=sizeof($categories_id) ; $j ++) {
			
			$products_query = tep_db_query("select * from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$products_id[$i] . "' and categories_id = '" . (int)$categories_id[$j] . "' LIMIT 1");
			
			if(tep_db_num_rows($products_query) == 0 && (int)$categories_id[$j]!=0) {
			
				
				tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id[$i] . "', '" . (int)$categories_id[$j] . "')");
			}
		#############################################################################
		}
	}
	
	$messageStack->add_session('Success : Les produits ont bien �t� copi�s dans les cat�gories sp�cifi�es', 'success');
	
	tep_redirect(tep_href_link("article_duplicate_to_cat.php", 'pID='. $_GET['pID']));
}

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="JavaScript" src="ajax/duplicate.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<div id="popupcalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<?php
if( isset($_GET['pID']) && empty($_GET['pID']) || !isset($_GET['pID']) ) {
	echo '<h1>Aucun Produit � dupliquer</h1>';
} else { 
?>
    
    <!-- body //-->
    <table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
        <!-- body_text //-->
        <td width="100%" valign="top">
            
            <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr>
                <td width="100%">
                	
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="pageHeading">Dupliquer des articles</td>
                        <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
                    </tr>
                    </table>
				</td>
            </tr>
            
            <tr>
            	<td>
                <form action="" method="post" onSubmit="if(document.getElementById('products_id').value == 0) {
            alert('Il faut choisir un produit !');
            return false;
            }
            if(document.getElementById('categories_id').value == 0) {
            alert('Il faut choisir une ou plusieurs cat�gorie(s) !');
            return false;
            }
            ">
            
            
            <table border="0" cellspacing="0" cellpadding="5">
            <tr>
            	<td valign="top" class="main">Selectionnez les cat�gories dans lesquelles dupliquer les articles :&nbsp;</td>
            	<td class="main">
            
					<?php 
                    $category_tree_array = array();
                    $category_tree_array[] = array('id' => 0, 'text' => 'S�l�ctionnez les cat�gories dans la liste');
                    echo tep_draw_pull_down_menu('categories_id[]', tep_get_full_category_tree(0,'','',$category_tree_array ),0, ' size="40" multiple="multiple" id="categories_id"') 
                    ?>
            
            	</td>
            </tr>
            </table>
            
				</td>
            </tr>
            
            <tr>
            	<td>
                	<table border="0" width="100%" cellspacing="0" cellpadding="2">
            		<tr>
            			<td class="main" align="right" valign="top">
						<?php echo tep_image_submit('button_insert.gif', IMAGE_INSERT) . '&nbsp;&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_DUPLICATE) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) .'</a>'; ?></td>
                   	<tr>
                    </table>
                </td>
                </form>
            </tr>
            </table>
            
        </td>
    <!-- body_text_eof //-->
    </tr>
    </table>
    <!-- body_eof //-->
<?php } ?>
<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
