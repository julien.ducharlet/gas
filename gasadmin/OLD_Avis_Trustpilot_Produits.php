<?php
require('includes/application_top.php');
//require('../' . DIR_WS_SITE . '/includes/fonctions/mail.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title>Export TrustPilot : Produit</title>
<link href="includes/stylesheet.css" rel="stylesheet" type="text/css">
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">

	<?php	
	$last=0;
	if (isset($HTTP_GET_VARS['commandes']) && strlen($HTTP_GET_VARS['commandes'])>0){
		$id_commandes=substr(ltrim($HTTP_GET_VARS['commandes']),0,-1);
		$cmd = str_replace(' ', "','", ($id_commandes));
		$cmd= "'".$cmd."'";
		$query_orders = 'select o.orders_id, o.delivery_country, o.customers_id, p.products_id, p.products_bimage, orders_products_id, products_name, op.id_rayon, op.cpath
						from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS  .' op, '. TABLE_PRODUCTS .' p
						where o.orders_id=op.orders_id
						and p.products_id=op.products_id
						and o.orders_id in ('. $cmd .')
						order by orders_id';
		
		$orders_query = tep_db_query($query_orders);
		
		while($tuple = tep_db_fetch_array($orders_query)) {
				
			$nom_produit = $tuple['products_name'];
			
			// S�parateur de commande
			if($tuple['orders_id']!=$last && $_REQUEST["export"]!="fournisseur") { 
				echo '<tr><td colspan="10" height="20" ></td></tr>'; 
				$last=$tuple['orders_id']; 
			}
			
			// Requ�te pour r�cup�rer les information du client
			$customers_type_query = tep_db_query("select customers_id, customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_id = '" . $tuple['customers_id'] . "'");
			$row_client = mysql_fetch_array($customers_type_query);	
			
			//On trasforme le pr�nom et le nom sans accent
			$prenomnom = ucfirst(strtolower($row_client['customers_firstname'])) . ' '. strtoupper($row_client['customers_lastname']);
			
			// On d�coupe le "cpath" pour r�cup�rer les ID 				
			$DecoupeCpath= explode("_",$tuple['cpath']);
			
			//Requetes pour r�cup�rer l'URL des rayons, des marques et des mod�les
			$requete_rayon = 	mysql_query("SELECT 
								rubrique_id, 
								rubrique_url
								FROM
								rubrique
								WHERE
								rubrique_id = '". $tuple['id_rayon'] ."'
								");
			$data_requete_rayon = mysql_fetch_array($requete_rayon);
			
			$requete_marque = 	mysql_query("SELECT 
								categories_id, 
								categories_url
								FROM
								categories_description
								WHERE
								categories_id = '".$DecoupeCpath[0]."'
								");
			$data_requete_marque = mysql_fetch_array($requete_marque);
			
			$requete_modele = 	mysql_query("SELECT 
								categories_id, 
								categories_url
								FROM
								categories_description
								WHERE
								categories_id = '".$DecoupeCpath[1]."'
								");
			$data_requete_modele = mysql_fetch_array($requete_modele);
			
			// On stock les r�sultats de requete pour transformation dans des variables
			$url_rayon=$data_requete_rayon['rubrique_id'].'-'.strtolower(encode_url_sitemap_prod($data_requete_rayon['rubrique_url'])).'/';
			$url_marque=$data_requete_marque['categories_id'].'-' .strtolower(encode_url_sitemap_prod($data_requete_marque['categories_url'])).'/';
			$url_modele=$data_requete_modele['categories_id'].'-' .strtolower(encode_url_sitemap_prod($data_requete_modele['categories_url'])).'/';
			$url_produit=$tuple['products_id'].'-'.encode_url_sitemap_prod(strtolower(tep_get_products_url($tuple['products_id'],1))).'.html';
			
			// Affichage
			echo $row_client['customers_email_address'] . ', ';
			echo strip_accents($prenomnom) .', ';
			echo $row_client['customers_id'] .'-' .$tuple['orders_id'].', ';	  
			echo $tuple['products_id'].', ';
			echo strip_accents(tep_get_products_url($tuple['products_id'],1)).',';
			echo 'https://generalarmystore.fr/gas/'.$url_rayon.$url_marque.$url_modele.$url_produit.', ';				
			echo 'https://www.generalarmystore.fr/gas/images/products_pictures/normale/'.$tuple['products_bimage'].'<br>';
		}	
	}else{
		echo "<table><tr><td align=center><b>Aucune commande de s�lectionn�e</b></td></tr></table>";
	}
	?>
	
</body>
</html>