<?php
/*
  PAGE: duplicate_2.php
  Version du 11/06/2007
*/

require('includes/application_top.php');

if(isset($_POST['envoyer'])) {
  
	//copie du produit
	if(is_numeric($_POST['exemple-article']) && is_numeric($_POST['nouveau-article']))
		{
			$tableau_references='SELECT categories_id '
							   .'FROM products_to_categories '
							   .'WHERE products_id="'.$_POST['exemple-article'].'"';
			$reponse_tableau_references=mysql_query($tableau_references); $i=1;
			
			while($fetch=mysql_fetch_object($reponse_tableau_references))
				{
					$references[$i]=$fetch->categories_id;
					$i++;
				}
				
			for($i=1;$i<=count($references);$i++)
				{				
					$requete_ajout='INSERT INTO products_to_categories ( products_id , categories_id ) '
								  .'VALUES ("'.$_POST['nouveau-article'].'", "'.$references[$i].'"); '; 
					$rep=mysql_query($requete_ajout);
					
					$requete_categorie='SELECT categories_name '
									  .'FROM categories_description '
									  .'WHERE categories_id="'.$references[$i].'"';
					$reponse_cat=mysql_query($requete_categorie);
					$fetch=mysql_fetch_object($reponse_cat);
					
					echo '<div class="infoBoxHeading">Ajout du produit <b>'.$_POST['nouveau-article'].'</b> pour le t&eacute;l&eacute;phone : <b>'.$fetch->categories_name.'</b></div>';
					
					
				}
		}
}


 
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title>Dupliquer un article 2 - <?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/lib/jquery1.3.2.js"></script>
<script language="JavaScript" src="ajax/duplicate.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<div id="popupcalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Dupliquer un article 2</td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table>
		</td>
       </tr>
	   
	   <tr>
	  	  <td align="left" class="main">	
		  	Cette page permet de dupliquer un article dans les mod�les ou se trouve le deuxi�me article.<br />
			...<br />
		  </td>
       </tr>
	   <tr>
	  	  <td>
		  
	   		<form action="duplicate_2.php" method="post">
	   		  <table width="100%" border="0" cellspacing="5" cellpadding="5">
     	 		<tr class="infoBoxContent">
        		  <td width="30%">Selectionnez l'article qui va servire de guide pour la copie :</td>
        		  <td>
                  	
                    
                    <select name="rayon" id="rayon">
                	<?php
					$reponse_rayon = tep_db_query("SELECT *
												   FROM rubrique
												   ORDER BY rubrique_id");
					while($donnees_rayon = tep_db_fetch_array($reponse_rayon)) {
						
						?>
						<option value="<?php echo $donnees_rayon['rubrique_id']; ?>"><?php echo $donnees_rayon['rubrique_name']; ?></option>
						<?php
					}
					
					?>
					</select>
                    
           			
					
					<!--echo tep_draw_products_specials_pull_down_specials('products_id', 'style="font-size:10px"', $specials_array);-->
					
					<select name="marque" id="marque">
                        <option value="">Choisissez une marque</option>
                        <?php
                        $reponse_marque = tep_db_query("SELECT cd.categories_name, cd.categories_id, cd.categories_url, cd.balise_title_lien_texte, categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, categories_status_adm, categories_status_vip
                                                         FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id inner join categories_rubrique as cr on c.categories_id=cr.categories_id
                                                         WHERE c.parent_id='0' AND cr.rubrique_id = 1 and c.categories_status='1'
                                                         ORDER BY c.sort_order, cd.categories_name ASC");
                        while($donnees_marque = tep_db_fetch_array($reponse_marque)){
                            ?>
                            <option value="<?php echo $donnees_marque['categories_id']; ?>"><?php echo $donnees_marque['categories_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
            
                    <select name="modele" id="modele">
                        <option value="">Choisissez un mod�le</option>
                    </select>
                    
                    
                    <select name="exemple-article" id="products_id" disabled="disabled">
                        <option value="">Choisissez un article</option>
                    </select>
                    
        		</td>
      		  </tr>
      		  <tr class="infoBoxContent">
        		<td>Selectionnez l'article � dupliquer : </td>
        		<td>
                
                <select name="rayon_new" id="rayon_new">
                	<?php
					$reponse_rayon = tep_db_query("SELECT *
												   FROM rubrique
												   ORDER BY rubrique_id");
					while($donnees_rayon = tep_db_fetch_array($reponse_rayon)) {
						
						?>
						<option value="<?php echo $donnees_rayon['rubrique_id']; ?>"><?php echo $donnees_rayon['rubrique_name']; ?></option>
						<?php
					}
					
					?>
					</select>
                    
           			
					
					<!--echo tep_draw_products_specials_pull_down_specials('products_id', 'style="font-size:10px"', $specials_array);-->
					
					<select name="marque_new" id="marque_new">
                        <option value="">Choisissez une marque</option>
                        <?php
                        $reponse_marque = tep_db_query("SELECT cd.categories_name, cd.categories_id, cd.categories_url, cd.balise_title_lien_texte, categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, categories_status_adm, categories_status_vip
                                                         FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id inner join categories_rubrique as cr on c.categories_id=cr.categories_id
                                                         WHERE c.parent_id='0' AND cr.rubrique_id = 1 and c.categories_status='1'
                                                         ORDER BY c.sort_order, cd.categories_name ASC");
                        while($donnees_marque = tep_db_fetch_array($reponse_marque)){
                            ?>
                            <option value="<?php echo $donnees_marque['categories_id']; ?>"><?php echo $donnees_marque['categories_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
            
                    <select name="modele_new" id="modele_new">
                        <option value="">Choisissez un mod�le</option>
                    </select>
                    
                    
                    <select name="nouveau-article" id="products_new" disabled="disabled">
                        <option value="">Choisissez un article</option>
                    </select>
                    
            <?
			
			for($i=1;$i<=count($liste_articles_nouveau);$i++)
				{
					if($_POST['nouveau-article']==$liste_articles_nouveau[$i]['id'])
						{
							echo '<option value="'.$liste_articles_nouveau[$i]['id'].'" selected>'.$liste_articles_nouveau[$i]['name'].' - '.$liste_articles_nouveau[$i]['ref'].'</option>';
						}
					else
						{
							echo '<option value="'.$liste_articles_nouveau[$i]['id'].'">'.$liste_articles_nouveau[$i]['name'].' - '.$liste_articles_nouveau[$i]['ref'].'</option>';
						}
				}
		?>
          </select>
		  
		  
       
	   </td>
      </tr>
	  <tr>
	    <td>
		  <input type="submit" name="envoyer" value="Copier" />
	  	</td>
	  </tr> 
    </table>
	   </td>
      </tr>
	  
	
</form>
	   
	   
	   
	  </table>
	 </td>
	</tr>

      </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
