<?php require('includes/application_top.php'); ?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<script language="javascript" src="includes/javascript/ajax/fiche_client.js"></script>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<?php require(DIR_WS_INCLUDES . 'header.php');


$type_array=array(array('id' => '1', 'text' => 'Particulier'),array('id' => '2', 'text' => 'Professionnel'),array('id' => '3', 'text' => 'Revendeur'));

$req=tep_db_query("select * from ".TABLE_CUSTOMERS." c, ".TABLE_CUSTOMERS_INFO." ci where ci.customers_info_id=c.customers_id and c.customers_id=".$_GET['cID']."");
$res=tep_db_fetch_array($req);
$cust_type=$res['customers_type']-1;

$req2=tep_db_query("select * from ".TABLE_PARRAINAGE." where mail_filleul='".$res['customers_email_address']."'");
$res2=tep_db_fetch_array($req2);


$timestamp_inscription = strtotime($res['customers_info_date_account_created']);
$date_inscription = date_fr($res['customers_info_date_account_created']);

$timestamp_last_log = strtotime($res['customers_info_date_of_last_logon']);
$date_last_log = date_fr($res['customers_info_date_of_last_logon']);
$page="stats";

?>
<table border="0" cellspacing="2" cellpadding="2">
<?php include('client_header.php');?>
</table>

<div style="margin-top:20px; margin-left:10px;">
<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$res['customers_type']; ?>"  width="100%">
       <tr><strong>Compte client</strong></tr>
       <tr>
         <td class="main">Date d'inscription du client : <?php echo $date_inscription; ?> </td>
         <td class="main">Date de derni&egrave;re connexion : <?php echo $date_last_log; ?></td>
         <?php if($res2['id_parrain']!=''){
			 $parrain=$res2['id_parrain'];
		 }
		 else $parrain="aucun";
		 ?>
         
         <td class="main">Parrain de ce client : <?php echo $parrain; ?></td>
         
       </tr>
</table>
</div>
<div style="margin-top:20px; margin-left:10px;">
<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$res['customers_type']; ?>" style="width:1200px;" >
       <tr><strong>Commandes</strong></tr>
       <tr>
         <td class="main">Nombre de Commandes : <?php echo $res['nb_commande']; ?> </td>
         <td class="main">Panier moyen HT : <?php if($res['nb_commande']>0)echo round($res['total_commande']/$res['nb_commande'],2); else echo 0;?> �</td>
       </tr>
       <tr>
         <td class="main">Nombre d'articles achet&eacute;s : <?php echo $res['nb_articles']; ?></td>
         <td class="main">Nombre d'articles en moyenne : <?php if($res['nb_commande']>0) echo round($res['nb_articles']/$res['nb_commande']); else echo 0;?></td>
       </tr>
       <tr>
         <td class="main">Marge totale : <?php echo $res['total_marge']; ?> �</td>
         <td class="main">Marge moyenne : <?php if($res['nb_commande']>0) echo round($res['total_marge']/$res['nb_commande'],2); else echo 0;?> �</td>
       </tr>
</table>
</div>

<?php 

$req3=tep_db_query("select * from ".TABLE_PARRAINAGE." where id_parrain=".$res['customers_id']." and statut='non lu'");
$res3=tep_db_fetch_array($req3);
$nb1=tep_db_num_rows($req3);

$req4=tep_db_query("select * from ".TABLE_PARRAINAGE." where id_parrain=".$res['customers_id']." and statut='inscrit'");
$res4=tep_db_fetch_array($req4);
$nb2=tep_db_num_rows($req4);

$req5=tep_db_query("select * from ".TABLE_PARRAINAGE." where id_parrain=".$res['customers_id']." and statut='achete'");
$nb3=tep_db_num_rows($req5);
?>
<div style="margin-top:20px; margin-left:10px;">
<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$res['customers_type']; ?>" style="width:1200px;" >
       <tr><strong>Parrainage</strong></tr>
       <tr>
         <td class="main">Filleul non inscrit : <?php echo $nb1; ?></td>
         <td class="main">Filleul inscrit sans commandes : <?php echo $nb2; ?></td>
         <td class="main">Filleul avec commande : <?php echo $nb3; ?></td>
       </tr>
       
       <?php
	   $nb_commande=0;
	   $total_achat=0;
	   $total_marge=0;
	   if($nb3>0){
			$nb_commande=0;
			$total_achat=0;
			$total_marge=0;
			while($res5=tep_db_fetch_array($req5)){
				$req=tep_db_query("select * from ".TABLE_CUSTOMERS." where customers_email_address='".$res5['mail_filleul']."'");
				$res=tep_db_fetch_array($req);
				$nb_commande+=$res['nb_commande'];
				$total_achat+=$res['total_commande'];
				$total_marge+=$res['total_marge'];
			} 
	   
		}
		?>
       
       <tr>
         <td class="main">Nombre de Commande : <?php echo $nb_commande; ?></td>
         <td class="main">Total HT des achats : <?php echo $total_achat; ?> �</td>
         <td class="main">Total Marge : <?php echo $total_marge; ?> �</td>
       </tr>
</table>
</div>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>