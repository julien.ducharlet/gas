<?php

if($_GET['ajax']==1) {
	
	include("includes/database_tables.php");
	require('includes/configure.php');
	require('includes/functions/general.php');
	require('includes/functions/database.php');
	require('includes/functions/html_output.php');
	
	tep_db_connect() or die('Unable to connect to database server!');
}
else include('includes/application_top.php');


if (isset($_GET['action']) && !empty($_GET['action']) && isset($_GET['pID']) && !empty($_GET['pID'])) {
	
	switch ($action) {
		
		case 'add':
			
			if($_GET['ajax']==1) {
				
				$query = 'select * from '. TABLE_PRODUCTS_ALERT_STOCK .' where products_id='. $_GET['pID'];
				$query = tep_db_query($query);
				
				if(tep_db_num_rows($query)==0) {
					
					$query = 'insert into '. TABLE_PRODUCTS_ALERT_STOCK .'(products_id) values('. $_GET['pID'] .')';
					tep_db_query($query);
					
					echo 'infos=Une alerte de stock vient d\'�tre cr��e';
				}
				else echo 'alert=Cet article est d�j� en alerte';
			}
		break;
		
		case 'deleteconfirm':
			
			$products = explode(' ', $_GET['pID']);
			printr($products);
			foreach($products as $productTodelete) {
				
				if(!empty($productTodelete)) {
					
					$query = 'delete from '.TABLE_PRODUCTS_ALERT_STOCK  .' where products_id='. (int)$productTodelete;
					tep_db_query($query);
				}
			}
			
			$messageStack->add_session('Les Alertes de Stock ont bien �t� supprim�es', 'success');
			tep_redirect('stock_alert.php');
		
		break;
	}
}
else {
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<title><?php echo TITLE; ?></title>
<?php include('includes/meta.php'); ?>
<script language="javascript" src="includes/javascript/jQuery/tipsy.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script type="text/javascript">

$(document).ready(function() {
    
	<?php include ('includes/headers/menuJS.php'); ?>
	
    // call the tablesorter plugin
    $(".tablesorter").tablesorter();
	
	$(".hoverInfos").tipsy({html:true,
							gravity: 'e',
							trigger: 'manual'});
	
	$(".checkAll").change(function() {
		
		classToCheck = $(this).val();
		
		if($(this).attr("checked")) {
			
			$("."+ classToCheck).attr("checked", true);
			$(".checkAll").attr("checked", true);
		}
		else {
			
			$("."+ classToCheck).attr("checked", false);
			$(".checkAll").attr("checked", false);
		}
	});
	
	$("#generalAction").change(function() {
		
		if($(this).val()==1) {
			
			data = '';
			alertToDelete = $("input:checkbox").serialize().split('&');
			
			for(i=0 ; i<alertToDelete.length ; i ++) {
				
				temp = alertToDelete[i].split('=');
				data += temp[1].substring(6) + ' ';
			}
		
			if(window.confirm("�tes-vous s�r de vouloir supprimer cette s�lection ?")) document.location.href='?pID='+ data +'&action=deleteconfirm<?php if(isset($_GET['date'])) echo '&date='. $_GET['date']; ?>';
		}
		else if($(this).val()==2) {
			
			$("input:checkbox:checked").each(function(i, element) {
				
				if(!$(this).hasClass('checkAll')) {
					
					productToShow = $(this).val().substring(6);
					window.open('frs_article_a_livrer.php?pID='+ productToShow +'&action=new_product');
				}
			});
		}
	});
});
</script>
</head>
<body>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

    <div class="pageHeading">Articles en alerte de stock</div>
        
	<table border="0" width="100%" cellspacing="2" cellpadding="2">
      </tr>
      <tr>
      	<td> <?php
			$query = 'select *
					  from '. TABLE_PRODUCTS_ALERT_STOCK .' pas,
					  '. TABLE_PRODUCTS .' p,
					  '. TABLE_PRODUCTS_DESCRIPTION .' pd
					  where pas.products_id=p.products_id
					  and p.products_id=pd.products_id
					  order by products_manage_stock, products_quantity_min, products_model';
			$query = tep_db_query($query);
			?>
          
		  <table class="tablesorter">
                	<thead>
						<tr>
                    		<th class="{sorter:false}" style="width:20px;"><input type="checkbox" class="checkAll" value="productChecked"/></th>
                    		<th align="center">R�f�rence</th>
                    		<th align="center">Nom</th>
                            <th align="center" style="width:90px;">En cours de CMD</th>
                            <th align="center" style="width:90px;">En attente de livraison</th>
                            <th align="center" style="width:90px;">Rupture CMD</th>
                    		<th align="center" style="width:90px;">Qt� R�elle</th>
                            <th align="center" style="width:90px;">Qt� Virtuelle</th>
                            <th align="center" style="width:90px;">Qt� Min</th>
                            <th align="center">Icones</th>
                            <th align="center">Statut</th>
                            <th align="center" style="width:100px;">Actions</th>
                		</tr>
					</thead>
                    <tbody>
			<?php
				while($data = tep_db_fetch_array($query)) {
					
					$total_cmd_en_cours = '';
					$total_cmd_en_attente = '';
					$total_cmd_en_rupture = '';
					
					if($data['products_options_values_id']>0) {//le produit contient des options
						
						$query_options = 'select products_options_values_name from
												'. TABLE_PRODUCTS_OPTIONS_VALUES .' pov
										  where products_options_values_id='. $data['products_options_values_id'];
						$query_options = tep_db_query($query_options);
						$data_options = tep_db_fetch_array($query_options);
						
						$link = tep_href_link(FILENAME_ARTICLE_EDIT_STOCKS_OPTIONS, 'pID=' . $data['products_id'] . '&action=new_product');
					}
					else {
						
						$link = tep_href_link('frs_article_a_livrer.php', 'pID=' . $data['products_id'] . '&action=move_product');
						
					}
					
					$query_count = 'select (products_quantity_ordered) as total, so.suppliers_orders_id, suppliers_orders_num, date_added, suppliers_name
											from '. TABLE_SUPPLIERS_ORDERS .' so,
												 '. TABLE_SUPPLIERS_ORDERS_HISTORY .' soh,
												 '. TABLE_SUPPLIERS_ORDERS_TO_PRODUCTS .' sotp,
												 '. TABLE_SUPPLIERS .' s
								where so.suppliers_orders_id=soh.suppliers_orders_id
								and so.suppliers_orders_id=sotp.suppliers_orders_id
								and so.suppliers_id=s.suppliers_id
								and products_id='. $data['products_id'] .'
								and suppliers_orders_status_id=1
								and date_added=(select MAX(date_added) from '. TABLE_SUPPLIERS_ORDERS_HISTORY .' soh2 where soh2.suppliers_orders_id=soh.suppliers_orders_id)';
					$query_count = tep_db_query($query_count);
					
					$tooltip_cmd_en_cours = '<table><thead><tr><th width=\'100\'>Fournisseur</th><th>Qt�</th></tr></thead><tbody>';
					
					while($data_cmd_en_cours = tep_db_fetch_array($query_count)) {
						
						$tooltip_cmd_en_cours .= '<tr>';
							$tooltip_cmd_en_cours .= '<td><a href=\'frs_cmd_edit.php?soID='. $data_cmd_en_cours['suppliers_orders_id'] .'\' target=\'_blank\'>'. $data_cmd_en_cours['suppliers_name'] .'</a></td>';
							$tooltip_cmd_en_cours .= '<td>'. $data_cmd_en_cours['total'] .'</td>';
						$tooltip_cmd_en_cours .= '</tr>';
						
						$total_cmd_en_cours += $data_cmd_en_cours['total'];
					}
					
					$tooltip_cmd_en_cours .= '</tbody></table>';
					
					
					$query_count = 'select (products_quantity_ordered) as total, so.suppliers_orders_id, suppliers_name, suppliers_orders_num, suppliers_orders_date_foreseen
											from '. TABLE_SUPPLIERS_ORDERS .' so,
												 '. TABLE_SUPPLIERS_ORDERS_HISTORY .' soh,
												 '. TABLE_SUPPLIERS_ORDERS_TO_PRODUCTS .' sotp,
												 '. TABLE_SUPPLIERS .' s
								where so.suppliers_orders_id=soh.suppliers_orders_id
								and so.suppliers_orders_id=sotp.suppliers_orders_id
								and so.suppliers_id=s.suppliers_id
								and products_id='. $data['products_id'] .'
								and suppliers_orders_status_id=2
								and date_added=(select MAX(date_added) from '. TABLE_SUPPLIERS_ORDERS_HISTORY .' soh2 where soh2.suppliers_orders_id=soh.suppliers_orders_id)';
					$query_count = tep_db_query($query_count);
					
					$tooltip_cmd_en_attente = '<table><thead><tr><th width=\'100\'>Fournisseur</th><th width=\'100\'>Num�ro de cmd</th><th width=\'100\'>Livraison pr�vu le</th><th>Qt�</th></tr></thead><tbody>';
					
					while($data_cmd_en_attente = tep_db_fetch_array($query_count)) {
						
						$tooltip_cmd_en_attente .= '<tr>';
							$tooltip_cmd_en_attente .= '<td><a href=\'frs_cmd_edit.php?soID='. $data_cmd_en_attente['suppliers_orders_id'] .'\' target=\'_blank\'>'. $data_cmd_en_attente['suppliers_name'] .'</a></td>';
							$tooltip_cmd_en_attente .= '<td>'. $data_cmd_en_attente['suppliers_orders_num'] .'</td>';
							$tooltip_cmd_en_attente .= '<td>'. showDateFr('%d %B %Y', $data_cmd_en_attente['suppliers_orders_date_foreseen']) .'</td>';
							$tooltip_cmd_en_attente .= '<td>'. $data_cmd_en_attente['total'] .'</td>';
						$tooltip_cmd_en_attente .= '</tr>';
						
						$total_cmd_en_attente += $data_cmd_en_attente['total'];
					}
					
					
					$query_count = 'select (products_quantity_ordered-products_quantity_delivered) as total, so.suppliers_orders_id, suppliers_orders_num, suppliers_name, date_added
												from '. TABLE_SUPPLIERS_ORDERS .' so,
													 '. TABLE_SUPPLIERS_ORDERS_HISTORY .' soh,
													 '. TABLE_SUPPLIERS_ORDERS_TO_PRODUCTS .' sotp,
													 '. TABLE_SUPPLIERS .' s
								where so.suppliers_orders_id=soh.suppliers_orders_id
								and so.suppliers_orders_id=sotp.suppliers_orders_id
								and so.suppliers_id=s.suppliers_id
								and products_id='. $data['products_id'] .'
								and suppliers_orders_status_id=3
								and products_quantity_ordered>products_quantity_delivered
								and date_added=(select MAX(date_added) from '. TABLE_SUPPLIERS_ORDERS_HISTORY .' soh2 where soh2.suppliers_orders_id=soh.suppliers_orders_id)';
					$query_count = tep_db_query($query_count);
					
					$tooltip_cmd_en_rupture = '<table><thead><tr><th width=\'100\'>Fournisseur</th><th width=\'100\'>Num�ro de cmd</th><th width=\'100\'>Livraison re�u le</th><th>Qt�</th></tr></thead><tbody>';
					
					while($data_cmd_en_rupture = tep_db_fetch_array($query_count)) {
						
						$tooltip_cmd_en_rupture .= '<tr>';
							$tooltip_cmd_en_rupture .= '<td><a href=\'frs_cmd_edit.php?soID='. $data_cmd_en_rupture['suppliers_orders_id'] .'\' target=\'_blank\'>'. $data_cmd_en_rupture['suppliers_name'] .'</a></td>';
							$tooltip_cmd_en_rupture .= '<td>'. $data_cmd_en_rupture['suppliers_orders_num'] .'</td>';
							$tooltip_cmd_en_rupture .= '<td>'. showDateFr('%d %B %Y', $data_cmd_en_rupture['suppliers_orders_date_foreseen']) .'</td>';
							$tooltip_cmd_en_rupture .= '<td>'. $data_cmd_en_rupture['total'] .'</td>';
						$tooltip_cmd_en_rupture .= '</tr>';
						
						$total_cmd_en_rupture += $data_cmd_en_rupture['total'];
					}
					
					
					
					if($data['products_manage_stock']=='stock' && $data['products_quantity_min']==0) {
				
						echo '<tr class="hiddenRow">' . "\n";
					}
					else echo '<tr>';
					
					echo '<td><input type="checkbox" class="productChecked" value="alert_'. $data['products_id'] .'" name="alert"></td>';
					echo '<td><a href="'. $link .'" target="_blank">'. $data['products_model']. '</a></td>';
					echo '<td>';
						echo '<a href="'. $link .'" target="_blank">'. bda_product_name_transform($data['products_name']) .'</a>';
						
						if($data['products_options_values_id']>0) {
							
							echo '<br/><a style="color:#7F7F7F;font-size:12px;font-weight:bold;">'. $data_options['products_options_values_name'] .'</a>';
						}
					
					echo '</td>';
					echo '<td align="center"><span class="hoverInfos cursor" title="'. $tooltip_cmd_en_cours .'" onclick=\'$(this).tipsy("show"); return false;\'>'. $total_cmd_en_cours .'</span></td>';
					echo '<td align="center"><span class="hoverInfos cursor" title="'. $tooltip_cmd_en_attente .'">'. $total_cmd_en_attente .'</span></td>';
					echo '<td align="center"><span class="hoverInfos cursor" title="'. $tooltip_cmd_en_rupture .'">'. $total_cmd_en_rupture .'</span></td>';
					echo '<td align="center">'. $data['products_quantity_reel']. '</td>';
					echo '<td align="center">'. ( ($data['products_quantity']<=0) ? '<a style="color:#FF0000;font-weight:bold;">' : '<a>') . $data['products_quantity'] .'</td>';
					echo '<td align="center">'. $data['products_quantity_min'] .'</a></td>';
					echo '<td>';
						
						if($data['products_date_prevision_cmd']!=0) {
							
							echo '<img src="..'. DIR_WS_ADMIN .'images/icons/icone_vente_sur_cmd.jpg" alt="Selection">';
						}
						
						if(!empty($data['prix_vente'])) {
							
							echo '<img src="..'. DIR_WS_ADMIN .'images/icons/icone_vente_flash.jpg" alt="Vente Flash" title="'. round($data['prix_vente'], 2) .' &euro;">';
						}
						
						if($data['products_icon']=='destock') {
							
							echo '<img src="..'. DIR_WS_ADMIN .'images/icons/icone_vente_destock.jpg" alt="D�stockage" title="Destockage">';
						}
						
						if(!empty($data['specials_new_products_price'])) {
							
							echo '<img src="..'. DIR_WS_ADMIN .'images/icons/icone_vente_promo.jpg" alt="PROMO" title="'. round($data['specials_new_products_price'], 2) .' &euro;">';
						}
						
						
						if($data['products_icon']=='fin_vie') {
							
							echo '<img src="..'. DIR_WS_ADMIN .'images/icons/icone_vente_fin_de_vie.jpg" alt="Fin de s�rie">';
						}
						elseif($data['products_icon']=='selection') {
							
							echo '<img src="..'. DIR_WS_ADMIN .'images/icons/icone_vente_selection.jpg" alt="Selection">';
						}
						
					echo '</td>';
					echo '<td align="center">';
						
						if ($data['products_status']) {
                        	echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10);
                        } 
						else {
                        	echo tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                        }
						
					echo '</td>';
					echo '<td class="actionButtons">';
						echo '<a href="' . tep_href_link('article_edit.php', 'pID=' . $data['products_id'] . '&action=new_product') . '" target="_blank">' . tep_image(DIR_WS_IMAGES . '/icons/editer.png', "Editer") . '</a>';
						echo '<a href="'. tep_href_link('article_edit_prix_fournisseurs.php', 'pID=' . $data['products_id'] . '&action=new_product') . '" target="_blank">'. tep_image(DIR_WS_ICONS .'icone_prix.png', 'Stock / Options') .'</a>';
						//Commande Fournisseur
						echo '<a href="'. tep_href_link('frs_article_a_livrer.php', 'pID=' . $data['products_id'] . '&action=new_product') . '" target="_blank">'. tep_image(DIR_WS_ICONS .'icone_caddie.png', 'Passer commande chez un fournisseur') .'</a>';
						echo '<a onclick="if (window.confirm(\'Etes vous sur de vouloir effacer cette alerte ?\')){return true;} else {return false;}" href="?pID='. $data['products_id'] .'&action=deleteconfirm">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>';
					echo '</td>';
				echo '</tr>';
            }
			?>
            </tbody>
            <tfoot>
            <tr>
                <td align="center"><!--<input type="checkbox" class="checkAll" value="productChecked"/>--></td>
                <td colspan="11">
                	<select id="generalAction">
                    	<option value="0">Choisir une action</option>
                        <option value="1">supprimer la s�lection</option>
                        <option value="2">passer commande</option>
                    </select>
                </td>
            </tr>
            </tfoot>
            </table>
            
      	</td>
      </tr>
    </table>
	</td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php
}
?>