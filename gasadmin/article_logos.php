<?php
/*
  $Id: article_logos.php,v 1.55 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

if (tep_not_null($action)) {
	  
    switch ($action) {
		
      case 'insert':
		if ($logo_image = new upload('logo_image', DIR_FS_CATALOG_IMAGES_LOGOS)) {
			
			$query = 'insert into '. TABLE_PRODUCTS_LOGOS .'(logo_name, logo_hover) values(\''.  $logo_image->filename .'\', \''.  $HTTP_POST_VARS['logo_hover'] .'\')';
			tep_db_query($query);
			
			$logo_id = tep_db_insert_id();
		}
		
		tep_redirect(tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . 'logoID=' . $logo_id));
		
		break;
	  	
     case 'save':
	 	
		$sql_data_array = array();
		
        if (isset($HTTP_GET_VARS['logoID'])) $logo_id = tep_db_prepare_input($HTTP_GET_VARS['logoID']);
		
		//ici on fait la requete de mise � jour de l'image si on up une nouvelle image
        if (!empty($_FILES['logo_image']['name']) && $logo_image = new upload('logo_image', DIR_FS_CATALOG_IMAGES_LOGOS)) {
		  //on met a jour la base de donn�e et on upload l'image si le champ logo_name n'est pas vide
		  if($logo_image->filename != "")
		  
			$sql_data_array['logo_name'] = $logo_image->filename;
			
        }
		
		$sql_data_array['logo_hover'] = $HTTP_POST_VARS['logo_hover'];
		
        tep_db_perform(TABLE_PRODUCTS_LOGOS, $sql_data_array, 'update', "logo_id = '" . (int)$logo_id . "'");
		
        tep_redirect(tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . 'logoID=' . $logo_id));
        break;
		
     case 'deleteconfirm':
        $logo_id = tep_db_prepare_input($HTTP_GET_VARS['logoID']);

        if (isset($HTTP_POST_VARS['delete_image']) && ($HTTP_POST_VARS['delete_image'] == 'on')) {
          $logo_query = tep_db_query("select logo_name from " . TABLE_PRODUCTS_LOGOS . " where logo_id = '" . (int)$logo_id . "'");
          $logo_info = tep_db_fetch_array($logo_query);

          $image_location = DIR_FS_CATALOG_IMAGES_LOGOS . $logo_info['logo_name'];

          if (file_exists($image_location)) @unlink($image_location);
        }

		tep_db_query("delete from " . TABLE_PRODUCTS_LOGOS ." where logo_id = '" . (int)$logo_id . "'");
		tep_db_query("delete from " . TABLE_PRODUCTS_TO_LOGOS ." where logo_id = '" . (int)$logo_id . "'");
		
        tep_redirect(tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page']));
        break;
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Les Logos des articles pr&eacute;sents sur <? echo STORE_NAME; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent">&nbsp;&nbsp;Les Logos des articles</td>
                <td class="dataTableHeadingContent" align="right">Action&nbsp;&nbsp;</td>
              </tr>
<?php

$query_logo = 'select * from '. TABLE_PRODUCTS_LOGOS .' order by logo_name';

$query_logo = tep_db_query($query_logo);

$logo_id = $HTTP_GET_VARS['logoID'];
  
while ($manufacturers = tep_db_fetch_array($query_logo)) {
	
    if ((!isset($HTTP_GET_VARS['logoID']) || (isset($HTTP_GET_VARS['logoID']) && ($HTTP_GET_VARS['logoID'] == $manufacturers['logo_id']))) && !isset($lInfo) && (substr($action, 0, 3) != 'new')) {
      $logo_products_query = tep_db_query("select count(*) as logos_count from " . TABLE_PRODUCTS_TO_LOGOS . " where logo_id = '" . (int)$manufacturers['logo_id'] . "'");
      $logo_products = tep_db_fetch_array($logo_products_query);

      $lInfo_array = array_merge($manufacturers, $logo_products);
      $lInfo = new objectInfo($lInfo_array);
    }

    if (isset($lInfo) && is_object($lInfo) && ($manufacturers['logo_id'] == $lInfo->logo_id)) {

		echo '<tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $manufacturers['logo_id'] . '&action=edit') . '\'">' . "\n";
    } 
	else {
      
		echo '<tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $manufacturers['logo_id']) . '\'">' . "\n";
    }
	
?>
                <td class="dataTableContent"><?php echo $manufacturers['logo_name']; ?></td>
                <td class="dataTableContent" align="right"><?php if (isset($lInfo) && is_object($lInfo) && ($manufacturers['logo_id'] == $lInfo->logo_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $manufacturers['logo_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo ""; ?></td>
                    <td class="smallText" align="right"><?php echo ""; ?></td>
                  </tr>
                </table></td>
              </tr>
<?php
  if (empty($action)) {
?>
              <tr>
                <td align="right" colspan="2" class="smallText"><?php echo '<a href="' . tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $lInfo->logo_id . '&action=new') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>'; ?></td>
              </tr>
<?php
  }
?>
            </table></td>
<?php 
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<b>Ajouter un nouveau logo</b>');

      $contents = array('form' => tep_draw_form('logo', FILENAME_TYPE_PRODUCTS_PICTURES, 'action=insert', 'post', 'enctype="multipart/form-data"'));
      $contents[] = array('text' => '<br />Image du logo : <br />'. tep_draw_file_field('logo_image') .'<br />');
      $contents[] = array('text' => '<br />Texte au survol de l\'image : <br />'. tep_draw_textarea_field('logo_hover', 'hard', 30, 5, ''));
      
      $contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $HTTP_GET_VARS['logoID']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
	  
    case 'edit':
	  
	  $query = 'select * from '. TABLE_PRODUCTS_LOGOS .' where logo_id='. $_GET['logoID'];
	  $query = tep_db_query($query);
	  
	  $data = tep_db_fetch_array($query);
	  
      $heading[] = array('text' => '<b>' . 'Editer le Logo' . '</b>');

      $contents = array('form' => tep_draw_form('logo', FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $data['logo_id'] . '&action=save', 'post', 'enctype="multipart/form-data"'));
      $contents[] = array('text' => 'Merci de faire les changements n&eacute;cessaires');
      $contents[] = array('text' => '<br />Image du logo : <br />'. tep_draw_file_field('logo_image') .'<br />'. $lInfo->logo_name);
      $contents[] = array('text' => '<br />Texte au survol de l\'image : <br />'. tep_draw_textarea_field('logo_hover', 'hard', 30, 5, $data['logo_hover']));
	  
      $contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $lInfo->logo_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
	  
    case 'delete':
      $heading[] = array('text' => '<b>Supprimer ce logo</b>');

      $contents = array('form' => tep_draw_form('logo', FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $lInfo->logo_id . '&action=deleteconfirm'));
      $contents[] = array('text' => 'Etes vous s�r de vouloir supprimer ce logo ?');
      $contents[] = array('text' => '<br /><b>' . $lInfo->logo_name . '</b>');
      $contents[] = array('text' => '<br />' . tep_draw_checkbox_field('delete_image', '', true) . ' ' . 'Suppprimer l\'image du logo ?');

      $contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $lInfo->logo_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
	  
    default:
        $heading[] = array('text' => '<b>' . $lInfo->logo_name . '</b>');

        $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $lInfo->logo_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_TYPE_PRODUCTS_PICTURES, 'page=' . $HTTP_GET_VARS['page'] . '&logoID=' . $lInfo->logo_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', 'Supprimer') . '</a>');
        $contents[] = array('text' => '<br />Date d\'ajout : ' . tep_date_short($lInfo->date_added));
		
        if (tep_not_null($lInfo->last_modified)) $contents[] = array('text' => 'Derni�re modification : ' . tep_date_short($lInfo->last_modified));
		
        $contents[] = array('text' => '<br /><div align="center">' . tep_image(DIR_WS_CATALOG_IMAGES_LOGOS . $lInfo->logo_name, $lInfo->logo_name) . '
																	</div>');
        $contents[] = array('text' => '<br />' . 'Produits :' . ' ' . $lInfo->logos_count);
     	
      break;
  }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
