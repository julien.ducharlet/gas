<?php
require('includes/application_top.php'); 
require(DIR_WS_CLASSES . 'currencies.php');
include("../". DIR_WS_SITE ."/includes/fonctions/mail.php");

$currencies = new currencies();

function seadate($day) {
	
	$rawtime = strtotime("-".$day." days");
	$ndate = date("Ymd", $rawtime);
	return $ndate;
}

function cart_date_short($raw_date) {
	if ( ($raw_date == '00000000') || ($raw_date == '') ) return false;

	$year = substr($raw_date, 0, 4);
	$month = (int)substr($raw_date, 4, 2);
	$day = (int)substr($raw_date, 6, 2);

	if (@date('Y', mktime(0, 0, 0, $month, $day, $year)) == $year) {
	  return date(DATE_FORMAT, mktime(0, 0, 0, $month, $day, $year));
	} 
	else {
	  return ereg_replace('2037' . '$', $year, date(DATE_FORMAT, mktime(0, 0, 0, $month, $day, 2037)));
	}
}


// This will return a list of customers with sessions. Handles either the mysql or file case
// Returns an empty array if the check sessions flag is not true (empty array means same SQL statement can be used)
function _GetCustomerSessions() {
	
	$cust_ses_ids = array();
	
	if( RCS_CHECK_SESSIONS == 'true' ) {
		
		if(STORE_SESSIONS == 'mysql') {
			
			$sesquery = tep_db_query("select value from " . TABLE_SESSIONS . " where 1");
			
			while($ses = tep_db_fetch_array($sesquery)) {
				
				if(ereg( "customer_id[^\"]*\"([0-9]*)\"", $ses['value'], $custval)) $cust_ses_ids[] = $custval[1];
			}
		}
		else {
			
			if($handle = opendir(tep_session_save_path())) {
				
				while (false !== ($file = readdir( $handle ))) {
					
					if ($file != "." && $file != "..") {
						
						$file = tep_session_save_path() . '/' . $file;	// create full path to file!
						
						if($fp = fopen($file, 'r')) {
							
							$val = fread($fp, filesize($file));
							fclose($fp);
							if(ereg( "customer_id[^\"]*\"([0-9]*)\"", $val, $custval)) $cust_ses_ids[] = $custval[1];
						}
					}
				}
				closedir( $handle );
			}
		}
	}
	return $cust_ses_ids;
}

// Supprimer un vieux panier
if ($HTTP_GET_VARS['action']=='delete') { 
   $reset_query_raw = "delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id=" . $HTTP_GET_VARS[customer_id]; 
   tep_db_query($reset_query_raw); 
   $reset_query_raw2 = "delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id=" . $HTTP_GET_VARS[customer_id]; 
   tep_db_query($reset_query_raw2);
   tep_redirect(tep_href_link(FILENAME_RECOVER_CART_SALES, 'delete=1&customer_id='. $HTTP_GET_VARS['customer_id'] . '&tdate=' . $HTTP_GET_VARS['tdate'])); 
} 
if ($HTTP_GET_VARS['delete']) { 
   $messageStack->add('Panier pour Client-ID ' . $HTTP_GET_VARS['customer_id'] . ' suppression r�ussie', 'success'); 
} 

//Marquer un appel t�l�phonique
if ($HTTP_GET_VARS['action']=='relancetel') { 
	$donequery = tep_db_query("select * from ". TABLE_SCART ." where customers_id = '".$HTTP_GET_VARS[customer_id]."'");
	if (mysql_num_rows($donequery) == 0)
		tep_db_query("insert into ".TABLE_SCART." (customers_id, dateadded, datemodified, type_modif, id_modificateur ) values ('".$HTTP_GET_VARS[customer_id] ."', '".seadate('0')."', '".seadate('0')."', 'telephone', '".$login_id."')");
	else
		tep_db_query("update ".TABLE_SCART." set datemodified = '".seadate('0')."', type_modif='t&eacute;l&eacute;phone', id_modificateur='".$login_id."' where customers_id = ".$HTTP_GET_VARS[customer_id] );
	$messageStack->add_session('Mise � jour du client pour ajout relance t�l', 'success');
	//$messageStack->add_session('addressbook', SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED, 'success');

}

// Relancer un client particulier par e-mail
if ($_REQUEST['action'] == 'relancemail') {
	envoi_mail_lost_basket($_GET['customer_id']);
}

$tdate = $_REQUEST['tdate'];
if ($tdate == '') $tdate = RCS_BASE_DAYS;

$sdate = $_REQUEST['sdate'];
if( $sdate == '' ) $sdate = RCS_SKIP_DAYS;
?>


<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET;?>">
      <title><?php echo TITLE; ?></title>
      <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
    </head>
    
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
    	<?php
        require(DIR_WS_INCLUDES . 'header.php'); 
        
        // Envoi des emails clients
        if (count($custid) > 0 ) {
            foreach ($custid as $cid){
				envoi_mail_lost_basket($cid);
            }//fin foreach
            $messageStack->add_session('Envoi mail OK !', 'success');
        }//fin if envoi mail
    	?>
    
        <table border="0" width="100%" cellspacing="2" cellpadding="2">
            <tr>
                <td width="100%" valign="top">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
                            <td colspan="12">
                                <table border="0" width="100%" height="44" style="background-color: #e1e1f5; font-size: 0.9em;">
                                    <tr>
                                        <form method="post" action="<?php echo $PHP_SELF;?>" >
                                            <td class="dataTableContent" align="left" style="font-size: 0.9em;">
                                                <?php
                                                $new_array[] = array('id' => "ok", 'text' => "seulement les nouveaux paniers");
                                                $new_array[] = array('id' => "nok", 'text' => "tous les paniers");
                                                echo "Visualiser : " . tep_draw_pull_down_menu('new', $new_array);
                                                ?>
                                                
                                                <input type="submit" value="<?php echo 'Go'; ?>">
                                            </td>
                                            
                                            <td class="dataTableContent" align="right" style="font-size: 0.8em;">
                                                  <?php echo 'Montrer depuis les derniers '; ?> <input type="text" size="4" width="4" value="<?php echo $sdate; ?>" name="sdate"> - <input type="text" size="4" width="4" value="<?php echo $tdate; ?>" name="tdate"><?php echo ' jours '; ?><input type="submit" value="<?php echo 'Go'; ?>">
                                            </td>
                                        </form>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr><td>&nbsp;</td></tr>
                 
                        <form method="post" action="<?php echo $PHP_SELF; ?>">
                        <tr class="dataTableHeadingRow">
                            <td class="dataTableHeadingContent" align="left" colspan="2" width="10%" nowrap><?php echo 'CONTACT'; ?></td>
                            <td class="dataTableHeadingContent" align="left" colspan="1" width="15%" nowrap><?php echo 'DATE'; ?></td>
                            <td class="dataTableHeadingContent" align="left" colspan="2" width="10%" nowrap>&nbsp;</td>
                            <td class="dataTableHeadingContent" align="left" colspan="1" width="30%" nowrap><?php echo 'NOM'; ?></td>
                            <td class="dataTableHeadingContent" align="left" colspan="1" width="20%" nowrap><?php echo 'E-MAIL'; ?></td>
                            <td class="dataTableHeadingContent" align="left" colspan="2" width="15%" nowrap><?php echo 'TELEPHONE'; ?></td>
                        </tr>
                        <tr class="dataTableHeadingRow">
                            <td class="dataTableHeadingContent" align="left"   colspan="2"  width="10%" nowrap>&nbsp; </td>
                            <td class="dataTableHeadingContent" align="left"   colspan="1"  width="15%" nowrap><?php echo 'REFERENCE'; ?></td>
                            <td class="dataTableHeadingContent" align="left"   colspan="3" width="55%" nowrap><?php echo 'ARTICLE'; ?></td>
                            <td class="dataTableHeadingContent" align="center" colspan="1" width="5%" nowrap> <?php echo 'QTE'; ?></td>
                            <td class="dataTableHeadingContent" align="right"  colspan="1"  width="5%" nowrap><?php echo 'PRIX HT'; ?></td>
                            <td class="dataTableHeadingContent" align="right"  colspan="1" width="10%" nowrap><?php echo 'TOTAL HT'; ?></td>
						</tr>
                        
						<?php
							$cust_ses_ids = _GetCustomerSessions();
							$bdate = seadate($sdate);
							$ndate = seadate($tdate);
							
							$query1 = tep_db_query("SELECT cb.customers_id cid, cb.products_id pid, cb.customers_basket_quantity qty, cb.customers_basket_date_added bdate, a.entry_country_id,
														cus.customers_firstname fname, cus.customers_lastname lname, cus.customers_country, cus.customers_telephone phone, cus.customers_email_address email
												 FROM   " . TABLE_CUSTOMERS_BASKET . " cb, " . TABLE_CUSTOMERS . " cus LEFT JOIN " . TABLE_ADDRESS_BOOK . " a ON cus.customers_default_address_id = a.address_book_id
												 WHERE  cb.customers_basket_date_added <= '" . $bdate . "' AND cb.customers_basket_date_added > '" . $ndate . "' AND
														cus.customers_id not in ('" . implode(", ", $cust_ses_ids) . "') AND cb.customers_id = cus.customers_id 
												ORDER BY cb.customers_basket_date_added desc, cb.customers_id ");
							$results = 0;
							$curcus = "";
							$total_price = 0;
							$total_marge = 0;
							$totalAll = 0;
							$first_line = true;
							$skip = false;
							
							// Modif permettant d'afficher par d�faut que les gens pas encore relanc�s
							if (!isset($_REQUEST["new"]))
								$_REQUEST["new"] = 'ok';
							
							$knt = mysql_num_rows($query1);
							for ($i = 0; $i <= $knt; $i++){
								$inrec = tep_db_fetch_array($query1);
								
								// If this is a new customer, create the appropriate HTML
								if ($curcus != $inrec['cid']){
									// output line
									$totalAll += $total_price;
									$cline .= "       </td>
													<tr><td class='dataTableContent' align='right' colspan='8'><b>" . 'Panier Total: ' . "</b>" . $currencies->format($total_price) . " soit ".$currencies->format($total_price*1.2)."(TTC) pour une marge HT de : ".$currencies->format($total_marge)."</td></tr>
													<tr>
													<td align='right'><a href=".tep_href_link(FILENAME_RECOVER_CART_SALES,"action=delete&customer_id=".$curcus."&tdate=".$tdate."&sdate=".$sdate).">".tep_image_button('button_delete.gif', 'Effacer ce panier perdu')."</a></td>
													<td colspan='5'>&nbsp;</td>
													<td colspan='1' align='right'><a href=".tep_href_link(FILENAME_RECOVER_CART_SALES,"action=relancetel&customer_id=".$curcus ) . ">".tep_image_button('button_relance_tel.gif', 'Indiquer que le client a �t� relanc� par t�l�phone')."</a></td>
													<td>&nbsp;&nbsp;</td>
													<td colspan='1' align='right'><a href='" . $_SERVER['PHP_SELF'] . "?action=relancemail&customer_id=".$curcus."'>".tep_image_button('button_relance_mail.gif', 'Indiquer que le client a �t� relanc� par email')."</a></td>
													</tr>
													<tr><td colspan='11'><hr> </td></tr>\n";
									if ($curcus != "" && !$skip)
										echo $cline;
									
									// set new cline and curcus
									$curcus = $inrec['cid'];
									
									if ($curcus != ""){
										$total_price = 0;
										$total_marge = 0;
										
										// change the color on those we have contacted add customer tag to customers
										$fcolor = RCS_UNCONTACTED_COLOR;
										$checked = 1;	// assume we'll send an email
										$new = 1;
										$skip = false;
										$sentdate = "";
										$beforeDate = RCS_CARTS_MATCH_ALL_DATES ? '0' : $inrect['bdate'];
										$res_pays=tep_db_query("select countries_name from countries where countries_id='".$inrec['entry_country_id']."'");
										$row_pays=tep_db_fetch_array($res_pays);
										$customer = get_drapeau($row_pays['countries_name'])." ".$inrec['fname'] . " " . $inrec['lname'];
										$status = "";
									
										$donequery = tep_db_query("select * from ". TABLE_SCART ." where customers_id = '".$curcus."'");
										$emailttl = seadate(RCS_EMAIL_TTL);
										$deja_contacte="nok";
										
										if (mysql_num_rows($donequery) > 0) {
											$ttl = tep_db_fetch_array($donequery);
											if( $ttl ){
												if( tep_not_null($ttl['datemodified']) )	// allow for older scarts that have no datemodified field data
													$ttldate = $ttl['datemodified'];
												else
													$ttldate = $ttl['dateadded'];
									
												if ($emailttl <= $ttldate) {
													$sentdate = $ttldate;
													$fcolor = RCS_CONTACTED_COLOR;
													$checked = 0;
													$new = 0;
													if($_REQUEST["new"]=="ok"){
														$skip=true;
													}
												}
											}
										}
						
										// See if the customer has purchased from us before
										// Customers are identified by either their customer ID or name or email address
										// If the customer has an order with items that match the current order, assume order completed, bail on this entry!
										$ccquery = tep_db_query('select orders_id, orders_status from ' . TABLE_ORDERS . ' where (customers_id = ' . (int)$curcus . ' OR customers_email_address like "' . $inrec['email'] .'" or customers_name like "' . $inrec['fname'] . ' ' . $inrec['lname'] . '") and date_purchased >= "' . $beforeDate . '"' );
										
										if (mysql_num_rows($ccquery) > 0){
											// We have a matching order; assume current customer but not for this order
											$customer = '<font color=' . RCS_CURCUST_COLOR . '><b>' . $customer . '</b></font>';
										
											// Now, look to see if one of the orders matches this current order's items
											while( $orec = tep_db_fetch_array( $ccquery ) ){
												$ccquery = tep_db_query( 'select products_id from ' . TABLE_ORDERS_PRODUCTS . ' where orders_id = ' . (int)$orec['orders_id'] . ' AND products_id = ' . (int)$inrec['pid'] );
												if( mysql_num_rows( $ccquery ) > 0 ){
													if( $orec['orders_status'] > RCS_PENDING_SALE_STATUS )
														$checked = 0;
										
													// OK, we have a matching order; see if we should just skip this or show the status
													if( (RCS_SKIP_MATCHED_CARTS == 'true' ) && !$checked ){
														$skip = true;	// reset flag & break us out of the while loop!
														break;
													}
													else{
														// It's rare for the same customer to order the same item twice, so we probably have a matching order, show it
														$fcolor = RCS_MATCHED_ORDER_COLOR;
														$skip = true;
														$ccquery = tep_db_query("select orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = " . (int)$languages_id . " AND orders_status_id = " . (int)$orec['orders_status'] );
											
														if( $srec = tep_db_fetch_array( $ccquery ) )
															$status = ' [' . $srec['orders_status_name'] . ']';
														else
															$status = ' ['. 'CUSTOMER' . ']';
													}
												}
											}
											if( $skip )
												continue;	// got a matched cart, skip to next one
										}
										
										$sentInfo = 'Non contact�';
							
										if ($sentdate != '')
											$sentInfo = cart_date_short($sentdate);
										
										$cline = "
											<tr bgcolor=" . $fcolor . ">
											<td class='dataTableContent' align='center' >" . tep_draw_checkbox_field('custid[]', $curcus, RCS_AUTO_CHECK == 'true' ? $checked : 0) . "</td>
											<td class='dataTableContent' align='left' nowrap><b>" . $sentInfo . "</b></td>
											<td class='dataTableContent' align='left' nowrap> " . cart_date_short($inrec['bdate']) . "</td>
											<td class='dataTableContent' align='left' colspan='2' nowrap>&nbsp;";
										
										$cline.="</td><td class='dataTableContent' align='left' nowrap><a href='" . tep_href_link(FILENAME_CLIENT_LIST, 'search=' . $inrec['lname'], 'NONSSL') . "'>" . $customer . "</a>".$status."</td>
											<td class='dataTableContent' align='left' nowrap><a href='" . tep_href_link('mail.php', 'selected_box=tools&customer=' . $inrec['email']) . "'>" . $inrec['email'] . "</a></td>
											<td class='dataTableContent' align='left' colspan='2' nowrap>" . $inrec['phone'] . "</td>
											</tr>";
									}
								}
								
								// We only have something to do for the product if the quantity selected was not zero!
								if ($inrec['qty'] != 0){
									// Get the product information (name, price, etc)
									$query2 = tep_db_query("select  p.products_quantity, p.products_price price, p.products_cost cost, p.products_tax_class_id taxclass, p.products_model model,	pd.products_name name
															from    " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd
															where   p.products_id = '" . (int)$inrec['pid'] . "' and pd.products_id = p.products_id and pd.language_id = " . (int)$languages_id );
									$inrec2 = tep_db_fetch_array($query2);
						
									// Check to see if the product is on special, and if so use that pricing
									$sprice = tep_get_products_special_price( $inrec['pid'] );
									if( $sprice < 1 )
										$sprice = $inrec2['price'];
									// Some users may want to include taxes in the pricing, allow that. NOTE HOWEVER that we don't have a good way to get individual tax rates based on customer location yet!
									if( RCS_INCLUDE_TAX_IN_PRICES  == 'true' )
										$sprice += ($sprice * tep_get_tax_rate( $inrec2['taxclass'] ) / 100);
									else if( RCS_USE_FIXED_TAX_IN_PRICES  == 'true' && RCS_FIXED_TAX_RATE > 0 )
										$sprice += ($sprice * RCS_FIXED_TAX_RATE / 100);
						
									// BEGIN OF ATTRIBUTE DB CODE
									$prodAttribs = ''; // DO NOT DELETE
									if (RCS_SHOW_ATTRIBUTES == 'true'){
										$attribquery = tep_db_query("select cba.products_id pid, po.products_options_name poname, pov.products_options_values_name povname
																		  from    " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " cba, " . TABLE_PRODUCTS_OPTIONS . " po, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov
																		  where   cba.products_id ='" . $inrec['pid'] . "' and cba.customers_id = " . $curcus . " and
																					 po.products_options_id = cba.products_options_id and pov.products_options_values_id = cba.products_options_value_id and
																					 po.language_id = " . (int)$languages_id . " and pov.language_id = " . (int)$languages_id);
										$hasAttributes = false;
										if (tep_db_num_rows($attribquery)){
										  $hasAttributes = true;
										  $prodAttribs = '<br>';
						
										  while ($attribrecs = tep_db_fetch_array($attribquery))
											 $prodAttribs .= '<small><i> - ' . $attribrecs['poname'] . ' ' . $attribrecs['povname'] . '</i></small><br>';
										}
									}

									// END OF ATTRIBUTE DB CODE
									$total_price = $total_price + ($inrec['qty'] * $sprice);
									$total_marge = $total_marge + ($inrec['qty'] * ($sprice - $inrec2['cost']));
									$pprice_formated  = $currencies->format($sprice);
									$tpprice_formated = $currencies->format(($inrec['qty'] * $sprice));
											
									/********************* EXTENSION POLYTECH *********************/
									$repertoire = explode("_",$inrec['pid']);
									$inrec2['name']=bda_product_name_transform($inrec2['name'],$repertoire[2]);							
									// FIN EXTENSION POLYTECH 
									/********************* FIN EXTENSION ***************************/
									if(!empty($repertoire[1])) {
										
										$query_rubrique = 'select rubrique_id from '. TABLE_CATEGORIES_RAYON .' where categories_id='. $repertoire[1];
										$query_rubrique = tep_db_query($query_rubrique);
										$data_rubrique = tep_db_fetch_array($query_rubrique);
										
								
										$cline .= "<tr class='dataTableRow'>
												<td class='dataTableContent' align='left' vAlign='top' colspan='2' width='12%' nowrap> &nbsp;</td>
												<td class='dataTableContent' align='left' vAlign='top' width='13%' nowrap>" . $inrec2['model'] . "</td>
												<td class='dataTableContent' align='left' vAlign='top' colspan='3' width='55%'>
												<a href='".tep_href_link(FILENAME_ARTICLE_EDIT, 'action=new_product&pID=' . $repertoire[0] .'&cPath='.$repertoire[1]."_".$repertoire[2] 
												, 'NONSSL') . "'><b>" . $inrec2['name']. "</b></a>" . $prodAttribs . "
												<a href=\"". BASE_DIR ."/". $data_rubrique['rubrique_id'] ."-1/".$repertoire[1]."-2/".$repertoire[2]."-3.html\" target=\"_blank\">voir la cat�gories</a>
												</td>
												<td class='dataTableContent' align='center' vAlign='top' width='5%' nowrap>" . $inrec['qty']." (".$inrec2['products_quantity'].")" . "</td>
												<td class='dataTableContent' align='right'  vAlign='top' width='5%' nowrap>" . $pprice_formated . "</td>
												<td class='dataTableContent' align='right'  vAlign='top' width='10%' nowrap>" . $tpprice_formated . "</td>
												</tr>";
									}
								}
							}

							$totalAll_formated = $currencies->format($totalAll);
							$cline = "<tr></tr><td class='dataTableContent' align='right' colspan='8'><hr align=right width=55><b>" . 'Grand Total: ' . "</b>". $totalAll_formated . "</td></tr>";
							$cline .= "<tr></tr><td class='dataTableContent' align='right' colspan='8'><hr align=right width=55><b>" . 'Grand Total: ' . "TTC</b>".round($totalAll_formated*1.2,2)."</td></tr>";
							echo $cline;
							echo "<tr><td colspan=8><hr size=1 color=000080><b>". 'facultatif PS Message: ' ."</b><br>". 
							tep_draw_textarea_field('message', 'soft', '80', '5') ."<br>" . 
							tep_draw_selection_field('submit_button', 'submit', 'Envoyer e-mail') . "</td></tr>";
						?>
						</form>
						
					</table>
				</td>
			</tr>
        </table>
        
		<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
        <br />
		
    </body>
</html>

<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>