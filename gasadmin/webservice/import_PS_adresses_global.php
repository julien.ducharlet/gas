<?php
// Permet de faire un export de tous les clients 
// pour lancer la page
// https://www.generalarmystore.fr/gasadmin/webservice/import_PS_adresses_global.php

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/webservice/donnees/import_PS_adresses_global.csv','w+');
	
	$write = 'ID Adresse;'; // ID de l'adresse
	$write .= 'Alias;';// Alias*
	$write .= 'Actif;'; // Actif (0/1)
	$write .= 'Email;'; // E-mail du client*
	$write .= 'ID client;'; // ID du client
	$write .= 'Marque;'; // Marque
	$write .= 'Fournisseurs;'; // Fournisseurs
	$write .= 'Societe;'; // Soci�t�
	$write .= 'Nom;'; // Nom*
	$write .= 'Prenom;'; // Pr�nom*
	$write .= 'Adresse 1;'; // Adresse*
	$write .= 'Adresse 2;'; // Adresse (2)
	$write .= 'Code postal;'; // Code postal*
	$write .= 'Ville;'; // Ville*
	$write .= 'Pays;'; // Pays*
	$write .= 'Etat;'; // �tat
	$write .= 'Autre;'; // Autre
	$write .= 'Telephone Fixe;'; // T�l�phone
	$write .= 'Telephone Mobile;'; // T�l�phone mobile
	$write .= 'Numero de TVA;'; // Num�ro de TVA
	$write .= 'DNI/NIF/NIE'; // Num�ro d'identification fiscale

	// a ajouter
	
	
	fwrite( $fp , $write . "\n");
	
	$query = tep_db_query("	SELECT 
								
								ab.entry_address_title,
								ab.entry_company,
								ab.entry_lastname,
								ab.entry_firstname,
								ab.entry_street_address,
								ab.entry_suburb,
								ab.entry_postcode,
								ab.entry_city,
								ab.entry_country_id,
								
								c.customers_telephone,
								c.customers_fixe,
								c.customers_tva,	
								c.customers_email_address,
								c.customers_id,

								cou.countries_id,
								cou.countries_id_prestashop
																
							FROM 
							
								customers c,
								address_book ab,
								countries cou
								
							WHERE
							
								c.customers_id = ab.customers_id 
								AND
								ab.entry_country_id = cou.countries_id
								
							ORDER BY customers_id");
							//AND
							//c.customers_id <= 500
							
							// LIMIT 1, 1000
							// AND
							
							//	p.products_id IN (8843, 8833, 8223)
							
	$i=99;
	
 	while($sql = tep_db_fetch_array($query) ){
		
		// On retire les retours chariot \n du commentaire sur le client		
		//$commentaire_sur_client = str_replace(array(";", "\r\n", "\r", "\n", PHP_EOL, chr(10), chr(13), chr(10).chr(13)), "", $sql['customers_comment']);
		// on formate la date au format de prestashop 
		// 
		$i++;
		
		$write = $i . ";"; // ID de l'adresse on laisse vide
		$write .= strip_accents(retirer_les_caracteres_speciaux($sql['entry_address_title'])) . ";"; // nom de l'adresse
		$write .= "1;";
		$write .= strtolower($sql['customers_email_address']) . ";"; // adresse email 
		$write .= $sql['customers_id'] . ";"; // ID du client
		$write .= ";"; // Marque on laisse vide
		$write .= ";"; // Fournisseur on laisse vide
		
		
		$write .= strip_accents(strtoupper(retirer_les_caracteres_speciaux($sql['entry_company']))) . ";"; // Soci�t�
		$write .= strip_accents(strtoupper(retirer_les_caracteres_speciaux($sql['entry_lastname']))) . ";"; // Nom
		$write .= ucfirst(strip_accents(strtolower(retirer_les_caracteres_speciaux($sql['entry_firstname'])))) . ";"; // Pr�nom
		$write .= strip_accents(retirer_les_caracteres_speciaux(strtolower($sql['entry_street_address']))) . ";"; // Adresse 1
		$write .= strip_accents(retirer_les_caracteres_speciaux(strtolower($sql['entry_suburb']))) . ";"; // Adresse 2
		$write .= $sql['entry_postcode'] . ";"; // Code postale
		$write .= strip_accents(strtoupper(retirer_les_caracteres_speciaux($sql['entry_city']))) . ";"; // Ville 
		$write .= $sql['countries_id_prestashop'] . ";"; // ID du pays 
		$write .= ";"; // Etat du pays on laisse vide
		$write .= ";"; // Autre on laisse vide
		$write .= $sql['customers_fixe'] . ";"; // T�l�phone Fixe
		$write .= $sql['customers_telephone'] . ";"; // T�l�phone Mobile		
		$write .= $sql['customers_tva'] . ";"; // Numero de TVA		
		$write .= ""; // Num�ro d'identification fiscale on laisse vide
		
		
		
		fwrite( $fp , $write . "\n");
	}	
/*
Explication dans l'ordre du fichier pour EB

ID
Alias*
Actif (0/1)
E-mail du client*
ID client
Marque
Fournisseurs
Soci�t�
Nom*
Pr�nom *
Adresse*
Adresse (2)
Code postal*
Ville *
Pays*
�tat
Autre
T�l�phone
T�l�phone mobile
Num�ro de TVA
Num�ro d'identification fiscale

*/	
	
?>









