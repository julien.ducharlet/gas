<?php
// Permet de faire un export de tous les clients 
// pour lancer la page
// https://www.generalarmystore.fr/gasadmin/webservice/import_PS_clients_global.php

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/webservice/donnees/import_PS_clients_global.csv','w+');
		
	$write = 'ID;'; // id du client
	$write .= 'Actif;'; // 1=oui et 2=non
	$write .= 'H ou F ;'; // 1=H et 2=F   
	$write .= 'Email;'; // adresse email
	$write .= 'Mot de passe;'; // adresse email
	$write .= 'Date de naissance;'; // date de naissance
	$write .= 'Nom;'; // Nom
	$write .= 'Prenom;'; // Pr�nom
	$write .= 'Recevoir newsletter;'; // accepte ou non la newsletter 0=non ou 1=oui
	$write .= 'Recevoir news partenaire;'; // accepte ou non la newsletter partenaire 0=non ou 1=oui
	//$write .= 'Date de cr�ation;'; // Date de cr�ation du compte uniquement dans la version PS 1.7
	$write .= 'Groupe du client;'; // 1=3 - 2=3 - 3=4 - 4=3
	$write .= 'Groupe par defaut;'; // 1=3 - 2=3 - 3=4 - 4=3
	$write .= 'ID boutique'; // ID de la boutique
		
	// a ajouter
	//$write .= 'customers_comment;'; // commentaire interne sur le client
	//$write .= 'customers_info_date_account_last_modified'; // Date de dernier modification
	//$write .= 'porte monnaie virtuel '; // montant en porte monnaie virtuel
	
	fwrite( $fp , $write . "\n");
	
	$query = tep_db_query("	SELECT 
								c.customers_id,
								c.customers_gender,
								c.customers_type,
								c.customers_company,
								c.customers_siret,
								c.customers_firstname,
								c.customers_lastname,
								c.customers_email_address,
								c.customers_dob,
								c.customers_comment,
								c.customers_newsletter,
								
								ci.customers_info_date_account_created, 
								ci.customers_info_date_account_last_modified 
								
							FROM 
							
								customers c,
								customers_info ci
								
							WHERE
							
								c.customers_id = ci.customers_info_id 
								
								
							ORDER BY c.customers_id");
							//AND
							//c.customers_id <= 500
							
							// LIMIT 1, 1000
							// AND
							
							//	p.products_id IN (8843, 8833, 8223)
							
	
 	while($sql = tep_db_fetch_array($query) ){
		
		// On retire les retours chariot \n du commentaire sur le client		
		//$commentaire_sur_client = str_replace(array(";", "\r\n", "\r", "\n", PHP_EOL, chr(10), chr(13), chr(10).chr(13)), "", $sql['customers_comment']);
		// on formate la date au format de prestashop 
		// 
		
		
		$write = $sql['customers_id'] . ";";
		$write .= "1;";
		
		// Modification du "m" en 1 et du "f" en 2
		if ($sql['customers_gender'] == 'm') {
			$write .= "1;"; 
		} else { 
			$write .= "2;";
		}
		
		$write .= strtolower($sql['customers_email_address']) . ";"; // adresse email 
		$write .= "azerty;"; // Mot de passe
		if ($sql['customers_dob'] != '0000-00-00 00:00:00') { // si la date de naissance n'est pas vide 
			$write .= date("Y-m-d", strtotime($sql['customers_dob'])) . ";"; // date de naissance format XXXX-XX-XX date("Y-m-d", strtotime($sql['customers_dob']))
		} else { // sinon 
			$write .= ";";
		}
		$write .= retirer_les_caracteres_speciaux(strip_accents(strtoupper($sql['customers_lastname']))) . ";";	// Nom 
		$write .= retirer_les_caracteres_speciaux(strip_accents(strtolower(ucfirst($sql['customers_firstname'])))) . ";"; // Pr�nom strip_accents
		$write .= $sql['customers_newsletter'] . ";"; // accepte ou non la newsletter 0=non ou 1=oui
		$write .= "0;"; // accepte ou non la newsletter partenaire 0=non ou 1=oui
		//$write .= date("Y-m-d", strtotime($sql['customers_info_date_account_created'])) . ";"; // Date de cr�ation du compte uniquement dans la version PS 1.7
		
		// modification du groupe entre OSC et PS (1=3 - 2=3 - 3=4 - 4=3)
		if ($sql['customers_type'] == '3') {
			$write .= "4;4;"; 
		} else { 
			$write .= "3;3;";
		}
		
		$write .= "1"; // ID de la boutique
		
		/*
		$write .= $commentaire_sur_client . ";";		
		$write .= $sql['customers_info_date_account_last_modified'];
		*/
		
		fwrite( $fp , $write . "\n");
	}	
/*
Explication dans l'ordre du fichier pour EB

id_customer
id_gender
id_default_group
firstname
lastname
email
birthday
note
date_add
date_upd
*/	
	
?>









