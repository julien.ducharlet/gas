<?php
// Permet de faire un export de tous les produits 
// pour lancer la page
// https://www.generalarmystore.fr/gasadmin/webservice/produits_global.php

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/webservice/donnees/produits_global.csv','w+');
		
	$write = 'id_product;'; 
	$write .= 'id_manufacturer;'; 
	$write .= 'id_category_default;'; 
	$write .= 'ean13;';
	$write .= 'ecotax;';
	$write .= 'quantity;';
	$write .= 'low_stock_threshold;';
	$write .= 'price;';
	$write .= 'wholesale_price;';
	$write .= 'reference;';
	$write .= 'reference_interne;';
	$write .= 'weight;'; 
	$write .= 'out_of_stock;';
	$write .= 'active;';
	$write .= 'available_date;';
	$write .= 'date_add;';
	$write .= 'date_upd;';
	$write .= 'Image 1;';
	$write .= 'Image 2;';
	$write .= 'Image 3;';
	$write .= 'Image 4;';
	$write .= 'Image 5;';
	$write .= 'description;'; 
	$write .= 'meta_description;'; 
	$write .= 'meta_keywords;';
	$write .= 'meta_title;';
	$write .= 'name';
	
	fwrite( $fp , $write . "\n");
	
	$query = tep_db_query("	SELECT 
								p.products_id, 
								m.manufacturers_id_prestashop,
								p.products_code_barre,
								p.products_ecotaxe,
								p.products_quantity_reel,
								p.products_quantity_min,								
								p.products_price,
								p.products_cost,
								p.products_ref_origine,
								p.products_model,								
								p.products_weight,
								p.products_manage_stock,
								p.products_status,								
								p.products_date_available,
								p.products_date_added,
								p.products_last_modified,
								p.products_bimage,
								
								pd.products_description,
								pd.products_head_desc_tag, 
								pd.products_head_keywords_tag,
								pd.products_head_title_tag,								
								pd.products_name,
								
								m.manufacturers_id_prestashop
								
							FROM 
							
								products p,
								products_description pd,
								manufacturers m
								
							WHERE
							
								p.products_id = pd.products_id 
								AND
								p.manufacturers_id = m.manufacturers_id
								
							ORDER BY products_id");
							//AND
							//p.products_id <= 2427
							
							// LIMIT 1, 1000
							// AND
							
							//	p.products_id IN (8843, 8833, 8223)
							//
							// ORDER BY products_id");
	
 	while( $sql = tep_db_fetch_array($query) ){
		
		// On retire les retours chariot \n de la description		
		$description = str_replace(array(";", "\r\n", "\r", "\n", PHP_EOL, chr(10), chr(13), chr(10).chr(13)), "", $sql['products_description']);
		
		$write = $sql['products_id'] . ";";
		$write .= $sql['manufacturers_id_prestashop'] . ";"; 
		$write .= "8;";
		$write .= $sql['products_code_barre'] . ";";
		$write .= $sql['products_ecotaxe'] . ";";
		$write .= $sql['products_quantity_reel'] . ";";
		$write .= $sql['products_quantity_min'] . ";";
		$write .= $sql['products_price'] . ";";
		$write .= $sql['products_cost'] . ";";
		$write .= $sql['products_ref_origine'] . ";";
		$write .= $sql['products_model'] . ";";
		$write .= $sql['products_weight'] . ";";
		$write .= product_manage_stock($sql['products_manage_stock']) . ";"; 
		$write .= $sql['products_status'] . ";";
		$write .= $sql['products_date_available'] . ";";
		$write .= $sql['products_date_added'] . ";";
		$write .= $sql['products_last_modified'] . ";";
		
			
			$tableau = array();
			$images = mysql_query("SELECT products_bimage FROM products_image WHERE products_id = ". $sql['products_id'] ." ORDER BY index_image");
						
			while( $image = mysql_fetch_array($images) ){				
				$tableau[] = $image['products_bimage'];
			}
			$url_image_ori = 'https://www.generalarmystore.fr/gas/images/products_pictures/ori/';
			
		
		if (!empty($sql['products_bimage'])) {
			$write .= $url_image_ori . $sql['products_bimage'] . ";"; // Image 1 
		} else { $write .= ";"; /* Image 1 */ }
		
		if (!empty($tableau[0])) {
			$write .= $url_image_ori . $tableau[0] . ";"; // Image 2
		} else { $write .= ";"; /* Image 2 */ }
		if (!empty($tableau[1])) {
			$write .= $url_image_ori . $tableau[1] . ";"; // Image 3
		} else { $write .= ";"; /* Image 3 */}
		if (!empty($tableau[2])) {
			$write .= $url_image_ori . $tableau[2] . ";"; // Image 4
		} else { $write .= ";"; /* Image 4 */}
		if (!empty($tableau[3])) {
			$write .= $url_image_ori . $tableau[3] . ";"; // Image 5
		} else { $write .= ";"; /* Image 5 */ }
		
		/*
		$write .= $url_image_ori . $sql['products_bimage'] . ";"; // Image 1 
		$write .= $url_image_ori . $tableau[0] . ";"; // Image 2
		$write .= $url_image_ori . $tableau[1] . ";"; // Image 3
		$write .= $url_image_ori . $tableau[2] . ";"; // Image 4
		$write .= $url_image_ori . $tableau[3] . ";"; // Image 5
		*/
		$write .= $description . ";"; 
		//$write .= $sql['products_head_desc_tag'] . ";"; 
		$write .= str_replace(array(";", "\r\n", "\r", "\n", PHP_EOL, chr(10), chr(13), chr(10).chr(13)), "", $sql['products_head_desc_tag']) . ";"; 
		//$write .= $sql['products_head_keywords_tag'] . ";"; 
		$write .= str_replace(array(";", "\r\n", "\r", "\n", PHP_EOL, chr(10), chr(13), chr(10).chr(13)), "", $sql['products_head_keywords_tag']) . ";";
		//$write .= $sql['products_head_title_tag'] . ";"; 
		$write .= str_replace(array(";", "\r\n", "\r", "\n", PHP_EOL, chr(10), chr(13), chr(10).chr(13)), "", $sql['products_head_title_tag']) . ";";
		$write .= $sql['products_name']; // 5325
				
		fwrite( $fp , $write . "\n");

	}	
?>









