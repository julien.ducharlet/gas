<?php
// Permet de faire un export de tous les clients 
// pour lancer la page
// https://www.generalarmystore.fr/gasadmin/webservice/client_global.php

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/webservice/donnees/clients_global.csv','w+');
		
	$write = 'customers_id;'; 
	$write .= 'customers_gender;'; // 1=Mr et 2=mme    
	$write .= 'customers_type;'; // 1=3 - 2=3 - 3=4 - 4=3
	// $write .= 'customers_company;'; // nom de la soci�t� 
	// $write .= 'customers_siret;'; // N� de siret
	$write .= 'customers_firstname;'; // Pr�nom
	$write .= 'customers_lastname;'; // Nom
	$write .= 'customers_email_address;'; // adresse email
	$write .= 'customers_dob;'; // date anniverssaire
	$write .= 'customers_comment;'; // commentaire interne sur le client
	$write .= 'customers_info_date_account_created;'; // Date de cr�ation du compte
	$write .= 'customers_info_date_account_last_modified'; // Date de dernier modification
	
	fwrite( $fp , $write . "\n");
	
	$query = tep_db_query("	SELECT 
								c.customers_id,
								c.customers_gender,
								c.customers_type,
								c.customers_company,
								c.customers_siret,
								c.customers_firstname,
								c.customers_lastname,
								c.customers_email_address,
								c.customers_dob,
								c.customers_comment,
								
								ci.customers_info_date_account_created, 
								ci.customers_info_date_account_last_modified 
								
							FROM 
							
								customers c,
								customers_info ci
								
							WHERE
							
								c.customers_id = ci.customers_info_id 
								
								
							ORDER BY c.customers_id");
							//AND
							//c.customers_id <= 500
							
							// LIMIT 1, 1000
							// AND
							
							//	p.products_id IN (8843, 8833, 8223)
							
	
 	while( $sql = tep_db_fetch_array($query) ){
		
		// On retire les retours chariot \n du commentaire sur le client		
		$commentaire_sur_client = str_replace(array(";", "\r\n", "\r", "\n", PHP_EOL, chr(10), chr(13), chr(10).chr(13)), "", $sql['customers_comment']);
		
		$write = $sql['customers_id'] . ";";

		// Modification du "m" en 1 et du "f" en 2
		if ($sql['customers_gender'] == 'm') {
			$write .= "1;"; 
		} else { 
			$write .= "2;";
		}
		// modification du groupe entre OSC et PS (1=3 - 2=3 - 3=4 - 4=3)
		if ($sql['customers_type'] == '3') {
			$write .= "4;"; 
		} else { 
			$write .= "3;";
		}
		
		// $write .= $sql['customers_company'] . ";";
		// $write .= $sql['customers_siret'] . ";";
		
		$write .= $sql['customers_firstname'] . ";";
		$write .= $sql['customers_lastname'] . ";";
		$write .= $sql['customers_email_address'] . ";";
		$write .= $sql['customers_dob'] . ";";
		$write .= $commentaire_sur_client . ";";
		$write .= $sql['customers_info_date_account_created'] . ";";
		$write .= $sql['customers_info_date_account_last_modified'];
				
		fwrite( $fp , $write . "\n");

	}	
/*
Explication dans l'ordre du fichier pour EB

id_customer
id_gender
id_default_group
firstname
lastname
email
birthday
note
date_add
date_upd
*/	
	
?>









