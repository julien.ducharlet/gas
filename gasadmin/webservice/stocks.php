<?php
// Permet de faire un export du stock des produits 
// pour lancer la page
// https://www.generalarmystore.fr/gasadmin/webservice/stocks.php

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/webservice/donnees/export_stock.csv','w+');
	
	$write = 'products_id' . ';' ; 
	$write .= 'products_quantity' . ';' ; 
	$write .= 'products_quantity_reel' ;
	fwrite( $fp , $write . "\n");
	
	$query = tep_db_query("	SELECT 
							p.products_id, 
							p.products_quantity, 
							p.products_quantity_reel
							FROM 
							products p 
							ORDER BY p.products_id");
	
	
 	while( $sql = tep_db_fetch_array($query) ){
				
		$write = $sql['products_id'] . ';' ;
		$write .= $sql['products_quantity'] . ';' ;
		$write .= $sql['products_quantity_reel'] ; 
		fwrite( $fp , $write . "\n");
	}	
?>