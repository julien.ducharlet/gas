<?php
// Permet de faire un export de toutes les adresses des clients 
// pour lancer la page
// https://www.generalarmystore.fr/gasadmin/webservice/adresses_global.php

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/webservice/donnees/adresses_global.csv','w+');
	
	
	$write = 'customers_id;'; // ID du client
	$write .= 'address_name;';// Nom de l'adresse
	$write .= 'customers_gender;';// H ou F (1=Mr et 2=mme)
	$write .= 'customers_company;'; // Nom de soci�t�
	$write .= 'customers_firstname;'; // Pr�nom
	$write .= 'customers_lastname;'; // Nom
	$write .= 'customers_address;'; // Adresse
	$write .= 'customers_address_complement;'; // Compl�ment d'adresse
	$write .= 'post_code;'; // Code postal
	$write .= 'city;'; // Ville
	$write .= 'country'; // Pays

	
	fwrite( $fp , $write . "\n");
	
	$query = tep_db_query("	SELECT 
								ab.customers_id,
								ab.entry_address_title,
								ab.entry_gender,
								ab.entry_company,
								ab.entry_firstname , 
								ab.entry_lastname , 
								ab.entry_street_address , 
								ab.entry_suburb , 
								ab.entry_postcode , 
								ab.entry_city , 
								ab.entry_country_id 								
								
							FROM 
							
								address_book ab								
								
							WHERE
							
								ab.customers_id <= 500
								
							ORDER BY customers_id ASC, address_book_id ASC");
							//AND
							//c.customers_id <= 500
							
							// LIMIT 1, 1000
							// AND
							
							//	p.products_id IN (8843, 8833, 8223)
							
	
 	while( $sql = tep_db_fetch_array($query) ){
		
		$write = $sql['customers_id'] . ";";
		$write .= $sql['entry_address_title'] . ";";
		
		// Modification du "m" en 1 et du "f" en 2
		if ($sql['customers_gender'] == 'm') {
			$write .= "1;"; 
		} else { 
			$write .= "2;";
		}
				
		$write .= $sql['entry_company'] . ";";
		$write .= $sql['entry_firstname'] . ";";
		$write .= $sql['entry_lastname'] . ";";
		$write .= $sql['entry_street_address'] . ";";
		$write .= $sql['entry_suburb'] . ";";
		$write .= $sql['entry_postcode'] . ";";
		$write .= $sql['entry_city'] . ";";		
		$write .= $sql['entry_country_id'];
				
		fwrite( $fp , $write . "\n");

	}	
/*
Explication dans l'ordre du fichier pour EB


*/	
	
?>









