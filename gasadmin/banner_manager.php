<?php
//Gestion des banni�res
require('includes/application_top.php');

$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
$banner_extension = tep_banner_image_extension();

if (tep_not_null($action)) {  
	switch ($action) {
		case 'setflag':
			if ( ($HTTP_GET_VARS['flag'] == '0') || ($HTTP_GET_VARS['flag'] == '1') ) {
				tep_set_banner_status($HTTP_GET_VARS['bID'], $HTTP_GET_VARS['flag']);
				$messageStack->add_session('Success : Le statut de la bani&egrave;re a &eacute;t&eacute; mis &agrave; jour.', 'success');
			} else {
				$messageStack->add_session('Erreur : Statut flag inconnu.', 'error');
			}

			tep_redirect(tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $HTTP_GET_VARS['bID']));
		break;
		
		case 'insert':
		case 'update':
        if (isset($_POST['banners_id'])) $banners_id = tep_db_prepare_input($_POST['banners_id']);
        $banners_title = tep_db_prepare_input($_POST['banners_title']);
		$banners_pages = implode(',', tep_db_prepare_input($_POST['banners_pages']));
        $banners_url = tep_db_prepare_input($_POST['banners_url']);
        $new_banners_group = tep_db_prepare_input($_POST['new_banners_group']);
        $banners_group = (empty($new_banners_group)) ? tep_db_prepare_input($_POST['banners_group']) : $new_banners_group;
        $banners_html_text = tep_db_prepare_input($_POST['banners_html_text']);
		$banners_hover_text = tep_db_prepare_input($_POST['banners_hover_text']);
        $banners_image_local = tep_db_prepare_input($_POST['banners_image_local']);
        $banners_image_target = tep_db_prepare_input($_POST['banners_image_target']);
        $db_image_location = '';
        $expires_date = tep_db_prepare_input($_POST['expires_date']);
        //$expires_impressions = tep_db_prepare_input($_POST['expires_impressions']);
        $date_scheduled = tep_db_prepare_input($_POST['date_scheduled']);

		$banner_error = false;
		if (empty($banners_title)) {			
			$messageStack->add('Erreur : Titre de bani&egrave;re requis.', 'error');
			$banner_error = true;
		}

		if (empty($banners_group)) {
			$messageStack->add('Erreur : Groupe de bani&egrave;re requis.', 'error');
			$banner_error = true;
        }

		if (empty($banners_html_text)) {			
			if (!empty($_FILES['banners_image']['name'])) {
				$banners_image = new upload('banners_image');
				$banners_image->set_destination(DIR_FS_BANNER_IMAGES . $banners_image_target);
				if (!$banners_image->parse() || !$banners_image->save()) {	
					$banner_error = true;
				}
			}
        }

		if ($banner_error == false) {
			
			$db_image_location = (empty($banners_image->filename)) ? $banners_image_local : $banners_image_target . $banners_image->filename;
			$sql_data_array = array('banners_title' => $banners_title,
									'pages' => $banners_pages,
									'banners_url' => $banners_url,
									'banners_image' => $db_image_location,
									'banners_group' => $banners_group,
									'banners_html_text' => $banners_html_text,
									'banners_hover' => $banners_hover_text);

			if ($action == 'insert') {
				$insert_sql_data = array('date_added' => 'now()', 'status' => '1');
				$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
				tep_db_perform(TABLE_BANNERS, $sql_data_array);
				$banners_id = tep_db_insert_id();
				$messageStack->add_session('Success : La bani&egrave;re a &eacute;t&eacute; ins&eacute;r&eacute;e.', 'success');
			} elseif ($action == 'update') {
				tep_db_perform(TABLE_BANNERS, $sql_data_array, 'update', "banners_id = '" . (int)$banners_id . "'");
				$messageStack->add_session('Success : La bani&egrave;re a &eacute;t&eacute; mise &agrave; jour.', 'success');
			}

			if(tep_not_null($expires_date)) {  
				list($day, $month, $year) = explode('/', $expires_date);
				$expires_date = $year . ((strlen($month) == 1) ? '0' . $month : $month) . ((strlen($day) == 1) ? '0' . $day : $day);
				tep_db_query("update ". TABLE_BANNERS ." set expires_date = '" . tep_db_input($expires_date) . "' where banners_id = '" . (int)$banners_id . "'");
			} else {
				tep_db_query("update ". TABLE_BANNERS ." set expires_date = NULL where banners_id = '" . (int)$banners_id . "'");
			}
 
			if(tep_not_null($date_scheduled)) {
				list($day, $month, $year) = explode('/', $date_scheduled); 
				$date_scheduled = $year . ((strlen($month) == 1) ? '0' . $month : $month) . ((strlen($day) == 1) ? '0' . $day : $day);
				tep_db_query("update " . TABLE_BANNERS . " set date_scheduled = '" . tep_db_input($date_scheduled) . "' where banners_id = '" . (int)$banners_id . "'");
			} else {
				tep_db_query("update " . TABLE_BANNERS . " set date_scheduled = NULL where banners_id = '" . (int)$banners_id . "'"); 
			}
			tep_redirect(tep_href_link(FILENAME_BANNER_MANAGER, (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . 'bID=' . $banners_id));
        } else { 
			$action = 'new';
		}
		break;
		
		case 'deleteconfirm':
        $banners_id = tep_db_prepare_input($HTTP_GET_VARS['bID']);
        if (isset($HTTP_POST_VARS['delete_image']) && ($HTTP_POST_VARS['delete_image'] == 'on')) {
			$banner_query = tep_db_query("select banners_image from " . TABLE_BANNERS . " where banners_id = '" . (int)$banners_id . "'");
			$banner = tep_db_fetch_array($banner_query);

			if (is_file(DIR_FS_BANNER_IMAGES . $banner['banners_image'])) {
				if (is_writeable(DIR_FS_BANNER_IMAGES . $banner['banners_image'])) {
					unlink(DIR_FS_BANNER_IMAGES . $banner['banners_image']);
				} else { 
					$messageStack->add_session('Erreur : L\'image ne peut pas &ecirc;tre supprim&eacute;e.', 'error');
				}
			} else {
				$messageStack->add_session('Erreur : L\'image n\'existe pas.', 'error');
			}
		}
		tep_db_query("delete from " . TABLE_BANNERS . " where banners_id = '" . (int)$banners_id . "'");
		tep_db_query("delete from " . TABLE_BANNERS_HISTORY . " where banners_id = '" . (int)$banners_id . "'");
		$messageStack->add_session('Success : La bani&egrave;re a &eacute;t&eacute; supprim&eacute;e.', 'success');
        tep_redirect(tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page']));
        break;
	}
}

// check if the graphs directory exists
$dir_ok = false;
if (function_exists('imagecreate') && tep_not_null($banner_extension)) {
    if (is_dir('..'. DIR_WS_BANNER_IMAGES)) {
		if (is_writeable('..'. DIR_WS_BANNER_IMAGES)) { 
			$dir_ok = true;
		} else { 
			$messageStack->add('Erreur : Le r&eacute;pertoire de banni&egrave;res n\'est pas autoris&eacute; en &eacute;criture.', 'error');
		}
	} else {
		$messageStack->add('Erreur : Le r&eacute;pertoire de banni&egrave;res n\'existe pas. Cr&eacute;er un r&eacute;pertoire \'bannieres\' dans \'images\'.'. DIR_WS_BANNER_IMAGES, 'error');
	}
}
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<style> input[onblur] {	width:84px; } </style>

<script language="javascript" src="includes/general.js"></script>
<script language="javascript"><!--
function popupImageWindow(url) {
	window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<div id="spiffycalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Gestionnaire de Banni�re</td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
if ($action=='new' || $action=='update') {
	$zones = get_set(TABLE_BANNERS, 'pages');
	$zone_liste = array();
	$page_liste = array();
	
	foreach($zones as $key) { 		  
		$zone_liste[] = array('id' => $key, 'text' => $key);
	}
}
  
if ($action == 'new') {
    $form_action = 'insert';

    $parameters = array('expires_date' => '',
                        'date_scheduled' => '',
                        'banners_title' => '',
                        'banners_url' => '',
                        'banners_group' => '',
                        'banners_image' => '',
                        'banners_html_text' => '');
                        //'expires_impressions' => '');
    $bInfo = new objectInfo($parameters);
	
    if (isset($HTTP_GET_VARS['bID'])) {
		
		$form_action = 'update';
		$bID = tep_db_prepare_input($HTTP_GET_VARS['bID']);

		$banner_query = tep_db_query("SELECT banners_title, banners_url, banners_image, banners_group, banners_html_text, banners_hover, status, date_format(date_scheduled, '%d/%m/%Y')  date_scheduled, date_format(expires_date, '%d/%m/%Y') as expires_date, expires_impressions, date_status_change FROM " . TABLE_BANNERS . " WHERE banners_id = '" . (int)$bID . "'");
		$banner = tep_db_fetch_array($banner_query);

		$bInfo->objectInfo($banner);
		//pour r�cup�rer les pages sur lesquelles la banni�re est affich�e
		$page_array = array();
	  
		$page_query = tep_db_query('SELECT pages FROM '. TABLE_BANNERS .' WHERE banners_id='. $_GET['bID']);
		$data_page = tep_db_fetch_array($page_query);
		$page_liste = explode(',', $data_page['pages']);
	} elseif (tep_not_null($HTTP_POST_VARS)) {
		$bInfo->objectInfo($HTTP_POST_VARS);
	}

    $groups_array = array();
    $groups_query = tep_db_query("SELECT DISTINCT banners_group FROM " . TABLE_BANNERS . " ORDER BY banners_group");
    while ($groups = tep_db_fetch_array($groups_query)) { 
		$groups_array[] = array('id' => $groups['banners_group'], 'text' => $groups['banners_group']);
    }
?>
<link rel="stylesheet" type="text/css" href="includes/javascript/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="includes/javascript/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="javascript">
  var dateExpires = new ctlSpiffyCalendarBox("dateExpires", "new_banner", "expires_date","btnDate1","<?php echo $bInfo->expires_date; ?>",scBTNMODE_CUSTOMBLUE);
  var dateScheduled = new ctlSpiffyCalendarBox("dateScheduled", "new_banner", "date_scheduled","btnDate2","<?php echo $bInfo->date_scheduled; ?>",scBTNMODE_CUSTOMBLUE);
</script>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr><?php echo tep_draw_form('new_banner', FILENAME_BANNER_MANAGER, (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . 'action=' . $form_action, 'post', 'enctype="multipart/form-data"'); if ($form_action == 'update') echo tep_draw_hidden_field('banners_id', $bID); ?>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main">Titre de la banni&egrave;re :</td>
            <td class="main"><?php echo tep_draw_input_field('banners_title', $bInfo->banners_title, 'style="width:75%"', true); ?></td>
          </tr>
          <tr>
            <td class="main">Afficher la banni�re sur les pages :</td>
            <td class="main"><?php echo tep_draw_pull_down_menu('banners_pages[]', $zone_liste, $page_liste, 'multiple="multiple"'); ?></td>
          </tr>
          <tr>
            <td class="main">URL banni&egrave;re :</td>
            <td class="main"><?php 	
				echo tep_draw_input_field('banners_url', $bInfo->banners_url, 'style="width:75%"'); 
				echo tep_draw_hidden_field('new_banners_group', '468x120');
			?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main" valign="top">Image :</td>
            <td class="main"><?php echo tep_draw_file_field('banners_image') . ' , ou sp&eacute;cifiez un fichier local ci-dessous<br><div style="float:left;margin-right:5px;">'. DIR_FS_BANNER_IMAGES .'</div>'. tep_draw_input_field('banners_image_local', (isset($bInfo->banners_image) ? $bInfo->banners_image : ''), 'style="width:200px"'); ?></td>
          </tr>
		  <? if (!empty($bInfo->banners_image)) { ?>
			  <tr>
				<td class="main" valign="top"></td>
				<td class="main"><img src="https://www.generalarmystore.fr/gas/images/bannieres/<?php echo $bInfo->banners_image ;?>"></td>
			  </tr>
		  <? } ?>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main">Cible de l'image (Sauvegarder dans) :</td>
            <td class="main"><?php echo '<div style="float:left;margin-right:5px;">'. DIR_FS_BANNER_IMAGES .'</div>'. tep_draw_input_field('banners_image_target', '', 'style="width:200px"'); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td valign="top" class="main">Texte HTML :</td>
            <td class="main"><?php echo tep_draw_textarea_field('banners_html_text', 'soft', '60', '5', $bInfo->banners_html_text); ?></td>
          </tr>
          <tr>
            <td valign="top" class="main">Texte au survol :</td>
            <td class="main"><?php echo tep_draw_input_field('banners_hover_text', $bInfo->banners_hover); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main">Planifi&eacute; le :<br><small>(dd/mm/yyyy)</small></td>
            <td valign="top" class="main"><script language="javascript">dateScheduled.writeControl(); dateScheduled.dateFormat="dd/MM/yyyy";</script></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td valign="top" class="main">Expire le :<br><small>(dd/mm/yyyy)</small></td>
            <td class="main"><script language="javascript">dateExpires.writeControl(); dateExpires.dateFormat="dd/MM/yyyy";</script><?php //echo ', ou &agrave;<br>' . tep_draw_input_field('expires_impressions', $bInfo->expires_impressions, 'maxlength="7" size="7"') . ' Impressions/Vues.'; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><?php echo '<b>Remarque sur la banni&egrave;re :</b><ul><li>Utilisez une image ou du texte HTML pour la banni&egrave;re mais pas les deux.</li><li>Le texte HTML a priorit&eacute; sur l\'image</li></ul><br><b>Remarque sur l\'image :</b><ul><li>Le r&eacute;pertoire de destination lors du transfert doit avoir les bonnes permissions (&eacute;criture) sur le serveur!</li><li>Ne remplissez pas la cible de l\'image (\'Sauvegarder dans\') si vous ne transf&eacute;rez pas d\'image sur le serveur web (dans le cas o&ugrave; vous utilisez une image d&eacute;ja pr&eacute;sente sur celui-ci).</li><li>La cible de l\'image (\'Sauvegarder dans\') doit pointer sur un r&eacute;pertoire existant et le slash de fin doit &ecirc;tre pr&eacute;sent (ex., banners/).</li></ul><br><b>Remarque sur l\'expiration :</b><ul><li>Seul un des 2 champs doit &ecirc;tre saisi.</li><li>Pour laisser la banni&egrave;re active, sans expiration automatique, laisser ces champs vides.</li></ul><br><b>Remarque sur la planification :</b><ul><li>Si la date de planification est pr&eacute;cis&eacute;e, la banni&egrave;re sera activ&eacute;e seulement &agrave; partir de cette date.</li><li>Toutes les banni&egrave;res planifi&eacute;es seront marqu&eacute;es inactives jusqu\'&agrave; ce que la date de planification soit atteinte. Elles seront alors actives.</li></ul>'; ?></td>
            <td class="main" align="right" valign="top" nowrap><?php echo (($form_action == 'insert') ? tep_image_submit('button_insert.gif', IMAGE_INSERT) : tep_image_submit('button_update.gif', IMAGE_UPDATE)). '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_BANNER_MANAGER, (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . (isset($HTTP_GET_VARS['bID']) ? 'bID=' . $HTTP_GET_VARS['bID'] : '')) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
          </tr>
        </table></td>
      </form></tr>
<?php
  }
  else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent">Banni&egrave;res</td>
                <td class="dataTableHeadingContent" align="center">Taille de l'image</td>
                <td class="dataTableHeadingContent" align="right">Affichages / Clics</td>
                <td class="dataTableHeadingContent" align="right">Statut</td>
                <td class="dataTableHeadingContent" align="right">Action&nbsp;</td>
              </tr>
<?php
    $banners_query_raw = "select banners_id, banners_title, banners_image, banners_group, status, expires_date, expires_impressions, date_status_change, date_scheduled, date_added from " . TABLE_BANNERS . " order by banners_title, banners_group";
    $banners_split = new splitPageResults($HTTP_GET_VARS['page'], MAX_DISPLAY_SEARCH_RESULTS, $banners_query_raw, $banners_query_numrows);
    $banners_query = tep_db_query($banners_query_raw);
    while ($banners = tep_db_fetch_array($banners_query)) {
		
      $info_query = tep_db_query("select sum(banners_shown) as banners_shown, sum(banners_clicked) as banners_clicked from " . TABLE_BANNERS_HISTORY . " where banners_id = '" . (int)$banners['banners_id'] . "'");
      $info = tep_db_fetch_array($info_query);

      if ((!isset($HTTP_GET_VARS['bID']) || (isset($HTTP_GET_VARS['bID']) && ($HTTP_GET_VARS['bID'] == $banners['banners_id']))) && !isset($bInfo) && (substr($action, 0, 3) != 'new')) {
		  
        $bInfo_array = array_merge($banners, $info);
        $bInfo = new objectInfo($bInfo_array);
      }

      $banners_shown = ($info['banners_shown'] != '') ? $info['banners_shown'] : '0';
      $banners_clicked = ($info['banners_clicked'] != '') ? $info['banners_clicked'] : '0';

      if (isset($bInfo) && is_object($bInfo) && ($banners['banners_id'] == $bInfo->banners_id)) {
		  
        echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_BANNER_STATISTICS, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $bInfo->banners_id) . '\'">' . "\n";
      }
	  else {
		  
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $banners['banners_id']) . '\'">' . "\n";
      }
	  
	  
		$url_image = 'https://www.generalarmystore.fr/gas/images/bannieres/' . $banners['banners_image'];
		$dimensions = getimagesize($url_image);
		if (!empty($banners['banners_image'])) {
			$dimensions_a_afficher = $dimensions[0] . 'x' . $dimensions[1];
		} else {
			$dimensions_a_afficher = 'pas d\'image';
		}
?>
                <td class="dataTableContent"><?php echo '<a href="javascript:popupImageWindow(\'' . FILENAME_POPUP_IMAGE . '?banner=' . $banners['banners_id'] . '\')">' . tep_image(DIR_WS_IMAGES . 'icon_popup.gif', 'View Banner') . '</a>&nbsp;' . $banners['banners_title']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $dimensions_a_afficher; ?></td>
                <td class="dataTableContent" align="right"><?php echo $banners_shown . ' / ' . $banners_clicked; ?></td>
                <td class="dataTableContent" align="right">
<?php
      if ($banners['status'] == '1') {
		  
        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', 'Active', 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $banners['banners_id'] . '&action=setflag&flag=0') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', 'Set Inactive', 10, 10) . '</a>';
      }
	  else {
		  
        echo '<a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $banners['banners_id'] . '&action=setflag&flag=1') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', 'Set Active', 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', 'Inactive', 10, 10);
      }
?></td>
                <td class="dataTableContent" align="right"><?php echo '<a href="' . tep_href_link(FILENAME_BANNER_STATISTICS, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $banners['banners_id']) . '">' . tep_image(DIR_WS_ICONS . 'statistics.gif', ICON_STATISTICS) . '</a>&nbsp;'; if (isset($bInfo) && is_object($bInfo) && ($banners['banners_id'] == $bInfo->banners_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $banners['banners_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $banners_split->display_count($banners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_BANNERS); ?></td>
                    <td class="smallText" align="right"><?php echo $banners_split->display_links($banners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page']); ?></td>
                  </tr>
                  <tr>
                    <td align="right" colspan="2"><?php echo '<a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'action=new') . '">' . tep_image_button('button_new_banner.gif', IMAGE_NEW_BANNER) . '</a>'; ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();
  switch ($action) {
    case 'delete':
      $heading[] = array('text' => '<b>' . $bInfo->banners_title . '</b>');

      $contents = array('form' => tep_draw_form('banners', FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $bInfo->banners_id . '&action=deleteconfirm'));
      $contents[] = array('text' => 'Etes vous sur de vouloir supprimer cette banni&egrave;re ?');
      $contents[] = array('text' => '<br><b>' . $bInfo->banners_title . '</b>');
      if ($bInfo->banners_image) $contents[] = array('text' => '<br>' . tep_draw_checkbox_field('delete_image', 'on', true) . ' Supprimer l\'image de la banni&egrave;re');
      $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . '&nbsp;<a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $HTTP_GET_VARS['bID']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    default:
      if (is_object($bInfo)) {
		  
        $heading[] = array('text' => '<b>' . $bInfo->banners_title . '</b>');

        $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $bInfo->banners_id . '&action=new') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $HTTP_GET_VARS['page'] . '&bID=' . $bInfo->banners_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
        $contents[] = array('text' => '<br>Date d\'ajout : ' . tep_date_short($bInfo->date_added));

        if ( (function_exists('imagecreate')) && ($dir_ok) && ($banner_extension) ) {
			
          $banner_id = $bInfo->banners_id;
          $days = '3';
          include(DIR_WS_INCLUDES . 'graphs/banner_infobox.php');
          $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image(DIR_WS_IMAGES . 'graphs/banner_infobox-' . $banner_id . '.' . $banner_extension));
        }
		else {
			
          include(DIR_WS_FUNCTIONS . 'html_graphs.php');
          $contents[] = array('align' => 'center', 'text' => '<br>' . tep_banner_graph_infoBox($bInfo->banners_id, '3'));
        }

        $contents[] = array('text' => tep_image(DIR_WS_IMAGES . 'graph_hbar_blue.gif', 'Blue', '5', '5') . ' Nombre d\'affichages<br>' . tep_image(DIR_WS_IMAGES . 'graph_hbar_red.gif', 'Red', '5', '5') . ' Nombre de clics');

        if ($bInfo->date_scheduled) $contents[] = array('text' => '<br>' . sprintf('Planifi&eacute; &agrave;: <b>%s</b>', tep_date_short($bInfo->date_scheduled)));

        if ($bInfo->expires_date) {
			
          $contents[] = array('text' => '<br>' . sprintf('Expire le : <b>%s</b>', tep_date_short($bInfo->expires_date)));
        }
		/*elseif ($bInfo->expires_impressions) {
			
          $contents[] = array('text' => '<br>' . sprintf('Expire au bout de : <b>%s</b> affichages', $bInfo->expires_impressions));
        }*/

        if ($bInfo->date_status_change) $contents[] = array('text' => '<br>' . sprintf('Changement du statut : %s', tep_date_short($bInfo->date_status_change)));
      }
      break;
  }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
	  
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
