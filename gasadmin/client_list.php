<?php
/* Fonction : Listing des clients */
  require('includes/application_top.php');
  require('includes/functions/date.php');
  require('../' . DIR_WS_SITE . '/includes/fonctions/mail.php');
  $Nombre_de_ligne_par_page=20;
  
  $action = (isset($_REQUEST['action']) ? $_REQUEST['action'] : '');
  
  if (isset($HTTP_GET_VARS['search']) && tep_not_null($HTTP_GET_VARS['search'])) {
	if(strpos($HTTP_GET_VARS['search'], "@")===false){
		$HTTP_GET_VARS['nom']=$HTTP_GET_VARS['search'];
		$GLOBALS['nom']=$HTTP_GET_VARS['search'];
	}
	else{
		$HTTP_GET_VARS['mail']=$HTTP_GET_VARS['search'];
		$GLOBALS['mail']=$HTTP_GET_VARS['search'];
	}
  }
  
  if (tep_not_null($action)) {
    switch ($action) {
      case "add_versement" :
		$date_now = date("Y-m-d H:i:s", time());
		$versement_query = 'insert into '. TABLE_CUSTOMERS_ARGENT .' (customers_id, somme, raison, raison_client, date_versement, user_id)
							values (\''. $_REQUEST['id_client'] .'\',
									\''. $_REQUEST['versement_somme'] .'\',
									\''. addslashes($_REQUEST['versement_raison']) .'\',
									\''. addslashes($_REQUEST['versement_raison_client']) .'\',
									\''. $date_now .'\',
									\''. $login_id .'\')';
		tep_db_query($versement_query);
		
		$res_argent_client = tep_db_query('select customers_argent_virtuel from '. TABLE_CUSTOMERS .' where customers_id=\''. $_REQUEST['id_client'] .'\'');
		$row_argent_client = tep_db_fetch_array($res_argent_client);
		
		$customers_argent_virtuel = $row_argent_client['customers_argent_virtuel']+$_REQUEST['versement_somme'];
		
		$update_client = tep_db_query('update '. TABLE_CUSTOMERS .' set customers_argent_virtuel=\''. $customers_argent_virtuel .'\' where customers_id=\''. $_REQUEST['id_client'] .'\'');
		
		$email_infos_query = tep_db_query('SELECT customers_email_address, customers_firstname, customers_lastname FROM '. TABLE_CUSTOMERS .' WHERE customers_id = \''. $_REQUEST['id_client'] .'\'');
		$email_infos = tep_db_fetch_array($email_infos_query);
		
		$versement_ttc = roundNumber($_REQUEST['versement_somme']*1.2, 2);
		$somme_ttc = roundNumber($customers_argent_virtuel*1.2, 2);
		
		// On construit le mail pour le client et on l'envoie
		$message_mail = 'Bonjour <span style="font-weight: bold; color: rgb(170, 180, 29);">'.$email_infos['customers_firstname']." ".$email_infos['customers_lastname'].'</span>,<br /><br />Votre Porte-monnaie Virtuel a �t� cr�dit� de <span style="font-weight:bold;">' . roundNumber($_REQUEST["versement_somme"], 2) . ' &euro;</span> HT ( <span style="font-weight:bold;color:#fe5700;">' . $versement_ttc . ' &euro; TTC</span> ).<br />';
		
		if(roundNumber($customers_argent_virtuel, 2)>0) {
			
		$message_mail .= 'Vous disposez � pr&eacute;sent de <span style="font-weight:bold;">' . roundNumber($customers_argent_virtuel, 2) .' &euro;</span> HT ( <span style="font-weight:bold;color:#fe5700;">'. $somme_ttc .' &euro; TTC</span> ) &agrave; d&eacute;penser sur notre site.';
		}
		else {
			
			$message_mail .= 'Vous ne disposez d�sormais plus d\'aucun cr�dit dans votre Porte-monnaie virtuel';
		}
		
		$message_mail .= '<br /><br /><strong>Comment utiliser l\'argent de mon porte-monnaie virtuel ?</strong><br />'. EXPLICATIONS_PMV .'<br /><br />'. SIGNATURE_MAIL;
		
		mail_client_commande($email_infos['customers_email_address'], SUJET_INFORMATION_PMV, $message_mail);

		break;
	  case 'deleteconfirm':
        $customers_id = tep_db_prepare_input($HTTP_GET_VARS['cID']);
        if (isset($HTTP_POST_VARS['delete_reviews']) && ($HTTP_POST_VARS['delete_reviews'] == 'on')) {
          	$reviews_query = tep_db_query("select reviews_id from " . TABLE_REVIEWS . " where customers_id = '" . (int)$customers_id . "'");
          	while ($reviews = tep_db_fetch_array($reviews_query)) {
            	tep_db_query("delete from " . TABLE_REVIEWS_DESCRIPTION . " where reviews_id = '" . (int)$reviews['reviews_id'] . "'");
          	}
          	tep_db_query("delete from " . TABLE_REVIEWS . " where customers_id = '" . (int)$customers_id . "'");
        } 
		else {
          	tep_db_query("update " . TABLE_REVIEWS . " set customers_id = null where customers_id = '" . (int)$customers_id . "'");
        }

        tep_db_query("delete from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customers_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customers_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customers_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customers_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customers_id . "'");
        tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where customer_id = '" . (int)$customers_id . "'");

        tep_redirect(tep_href_link(FILENAME_CLIENT_LIST, tep_get_all_get_params(array('cID', 'action'))));
        break;
      default:
	  	//modif bruno 1/9/7
        $customers_query = tep_db_query("select c.customers_id, c.customers_gender, c.customers_firstname, c.customers_lastname, c.customers_dob, c.customers_email_address, c.customers_company, a.entry_company, a.entry_street_address, a.entry_suburb, a.entry_postcode, a.entry_city, a.entry_state, a.entry_zone_id, a.entry_country_id, c.customers_telephone, c.customers_newsletter, c.customers_news_sms, c.customers_type, c.customers_comment, c.customers_default_address_id from " . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " a on c.customers_default_address_id = a.address_book_id where a.customers_id = c.customers_id and c.customers_id = '" . (int)$HTTP_GET_VARS['cID'] . "'");
		$customers = tep_db_fetch_array($customers_query);
        $cInfo = new objectInfo($customers);
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/lightbox.css">
<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css"></link>
<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/javascript/scripts/prototype.js"></script>
<!--2020-02-13 -- <script language="javascript" src="includes/javascript/scripts/scriptaculous.js"></script>-->
<script language="javascript" src="includes/javascript/scripts/lightbox.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/function.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
	<tr>
		<td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
			<form name="myform2" action="<?php echo $PHP_SELF; ?>" method="get">
				<td>
					<table border="0" width="100%" cellspacing="0" cellpadding="0" class="smallText">
						<tr align="center" style="background-color: #d8d8eb;" height="25">
							<td>
								ID : <?php echo tep_draw_input_field('id_client', '' , 'size="6"'); ?>
							</td>
							<!--<td>
								<?php 
								$admin_array=array();
								$admin_array[] = array('id' => "", 'text' => "Tous");
								$admin_array[] = array('id' => "00", 'text' => "Sans Commercial");
								$res_admin=tep_db_query("select admin_lastname, admin_firstname, admin_id from admin  where admin_groups_id<>4 order by admin_lastname, admin_firstname");
								while($row_admin=tep_db_fetch_array($res_admin)){
									$admin_array[]=array('id' => $row_admin["admin_id"], 'text' => $row_admin["admin_lastname"]." ".$row_admin["admin_firstname"]);
								}
								echo "Clients de :&nbsp;".tep_draw_pull_down_menu('customers_admin', $admin_array, '', 'onChange="this.form.submit();"');
								?>
							</td>-->
							<td>
								Nom : <?php echo tep_draw_input_field('nom', '', 'size="15"'); ?>
							</td>
							<td>
								Pr�nom : <?php echo tep_draw_input_field('prenom', '', 'size="15"'); ?>
							</td>
							<td>
								Soci�t� : <?php echo tep_draw_input_field('nom_societe', '', 'size="15"'); ?>
							</td>
							<td>
								Email : <?php echo tep_draw_input_field('mail'); ?>
							</td>
							<td>
								T�l�phone Mobile : <?php echo tep_draw_input_field('tel_mobile', '', 'size="15"'); // 'size="20" id="focusorders"'?>
							</td>
							<td>
								D�partement : <?php echo tep_draw_input_field('departement', '', 'size="3"'); ?>
							</td>
							<td>
								<?php 	
								$nb_resultats_array = array();
								$nb_resultats_array[] = array('id' => 20, 'text' => "20");
								$nb_resultats_array[] = array('id' => 50, 'text' => "50");
								$nb_resultats_array[] = array('id' => 100, 'text' => "100");
								$nb_resultats_array[] = array('id' => 150, 'text' => "150");
								$nb_resultats_array[] = array('id' => 200, 'text' => "200");
								$nb_resultats_array[] = array('id' => 300, 'text' => "300");
								$nb_resultats_array[] = array('id' => 400, 'text' => "400");
								$nb_resultats_array[] = array('id' => 500, 'text' => "500");
								echo "Nb Clients :&nbsp;".tep_draw_pull_down_menu('nb_resultats', $nb_resultats_array, '', 'onChange="this.form.submit();"'); 
								?>
							</td>
						</tr>
					</table>
					<table border="0" width="100%" cellspacing="0" cellpadding="0" class="smallText">
						<tr align="center" style="background-color: #e1e1f5;" height="35">
							<td colspan="3">        
								<?php	
								echo "Du : ".tep_draw_input_field('date_deb', '', 'size="10"'); // 'size="10" id="focusorders"' ?>
								<a href="#" onClick="displayCalendar(myform2.date_deb,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
								<?php 
								echo "Au&nbsp;".tep_draw_input_field('date_fin', '', 'size="10"'); // 'size="10" id="focusorders"' ?>
								<a href="#" onClick="displayCalendar(myform2.date_fin,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
							</td>
							<td>
								<?php	
								$type_client_array = array();
								$type_query = tep_db_query("select type_id, type_name from " . TABLE_CUSTOMERS_TYPE . " order by type_name  ");
								$type_client_array[] = array('id' => "", 'text' => "Tous");
								while ($type_values = tep_db_fetch_array($type_query)) {
								  $type_client_array[] = array('id' => $type_values['type_id'], 'text' => $type_values['type_name']);
								}
								echo "Type de client :&nbsp;".tep_draw_pull_down_menu('type_client', $type_client_array, '', 'onChange="this.form.submit();"'); ?>					
							</td>
							<td>
								<?php	
								$groupe_client_array = array();
								$groupe_client_array[] = array('id' => "", 'text' => "Tous");
								$groupe_client_array[] = array('id' => "Militaire", 'text' => "PART/PRO - Militaire");
								$groupe_client_array[] = array('id' => "Gendarme", 'text' => "PART/PRO - Gendarme");
								$groupe_client_array[] = array('id' => "Policier (PN, CRS, Douanier)", 'text' => "PART/PRO - Policier (PN, CRS, Douanier)");
								$groupe_client_array[] = array('id' => "Policier (PM-ASVP)", 'text' => "PART/PRO - Policier (PM-ASVP)");
								$groupe_client_array[] = array('id' => "Agent P�nitentiaire", 'text' => "PART/PRO - Agent P�nitentiaire");
								$groupe_client_array[] = array('id' => "Agent de S�curit�", 'text' => "PART/PRO - Agent de S�curit�");
								$groupe_client_array[] = array('id' => "Sapeur Pompier", 'text' => "PART/PRO - Sapeur Pompier");
								$groupe_client_array[] = array('id' => "Autre profession", 'text' => "PART/PRO - Autre profession");
								$groupe_client_array[] = array('id' => "Mairie", 'text' => "ADM - Mairie");
								$groupe_client_array[] = array('id' => "Communaut� de Communes", 'text' => "ADM - Communaut� de Communes");
								$groupe_client_array[] = array('id' => "Communaut� d�Agglom�ration", 'text' => "ADM - Communaut� d�Agglom�ration");
								$groupe_client_array[] = array('id' => "Conseil G�n�ral", 'text' => "ADM - Conseil G�n�ral");
								$groupe_client_array[] = array('id' => "Conseil R�gional", 'text' => "ADM - Conseil R�gional");
								$groupe_client_array[] = array('id' => "Minist�re", 'text' => "ADM - Minist�re");
								$groupe_client_array[] = array('id' => "Pr�fecture / Sous-Pr�fecture", 'text' => "ADM - Pr�fecture / Sous-Pr�fecture");
								$groupe_client_array[] = array('id' => "Association", 'text' => "ADM - Association");
								$groupe_client_array[] = array('id' => "Autre administration", 'text' => "ADM - Autre administration");					
								echo "Groupe de client :&nbsp;".tep_draw_pull_down_menu('groupe_client', $groupe_client_array, '', 'onChange="this.form.submit();"');
								?>
							</td>
							<td><?php	$nbr_commandes_array = array();
										$nbr_commandes_array[] = array('id' => "", 'text' => "Tout type");
										$nbr_commandes_array[] = array('id' => 1, 'text' => "Sans commande");
										$nbr_commandes_array[] = array('id' => 2, 'text' => "Avec commande");
										$nbr_commandes_array[] = array('id' => 3, 'text' => "Avec 2 commandes ou +");
										echo "Nombre de commandes :&nbsp;".tep_draw_pull_down_menu('nbr_commandes', $nbr_commandes_array, '', 'onChange="this.form.submit();"');?>
							</td>
							<td><?php	$argent_client_array = array();
										$argent_client_array[] = array('id' => "", 'text' => "Tous");
										$argent_client_array[] = array('id' => "avec", 'text' => "Avec argent");
										$argent_client_array[] = array('id' => "sans", 'text' => "Sans argent");
										echo "Argent virtuel :&nbsp;".tep_draw_pull_down_menu('argent_virtuel', $argent_client_array, '', 'onChange="this.form.submit();"');?>
							</td>							
							<td><input type=submit value="Go"></td>
						</tr>
					</table>
				</td>
			</tr>
			</form>
			<tr>
				<td>
				<form name="myform" action="" method="post">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top">
              
								<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
									<thead>
										<th style="text-align:center;">ID</th>
										<th style="text-align:center;">Type</th>
										<th>&nbsp;</th>
										<th>Nom</th>
										<th>Pr�nom</th>
										<th>Soci�t�</th>
										<th>Adresse Email</th>
										<th style="text-align:center;">Date de<br />Naissance</th>
										<th style="text-align:center;">Date<br />Cr�ation</th>
										<th>
										<a href="<?php echo tep_href_link(FILENAME_CLIENT_LIST, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=nb_commande&type_ordre=asc");?>">
										<?php echo tep_image(DIR_WS_IMAGES . 'icon_up.gif', 'ascendant');?></a>
										<strong>Nombre<br /> de cmd</strong>
										<a href="<?php echo tep_href_link(FILENAME_CLIENT_LIST, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=nb_commande&type_ordre=desc");?>">
										<?php echo tep_image(DIR_WS_IMAGES . 'icon_down.gif', 'descendant');?></a>
										</th>
										<th>
										<a href="<?php echo tep_href_link(FILENAME_CLIENT_LIST, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=nb_articles&type_ordre=asc");?>">
										<?php echo tep_image(DIR_WS_IMAGES . 'icon_up.gif', 'ascendant');?></a>
										<strong>Nombre<br /> d'article</strong>
										<a href="<?php echo tep_href_link(FILENAME_CLIENT_LIST, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=nb_articles&type_ordre=desc");?>">
										<?php echo tep_image(DIR_WS_IMAGES . 'icon_down.gif', 'descendant');?></a>
										</td>
										<!--<td class="dataTableHeadingContent" align="center">Nombre de t�l</td>-->
										<th>Chiffre<br />d'Affaire</th>
										<th>Marge</th>
										<th>
										<a href="<?php echo tep_href_link(FILENAME_CLIENT_LIST, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=customers_argent_virtuel&type_ordre=asc");?>">
										<?php echo tep_image(DIR_WS_IMAGES . 'icon_up.gif', 'ascendant');?></a>
										<strong>Argent virtuel</strong>
										<a href="<?php echo tep_href_link(FILENAME_CLIENT_LIST, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=customers_argent_virtuel&type_ordre=desc");?>">
										<?php echo tep_image(DIR_WS_IMAGES . 'icon_down.gif', 'descendant');?></a>
										</th>				
										<th class="{sorter:false}" style="text-align:center;width:340px;">Actions</th>
									</thead>
									<tbody>
              
<?php
    if (isset($_REQUEST['id_client']) && tep_not_null($_REQUEST['id_client'])) {
      	$id_client = tep_db_input(tep_db_prepare_input($_REQUEST['id_client']));
      	$where = " and c.customers_id ='" . $id_client . "'";
    }
    if (isset($HTTP_GET_VARS['nom_societe']) && tep_not_null($HTTP_GET_VARS['nom_societe'])) {
      	$nom_societe = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['nom_societe']));
      	$where = " and c.customers_company like '%" . $nom_societe . "%'"; // $where = " and a.entry_company like '%" . $nom_societe . "%'";
    }
    if (isset($HTTP_GET_VARS['departement']) && tep_not_null($HTTP_GET_VARS['departement'])) {
      	$departement = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['departement']));
      	$where = " and a.entry_postcode like '" . $departement . "%'";
    }
	if(isset($HTTP_GET_VARS['date_deb']) && !empty($HTTP_GET_VARS['date_deb'])) {
		$date_deb = tep_db_prepare_input(date_fr_to_bdd($HTTP_GET_VARS['date_deb']));
      	$where .= " and customers_info_date_account_created > '".$date_deb ."'";
	}
	if(isset($HTTP_GET_VARS['date_fin']) && !empty($HTTP_GET_VARS['date_fin'])) {
		$date_fin = tep_db_prepare_input(date_fr_to_bdd($HTTP_GET_VARS['date_fin']));
      	$where .= " and customers_info_date_account_created < '".$date_fin." 23:59:59'";
	}
    if (isset($HTTP_GET_VARS['nom']) && tep_not_null($HTTP_GET_VARS['nom'])) {
      	$nom_client = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['nom']));
      	$where = " and c.customers_lastname like '%" . $nom_client . "%'";
    }
    if (isset($HTTP_GET_VARS['prenom']) && tep_not_null($HTTP_GET_VARS['prenom'])) {
      	$prenom_client = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['prenom']));
      	$where .= " and c.customers_firstname like '%" . $prenom_client . "%'";
    }
	if (isset($HTTP_GET_VARS['mail']) && tep_not_null($HTTP_GET_VARS['mail'])) {
      	$mail_client = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['mail']));
      	$where .= " and c.customers_email_address like '%" . $mail_client . "%'";
    }
	if (isset($HTTP_GET_VARS['customers_admin']) && tep_not_null($HTTP_GET_VARS['customers_admin'])) {
      	$customers_admin = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['customers_admin']));
		if ($customers_admin == 'aucun') {
			$customers_admin = 0;
		}
      	$where .= " and c.customers_admin='" . $customers_admin . "'";
    }
	if (isset($HTTP_GET_VARS['type_client']) && tep_not_null($HTTP_GET_VARS['type_client'])) {
      	$type_client = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['type_client']));
      	$where .= " and c.customers_type='" . $type_client . "'";
    }
	if (isset($HTTP_GET_VARS['groupe_client']) && tep_not_null($HTTP_GET_VARS['groupe_client'])) {
      	$groupe_client = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['groupe_client']));
      	$where .= " and c.customers_company_type='" . $groupe_client . "'";
    }
	if (isset($HTTP_GET_VARS['tel_mobile']) && tep_not_null($HTTP_GET_VARS['tel_mobile'])) {
      	$tel_mobile = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['tel_mobile']));
      	$where .= " and c.customers_telephone like '%" . $tel_mobile . "%'";
    }
	if (isset($HTTP_GET_VARS['argent_virtuel']) && tep_not_null($HTTP_GET_VARS['argent_virtuel'])) {
      	if($HTTP_GET_VARS['argent_virtuel']=="avec"){
	  		$where .= " and c.customers_argent_virtuel > 0";
    	}
      	elseif($HTTP_GET_VARS['argent_virtuel']=="sans"){
	  		$where .= " and c.customers_argent_virtuel = ''";
    	}
    }  
	if (isset($HTTP_GET_VARS['nbr_commandes']) && tep_not_null($HTTP_GET_VARS['nbr_commandes'])) {
      	$nbr_commandes = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['nbr_commandes']));
      	if($nbr_commandes==1){//sans commande
			$where .= " and c.nb_commande=0";
		}
		if($nbr_commandes==2){//avec commande
			$where .= " and c.nb_commande>0";
		}
		if($nbr_commandes==3){//avec 2 commandes ou plus
			$where .= " and c.nb_commande>1";
		}
    }
	if (isset($HTTP_GET_VARS['nb_resultats']) && tep_not_null($HTTP_GET_VARS['nb_resultats'])) {
		$nb_client_par_page = tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['nb_resultats']));
      	$Nombre_de_ligne_par_page=$nb_client_par_page;
	}
	
    $ordre=" order by customers_info_date_account_created desc";
	if (isset($HTTP_GET_VARS['ordre']) && tep_not_null($HTTP_GET_VARS['ordre'])) {
      	$ordre=" order by ".tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['ordre']))." ".tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['type_ordre']));
	}
	
	$customers_query_raw = "select c.customers_type, c.nb_commande, c.total_commande, c.total_marge, c.nb_articles, c.customers_nb_tel, c.customers_company, a.entry_company, customers_gender, customers_dob as date_of_birthday, customers_argent_virtuel, customers_info_date_account_created as date_account_created, customers_info_date_account_last_modified as date_account_last_modified, customers_info_date_of_last_logon as date_last_logon, customers_info_number_of_logons as number_of_logons, customers_newsletter, customers_news_sms, c.customers_id, c.customers_lastname, c.customers_firstname, c.customers_email_address, a.entry_country_id from ((" . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " a on c.customers_id = a.customers_id and c.customers_default_address_id = a.address_book_id) left join " . TABLE_CUSTOMERS_INFO . " i on  i.customers_info_id = c.customers_id ) ";
	$customers_query_raw =$customers_query_raw." where 1 ".$where.$ordre;
	
	$customers_split = new splitPageResults($_REQUEST['page'], $Nombre_de_ligne_par_page, $customers_query_raw, $customers_query_numrows);
    $customers_query = tep_db_query($customers_query_raw);
	
    while ($customers = tep_db_fetch_array($customers_query)) {?>
		<tr class="dataTableRow" >
			<td class="dataTableContent" style="text-align:center;"><a href="<?php echo tep_href_link(FILENAME_CLIENT_EDIT, tep_get_all_get_params(array('cID'))."cID=".$customers['customers_id'])."&action=edit";?>"><?php echo strtoupper($customers['customers_id']);?></a></td> 
            <td class="dataTableContent" align="center">
                <?php // Code pour mettre des icones en fontion du TYPE de client
                    if($customers['customers_type'] == '1'){
                        echo '<img src="images/icons/icone_particulier.gif" alt="Client Particulier" title="Client Particulier">';
                    }
                    else if($customers['customers_type'] == '2'){
                        echo '<img src="images/icons/icone_professionnel.gif" alt="Client Professionnel" title="Client Professionnel">';
                    }
                    else if($customers['customers_type'] == '3'){
                        echo '<img src="images/icons/icone_revendeur.gif" alt="Client Revendeur" title="Client Revendeur">';
                    }
					else if($customers['customers_type'] == '4'){
						echo '<img src="images/icons/icone_administration.gif" alt="Client Administration" title="Client Administration">';
					}?>
            </td>
			<!-- Affichage de l'icone pour indiquer si le client est un Homme ou une Femme -->
			<td align="center">
			<?php 
				if($customers['customers_gender'] == 'm'){
					echo "<img src='images/icons/homme.png' title='C est un Homme'>";
				}
				else{
					echo "<img src='images/icons/femme.png' title='C est une Femme'>";
				}?>
			</td> 
			<td class="dataTableContent"><a href="<?php echo tep_href_link(FILENAME_CLIENT_EDIT, tep_get_all_get_params(array('cID','page'))."cID=".$customers['customers_id'])."&action=edit";?>"><?php echo $customers['customers_lastname'];?></a></td> 
			<td class="dataTableContent"><a href="<?php echo tep_href_link(FILENAME_CLIENT_EDIT, tep_get_all_get_params(array('cID','page'))."cID=".$customers['customers_id'])."&action=edit";?>"><?php echo $customers['customers_firstname'];?></a></td>
			<td class="dataTableContent"><?php echo $customers['customers_company']; // entry_company ?></td>
            <td class="dataTableContent"><?php echo $customers['customers_email_address'];?></td> 
			<td class="dataTableContent" align="center"><?php echo tep_date_short($customers['date_of_birthday']);?></td>  
			<td class="dataTableContent" align="center"><?php echo tep_date_short($customers['date_account_created']);?></td>  

			<td class="dataTableContent" align="center"><a href="<?php echo tep_href_link(FILENAME_CMD_LIST, 'cID='.$customers['customers_id']);?>"><?php echo vider($customers['nb_commande']);?></a></td>
			<td class="dataTableContent" align="center"><?php echo vider($customers['nb_articles']);?></td>
			<!--<td class="dataTableContent" align="center"><?php echo vider($customers['customers_nb_tel']);?></td>-->
            <td class="dataTableContent" align="center"><?php echo vider($customers['total_commande']);?></td>
			<td class="dataTableContent" align="center"><?php echo vider($customers['total_marge']);?></td>
            <td class="dataTableContent" align="center"><?php echo vider($customers['customers_argent_virtuel']);?></td>
			<td class="dataTableContent" align="right">
			<?php
			$res_basket=tep_db_query("SELECT sum(customers_basket_quantity) as nb_articles, bask.* FROM ".TABLE_CUSTOMERS_BASKET." bask where customers_id='".$customers['customers_id']."' group by customers_id");
			if(tep_db_num_rows($res_basket)>0){
				$row_basket=tep_db_fetch_array($res_basket);
			?>
            <a href="/<?php echo DIR_WS_SITE;?>/compte/connexion.php?panier_admin=true&connexion=ok&mail=<?php echo $customers['customers_email_address'];?>&mdp=azertyuiopqsdfghjklm" target="_blank"><?php echo tep_image(DIR_WS_IMAGES . 'icons/commande_01.png', $row_basket["nb_articles"]." article(s)");?></a>
            |
			<?php }
			echo '<a href="' . tep_href_link(FILENAME_CLIENT_EDIT, 'cID=' . $customers['customers_id']) . '&action=edit">' . tep_image(DIR_WS_IMAGES . 'icons/editer.png', IMAGE_ICON_INFO) . '</a>';
			// modification de thierry pour eviter d'avoir les parametres dans l'url de la page d'avant
			// echo '<a href="' . tep_href_link(FILENAME_CLIENT_EDIT, tep_get_all_get_params(array('cID','page')) . 'cID=' . $customers['customers_id']) . '&action=edit">' . tep_image(DIR_WS_IMAGES . 'icons/editer.png', IMAGE_ICON_INFO) . '</a>';
			?> 
			| <?php echo '<a href="' . tep_href_link(FILENAME_CMD_LIST, 'cID=' . $customers['customers_id']) . '">' . tep_image_button('commande_02.png', IMAGE_ORDERS) . '</a>'; ?> 
			| <?php echo '<a href="' . tep_href_link(FILENAME_CLIENT_LIST, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers['customers_id'] . '&action=deleteconfirm') . '" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer  ' . strtoupper($customers['customers_lastname']) . '  DEFINITIVEMENT ?\'))    
			{return true;}else {return false;}">' . tep_image_button('supprimer.png', IMAGE_DELETE) . '</a>'; ?>	
			| <?php 
			if($customers['customers_newsletter']) $ncustomers_newsletter = "<img src=\"images/icons/newsletter_oui.png\">"; 
			else $ncustomers_newsletter = "<img src=\"images/icons/newsletter_non.png\">";				
			echo $ncustomers_newsletter ;?>
            | 
			<?php // ICONE POUR Ajouter de l'argent 
            echo '<a href="'.tep_href_link(FILENAME_CLIENT_VERSEMENT, 'id_client='.$customers['customers_id']).'&action=make_versement" class="lbOn">'.tep_image(DIR_WS_IMAGES.'/icons/crediter.png', $customers['customers_argent_virtuel']." � sur le compte").'</a>';?>
            |
            <a href="nouvelle_commandeP1.php?cID=<?php echo $customers['customers_id']; ?>"><input type="button" value="CMD" /></a>
            </td>
		  </tr>
<?php }//fin while clients?>
			  <tr>
            	<td colspan="17">
                  	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
          			<tr>
                        <td class="smallText" width="33%"><?php echo $customers_split->display_count($customers_query_numrows, $Nombre_de_ligne_par_page, $_REQUEST['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></td>
                        <td class="smallText" width="33%" align="center"><?php echo $customers_split->display_links($customers_query_numrows, $Nombre_de_ligne_par_page, MAX_DISPLAY_PAGE_LINKS, $_REQUEST['page'], tep_get_all_get_params(array('page', 'info', 'x', 'y', 'cID'))); ?></td>
                        <td class="smallText" width="33%">&nbsp;</td>
                        
                  	</tr>
                	</table>
                </td>
              </tr>
              </tbody>
            </table></td>
    </form>
	</table>
	</td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>