<?php
/* 2009/01/13 LL last modif by Ludo => refonte complete de la page order, pour la couper en 2*/
  require('includes/application_top.php');
  require('includes/functions/date.php');
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();
  
  $Nombre_de_ligne_par_page="110";
  
  $orders_statuses = array();
  $orders_status_array = array();
  $orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' ORDER BY orders_status_name ");
  while ($orders_status = tep_db_fetch_array($orders_status_query)) {
	  
    $orders_statuses[] = array('id' => $orders_status['orders_status_id'], 'text' => $orders_status['orders_status_name']);
    $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
  }
  
  
  include(DIR_WS_CLASSES . 'order.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/function.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<div id="popupcalendar" class="text"></div>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" height="25" style="background-color: #d8d8eb;">
            <tr>
            <td class="smallText" align="left" width="50%">
              <?php echo tep_draw_form('orders', FILENAME_CMD_ATTENTE, '', 'get'); 
                    echo '<b>&nbsp;&nbsp;ID Article :&nbsp;&nbsp;</b>'. tep_draw_input_field('pID', '', 'size="12" id="focusorders"') ?>
              </form><br /><br />
              <?php echo tep_draw_form('orders', FILENAME_CMD_ATTENTE, '', 'get'); 
                    echo '<b>&nbsp;&nbsp;R�f�rence :&nbsp;&nbsp;</b>'. tep_draw_input_field('ref', '', 'size="12" id="focusorders"') ?>
              </form>
              <script type="text/javascript"> document.orders.focusorders.focus(); </script>
            </td>
            <td class="smallText" align="right" width="50%">
            <?php 
            echo tep_draw_form('Filtres', FILENAME_CMD_ATTENTE, '', 'get'); 
            //echo '<b>Statut :&nbsp;</b>'.tep_draw_pull_down_menu('status', array_merge(array(array('id' => '', 'text' => 'Toutes les commandes')), $orders_statuses), '', 'onChange="this.form.submit();"').'&nbsp;&nbsp;';
            ?>
            </td>
            </tr>
        </table>
        </td>
	  </tr>
	  <tr>
		<td>
          <table border="0" width="100%" cellspacing="0" cellpadding="0">
          	<tr><td>&nbsp;</td></tr>
            <tr>
          		<td valign="top">
					<table border="0" width="100%" cellspacing="0" cellpadding="2">
              		<?php echo tep_draw_form('batch_orders', 'OSC_Expeditor_process.php', '', 'post', 'target="_blank"'); ?>
                    <tr class="dataTableHeadingRow">
                      <td class="dataTableHeadingContent" align="center">Type</td>
                      <td class="dataTableHeadingContent" align="center">Quantit&eacute;</td>
                      <td class="dataTableHeadingContent" align="center">Reste � livrer</td>
                      <td class="dataTableHeadingContent" align="center">ID</td>
                      <td class="dataTableHeadingContent" align="left">Client</td>
                      <td class="dataTableHeadingContent" align="left">
                      <a href="<?php echo tep_href_link(FILENAME_CMD_ATTENTE, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=customers_company&type_ordre=asc");?>">
                      <?php echo tep_image(DIR_WS_IMAGES . 'icon_up.gif', 'ascendant');?></a>
                      <strong>Soci&eacute;t&eacute;</strong>
                      <a href="<?php echo tep_href_link(FILENAME_CMD_ATTENTE, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=customers_company&type_ordre=desc");?>">
                      <?php echo tep_image(DIR_WS_IMAGES . 'icon_down.gif', 'descendant');?></a>
                      </td>
                      <td class="dataTableHeadingContent" align="left">Total</td>
                      <td class="dataTableHeadingContent" align="left">
                      <a href="<?php echo tep_href_link(FILENAME_CMD_ATTENTE, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=marge&type_ordre=asc");?>">
                      <?php echo tep_image(DIR_WS_IMAGES . 'icon_up.gif', 'ascendant');?></a>
                      <strong>Marge</strong>
                      <a href="<?php echo tep_href_link(FILENAME_CMD_ATTENTE, tep_get_all_get_params(array('ordre','type_ordre'))."&ordre=marge&type_ordre=desc");?>">
                      <?php echo tep_image(DIR_WS_IMAGES . 'icon_down.gif', 'descendant');?></a>
                      </td>
                      <td class="dataTableHeadingContent" align="left">Date d'achat</td>
                      <td class="dataTableHeadingContent" align="center">Mode Paiement</td>
                      <td class="dataTableHeadingContent" align="Left">Statut</td>
                    </tr>
<?php
	
	if( (isset($_GET['pID']) && !empty($_GET['pID'])) || (isset($_GET['ref']) && !empty($_GET['ref'])) ) {
		
		$orders_query_raw="select distinct(o.orders_id), o.customers_id, o.delivery_country, o.origine, o.customers_name, o.customers_company, o.customers_id, o.customer_ip_country, o.customer_ip, o.payment_method, o.total, o.marge, o.remise, o.date_purchased, o.last_modified, o.currency, o.currency_value, o.marge, s.orders_status_name, o.orders_numero_facture 
											 from " . TABLE_ORDERS . " o, ". TABLE_ORDERS_PRODUCTS ." op, " . TABLE_ORDERS_STATUS . " s, " . TABLE_CUSTOMERS . " c 
											 where o.orders_status=s.orders_status_id 
											 and o.customers_id=c.customers_id 
											 and o.orders_id=op.orders_id
											 and products_quantity>products_quantity_sent
											 and s.language_id = '" . (int)$languages_id . "'";
		if(isset($_GET['pID']) && !empty($_GET['pID'])) {
			
			$pID = tep_db_prepare_input($_GET['pID']);
			$orders_query_raw.=" and op.products_id = '". (int)$pID ."'";
		}
		
		if(isset($_GET['ref']) && !empty($_GET['ref'])) {
			
			$ref = tep_db_prepare_input($_GET['ref']);
			$orders_query_raw.=" and op.products_model = '". $ref ."'";
		}
		
		$ordre=" order by o.orders_id DESC";
		$orders_query_raw .= $ordre;
		
		$orders_split = new splitPageResults($HTTP_GET_VARS['page'], $Nombre_de_ligne_par_page, $orders_query_raw, $orders_query_numrows);
		$orders_query = tep_db_query($orders_query_raw);
		
		while ($orders = tep_db_fetch_array($orders_query)) {
			
			if ((!isset($HTTP_GET_VARS['oID']) || (isset($HTTP_GET_VARS['oID']) && ($HTTP_GET_VARS['oID'] == $orders['orders_id']))) && !isset($oInfo)) {
				$oInfo = new objectInfo($orders);
			}
			if (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)) {
				echo '  <tr id="defaultSelected" class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" >' . "\n";
				
			} 
			else {
				echo '<tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
				
			}
			// Code pour mettre des icones en fontion du TYPE de client
			$customers_type_query = tep_db_query("select customers_id,customers_type from " . TABLE_CUSTOMERS . " where customers_id = '" . $orders["customers_id"] . "'");
			$row_client = mysql_fetch_array($customers_type_query);												
			//if(!isset($_REQUEST["type_client"]) || (isset($_REQUEST["type_client"]) && $_REQUEST["type_client"]== $row_client['customers_type']) ){			
			?>
						  
						  <td class="dataTableContent" align="center">
							<?php // Code pour mettre des icones en fontion du TYPE de client
								if($row_client['customers_type'] == '1'){
									echo '<img src="images/icons/icone_particulier.gif" alt="Client Particulier" title="Client Particulier">';
								}
								else if($row_client['customers_type'] == '2'){
									echo '<img src="images/icons/icone_professionnel.gif" alt="Client Professionnel" title="Client Professionnel">';
								}
								else if($row_client['customers_type'] == '3'){
									echo '<img src="images/icons/icone_revendeur.gif" alt="Client Revendeur" title="Client Revendeur">';
								}
								else if($row_client['customers_type'] == '4'){
									echo '<img src="images/icons/icone_administration.gif" alt="Client Administration" title="Client Administration">';
								}?>
						  </td>
						  <!--qte -->
						  <td class="dataTableContent" align="center">
						  <?php
								$qte_query = "SELECT SUM(products_quantity) as qte_totale, SUM(products_quantity_sent) as qte_totale_livree FROM " . TABLE_ORDERS_PRODUCTS . " WHERE orders_id = '" . $orders['orders_id'] . "'";
								
								if(isset($_GET['pID']) && !empty($_GET['pID'])) {
									
									$qte_query .= " and products_id = '". (int)$pID ."'";
								}
								
								if(isset($_GET['ref']) && !empty($_GET['ref'])) {
									
									$qte_query .= " and products_model = '". $ref ."'";
								}
								
								$qte_query = tep_db_query($qte_query);
								$qte = tep_db_fetch_array($qte_query);
								
								echo $qte['qte_totale'];
						  ?>
						  </td>
						  <!--relicats -->
						  <td class="dataTableContent" align="center">
						  <?php 
								
								echo '<strong style="color: red;">'. ($qte['qte_totale'] - $qte['qte_totale_livree']) .'</strong>';
						  ?>
						  </td>
						  <!--ID Commande -->
						  <td <?php echo $link; ?> class="dataTableContent" align="center">
							<?php echo '<a href="'.tep_href_link(FILENAME_CMD_EDIT, 'oID='.$orders['orders_id'].'&action=edit') .'" target="_blank">'.$orders['orders_id'].'</a>&nbsp;'; ?>
						  </td>
						  <td <?php echo $link; ?> class="dataTableContent"><?php echo $orders['customers_name']; ?></td>
						  <td class="dataTableContent">
							<?php echo '<a href="'.tep_href_link(FILENAME_CMD_LIST, 'cID='.$orders['customers_id']) .'" target="_blank" title="Voir Toutes les commandes du client">'.$orders['customers_company'].'</a>'; ?>
						  </td>
						  <td <?php echo $link; ?> class="dataTableContent" align="left"><?php echo $orders['total']; ?>&euro;&nbsp;&nbsp; 
							<?php // Code qui affiche le montant du coupon d'une commande.
							if($orders['remise']!=0.00){
								echo '<font color="RED">' . $orders['remise'] .'</font>';
							}?>
						  </td>
						  
						  <?php
						  $marge_couleur = "style=\"color: #000; font-weight: normal;\"";
						  if ($orders["marge"] < $marge_orange)
							  $marge_couleur = "style=\"color: #F60; font-weight: bold;\"";
						  if ($orders["marge"] < $marge_rouge)
							  $marge_couleur = "style=\"color: #F00; font-weight: bold;\"";
						  ?>
						  <td class="dataTableContent" align="left" <?php echo $marge_couleur; ?>><?php echo $orders["marge"];?> &euro;</td>
						  <td <?php echo $link; ?> class="dataTableContent" align="left"><?php echo tep_datetime_short($orders['date_purchased']); ?></td>
						  <td <?php echo $link; ?> class="dataTableContent" align="center">				
							<?php // Code pour mettre des icones en fontion du mode de paiement 
								if(substr_count($orders['payment_method'],"Carte")>0){
									echo "<img src='images/icons/carte_bleue.gif' title='Paiement CB'>";
								}
								else if(substr_count(strtoupper($orders['payment_method']),strtoupper("Ch�que"))>0){
									echo "<img src='images/icons/cheque.gif' title='Paiement Ch�que'>";
								}
								else if(substr_count($orders['payment_method'],"Virement")>0){
									echo "<img src='images/icons/virement.gif' title='Paiement Virement'>";
								}				
								else if(substr_count($orders['payment_method'],"Esp�ce")>0){
									echo "<img src='images/icons/espece.gif' title='Paiement Esp�ce'>";
								}
								else if(substr_count($orders['payment_method'],"Mandat Cash")>0){
									echo "<img src='images/icons/mandat-cash.gif' title='Paiement Mandat Cash'>";
								}
								else if(substr_count($orders['payment_method'],"Porte")>0){
									echo "virtuel";
								}?>
						
						  </td>
						  <td <?php echo $link; ?> class="dataTableContent" align="left"><?php echo $orders['orders_status_name']; ?></td>
						</tr>
			<?php
			// begin OSC-Expeditor
			$batch_order_numbers[] = $orders['orders_id'];
		}
		
	}// fin de condition d'affichage (que si pID ou ref ds l'url)
		?>
					<tr>
					  <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
                </table></td>
            </tr>
			<?php echo '</form>'. "\n";?>
            </table></td>
          	</tr>
          </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?><br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>