<?php
/*
	Page permettant de passer une commande de l'admin ou de creer un devis
*/
	
	require('includes/application_top.php');
	require('nouvelle_commande_traitement.php');
	require('../'.DIR_WS_SITE.'/includes/fonctions/mail.php');
	session_start();
	
	if($_SESSION['id_client_panier']!= $_GET['cID']) {
		unset($_SESSION['panier_admin']);
		unset($_SESSION['poids_commande_admin']);
		unset($_SESSION['montant_ht_commande_admin']);
		unset($_SESSION['remise_porte_monnaie_admin']);
		unset($_SESSION['remise_panier_pourcent']);
		unset($_SESSION['taux_tva_commande_admin']);
		unset($_SESSION['livraison_commande_admin']);
		unset($_SESSION['livraison_commande_admin_exact']);
		unset($_SESSION['montant_panier_admin']);
		unset($_SESSION['adresse_commande_admin']);
	}	
	
	$_SESSION['id_client_panier']=$_GET['cID'];
	
	if(isset($_GET['action']) && $_GET['action']=='do_basket') set_session_for_customer($_GET['cID']);
		
	if(isset($_POST['a_remise_g'])) {
		$_SESSION['remise_panier_admin'] = str_replace('%', '', $_POST['remise']);
	}
	
	// Affichage des prix en fonction du pays passé en parametre
	function parametrage_affiche_prix($pays){
			$req_client=tep_db_query("select customers_type from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['id_client_panier']."");
			$res_client=tep_db_fetch_array($req_client);
			$type_client = $res_client['customers_type'];
			
			$req_zone_tva = tep_db_query("select geo_zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where zone_country_id=".$pays."");
			$tva = tep_db_fetch_array($req_zone_tva);
			
			if($tva['geo_zone_id']==GEO_ZONE_AVEC_TVA){
				return 1;
			}
			elseif($type_client==1){
				return 1.2;
			}
			else{
				$req_tva_intra=tep_db_query("select customers_tva from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['id_client_panier']."");
				$nb_res=tep_db_num_rows($req_tva_intra);
				
				if($nb_res==0){}
				elseif($pays==73){return 1.2; }
					else return 1;
			}
	}
	
	function info_produit($i, $id, $option, $option_value, $qte, $cPath) {
		
		$cPath_explode = explode("_", $cPath);
		
		$req = tep_db_query("select p.products_price, p.products_weight, products_name, p.products_model, p.products_cost from ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd where p.products_id=pd.products_id and p.products_id=".$id." ");
		$res = tep_db_fetch_array($req);
		
		$reponse_nom_categorie = tep_db_query("SELECT categories_name, categories_id
										   FROM categories_description
										   WHERE categories_id = '" . $cPath_explode[2] . "'");
		$categorie = tep_db_fetch_array($reponse_nom_categorie);
		
		if($option!=0) {
			
			$req1 = tep_db_query("	
									SELECT 
										products_options_name 
									FROM 
										products_options 
									WHERE 
										products_options_id='".$option."'
								");
			$res1 = tep_db_fetch_array($req1);
			$req2 = tep_db_query("
									SELECT 
										products_options_values_name 
									FROM 
										products_options_values 
									WHERE 
										products_options_values_id='".$option_value."'
									ORDER BY 
										products_options_values_id 
										
								");
			$res2 = tep_db_fetch_array($req2);
			$op = $res1['products_options_name'];
			$op_val = $res2['products_options_values_name'];
		}
		else {
			$op = '';
			$op_val = '';
		}
		
		if(isset($_SESSION['remise_panier_admin']) && !empty($_SESSION['remise_panier_admin'])) {
			$_SESSION['panier_admin']['products_price'][$i] = $_SESSION['panier_admin']['products_price'][$i]-($_SESSION['panier_admin']['products_price'][$i]*($_SESSION['remise_panier_admin']/100));
		}
		
		$prix = (isset($_SESSION['panier_admin']['products_price'][$i]))
			? $_SESSION['panier_admin']['products_price'][$i]
			: $_SESSION['panier_admin']['products_price'][$i] = $res['products_price'];
					
		echo"
		<tr>
		 <td align='center'>".$res['products_model']."</td>
		 <td>".bda_product_name_transform($res['products_name'], $categorie['categories_id'])."</td>
		 <td>".$op.' : '.$op_val."&nbsp;</td>
		 <td align='center'>" . format_to_money($res['products_cost']) . "</td>
		 <td align='center'><input type='text' id='price".$i."' name='price_p".$i."' value='".$prix."' size='6'/></td>
		 <td align='center'><input type='text' id='qte".$i."' name='qte_p".$i."' value=".$qte." size='3' /></td>
		 <td align='center'>".format_to_money($qte*$prix)."</td>
		 <td align='center'>".format_to_money(($qte*$prix)*1.2)."</td>
		 <td align='center'><input type='button' id='qte".$i."' value='supprimer' size='3' onclick='suppr_prod(".$i.",".$_GET["cID"].")'/> <input type='hidden' id='poids".$i."' value='".$res['products_weight']."' /></td>	 
		<tr/>";
	}
	  
	?>
	<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html <?php echo HTML_PARAMS; ?>>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
	<title><?php echo TITLE; ?></title>
	<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
	<script language="javascript" src="includes/general.js"></script>
	</head>
	<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="aff_montant();">
	<!-- header //-->
	<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
	<!-- header_eof //-->
	<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
	<script language="javascript1.1" src="includes/javascript/ajax/module_livraison.js"></script>
	<script language="javascript1.1" src="includes/javascript/ajax/new_commande.js"></script>
	<SCRIPT type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
	<!-- body //-->
	
	<div style="width:100%; margin-top: 10px;">

<!-- Bloc de sélection du Devis -->

    <div id="content info_client" style="border:solid; width:97.5%; margin-left:10px; padding:5px; margin-bottom:10px;">
        <input type="checkbox" id="id_devis" name="devis"  onclick="devis(<?php echo $_GET['cID']; ?>)" <?php if($_SESSION['devis_commande_admin']==1) echo'checked="checked"'; ?> />Faire un devis
    </div>

<!-- Bloc d'informations sur le client -->

    <div id="content info_client" style="border:solid; width:97.5%; margin-left:10px; padding:5px;">
        <?php
        function nom_pays($id){
            $req=tep_db_query("select countries_name from ".TABLE_COUNTRIES." where countries_id=".$id."");
            $res=tep_db_fetch_array($req);
            return $res['countries_name'];
        }
        
        $req_client_info=tep_db_query("select * from ".TABLE_CUSTOMERS." c, ".TABLE_ADDRESS_BOOK." b where c.customers_default_address_id=b.address_book_id and c.customers_id=".$_GET['cID']."");
        $res_client=tep_db_fetch_array($req_client_info);
        ?>
        <table width="100%" border="1" style="text-align: center;">
            <tr>
              <td width="12%"><strong>Num&eacute;ro de Client : </strong><a href="/mtp34/client_edit.php?page=1&cID=<?php echo $res_client['customers_id']; ?>&action=edit" target="_blank"><?php echo $res_client['customers_id']; ?></a></td>
                <td><strong>Prenom : </strong><?php echo $res_client['customers_firstname']; ?></td>
                <td><strong>Nom : </strong><?php echo $res_client['customers_lastname']; ?></td>
                <td><strong>Adresse email : </strong><?php echo $res_client['customers_email_address']; ?></td>
                <td width="15%"><strong>Telephone portable : </strong><?php echo $res_client['customers_telephone']; ?></td>
                <td width="15%"><strong>Telephone fixe : </strong><?php echo $res_client['customers_fixe']; ?></td>
            <td><strong>Nom de la societe : </strong><?php echo $res_client['customers_company']; ?></td>
            </tr>
        </table>
               
        <br />
        Adresse de facturation :<br />        
		<table border="1" width="100%">
			<tr>
				<td width="50px" align="center">-</td>
				<td width="100px" align="center">Nom</td>
				<td width="100px" align="center">Prenom</td>
				<td width="100px" align="center">Societe</td>
				<td width="300px" align="center">Adresse</td>
				<td width="100px" align="center">Code Postal</td>
				<td width="100px" align="center">Ville</td>
				<td width="200px" align="center">Pays</td>
			</tr>
          
          <?php
		$req_adresse=tep_db_query("	SELECT cus.customers_default_address_id, a.*  
									FROM ".TABLE_ADDRESS_BOOK." a, ".TABLE_CUSTOMERS." cus 
									WHERE a.customers_id=".$_GET['cID']." AND cus.customers_id = a.customers_id");
		while($res_adresse=tep_db_fetch_array($req_adresse)){
		?>
			<tr <?php if ($res_adresse['address_book_id']==$res_adresse['customers_default_address_id']) { echo 'style="background-color: #99cc99;"'; } ?> >
				<td align="center">
					<input type="radio" <?php if ($res_adresse['address_book_id']==$res_adresse['customers_default_address_id']) { echo 'checked="checked"'; } else { echo 'disabled="disabled"'; } ?> />
				</td>
				<td align="center"><?php echo $res_adresse['entry_lastname']; ?></td>
				<td align="center"><?php echo $res_adresse['entry_firstname']; ?></td>
				<td align="center"><?php echo $res_adresse['entry_company'];?></td>
				<td align="center">
					<?php 
					if (!empty($res_adresse['entry_street_address_3'])) { echo $res_adresse['entry_street_address_3'].'<br>'; } 
					if (!empty($res_adresse['entry_suburb'])) { echo $res_adresse['entry_suburb'].'<br>'; }
					echo $res_adresse['entry_street_address'].'<br>'; 					 
					if (!empty($res_adresse['entry_street_address_4'])) { echo $res_adresse['entry_street_address_4']; } 
					?>
				</td>
				<td align="center"><?php echo $res_adresse['entry_postcode']; ?></td>
				<td align="center"><?php echo $res_adresse['entry_city']; ?></td>
				<td align="center"><?php echo nom_pays($res_adresse['entry_country_id']); ?></td>
			</tr>		  
		<?php
		}
		?>
        </table>
        
        <div style="clear:both;"></div>
        <br />
        Adresses de livraison :<br />
        
		<table border="1" width="100%">
			<tr>
				<td width="50px" align="center">-</td>
				<td width="100px" align="center">Nom</td>
				<td width="100px" align="center">Prenom</td>
				<td width="100px" align="center">Societe</td>
				<td width="300px" align="center">Adresse</td>
				<td width="100px" align="center">Code Postal</td>
				<td width="100px" align="center">Ville</td>
				<td width="200px" align="center">Pays</td>
			</tr>
        <?php
		$req_adresse=tep_db_query("select cus.customers_default_address_id, a.*  
								     from ".TABLE_ADDRESS_BOOK." a, ".TABLE_CUSTOMERS." cus 
									 where a.customers_id=".$_GET['cID']." and cus.customers_id = a.customers_id");
        while($res_adresse=tep_db_fetch_array($req_adresse)){
        ?>
			<tr <?php if ($res_adresse['address_book_id']==$res_adresse['customers_default_address_id']) { echo 'style="background-color: #99cc99;"'; } ?>>
				<td align="center"><input type="radio" name="rd" id="address<?php echo $res_adresse['address_book_id']; ?>" onClick="choisse(<?php echo $res_adresse['address_book_id']; ?>, <?php echo $_GET['cID'];?>,'livraison');" <?php if(isset($_SESSION['adresse_commande_admin']) && $_SESSION['adresse_commande_admin']==$res_adresse['address_book_id']) echo 'checked="checked"'; ?> /></td>
				<td align="center"><?php echo $res_adresse['entry_lastname']; ?></td>
				<td align="center"><?php echo $res_adresse['entry_firstname']; ?></td>
				<td align="center"><?php echo $res_adresse['entry_company'];?>&nbsp;</td>
				<td align="center">
					<?php 
					if (!empty($res_adresse['entry_street_address_3'])) { echo $res_adresse['entry_street_address_3'].'<br>'; } 
					if (!empty($res_adresse['entry_suburb'])) { echo $res_adresse['entry_suburb'].'<br>'; }
					echo $res_adresse['entry_street_address'].'<br>'; 
					if (!empty($res_adresse['entry_street_address_4'])) { echo $res_adresse['entry_street_address_4']; } 
					?>					
				</td>
				<td align="center"><?php echo $res_adresse['entry_postcode']; ?></td>
				<td align="center"><?php echo $res_adresse['entry_city']; ?></td>
				<td align="center"><?php echo nom_pays($res_adresse['entry_country_id']); ?></td>
			</tr>
		<?php
		}
		?>
		</table>
    </div>

<!-- Bloc de recherche des articles -->
	
	<?php 
    if(isset($_SESSION['adresse_commande_admin']) && !empty($_SESSION['adresse_commande_admin'])){
    ?>
    <div id="info_commande" style="margin-top:10px; border:solid; width:97.5%; margin-left:10px; padding:5px;">
    
    <div style="float: left;">
    	<span style="font-weight: bold; font-size: 1.1em; color: #F60;">Choix de l'article<br /></span>
        
        <div style="border: 1px solid black; background-color: #99cc99; padding: 5px; height: 296px;">
            <select name="rayon" id="rayon" onChange="marque_search();">
                <?php
                $reponse_rayon = tep_db_query("SELECT *
                                               FROM rubrique
                                               ORDER BY rubrique_id");
                while($donnees_rayon = tep_db_fetch_array($reponse_rayon)){
                    ?>
                    <option value="<?php echo $donnees_rayon['rubrique_id']; ?>"><?php echo $donnees_rayon['rubrique_name']; ?></option>
                    <?php
                }
                ?>
            </select>
            
            <select name="marque" id="marque" onChange="modele_search();article_search();">
                <option value="">Choisissez une marque</option>
                <?php
                $reponse_marque = tep_db_query("SELECT cd.categories_name, cd.categories_id, cd.categories_url, cd.balise_title_lien_texte, categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, categories_status_adm, categories_status_vip
                                                 FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id inner join categories_rubrique as cr on c.categories_id=cr.categories_id
                                                 WHERE c.parent_id='0' AND cr.rubrique_id = 1 and c.categories_status='1'
                                                 ORDER BY c.sort_order, cd.categories_name ASC");
                while($donnees_marque = tep_db_fetch_array($reponse_marque)){
                    ?>
                    <option value="<?php echo $donnees_marque['categories_id']; ?>"><?php echo $donnees_marque['categories_name']; ?></option>
                    <?php
                }
                ?>
            </select>
            
            <select name="modele" id="modele" onChange="article_search();">
                <option value="">Choisissez un mod&egrave;le</option>
            </select>
            
            <br /><br />
            
            <select name="liste_articles" id="liste_articles" size="15" onclick="options_search();" disabled >
                <option value="">Veuillez d'abord remplir les champs ci-dessus</option>
            </select>
            
            <select name="liste_options" id="liste_options" size="15" onclick="verif_qte_2();" disabled >
                <option value="">Pas d'options</option>
            </select>
            
            <input type="hidden" id="cPath" value="" />
            <input type="hidden" id="recup_info" value="" />
            <br /><br />
            Quantit&eacute; : <input type="text" id="qte_prod" value="" onKeyUp="verif_qte_2();" disabled/>
            <input type="button" id="add_prod" value="ajouter" onClick="ajout_prod(<?php echo $_GET['cID']; ?>)" disabled />
		</div>
    </div>
    
    <div style="float: left; margin-left: 5px;">
	    <span style="font-weight: bold; font-size: 1.1em; color: #F60;">Informations de l'article<br /></span>
        
        <div style="border: 1px solid black; background-color: #99cc99; padding: 15px; width:250px; height: 276px;" id="article_infos">
    	</div>
    </div>
    
    <div style="clear: both;"></div>
    
    </div>

<!-- Bloc de détails des articles -->
    
    <div id="info_produit" style="margin-top:10px; border:solid; width:97.5%; margin-left:10px; padding:5px;">
    <?php
        if(count($_SESSION['panier_admin']['products_id'])==0){
            $_SESSION['panier_admin']=array();
            $_SESSION['panier_admin']['products_id']=array();
            $_SESSION['panier_admin']['option_id']=array();
            $_SESSION['panier_admin']['option_values_id']=array();
            $_SESSION['panier_admin']['qte']=array();
        }
        
    echo "Nombre d'articles : ".count($_SESSION['panier_admin']['products_id'])."<br/>";
    ?>
    <form method="post" action="nouvelle_commandeP1.php?cID=<?php echo $_GET['cID']; ?>" name="liste">
    <?php
    
    //mise a jour 
    if(isset($_POST['liste_p'])){
        for($i=0;$i<count($_SESSION['panier_admin']['products_id']);$i++){
            $_SESSION['panier_admin']['qte'][$i]=$_POST['qte_p'.$i];
            $_SESSION['panier_admin']['products_price'][$i]=$_POST['price_p'.$i];		
        }
    }
    
    echo "<table border='1' width='100%'>
    <tr>
         <td width='150px' align='center'>Ref Produit</td>
         <td width='300px' align='center'>Nom produit</td>
         <td width='200px' align='center'>Option</td>
		 <td width='125px' align='center'>Prix Achat</td>
         <td width='125px' align='center'>Prix ht</td>
         <td width='125px' align='center'>Qte</td>
         <td width='125px' align='center'>Prix ht</td>
         <td width='125px' align='center'>Prix ttc</td>
         <td width='125px' align='center'>Action</td>	 
        <tr/>";
    for($i=0;$i<count($_SESSION['panier_admin']['products_id']);$i++){
        
        $id=$_SESSION['panier_admin']['products_id'][$i];
        $option=$_SESSION['panier_admin']['option_id'][$i];
        $option_value=$_SESSION['panier_admin']['option_values_id'][$i];
        $qte=$_SESSION['panier_admin']['qte'][$i];
		$cPath = $_SESSION['panier_admin']['cPath'][$i];
        
        info_produit($i,$id,$option,$option_value,$qte, $cPath);
        
    }
    echo "</table>";
    
    if(isset($_SESSION['remise_panier_admin']) && !empty($_SESSION['remise_panier_admin'])) {
		
    	$_SESSION['remise_panier_pourcent'] = $_SESSION['remise_panier_admin'];
    	unset($_SESSION['remise_panier_admin']);
    }
    
    if(count($_SESSION['panier_admin']['products_id'])>0) {
    ?>
    <input type="submit" id="maj" name="liste_p" value="Mise A Jour" />
    </form>
    <?php } ?>
    </div>

<!-- Bloc de Total -->

<?php // ****************************** ?>

    <div id="total_produit" style="margin-top:10px; border:solid; width:97.5%; margin-left:10px; padding:5px;">
    <input type="hidden" id="nb_ligne_panier" value="<?php echo count($_SESSION['panier_admin']['products_id']); ?>" />
    <input type="hidden" id="adresse_id" value="<?php echo $_SESSION['adresse_commande_admin']; ?>" />
    <div id="affiche_montant"></div>
    </div>

<!-- Bloc de remise -->

    <div id="info_produit" style="margin-top:10px; border:solid; width:97.5%; margin-left:10px; padding:5px;">
    <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>" name="rm">
    Appliquer une remise en pourcentage a tous les articles :
                            <input type="text" id="remise_g" name="remise" value="" />%
                            <input type="submit" id="a_remise" name="a_remise_g" value="appliquer" /> <font color="#FF0000">* ne mettre que des chiffres</font>
    </form>
    </div>

<!-- Bloc de commentaires -->

    <div id="commentaire_orders" style="margin-top:10px; border:solid; width:97.5%; margin-left:10px; padding:5px;">
    <?php
    if(isset($_POST['orders_comments'])){	
        $_SESSION['orders_comments']=$_POST['orders_comments'];
    }
    ?>
    <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>" name="comments">
    Appliquer un commentaire a cette commande/devis :<br />
                            <textarea id="orders_comments" name="orders_comments" cols="150" rows="5"><?php echo $_SESSION['orders_comments'];?></textarea><br />
                            <input type="submit" id="o_comments" name="o_comments" value="Sauvegarder le commentaire" />
    </form>
    </div>

<!-- Bloc moyen paiement -->
    
    <div id="total_produit" style="margin-top:10px; border:solid; width:97.5%; margin-left:10px; padding:5px;">
    <?php
    //devis pas de moyen de paiement
    if($_SESSION['devis_commande_admin']==0) {
        
		if($_SESSION['montant_panier_admin']>0 && $_SESSION['remise_porte_monnaie_admin']>0){
			
			if($_SESSION['moyen_paiement_panier_admin']==4) {
				
				unset($_SESSION['moyen_paiement_panier_admin']);
			}
			echo '<div style="color:#FF0000;font-weight:bold;">Porte monnaie entierement utilisé choisir un autre moyen de paiement pourt le reste ou modifier le moyen de paiment en cliquand <a href="javascript:onclick=n_paiement('. $_GET['cID'] .');">ici</a></div>';
		}
        
    if(isset($_SESSION['montant_panier_admin']) && !empty($_SESSION['montant_panier_admin'])) {
                    $i=0;
                    $moyen_p=tep_db_query("select * from ".TABLE_MODULE_PAIEMENT." where paiement_visible='1' AND paiement_id <= 6 AND paiement_montant_max>".$_SESSION['montant_panier_admin']."  order by paiement_ordre_affichage");
                    while($res_paiement=tep_db_fetch_array($moyen_p)) {
						
                        if($res_paiement['paiement_id']==4) {
							
                            $somme_av=tep_db_query("select customers_argent_virtuel from ".TABLE_CUSTOMERS." where customers_id=".$_GET['cID']."");
                            $res_av=tep_db_fetch_array($somme_av);
                        }
                        
                        echo '<div style="'; 
                        if(($res_av['customers_argent_virtuel']<=0 && $res_paiement['paiement_id']==4 ) || ($res_paiement['paiement_id']==4 && $_SESSION['montant_panier_admin']>0 && $_SESSION['remise_porte_monnaie_admin']>0)) echo ' display:none; ';
                                  echo 'height:50px; background-image:url(../'.DIR_WS_SITE.'/template/base/images_paiement/fond_moyen_de_paiement.gif); background-repeat:no-repeat; margin-top:10px; margin-left: 20px; margin-bottom:10px; ">
                                  
                              <div style="float:left; width:250px; text-decoration:underline; height:30px; color:#8e2eaa; font-size:12px; margin-left:15px; margin-top:15px; "><strong>Paiement par '.$res_paiement['paiement_nom'].'</strong>';
                              echo'</div>';						  
                              echo '<div style="float:left; margin-top:18px;  font-size:11px;  color:#788403;">
                                  ';
                                  if($res_av['customers_argent_virtuel']>0 && $res_paiement['paiement_id']==4){
                                      echo "Utilisez les <span style='color:#df4e02;'>".($res_av['customers_argent_virtuel'])." &euro;</span> ";	  
                                      echo "que vous avez dans votre porte monnaie virtuel";
                                  }
                                  else echo $res_paiement['paiement_description'];
                                  
                                  echo'
                              </div>						  						  
                              <div style="float:right; margin-top:15px; margin-right:375px;">
                                  <input ';
                                  if((isset($_SESSION['moyen_paiement_panier_admin']) && !empty($_SESSION['moyen_paiement_panier_admin'])) && $_SESSION['moyen_paiement_panier_admin']==$res_paiement['paiement_id']) 
                                     {echo'checked="checked"';}
                                     
                                 echo ' type="radio" id="paiement'.$i.'" name="supri" onclick="paiement('.$res_paiement['paiement_id'].','.$_GET['cID'].');" />
                             </div>						
                             <div style="float:right; margin-top:5px; margin-right:10px;">
                                <img src="../'.DIR_WS_SITE.'/template/base/images_paiement/'.$res_paiement['paiement_img'].'" />
                             </div>
                      </div>';
                      $i++;
                    }
                    echo'<input type="hidden" id="compteur" name="compt" value="'.$i.'" />';
    }
    }
    ?>
    </div>

<!-- Bloc fin validité devis -->

	<?php 
    if(isset($_SESSION['adresse_commande_admin']) && !empty($_SESSION['adresse_commande_admin']) && count($_SESSION['panier_admin']['products_id'])>0) {
		?>
		<form method="post" action="nouvelle_commandeP1.php?cID=<?php echo $_GET['cID']; ?>" name="valid">
		<div id="validation" style="margin-top:10px; border:solid; width:97.5%; margin-left:10px; padding:5px;">
		<?php if(isset($_SESSION['devis_commande_admin']) && $_SESSION['devis_commande_admin']==1){ ?>
		Date de fin de validit&eacute; du devis <input type="text" id="date_devis" name="date_dev" value="<?php echo date("d-m-Y", mktime(0, 0, 0, date("m")  , date("d")+10, date("Y"))); ?>"  />
		<a href="#validation" onClick="displayCalendar(date_devis,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
		<br /><br />
		
		<?php } ?>
		<input type="submit" id="valider_cmd" name="valider_c" value="Valider la commande" />
		</form>
		
		<?php
		if(isset($_POST['valider_c'])){
		
			if($_SESSION['taux_tva_commande_admin']==1.2){
			 $_SESSION['tva_frais_de_port']=round($_SESSION['livraison_commande_admin_exact']*0.2,2);}
			else $_SESSION['tva_frais_de_port']=0;
		
			$date = explode("-", $_POST['date_dev']);
			$_SESSION['date_fin_devis']=$date[2]."-".$date[1]."-".$date[0]." 23:59:59";
			
			if( (isset($_SESSION['moyen_paiement_panier_admin']) && !empty($_SESSION['moyen_paiement_panier_admin']))
				|| isset($_SESSION['devis_commande_admin']) && $_SESSION['devis_commande_admin']) 
			
			traitement_commande();
		}
		?>
		</div>
		<?php
    }
    }
    ?>
    </div>
    <!-- body_eof //-->
    
    <!-- footer //-->
    <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
    <!-- footer_eof //-->
    <br>
    </body>
    </html>
    <?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>