<?php
/*
  $Id: reclamaposte_extract.php,v 1.0 11/03/2009 Phocea

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
  
  USe this file to retrieve MD5 of new images. Image must be named servlet.png and placed in same folder as this script.

*/
echo md5(file_get_contents('servlet.png'));
?>