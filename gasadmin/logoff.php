<?php
/*
  $Id: logoff.php,v 1.12 2003/02/13 03:01:51 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  //require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGOFF);

//tep_session_destroy();
  tep_session_unregister('login_id');
  tep_session_unregister('login_firstname');
  tep_session_unregister('login_groups_id');

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<style type="text/css">
<!--
a { color:#080381; text-decoration:none; }
a:hover { color: #000000; color:#aabbdd; text-decoration:underline; }
a.text:link, a.text:visited { color: #000000; text-decoration: none; }
a:text:hover { color: #000000; text-decoration: underline; }
A.login_heading:link, A.login_heading:visited { underline; font-family: Verdana, Arial, sans-serif; font-size: 12px; color: #F0F0FF;}
A.login_heading: hover { color: #F0F0FF;}
.heading { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 20px; font-weight: bold; line-height: 1.5; color: #D3DBFF; }
.text { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; color: #000000; }
.smallText { font-family: Verdana, Arial, sans-serif; font-size: 10px; }
.login_heading { font-family: Verdana, Arial, sans-serif; font-size: 12px; color: #ffffff;}
.login { font-family: Verdana, Arial, sans-serif; font-size: 12px; color: #000000;}
//--></style>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

<table border="0" width="600" height="100%" cellspacing="0" cellpadding="0" align="center" valign="middle">
  <tr>
    <td><table border="0" width="600" height="440" cellspacing="0" cellpadding="1" align="center" valign="middle">
      <tr bgcolor="#000000">
        <td><table border="0" width="600" height="440" cellspacing="0" cellpadding="0">
          <tr bgcolor="#ffffff" height="50">
            <td width="600" height="60" align="center" bgcolor="#ffffff">
			<img src="images/boite_boutique.gif" width="180" height="110" border="0">
			</td>
            </tr>
			<tr>
			<td height="1"></td>
			</tr>
          <tr bgcolor="#080381">
            <td colspan="2" align="center" valign="middle" bgcolor="#FFFFFF"><table width="330" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td align="center" valign="top" class="login">&nbsp;<b>D&eacute;connexion de l'Administration</b></td>
              </tr>
              <tr>
                <td height="100%" width="100%" valign="top" align="center">
                  <table border="0" height="100%" width="100%" cellspacing="0" cellpadding="1" bgcolor="#666666">
                    <tr>
                      <td><table border="0" width="100%" height="100%" cellspacing="3" cellpadding="2" bgcolor="#CCCCCC">
                          <tr>
                            <td align="center" class="login"><?php echo '<br>Votre session sur <b>l�administration est ferme</b>. Merci de votre visite et � bient�t !<br><br>'; ?></td>
                          </tr>
                          <tr>
                            <td class="login" align="center"><?php echo '<a class="login" href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
                          </tr>
                       </table></td>
                    </tr>
                </table></td>
              </tr>
            </table>             
           </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><?php require(DIR_WS_INCLUDES . 'footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>

</body>

</html>
