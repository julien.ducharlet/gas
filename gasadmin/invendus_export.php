<?php
// Include application configuration parameters
	require('includes/configure.php');
  
// include the database functions
	require('includes/functions/database.php');

// make a connection to the database... now
	tep_db_connect() or die('Unable to connect to database server!');
  
	if ($_GET['action'] == 'export'){
		if (isset($_POST['export_article'])){
			
		$contenu_fichier = "id;Référence;Nom\n";
		
		foreach($_POST['export_article'] as $id_article){
			$reponse = mysql_query('SELECT products_model FROM products WHERE products_id = "' . $id_article . '"');
			$donnees = mysql_fetch_array($reponse);
			
			$reponse = mysql_query('SELECT products_name FROM products_description WHERE products_id = "' . $id_article . '"');
			$result_nom = mysql_fetch_array($reponse);
			$products_name = $result_nom['products_name'];
			
			$suppressions_surplus = array("pour", "#", "*", "[1]");
			$titre_description = str_replace($suppressions_surplus, "", $products_name);
			
			$contenu_fichier .= $id_article . ";" . $donnees['products_model'] . ";" . $titre_description . "\n";
		}
			
		header("Content-disposition: attachment; filename=articles_supprimés.csv" );
		header("Content-Type: application/force-download" );
		header("Content-Transfer-Encoding: text/csv\n" ); // Surtout ne pas enlever le \n
		header("Content-Length: " . strlen($contenu_fichier));
		header("Pragma: no-cache" );
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public" );
		header("Expires: 0" );
		
		echo $contenu_fichier;
			
		}
	}
	
	echo '<META HTTP-EQUIV="Refresh" CONTENT="0; URL=invendus.php">';
?>