<?php
if($_GET['spplus']=="oui"){
	unset($_SESSION['id_client_panier']);
	unset($_SESSION['remise_porte_monnaie_admin']);
	unset($_SESSION['montant_panier_admin']);
	unset($_SESSION['montant_ht_commande_admin']);
	unset($_SESSION['livraison_commande_admin']);
	unset($_SESSION['remise_porte_monnaie_admin']);
	unset($_SESSION['taux_tva_commande_admin']);
	unset($_SESSION['livraison_commande_admin']);
	unset($_SESSION['tva_frais_de_port']);
	unset($_SESSION['poids_commande_admin']);

	$_SESSION['panier_admin']=array();
	$_SESSION['panier_admin']['products_id']=array();
	$_SESSION['panier_admin']['option_id']=array();
	$_SESSION['panier_admin']['option_values_id']=array();
	$_SESSION['panier_admin']['qte']=array();
	$_SESSION['panier_admin']['products_price']=array();
}

	require('includes/application_top.php');
	require('includes/functions/spplus_code_etat.php');
	require('includes/functions/spplus_pays_iso.php');
	require('../' . DIR_WS_SITE . '/includes/configure_mail.php');
	require('../' . DIR_WS_SITE . '/includes/fonctions/mail.php');

	require(DIR_WS_CLASSES . 'currencies.php');
	$currencies = new currencies();

	$orders_statuses = array();
	$orders_status_array = array();
	$orders_status_query = tep_db_query("SELECT orders_status_id, orders_status_name FROM " . TABLE_ORDERS_STATUS . " WHERE language_id = '" . (int)$languages_id . "' ORDER BY orders_status_name ");
	while ($orders_status = tep_db_fetch_array($orders_status_query)) {
		$orders_statuses[] = array('id' => $orders_status['orders_status_id'],
								   'text' => strip_tags($orders_status['orders_status_name']));
		$orders_status_array[$orders_status['orders_status_id']] = strip_tags($orders_status['orders_status_name']);
	}
  
	$orders_statuses_2 = array();
	$orders_status_array_2 = array();
	$orders_status_query_2 = tep_db_query("SELECT orders_status_id, orders_status_name FROM " . TABLE_ORDERS_STATUS . " WHERE language_id = '" . (int)$languages_id . "' ORDER BY orders_status_name ");
	while ($orders_status_2 = tep_db_fetch_array($orders_status_query_2)) {
		$orders_statuses_2[] = array('id' => $orders_status_2['orders_status_id'],
                               'text' => $orders_status_2['orders_status_name']);
		$orders_status_array_2[$orders_status_2['orders_status_id']] = $orders_status_2['orders_status_name'];
	}
  
  $action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
  if (tep_not_null($action)) {
		switch ($action) {

		// action pour supprimer une information (email, ...) concernant la commande d'un client
		case 'deleteInfo':
			$oSHID= tep_db_prepare_input($HTTP_GET_VARS['oSHID']);// orders_status_history_id � supprimer de la table
			$query = tep_db_query("delete from ".TABLE_ORDERS_STATUS_HISTORY." where orders_status_history_id = '" . $oSHID . "'");
			tep_redirect(tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('action')) . 'action=edit'));
		break;
      
		case 'update_date_fin_devis':
			$query = 'UPDATE '. TABLE_ORDERS .' SET orders_date_fin_devis=\''. $_POST['validity_end'] .' 23:59:59\' WHERE orders_id='. $_GET['oID'];
			tep_db_query($query);
			
			tep_redirect(tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('action')) . 'action=edit'));
		break;
		
		case 'update_order':
        $oID = tep_db_prepare_input($HTTP_GET_VARS['oID']);
        $status = tep_db_prepare_input($HTTP_POST_VARS['status']);
        $comments = tep_db_prepare_input($HTTP_POST_VARS['comments']);
		
		// SUIVI COLLISSIMO+UPS
     	$track_num = tep_db_prepare_input($HTTP_POST_VARS['track_num']);
     	$track_num2 = tep_db_prepare_input($HTTP_POST_VARS['track_num2']);    
		$track_num3 = tep_db_prepare_input($HTTP_POST_VARS['track_num3']);
		$track_num4 = tep_db_prepare_input($HTTP_POST_VARS['track_num4']);
		$track_num5 = tep_db_prepare_input($HTTP_POST_VARS['track_num5']);
		// FIN SUIVI COLLISSIMO+UPS

        $order_updated = false;
        $check_status_query = tep_db_query("SELECT customers_name, customers_email_address, orders_status, date_purchased, orders_numero_facture FROM " . TABLE_ORDERS . " WHERE orders_id = '" . (int)$oID . "'");
        $check_status = tep_db_fetch_array($check_status_query);
		
        if ( ($check_status['orders_status'] != $status) || tep_not_null($comments)) {
        	tep_db_query("UPDATE " . TABLE_ORDERS . " SET orders_status = '" . tep_db_input($status) . "', last_modified = now() WHERE orders_id = '" . (int)$oID . "'");    
         	// AJOUT POUR LES NUMEROS DE FACTURE
			if (AUTORISE_NUMERO_FACTURE == 'true') {
				
           		if (tep_db_input($status) == STATUT_GENERANT_FACTURE) {
					
					$check_invoice_counter_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'COMPTEUR_FACTURE'");
					$check_invoice_counter = tep_db_fetch_array($check_invoice_counter_query);
					$numero_facture = $check_invoice_counter['configuration_value'];
					
					if ($check_status['orders_numero_facture'] == 0) {
						
               			tep_db_query("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '" . ($numero_facture+1) . "' WHERE configuration_key = 'COMPTEUR_FACTURE'");
						tep_db_query("UPDATE " . TABLE_ORDERS . " SET orders_numero_facture = '" . $numero_facture . "', last_modified = now(), orders_date_facture = now() WHERE orders_id = '" . (int)$oID . "'");
   					}
					else $messageStack->add_session('Attention : Un nouveau num&eacute;ro de facture n\'a pas &eacute;t&eacute; g&eacute;n&eacute;r&eacute;, car la commande avait d&eacute;j&agrave; fait l\'objet d\'une facture.', 'warning');
           		}
         	}
            $customer_notified = '0';
			
          	if (isset($HTTP_POST_VARS['notify']) && ($HTTP_POST_VARS['notify'] == 'on')) {
		  		
				$owner_email=STORE_OWNER_EMAIL_ADDRESS;
				$email_status='Le statut de votre commande a �t� mis � jour : %s' . "\n\n" . SIGNATURE_MAIL;
				
				$notify_comments = '';
				
            	if(strlen($comments)>0) {
					
			  		$notify_comments = STORE_NAME . ' vous informe : ' . "\n\n".$comments."\n" . "\n\n";
			  	}
				else $comments=NULL;
				
            	// SUIVI COLLISSIMO+UPS
				if ((isset($HTTP_POST_VARS['notify_tracking']) && ($HTTP_POST_VARS['notify_tracking'] == 'on')) & (tep_not_null($track_num))) {
					
  					$notify_tracking = 'Vous pouvez suivre votre colis en cliquant sur le lien ci-dessous : ' . "\n" . '<a href="https://www.coliposte.net/particulier/suivi_particulier.jsp?colispart=' . $track_num . '" style="color:#aab41d; font-weight:bold; " target="_blank">https://www.coliposte.net/particulier/suivi_particulier.jsp?colispart=' . $track_num . '</a>' . "\n\n";
  				}
				
				if ((isset($HTTP_POST_VARS['notify_tracking']) && ($HTTP_POST_VARS['notify_tracking'] == 'on')) & (tep_not_null($track_num2))) {
					
 					$notify_tracking = 'Vous pouvez suivre votre colis en cliquant sur le lien ci-dessous : ' . "\n" . '<a href="https://www.csuivi.courrier.laposte.fr/default.asp?EZ_ACTION=rechercheRapide&tousObj=&numObjet=' . $track_num2 . '" style="color:#aab41d; font-weight:bold; " target="_blank">https://www.csuivi.courrier.laposte.fr/default.asp?EZ_ACTION=rechercheRapide&tousObj=&numObjet=' . $track_num2 . '</a>' . "\n\n";
   				}
				
				if ((isset($HTTP_POST_VARS['notify_tracking']) && ($HTTP_POST_VARS['notify_tracking'] == 'on')) & (tep_not_null($track_num3))) {
					
 					$notify_tracking = 'Vous pouvez suivre votre colis en cliquant sur le lien ci-dessous : ' . "\n" . '<a href="https://www.fr.chronopost.com/fr/tracking/result?listeNumeros=' . $track_num3 . '" style="color:#aab41d; font-weight:bold; " target="_blank">https://www.fr.chronopost.com/fr/tracking/result?listeNumeros=' . $track_num3 . '</a>' . "\n\n";
   				}
				
				if ((isset($HTTP_POST_VARS['notify_tracking']) && ($HTTP_POST_VARS['notify_tracking'] == 'on')) & (tep_not_null($track_num4))) {
					
 					$notify_tracking = 'Vous pouvez suivre votre colis en cliquant sur le lien ci-dessous : ' . "\n" . '<a href="https://www.dpd.fr/traces_exd_' . $track_num4 . '" style="color:#aab41d; font-weight:bold; " target="_blank">https://www.dpd.fr/traces_exd_' . $track_num4 . '</a>' . "\n\n";
   				}
				
				if ((isset($HTTP_POST_VARS['notify_tracking']) && ($HTTP_POST_VARS['notify_tracking'] == 'on')) & (tep_not_null($track_num5))) {
					
 					$notify_tracking = 'Vous pouvez suivre votre colis en cliquant sur le lien ci-dessous : ' . "\n" . '<a href="https://www.ups.com/WebTracking?loc=fr_fr&requester=ST&trackingNumber=' . $track_num5 . '" style="color:#aab41d; font-weight:bold; " target="_blank">https://www.ups.com/WebTracking?loc=fr_fr&requester=ST&trackingNumber=' . $track_num5 . '</a>' . "\n\n";
   				}
			
				if(strcmp($notify_tracking,"on")==0) $notify_tracking='';
				
				$email = 'Num�ro de commande : ' . $oID . "\n" . 
						 'Date de commande : ' . tep_date_long($check_status['date_purchased']) . "\n\n" . $notify_tracking . 
				$notify_comments . sprintf($email_status, $orders_status_array[$status]);
				
				mail_client_commande($check_status['customers_email_address'], SUJET_INFORMATION_CMD, nl2br($email));
				
            	//tep_mail($check_status['customers_name'], $check_status['customers_email_address'], EMAIL_TEXT_SUBJECT, $email, $store, $owner_email);
	            $customer_notified = '1';
			}

			tep_db_query("INSERT INTO " . TABLE_ORDERS_STATUS_HISTORY . " (orders_id, orders_status_id, date_added, customer_notified, comments, track_num, track_num2, track_num3, track_num4, track_num5) values ('" . (int)$oID . "', '" . tep_db_input($status) . "', now(), '" . tep_db_input($customer_notified) . "', '" . tep_db_input($comments)  . "',
				'" . tep_db_input($track_num) . "',
				'" . tep_db_input($track_num2) . "',
				'" . tep_db_input($track_num3) . "',
				'" . tep_db_input($track_num4) . "',
				'" . tep_db_input($track_num5) . "'
			)");

		    $order_updated = true;
        }

        ($order_updated) 
				? $messageStack->add_session('Bravo : La commande est mise � jour avec succ&eacute;s.', 'success')
				: $messageStack->add_session('Attention : Aucune modification n\'a &eacute;t&eacute; effectu&eacute;e. La commande n\'a pas &eacute;t&eacute; mise a jour.', 'warning');

        tep_redirect(tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('action')) . 'action=edit'));
        break;
    }
  }

  if (($action == 'edit') && isset($HTTP_GET_VARS['oID'])) {
    $oID = tep_db_prepare_input($HTTP_GET_VARS['oID']);
    $orders_query = tep_db_query("SELECT orders_id FROM " . TABLE_ORDERS . " WHERE orders_id = '" . (int)$oID . "'");
    $order_exists = true;
    if (!tep_db_num_rows($orders_query)) {
      	$order_exists = false;
      	$messageStack->add(sprintf('Erreur : La commande n\'existe pas.', $oID), 'error');
    }
  }

  include(DIR_WS_CLASSES . 'order.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
	<?php 
		// fonction qui permet de convertir un tableau php (une dimension) en tableau jvs
		function construisTableauJS($tableauPHP, $nomTableauJS){
			echo $nomTableauJS." = new Array();";
			for($i = 0; $i < count($tableauPHP); $i++){
				if(!is_array($tableauPHP[$i])){
					echo $nomTableauJS."[".$i."] = '".$tableauPHP[$i]."';";
				}
				else{
					construisTableauJS($tableauPHP[$i], $nomTableauJS."[".$i."]");
				}
			}
			return;
		}

		// On recupere toutes les informations concernant les statuts, les commentaires associ�s qui correspondent
		$order_status_query = tep_db_query("SELECT * FROM " . TABLE_ORDERS_STATUS);
		$list_statut_id = array(); 
		$list_statut_comments = array(); 
		$list_statut_message = array(); 
		
		while ($tuple = mysql_fetch_array($order_status_query)){
			$list_statut_id[]= $tuple['orders_status_id'];
			$list_statut_comments[]= $tuple['orders_status_comments'];
			// surement pour le SMS ce qui est en dessous
			$list_statut_message[]= $tuple['orders_status_message'];	
		}

		// script jvs qui permet de construire des tableaux jvs a partir de tableau php
		echo "<script type='text/javascript'>";
		construisTableauJS($list_statut_id, "monTableauJS_id");
		construisTableauJS($list_statut_comments, "monTableauJS_comments");
		// surement pour le SMS ce qui est en dessous
		construisTableauJS($list_statut_message, "monTableauJS_message");
		echo "</script> ";
		//***********************************************************************************
	?>

	<script language="JavaScript">
		// fonction pour charger le texte pr�-d�finit d'un statut
		function Charger_statut(idStatut) {
			for (i = 0, j = monTableauJS_id.length, k = 0; i < j; i++){
				if(monTableauJS_id[i]==idStatut)
					var indice=i;
			}
			
			// On remplie le champs associe au commentaire 
			champ_commentaire = document.formulaire.comments;
			var reg=new RegExp("(-- )", "g");
			champ_commentaire.value = monTableauJS_comments[indice].replace(reg,"\n");
			
			document.getElementById('Textarea_sms').focus();
		}
		
		// fonction pour supprimer le contenu du champ d'un commentaire
		function Effacer_champ(leChamp) {
			champ_commentaire = document.formulaire.comments;
			champ_commentaire.value ="";
		}
	</script>
	<!-- ORDERS EDITOR AJAX -->
	<script language="javascript" src="includes/ajax.js"></script>
	<!-- ORDERS EDITOR AJAX -->
    
    

<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/lightbox.css">
<!--<script language="javascript" src="includes/javascript/scripts/prototype.js"></script>
<script language="javascript" src="includes/javascript/scripts/lightbox.js"></script>-->
<script language="javascript" src="includes/general.js"></script>

<script src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script type="text/javascript">    
$(document).ready(function() {

	var ORDER = '<?php echo $_GET['oID']; ?>';
	var CUSTOMER = $("#customer_id").val();
		
	$("#showPorteMonnaie").click(function() {
		
		$("#overlay").show();
		
		$.ajax({
			type: "GET",
			url: 'client_versement.php',
			
			data: 'id_client='+ CUSTOMER +'&action=make_versement',
			dataType: "text",
			processData: false,
			success: function(data) {
				
				$("#lightbox").html(data);
				$("#lightbox").slideDown();
			}
		});
	});
			
	$("#lightbox #myOrderForm").live('submit', function() {
		
		$.ajax({
			type: "POST",
			url: 'includes/javascript/ajax/update_product_order.php?action=update&orders_id='+ ORDER,
			
			data: $(this).serialize(),
			dataType: "text",
			processData: false,
			success: function(data) {
				
				close_lightbox('lightbox');
				
				location.reload();
			}
		});
		
		return false;
	});
			
			
	$("#lightbox #myForm").live('submit', function() {
		
		orders_products_id = $("#orders_products_id").val()
		
		$.ajax({
			type: "POST",
			cache: false,
			url: 'includes/javascript/ajax/update_product_order.php?action=update&orders_products_id='+ orders_products_id,
			
			data: $(this).serialize(),
			dataType: "text",
			processData: false,
			success: function(data) {
				
				close_lightbox('lightbox');
				
				location.reload();
			}
		});
		
		return false;
	});
			
	//�dition d'un produit
	$(".edit").click(function() {
		
		orders_products_id = $(this).attr('id').split('_');
		orders_products_id = orders_products_id[1];
		
		$("#overlay").show();
		
		$.ajax({
			type: "GET",
			url: 'includes/javascript/ajax/update_product_order.php',
			
			data: "action=select&orders_products_id="+ orders_products_id,
			dataType: "text",
			processData: false,
			success: function(data) {
				
				$("#lightbox").html(data);
				$("#lightbox").slideDown();
			}
		});
	});
			
	//�dition de la commande
	$(".edit_order").click(function() {
		
		$("#overlay").show();
		
		$.ajax({
			type: "GET",
			url: 'includes/javascript/ajax/update_product_order.php',
			
			data: "action=select&orders_id="+ ORDER,
			dataType: "text",
			processData: false,
			success: function(data) {
				
				$("#lightbox").html(data);
				$("#lightbox").slideDown();
			}
		});
	});
			
	$("#overlay").click(function() {	
		close_lightbox('lightbox');
	});

	function close_lightbox(box_id) {	
		$("#"+ box_id).slideUp("slow").queue(function() {				  
			$("#overlay").hide();
			$(this).dequeue();
		});
	}
});
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

<!-- ORDERS EDITOR AJAX -->
<div id="add-Product" class="addProduct">
<div><?php echo DIV_ADD_PRODUCT_HEADING; ?></div>
<div id="add-product-product" class="addProductContents"><?php echo ADD_PRODUCT_SELECT_PRODUCT; ?></div>
<div id="addProductSearch" class="addProductContentsSearch"><?php require (DIR_WS_INCLUDES . 'advanced_search.php'); ?></div>
<div id="addProductFind">&nbsp;</div>
<div id="ProdAttr">&nbsp;</div>
<a href="javascript: hideAddProducts();"><?php echo tep_image(DIR_WS_LANGUAGES . $language  . '/images/buttons/button_cancel.gif'); ?></a></div>
<!-- ORDERS EDITOR AJAX -->

<?php require(DIR_WS_INCLUDES . 'header.php');?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top">
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  	if (($action == 'edit') && ($order_exists == true)) {
    	$order = new order($oID);
		
		// on recupere toute la fiche (client nom,prenom, etc...)
		$customer_query = tep_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE  customers_id  = '" .$order->customer['id'] . "'");
		while ($tuple = mysql_fetch_array($customer_query)){
			//$option_sms= $tuple['customers_news_sms'];
			$portable_client= $tuple['customers_telephone'];
			$fixe_client= $tuple['customers_fixe'];
			$email_client= $tuple['customers_email_address'];
		}
		
?>
<tr>
	<td width="100%">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageHeading">
					<?php echo '<input type="hidden" id="customer_id" value="'. $order->customer['id'] .'">'; ?>
					Num�ro de commande : <?php echo $oID ?> - Code Client :
					<!--  DEBUT REDIRECTION VIA CODE CLIENT VERS LA FICHE DU CLIENT -->
					<?php 
            
                    // code pour v�rifier si le client existe
                    $customers_exists_query = tep_db_query("SELECT customers_id FROM " . TABLE_CUSTOMERS . " WHERE customers_id = '" . $order->customer['id'] . "'");
                    $id_client = mysql_fetch_array($customers_exists_query);
                    if ($id_client != "") {
                        echo '<a href="' . tep_href_link(FILENAME_CLIENT_EDIT . '?cID=' . $order->customer['id']) . '&action=edit" target="_blank" title="Voir la fiche du client">
					<font class="pageHeading">'.$order->customer['id'].'</b></font></a>';
                    }
                    else {
                        echo "XXXXX";
                    }
                    ?>
					-
					<?php
					// Code pour mettre des icones en fontion du TYPE de client
					$customers_type_query = tep_db_query("SELECT customers_id, customers_type FROM " . TABLE_CUSTOMERS . " WHERE customers_id = '" . $order->customer['id'] . "'");
					$type_client = mysql_fetch_array($customers_type_query);
					if($type_client['customers_type'] == '1'){
						echo 'Client Particulier';
					} else if($type_client['customers_type'] == '2'){
						echo 'Client Professionnel';
					} else if($type_client['customers_type'] == '3'){
						echo 'Client Revendeur';
					} else if($type_client['customers_type'] == '4'){
						echo 'Client Administration';
					} ?>
					<?php 
						echo " - Poids CMD : " . $order->info['poids'] . " kg";
					?>
					&nbsp;&nbsp;&nbsp;
					<form method="post" action="OSC_Expeditor_process.php" target="_blank">
						<input value="Exporter vers Expeditor"  name="batch_order_numbers[<?php echo $oID ;?>]" type="submit">
						<input type="hidden" name="Action_orders" value="expeditor">
					</form>
					&nbsp;&nbsp;&nbsp;
					<a href="/<?php echo DIR_WS_SITE;?>/compte/connexion.php?panier_admin=true&connexion=ok&mail=<?php echo $email_client;?>&mdp=azertyuiopqsdfghjklm" target="_blank"><?php echo tep_image(DIR_WS_IMAGES . 'icons/commande_01.png', $row_basket["nb_articles"]." article(s)");?></a>
				</td>
				<td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
				<td class="pageHeading" align="right">&nbsp;</td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="1" cellpadding="1">
        	<tr><td colspan="3"><?php echo tep_draw_separator(); ?></td></tr>
		</table>
		<?php 		
		// On affiche pas les stats pour le compte de GAS ID CLIENT 73
		if ($order->customer['id'] != '73') {
		
			$total_articles=0;
			$total_price=0;
			$total_cost=0;
			//trouver les num�ros des commandes du client dans table "orders" en fonction de "customers_id"
			$IDcommande = tep_db_query("SELECT orders_id FROM orders WHERE customers_id = " .$order->customer['id'] . "");
			$nombre_de_commandes = mysql_num_rows($IDcommande);
			while($row = mysql_fetch_array($IDcommande)) {
				$total=tep_db_query("SELECT products_quantity AS QUANTITY, products_price AS PRICE, products_cost AS COST FROM orders_products WHERE orders_id=". $row['orders_id']);
				while($data_total=mysql_fetch_object($total)) {
					$total_articles= $total_articles+$data_total->QUANTITY; 
					$total_price=$total_price+$data_total->PRICE*$data_total->QUANTITY;
					$total_cost=$total_cost+$data_total->COST*$data_total->QUANTITY;
				} 
			}	
				
			$total_marge=tep_db_query("SELECT sum(marge) AS total_marge FROM orders WHERE customers_id = " .$order->customer['id']);
			$row_marge = mysql_fetch_array($total_marge);
			$total_price=convert_price(round($total_price*1.2, 2));
			foreach($order->products as $prod){
				$total_article_commande += $prod["qty"];
			}
			?>		
			<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#BFDBDF">
			<tr>
				<td height="25" align="right" width="12.5%" class="formAreaTitle">Nombre de commande : </td>
				<td align="left" width="12.5%">&nbsp;
					<?php echo ' <a href="' . tep_href_link(FILENAME_CMD_LIST . '?cID=' .$order->customer['id'])  . '" target="_blank" title="Voir les commandes du client">
					<font class="pageHeading"><b>'.$nombre_de_commandes.'</b></font></a>'?>
				</td>
				<td align="right" width="12.5%" class="formAreaTitle">Total article(s) achet�(s) : </td>
				<td align="left" width="12.5%" class="formAreaTitle"><font color="#FF0000">&nbsp;<?php echo $total_articles;?></font></td>
				<td align="right" width="12.5%" class="formAreaTitle">Total Achat TTC : </td>
				<td align="left" width="12.5%" class="formAreaTitle"><font color="#FF0000">&nbsp;<?php echo $total_price." �";?></font></td>
				<td align="right" width="12.5%" class="formAreaTitle">Total de la marge HT : </td>
				<td align="left" width="12.5%" class="formAreaTitle"><font color="#FF0000">&nbsp;<?php echo $row_marge["total_marge"]." �";?></font></td>
			</tr>
			<tr>
				<td height="25" align="right" width="12.5%" class="formAreaTitle">Cette commande : </td>
				<td align="right" width="12.5%" class="formAreaTitle"></td>
				<td align="right" width="12.5%" class="formAreaTitle">article(s) achet�(s) : </td>
				<td align="left" width="12.5%" class="formAreaTitle"><font color="#FF0000">&nbsp;<?php echo $total_article_commande;?></font></td>
				<td align="right" width="12.5%" class="formAreaTitle">Total Achat TTC (avec port) : </td>
				<td align="left" width="12.5%" class="formAreaTitle"><font color="#FF0000">&nbsp;<?php echo $order->info['total']." �";?></font></td>
				<td align="right" width="12.5%" class="formAreaTitle">Total de la marge HT : </td>
				<td align="left" width="12.5%" class="formAreaTitle"><font color="#FF0000">&nbsp;<?php echo $order->info['marge']." �";?></font></td>
			</tr>
			</table>
		<?php 
		} else {
			echo '<div style="text-align:center; font-weight:bold; color:red; padding:10px 0 10px 0 ">Pas de stats pour le compte de Group Army Store</div>';
		}
		?>		
		<table width="100%" border="0" cellspacing="1" cellpadding="1">
			<tr>
				<td colspan="3"><?php echo tep_draw_separator(); ?></td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr>
    	<td valign="top"> <!--  TABLEAU CONCERNANT L'ADRESSE DU CLIENT -->
            <table width="100%" border="0">
            <tr>
                <td class="main" valign="top" width="104"><b>Adresse client : </b></td>
                <td class="main"><?php echo tep_address_format($order->customer['format_id'], $order->customer, 1, '', '<br />', 'customers_', $oID); ?></td>
            </tr>
            </table>
		</td>
		<td valign="top"> <!--  TABLEAU CONCERNANT L'ADRESSE D'EXPEDITION  -->
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
                <td class="main" valign="top"  width="81"><b>Adresse livraison : </b></td>
				<td class="main"><?php echo tep_address_format($order->delivery['format_id'], $order->delivery, 1, '', '<br />', 'delivery_', $oID); ?></td>
			</tr>
			</table>
		</td>
        <td valign="top"> <!--  TABLEAU CONCERNANT LES COORDONNEES DU CLIENT (telephone fixe, portable, email, paiement)   -->
            <table border="0" cellspacing="0" cellpadding="2" width="100%">
            <tr>
                <td class="main" width="160" align="right"><b>T�l�phone Portable : </b></td>
                <td class="main"><?php echo  $portable_client;?></td>
            </tr>
            <tr>
                <td class="main" width="160" align="right"><b>T�l�phone Fixe : </b></td>
                <td class="main" ><?php 
                    if (strlen($fixe_client)!=0){ echo $fixe_client; }
                    else{ echo "Pas de fixe";}?>
                </td>
            </tr>
            <tr>
                <td class="main" width="160" align="right"><b>Adresse email : </b></td>
                <td class="main"><u><?php echo $email_client; ?></u></td>
            </tr>
            <tr>
                <td class="main" width="160" align="right"><b>M�thode de paiement : </b></td>
                <td class="main"><a href="javascript: updateOrderField('<?php echo $oID; ?>', 'orders', 'payment_method', '<?php echo addslashes($order->info['payment_method']); ?>');" class="ajaxLink"><?php echo $order->info['payment_method']; ?></a></td>
            </tr>
            <tr>
                <!-- D�termination du pays de l'adresse IP de notre client -->
                <?php
					/* extraction adresse IP du visiteur */
                    $ip = $order->customer['ip'];
                    /* transformation IP */
                    $dotted = preg_split( "/[.]+/", $ip);
                    $ip2 = (double) ($dotted[0]*16777216)+($dotted[1]*65536)+($dotted[2]*256)+($dotted[3]);    
                    
					$country_query = tep_db_query("SELECT `country_name` 
												  FROM `ip-to-country` 
												  WHERE $ip2 BETWEEN `ip_from` AND `ip_to`");
					$pays = mysql_fetch_array($country_query);
                ?>
                <td class="main" width="160" align="right"><b>IP de la commande : </b></td>
                <td class="main"><a href="https://en.utrace.de/?query=<?php echo $order->customer['ip']; ?>" style="font-size: 1em;" target="_blank"><?php echo $order->customer['ip'] . "&nbsp;(" . $pays['country_name'] . ")"; ?></a></td>
            </tr>
            <?php if ($order->info['orders_numero_facture'] != '0') {?>           
  			<tr>
    		<td class="main" width="160" align="right"><b>N� de facture : </b></td>
    		<td class="main"><font color="red"><b><?php echo $order->info['orders_numero_facture']; ?></b></font></td>
  			</tr>
            <?php }?>
			</table>
		</td>
		
	</tr>
    </table>
    </td>
  </tr>

<?php 
	$orders_paiement_cb_query = tep_db_query("SELECT orders_paiement_cb_id, orders_id, etat_paiement, numero_cb, paiement_garanti, pays_cb_paiement, reference_spplus, reference_client
												 FROM orders_paiement_cb
												 WHERE orders_id='".$_REQUEST['oID']."'
												 ORDER BY orders_paiement_cb_id ASC
												 "); 
	$res_orders_paiement_cb = tep_db_num_rows($orders_paiement_cb_query);

  	if (($order->info['payment_method']=='Carte Bancaire') && ($res_orders_paiement_cb != '0'))
	{
		 
?>

	<tr>
		<td>
			<table width="100%" border="1" cellspacing="0" cellpadding="2" bgcolor="#66FF99" class="main">
				<tr>
					<td align="center">Num�ro de CB</td>
					<td align="center">Expiration CB</td>
					<td align="center">Pays CB</td>
					<td align="center">Paiement Garantie</td>
					<td align="center">Etat paiement CB</td>
					<td align="center">R�f�rence SPPLUS</td>
					<td align="center">R�f�rence Client</td>
				</tr>
<?php
	while($res_orders_paiement_cb=tep_db_fetch_array($orders_paiement_cb_query)){
		echo '<tr>';
		
			echo '<td align="center">&nbsp;';
						
			echo $res_orders_paiement_cb['numero_cb'];
			echo '</td>';
			
			echo '<td align="center">&nbsp;';
			echo $order->info['cc_expires'];
			echo '</td>';
			
			echo '<td align="center">';
			if ($res_orders_paiement_cb['pays_cb_paiement'] != '') {
			echo $order->info['pays_cb_paiement'];
			} else { echo '&nbsp;'; }
			echo '</td>';
			
			echo '<td align="center">&nbsp;';
			if ($res_orders_paiement_cb['paiement_garanti']=='0') echo '<font color="red"><strong>NON</strong></font>';
			if ($res_orders_paiement_cb['paiement_garanti']=='1') echo '<font color="green"><strong>OUI</strong></font>';
			echo '</td>';
			
			echo '<td align="center">';
			echo '(' . $res_orders_paiement_cb['etat_paiement'] . ')&nbsp;' .spplus_code_etat($res_orders_paiement_cb['etat_paiement']);
			echo '</td>';
			
			echo '<td align="center">';
			echo $res_orders_paiement_cb['reference_spplus'];
			echo '</td>';
			
			echo '<td align="center">';
			echo $res_orders_paiement_cb['reference_client'];
			echo '</td>';	
									
		echo '</tr>';
	} 
?>

			</table>
		</td>
	</tr>
<?php } ?>
	

	<tr><td> <?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td></tr>
	<tr>
		<td><form method="post" name="form_products" action="<?php $_SERVER['PHP_SELF'];?>">
        <input type="hidden" name="action_orders_products" value="ok">
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
    	<tr class="dataTableHeadingRow">	  
			<td class="dataTableHeadingContent" colspan="2" style="text-align: center;">Articles<!--<?php echo sprintf('Produits &nbsp; <a href="javascript: addProduct(\'%s\');">Ajouter un produit</a>', $oID); ?>-->&nbsp;</td>
            <td class="dataTableHeadingContent" style="text-align: center;">Reste &agrave; livrer</td>
            <td class="dataTableHeadingContent" style="text-align: center;">Stock Virtuel</td>
            <td class="dataTableHeadingContent">R�f�rence</td>
            <td class="dataTableHeadingContent" align="right"><!--TVA-->&nbsp;</td>
            <td class="dataTableHeadingContent" align="right">Prix d'achat HT</td>
            <td class="dataTableHeadingContent" align="right">Prix Unit. HT</td>
            <td class="dataTableHeadingContent" align="right">Prix Unit. TTC</td>
            <td class="dataTableHeadingContent" align="right">Marge HT</td>
            <td class="dataTableHeadingContent" align="right">Total HT</td>
            <td class="dataTableHeadingContent" align="right">Total TTC</td>
            <td class="dataTableHeadingContent" align="center">Action</td>
       	</tr>
<?php
    for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
		$suppressions_dans_reference = array("#", "#####");
		$application_suppressions_dans_reference = str_replace($suppressions_dans_reference, "", $order->products[$i]['model']);?>
		<tr class="dataTableRow">
	    	<td class="dataTableContent" valign="top" align="center">
				
                <?php 
				
				//echo '<input type="hidden" name="orders_products_id" value="' . $orders_products_id . '" />';
				echo $order->products[$i]['qty'];
				?>
                &nbsp;<strong>X</strong>
            </td>
            <td class="dataTableContent" valign="top">
            <a href="<?php echo FILENAME_ARTICLE_EDIT;?>?cPath=<?php echo $order->products[$i]['cpath'];?>&pID=<?php echo $order->products[$i]['id'];?>&action=new_product" target="_blank">
			<?php echo $order->products[$i]['name'];?></a>
			<?php
            if (isset($order->products[$i]['attributes']) && (sizeof($order->products[$i]['attributes']) > 0)) {
				
                for ($j = 0, $k = sizeof($order->products[$i]['attributes']); $j < $k; $j++) {
					
                    echo '<br /><nobr><small>&nbsp;<i> - <u>' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
                    if ($order->products[$i]['attributes'][$j]['price'] != '0') echo ' (' . $order->products[$i]['attributes'][$j]['prefix'] . $currencies->format($order->products[$i]['attributes'][$j]['price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . ')';
                    echo '</u></i></small></nobr>';
                }
            }?>
			</td>
            <td class="dataTableContent" valign="top" style="text-align: center;">
            	<?php 
					$relicats_query = tep_db_query("SELECT products_quantity, products_quantity_sent FROM orders_products WHERE orders_products_id = '" . $order->products[$i]['orders_products_id'] . "' AND orders_id = '" . $oID . "'");
					$relicats = tep_db_fetch_array($relicats_query);
					$reste_a_livrer = $relicats['products_quantity'] - $relicats['products_quantity_sent'];
				?>
				<span <?php if ($reste_a_livrer != 0) echo "style='color: red; font-weight: bold;'" ?>><?php echo $reste_a_livrer; ?> Article<?php if ($reste_a_livrer > 1) echo "s"; ?></span>
            </td>
            <td class="dataTableContent" valign="top" style="text-align: center;">
            <?php
				
				$res_stock = 'SELECT pa.options_quantity AS products_quantity FROM '. TABLE_PRODUCTS_ATTRIBUTES .' pa, '. TABLE_ORDERS_PRODUCTS .' op, '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES .' opa
								WHERE pa.products_id=op.products_id
								AND op.orders_products_id=opa.orders_products_id
								AND opa.options_values_id=pa.options_values_id
								AND op.orders_products_id='. $order->products[$i]['orders_products_id'];
				$res_stock = tep_db_query($res_stock);
				
				if(tep_db_num_rows($res_stock) == 0) {
					
					$res_stock = 'SELECT products_quantity FROM products WHERE products_id='. $order->products[$i]['id'];
					$res_stock = tep_db_query($res_stock);
				}
				
				$row_stock=tep_db_fetch_array($res_stock);
				echo $row_stock['products_quantity'];
			?>
            </td>
      		<td class="dataTableContent">
                <u><?php echo $order->products[$i]['model'];?></u>
            </td>
		    <td class="dataTableContent" align="right" valign="top"><!--<?php echo tep_display_tax_value($order->products[$i]['tax']);?>%-->&nbsp;</td>
            <td class="dataTableContent" align="right" valign="top">
            	<?php echo $currencies->format($order->products[$i]['cost'], true, $order->info['currency'], $order->info['currency_value']);?>
            </td>
            <td class="dataTableContent" align="right" valign="top">
            <?php $prix=number_format($order->products[$i]['final_price'],2);
				echo $prix;
			?>
            </td>
		    <td class="dataTableContent" align="right" valign="top">
            	<b><u><?php echo $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']), true, $order->info['currency'], $order->info['currency_value']);?></u></b>
            </td>
		    <td class="dataTableContent" align="right" valign="top"><b><?php 
			
			if($order->products[$i]['final_price']<0) {
				
				echo $currencies->format(($order->products[$i]['final_price'] * $order->products[$i]['qty'] + $order->products[$i]['cost'] * $order->products[$i]['qty']), true, $order->info['currency'], $order->info['currency_value']);
			}
			else {
				
				echo $currencies->format(($order->products[$i]['final_price'] * $order->products[$i]['qty'] - $order->products[$i]['cost'] * $order->products[$i]['qty']), true, $order->info['currency'], $order->info['currency_value']);
			}
			?>
            </b></td>
            <td class="dataTableContent" align="right" valign="top"><b><?php echo $currencies->format($order->products[$i]['final_price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']);?></b></td>
            <td class="dataTableContent" align="right" valign="top"><b><?php echo $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']) * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']);?></b></td>
            <td class="dataTableContent" align="center" valign="top">
            	<img border="0" alt="Editer" src="images/icons/editer.png"
					<?php echo /*($order->info['orders_status']!=4) ?*/ 
					'id="edit_'. $order->products[$i]['orders_products_id'] .'" class="edit"' 
					//:'onclick="alert(\'Cette commande a �t� envoy�, vous ne pouvez donc pas changer les quantit�s des produits\')"'
					;
					//echo 'id="edit_'. $order->products[$i]['orders_products_id'] .'" class="edit"';
					?>></td>
      	</tr>
<?php }?>
        <tr>
            <td align="center" colspan="7" valign="top"><input type="hidden" value="Modifier les prix" /></td>
            <td align="right" colspan="5">
            <table border="0" cellspacing="0" cellpadding="2">
				<tr>
                    <td align="right" class="smallText">Sous Total HT : </td>
                    <td align="right" class="smallText"><?php echo $order->info['ss_total']; ?> � (<?php echo number_format($order->info['ss_total']*1.2,2); ?> � TTC)</td>
                </tr>
                <tr>
                    <td align="right" class="smallText">Frais de port HT : </td>
                    <td align="right" class="smallText"><?php 	echo $order->info['frais_port_client']; ?> � (<?php echo $order->info['frais_port_client']+$order->info['tva_port']; ?> � TTC)</td>
                </tr>
                <tr>
                    <td align="right" class="smallText">Dont TVA Fran�aise : </td>
                    <td align="right" class="smallText"><?php echo $order->info['tva_total']; ?> � </td>
                </tr>
                <?php if($order->info['remise']!=0){?>
                <tr>
                    <td align="right" class="smallText">Remise HT : </td>
                    <td align="right" class="smallText"><?php echo $order->info['remise']; ?> � ( <?php echo number_format($order->info['remise']*1.2,2); ?> � TTC)</td>
                </tr>
                <?php }?>
                <?php if($order->info['remise_pourcent']!=0){?>
                <tr>
                    <td align="right" class="smallText">Remise Pourcentage HT : </td>
                    <td align="right" class="smallText"><?php echo $order->info['remise_pourcent']; ?> %</td>
                </tr>
                <?php }?>
                <?php if($order->info['remise_porte_monnaie']!=0){?>
                <tr>
                    <td align="right" class="smallText">Remise Porte Monnaie HT : </td>
                    <td align="right" class="smallText"><?php echo $order->info['remise_porte_monnaie']; ?> � ( <?php $avec_tva = correction_tva($order->info['remise_porte_monnaie']*1.2); echo $avec_tva['arrondie']; ?> � TTC)</td>
                </tr>
                <?php }?>
                <tr>
                    <td align="right" class="smallText">Marge : </td>
                    <td align="right" class="smallText"><?php echo $order->info['marge']; ?> �</td>
                </tr>
                <tr>
                    <td align="right" class="smallText">Total TTC : </td>
                    <?php 
					$total_final = $order->info['total'];
					?>
                    <td align="right" class="smallText"><?php echo $total_final; ?> �</td>
                </tr>
                <tr>
                    <td align="right" class="smallText"></td>
                    <td align="right" class="smallText"><br ></td>
                </tr>
            </table></td>
            <td align="center" valign="middle"><img border="0" alt="Editer" src="images/icons/editer.png" class="edit_order"></td>
        </tr>
       </table>
        </form>
	</td>
  </tr>
  <tr>
	<td>
        <table border="0">
        <tr>
			<td class="smallText" bgcolor="lightblue"><b>&nbsp;&nbsp;email&nbsp;&nbsp;</b></td>
			<td class="smallText" bgcolor="#FFE4C4"><b>&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
		</tr>
		</table>
	</td>
  </tr>
  <tr>
  	<td class="main">
    	<table border="1" cellspacing="0" cellpadding="5">
        <tr>
            <td class="smallText" align="center" bgcolor="#cccccc" ><b>Date</b></td>
            <td class="smallText" align="center" bgcolor="#cccccc"><b>Statut de la commande</b></td>
            <td class="smallText" align="center" bgcolor="#cccccc" width="500px"><b>Commentaire</b></td>
            <td class="smallText" align="center" bgcolor="#cccccc"><b>Num�ro de suivi</b></td>	
			<td class="smallText" align="center" bgcolor="#cccccc"><b>Action</b></td>			
        </tr>
<?php
    // requete pour recuperer les statuts concernants la commande
    $orders_status_max_query = tep_db_query("select MAX(orders_status_history_id) as max from ".TABLE_ORDERS_STATUS_HISTORY." where orders_id = '".tep_db_input($oID)."' GROUP BY (orders_id)");
	$orders_status_max = tep_db_fetch_array($orders_status_max_query);
	
    $orders_history_query = tep_db_query("SELECT * FROM ".TABLE_ORDERS_STATUS_HISTORY." WHERE orders_id = '".tep_db_input($oID)."' ORDER BY date_added");
    if (tep_db_num_rows($orders_history_query)) {
      	while ($orders_history = tep_db_fetch_array($orders_history_query)) {?>
        	<tr>
            	<td class="smallText" align="center"><?php echo tep_datetime_short($orders_history['date_added']);?></td>
				<?php
                // Contenu des message par champs pour stocker le mail
                $email_client=nl2br(tep_db_output($orders_history['comments']));
                ?>
                <td class="smallText"><?php echo $orders_status_array_2[$orders_history['orders_status_id']];?></td> 
              	<td class="smallText">
			 		<table border="0" width="100%">
					<?php // Si pas de message par EMAIL
					if ((strlen($email_client))==0) { ?>
						<tr><td>&nbsp;</td></tr>
			<?php	}	
					// Contenu des message par EMAIL
					if (strlen($email_client)>0) { ?>
						<tr><td class="smallText" style="width:100%; background-color:lightblue"><?php echo $email_client;?></td></tr>
			<?php	} ?>
					</table>
				</td>
			<?php
            // SUIVI COLLISSIMO+UPS
            $tracknum=nl2br(tep_output_string_protected(nl2br(tep_db_output($orders_history['track_num']))));
            $tracknum2=nl2br(tep_output_string_protected(nl2br(tep_db_output($orders_history['track_num2']))));
            $tracknum3=nl2br(tep_output_string_protected(nl2br(tep_db_output($orders_history['track_num3']))));
			$tracknum4=nl2br(tep_output_string_protected(nl2br(tep_db_output($orders_history['track_num4']))));$tracknum5=nl2br(tep_output_string_protected(nl2br(tep_db_output($orders_history['track_num5']))));
            echo '<td class="smallText">'.
			( !empty( $orders_history['track_num'] ) ? '<a href="https://www.coliposte.net/particulier/suivi_particulier.jsp?colispart='.$tracknum.'" target="_blank">'.$tracknum.'</a>' : '').
			( !empty( $orders_history['track_num2'] ) ? '<a href="https://www.csuivi.courrier.laposte.fr/default.asp?EZ_ACTION=rechercheRapide&tousObj=&numObjet='.$tracknum2.'" target="_blank">'.$tracknum2.'</a>' : '').
			( !empty( $orders_history['track_num3'] ) ? '<a href="https://www.fr.chronopost.com/fr/tracking/result?listeNumeros='.$tracknum3.'" target="_blank">'.$tracknum3.'</a>' : '').
			( !empty( $orders_history['track_num4'] ) ? '<a href="https://www.dpd.fr/traces_exd_'.$tracknum4.'" target="_blank">'.$tracknum4.'</a>' : '').
			( !empty( $orders_history['track_num5'] ) ? '<a href="https://www.ups.com/WebTracking?loc=fr_fr&requester=ST&trackingNumber='.$tracknum5.'" target="_blank">'.$tracknum5.'</a>' : '').
                '</td>' . "\n";
            // FIN SUIVI COLLISSIMO+UPS
			?>
				<td class="smallText" align="center">
                	<?php 
						if ($orders_history['orders_status_history_id'] != $orders_status_max['max']) { ?>
                			<a href="<?php echo tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('oID', 'action')).'oID='.$orders_history['orders_id'].'&oSHID='.$orders_history['orders_status_history_id'].'&action=deleteInfo');?>" onClick="if(window.confirm('Etes vous sur de vouloir effacer les donn�es ?')){return true;} else {return false;}"><?php echo tep_image(DIR_WS_IMAGES.'/icons/supprimer.png', "Supprimer cette ligne");?></a>
						<?php } else {
							echo tep_image(DIR_WS_IMAGES.'/icons/supprimer_inactif.png', "Impossible de supprimer");
						}
					?>
				</td>
            </tr>
<?php   }
	} 
	else {?>
    	<tr><td class="smallText" colspan="5">Aucun historique de commande disponible</td></tr>
<?php }?>
    </table></td></tr>
    <?php if($order->info['orders_date_fin_devis']!=NULL && $order->info['orders_date_fin_devis']!='0000-00-00 00:00:00') {
		
		$date_fin_devis = explode(' ', $order->info['orders_date_fin_devis']);
		$date_fin_devis = $date_fin_devis[0];
		?>
    <tr>
		<td colspan="3"><br /><?php echo tep_draw_separator(); ?></td>
	</tr>
    <tr>
		<td colspan="3"><br/>
		<form action="<?php echo FILENAME_CMD_EDIT;?>?oID=<?php echo $_GET['oID'];?>&action=update_date_fin_devis" method="post">
        	<div>Date de fin de validit� du devis : 
            	<input type="text" name="validity_end" value="<?php echo $date_fin_devis; ?>">
            	<input type="submit" value="Modifier">
            </div>
        </form>
        </td>
	</tr>
    <?php } ?>
    <tr>
		<td colspan="3"><br /><?php echo tep_draw_separator(); ?></td>
	</tr>
	<tr>
		
		<td class="main">
			<?php echo tep_draw_form('formulaire', FILENAME_CMD_EDIT, tep_get_all_get_params(array('action')) . 'action=update_order'); ?>
			<br /><b>Changer le Statut : </b> 
			<?php
			
			$devis = estDevis($_GET['oID']);
			if($devis) {
				$orders_statuses_devis[] = array('id' => 11, 'text' => 'Devis annul�');
				echo tep_draw_pull_down_menu('status', $orders_statuses_devis, $order->info['orders_status'], 'onChange=Charger_statut(this.options[this.selectedIndex].value)');
			} else {
				echo tep_draw_pull_down_menu('status', $orders_statuses, $order->info['orders_status'], 'onChange=Charger_statut(this.options[this.selectedIndex].value)');
			}
			?>
			
			<br><br>
			
			<table border="0" cellspacing="0">        
				<tr>
					<td align="left" class="dataTableHeadingRow">
						<input name="notify" type="checkbox" value="on" checked />&nbsp;<b>Commentaire : </b>
					</td>
					<td width="20">&nbsp;</td>
					<td align="right" bgcolor="#cccccc" class="main"><b>&nbsp;&nbsp;&nbsp;Indiquez un Num�ro de suivi&nbsp;&nbsp;&nbsp;</b></td>    
				</tr>     
				<tr>
					<td align=left valign=top>
						<?php 		
						// champs texte pour la modification du statut 
						echo tep_draw_textarea_field('comments', 'soft', '60', '5'); 
						// ENVOI D'UN EMAIL AVEC LE NOUVEAU STATUT DE LA COMMANDE
						// CHAMPS CACHE POUR ENVOYER LE NUMERO DU COLIS
						echo '<input type="hidden" name="notify_tracking" value="on">';
						?>							
					</td>
					<td width="20">&nbsp;</td>
					<td valign="top">
						<?php // SUIVI COLIS ?> 
						<table border="0">        
							<tr>
								<td width="150" class="main"><b>Envoi par Colissimo</b></td>
								<td><?php echo tep_draw_input_field('track_num', '', 'size=/"20/"'); ?></td>
							</tr>     
							<tr>
								<td class="main"><b>Envoi par Lettre Suivie</b></td>
								<td><?php echo tep_draw_input_field('track_num2', '', 'size=/"20/"'); ?></td>
							</tr>
							<tr>
								<td class="main"><b>Envoi par Chronopost</b></td>
								<td><?php echo tep_draw_input_field('track_num3', '', 'size=/"20/"'); ?></td>
							</tr>
							<tr>
								<td class="main"><b>Envoi par DPD</b></td>
								<td><?php echo tep_draw_input_field('track_num4', '', 'size=/"20/"'); ?></td>
							</tr>
							<tr>
								<td class="main"><b>Envoi par UPS</b></td>
								<td><?php echo tep_draw_input_field('track_num5', '', 'size=/"20/"'); ?></td>
							</tr>
						</table>
						<?php // FIN SUIVI COLIS ?>
					</td>
				</tr>     
				<tr>
					<td align='right' >
						<button type="button" onClick="Effacer_champ('commentaire')"><img src="images/icons/supprimer.png"></button>
					</td>
					<td></td>
					<td align="center" valign="center">
                    
					</td>
				</tr>
				<tr><td valign="top"><?php echo tep_image_submit('button_update.gif', IMAGE_UPDATE); ?></td></tr>
			</table>
		</form>
		
		
		
		
        </td>
    </tr>
    </table>
  	</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
		<?php         
		echo '<input type="button" value="Cr&eacute;diter/Voir porte-monnaie virtuel" id="showPorteMonnaie"/>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" value="Bon de Livraison" onclick="window.open(\'cmd_bon_de_livraison.php?oID='.$HTTP_GET_VARS['oID'].'\');"/>
		<input type="button" value="Voir les articles de la commande" onclick="window.open(\'cmd_list_products_cmd.php?commandes='.$HTTP_GET_VARS['oID'].' &action=articles_commandes\');"/>
		<input type="button" value="Mettre &agrave; jour le num&eacute;ro colissimo" onclick="window.open(\'cmd_a_livrer.php?commandes= '.$HTTP_GET_VARS['oID'].'&action=suivi_colissimo\');"/>
		<input type="button" value="Facture" onclick="window.open(\'/'.DIR_WS_SITE.'/compte/generation_pdf/facture.php?id_com='.$HTTP_GET_VARS['oID'].'&cli='.md5($order->customer['id']).'\');"/>';
		
		if ($order->info['orders_numero_facture'] != '0') {
        	echo '&nbsp;<a href="cmd_avoir.php?oID='. $_GET['oID'] .'"><input type="button" value="Faire un avoir" /></a>';
		}
		?>
	</td>
  </tr>
	<tr>
		<td colspan="2" align="center">
		<br />
		<?php 
		$query_order_referer = tep_db_query("SELECT referer, referer_host, visiteur_payant, visiteur_mot_cle FROM " . TABLE_ORDERS . " WHERE orders_id = '" . (int)$oID . "'");
        $res_order_referer = tep_db_fetch_array($query_order_referer);
		?>
		URL d'ariv�e : <a href="<?php echo $res_order_referer['referer']; ?>" target="_blank"><?php echo $res_order_referer['referer']; ?></a>&nbsp;&nbsp;-&nbsp;&nbsp;
		Site d'arriv�e : <?php echo $res_order_referer['referer_host']; ?>&nbsp;&nbsp;-&nbsp;&nbsp;
		Mot Cl� : <?php echo $res_order_referer['visiteur_mot_cle']; ?>&nbsp;&nbsp;-&nbsp;&nbsp;
		<strong>Lien <?php if ($res_order_referer['visiteur_payant']==0){ echo 'GRATUIT'; }else{ echo 'PAYANT'; }?></strong>
		</td>
	</tr>
  </table></td>
  <?php }?>
  </tr>
</table>
<div id="overlay"></div>
<div id="lightbox"></div>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>