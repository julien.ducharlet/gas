<?php
require('includes/application_top.php');

if(isset($HTTP_GET_VARS['action'])){
	$action = $HTTP_GET_VARS['action'] ;
}
else{
	tep_redirect(tep_href_link(FILENAME_RAYON, tep_get_all_get_params(array('rID', 'action'))));
}
$rayon_id = tep_db_prepare_input($HTTP_GET_VARS['rID']);
        
if (tep_not_null($action)) {
	switch ($action) {
      case 'update':
	    $rayon_status_tous=$HTTP_POST_VARS['rayon_status_tous'];
		if($rayon_status_tous==""){$rayon_status_tous=0;}
		$rayon_status_client=$HTTP_POST_VARS['rayon_status_client'];
		if($rayon_status_client==""){$rayon_status_client=0;}
		$rayon_status_rev=$HTTP_POST_VARS['rayon_status_rev'];
		if($rayon_status_rev==""){$rayon_status_rev=0;}
		$rayon_status_pro=$HTTP_POST_VARS['rayon_status_pro'];
		if($rayon_status_pro==""){$rayon_status_pro=0;}
		$rayon_status_adm=$HTTP_POST_VARS['rayon_status_adm'];
		if($rayon_status_adm==""){$rayon_status_adm=0;}
		$rayon_status_vip=$HTTP_POST_VARS['rayon_status_vip'];
		if($rayon_status_vip==""){$rayon_status_vip=0;}
		
		$sql_data_array = array('rubrique_name' => tep_db_prepare_input($HTTP_POST_VARS['rubrique_name']), 'rubrique_ordre' => tep_db_prepare_input($HTTP_POST_VARS['rubrique_ordre']),
								'rayon_status_tous' => $rayon_status_tous, 
								'rayon_status_adm' => $rayon_status_adm, 'rayon_status_rev' => $rayon_status_rev, 'rayon_status_client' => $rayon_status_client,
								'rayon_status_pro' => $rayon_status_pro, 'rayon_status_vip' => $rayon_status_vip, 'rubrique_htc_title_tag' => tep_db_prepare_input($HTTP_POST_VARS['rubrique_htc_title_tag']),
								'rubrique_htc_desc_tag' => tep_db_prepare_input($HTTP_POST_VARS['rubrique_htc_desc_tag']), 'rubrique_htc_keywords_tag' => tep_db_prepare_input($HTTP_POST_VARS['rubrique_htc_keywords_tag']),
								'rubrique_url' => tep_db_prepare_input($HTTP_POST_VARS['rubrique_url']), 'rubrique_baseline' => tep_db_prepare_input($HTTP_POST_VARS['rubrique_baseline'])
								);
		tep_db_perform(TABLE_RAYON, $sql_data_array, 'update', "rubrique_id = '" . (int)$rayon_id . "'");
		
        tep_redirect(tep_href_link(FILENAME_RAYON, tep_get_all_get_params(array('rID', 'action')) . 'rID=' . $rayon_id));

      	break;
      default:
    }
}?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<style>

input[type="text"], textarea {width:80%}

</style>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  if ($action == 'edit' || $action == 'update') {
  $rayon_query="select * from ".TABLE_RAYON." where rubrique_id='".$rayon_id."'";
  $res_rayon=tep_db_query($rayon_query);
  $row_rayon=tep_db_fetch_array($res_rayon);
  ?>
      <tr><?php echo tep_draw_form('rayon', FILENAME_RAYON_EDIT, tep_get_all_get_params(array('action')) . 'action=update', 'post', 'onSubmit="return check_form();"'); ?>
        <td class="formAreaTitle">Informations</td>
      </tr>
      <tr>
        <td class="main">Nom</td>
        <td class="main"><?php echo tep_draw_input_field('rubrique_name', $row_rayon["rubrique_name"], 'maxlength="32"') . '&nbsp;';?></td>
      </tr>
      <tr>
        <td class="main">Ordre</td>
        <td class="main"><?php echo tep_draw_input_field('rubrique_ordre', $row_rayon["rubrique_ordre"], 'style="width:20px"') . '&nbsp;';?></td>
      </tr>
      <tr>
      	<td class="main">Autorisation</td>
        <td class="main"><?php echo '
        	Non logu� :'.  tep_draw_checkbox_field('rayon_status_tous','1',$row_rayon["rayon_status_tous"]==1).'&nbsp;&nbsp;
			Client :'.  tep_draw_checkbox_field('rayon_status_client','1',$row_rayon["rayon_status_client"]==1).'&nbsp;&nbsp;
			Revendeur :'.  tep_draw_checkbox_field('rayon_status_rev','1',$row_rayon["rayon_status_rev"]==1).'&nbsp;&nbsp;
			Pro :' . tep_draw_checkbox_field('rayon_status_pro','1',$row_rayon["rayon_status_pro"]==1) . '&nbsp;&nbsp;
			Administration :' . tep_draw_checkbox_field('rayon_status_adm','1',$row_rayon["rayon_status_adm"]==1) . '&nbsp;&nbsp;
			VIP :'.  tep_draw_checkbox_field('rayon_status_vip','1',$row_rayon["rayon_status_vip"]==1).'<br /><br />';?>
	  	</td>
      </tr>
      <tr>
        <td class="main">Meta title</td>
        <td class="main"><?php echo tep_draw_input_field('rubrique_htc_title_tag', $row_rayon["rubrique_htc_title_tag"], 'maxlength="32" width="50%"') . '&nbsp;';?></td>
      </tr>
      <tr>
        <td class="main">Meta Description</td>
        <td class="main"><?php echo tep_draw_input_field('rubrique_htc_desc_tag', $row_rayon["rubrique_htc_desc_tag"], 'maxlength="32"') . '&nbsp;';?></td>
      </tr>
      <tr>
        <td class="main">Meta keywords</td>
        <td class="main"><?php echo tep_draw_input_field('rubrique_htc_keywords_tag', $row_rayon["rubrique_htc_keywords_tag"], 'maxlength="32"') . '&nbsp;';?></td>
      </tr>
      <tr>
        <td class="main">Url</td>
        <td class="main"><?php echo tep_draw_input_field('rubrique_url', $row_rayon["rubrique_url"], 'maxlength="32"') . '&nbsp;';?></td>
      </tr>
      <tr>
        <td class="main">Baseline</td>
        <td class="main"><?php echo tep_draw_textarea_field('rubrique_baseline', 'soft', '', '10', $row_rayon["rubrique_baseline"]); ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
  	<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
  </tr>
  <tr>
    <td align="right" class="main"><?php echo tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link(FILENAME_RAYON_EDIT, tep_get_all_get_params(array('action'))) .'">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
  </tr></form>
<?php }?>
    </form></table></td>
  </tr>
</table>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>