<?php
  require('includes/application_top.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<br /><br />
<table border="0" width="100%" cellspacing="2" cellpadding="2">
	<tr><td width="100%" valign="top" align="center"><h2>Liste des pages lanc�es chaque nuit en tache cron</h2></td></tr>
  	<tr><td width="100%" valign="top" align="center"><a href="cron/update_commande_client.php">Page mise � jour des commandes de client</a></td></tr>
    <tr><td width="100%" valign="top" align="center"><a href="cron/relance_porte-monnaie_virtuel.php">Page pour relancer les clients ayant un porte-monnaie virtuel</a></td></tr>
    <tr><td width="100%" valign="top" align="center"><a href="cron/update_date_dispo.php">Page de mise � jour des articles dont la date de disponibilit� est inf�rieure � Aujourd'hui</a></td></tr>
    <tr><td width="100%" valign="top" align="center"><a href="cron/update_commande_client.php">Ins�rer dans la table CUSTOMERS toutes les commandes du client</a></td></tr>
    <tr><td width="100%" valign="top" align="center"><a href="cron/send_previous_command.php">Relancer les clients pour les paiemments non-effectu�s</a></td></tr>
    <tr><td width="100%" valign="top" align="center"><a href="cron/send_lost_basket.php">Relacer les paniers perdus</a></td></tr>
</table>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>