<?php
require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
include(DIR_WS_CLASSES . 'order.php');


// Action a faire selon le choix que l'utilisateur souhaite faire dans la page des commandes
$action = (isset($HTTP_POST_VARS['Action_orders']) ? $HTTP_POST_VARS['Action_orders'] : '');

if (tep_not_null($action)) {

	if (isset($HTTP_POST_VARS['batch_order_numbers'])) {
		
		foreach($HTTP_POST_VARS['batch_order_numbers'] as $order_number => $print_order) {
			$batch_order_numbers[] = $order_number;
			$liste_commandes=$liste_commandes." ".$order_number;
			$liste_commandes_inverse=$order_number." ".$liste_commandes_inverse;
		}
		
		switch ($action) {
			case 'pavisverifies3j': 
			$pageProduitsTrustpilot='Avis_Avis-Verifies_Produits_3j.php?commandes='.$liste_commandes_inverse."&action=AvisVerifies3j";
			tep_redirect(tep_href_link($pageProduitsTrustpilot));
			break;
			
			case 'pavisverifies10j': 
			$pageProduitsTrustpilot='Avis_Avis-Verifies_Produits_10j.php?commandes='.$liste_commandes_inverse."&action=AvisVerifies10j";
			tep_redirect(tep_href_link($pageProduitsTrustpilot));
			break;
							
			case 'Liste des articles des cmd': 
			$pageArticlesCommandes='cmd_list_products_cmd.php?commandes='.$liste_commandes_inverse."&action=articles_commandes";
			tep_redirect(tep_href_link($pageArticlesCommandes));
			break;
			
			case 'Liste des articles des cmd fournisseur': 
			$pageArticlesCommandes='products_to_order.php?export=fournisseur&commandes='.$liste_commandes_inverse."&action=articles_commandes";
			tep_redirect(tep_href_link($pageArticlesCommandes));
			break;
		
			case 'MAJ numero colissimo': 
			case 'MAJ numero colissimo':  
			$pageSuiviColissimo='cmd_a_livrer.php?commandes='.$liste_commandes."&action=suivi_colissimo";
			tep_redirect(tep_href_link($pageSuiviColissimo));
			break;		
				
			case 'MAJ numero distingo':  
			$pageSuiviDistingo='cmd_a_livrer.php?commandes='.$liste_commandes."&action=suivi_distingo";
			tep_redirect(tep_href_link($pageSuiviDistingo));
			break;
		
			case 'MAJ numero chronopost':  
			$pageSuiviChronopost='cmd_a_livrer.php?commandes='.$liste_commandes."&action=suivi_chronopost";
			tep_redirect(tep_href_link($pageSuiviChronopost));
			break;
		}
	}
}

$target_file = $HTTP_POST_VARS['target_file'];

unset($batch_order_numbers);
if ($_SESSION['commandes']) {
	$batch_order_numbers = $_SESSION['commandes'];
	$action = 'expeditor';
}

if($HTTP_POST_VARS['batch_order_numbers']){
	foreach($HTTP_POST_VARS['batch_order_numbers'] as $order_number => $print_order) {
		$batch_order_numbers[] = $order_number;
	}
}

/*echo "<pre>";
print_r($_SESSION);
print_r($batch_order_numbers);
echo "</pre>";*/
  
// Si aucune commande n'est s�lectionn� on affiche un message d'erreur
if (!(is_array($batch_order_numbers))){
	exit('Erreur : Vous n\'avez pas s�lectionn� de commande !');
}
	 
sort($batch_order_numbers);
$number_of_orders = sizeof ($batch_order_numbers);

if($target_file == 'labels') {
	header ("Content-type: application/csv\nContent-Disposition: \"inline; filename=customers.csv\"");
	echo "Nom,Societe,Adresse_1,Adresse_2,Adresse_3,Adresse_4,City,State,Postcode,Country \n";

	foreach ($batch_order_numbers as $order_number) {
		$oID = tep_db_prepare_input($order_number);
		$orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
		$order = new order($oID);
		echo "" . $order->customer['name'] . "," . $order->customer['company'] . "," . $order->customer['street_address'] . "," . $order->customer['suburb'] . "," . $order->customer['city'] . "," . $order->customer['state'] . "," . $order->customer['postcode'] . "," . $order->customer['country'] . "\n";
	}
	
} else {
?>

	<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html <?php echo HTML_PARAMS; ?>>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
	<title><?php echo TITLE . $autostatus; ?></title>
	<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
	</head>
	<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

	<?php

	switch($action) {
		case 'dymo': $formAction = 'suivi_distingo';
		break;
		case 'expeditor': $formAction = 'suivi_colissimo';
		break;
	}

	if ($action=="dymo" || $action=="expeditor") { ?>
		
		<br />
		<?php
		$firstinvoice = true;
		
		$commandes_colissimo .= implode(' ', $batch_order_numbers);
		
		if($action=='expeditor') {
			
			?><b><u>Format export vers Colissimo: </u></b><br /><?php
			foreach ($batch_order_numbers as $order_number) {
				if($firstinvoice) {
					echo '<br style="page-break-before:always;">';
					$firstinvoice = false;
				}
				$oID = $order_number;
				include('OSC_Expeditor.php');
			}
			$firstinvoice = true;
			
			?><br><br><b><u>Format export vers Dymo : </u></b><br /><?php
			foreach ($batch_order_numbers as $order_number) {
				if($firstinvoice) {
					echo '<br style="page-break-before:always;">';
					$firstinvoice = false;
				}
				$oID = $order_number;
				include('OSC_DISTINGO.php');
			}
		}
		elseif($action=='dymo') { ?>
		<b><u>Format export vers Dymo : </u></b><br />
			<?php	
			foreach ($batch_order_numbers as $order_number) {
				if($firstinvoice) {
					echo '<br style="page-break-before:always;">';
					$firstinvoice = false;
				}
				$oID = $order_number;
				include('OSC_DISTINGO.php');
			}
		}
		
		unset($_SESSION['commandes']);		
		?>
		<br />
		<br />
		<input type="button" value="Mise � jour des num�ros colissimo" onClick="javascript:location.href='cmd_a_livrer.php?commandes=<?php echo $commandes_colissimo; ?>&action=<?php echo $formAction;?>'" />
		<?php
	} //fin export ?>
	</body>
	</html>
<?php
}
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>