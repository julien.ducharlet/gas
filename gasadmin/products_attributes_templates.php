<?php
/*  */

require('includes/application_top.php');
$languages = tep_get_languages();
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<!-- options and values//-->
	<tr>
	
        <td width="100%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" width="50%">
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td class="pageHeading">&nbsp;Affichage des templates des options&nbsp;</td>
								<td>&nbsp;<?php echo tep_image(DIR_WS_IMAGES . 'pixel_trans.gif', '', '1', '53'); ?>&nbsp;</td>
							</tr>     
						</table>
						<?php
						
						$am_template = tep_db_query("SELECT * FROM am_templates ORDER BY template_name");
    
						while ($result_am_template = tep_db_fetch_array($am_template)) {							
							echo '<font size="5">Template : <strong>' . $result_am_template['template_name'] .' </font> <font size="2">('. $result_am_template['template_id'] . ')</font></strong><br><br>'; 
							
							$am_attributes_to_templates = tep_db_query("SELECT options_id, option_values_id
																		FROM am_attributes_to_templates 
																		WHERE template_id = ".$result_am_template['template_id']."
																		ORDER BY products_options_sort_order");
																		
							
							/*while ($result_am_attributes_to_templates = tep_db_fetch_array($am_attributes_to_templates)) {	
								
								$products_options = tep_db_query("
								SELECT * 
								FROM products_options 
								WHERE products_options_id = ".$result_am_attributes_to_templates['options_id']."");
								$array_products_options = tep_db_fetch_array($products_options);
								
								$products_options_values = tep_db_query("
								SELECT * 
								FROM products_options_values 
								WHERE products_options_values_id = ".$result_am_attributes_to_templates['option_values_id']." ORDER BY products_options_values_name");
								$array_products_options_values = tep_db_fetch_array($products_options_values);
							
							
							
								echo '<font size="4">' . $array_products_options['products_options_name'] .' </font><font size="2">('. $array_products_options['products_options_id'] . ')</font>';
								echo ' --- <font size="4">' . $array_products_options_values['products_options_values_name'] .' </font> <font size="2">(' .$array_products_options_values['products_options_values_id'] . ')</font><br>';
							
							}*/
							
							while ($result_am_attributes_to_templates = tep_db_fetch_array($am_attributes_to_templates)) {	
								
								$requete = tep_db_query("
								SELECT  
									po.products_options_id,
									po.products_options_name,
									pov.products_options_values_id,
									pov.products_options_values_name
								FROM 
									products_options AS po, 
									products_options_values AS pov 
								WHERE 
									pov.products_options_values_id = ".$result_am_attributes_to_templates['option_values_id']." 
									AND
									po.products_options_id = ".$result_am_attributes_to_templates['options_id']." 
								ORDER BY 
									pov.products_options_values_name DESC
								");
								$array_requete = tep_db_fetch_array($requete);						
							
								echo '<font size="4">' . $array_requete['products_options_name'] .' </font><font size="2">('. $array_requete['products_options_id'] . ')</font>';
								echo ' --- <font size="4">' . $array_requete['products_options_values_name'] .' </font> <font size="2">(' .$array_requete['products_options_values_id'] . ')</font><br>';
							}
							echo '<br>';
						}
							
						?>						
					</td>
				</tr>
			</table>
		</td>

	</tr> 
<!-- products_attributes //-->  
</table>
<!-- body_text_eof //-->
<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
