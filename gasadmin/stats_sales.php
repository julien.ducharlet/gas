<?php

/*
Page : stats_sales.php
*/


  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  if ($HTTP_GET_VARS['month'] == '') {
    $month = date("m");
    $year = '20' . date("y");
  } else {
    $month = $HTTP_GET_VARS['month'];
    $year = $HTTP_GET_VARS['year'];
  }
  
  $months = array();
  $months[] = array('id' => 1, 'text' => 'Janvier');
  $months[] = array('id' => 2, 'text' => 'Fevrier');
  $months[] = array('id' => 3, 'text' => 'Mars');
  $months[] = array('id' => 4, 'text' => 'Avril');
  $months[] = array('id' => 5, 'text' => 'Mai');
  $months[] = array('id' => 6, 'text' => 'Juin');
  $months[] = array('id' => 7, 'text' => 'Juillet');
  $months[] = array('id' => 8, 'text' => 'Aout');
  $months[] = array('id' => 9, 'text' => 'Septembre');
  $months[] = array('id' => 10, 'text' => 'Octobre');
  $months[] = array('id' => 11, 'text' => 'Novembre');
  $months[] = array('id' => 12, 'text' => 'Decembre');

  $years = array();

  $years[] = array('id' => 2004, 'text' => '2004');
  $years[] = array('id' => 2005, 'text' => '2005');
  $years[] = array('id' => 2006, 'text' => '2006');
  $years[] = array('id' => 2007, 'text' => '2007');
  $years[] = array('id' => 2008, 'text' => '2008');
  $years[] = array('id' => 2009, 'text' => '2009');
  $years[] = array('id' => 2010, 'text' => '2010');
  $years[] = array('id' => 2011, 'text' => '2011');

  $status = (int)$HTTP_GET_VARS['status'];

  $statuses_query = tep_db_query("select * from orders_status where language_id = $languages_id order by orders_status_name");
  $statuses = array();
  $statuses[] = array('id' => 0, 'text' => 'Afficher tous les statuts');
  while ($st = tep_db_fetch_array($statuses_query)) {
     $statuses[] = array('id' => $st['orders_status_id'], 'text' => $st['orders_status_name']);
  }

  if ($status != 0)  {
    $os = " and o.orders_status = " . $status . " ";
  } else {
    $os = '';
  }

  if ($HTTP_GET_VARS['by']=='product') {
    $sales_products_query = tep_db_query("select sum(op.final_price*op.products_quantity) as daily_prod, sum(op.products_cost*op.products_quantity) as daily_cost, sum(op.final_price*op.products_quantity*(1+op.products_tax/100)) as withtax, o.date_purchased, op.products_name, sum(op.products_quantity) as qty, op.products_model from orders as o, orders_products as op where o.orders_id = op.orders_id and month(o.date_purchased) = " . $month . " and year(o.date_purchased) = " . $year . $os . " GROUP by products_id ORDER BY products_model");  
  } else
    $sales_products_query = tep_db_query("select sum(op.final_price*op.products_quantity) as daily_prod, sum(op.products_cost*op.products_quantity) as daily_cost, sum(op.final_price*op.products_quantity*(1+op.products_tax/100)) as withtax, o.date_purchased, op.products_name, op.products_quantity as qty, op.products_model from orders as o, orders_products as op where o.orders_id = op.orders_id and month(o.date_purchased) = " . $month . " and year(o.date_purchased) = " . $year . $os . " GROUP by date_purchased, products_id");
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <form action="stats_sales.php" method=get>
          <tr>
            <td class="pageHeading"><?php echo 'Commandes Mensuelle'; ?></td>
            <td class="main" align="right"><?='Mois : ' . tep_draw_pull_down_menu('month', $months, $month, 'onchange=\'this.form.submit();\'') . '<br>Ann�e : ' . tep_draw_pull_down_menu('year', $years, $year, 'onchange=\'this.form.submit();\'')?></td>
          </tr>
          <tr>
            <td class="pageHeading">&nbsp;</td>
            <td class="main" align="right"><?='Statuts : ' . tep_draw_pull_down_menu('status', $statuses, $status, 'onchange=\'this.form.submit();\'')?></td>
          </tr>
          <input type="hidden" name="by" value="<?=$HTTP_GET_VARS['by']?>">
          </form>
        </table></td>
      </tr>
	  
<?php

  if (tep_db_num_rows($sales_products_query) > 0) {
    $dp = '';
    $total=0;
	$total_wtax=0;

    while ($sales_products = tep_db_fetch_array($sales_products_query)) {
      if (($HTTP_GET_VARS['by']=='product')) $ddp='Product';
	    else $ddp = tep_date_short($sales_products['date_purchased']);
      $table_title = tep_date_long($sales_products['date_purchased']);
        if (($dp != $ddp)) { //if day has changed (or first day)
          if ($dp != '') { //close previous day if not first one
?>


              <tr class="dataTableRow">
			    <td class="dataTableContent">-</td>
                <td class="dataTableContent">-</td>
                <td class="dataTableContent" align=center>-</td>
                <td class="dataTableContent" align=center style="color:#FF0000"><?php echo $currencies->display_price($total_marge,0); $total_marge=0; ?>&nbsp;</td>
				<td class="dataTableContent" align=center style="color:#FF0000"><?php echo $currencies->display_price($daily_without,0); $daily_without=0; ?>&nbsp;</td>
				<td class="dataTableContent" align=center style="color:#FF0000"><?php echo $currencies->display_price($daily_with,0); $daily_with=0; ?>&nbsp;</td>
              </tr>

            </table></td>
           </tr>
        </table></td>
      </tr>

      <tr>
        <td><br></td>
      </tr>

<?php
        }
?>
      <tr>
        <td class=main><b><?php echo $table_title ?></td>
      </tr>

      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="2">
              <tr class="dataTableHeadingRow">
			    <td class="dataTableHeadingContent" width="15%"><?php echo 'R�f�rence'; ?></td>
                <td class="dataTableHeadingContent" width="45%"><?php echo 'Nom de l\'article'; ?></td>
                <td class="dataTableHeadingContent" align=center width="10%"><?php echo 'Quantit�'; ?></td>
                <td class="dataTableHeadingContent" align="center" width="10%"><?php echo 'Marge'; ?>&nbsp;</td>
				<td class="dataTableHeadingContent" align="center" width="10%"><?php echo 'Total HT'; ?>&nbsp;</td>
				<td class="dataTableHeadingContent" align="center" width="10%"><?php echo 'Total TTC'; ?>&nbsp;</td>
              </tr>
<?php }
?>

              <tr class="dataTableRow">
			    <td class="dataTableContent"><?php echo $sales_products ['products_model']; ?></td>
                <td class="dataTableContent"><?php echo $sales_products ['products_name']; ?></td>
                <td class="dataTableContent" align=center><?php echo $sales_products ['qty']; ?></td>
                <td class="dataTableContent" align=center><?php echo $currencies->display_price($sales_products ['daily_prod']-$sales_products ['daily_cost'],0); 
															    $total_marge+=$sales_products ['daily_prod']-$sales_products ['daily_cost']; ?>&nbsp;</td>
				<td class="dataTableContent" align=center><?php echo $currencies->display_price($sales_products ['daily_prod'],0); 
															    $daily_without+=$sales_products['daily_prod']; ?>&nbsp;</td>
				<td class="dataTableContent" align=center><?php echo $currencies->display_price($sales_products ['withtax'],0); 
																$daily_with+=$sales_products['withtax']; ?>&nbsp;</td>
              </tr>
<?php 
      $total+=$sales_products ['daily_prod'];
	  $total_wtax+=$sales_products ['withtax'];
      $dp = $ddp;
   }
   ?>
   <tr class="dataTableRow">
			    <td class="dataTableContent">-</td>
                <td class="dataTableContent">-</td>
                <td class="dataTableContent" align=center>-</td>
                <td class="dataTableContent" align=center style="color:#FF0000"><?php echo $currencies->display_price($total_marge,0); $total_marge=0; ?>&nbsp;</td>
				<td class="dataTableContent" align=center style="color:#FF0000"><?php echo $currencies->display_price($daily_without,0); $daily_without=0; ?>&nbsp;</td>
				<td class="dataTableContent" align=center style="color:#FF0000"><?php echo $currencies->display_price($daily_with,0); $daily_with=0; ?>&nbsp;</td>
              </tr>

            </table></td>
           </tr>
        </table></td>
      </tr>
      <tr>
        <td><br></td>
      </tr>
   <?php
    echo '<tr><td colspan=3>'.tep_draw_separator('pixel_trans.gif', '100%', '10').'</td></tr>';
    echo '<td colspan=3 class="main">'.'Montant Total HT du mois :'.'&nbsp;<b>'.$currencies->display_price($total,0).'</b> <br /> '.'Montant Total TTC du mois :'.'&nbsp;<b>'.$currencies->display_price($total_wtax,0).'</b></td></tr>';
   } else {
?>
  <tr>
    <td class=main><b>Il n'y a aucune vente pour ce mois</td>
  </tr>
<?php
   }
?>
    </table>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
