<?php
require('includes/application_top.php');
require('../' . DIR_WS_SITE . '/includes/fonctions/mail.php');

if(isset($_POST['maj']) && isset($_POST['iOrders']) && !empty($_POST['iOrders'])) {
    //recuperer le contenu du mail d'avertissement
    $re_mail='SELECT orders_status_comments, orders_status_message, orders_status_name '
            .'FROM orders_status '
            .'WHERE orders_status_id="4" ';
		
    $rep_mail=tep_db_query($re_mail);
    $content_mail=tep_db_fetch_array($rep_mail);
    $sms_contenu=$content_mail['orders_status_message'];

    $in='';
    for($i=0;$i<$_POST['iOrders'];$i++) {

        $in.=$_POST['oID'.$i].',';
    }

    $re_col='SELECT c.customers_id as customers, c.customers_email_address as mail, c.customers_telephone as tel, c.customers_news_sms as i_sms,
					o.customers_name as name, o.date_purchased as date, o.orders_id as oID '
            .'FROM   customers c, orders o '
            .'WHERE  o.orders_id IN ('.substr($in,0,strlen($in)-1).') AND o.customers_id=c.customers_id ';
    $rep_col=tep_db_query($re_col);
    
	$facture = array();
	$message_error = array();
	$mail_sent = array();
	
	$i = 0;

    while($arr_col=tep_db_fetch_array($rep_col)) {
		
		$sans_num_colis = false;

        if (isset($_POST['oCO'.$i]) && !empty($_POST['oCO'.$i])) {
            $num_colis = $_POST['oCO'. $i];
            $track_num_url = '<a href="https://www.coliposte.net/particulier/suivi_particulier.jsp?colispart='. $num_colis .'" style="color:#AAB41D">
								https://www.coliposte.net/particulier/suivi_particulier.jsp?colispart='. $num_colis .'</a>';
        } elseif (isset($_POST['oLM'. $i]) && !empty($_POST['oLM'. $i])) {
            $num_colis = $_POST['oLM'. $i];
            $track_num_url = '<a href="https://www.csuivi.courrier.laposte.fr/default.asp?EZ_ACTION=rechercheRapide&amp;tousObj=&amp;numObjet='. $num_colis .'">	https://www.csuivi.courrier.laposte.fr/default.asp?EZ_ACTION=rechercheRapide&amp;tousObj=&amp;numObjet='. $num_colis .'</a>';
        } elseif (isset($_POST['oCH'. $i]) && !empty($_POST['oCH'. $i])) {
			
            $num_colis = $_POST['oCH'. $i];
            $track_num_url = '<a href="https://www.fr.chronopost.com/fr/tracking/result?listeNumeros='. $num_colis .'">https://www.fr.chronopost.com/fr/tracking/result?listeNumeros='. $num_colis .'</a>';
			
        } elseif (isset($_POST['oGLS'. $i]) && !empty($_POST['oGLS'. $i])) {

            $num_colis = $_POST['oGLS'. $i];
            $track_num_url = '<a href="https://www.dpd.fr/traces_exd_'. $num_colis .'">https://www.dpd.fr/traces_exd_'. $num_colis .'</a>';
        
		} elseif (isset($_POST['oUPS'. $i]) && !empty($_POST['oUPS'. $i])) {

            $num_colis = $_POST['oUPS'. $i];
            $track_num_url = '<a href="https://www.ups.com/WebTracking?loc=fr_fr&requester=ST&trackingNumber='. $num_colis .'">https://www.ups.com/WebTracking?loc=fr_fr&requester=ST&trackingNumber='. $num_colis .'</a>';
        
		} elseif($_POST['sans_colis'. $i]=='on') {
			
			$sans_num_colis = true;
		}


        if(!empty($num_colis) || $sans_num_colis) {

            /*if(isset($arr_col['i_sms']) && ($arr_col['i_sms']==1)) {

                $Objet_mail="ytreza:".$arr_col['tel'];

                if(strlen($sms_contenu)>0) {

                    //ENVOI DU SMS PAR MAIL AU PRESTATAIRE
                    tep_mail($arr_col['name'], "sms@smstob.com", $Objet_mail, $sms_contenu, MAIL_ACHAT, MAIL_ACHAT);
                }
            }*/


            $relicats_commande = false;
			$rupture_commande = false;
            $liste_articles_envoyes = '';
            $liste_articles_non_envoyes = '';
			$liste_articles_rupture = '';


            $recherche_articles_query = tep_db_query("SELECT products_name, products_quantity, products_quantity_sent, products_quantity_saisie, orders_products_id, products_rupture FROM " . TABLE_ORDERS_PRODUCTS . " op WHERE op.orders_id = '" . $arr_col['oID'] . "'");

            while($recherche_articles = tep_db_fetch_array($recherche_articles_query)) {
				
				$relicats_article = false;
				$rupture_article = false;
				
				if($recherche_articles['products_quantity_saisie'] > 0) {
					
                	$liste_articles_envoyes .= $recherche_articles['products_quantity_sent'] . " x " . $recherche_articles['products_name'];
				}
				
				if($recherche_articles['products_rupture']) {
					
					$rupture_article = true;
					$rupture_commande = true;
	
					$manque_rupture = $recherche_articles['products_quantity'] - $recherche_articles['products_quantity_sent'];
					$liste_articles_rupture .= $manque_rupture . " x " . $recherche_articles['products_name'];
				}
				else {// si pas de rupture, on v�rifie s'il y a un relicat pour ce produit
					
					// On cr�e un bool�en initialis� � true s'il y a des relicats sur l'article en cours
					$relicats_article = $recherche_articles['products_quantity'] != $recherche_articles['products_quantity_sent'];
					// On cr�e un second bool�en qui se met � true si la commande a au moins un article en relicat
					$relicats_commande = $relicats_commande || $relicats_article;
	
					// S'il y a des relicats sur l'article on l'ajoute � la liste
					if($relicats_article) {
	
						$manque = $recherche_articles['products_quantity']-$recherche_articles['products_quantity_sent'];
						$liste_articles_non_envoyes .= $manque . " x " . $recherche_articles['products_name'];
					}
				}

                // On recherche les options de l'article s'il en a on l'ajoute apr�s le nom de l'article
                $recherche_options_query = tep_db_query("SELECT products_options_values FROM " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " WHERE orders_products_id = '" . $recherche_articles['orders_products_id'] . "'");
                $recherche_options = tep_db_fetch_array($recherche_options_query);

                if($recherche_options['products_options_values'] != '') {
					
					if($recherche_articles['products_quantity_saisie'] > 0) {
						
                    	$liste_articles_envoyes .= " (" . $recherche_options['products_options_values'] . ")";
					}
					
					if($rupture_article) {
						
						$liste_articles_rupture .= " (" . $recherche_options['products_options_values'] . ")";
					}
                    if($relicats_article) {

                        $liste_articles_non_envoyes .= " (" . $recherche_options['products_options_values'] . ")";
                    }
                }
				
				if($recherche_articles['products_quantity_saisie'] > 0) $liste_articles_envoyes .= "\n";
				if($rupture_article) $liste_articles_rupture .= "\n";
				elseif($relicats_article) $liste_articles_non_envoyes .= "\n";
            }



			



            //MISE A JOUR DU STATUS DE LA COMMANDE
            tep_db_query('update '.TABLE_ORDERS.' set orders_status = "4", last_modified = "'.date('Y-m-d H:i:s').'" where orders_id = "' . $arr_col['oID'] . '" ');

            //Mise a jour stock r�el
            $orders_products_query = tep_db_query("select products_id, products_quantity_sent, products_quantity_saisie, orders_products_id from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . $arr_col['oID'] . "'");
            while ($orders_products = tep_db_fetch_array($orders_products_query)) {

                $orders_products_attributes_query = tep_db_query("select products_options_values, products_options from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_products_id = '" . $orders_products["orders_products_id"] . "'");

                if (tep_db_num_rows($orders_products_attributes_query) != 0) {//produit avec option

                    $orders_products_attributes = tep_db_fetch_array($orders_products_attributes_query);
					
					

                    $orders_products_option_id_query = 'select pov.products_options_values_id
														from '. TABLE_PRODUCTS_OPTIONS_VALUES .' pov, '. TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS .' povtpo, '. TABLE_PRODUCTS_OPTIONS .' po
														where pov.products_options_values_id=povtpo.products_options_values_id
														and povtpo.products_options_id=po.products_options_id
														and products_options_values_name = \''. addslashes($orders_products_attributes['products_options_values']) .'\'
														and products_options_name=\''. addslashes($orders_products_attributes['products_options']) .'\'';
					
					$orders_products_option_id_query = tep_db_query($orders_products_option_id_query);
                    $orders_products_option_id = tep_db_fetch_array($orders_products_option_id_query);

                    $products_quantity = "update ".TABLE_PRODUCTS_ATTRIBUTES." set options_quantity_reel=options_quantity_reel-" . $orders_products["products_quantity_saisie"] . " where products_id='" . $orders_products["products_id"] . "' AND options_values_id = '" . $orders_products_option_id['products_options_values_id'] . "'";
					tep_db_query($products_quantity);
                }

                    tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity_reel = products_quantity_reel - " . $orders_products["products_quantity_saisie"] . " where products_id = '".$orders_products["products_id"]."'");
            }
			
			
			

            /*if(isset($arr_col['i_sms']) && ($arr_col['i_sms']==1)) {

                tep_db_query("INSERT INTO ". TABLE_ORDERS_STATUS_HISTORY
                        . " (orders_id, orders_status_id, date_added, customer_notified, track_num, track_num2, track_num3, track_num4, track_num5, message_sms) VALUES ('"
                        . $arr_col['oID'] ."', '4',
						'". date('Y-m-d H:i:s') ."', 
						'1',
						'". $_POST['oCO'. $i] ."',
						'". $_POST['oLM'. $i] ."',
						'". $_POST['oCH'. $i] ."',
						'". $_POST['oGLS'. $i] ."',
						'". $_POST['oUPS'. $i] ."',
						'" .$sms_contenu ."'
				)");

            }
            else {*/

                tep_db_query("INSERT INTO ". TABLE_ORDERS_STATUS_HISTORY
                        . " (orders_id, orders_status_id, date_added, customer_notified, track_num, track_num2, track_num3, track_num4, track_num5) VALUES ('"
                        . $arr_col['oID'] . "', '4', '".date('Y-m-d H:i:s')."', '1',
						
						'".$_POST['oCO'. $i]."',
						'".$_POST['oLM'. $i]."',
						'".$_POST['oCH'. $i]."',
						'".$_POST['oGLS'. $i]."',
						'".$_POST['oUPS'. $i]."'
					)");
            /*}*/


            $message_relicats = '';
			$message_rupture = '';
			
			
			
			//au moins un cas de rupture dans la commande
			if($rupture_commande) {
				
				$commentaire_admin = "Les articles suivants �tant en rupture pour une dur�e ind�termin�e, nous sommes contraints de les annuler et de proc�der � leur remboursement :\n\n". $liste_articles_rupture;

                $message_rupture = '<br><hr style="color:#AAB41D;"/><br>Les articles suivants �tant en rupture pour une dur�e ind�termin�e, nous sommes contraints de les annuler et de proc�der � leur remboursement :<br />
				<br />'. nl2br(stripslashes($liste_articles_rupture));
				
                $message_rupture .= '<br>Nous pouvons vous les rembourser soit :
				<ul><li>sur votre Porte Monnaie Virtuel (voir dans "Mon Compte" sur notre site <a href="'. WEBSITE .'">"'. ADRESSE_SITE .'"</a>)</li>
				<li>soit par le mode de r�glement que vous avez utilis� lors du paiement de votre commande : CB, Paypal, Ch�que..</li></ul>
				Nota : en ce qui concerne les commandes pay�es par virement, leurs remboursements ne pourront �tre effectu�s que sur PMV ou par Ch�que.<br>
				<br>
				<strong>Merci de nous indiquer par retour de mail quel mode de remboursement vous avez choisi.</strong><br />
				<br />Nous vous pr�sentons toutes nos excuses pour le d�sagr�ment caus�.<br>';
				
				tep_db_query('update '. TABLE_ORDERS .' set orders_status=15 where orders_id='. $arr_col['oID']);
				tep_db_query('insert into '. TABLE_ORDERS_STATUS_HISTORY .'(orders_id, orders_status_id, date_added, customer_notified, comments)
				values('. $arr_col['oID'] .', 15, \''. date('Y-m-d H:i:s') .'\', 1, \''. addslashes($commentaire_admin) .'\')');
			}
			
            // S'il y a des relicats sur la commande on met � jour le status de la commande
            if ($relicats_commande) {

                $commentaire_admin =  "Nous avons constat� une rupture de stock sur certains articles de votre commande.\nIl manque donc les articles suivants au colis : \n\n" . $liste_articles_non_envoyes . "<br>Nous enverrons le reste de votre commande � nos frais dans les plus brefs d�lais.<br>Merci par avance de votre compr�hension.<br>";

                $message_relicats = '<br><hr style="color:#AAB41D;"/><br>' . nl2br(stripslashes($commentaire_admin));

                tep_db_query('update '. TABLE_ORDERS .' set orders_status=23 where orders_id='. $arr_col['oID']);
                tep_db_query('insert into '. TABLE_ORDERS_STATUS_HISTORY .'(orders_id, orders_status_id, date_added, customer_notified, comments) values('. $arr_col['oID'] .', 23, \''. date('Y-m-d H:i:s') .'\', 1, \''. addslashes($commentaire_admin) .'\')');
            }

            // On construit le mail pour le client et on l'envoie
            $message_mail = 'Bonjour <span style="font-weight: bold; color: rgb(170, 180, 29);">'. ucwords(strtolower($arr_col['name'])) .'</span>,<br /><br />';
            $message_mail .= 'Nous vous informons que votre commande num�ro <span style="font-weight:bold;color:#FE5700;">' . $arr_col['oID'] . '</span> vient d\'�tre exp�di�e !<br><br>';
			$message_mail .= '<hr style="color:rgb(170, 180, 29);"/><br>Les articles suivants vous ont �t�s envoy�s :<br><br>' . nl2br(stripslashes($liste_articles_envoyes));

            //$message_mail .= '<br><hr style="color:#AAB41D;"/><br>';
            $message_mail .= $message_relicats;
            $message_mail .= $message_rupture;
			
			if(!$sans_num_colis) {
				
				$message_mail .= '<br><hr style="color:rgb(170, 180, 29);"/><br>';
				$message_mail .= 'Voici le lien de suivi de votre colis : '. $track_num_url .'<br />';
				$message_mail .= 'Le num�ro de votre colis est le : <span style="font-weight:bold;color:#FE5700;">'. $num_colis .'</span><br /><br />';
				$message_mail .= 'Si vous n\'avez pas re�u votre colis sous 5 jours pour une livraison en France et 10 jours pour une livraison en Europe ou dans les DOM TOM, merci de nous contacter au '. TEL_NORMAL .' '. HORAIRES_BOUTIQUE .'.<br /><br />';
            	$message_mail .= 'ATTENTION : Pass� un d�lai de 20 jours apr�s la date d\'exp�dition, plus aucun recours ne sera possible aupr�s du prestataire ayant effectu� la livraison. Si vous constatez que votre colis est endommag� lors de la livraison, merci de le refuser.';
			}
			
			
            $message_mail .= '<br><br><hr style="color:rgb(170, 180, 29);"/><br>Votre facture est disponible en cliquant sur ce lien : <a href="'. WEBSITE."/".DIR_WS_SITE.'/compte/generation_pdf/facture.php?id_com='. $arr_col['oID'] .'&cli='. md5($arr_col['customers']) .'" style="font-weight:bold;color:#FE5700;">Facture de la commande '. $arr_col['oID'] .'</a><br><br><hr style="color:rgb(170, 180, 29);"/>';
            $message_mail .=  "<br>" . SIGNATURE_MAIL;

            $mail_sent[$arr_col['oID']] = (mail_client_commande($arr_col['mail'], "Votre commande vient d'etre envoyee", $message_mail)) ? true : false;


            //Necessaire pour la facturation
            $check_status_query = tep_db_query("select customers_name, customers_email_address, orders_status, date_purchased, orders_numero_facture,orders_numero_facture from " .    TABLE_ORDERS . " where orders_id = '" . $arr_col['oID'] . "'");
            $check_status = tep_db_fetch_array($check_status_query);

            $check_invoice_counter_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'COMPTEUR_FACTURE'");
            $check_invoice_counter = tep_db_fetch_array($check_invoice_counter_query);
            $numero_facture = $check_invoice_counter['configuration_value'];

            if ($check_status['orders_numero_facture'] == 0) {
				
				$facture[$arr_col['oID']] = $numero_facture;
                tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . ($numero_facture+1) . "' where configuration_key = 'COMPTEUR_FACTURE'");
                tep_db_query("update " . TABLE_ORDERS . " set orders_numero_facture = '" . $numero_facture . "', last_modified = now() where orders_id = '"  . $arr_col['oID']. "'");
                tep_db_query("update " . TABLE_ORDERS . " set orders_date_facture = now() where orders_id = '"  . $arr_col['oID'] . "'");
            }
			else $facture[$arr_col['oID']] = '0';
            //fin facturation

            tep_db_query("update " . TABLE_ORDERS_PRODUCTS . " set products_quantity_saisie = 0 where orders_id = '".$arr_col['oID']."'");

            $i++;
        } else {
            $message_error[$arr_col['oID']] = '<a style="color:#FF0000;font-weight:bold;">Aucune Maj dans la bdd</a>';
            $message_error[$arr_col['oID']] += 'Donn�es envoy�es en param�tre : <pre>';
            $message_error[$arr_col['oID']] += print_r($_POST);
			$message_error[$arr_col['oID']] += '</pre>';
        }
    }
}
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Mise a jour group�e des num�ros de suivi - <?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <script language="javascript" src="includes/general.js"></script>
        <script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
        
        <script type="text/javascript" language="javascript">
			$(document).ready(function() {				
				$(".checkAll").change(function() {
					classToCheck = $(this).val();
					if($(this).attr("checked")) {
						$("."+ classToCheck).attr("checked", true);	
					} else {						
						$("."+ classToCheck).attr("checked", false);
					}
					$(".GLSToCheck").trigger('change');
				});
				
				$(".GLSToCheck").change(function() {
					id = $(this).attr('id').substring(8);
					if($(this).attr("checked")) {
						$("#oGLS"+ id).val($("#oID"+ id).val());
					} else {						
						$("#oGLS"+ id).val('');
					}
				});
			});
		</script>
        
    </head>
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

        <table border="0" width="100%" cellspacing="2" cellpadding="2">
            <tr>
                <td width="100%" valign="top">
                    <?php
if(isset($_POST['maj'])) { //Verification du passage dans la page pour la MAJ des commandes
?>
	<table align="center" border="0" class="tablesorter">
		<thead><tr>
			<th width="300"><b>ID commande</b></th>
			<th width="650"><b>Status</b></th>
			<th width="500"><b>Contient des relicats</b></th>
			<th width="350"><b>n� Facture</b></th>
			<th width="350"><b>Envoi Mail</b></th>
			<th class="{sorter:false}"><b>Message d'erreur</b></th>
		</tr></thead>
		<tbody>

			<?php // Creation du contenu du tableau qui permettra de voir les commandes bien ex�cut�
			if (isset($HTTP_GET_VARS['commandes']) && strlen($HTTP_GET_VARS['commandes'])>0) {
				$id_commandes = ltrim(($HTTP_GET_VARS['commandes']));
				$tableau_cmd = explode(' ', ($id_commandes));

				$in='';
				for($i=0;$i<count($tableau_cmd);$i++) {
					$in.=$tableau_cmd[$i].',';
				}
				$in=substr($in,0,strlen($in)-1);
				$re_orders='SELECT orders_id as id, orders_status as status FROM orders WHERE orders_id IN ('.$in.') ORDER BY orders_id ';
				$rep_orders=tep_db_query($re_orders);

				$i=0;
				while($commandes=tep_db_fetch_array($rep_orders)) {

					// Initialisation de quelques variables utiles par la suite
					$relicats_commande = false;
					$liste_articles_envoyes = '';
					$liste_articles_non_envoyes = '';

					// On liste les articles de la commande
					$recherche_articles_query = tep_db_query("SELECT products_name, products_quantity, products_quantity_sent, products_quantity_saisie, orders_products_id FROM " . TABLE_ORDERS_PRODUCTS . " op WHERE op.orders_id = '" . $commandes['id'] . "'");
					while ($recherche_articles = tep_db_fetch_array($recherche_articles_query)) {

						// On cr�e un bool�en initialis� � true s'il y a des reliquats sur l'article en cours
						$relicats_article = $recherche_articles['products_quantity'] != $recherche_articles['products_quantity_sent'];
						// On cr�e un second bool�en qui se met � true si la commande a au moins un article en reliquat
						$relicats_commande = $relicats_commande || $relicats_article;
					}
					?>
					<tr>
						<td align="center" class="main">
							<a href="<?php echo tep_href_link(FILENAME_CMD_EDIT,'page=1&oID='.$commandes['id'].'&action=edit'); ?>" TARGET="_self"><?php echo $commandes['id']; ?></a>
							<input type="hidden" name="oID<?php echo $i; ?>" value="<?php echo $commandes['id']; ?>" />
						</td>
						<td align="center">
							<?php
							if($commandes['orders_status']!=4) {
								echo '<b>La commande a bien �t� envoy�e</b>';
							} else {
								echo '<b></b>';
							}
							?>
						</td>
						<td align="center">
							<?php
							// Si la commande a des reliquats on l'affiche
							if ($relicats_commande) {
								echo '<b style="color: red;">OUI</b>';
							} else {
								echo 'non';
							}
							?>
						</td>
						<td align="center">
							<?php echo $facture[$commandes['id']]; ?>
						</td>
						<td align="center">
							<?php echo ($mail_sent[$commandes['id']]) ? 'mail envoy�' : '<a style="color:#FF0000;">mail non envoy�</a>'; ?>
						</td>
						<td align="center">
							<?php echo $message_error[$commandes['id']]; ?>
						</td>
					</tr>
				<?php
				$i++;
				}
			}
			?>

			<tr>
				<td colspan="6" align="center"><a href="cmd_list.php"><input type="submit" value="Retour" ></a></td>
			</tr>
		</tbody>
	</table>

	<?php
} else {  // FIN Affichage Page de Confirmation des commandes   
?>
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pageHeading">Mise � jour du suivi des commandes</td>
                </tr>
                <tr>
                    <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>

                <tr>
                    <td align="center">
                        <form name="Traitement_commande" method="post">
                            <table border="0" width="100%">
                                <tr>
                                    <td align="center" class="dataTableHeadingRow"><b>Id cmd</b></td>
                                    <td align="center" class="dataTableHeadingRow"><b>Colissimo</b></td>
                                    <td align="center" class="dataTableHeadingRow"><b>Lettre suivi</b></td>
                                    <td align="center" class="dataTableHeadingRow"><b>Chronopost</b></td>
                                    <td align="center" class="dataTableHeadingRow"><b>DPD</b></td>
                                    <td align="center" class="dataTableHeadingRow"><b>UPS</b></td>
                                    <td align="center" class="dataTableHeadingRow"><b>Sans n� de Colis</b></td>
                                    <td align="center" class="dataTableHeadingRow"><b>Statut</b></td>
                                </tr>
								<tr><td colspan="8" style="height:10px"></td></tr>
								<?php
								if (isset($HTTP_GET_VARS['commandes']) && strlen($HTTP_GET_VARS['commandes'])>0) {

									$id_commandes = ltrim(($HTTP_GET_VARS['commandes']));
									$tableau_cmd = explode(' ', ($id_commandes));
									$in='';
									for($i=0;$i<count($tableau_cmd);$i++) {
										$in.=$tableau_cmd[$i].',';
									}
									$in=substr($in,0,strlen($in)-1);
									$re_orders='SELECT orders_id as id, orders_status as status FROM orders WHERE orders_id IN ('.$in.') ORDER BY orders_id';
									$rep_orders=tep_db_query($re_orders);
									$i = 0;

									while($commandes = tep_db_fetch_array($rep_orders)) {
										
										// initialisation des tabindex pour une navigation par tabulation
										$j = $i+1;
										$k = $j+1000;
										$l = $j+2000;
										$m = $j+3000;
										$n = $j+4000;

										echo '<tr class="dataTableRow">';
										echo ' <td align="center" class="main">';
										echo '	<a href="'.tep_href_link(FILENAME_CMD_EDIT,'page=1&oID='.$commandes['id'].'&action=edit').'" TARGET="_self">'.$commandes['id'].'</a>';
										echo '  <input type="hidden" id="oID'.$i.'" name="oID'.$i.'" value="'.$commandes['id'].'" />';
										echo ' </td>';
										// colissimo
										echo ' <td align="center">';
										echo '  <input type="text" name="oCO'.$i.'" size="20" tabindex="'. $j .'"/>';
										echo ' </td>';
										// lettre suivi
										echo ' <td align="center">';
										echo '  <input type="text" name="oLM'.$i.'" size="20" tabindex="'. $k .'"/>';
										echo ' </td>';
										// chronopost
										echo ' <td align="center">';
										echo '  <input type="text" name="oCH'.$i.'" size="20" tabindex="'. $l .'"/>';
										echo ' </td>';
										// gls
										echo ' <td align="center">';
										echo '  <input type="text" name="oGLS'.$i.'" size="20" tabindex="'. $m .'"/>'; 
										echo ' </td>';
										// UPS
										echo ' <td align="center">';
										echo '  <input type="text" name="oUPS'.$i.'" size="20" tabindex="'. $n .'"/>'; 
										echo ' </td>';
										echo ' <td align="center">';
										echo '  <input type="checkbox" name="sans_colis'.$i.'"/>'; 
										echo ' </td>';
										echo ' <td align=center>';

										if($commandes['orders_status']!=4) {
											echo '<b>Commande jamais envoy�e</b>';
										} else {
											echo '<b></b>';
										}
										echo '</td>';
										echo '</tr>';
										echo '<tr><td colspan="8" style="height:10px"></td></tr>';

										$i++;
									}
								} else {
									echo '<tr><td align="center"><b>Aucune commande</b></td></tr>';
								}
								?>
                            </table>
                            <input type="hidden" value="<? echo $i; ?>" name="iOrders"/>
                            <input type="submit" value="Envoyer" name="maj" tabindex="20000"/>
                        </form>
                    </td>
                </tr>
            </table>
    <? } ?></td>
    </tr>
</table>
<?php 
require(DIR_WS_INCLUDES . 'footer.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
</body>
</html>