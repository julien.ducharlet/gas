<?php

include_once('entity.php');

class property_category extends entity {
	
	private $property_category_id = 0;
	private $property_category_infos = '';
	
	public function __construct($pID='') {
		
		if(!empty($pID)) {
			
			$this->property_category_id = $pID;
			
			$query = 'select * from '. TABLE_PROPERTIES_CATEGORIES .'
					  where properties_categories_id='. $this->property_category_id;
			$query = tep_db_query($query);
			
			$this->property_category_infos = tep_db_fetch_array($query);
		}
	}
	
	public function sendQuery(array $pField, $pTypeQuery) {
			
		switch($pTypeQuery) {
			
			case 'update':
				
				if(!empty($this->property_category_id)) {
					
					$this->setTable(TABLE_PROPERTIES_CATEGORIES);
					$this->setField($pField);
					
					$where = 'properties_categories_id='. $this->property_category_id;
					
					$this->executeQuery('update', $where);
				}
				break;
			case 'insert':
					
					$this->setTable(TABLE_PROPERTIES_CATEGORIES);
					$this->setField($pField);
					
					$this->executeQuery('insert', $where);
				break;
			
			case 'delete':
				
				if($this->getNumberOfProperties()>0) {//pas de suppression possible si des propriétés existent dans la catégorie
					
					session::addMessage('error', 'la caractéristique ne doit comporter aucune propriété pour pouvoir être supprimée');
				}
				else {
					session::addMessage('success', 'la caractéristique '. $this->property_category_infos['properties_categories_name'] .' a bien été supprimée');
					$this->setTable(TABLE_PROPERTIES_CATEGORIES);
					$where = 'properties_categories_id='. $this->property_category_id;
					$this->executeQuery('delete', $where);
				}
		}
	}
	
	public function registerFlag($pFlag) {
		
		$arrayField = array();
		$arrayField['properties_categories_show'] = $pFlag;
		
		$where = 'properties_categories_id='. $this->property_category_id;
		
		$this->setTable(TABLE_PROPERTIES_CATEGORIES);
		$this->setField($arrayField);
		$this->executeQuery('update', $where);
	}
	
	public function getNumberOfProperties() {
		
		$query = 'select * from '. TABLE_PROPERTIES_INFOS .' where properties_categories_id='. $this->property_category_id;
		echo $query;
		$query = tep_db_query($query);
		
		return tep_db_num_rows($query);
		
	}
	
	
	public function getInfos() {
		
		return $this->property_category_infos;
	}
}
?>