<?php

include_once('entity.php');

class property extends entity {
	
	private $property_id = 0;
	private $property_infos = '';
	private $all = array();
	
	public function __construct($pID='') {
		
		if(!empty($pID)) {
			
			$this->property_id = $pID;
			
			$query = 'select * from '. TABLE_PROPERTIES_CATEGORIES .' pc, '. TABLE_PROPERTIES_INFOS .' pi
					  where pc.properties_categories_id=pi.properties_categories_id
					  and pi.properties_infos_id='. $this->property_id;
			$query = tep_db_query($query);
			
			$this->property_infos = tep_db_fetch_array($query);
		}
	}
	
	
	public function getInfos() {
		
		return $this->property_infos;
	}
	
	/*
	 * retourne toutes les propriétés triées par catégories
	 */
	public function getAll() {
		
		
		$query = 'select * from '. TABLE_PROPERTIES_CATEGORIES .' pc, '. TABLE_PROPERTIES_INFOS .' pi
							where pc.properties_categories_id=pi.properties_categories_id
							order by properties_categories_sort, properties_categories_name, properties_infos_name';
		$query = tep_db_query($query);
		
		while($data = tep_db_fetch_array($query)) {
			
			$this->all[] = $data;
		}
		
		return $this->all;
	}
	
	/*
	 * retourn la catégorie d'une propriété
	 */
	public function getPropertyCategory($pID) {
		
		$query = 'select properties_categories_id from '. TABLE_PROPERTIES_INFOS .' where properties_infos_id='. $pID;
		$query = tep_db_query($query);
		
		$data = tep_db_fetch_array($query);
		
		return $data;
	}
	
	
	
	public function sendQuery(array $pField, $pTypeQuery) {
			
			switch($pTypeQuery) {
				
				case 'update':
						
						$this->setTable(TABLE_PROPERTIES_INFOS);
						$this->setField($pField);
						
						$where = 'properties_infos_id='. $this->property_id;
						$this->executeQuery('update', $where);
					break;
				case 'insert':
					
						$this->setTable(TABLE_PROPERTIES_INFOS);
						$this->setField($pField);
						
						$this->executeQuery('insert');
					break;
				
				case 'delete':
					
						$where = 'properties_infos_id='. $this->property_id;
						
						$this->setTable(TABLE_PROPERTIES_INFOS);
						$this->executeQuery('delete', $where);
						
						
						$this->setTable(TABLE_PROPERTIES_INFOS_TO_PRODUCTS);
						$this->executeQuery('delete', $where);
						
						$this->setTable(TABLE_PROPERTIES_INFOS_TO_CATEGORIES);
						$this->executeQuery('delete', $where);
					break;
		}
	}
	
	
}
?>