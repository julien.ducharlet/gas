<?php

include_once('session.php');

abstract class entity {
	
	private $field;
	private $tableName;
	
	private $debug = false;
	
	private $stats = array('delete', 'update', 'insert');
	
	public function debug() {
		
		$this->debug = !$this->debug;
	}
	
	
	protected function setTable($tableName) {
		
		$this->tableName = $tableName;
	}
	
	protected function setField($pField) {
		
		$this->field = $pField;
	}
	
	/*
	 * exécute les différentes requêtes de modification
	 * @$pTypeQuery : type de la requête
	 * @$pCondition : condition dans la clause `where` de la requête
	 * @$pIgnore : permet d'ignorer l'erreur générée par un duplicate entry (s'il y en a une)
	 */
	protected function executeQuery($pTypeQuery, $pCondition='', $pIgnore = false) {
		
		if(!empty($this->tableName) && (!empty($this->field) || ($pTypeQuery=='delete' && $pCondition!=''))) {
			
			switch($pTypeQuery) {
				
				case'update':
					
					$i = 0;
					$query = 'update `'. $this->tableName .'` SET ';
					
					foreach($this->field as $key => $value) {
						
						$query .= '`'. $key .'` = \''. tep_db_input($value) .'\'';
						
						if(++$i < sizeof($this->field)) $query .= ',' ."\n";
					}
					
					$query .= ' where '. $pCondition;
					
					$query = str_replace(', where', ' where', $query);
					
					$this->execute($query);
					
					$this->stats['update']++;
					
					break;
					
				case 'insert':
					
					$i = 0;
					$query = 'insert '. (($pIgnore) ? 'IGNORE' : '') .' into '. $this->tableName .' (';
					$values = ' values(';
					
					foreach($this->field as $key => $value) {
						
						$query .= '`'. $key .'`';
						$values .= '\''. tep_db_input($value) .'\'';
						
						if(++$i < sizeof($this->field)) {
							
							$query .= ",\n";
							$values .= ",\n";
						}
					}
					
					$query .= ')'. $values .')';
					
					$this->execute($query);
					
					$this->stats['insert']++;
					
					break;
				case 'delete':
					
					$query = 'delete from '. $this->tableName .' where '. $pCondition .'';
					$this->execute($query);
					
					$this->stats['delete']++;
					
					break;
			}
		}
		else {
			
			echo '<h2 style="color:#FF0000;">Le nom de la table ou bien les champs sont vides pour la table '. $this->tableName .' ['. $pTypeQuery .']</h2>';
		}
		
		$this->resetAllFields();
	}
	
	protected function getLastInsertedId() {
		
		return tep_db_insert_id();
	}
	
	public function getStats() {
		
		return $this->stats;
	}
	
	private function resetAllFields() {
		
		$this->setTable('');
		$this->setField('');
	}
	
	private function execute($pQuery) {
		
		if($this->isDebugging()) {
			
			echo nl2br($pQuery) .'<br/><br/>';
		}
		else tep_db_query($pQuery);
	}
	
	private function isDebugging() {
		
		return $this->debug;
	}
}
?>