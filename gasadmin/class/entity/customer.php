<?php

include_once('entity.php');

class customer extends entity{
	
	private $customer_id = 0;
	private $customer_infos = '';
	private $customer_mails = array();
	private $customer_addresses = array();
	
	public function __construct($pID) {
		
		if(!empty($pID)) {
			
			$this->customer_id = $pID;
			
			$query = 'select * from '. TABLE_CUSTOMERS .' c, '. TABLE_CUSTOMERS_INFO .' ci
					  where c.customers_id=ci.customers_info_id
					  and c.customers_id='. $this->customer_id;
			$query = tep_db_query($query);
			$data = tep_db_fetch_array($query);
			
			$this->customer_infos = $data;
		}
	}
	
	public function getId() {
		
		return $this->customer_id;
	}
	
	public function getInfos() {
		
		return $this->customer_infos;
	}
	
	
	public function getEmail($pID) {
		
		$query = 'select e.email_id, email_name, email_type, email_send
					from '. TABLE_CUSTOMERS_TO_EMAIL .' cte, '. TABLE_EMAIL .' e
					where cte.email_id=e.email_id
					and customers_id='. $this->customer_id .'
					and e.email_id='. $pID;
		$query = tep_db_query($query);
		
		return tep_db_fetch_array($query);
	}
	
	public function getEmails() {
		
		if(empty($this->customer_mails)) {
			
			$query = 'select e.email_id, email_name, email_type, email_send
						from '. TABLE_CUSTOMERS_TO_EMAIL .' cte, '. TABLE_EMAIL .' e
						where cte.email_id=e.email_id
						and customers_id='. $this->customer_id;
			$query = tep_db_query($query);
			while($data = tep_db_fetch_array($query)) {
				
				$this->customer_mails[] = $data;
			}
		}
		
		return $this->customer_mails;
	}
	
	public function getCountMobiles() {
		
		$query = 'select count(*) as total from '. TABLE_CUSTOMERS_MOBILE .' where customers_id='. $this->customer_id;
		$query  = tep_db_query($query );
		$data = tep_db_fetch_array($query );
		
		return $data['total'];
	}
}
?>