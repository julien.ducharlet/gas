<?php

include_once('entity.php');

class order_status extends entity{
	
	private $orders_status_id = 0;
	private $orders_status_infos = '';
	
	private $orders_status_messages = array();
	
	
	public function __construct($pID) {
		
		$this->orders_status_id = $pID;
		
		$query = 'select * from '. TABLE_CONFIGURATION .' where configuration_key = \'DEFAULT_ORDERS_STATUS_ID\' and configuration_value = \''. $this->orders_status_id .'\'';
		$query = tep_db_query($query);
		$default = (tep_db_num_rows($query)>0) ? true : false;
		
		$query = 'select * from '. TABLE_ORDERS_STATUS .'
				  where orders_status_id='. $this->orders_status_id;
				  
		$query = tep_db_query($query);
		
		$this->orders_status_infos = tep_db_fetch_array($query);
		$this->orders_status_infos['orders_default'] = $default;
	}
	
	/*
	 *
	 *
	 */
	public function getInfos() {
		
		return $this->orders_status_infos;
	}
	
	public function getMessages() {
		
		if(empty($this->orders_status_messages)) {
			
			$query = 'select * from '. TABLE_ORDERS_STATUS_MESSAGES .' where orders_status_id='. $this->orders_status_id .' order by orders_status_messages_name';
			
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$this->orders_status_messages[] = $data;
			}
		}
		
		return $this->orders_status_messages;
	}
	
	/*
	 *
	 */
	public function getMessage($pID) {
		
		$query = 'select * from '. TABLE_ORDERS_STATUS_MESSAGES .' where orders_status_messages_id='. $pID;
		$query = tep_db_query($query);
		
		return tep_db_fetch_array($query);
	}
	
	/*
	 *
	 */
	public function getCountMessages() {
		
		return sizeof($this->getMessages());
	}
	
	/*
	 *
	 */
	public function registerStatus(array $pField, $pTypeQuery) {
		
		switch($pTypeQuery) {
			
			case 'update':
				
				if(!empty($this->orders_status_id)) {
					
					$this->setTable(TABLE_ORDERS_STATUS);
					$this->setField($pField);
					
					$where = 'orders_status_id='. $this->orders_status_id;
					
					$this->executeQuery('update', $where);
				}
				break;
				
			case 'insert':
					
				$this->setTable(TABLE_CATEGORIES);
				$this->setField($pField);
				
				$this->executeQuery('insert');
				
				$this->category_id = $this->getLastInsertedId();
				break;
				
			case 'delete':
				
				break;
		}
	}
	
	/*
	 *
	 */
	public function registerMessage(array $pField, $pTypeQuery, $pID) {
		
		switch($pTypeQuery) {
			
			case 'update':
				
				if(!empty($this->orders_status_id)) {
					
					
					$this->setTable(TABLE_ORDERS_STATUS_MESSAGES);
					$this->setField($pField);
					
					$where = 'orders_status_messages_id='. $pID;
					
					$this->executeQuery('update', $where);
				}
				break;
				
			case 'insert':
				
				$pField['orders_status_id'] = $this->orders_status_id;
				
				$this->setTable(TABLE_ORDERS_STATUS_MESSAGES);
				$this->setField($pField);
				
				$this->executeQuery('insert');
				
				break;
				
			case 'delete':
				
				break;
		}
	}
}
?>