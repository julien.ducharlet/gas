<?php

include_once('entity.php');

class product extends entity{
	
	private $product_id = 0;
	private $product_infos = '';
	private $rangedProperties = array();
	private $properties = array();
	private $compatibilities = array();
	private $category;
	
	public function __construct($pID='') {
		
		if(!empty($pID)) {
			$this->product_id = $pID;
			
			$query = 'select * from '. TABLE_PRODUCTS .' p, '. TABLE_PRODUCTS_DESCRIPTION .' pd
					  where p.products_id=pd.products_id
					  and p.products_id='. $this->product_id;
			$query = tep_db_query($query);
			
			$this->product_infos = tep_db_fetch_array($query);
		}
	}
	
	/*
	 *
	 * g�re les requ�tes de cr�ation ou de modification d'un produit
	 */
	public function sendQuery(array $pField, $pTypeQuery, array $pFieldDescription = NULL) {
		
		switch($pTypeQuery) {
			
			case 'update':
				
				if(!empty($this->product_id)) {
					
					$this->setTable(TABLE_PRODUCTS);
					$this->setField($pField);
					
					$where = 'products_id='. $this->product_id;
					
					$this->executeQuery('update', $where);
				}
				break;
			case 'insert':
				
				if(!empty($pFieldDescription)) {
					
					$this->setTable(TABLE_PRODUCTS);
					$this->setField($pField);
					$this->executeQuery('insert');
					
					$productInserted = $this->getLastInsertedId();
					
					$pFieldDescription['products_id'] = $productInserted;
					
					$this->setTable(TABLE_PRODUCTS_DESCRIPTION);
					$this->setField($pFieldDescription);
					
					$this->executeQuery('insert');
					
					return $productInserted;
				}
				
				break;
		}
	}
	
	/*
	 *
	 *
	 */
	public function getId() {
		
		return $this->product_id;
	}
	
	/*
	 *
	 *
	 */
	public function getInfos() {
		
		return $this->product_infos;
	}
	
	
	/*
	 *
	 */
	public function getAllBroken() {
		
		$myFile = new myFile('files/', 'products_problem.txt');
		$myFile->open('r');
		$content = $myFile->getContent();
			
		$content = $content[0];
		
		return explode(' ', $content);
	}
	
	/*
	 *
	 */
	public function getAllCategories() {
		
		$query = 'select cd.categories_id, categories_name
				  from '. TABLE_PRODUCTS_TO_CATEGORIES .' ptc, '. TABLE_CATEGORIES_DESCRIPTION .' cd
				  where ptc.categories_id=cd.categories_id
				  and products_id='. $this->product_id;
		$query = tep_db_query($query);
		
		while($data = tep_db_fetch_array($query)) {
			
			$categories[] = $data['categories_id'];
		}
		
		return $categories;
	}
	
	/*
	 *
	 *
	 */
	public function getAllHidden() {
		
		$query = $this->queryHidden();
		
		while($data = tep_db_fetch_array($query)) {
			
			$allProducts[] = $data;
		}
		
		return $allProducts;
	}
	
	/*
	 *
	 *
	 */
	public function getAllWithPriceProblem() {
		
		$query = 'select * from '. TABLE_PRODUCTS .' p, '. TABLE_PRODUCTS_PRICE_BY_QUANTITY .' ppbq
				  where p.products_id=ppbq.products_id
				  and (part_price_by_1!=p.products_price OR products_price!=pro_price_by_1 OR rev_price_by_1<products_cost)';
		$query = tep_db_query($query);
		//products_price!=adm_price_by_1 OR 
		
		return tep_db_num_rows($query);
	}
	
	
	/*
	 *
	 */
	public function getAllSuppliers($pSupplier = 0) {
		
		if($pSupplier > 0) {
			
			$query = 'select * from '. TABLE_PRODUCTS_TO_SUPPLIERS .' where products_id='. $this->product_id .' and suppliers_id='. $pSupplier;
			$query = tep_db_query($query);
			
			return tep_db_fetch_array($query);
		}
	}
	
	
	/*
	 *
	 *
	 */
	public function getCountCategories() {
		
		$query = 'select count(*) as total from '. TABLE_PRODUCTS_TO_CATEGORIES .' where products_id='. $this->product_id;
		$query = tep_db_query($query);
		$data = tep_db_fetch_array($query);
		
		return $data['total'];
	}
	
	/*
	 *
	 */
	public function getCountAllBroken() {
		
		$myFile = new myFile('files/', 'products_problem.txt');
		$myFile->open('r');
		$content = $myFile->getContent();
		if(!empty($content[0])) {
			
			$content = $content[0];
			$content = explode(' ', $content);
			$return = sizeof($content);
		}
		else $return = 0;
		
		return $return;
	}
	
	/*
	 *
	 *
	*/
	
	public function getCountAllOptionBroken() {
		
		$query = 'select p.products_id, p.products_model, pd.products_name, products_quantity_reel, products_quantity, sum(options_quantity_reel) as options_quantity_reel, sum(options_quantity) as options_quantity
				  from '. TABLE_PRODUCTS .' p,
				  '. TABLE_PRODUCTS_DESCRIPTION .' pd,
				  '. TABLE_PRODUCTS_ATTRIBUTES .' pa
				  
				  where p.products_id=pd.products_id
				  and pd.products_id=pa.products_id
				  
				  group by p.products_id
				  having (products_quantity_reel!=options_quantity_reel OR products_quantity!=options_quantity)
				  
				  order by products_model';
		$query = tep_db_query($query);
		
		return tep_db_num_rows($query);
	}
	
	/*
	 *
	 *
	 */
	public function getCountAllHidden() {
		
		$query = $this->queryHidden();
		
		return tep_db_num_rows($query);
	}
	
	
	/*
	 *
	 *
	 */
	public function getAverageSales($pMonth) {
		
		$dateFrom = strtotime('- '. $pMonth .' months');
		$dateAdded = strtotime($this->product_infos['products_date_added']);
		
		$nbMonths = floor($dateAdded-strtotime(date('Y-m-d')))/(3600*24*30);
		
		$query = 'select SUM(products_quantity) as total
					from '. TABLE_ORDERS .' o,
						 '. TABLE_ORDERS_PRODUCTS .' op
					where o.orders_id=op.orders_id
					and op.products_id='. $this->product_id .'
					and date_purchased >= \''. date('Y-m', $dateFrom) .'01 00:00:00\'
					and date_purchased < NOW()';
		//printr($query);
		$query = tep_db_query($query);
		$data = tep_db_fetch_array($query);
		//echo $this->product_id .' => '. $data['total'] .'/'. $nbMonths .'<br>';
		return round($data['total']/$nbMonths, 2);
	}
	
	/*
	 *
	 * retourne les diff�rents prix du produit
	 */
	public function getPrice() {
		 
		$query = 'select * from '. TABLE_PRODUCTS_PRICE_BY_QUANTITY .' where products_id='. $this->product_id;
		$query = tep_db_query($query);
		
		return tep_db_fetch_array($query);
	}
	 
	/*
	 *
	 * retourne toutes les propri�t�s du produit
	 */
	public function getProperties($pRefresh = false) {
		
		$properties = array();
		
		if(empty($this->properties) || $pRefresh) {
			
			$query = 'select * from '. TABLE_PROPERTIES_INFOS_TO_PRODUCTS .' where products_id='. $this->product_id;
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$properties[] = $data['properties_infos_id'];
			}
			
			$this->properties = $properties;
		}
		
		return $this->properties;
	}
	
	
	/*
	 *
	 * retourne les propri�t�s class�es par leurs cat�gories de propri�t�s
	 */
	public function getRangedProperties($pRefresh = false) {
		
		$properties = array();
		
		if(empty($this->rangedProperties) || $pRefresh) {
			
			$query = 'select * from '. TABLE_PROPERTIES_INFOS_TO_PRODUCTS .' pitp, '. TABLE_PROPERTIES_INFOS .' pi, '. TABLE_PROPERTIES_CATEGORIES .' pc
							   where pitp.properties_infos_id=pi.properties_infos_id
							   and pi.properties_categories_id=pc.properties_categories_id
							   and products_id='. $this->product_id .'
							   order by pc.properties_categories_id';
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$properties[$data['properties_categories_id']][] = $data['properties_infos_id'];
			}
			
			$this->rangedProperties = $properties;
		}
		
		return $this->rangedProperties;
	}
	
	/*
	 *
	 * retourne toutes les cat�gories concern�es par une compatibilit�
	 */
	public function getCategoriesFromCompatibility($pCompatibility) {
		
		$categories = array();
			
		$query = 'select categories_id
					from '. TABLE_CATEGORIES .'
					where parent_id='. $pCompatibility;
		$query = tep_db_query($query);
		
		while($data = tep_db_fetch_array($query)) {
			
			$categories[] = $data['categories_id'];
		}
		
		return $categories;
	}
	
	
	/*
	 *
	 * retourne toutes les cat�gories compatibles du produit
	 */
	public function getCompatibilities($pRefresh = false) {
		
		$compatibilities = array();
		
		if(empty($this->compatibilities) || $pRefresh) {
			
			$query = 'select categories_id from '. TABLE_PRODUCTS_COMPATIBILITIES .' where products_id='. $this->product_id;
			
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$compatibilities[] = $data['categories_id'];
			}
			
			$this->compatibilities = $compatibilities;
		}
		
		return $this->compatibilities;
	}
	
	/*
	 *
	 * pID : id de la categorie
	 */
	public function getRubrique($pId = 0) {
		
		if($pId == 0) {
			
			$query = 'select c.parent_id, c.categories_id from '. TABLE_PRODUCTS_TO_CATEGORIES .' ptc, '. TABLE_CATEGORIES .' c where ptc.categories_id=c.categories_id and products_id='. $this->product_id;
		}
		else {
			
			$query = 'select parent_id, categories_id from '. TABLE_CATEGORIES .' where categories_id='.$pId;
		}
		
		$query = tep_db_query($query);
		$data = tep_db_fetch_array($query);
		
		if($data['parent_id'] != 0) $this->getRubrique($data['parent_id']);
		else {
			
			$query = 'select rubrique_id from '. TABLE_CATEGORIES_RAYON .' where categories_id='.$data['categories_id'];
			$query = tep_db_query($query);
			$data = tep_db_fetch_array($query);
				
			return $data['rubrique_id'];
		}
	}
	
	
	/*
	 * trouve la liste des alias du t�l�phone dans lesquels le produit n'est pas dupliqu�
	*/
	public function getEmptyAlias($pID) {
		
		if(empty($this->category)) {
			
			$this->category = new category($pID);
		}
		
		$aliasToVerify = array();
		$aliasList = $this->category->getAllAlias();
		$allCategories = $this->getAllCategories();
		
		foreach($aliasList as $alias) {
			
			if(!in_array($alias['categories_id'], $allCategories)) {
				
				$aliasToVerify[] = $alias;
			}
		}
		
		return $aliasToVerify;
	}
	
	
	/*
	 *
	 * TODO
	 */
	public function getSalesStats($period, $countProOrders) {
		
		for($i=0 ; $i<$period ; $i++) {
			
			$monthBegin = strtotime(date('Y-'));
			
			$today = date('Y-m-d', strtotime('2011-04-15'));
			
			$monthEnd = strtotime('-1 month', $monthBegin);
			
			
			$query_order = 'select SUM(products_quantity) as products_quantity_total_ordered
				from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op
				where o.orders_id=op.orders_id
				and products_id='. $this->product_id .'
				and date_purchased >= \''. $monthBegin .'\'
				and date_purchased < \''. $monthEnd .'\'';
		}
	}
	
	
	
	
	
	public function setPriceByQuantity(array $pArray) {
		
		$this->setTable(TABLE_PRODUCTS_PRICE_BY_QUANTITY);
		$this->setField($pArray);
		$this->executeQuery('insert');
	}
	
	public function setSupplier($pSupplier, $pPrice, $pRef) {
		
		$this->setTable(TABLE_PRODUCTS_TO_SUPPLIERS);
		$this->setField(array('suppliers_id' => $pSupplier, 'products_id' => $this->product_id, 'prix_achat' => $pPrice, 'ref_supplier' => $pRef));
		$this->executeQuery('insert');
	}
	
	public function setCategory($pCategory) {
		
		$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
		$this->setField(array('categories_id' => $pCategory, 'products_id' => $this->product_id));
		$this->executeQuery('insert');
	}
	 
	public function setProperty($pProperty) {
		
		$this->setTable(TABLE_PROPERTIES_INFOS_TO_PRODUCTS);
		$this->setField(array('properties_id' => $pCategory, 'products_id' => $this->product_id));
		$this->executeQuery('insert');
	}
	
	/*
	 *
	 */
	public function setBroken($products) {
		
		$myFile = new myFile('files/', 'products_problem.txt');
		$myFile->write($products);
	}
	
	/*
	 *
	 */
	public function setCompatibility($pCategory) {
		
		$this->setTable(TABLE_PRODUCTS_COMPATIBILITIES);
		$this->setField(array('categories_id' => $pCategory, 'products_id' => $this->product_id));
		$this->executeQuery('insert');
	}
	
	
	
	
	/*
	 *
	 * met � jour les propri�t�s du produit
	 */
	public function updateProperties(array $pPropertiesToInsert) {
		
		$arrayField = array();
		$allProperties = $this->getProperties();
		
		$propertiesToDelete = getValuesToDelete($allProperties, $pPropertiesToInsert);
		$propertiesToInsert = getValuesToInsert($allProperties, $pPropertiesToInsert);
		
		
		foreach($propertiesToDelete as $propertyToDelete) {
			
			$where = 'properties_infos_id='. $propertyToDelete .' and products_id='. $this->getId();
			
			$this->setTable(TABLE_PROPERTIES_INFOS_TO_PRODUCTS);
			$this->executeQuery('delete', $where);
			
			//suppression des cat�gories concern�es par cette propri�t�
			$this->updateCategories($propertyToDelete, 'delete');
		}
		
		foreach($propertiesToInsert as $propertyToInsert) {
			
			$arrayField['properties_infos_id'] = $propertyToInsert;
			$arrayField['products_id'] = $this->getId();
			
			$this->setTable(TABLE_PROPERTIES_INFOS_TO_PRODUCTS);
			$this->setField($arrayField);
			$this->executeQuery('insert');
			
			//pas besoin de faire de mise � jour sur l'ensemble des cat�gories. La liaison entre le produit et les futures cat�gories concern�es se fait depuis la m�thode updateCompatibilities
			
			//mise � jour pour les cat�gories compatibles d�j� li�es
			//$this->updateCategories($propertyToInsert, 'insert');
		}
	}
	
	
	/*
	 *
	 * modifie les compatibilit�s d'un produit et met � jour la liaison entre le produit et les cat�gories concern�es par les propri�t�s
	 */
	public function updateCompatibilities(array $pCompatibilitiesToInsert) {
		
		$arrayField = array();
		$allCompatibilities = $this->getCompatibilities();
		
		$compatibilitiesToDelete = getValuesToDelete($allCompatibilities, $pCompatibilitiesToInsert);
		$compatibilitiesToInsert = getValuesToInsert($allCompatibilities, $pCompatibilitiesToInsert);
		
		//pas de maj des cat�gories => insert sur toutes les marques s�lectionn�es
		if(empty($compatibilitiesToDelete) && empty($compatibilitiesToInsert)) $compatibilitiesToInsert = $pCompatibilitiesToInsert;
		
		foreach($compatibilitiesToDelete as $compatibilityToDelete) {
			
			$where = 'categories_id='. $compatibilityToDelete .' and products_id='. $this->product_id;
			
			$this->setTable(TABLE_PRODUCTS_COMPATIBILITIES);
			$this->executeQuery('delete', $where);
			
			$categoriesForThisCompatibility = $this->getCategoriesFromCompatibility($compatibilityToDelete);
			
			foreach($categoriesForThisCompatibility as $category) {//on supprime toutes les liaisons avec les cat�gories compatibles
				
				$where = 'categories_id='. $category .' and products_id='. $this->product_id;
				
				$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
				$this->executeQuery('delete', $where);
			}
		}
		
		foreach($compatibilitiesToInsert as $compatibilityToInsert) {
			
			$arrayField['categories_id'] = $compatibilityToInsert;
			$arrayField['products_id'] = $this->getId();
			
			$this->setTable(TABLE_PRODUCTS_COMPATIBILITIES);
			$this->setField($arrayField);
			$this->executeQuery('insert', '', true);
			
			//on r�cup�re la liste de toutes les cat�gories concern�es
			$categoriesForThisCompatibility = $this->getCategoriesFromCompatibility($compatibilityToInsert);
			//echo '<h1>INSERT after update</h1>';
			foreach($categoriesForThisCompatibility as $category) {
				
				//on v�rifie si la nouvelle cat�gorie correspond aux crit�res
				if($this->isCategoryCompatible($category)) {
					
					$arrayField['categories_id'] = $category;
					$arrayField['products_id'] = $this->product_id;
					//echo $category .'<br>';
					$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
					$this->setField($arrayField);
					$this->executeQuery('insert', '', true);
				}
				//else echo '<b>'. $category .'</b><br>';
			}
		}
	}
	
	
	/*
	 *
	 * met � jour la liaison entre le produit et les cat�gories en se basant sur les propri�t�s du produit (produit universel)
	 */
	private function updateCategories($pPropertyId, $pUpdateType) {
		
		$arrayField = array();
		
		$query = 'select categories_id from '. TABLE_PROPERTIES_INFOS_TO_CATEGORIES .' where properties_infos_id='. $pPropertyId;
		$query = tep_db_query($query);
		
		switch($pUpdateType) {
			
			case 'insert':
			
				/*while($data = tep_db_fetch_array($query)) {
					
					$arrayField['products_id'] = $this->product_id;
					$arrayField['categories_id'] = $data['categories_id'];
					
					$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
					$this->setField($arrayField);
					
					$this->executeQuery('insert', '', true);
				}*/
				break;
			
			case 'delete':
			
				while($data = tep_db_fetch_array($query)) {
					
					$where = 'categories_id='. $data['categories_id'] .' and products_id='. $this->product_id;
					
					$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
					
					$this->executeQuery('delete', $where);
				}
				break;
		}
	}
	
	
	/*
	 *
	 * Mise � jour du statut du produit
	*/
	public function updateStatus($pStatus) {
		
		$arrayField = array();
		
		$arrayField['products_status'] = (int)$pStatus;
		
		$where = 'products_id='. $this->getId();
		
		$this->setTable(TABLE_PRODUCTS);
		$this->setField($arrayField);
		
		$this->executeQuery('update', $where);
	}
	
	
	/*
	 *
	 *
	 */
	private function queryHidden() {
		
		$allProducts = array();
		
		$query = 'select * from '. TABLE_PRODUCTS_DESCRIPTION .' where products_id NOT IN(select products_id from '. TABLE_PRODUCTS_TO_CATEGORIES .')';
		return tep_db_query($query);
	}
	
	
	/*
	 *
	 *
	 */
	private function convertPropertiesForQuery(array $pProperties) {
		
		$tempArray = array();
		$propertyCondition = array();
		
		foreach($pProperties as $propertyCategory) {
			
			if(sizeof($propertyCategory)>1) {
				
				foreach($propertyCategory as $property) {
					
					$tempArray[] = 'properties_infos_id='. $property;
				}
				
				$propertyCondition[] = '('. implode(' or ', $tempArray) .')';
			}
			else $propertyCondition[] = 'properties_infos_id='. $propertyCategory[0];
		}
		
		return implode(' and ', $propertyCondition);
	}
	
	/*
	 *
	 * d�termine si une cat�gorie est compatible avec le produit
	 */
	private function isCategoryCompatible($pCategory) {
		
		$compatibility = true;
		$tempCompatibility = false;
		$categoryProperties = array();
		$productCategoriesProperties = $this->getRangedProperties();
		
		
		//$propertyCondition = $this->convertPropertiesForQuery($allProperties);
		
		///on r�cup�re toutes les propri�t�s de la cat�gorie
		$query = 'select * from '. TABLE_PROPERTIES_INFOS_TO_CATEGORIES .'
						   where categories_id='. $pCategory;
		$query = tep_db_query($query);
		
		while($data = tep_db_fetch_array($query)) {
			
			$categoryProperties[] = $data['properties_infos_id'];
		}
		
		if(sizeof($productCategoriesProperties)>0) {
			
			//on les compare aux propri�t�s du produit.
			foreach($productCategoriesProperties as $productCategoriesProperty) {
				
				$tempCompatibility = false;
				
				foreach($productCategoriesProperty as $productProperty) {
					
					if(in_array($productProperty, $categoryProperties)) {
						$tempCompatibility = true;
					}
				}
				
				$compatibility *= $tempCompatibility;
			}
		}
		else $compatibility = false;
		
		return $compatibility;
	}
	
	public function isInDestocking() {
		
		return ($this->product_infos['products_quantity_min']==0 &&
				$this->product_infos['products_quantity_max']==0 &&
				$this->product_infos['products_quantity_ideal']==0 &&
				$this->product_infos['products_manage_stock']=='stock' &&
				$this->product_infos['products_icon']=='destock');
	}
	
	public function isInHiddenDestocking() {
		
		return ($this->product_infos['products_quantity_min']==0 &&
				$this->product_infos['products_quantity_max']==0 &&
				$this->product_infos['products_quantity_ideal']==0 &&
				$this->product_infos['products_manage_stock']=='stock' &&
				$this->product_infos['products_icon']=='');
	}
	
	public function isEnding() {
		
		return ($this->product_infos['products_quantity_min']==0 &&
				$this->product_infos['products_quantity_max']==0 &&
				$this->product_infos['products_quantity_ideal']==0 &&
				$this->product_infos['products_manage_stock']=='stock' &&
				$this->product_infos['products_icon']=='fin_vie');
	}
	
}
?>