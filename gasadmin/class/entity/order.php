<?php

include_once('entity.php');

class order extends entity{
	
	private $order_id = 0;
	private $order_infos = '';
	private $order_iso = '';
	private $order_products = array();
	private $order_products_to_send = array();
	
	public function __construct($pID) {
		
		if(!empty($pID)) {
			
			$this->order_id = $pID;
			
			$query = 'select o.*, customers_gender from '. TABLE_ORDERS .' o, '. TABLE_CUSTOMERS .' c
					  where o.customers_id=c.customers_id
					  and o.orders_id='. $this->order_id;
			$query = tep_db_query($query);
			$data = tep_db_fetch_array($query);
			
			$this->order_infos = $data;
			
			$query = 'select * from '. TABLE_ORDERS_PRODUCTS .' where orders_id='. $this->order_id;
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$this->order_products[] = $data;
			}
		}
	}
	
	public function getId() {
		
		return $this->order_id;
	}
	
	public function getInfos() {
		
		return $this->order_infos;
	}
	
	/*
	 *
	 */
	public function getProducts() {
		
		return $this->order_products;
	}
	
	/*
	 *
	 */
	public function getCountryIso() {
		
		if(empty($this->order_iso)) {
			
			$query = 'select countries_iso_code_2 from '. TABLE_COUNTRIES .' where countries_name=\''. $this->order_infos['delivery_country'] .'\'';
			$query = tep_db_query($query);
			$data = tep_db_fetch_array($query);
			$this->order_iso = $data['countries_iso_code_2'];
		}
		
		return $this->order_iso;
	}
	
	
	
	/*
	 *
	 */
	public function getSignature() {
		
		$TRANCHE_SUP = 79.99; // au DESSUS du montant on envoi AVEC remise contre signature
		$TRANCHE_MOINS = 80; // au DESSOUS du montant on envoi SANS remise contre signature
		$TRANCHE_SUP_OM = 79.99; // au DESSUS du montant on envoi AVEC remise contre signature
		$TRANCHE_MOINS_OM = 80;
		
		$ISO = $this->order_iso;
		
		if($ISO=='FR' && $this->order_infos['total']<$TRANCHE_MOINS) $return = 'COLD';
		elseif ($ISO=='FR' && $this->order_infos['total']>$TRANCHE_SUP) $return = 'COL';
		elseif($ISO=='BE' || $ISO=='DE' || $ISO=='LU' || $ISO=='NL' || $ISO=='ES' || $ISO=='IT' || $ISO=='GB' || $ISO=='AT' || $ISO=='DK' || $ISO=='IE' || $ISO=='PT' || $ISO=='FI' || $ISO=='NO' || $ISO=='SE' || $ISO=='CH') $return = 'COLI';
		elseif(($ISO=='GP' || $ISO=='MQ' || $ISO=='GF' || $ISO=='RE' || $ISO=='YT' || $ISO=='PM' || $ISO=='NC' || $ISO=='PF' || $ISO=='WF') && $this->order_infos['total']<$TRANCHE_MOINS_OM) $return = 'COM';
		elseif(($ISO=='GP' || $ISO=='MQ' || $ISO=='GF' || $ISO=='RE' || $ISO=='YT' || $ISO=='PM' || $ISO=='NC' || $ISO=='PF' || $ISO=='WF') && $this->order_infos['total']>$TRANCHE_SUP_OM) $return = 'CDS';
		
		return $return;
	}
	
	/*
	 *
	 */
	public function hasCustoms() {
		
		if(empty($this->order_iso)) $this->getCountryIso();
		
		$signature = $this->getSignature();
		
		return ($this->order_iso=='CH' || $signature=='COM' || $signature=='CDS') ? true : false;
	}
	
	public function getProductsToSend() {
		
		if(empty($this->order_products_to_send)) {
			
			$query = 'select * from '. TABLE_ORDERS_PRODUCTS .' where products_quantity_saisie>0 and orders_id='. $this->order_id;
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$this->order_products_to_send[] = $data;
			}
		}
		
		return $this->order_products_to_send;
	}
}
?>