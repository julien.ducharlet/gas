<?php

(empty($_GET['ajax'])) ? include_once('entity.php') : include_once('entity.php');

class category extends entity{
	
	private $category_id = 0;
	private $category_infos = array();
	private $rubrique;
	private $propertiesInfos = array();
	private $properties = array();//à implémenter
	
	public function __construct($pID='') {
		
		if(!empty($pID)) {
			
			$this->category_id = $pID;
			$this->setInfos();
		}
	}
	
	
	public function getId() {
		
		return $this->category_id;
	}
	
	public function getInfos() {
		
		return $this->category_infos;
	}
	
	/*
	 *
	 * retourne toutes les catégories alias basées sur ce modèle
	*/
	public function getAllAlias() {
		
		$alias = array();
		
		if(!empty($this->category_infos['alias'])) {
			
			$alias_id = $this->category_infos['alias'];
			
			$query = 'select c.categories_id, categories_name, parent_id from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd where c.categories_id=cd.categories_id and c.categories_id='. $alias_id;
			$query = tep_db_query($query);
			$data = tep_db_fetch_array($query);
			
			$alias[$data['categories_id']] = $data;
		}
		else {
			
			$alias_id = $this->category_id;
			
			$query = 'select c.categories_id, categories_name, parent_id from '. TABLE_CATEGORIES  .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd where c.categories_id=cd.categories_id and alias='. $alias_id;
			$query = tep_db_query($query);
			
			if(tep_db_num_rows($query)>0) {
				
				$data = tep_db_fetch_array($query);
				
				$alias[$data['categories_id']] = $data;
			}
		}
		
		$query = 'select c.categories_id, categories_name from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd where c.categories_id=cd.categories_id and alias='. $alias_id;
		$query = tep_db_query($query);
		
		if(tep_db_num_rows($query)>0) {
			
			while($data = tep_db_fetch_array($query)) {
				
				$query_select = 'select c.categories_id, categories_name, parent_id from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd where c.categories_id=cd.categories_id and c.categories_id='. $data['categories_id'];
				$query_select = tep_db_query($query_select);
				
				$data_select = tep_db_fetch_array($query_select);
				
				$alias[$data_select['categories_id']] = $data_select;
			}
		}
		
		return $alias;
	}
	
	public function getAllProductsFromFamilyClient($pID) {
		
		$query = 'select * from '. TABLE_PRODUCTS_TO_CATEGORIES .' ptc, '. TABLE_PRODUCTS .' p, '. TABLE_PRODUCTS_DESCRIPTION .' pd
					where ptc.products_id=p.products_id
					and p.products_id=pd.products_id
					and ptc.categories_id='. $this->category_id .'
					and family_client_id='. $pID .'
					group by ptc.products_id';
		$query = tep_db_query($query);
		while($data = tep_db_fetch_array($query)) {
			
			$query_count = 'select * from '. TABLE_PRODUCTS_TO_CATEGORIES .' where products_id='. $data['products_id'];
			$query_count = tep_db_query($query_count);
			$data['nb_cat'] = tep_db_num_rows($query_count);
			
			$data['products_name'] = bda_product_name_transform($data['products_name'], $this->category_id);
			$result[] = $data;
		}
		
		return $result;
	}
	
	public function getAllTree() {
		
		$marques = array();
		
		$rubrique = $this->getRubrique();
		if(empty($rubrique['rubrique_id'])) $rubrique['rubrique_id'] = 1;
		
		$query = 'select marque_desc.categories_name as marque_name, modele_desc.categories_id as modele_id, modele_desc.categories_name as modele_name from '. TABLE_CATEGORIES .' modele, '. TABLE_CATEGORIES_DESCRIPTION .' modele_desc, '. TABLE_CATEGORIES_DESCRIPTION .' marque_desc, '. TABLE_CATEGORIES_RAYON .' cr where modele_desc.categories_id=modele.categories_id and modele.parent_id=marque_desc.categories_id and marque_desc.categories_id=cr.categories_id and cr.rubrique_id='. $rubrique['rubrique_id'] .' order by marque_desc.categories_name, modele_desc.categories_name';
		$query = tep_db_query($query);
		
		while($data = tep_db_fetch_array($query)) {
			
			$marques[$data['marque_name']][] = array('modele_id' => $data['modele_id'], 'modele_name' => $data['modele_name']);
		}
		
		return $marques;
	}
	
	/*
	 *
	 * 
	*/
	public function getAlias() {
		
		$alias = array();
			
		if(!empty($this->category_id)) {
			
			$query = 'select c.categories_id, categories_name from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd where c.categories_id=cd.categories_id and alias='. $this->category_id;
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$alias[] = $data;
			}
		}
		
		return $alias;
	}
	
	/*
	 *
	 * retourne les informations sur la catégorie qui sert de modèle d'alias
	*/
	public function getAliasModel() {
		
		if(!empty($this->category_infos['alias'])) {
			
			$query = 'select categories_id, categories_name from '. TABLE_CATEGORIES_DESCRIPTION .' where categories_id='. $this->category_infos['alias'];
			$query = tep_db_query($query);
			
			return tep_db_fetch_array($query);
		}
		else return '';
	}
	
	public function getCategoriesFromMarque($pMarque = 0) {
		
		$marque = ($pMarque == 0) ? $this->category_infos['parent_id'] : $pMarque;
		$categories = array();
		
		$query = 'select c.categories_id, categories_name from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd where c.categories_id=cd.categories_id and parent_id='. $marque .' and c.categories_id!='. $this->category_id .' order by categories_name';
		$query = tep_db_query($query);
		
		while($data = tep_db_fetch_array($query)) {
			
			$categories[] = $data;
		}
		
		return $categories;
	}
	
	public function getCountCustomers() {
		
		$query = 'select count(*) as total from '. TABLE_CUSTOMERS_MOBILE .' where mobile_id='. $this->category_id;
		$query = tep_db_query($query);
		
		$data = tep_db_fetch_array($query);
		
		return $data['total'];
	}
	
	public function getDefaultBaseline() {
		
		$rayonAllBaseline = array();
		$allAdwords = array();
		
		$parentInfos = $this->getParentInfos();
		
		$query = 'select * from '. TABLE_CATEGORIES_RAYON .' cr, '. TABLE_RAYON_BASELINE .' rb
				  where cr.rubrique_id=rb.rubrique_id
				  and cr.categories_id='. $this->category_infos['parent_id'];
		$query = tep_db_query($query);
		
		
		while($data = tep_db_fetch_array($query)) {
			
			$rayonAllBaseline[] = $data['rubrique_baseline_name'];
		}
		
		$allResults = array();
		
		$adwords = (!empty($this->category_infos['adwords_exp_large'])) ? $this->category_infos['adwords_exp_large'] : '';
		if(!empty($this->category_infos['adwords_exp_exact'])) {
			
			if(!empty($this->category_infos['adwords_exp_large'])) $adwords .= ', ';
			$adwords .= $this->category_infos['adwords_exp_exact'];
		}
		
		$adwords = str_replace(', ', ',', $adwords);
		$allAdwords = explode(',', $adwords);
		
		foreach($allAdwords as $adword) {
			
			$allAdwords[] = $parentInfos['categories_name'] .' '. $adword;
		}
		
		
		foreach($rayonAllBaseline as $rayonBaseline) {
			
			foreach($allAdwords as $adword) {
				
				$allResults[] = $rayonBaseline .' '. $adword;
			}
		}
		
		return $parentInfos['categories_name'] .' '. $this->category_infos['categories_name'] .', '. implode(', ', $allResults);
	}
	
	public function getRubrique() {
		
		$parent = ($this->category_infos['parent_id']==0) ? $this->category_id : $this->category_infos['parent_id'];
		
		$this->rubrique = new rubrique();
		$this->rubrique->setInfosFromCategory($parent);
		
		return $this->rubrique->getInfos();
	}
	
	public function getParentInfos($pID=0) {
		
		$query = 'select * from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd
				  where c.categories_id=cd.categories_id
				  and c.categories_id='. (($pID>0) ? $pID : $this->category_infos['parent_id']);
		$query = tep_db_query($query);
		
		return tep_db_fetch_array($query);
	}
	
	/*
	 * retourne toutes les informations des propriétés de la catégorie
	 */
	public function getProperties($pRefresh = false) {
		
		$properties = array();
		
		if(empty($this->properties) || $pRefresh) {
			
			$query = 'select * from '. TABLE_PROPERTIES_INFOS_TO_CATEGORIES .' pdtc,
									'. TABLE_PROPERTIES_INFOS .' pd,
									'. TABLE_PROPERTIES_CATEGORIES .' p
							   where pdtc.properties_infos_id=pd.properties_infos_id
							   and pd.properties_categories_id=p.properties_categories_id
							   and categories_id='. $this->category_id;
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$properties[] = $data;
			}
			
			$this->properties = $properties;
			
		}
		
		return $this->properties;
	}
	
	public function getPropertiesId() {
		
		if(empty($this->propertiesInfos)) {
			
			$query = 'select * from '. TABLE_PROPERTIES_INFOS_TO_CATEGORIES .' where categories_id='. $this->category_id;
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$this->propertiesInfos[] = $data['properties_infos_id'];
			}
		}
		
		return $this->propertiesInfos;
	}
	
	/*
	 *
	 * retourne les propriétés classées par leurs catégories de propriétés
	 */
	private function getRangedProperties($pProduct) {
		
		$properties = array();
		
		$query = 'select pi.properties_infos_id, pc.properties_categories_id from '. TABLE_PROPERTIES_INFOS_TO_PRODUCTS .' pitp, '. TABLE_PROPERTIES_INFOS .' pi, '. TABLE_PROPERTIES_CATEGORIES .' pc
						   where pitp.properties_infos_id=pi.properties_infos_id
						   and pi.properties_categories_id=pc.properties_categories_id
						   and products_id='. $pProduct .'
						   order by pc.properties_categories_id';
		$query = tep_db_query($query);
		
		while($data = tep_db_fetch_array($query)) {
			
			//if(!is_array($properties[$data['properties_categories_id']])) $properties[$data['properties_categories_id']] = array();
			
			$properties[$data['properties_categories_id']][] = $data['properties_infos_id'];
		}
		
		return $properties;
	}
	
	
	
	public function setInfosFromParent($pID) {
	
		$query = 'select * from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd
				  where c.categories_id=cd.categories_id
				  and c.parent_id='. $pID;
		$query = tep_db_query($query);
		
		$this->category_infos = tep_db_fetch_array($query);
		$this->category_id = $this->category_infos['categories_id'];
	}
	
	private function setInfos($pRefresh = false) {
		
		if(empty($this->category_infos) || $pRefresh) {
			
			$query = 'select * from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd
					  where c.categories_id=cd.categories_id
					  and c.categories_id='. $this->category_id;
			$query = tep_db_query($query);
			
			$this->category_infos = tep_db_fetch_array($query);
		}
	}
	
	public function setParent($pID) {
		
		$this->category_infos['parent_id'] = $pID;
	}
	
	
	
	
	
	
	public function registerCategory(array $pField, $pTypeQuery) {
		
		switch($pTypeQuery) {
			
			case 'update':
				
				if(!empty($this->category_id)) {
					
					$this->setTable(TABLE_CATEGORIES);
					$this->setField($pField);
					
					$where = 'categories_id='. $this->getId();
					
					$this->executeQuery('update', $where);
				}
				break;
				
			case 'insert':
					
				$this->setTable(TABLE_CATEGORIES);
				$this->setField($pField);
				
				$this->executeQuery('insert');
				
				$this->category_id = $this->getLastInsertedId();
				break;
				
			case 'delete':
				
				//suppression des catégories enfants
				$this->setTable(TABLE_CATEGORIES);
				$where = 'parent_id='. $this->getId();
				$this->executeQuery('delete', $where);
				
				//suppression des produits de la catégorie
				$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
				$where = 'categories_id='. $this->getId();
				$this->executeQuery('delete', $where);
				
				//Suppression de la liaison entre la catégorie et le rayon
				$this->setTable(TABLE_CATEGORIES_RAYON);
				$where = 'categories_id='. $this->getId();
				$this->executeQuery('delete', $where);
				
				
				//suppression des propriétés de la catégorie
				$this->setTable(TABLE_PROPERTIES_INFOS_TO_CATEGORIES);
				$where = 'categories_id='. $this->getId();
				$this->executeQuery('delete', $where);
				
				//suppression des compatibilités
				$this->setTable(TABLE_PRODUCTS_COMPATIBILITIES);
				$where = 'categories_id='. $this->getId();
				$this->executeQuery('delete', $where);
				
				//suppression de la catégorie
				$this->setTable(TABLE_CATEGORIES);
				$where = 'categories_id='. $this->getId();
				$this->executeQuery('delete', $where);
				
				//suppression des catégories enfants
				$this->setTable(TABLE_CATEGORIES_DESCRIPTION);
				$where = 'categories_id='. $this->getId();
				$this->executeQuery('delete', $where);
				
				break;
			
			case 'duplicate':
				
				############################
				$arrayField = array();
				
				$arrayField['sort_order'] = $this->category_infos['sort_order'];
				$arrayField['categories_status_rev'] = $this->category_infos['categories_status_rev'];
				$arrayField['parent_id'] = $this->category_infos['parent_id'];
				$arrayField['categories_status'] = 0;
				$arrayField['categories_status_tous'] = $this->category_infos['categories_status_tous'];
				$arrayField['categories_status_adm'] = $this->category_infos['categories_status_adm'];
				$arrayField['categories_status_client'] = $this->category_infos['categories_status_client'];
				$arrayField['categories_status_pro'] = $this->category_infos['categories_status_pro'];
				$arrayField['categories_status_vip'] = $this->category_infos['categories_status_vip'];
				$arrayField['categories_is_adwords'] = $this->category_infos['categories_is_adwords'];
				$arrayField['date_added'] = date('Y-m-d');
				
				$this->setTable(TABLE_CATEGORIES);
				$this->setField($arrayField);
				$this->executeQuery('insert');
				
				$categoryInserted = $this->getLastInsertedId();
				
				############################
				$arrayField = array();
				
				$arrayField['categories_id'] = $categoryInserted;
				$arrayField['categories_name'] = 'AAA NEW CATEGORIE';
				$arrayField['categories_htc_title_tag'] = $this->category_infos['categories_htc_title_tag'];
				$arrayField['categories_htc_desc_tag'] = $this->category_infos['categories_htc_desc_tag'];
				$arrayField['categories_htc_keywords_tag'] = $this->category_infos['categories_htc_keywords_tag'];
				$arrayField['categories_htc_description'] = $this->category_infos['categories_htc_description'];
				$arrayField['balise_title_lien_image'] = $this->category_infos['balise_title_lien_image'];
				$arrayField['balise_title_lien_texte'] = $this->category_infos['balise_title_lien_texte'];
				$arrayField['categories_baseline'] = $this->category_infos['categories_baseline'];
				$arrayField['categories_before_title'] = $this->category_infos['categories_before_title'];
				$arrayField['language_id'] = $this->category_infos['language_id'];
				
				$this->setTable(TABLE_CATEGORIES_DESCRIPTION);
				$this->setField($arrayField);
				$this->executeQuery('insert');
				
				############################
				/*$arrayField = array();
				
				$query = 'select * from '. TABLE_CATEGORIES_RAYON .' where categories_id='. $this->category_id;
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$arrayField['rubrique_id'] = $data['rubrique_id'];
					$arrayField['categories_id'] = $categoryInserted;
					
					$this->setTable(TABLE_CATEGORIES_RAYON);
					$this->setField($arrayField);
					$this->executeQuery('insert');
				}*/
				
				############################
				$arrayField = array();
				
				$query = 'select * from '. TABLE_PRODUCTS_TO_CATEGORIES .' where categories_id='. $this->category_id;
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$arrayField['products_id'] = $data['products_id'];
					$arrayField['categories_id'] = $categoryInserted;
					
					$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
					$this->setField($arrayField);
					$this->executeQuery('insert');
				}
				
				############################
				$arrayField = array();
				
				$query = 'select * from '. TABLE_PRODUCTS_COMPATIBILITIES .' where categories_id='. $this->category_id;
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$arrayField['products_id'] = $data['products_id'];
					$arrayField['categories_id'] = $categoryInserted;
					
					$this->setTable(TABLE_PRODUCTS_COMPATIBILITIES);
					$this->setField($arrayField);
					$this->executeQuery('insert');
				}
				
				############################
				$arrayField = array();
				
				$query = 'select * from '. TABLE_PROPERTIES_INFOS_TO_CATEGORIES .' where categories_id='. $this->category_id;
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$arrayField['properties_infos_id'] = $data['properties_infos_id'];
					$arrayField['categories_id'] = $categoryInserted;
					
					$this->setTable(TABLE_PROPERTIES_INFOS_TO_CATEGORIES);
					$this->setField($arrayField);
					$this->executeQuery('insert');
				}
				
				break;
			
			case 'create_alias':
				
				############################ INFOS
				$arrayField = array();
				
				$arrayField['sort_order'] = $this->category_infos['sort_order'];
				$arrayField['categories_image2'] = $this->category_infos['categories_image2'];
				$arrayField['categories_size'] = $this->category_infos['categories_size'];
				$arrayField['parent_id'] = $this->category_infos['parent_id'];
				$arrayField['categories_status'] = 0;
				$arrayField['categories_status_tous'] = $this->category_infos['categories_status_tous'];
				$arrayField['categories_status_client'] = $this->category_infos['categories_status_client'];
				$arrayField['categories_status_adm'] = $this->category_infos['categories_status_adm'];
				$arrayField['categories_status_pro'] = $this->category_infos['categories_status_pro'];
				$arrayField['categories_status_rev'] = $this->category_infos['categories_status_rev'];
				$arrayField['categories_status_vip'] = $this->category_infos['categories_status_vip'];
				$arrayField['categories_is_adwords'] = $this->category_infos['categories_is_adwords'];
				$arrayField['date_added'] = date('Y-m-d');
				
				$this->setTable(TABLE_CATEGORIES);
				$this->setField($arrayField);
				$this->executeQuery('insert');
				
				$categoryInserted = $this->getLastInsertedId();
				
				
				############################ DESCRIPTION
				$arrayField = array();
				
				$arrayField['categories_id'] = $categoryInserted;
				$arrayField['categories_name'] = $this->category_infos['categories_name'] .' - 2';
				$arrayField['categories_htc_title_tag'] = $this->category_infos['categories_htc_title_tag'];
				$arrayField['categories_htc_desc_tag'] = $this->category_infos['categories_htc_desc_tag'];
				$arrayField['categories_htc_keywords_tag'] = $this->category_infos['categories_htc_keywords_tag'];
				$arrayField['categories_htc_description'] = $this->category_infos['categories_htc_description'];
				$arrayField['balise_title_lien_image'] = $this->category_infos['balise_title_lien_image'];
				$arrayField['balise_title_lien_texte'] = $this->category_infos['balise_title_lien_texte'];
				$arrayField['categories_before_title'] = $this->category_infos['categories_before_title'];
				$arrayField['language_id'] = $this->category_infos['language_id'];
				
				$this->setTable(TABLE_CATEGORIES_DESCRIPTION);
				$this->setField($arrayField);
				$this->executeQuery('insert');
				
				
				############################ PRODUCTS
				$arrayField = array();
				
				$query = 'select * from '. TABLE_PRODUCTS_TO_CATEGORIES .' where categories_id='. $this->category_id;
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$arrayField['products_id'] = $data['products_id'];
					$arrayField['categories_id'] = $categoryInserted;
					
					$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
					$this->setField($arrayField);
					$this->executeQuery('insert');
				}
				
				############################ COMPATIBILITIES
				$arrayField = array();
				
				$query = 'select * from '. TABLE_PRODUCTS_COMPATIBILITIES .' where categories_id='. $this->category_id;
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$arrayField['products_id'] = $data['products_id'];
					$arrayField['categories_id'] = $categoryInserted;
					
					$this->setTable(TABLE_PRODUCTS_COMPATIBILITIES);
					$this->setField($arrayField);
					$this->executeQuery('insert');
				}
				
				############################ PROPERTIES
				$arrayField = array();
				
				$query = 'select * from '. TABLE_PROPERTIES_INFOS_TO_CATEGORIES .' where categories_id='. $this->category_id;
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$arrayField['properties_infos_id'] = $data['properties_infos_id'];
					$arrayField['categories_id'] = $categoryInserted;
					
					$this->setTable(TABLE_PROPERTIES_INFOS_TO_CATEGORIES);
					$this->setField($arrayField);
					$this->executeQuery('insert');
				}
				
				
				$categoryCreated = new category($categoryInserted);
				$categoryCreated->updateBaseline();
				$categoryCreated->registerAlias($this->category_id);
				
				return $categoryInserted;
				break;
		}
	}
	
	public function registerDescription(array $pField, $pTypeQuery) {
		
		switch($pTypeQuery) {
			
			case 'update':
				
				if(!empty($this->category_id)) {
					
					$this->setTable(TABLE_CATEGORIES_DESCRIPTION);
					$this->setField($pField);
					
					$where = 'categories_id='. $this->getId();
					
					$this->executeQuery('update', $where);
				}
				break;
			case 'insert':
					
				$this->setTable(TABLE_CATEGORIES_DESCRIPTION);
				$this->setField($pField);
				
				$this->executeQuery('insert');
				
				$this->setInfos();
				break;
		}
	}
	
	public function registerRubrique($pID) {
		
		$field = array();
		$field['categories_id'] = $this->category_id;
		$field['rubrique_id'] = $pID;
		
		$this->setTable(TABLE_CATEGORIES_RAYON);
		$this->setField($field);
		
		$this->executeQuery('insert');
	}
	
	public function registerAlias($cat) {
		
		$this->setTable(TABLE_CATEGORIES);
		
		$temp_cat = new category($cat);
		
		$arrayField = array();
		
		if($temp_cat->category_infos['alias']>0) {//le téléphone sélectionné a déjà un alias et ne peut donc pas servir de maître
			
			$pAlias = $temp_cat->category_infos['alias'];
			$arrayField['alias'] = $pAlias;
			
			$where = 'categories_id='. $this->category_id;
			
			$this->setField($arrayField);
			
			$this->executeQuery('update', $where);
		}
		else {
			
			$arrayField['alias'] = $this->category_id;
			
			$where = 'categories_id='. $cat;
			
			$this->setField($arrayField);
			
			$this->executeQuery('update', $where);
		}
	}
	
	/*
	 *
	 */
	public function registerProperty($pProperty) {
		
		$arrayField = array();
		$arrayField['properties_infos_id'] = $pProperty;
		$arrayField['categories_id'] = $this->category_id;
		
		
		//on récupère les informations de toutes les propriétés de la catégorie
		$allProperties = $this->getProperties();
		
		//on récupère l'id de la catégorie de la propriété à enregistrer pour vérifier si une autre option pour cette catégrie est déjà rentrée
		$propertyCategory = property::getPropertyCategory($pProperty);
		
		
		$propertyToModify = $this->verifIfProperpertyOfTheSameCategoryExists($propertyCategory['properties_categories_id'], $allProperties);
		
		//si aucune option n'a déjà été rentrée -> insertion
		if(!$propertyToModify) {
			
			$this->setTable(TABLE_PROPERTIES_INFOS_TO_CATEGORIES);
			
			$this->setField($arrayField);
			
			$this->executeQuery('insert', '', true);
		}
		elseif($pProperty!=$propertyToModify) {//sinon si la propriété à insérer est différente de celle déjà présente -> update
			
			$arrayField['properties_infos_id'] = $pProperty;
			
			$this->setTable(TABLE_PROPERTIES_INFOS_TO_CATEGORIES);
			$this->setField($arrayField);
			
			$where = 'categories_id='. $this->category_id .' and properties_infos_id='. $propertyToModify;
			
			$this->executeQuery('update', $where, true);
		}
	}
	
	
	
	/*
	 *
	 *
	 */
	public function deletePropertyForPropertyCategory($pCategory) {
		
		$query = 'select pi.properties_infos_id from '. TABLE_PROPERTIES_INFOS_TO_CATEGORIES .' pitc, '. TABLE_PROPERTIES_INFOS .' pi
				  where pitc.properties_infos_id=pi.properties_infos_id
				  and pi.properties_categories_id='. $pCategory .' and categories_id='. $this->category_id;
		
		$query = tep_db_query($query);
		
		if(tep_db_num_rows($query)>0) {
			
			$data = tep_db_fetch_array($query);
			
			$propertyToDelete = $data['properties_infos_id'];
		
			$this->setTable(TABLE_PROPERTIES_INFOS_TO_CATEGORIES);
			
			$where = 'categories_id='. $this->category_id .' and properties_infos_id='. $propertyToDelete;
			
			$this->executeQuery('delete', $where, true);
			
			$this->updateLinkToProducts();
		}
	}
	
	public function deleteAlias($pAlias) {
		
		$this->setTable(TABLE_CATEGORIES);
		
		$arrayField = array();
		$arrayField['alias'] = '';
		
		$this->setField($arrayField);
		
		$where = 'categories_id='. $pAlias .' and alias='. $this->category_id;
		
		$this->executeQuery('update', $where);
	}
	
	
	/*
	 * vérifie si une propriété appartenant à la même catégorie a déjà été enregistrée
	 */
	private function verifIfProperpertyOfTheSameCategoryExists($pPropertyCategoryId, array $pAllPropertyCategory) {
		
		$exist = false;
		
		foreach($pAllPropertyCategory as $propertyArray) {
			
			//on retourn l'id de la propriété qu'il faudra modifier
			if($pPropertyCategoryId == $propertyArray['properties_categories_id']) $exist = $propertyArray['properties_infos_id'];
		}
		
		return $exist;
	}
	
	/*
	 *
	 *
	 */
	private function isProductCompatible($pProduct) {
		
		$compatibility = true;
		$tempCompatibility = false;
		$productCategoriesProperties = array();
		$categoryProperties = $this->getPropertiesId();
		
		
		///on récupère toutes les propriétés du produit
		$productCategoriesProperties = $this->getRangedProperties($pProduct);
		
		//on les compare aux propriétés du produit.
		foreach($productCategoriesProperties as $productCategoriesProperty) {
			
			$tempCompatibility = false;
			
			foreach($productCategoriesProperty as $productProperty) {
				
				if(in_array($productProperty, $categoryProperties)) {
					
					$tempCompatibility = true;
				}
			}
			
			$compatibility *= $tempCompatibility;
		}
		return $compatibility;
	}
	
	public function isAlias() {
		
		$query = 'select * from '. TABLE_CATEGORIES .' where alias='. $this->category_id;
		$query = tep_db_query($query);
		
		return (tep_db_num_rows($query) > 0) ? true : false;
	}
	
	public function hasProducts(){
		
		$query = 'select * from '. TABLE_PRODUCTS_TO_CATEGORIES .' where categories_id='. $this->category_id;
		$query = tep_db_query($query);
		
		return tep_db_num_rows($query);
	}
	
	/*
	 *
	 * met à jour la liaison entre la catégorie et les produits possédant les mêmes propriétés
	 */
	public function updateLinkToProducts() {
		
		$arrayField = array();
		
		$this->getProperties(true);//on récupère toutes les propriétés
		
		//on récupère tous les produits compatibles
		$query = 'select products_id from '. TABLE_PRODUCTS_COMPATIBILITIES .' where categories_id='. $this->category_infos['parent_id'];
		$query = tep_db_query($query);
		
		
		while($data = tep_db_fetch_array($query)) {
					
			if($this->isProductCompatible($data['products_id'])) {
				
				$arrayField['products_id'] = $data['products_id'];
				$arrayField['categories_id'] = $this->category_id;
				
				$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
				$this->setField($arrayField);
				
				$this->executeQuery('insert', '', true);
			}
			else {
				
				$where = 'products_id='. $data['products_id'] .' and categories_id='. $this->category_id;
				
				$this->setTable(TABLE_PRODUCTS_TO_CATEGORIES);
				
				$this->executeQuery('delete', $where);
			}
		}
	}
	
	public function updateBaseline() {
		
		$arrayField = array();
		
		$this->setInfos(true);
		
		$arrayField['categories_baseline'] = $this->getDefaultBaseline();
		
		$this->registerDescription($arrayField, 'update');
	}
}


?>