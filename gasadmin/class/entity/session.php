<?php

abstract class session {
	
	private $field;
	
	public function addMessage($pType, $pMessage) {
		
		if(empty($this->field)) $this->field = 'tempSession';
		
		$_SESSION[$this->field][$pType][] = $pMessage;
	}
	
	public function delete() {
		
		unset($_SESSION[$this->field]);
	}
}

?>