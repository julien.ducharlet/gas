<?php

include('class/view/showInfosBanner.php');


class showInfosBanner {
	
	private $banner;
	
	function __construct(infosBanner $myBanner) {
		
		$this->banner = $myBanner;
	}
	
	
	
	
	function show() {
		
		$title = $this->banner->getTitle();
		$elements = $this->banner->getElementList();
		
		$return .= '<table class="client_header">';
			
			if(!empty($title)) {
				
				$return .= '<thead>';
					$return .= '<tr>';
						$return .= '<th colspan="'. sizeof($elements['img']) .'" valign="middle">'. $title .'</th>';
				$return .= '</thead>';
			}
			
			$return .= '<tbody>';
				
				$return .= '<tr>';
					
					foreach($elements as $element) {
						
						$return .='<td valign="middle" '. (($_SERVER['REQUEST_URI']==$element['link']) ? 'class="selected" ' : '') . $element['opt'] .'>';
							$return .= '<div>';
								$return .= '<img src="'. $element['img'] .'"/>';
								
								if(!empty($element['link'])) $return .= '<a href="'. $element['link'] .'">';
								
								$return .= $element['txt'];
								
								if(!empty($element['link'])) $return .= '</a>';
								
								$return .= '</div>';
						$return .= '</td>';
					}
					
				$return .= '</tr>';
			$return .= '</tbody>';
		$return .= '</table>';
		
		return $return;
	}
}

?>