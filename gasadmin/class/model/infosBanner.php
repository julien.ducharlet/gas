<?php

class infosBanner {
	
	private $title;
	
	/*
	 * @des : tableau contenant la liste des informations d'une case
	 *
	*/
	private $link = array('img' => array(), 'txt' => array(), 'link' => array(), 'opt' => array());
	
	function __construct($title) {
		
		$this->title = $title;
	}
	
	
	function addElement($img, $txt, $link='', $options='') {
		
		$size = sizeof($this->link['img']);
		
		$this->link[$size]['img'] = $img;
		$this->link[$size]['txt'] = $txt;
		$this->link[$size]['link'] = $link;
		$this->link[$size]['opt'] = $options;
	}
	
	function getTitle() {
		
		return $this->title;
	}
	function getElementList() {
		
		return $this->link;
	}
}
?>