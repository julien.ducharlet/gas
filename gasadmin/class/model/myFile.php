<?php
class myFile {
	
	var $file;
	var $pathToFile;
	var $fileName;
	
	function myFile($path, $fileName) {
		
		$this->pathToFile = $path;
		$this->fileName = $fileName;
	}
	
	function open($options) {
		
		$this->file = fopen($this->pathToFile . $this->fileName, $options);
	}
	
	function close() {
		
		return fclose($this->file);
	}
	
	function append($content) {
		
		$this->open('a');
		
		if(fwrite($this->file, $content) === false) {
			
			echo 'Echec lors de l\'�criture � la suite du fichier '. $this->pathToFile . $this->fileName;
		}
		
		$this->close();
	}
	
	
	
	function write($content) {
		
		$this->open('w');
		
		if(fwrite($this->file, $content) === false) {
			
			echo 'Echec lors de l\'�criture du fichier '. $this->pathToFile . $this->fileName;
		}
		
		$this->close();
	}
	
	function getContent() {
		
		$content = array();
		while(!feof($this->file)) {
			
			$content[] = fgets($this->file);
		}
		
		return $content;
	}
}
?>