<?php
/*
  Page: quick_updates.php

  Based on the original script contributed by Burt (burt@xwww.co.uk)
        and by Henri Bredehoeft (hrb@nermica.net)

  This version was contributed by Mathieu (contact@mathieueylert.com)
*/


########################################################################
########################################################################
#						FAIRE LES MODIFICATION A LA LIGNE 400
########################################################################
########################################################################

	// Nombre de lignes par page 
	//$nombre_de_ligne_par_page=20;
	// Nombre de lignes dans les resultats de recherche
	//$nombre_de_resultat_maximum=20;
	

  require('includes/application_top.php');

	$row_by_page = (isset($_GET['row_by_page']) && !empty($_GET['row_by_page'])) ? $_GET['row_by_page'] : 100;
	
	define('MAX_DISPLAY_ROW_BY_PAGE' , $row_by_page );

//// Tax Row
    $tax_class_array = array(array('id' => '0', 'text' => '- N/A -'));
    $tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
    while ($tax_class = tep_db_fetch_array($tax_class_query)) {
      $tax_class_array[] = array('id' => $tax_class['tax_class_id'],
                                 'text' => $tax_class['tax_class_title']);
    }

//  Info Row pour le champ fabriquant
        $manufacturers_array = array(array('id' => '0', 'text' => '- N/A -'));
        $manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . " order by manufacturers_name ");
        while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
                $manufacturers_array[] = array('id' => $manufacturers['manufacturers_id'],
                'text' => $manufacturers['manufacturers_name'] . ' - ' . $manufacturers['manufacturers_id']);
        }

//  Info Row pour le champ famille article client
        $family_client_array = array(array('id' => '0', 'text' => 'SANS FAMILLE ARTICLE CLIENT'));
        $family_client_query = tep_db_query("select family_client_id, family_client_name from " . TABLE_CLIENT_FAMILY . " order by family_client_name ");
        while ($family_client = tep_db_fetch_array($family_client_query)) {
                $family_client_array[] = array('id' => $family_client['family_client_id'],
                'text' => $family_client['family_client_name'] . ' - ' . $family_client['family_client_id'] );
        }

//  Info Row pour le champ famille article 
        $family_array = array(array('id' => '0', 'text' => 'SANS FAMILLE ARTICLE'));
        $family_query = tep_db_query("select family_id, family_name from " . TABLE_FAMILY . " order by family_name ");
        while ($family = tep_db_fetch_array($family_query)) {
                $family_array[] = array('id' => $family['family_id'],
                'text' => $family['family_name'] . ' - ' . $family['family_id']);
        }

// Affichage de la liste des type d'article / des fabricants 
function familys_list(){
        global $manufacturer;

        $manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . " order by manufacturers_name ASC");
        $return_string = '<select name="manufacturer" onChange="this.form.submit();">';
        $return_string .= '<option value="0">Toutes les Marques</option>';
        while($manufacturers = tep_db_fetch_array($manufacturers_query)){
                $return_string .= '<option value="' . $manufacturers['manufacturers_id'] . '"';
                if($manufacturer && $manufacturers['manufacturers_id'] == $manufacturer) 
												$return_string .= ' SELECTED';
												$return_string .= '>' . $manufacturers['manufacturers_name'] .'</option>';
												
					}
        $return_string .= '</select>';
        return $return_string;
}



##// Uptade database
  switch ($HTTP_GET_VARS['action']) {
    case 'update' :
      $count_update=0;
      $item_updated = array();
	  
	             
				
                if($HTTP_POST_VARS['product_new_model']){
                   foreach($HTTP_POST_VARS['product_new_model'] as $id => $new_model) {
                         if (trim($HTTP_POST_VARS['product_new_model'][$id]) != trim($HTTP_POST_VARS['product_old_model'][$id])) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_model='" . $new_model . "', products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				
				if($HTTP_POST_VARS['product_new_name']){
                   foreach($HTTP_POST_VARS['product_new_name'] as $id => $new_name) {
                         if (trim($HTTP_POST_VARS['product_new_name'][$id]) != trim($HTTP_POST_VARS['product_old_name'][$id])) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS_DESCRIPTION . " SET products_name='" . $new_name . "' WHERE products_id=$id and language_id=" . $languages_id);
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				
				if($HTTP_POST_VARS['product_new_code_barre']){
				   foreach($HTTP_POST_VARS['product_new_code_barre'] as $id => $new_code_barre) {
						 if ($HTTP_POST_VARS['product_new_code_barre'][$id] != $HTTP_POST_VARS['product_old_code_barre'][$id]) {
						   $count_update++;
						   $item_updated[$id] = 'updated';
						   mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_code_barre='". $new_code_barre ."', products_last_modified=now() WHERE products_id=$id");

						 }
				   }
                }
				
				if($HTTP_POST_VARS['product_new_ref_origine']){
				   foreach($HTTP_POST_VARS['product_new_ref_origine'] as $id => $new_ref_origine) {
						 if ($HTTP_POST_VARS['product_new_ref_origine'][$id] != $HTTP_POST_VARS['product_old_ref_origine'][$id]) {
						   $count_update++;
						   $item_updated[$id] = 'updated';
						   mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_ref_origine='". $new_ref_origine ."', products_last_modified=now() WHERE products_id=$id");

						 }
				   }
                }
				
				if($HTTP_POST_VARS['product_new_garantie']){
				   foreach($HTTP_POST_VARS['product_new_garantie'] as $id => $new_garantie) {
						 if ($HTTP_POST_VARS['product_new_garantie'][$id] != $HTTP_POST_VARS['product_old_garantie'][$id]) {
						   $count_update++;
						   $item_updated[$id] = 'updated';
						   mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_garantie='". $new_garantie ."', products_last_modified=now() WHERE products_id=$id");

						 }
				   }
                }
				
                if($HTTP_POST_VARS['product_new_price']){
                   foreach($HTTP_POST_VARS['product_new_price'] as $id => $new_price) {
                         if ($HTTP_POST_VARS['product_new_price'][$id] != $HTTP_POST_VARS['product_old_price'][$id] 
						 && $HTTP_POST_VARS['update_price'][$id] == 'yes') {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_price=$new_price, products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }

				
		//Matt Added Cost 
				if($HTTP_POST_VARS['product_new_cost']){
                   foreach($HTTP_POST_VARS['product_new_cost'] as $id => $new_cost) {
                         if ($HTTP_POST_VARS['product_new_cost'][$id] != $HTTP_POST_VARS['product_old_cost'][$id]) {
						   $count_update++;
                           $item_updated[$id] = 'updated';
						 
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_cost=$new_cost, products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
		//Matt Ended
		
		       if($HTTP_POST_VARS['product_new_weight']){
                   foreach($HTTP_POST_VARS['product_new_weight'] as $id => $new_weight) {
                         if ($HTTP_POST_VARS['product_new_weight'][$id] != $HTTP_POST_VARS['product_old_weight'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_weight=$new_weight, products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				
                if($HTTP_POST_VARS['product_new_quantity_reel']){
                   foreach($HTTP_POST_VARS['product_new_quantity_reel'] as $id => $new_quantity_reel) {
                         if ($HTTP_POST_VARS['product_new_quantity_reel'][$id] != $HTTP_POST_VARS['product_old_quantity_reel'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           $res_stock=mysql_query("select products_quantity from " . TABLE_PRODUCTS . " WHERE products_id=$id");
						   $row_stock=mysql_fetch_array($res_stock);
						   $new_quantity=$row_stock["products_quantity"]+$HTTP_POST_VARS['product_new_quantity_reel'][$id]-$HTTP_POST_VARS['product_old_quantity_reel'][$id];
						   $query_stock="UPDATE " . TABLE_PRODUCTS . " SET products_quantity=$new_quantity, products_quantity_reel=$new_quantity_reel, products_last_modified=now() WHERE products_id=$id";
						   mysql_query($query_stock);
                         }
                   }
                }
				
				// THIERRY STOCK MINI
				if($HTTP_POST_VARS['product_new_quantity_min']){
                   foreach($HTTP_POST_VARS['product_new_quantity_min'] as $id => $new_quantity_mini) {
                         if ($HTTP_POST_VARS['product_new_quantity_min'][$id] != $HTTP_POST_VARS['product_old_quantity_min'][$id]) {
						   $count_update++;
                           $item_updated[$id] = 'updated';

                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_quantity_min=$new_quantity_mini, products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				// THIERRY STOCK MINI END
				// THIERRY STOCK IDEAL
				if($HTTP_POST_VARS['product_new_quantity_ideal']){
                   foreach($HTTP_POST_VARS['product_new_quantity_ideal'] as $id => $new_quantity_ideal) {
                         if ($HTTP_POST_VARS['product_new_quantity_ideal'][$id] != $HTTP_POST_VARS['product_old_quantity_ideal'][$id]) {
						   						 $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_quantity_ideal=$new_quantity_ideal, products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				// THIERRY STOCK IDEAL END
				// THIERRY STOCK MAXI
				if($HTTP_POST_VARS['product_new_quantity_max']){
                   foreach($HTTP_POST_VARS['product_new_quantity_max'] as $id => $new_quantity_max) {
                         if ($HTTP_POST_VARS['product_new_quantity_max'][$id] != $HTTP_POST_VARS['product_old_quantity_max'][$id]) {
						   						 $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_quantity_max=$new_quantity_max, products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				// THIERRY STOCK MAXI END
				
				// THIERRY FAMILLE ARTICLE CLIENT
								if($HTTP_POST_VARS['product_new_family_client']){
                   foreach($HTTP_POST_VARS['product_new_family_client'] as $id => $new_family_client) {
                         if ($HTTP_POST_VARS['product_new_family_client'][$id] != $HTTP_POST_VARS['product_old_family_client'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET family_client_id=$new_family_client, products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				// THIERRY FAMILLE ARTICLE CLIENT END
				
				// THIERRY FAMILLE ARTICLE
								if($HTTP_POST_VARS['product_new_family']){
                   foreach($HTTP_POST_VARS['product_new_family'] as $id => $new_family) {
                         if ($HTTP_POST_VARS['product_new_family'][$id] != $HTTP_POST_VARS['product_old_family'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET family_id=$new_family, products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
				// THIERRY FAMILLE ARTICLE END
				
                if($HTTP_POST_VARS['product_new_manufacturer']){
                   foreach($HTTP_POST_VARS['product_new_manufacturer'] as $id => $new_manufacturer) {
                         if ($HTTP_POST_VARS['product_new_manufacturer'][$id] != $HTTP_POST_VARS['product_old_manufacturer'][$id]) {
                           $count_update++;
                           $item_updated[$id] = 'updated';
                           mysql_query("UPDATE " . TABLE_PRODUCTS . " SET manufacturers_id=$new_manufacturer, products_last_modified=now() WHERE products_id=$id");
                         }
                   }
                }
                
                if($HTTP_POST_VARS['product_new_status']){
				   foreach($HTTP_POST_VARS['product_new_status'] as $id => $new_status) {
						 if ($HTTP_POST_VARS['product_new_status'][$id] != $HTTP_POST_VARS['product_old_status'][$id]) {
						   $count_update++;
						   $item_updated[$id] = 'updated';
						   tep_set_product_status($id, $new_status);
						   mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_last_modified=now() WHERE products_id=$id");

						 }
				   }
                }
                
				if($HTTP_POST_VARS['product_new_tax']){
				   foreach($HTTP_POST_VARS['product_new_tax'] as $id => $new_tax_id) {
						 if ($HTTP_POST_VARS['product_new_tax'][$id] != $HTTP_POST_VARS['product_old_tax'][$id]) {
						   $count_update++;
						   $item_updated[$id] = 'updated';
						   mysql_query("UPDATE " . TABLE_PRODUCTS . " SET products_tax_class_id=$new_tax_id, products_last_modified=now() WHERE products_id=$id");
						 }
				   }
                }
     $count_item = array_count_values($item_updated);
     if ($count_item['updated'] > 0) $messageStack->add($count_item['updated'].' '.'produit actualis�!' . " $count_update " . 'Valeur(s) chang�e(s)', 'success');
     break;

     case 'calcul' :
      if ($HTTP_POST_VARS['spec_price']) $preview_global_price = 'true';
     break;
 }

//// explode string parameters from preview product
     if($info_back && $info_back!="-") {
       $infoback = explode('-',$info_back);
       $sort_by = $infoback[0];
       $page =  $infoback[1];
       $current_category_id = $infoback[2];
       $row_by_page = $infoback[3];
       $manufacturer = $infoback[4];
     }


// D�finition du menu d�roulant pour le nombre de resultat � afficher

   $row_bypage_array = array(array());
   for ($i = 50; $i <=300 ; $i=$i+25) {
      $row_bypage_array[] = array('id' => $i,
                                  'text' => $i);
   }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<script language="javascript">
<!--
var browser_family;
var up = 1;

if (document.all && !document.getElementById)
  browser_family = "dom2";
else if (document.layers)
  browser_family = "ns4";
else if (document.getElementById)
  browser_family = "dom2";
else
  browser_family = "other";
-->
</script>

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->

<td width="100%" valign="top">
<table border="0" width="100%" cellspacing="0" cellpadding="2">
	<tr>
		<td>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td class="pageHeading" colspan="3" valign="top">Mise � jour rapide des articles<?php //echo 'Quick Update Version : 2.5b full'; ?></td>
          <td class="pageHeading" align="right">&nbsp;</td>
        </tr>
      </table>
		</td>
</tr>
<tr>
	<td align="center">
		<table width="100%" cellspacing="0" cellpadding="0" border="1" bgcolor="#F3F9FB" bordercolor="#D1E7EF" height="100">
  <tr align="left">
    <td valign="middle">
    	<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td height="5" colspan="5"></td>
				</tr>
				<tr align="center">
					<td class="smalltext">
						<?php 
								echo tep_draw_form('row_by_page', FILENAME_QUICK_UPDATES, '', 'get'); 
								echo tep_draw_hidden_field( 'manufacturer', $manufacturer); 
								echo tep_draw_hidden_field( 'cPath', $current_category_id);
						?>
          </td>
          <td class="smallText">
						Nombre d'article par page&nbsp;&nbsp;
						<?php echo tep_draw_pull_down_menu('row_by_page', $row_bypage_array, $row_by_page, 'onChange="this.form.submit();"'); ?>
          </td></form>
          	<?php 
								echo tep_draw_form('familys', FILENAME_QUICK_UPDATES, '', 'get'); 
								echo tep_draw_hidden_field( 'row_by_page', $row_by_page); 
								echo tep_draw_hidden_field( 'cPath', $current_category_id);?>
          <td class="smallText" align="center" valign="top">
          	Afficher par Marque d'article&nbsp;&nbsp;<?php echo familys_list(); ?>
          </td></form>
						<?php 
            		// echo tep_draw_form('familys', FILENAME_QUICK_UPDATES, '', 'get'); 
								// echo tep_draw_hidden_field( 'row_by_page', $row_by_page); 
								// echo tep_draw_hidden_field( 'cPath', $current_category_id);?>
          <!--<td class="smallText" align="center" valign="top">
          	Afficher par MARQUE (A FAIRE) :&nbsp;&nbsp;<?php echo familys_list(); ?>
          </td></form> -->
				</tr>
			</table>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
        <form name="update" method="POST" action="<?php echo "$PHP_SELF?action=update&page=$page&sort_by=$sort_by&cPath=$current_category_id&row_by_page=$row_by_page&manufacturer=$manufacturer"; ?>">
        
        <tr>
        	<td height="50" align="center" style="color:#FF0000; font-weight:bold; font-size:20px">&nbsp;
						<?php 
						echo '<script language="javascript"><!--
            switch (browser_family)
            {
            case "dom2":
            case "ie4":
            document.write(\'<div id="descDiv">\');
            break;
            default:
            document.write(\'<ilayer id="descDiv"><layer id="descDiv_sub">\');
            break;
            }
            -->
            </script>' . "\n";
            ?>
        	</td>
        </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
					<td valign="top">
						<table border="0" width="100%" cellspacing="0" cellpadding="2">			
							<tr class="dataTableHeadingRow">


<!----------------------------- DEBUT REFERENCE ARTICLE --------------------------------------//-->
								<td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_MODEL == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"left\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_model ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Mod�le' . ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_model DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Mod�le' . ' ' . '- De Z � A')."</a>
                  <br />"  .'<br />Mod�le' . "
                  </td>
                  </tr>
                  </table>"; 
                  ?>
                </td>
<!----------------------------- FIN REFERENCE ARTICLE --------------------------------------//-->

<!----------------------------- DEBUT NOM ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"left\" valign=\"middle\">&nbsp;<a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=pd.products_name ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Nom' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=pd.products_name DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Nom' . ' ' . '- De Z � A')."</a>
                  <br />"  .'<br />Nom' . "
                  </td>
                  </tr>
                  </table>"; 
                  ?>
                </td>
<!----------------------------- FIN NOM ARTICLE --------------------------------------//-->


<!----------------------------- DEBUT CODE BARRE DE L'ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">			  
						<tr class="dataTableHeadingRow">
							<td class="dataTableHeadingContent" align="center" valign="middle">&nbsp;<br />Code Barre</td>
						</tr>
					</table>       
                </td>
<!----------------------------- FIN DEBUT CODE BARRE DE L'ARTICLE --------------------------------------//-->

<!----------------------------- DEBUT REFERENCE ORIGINE DE L'ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">			  
						<tr class="dataTableHeadingRow">
							<td class="dataTableHeadingContent" align="center" valign="middle">&nbsp;<br />R�f�rence Origine</td>
						</tr>
					</table>       
                </td>
<!----------------------------- FIN DEBUT REFERENCE ORIGINE DE L'ARTICLE --------------------------------------//-->

<!----------------------------- DEBUT GARANTIE DE L'ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">			  
						<tr class="dataTableHeadingRow">
							<td class="dataTableHeadingContent" align="center" valign="middle">&nbsp;<br />Garantie</td>
						</tr>
					</table>       
                </td>
<!----------------------------- FIN DEBUT GARANTIE DE L'ARTICLE --------------------------------------//-->















<!----------------------------- DEBUT ONLINE OU OFFLINE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_STATUT == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"center\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_status ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . 'OFF ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_status DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . 'ON ' . '- De A � Z')."</a>
                  <br />"  .'<br />Statut <br />(on/off)' . "
                  </td>
                  </tr>
                  </table>"; 
                  ?>
                </td>
<!----------------------------- FIN ONLINE OU OFFLINE --------------------------------------//-->

<!----------------------------- DEBUT POIDS DE L'ARTICLE --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_WEIGHT == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"center\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_weight ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Poids' . ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_weight DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Poids' . ' ' . '- De Z � A')."</a>
                  <br />" . '<br />Poids' . "
                  </td>
                  </tr>
                  </table>"; 
                  ?>            
                </td>
<!----------------------------- FIN DEBUT POIDS DE L'ARTICLE --------------------------------------//-->

<!----------------------------- DEBUT STOCK --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_QUANTITY == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"center\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_quantity_reel ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Qt�.' . ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_quantity_reel DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Qt�.' . ' ' . '- De Z � A')."</a>
                  <br />" . '<br />Qt�.' . "
                  </td>
                  </tr>
                  </table>"; 
                  ?>
                </td>
<!----------------------------- FIN STOCK --------------------------------------//-->
				
<!----------------------------- DEBUT STOCK MINIMUM --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_STOCK_MINI == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"center\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_quantity_min ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Qt�<br />mini'. ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_quantity_min DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Qt�<br />mini' . ' ' . '- De Z � A')."</a>
                  <br /> " . '<br />Qt�<br />mini' . "
                  </td>
                  </tr>
                  </table>";
                  ?>
                </td>
<!----------------------------- FIN STOCK MINIMUM --------------------------------------//-->

<!----------------------------- DEBUT STOCK IDEAL --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_STOCK_IDEAL == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"center\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_quantity_ideal ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Qt�<br />id�al'. ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_quantity_ideal DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Qt�<br />id�al' . ' ' . '- De Z � A')."</a>
                  <br /> " . '<br />Qt�<br />id�al' . "
                  </td>
                  </tr>
                  </table>";
                  ?>
                </td>
<!----------------------------- FIN STOCK IDEAL --------------------------------------//-->

<!----------------------------- DEBUT STOCK MAXIMUM --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_STOCK_MAXI == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"center\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_quantity_max ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Qt�<br />maxi'. ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_quantity_min DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Qt�<br />maxi' . ' ' . '- De Z � A')."</a>
                  <br /> " . '<br />Qt�<br />maxi' . "
                  </td>
                  </tr>
                  </table>";
                  ?>
                </td>
<!----------------------------- FIN STOCK MAXIMUM --------------------------------------//-->

<!----------------------------- DEBUT FAMILLE ARTICLE CLIENT --------------------------------------//-->	
								<td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_FAMILY_CLIENT == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"left\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=family_client_id ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . TABLE_HEADING_FAMILY_CLIENT . ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=family_client_id DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . TABLE_HEADING_FAMILY_CLIENT . ' ' . '- De Z � A')."</a>
                  <br /><br />
									Famille Article Client
                  </td>
                  </tr>
                  </table>"; 
									?>
                </td>
<!------------------------------ FIN FAMILLE ARTICLE CLIENT ---------------------------------------//-->

<!----------------------------- DEBUT FAMILLE ARTICLE  --------------------------------------//-->	
								<td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_FAMILY == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"left\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=family_id ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . TABLE_HEADING_FAMILY . ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=family_id DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . TABLE_HEADING_FAMILY . ' ' . '- De Z � A')."</a>
                  <br /><br />
									Famille Article
                  </td>
                  </tr>
                  </table>"; 
									?>
                </td>
<!------------------------------ FIN FAMILLE ARTICLE  ---------------------------------------//-->

<!----------------------------- DEBUT FABRICANT--------------------------------------//-->	
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_MANUFACTURER == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"left\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=manufacturers_id ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Type d\'article' . ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=manufacturers_id DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Type d\'article' . ' ' . '- De Z � A')."</a>
                  <br /><br />Les Marques
                  </td>
                  </tr>
                  </table>"; 
									?>
                </td>
<!----------------------------- FIN FABRICANT --------------------------------------//-->

<!----------------------------- DEBUT PRIX D'ACHAT --------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(ACTIVATE_COMMERCIAL_MARGIN == 'true')
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"center\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_cost ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Prix<br />d\'Achat'. ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_cost DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Prix<br />d\'Achat' . ' ' . '- De Z � A')."</a>
                  <br /><br /> Prix HT<br />d'achat
                  </td>
                  </tr>
                  </table>";
                  ?>
                </td>
<!----------------------------- FIN PRIX D'ACHAT --------------------------------------//-->

<!----------------------------- DEBUT PRIX DE VENTE--------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php 
                  echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">			  
                  <tr class=\"dataTableHeadingRow\">
                  <td class=\"dataTableHeadingContent\" align=\"center\" valign=\"middle\">&nbsp;
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_price ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_up.gif', '' . '<br />Prix<br />de vente' . ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_price DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer) ."\" >".tep_image(DIR_WS_IMAGES . 'icon_down.gif', '' . '<br />Prix<br />de vente' . ' ' . '- De Z � A')."</a>
                  <br /><br /> Prix de<br />vente HT
                  </td>
                  </tr>
                  </table>";
                  ?>
                </td>
<!----------------------------- FIN PRIX DE VENTE --------------------------------------//-->

<!----------------------------- DEBUT DE LA TAXE TVA--------------------------------------//-->
                <td class="dataTableHeadingContent" align="left" valign="top">
									<?php if(DISPLAY_TAX == 'true')
                  echo "
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_tax_class_id ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES  . 'icon_up.gif', '' . '<br />Taxe' . ' ' . '- De A � Z')."</a>
                  <a href=\"" . tep_href_link( FILENAME_QUICK_UPDATES, 'cPath='. $current_category_id .'&sort_by=p.products_tax_class_id DESC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer)."\" >".tep_image(DIR_WS_IMAGES  . 'icon_down.gif', '' . '<br />Taxe' . ' ' . '- De Z � A')."</a>
                  <br /><br />TVA </td> "; 
									?>
<!----------------------------- FIN DE LA TAXE TVA--------------------------------------//-->

<!----------------------------- DEBUT DE CELLULE VIDE --------------------------------------//-->
								<td class="dataTableHeadingContent" align="center" valign="middle"></td>
<!----------------------------- FIN DE CELLULE VIDE --------------------------------------//-->

<!----------------------------- DEBUT DE CELLULE VIDE --------------------------------------//-->
								<td class="dataTableHeadingContent" align="center" valign="middle"></td>
<!----------------------------- FIN DE CELLULE VIDE --------------------------------------//-->

              </tr>
              <tr class="datatableRow">
<?php
	if ($sort_by && !ereg('order by',$sort_by)) $sort_by = 'order by '.$sort_by ;
    $origin = FILENAME_QUICK_UPDATES."?info_back=$sort_by-$page-$current_category_id-$row_by_page-$manufacturer";
    $split_page = $page;
    if ($split_page > 1) $rows = $split_page * $row_by_page - $row_by_page;
	// THIERRY STOCK MINI IDEAL MAXI (ajout de : " p.products_quantity_min, p.products_quantity_ideal, p.products_quantity_max," )    
	$select="select * ";
	$from = "from  " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION .  " pd ";
	$where="where p.products_id = pd.products_id and pd.language_id = '$languages_id' ";
	if($manufacturer){
		$where.="and p.manufacturers_id = " . $manufacturer." ";
    }
  	if ($current_category_id != 0){
    	$from.=", " . TABLE_PRODUCTS_TO_CATEGORIES . " pc ";
		$where.="and p.products_id = pc.products_id and pc.categories_id = '" . $current_category_id . "' ";
  	}
	$products_query_raw=$select.$from.$where.$sort_by;
//// page splitter and display each products info
  $products_split = new splitPageResults($split_page, $row_by_page, $products_query_raw, $products_query_numrows);
  $products_query = tep_db_query($products_query_raw);
  while ($products = tep_db_fetch_array($products_query)) {
    $rows++;
    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
//// check for global add value or rates, calcul and round values rates
			if ($HTTP_POST_VARS['spec_price']){
      $flag_spec = 'true' ;
      if (substr($HTTP_POST_VARS['spec_price'],-1) == '%') {
                  if($HTTP_POST_VARS['marge'] && substr($HTTP_POST_VARS['spec_price'],0,1) != '-'){
                        $valeur = (1 - (ereg_replace("%", "", $HTTP_POST_VARS['spec_price']) / 100));
                        $price = sprintf("%01.2f", round($products['products_price'] / $valeur,2));
                }else{
                $price = sprintf("%01.2f", round($products['products_price'] + (($spec_price / 100) * $products['products_price']),2));
              }
          } else $price = sprintf("%01.2f", round($products['products_price'] + $spec_price,2));
			} else $price = $products['products_price'] ;

//// Check Tax_rate for displaying TTC
        $tax_query = tep_db_query("select r.tax_rate, c.tax_class_title from " . TABLE_TAX_RATES . " r, " . TABLE_TAX_CLASS . " c where r.tax_class_id=" . $products['products_tax_class_id'] . " and c.tax_class_id=" . $products['products_tax_class_id']);
        $tax_rate = tep_db_fetch_array($tax_query);
        if($tax_rate['tax_rate'] == '')$tax_rate['tax_rate'] = 0;

        if(MODIFY_MANUFACTURER == 'false'){
                $manufacturer_query = tep_db_query("select manufacturers_name from " . TABLE_MANUFACTURERS . " where manufacturers_id=" . $products['manufacturers_id']);
                $manufacturer = tep_db_fetch_array($manufacturer_query);
        }
//// AFFICHAGE DES INFOS DU PRIX TTC LORS DU SURVOLE
			
		echo '<tr class="dataTableRow">'; 
					


// DEBUT AFFICHAGE REFERENCE DE L'ARTICLE
			if(DISPLAY_MODEL == 'true') {
				
				if(MODIFY_MODEL == 'true') 
					echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"30\" name=\"product_new_model[".$products['products_id']."]\" value=\"".$products['products_model']."\"></td>\n";
				else 
					echo "<td class=\"smallText\" align=\"left\">" . $products['products_model'] . "</td>\n";
				}
				else
				{ 
					echo "<td class=\"smallText\" align=\"left\"></td>\n";
				}
// FIN AFFICHAGE REFERENCE DE L'ARTICLE

// DEBUT DE LA MODIFICATION DU NOM DE L'ARTICLE
			   	if(MODIFY_NAME == 'true')echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"60\" name=\"product_new_name[".$products['products_id']."]\" value=\"".str_replace("\"","&quot;",$products['products_name'])."\"></td>\n";
			   	else echo "<td class=\"smallText\" align=\"left\">".$products['products_name']."</td>\n";
// FIN DE LA MODIFICATION DU NOM DE L'ARTICLE
				
				echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"14\" maxlength=\"13\" name=\"product_new_code_barre[".$products['products_id']."]\" value=\"".$products['products_code_barre']."\"><input type=\"hidden\" size=\"14\" maxlength=\"13\" name=\"product_old_code_barre[".$products['products_id']."]\" value=\"".$products['products_code_barre']."\"></td>\n";
// FIN CODE BARRE DE L'ARTICLE

		// DEBUT REFERENCE ORIGINE DE L'ARTICLE
				echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"40\" name=\"product_new_ref_origine[".$products['products_id']."]\" value=\"".$products['products_ref_origine']."\"><input type=\"hidden\" size=\"40\" name=\"product_old_ref_origine[".$products['products_id']."]\" value=\"".$products['products_ref_origine']."\"></td>\n";
		// FIN REFERENCE ORIGINE DE L'ARTICLE
		
		// DEBUT GARANTIE DE L'ARTICLE
				echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"5\" name=\"product_new_garantie[".$products['products_id']."]\" value=\"".$products['products_garantie']."\"><input type=\"hidden\" size=\"5\" name=\"product_old_garantie[".$products['products_id']."]\" value=\"".$products['products_garantie']."\"></td>\n";

// DEBUT ONLINE OU OFFLINE
		if(DISPLAY_STATUT == 'true'){
         	if ($products['products_status'] == '1') {
            	echo "<td class=\"smallText\" align=\"left\"><input  type=\"radio\" name=\"product_new_status[".$products['products_id']."]\" value=\"1\" checked><input type=\"radio\" name=\"product_new_status[".$products['products_id']."]\" value=\"0\"  ></td>\n";
                } 
				else 
				{
                	echo "<td class=\"smallText\" align=\"left\"><input type=\"radio\" name=\"product_new_status[".$products['products_id']."]\" value=\"1\"  ><input type=\"radio\" name=\"product_new_status[".$products['products_id']."]\" value=\"0\" checked></td>\n";
                }
           	}
			else 
			echo "<td class=\"smallText\" align=\"left\"></td>";
// FIN ONLINE OU OFFLINE

// DEBUT POIDS DE L'ARTICLE
		if(DISPLAY_WEIGHT == 'true')echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"5\" name=\"product_new_weight[".$products['products_id']."]\" value=\"".$products['products_weight']."\"></td>\n";else echo "<td class=\"smallText\" align=\"left\"></td>";
// FIN POIDS DE L'ARTICLE

// DEBUT STOCK
		if(DISPLAY_QUANTITY == 'true')
			echo "<td class=\"smallText\" align=\"left\">
						<input type=\"text\" size=\"3\" name=\"product_new_quantity_reel[".$products['products_id']."]\" disabled=\"disabled\" value=\"".$products['products_quantity_reel']."\">
						<input type=\"hidden\" size=\"3\" name=\"product_old_quantity_reel[".$products['products_id']."]\" value=\"".$products['products_quantity_reel']."\">						
						</td>\n";
		else 
			echo "<td class=\"smallText\" align=\"left\"></td>";
// FIN STOCK

// DEBUT STOCK MINI
		if(DISPLAY_STOCK_MINI == 'true')echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"3\" name=\"product_new_quantity_min[".$products['products_id']."]\" value=\"".$products['products_quantity_min']."\"></td>\n";else echo "<td class=\"smallText\" align=\"left\"></td>"; 
// FIN STOCK MINI

// DEBUT  STOCK IDEAL
		if(DISPLAY_STOCK_IDEAL == 'true')echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"3\" name=\"product_new_quantity_ideal[".$products['products_id']."]\" value=\"".$products['products_quantity_ideal']."\"></td>\n";else echo "<td class=\"smallText\" align=\"left\"></td>"; 
// FIN STOCK IDEAL

// DEBUT  STOCK MAXI
		if(DISPLAY_STOCK_MAXI == 'true')echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"3\" name=\"product_new_quantity_max[".$products['products_id']."]\" value=\"".$products['products_quantity_max']."\"></td>\n";else echo "<td class=\"smallText\" align=\"left\"></td>"; 
// FIN STOCK MAXI
    
// DEBUT FAMILE ARTICLE CLIENT           
		if(DISPLAY_FAMILY_CLIENT == 'true'){
			//if(MODIFY_FAMILY_CLIENT == 'true')
				echo "<td class=\"smallText\" align=\"left\">".tep_draw_pull_down_menu("product_new_family_client[".$products['products_id']."]\"", $family_client_array, $products['family_client_id'])."</td>\n";
			//else 
			//	echo "<td class=\"smallText\" align=\"left\">" . $family_client['family_client_name'] . "</td>";
			}
		else{ echo "<td></td>";}
// FIN FAMILE ARTICLE CLIENT

// DEBUT FAMILE ARTICLE          
		if(DISPLAY_FAMILY == 'true'){
			//if(MODIFY_FAMILY == 'true')
				echo "<td class=\"smallText\" align=\"left\">".tep_draw_pull_down_menu("product_new_family[".$products['products_id']."]\"", $family_array, $products['family_id'])."</td>\n";
			//else 
			//	echo "<td class=\"smallText\" align=\"left\">" . $family['family_name'] . "</td>";
			}
		else{ echo "<td></td>";}
// FIN FAMILE ARTICLE

// DEBUT FABRICANT           
		if(DISPLAY_MANUFACTURER == 'true'){
			if(MODIFY_MANUFACTURER == 'true')
				echo "<td class=\"smallText\" align=\"left\">".tep_draw_pull_down_menu("product_new_manufacturer[".$products['products_id']."]\"", $manufacturers_array, $products['manufacturers_id'])."</td>\n";
			else 
				echo "<td class=\"smallText\" align=\"left\">" . $manufacturer['manufacturers_name'] . "</td>";}
		else{ echo "<td></td>";}
// FIN FABRICANT
				
// DEBUT PRIX ACHAT
		if(ACTIVATE_COMMERCIAL_MARGIN == 'true')echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"7\" name=\"product_new_cost[".$products['products_id']."]\" value=\"".$products['products_cost']."\"></td>\n";else echo "<td class=\"smallText\" align=\"left\"></td>";
// FIN PRIX D'ACHAT 
			               
// DEBUT PRIX DE VENTE ET PROMOTION
     $specials_array = array();
     $specials_query = tep_db_query("select p.products_id, s.products_id, s.specials_id from " . TABLE_PRODUCTS . " p, " . TABLE_SPECIALS . " s where s.products_id = p.products_id");
     while ($specials = tep_db_fetch_array($specials_query)) {
       $specials_array[] = $specials['products_id'];
     }
// VERIFICATION DES ARTICLES EN PROMOTION
       
		echo "<td class=\"smallText\" align=\"left\"><input type=\"text\" size=\"8\" disabled=\"disabled\" name=\"product_new_price[".$products['products_id']."]\" "; if(DISPLAY_TVA_UP == 'true'){ echo "onKeyUp=\"display_ttc('keyup', this.value" . ", " . $tax_rate['tax_rate'] . ", 1);\"";} echo " value=\"".$price ."\">".tep_draw_hidden_field('update_price['.$products['products_id'].']','yes'). "</td>\n";
        
// FIN PRIX DE VENTE ET PROMOTION

// DEBUT DE LA TVA
        if(DISPLAY_TAX == 'true'){if(MODIFY_TAX == 'true')echo "<td class=\"smallText\" align=\"left\">".tep_draw_pull_down_menu("product_new_tax[".$products['products_id']."]\"", $tax_class_array, $products['products_tax_class_id'])."</td>\n";else echo "<td class=\"smallText\" align=\"left\">" . $tax_rate['tax_class_title'] . "</td>";}else{ echo "<td class=\"smallText\" align=\"center\"></td>";}
// FIN DE LA TVA

// DEBUT DE LA PREVISUALISATION 
        if(DISPLAY_PREVIEW == 'true')echo "<td class=\"smallText\" align=\"left\"><a href=\"".tep_href_link (FILENAME_ARTICLE_EDIT, 'pID='.$products['products_id'].'&action=new_product_preview&read=only&sort_by='.$sort_by.'&page='.$split_page.'&origin='.$origin)."\">". tep_image(DIR_WS_IMAGES . 'icon_info.gif', 'produit pr�c�dent') ."</a></td>\n";
                if(DISPLAY_EDIT == 'true')echo "<td class=\"smallText\" align=\"left\"><a href=\"".tep_href_link (FILENAME_ARTICLE_EDIT, 'pID='.$products['products_id'].'&cPath='.$categories_products[0].'&action=new_product')."\" target=\"_blank\">". tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', 'Passez � l\'�dition compl�te') ."</a></td>\n";
// FIN DE LA PREVISUALISATION

//// Hidden parameters for cache old values
        if(MODIFY_NAME == 'true') echo tep_draw_hidden_field('product_old_name['.$products['products_id'].'] ',$products['products_name']);
        
		if(MODIFY_MODEL == 'true') echo tep_draw_hidden_field('product_old_model['.$products['products_id'].'] ',$products['products_model']);
                echo tep_draw_hidden_field('product_old_status['.$products['products_id'].']',$products['products_status']);
        		echo tep_draw_hidden_field('product_old_quantity['.$products['products_id'].']',$products['products_quantity']);
				// THIERRY STOCK MINI
				echo tep_draw_hidden_field('product_old_quantity_min['.$products['products_id'].']',$products['products_quantity_min']);
				// THIERRY STOCK MINI END
				// THIERRY STOCK IDEAL
				echo tep_draw_hidden_field('product_old_quantity_ideal['.$products['products_id'].']',$products['products_quantity_ideal']);
				// THIERRY STOCK IDEAL END
				// THIERRY STOCK MAXI
				echo tep_draw_hidden_field('product_old_quantity_max['.$products['products_id'].']',$products['products_quantity_max']);
				// THIERRY STOCK MAXI END
                
		// if(MODIFY_FAMILY_CLIENT == 'true')echo tep_draw_hidden_field('product_old_family_client['.$products['products_id'].']',$products['family_client_id']);
		
		// if(MODIFY_FAMILY == 'true')echo tep_draw_hidden_field('product_old_family['.$products['products_id'].']',$products['family_id']);
		
		if(MODIFY_MANUFACTURER == 'true')echo tep_draw_hidden_field('product_old_manufacturer['.$products['products_id'].']',$products['manufacturers_id']);
        
		echo tep_draw_hidden_field('product_old_weight['.$products['products_id'].']',$products['products_weight']);

		echo tep_draw_hidden_field('product_old_price['.$products['products_id'].']',$products['products_price']);
		//matt added cost
// COUCOU	
		echo tep_draw_hidden_field('product_old_cost['.$products['products_id'].']',$products['products_cost']);
		//end
		
        if(MODIFY_TAX == 'true')echo tep_draw_hidden_field('product_old_tax['.$products['products_id'].']',$products['products_tax_class_id']);
//// hidden display parameters
        echo tep_draw_hidden_field( 'row_by_page', $row_by_page);
        echo tep_draw_hidden_field( 'sort_by', $sort_by);
        echo tep_draw_hidden_field( 'page', $split_page);
     }
    echo "</table>\n";

?>
          </td>
        </tr>
       </table></td>
      </tr>
<tr>
<td align="right" height="40">
<?php
                 //// Affichage des boutons
                echo '<a href="javascript:window.print()">' . tep_image_button('button_print.gif', 'Imprimer cette page') . '</a>&nbsp;&nbsp;';
              	echo tep_image_submit('button_update.gif', IMAGE_UPDATE);
              	echo '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_QUICK_UPDATES,"row_by_page=$row_by_page") . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>';
?></td>
</tr>
</form>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                <td class="smallText"><?php echo $products_split->display_count($products_query_numrows, $row_by_page, $split_page, TEXT_DISPLAY_NUMBER_OF_PRODUCTS);  ?></td>
                <td class="smallText" align="right"><?php echo $products_split->display_links($products_query_numrows, $row_by_page, MAX_DISPLAY_PAGE_LINKS, $split_page, '&cPath='. $current_category_id .'&sort_by='.$sort_by . '&manufacturer='.$_GET['manufacturer'] .'&row_by_page=' . $row_by_page); ?></td>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->
  </tr>
</table>

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>