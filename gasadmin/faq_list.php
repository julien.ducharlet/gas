<?php
/*
  FAQ system for OSC 2.2 MS2 v2.1i  23.08.2007
  Originally Created by: https://adgrafics.com admin@adgrafics.net
  Updated by: https://www.webandpepper.ch osc@webandpepper.ch v2.0 (03.03.2004)
  Last Modified by Cydonian (cydonia@flashmail.com) (23.08.2007)
  Released under the GNU General Public License
  osCommerce, Open Source E-Commerce Solutions
  Copyright (c) 2004 osCommerce
*/
?>
<tr class="pageHeading"><td><?php echo $title ?></td></tr>
<tr><td>Famille de la question
    	<form action="<?php $_SERVER['PHP_SELF'];?>">
        <select name="family" onchange="this.form.submit();">
       	<option value="">Choisir une famille</option>
		<?php

		//requete normale pour effectuer la liste deroulante
		$requete_family_faq="SELECT faq_family_id, faq_name_family FROM faq_family ORDER BY v_order";
		$resultat_requete=tep_db_query($requete_family_faq);
		
		while($FAQ=tep_db_fetch_array($resultat_requete)){
			if($FAQ['faq_family_id'] == $_REQUEST['family']){
            	echo'<option value="'.$FAQ['faq_family_id'].'" selected="selected">'.$FAQ['faq_name_family'].'</option>';
            }
          	else{ 
				echo'<option value="'.$FAQ['faq_family_id'].'">'.$FAQ['faq_name_family'].'</option>';}
			}?>
       </select>
       </form>
	</td>
</tr>
<tr><td><table border="0" width="100%" cellpadding="2" cellspacing="1" bgcolor="#ffffff">
  <tr class="dataTableHeadingRow">
	<td align="center" class="dataTableHeadingContent">ID</td>
	<td align="center" class="dataTableHeadingContent">Famille</td>
	<td align="center" class="dataTableHeadingContent">Ordre de tri</td>
	<td align="center" class="dataTableHeadingContent">Question/R�ponse</td>
	<td align="center" class="dataTableHeadingContent">Statut</td>
	<td align="center" class="dataTableHeadingContent">Actions</td>
  </tr>
<?php
  $no = 1;
  if (sizeof($data) > 0) {
    while (list($key, $val) = each($data)) {
      $no % 2 ? $bgcolor="#DEE4E8" : $bgcolor="#F0F1F1";
?>
  <tr bgcolor="<?php echo $bgcolor?>">
    <td align="center" class="dataTableContent"><?php echo $val['faq_id'];?></td>
    <td align="center" class="dataTableContent"nowrap>
						<?php 
									$requete_family="SELECT faq_name_family FROM faq_family WHERE faq_family_id=".$val['faq_family_id'];
									$resultat_family=mysql_query($requete_family);
									
									$FAQ_FAM=mysql_fetch_array($resultat_family);
									echo $FAQ_FAM['faq_name_family'];?></td>
                  
    <td align="center" class="dataTableContent"><?php echo $val['v_order'];?></td>
    <td align="left" class="dataTableContent"><?php echo $val['question'] ;?></td> <? // ' (' . $val['language'] . ') ?>
    <td align="center" class="dataTableContent" nowrap>
<?php 
  	  if ($val['visible'] == 1) {
		echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;
		<a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Visible&faq_id=$val[faq_id]&visible=$val[visible]") . '">
		' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif'," D�sactiver? FAQ ID=$val[faq_id]", 10, 10) . '</a>';
  	  }else {
		echo 
		'<a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Visible&faq_id=$val[faq_id]&visible=$val[visible]") . '">
		' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif',"Activer? FAQ ID= $val[faq_id]", 10, 10) . '</a>&nbsp;&nbsp;&nbsp;'.tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10) ;
  	  };
?>
</td>
<td align=center class="dataTableContent">
<?php echo '<a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Edit&faq_id=$val[faq_id]&faq_lang=$val[language]", 'NONSSL') . '">' . tep_image(DIR_WS_ICONS . 'editer.png', 'Modification de la question/r�ponse suivante' . " $val[faq_id]") . '</a>'; ?>

<?php echo '<a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Delete&faq_id=$val[faq_id]", 'NONSSL') . '">' . tep_image(DIR_WS_ICONS . 'supprimer.png', 'supprimer FAQ ID=' . " $val[faq_id]") . '</a>'; ?>
</td>
</tr>
<?php
  	  $no++;
  	}
  } else {
?>
   <tr bgcolor="#DEE4E8">
    <td colspan="7">Syst�me de FAQ vide</td>
   </tr>
<?php
  }
?>
</table>
</td></tr>
<tr><td align="right">
<?php echo '<a href="' . tep_href_link(FILENAME_FAQ_MANAGER, 'faq_action=Added', 'NONSSL') . '">' . tep_image_button('button_insert.gif', 'Nouvelle FAQ') . '</a>'; ?>
<?php // echo '<a href="' . tep_href_link(FILENAME_FAQ_MANAGER, '', 'NONSSL') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>
</td></tr>