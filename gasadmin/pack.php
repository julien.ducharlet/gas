<?php
require('includes/application_top.php');

	if(isset($_GET['setFlag']) && $_GET['setFlag']>0 && isset($_GET['flag']) && ($_GET['flag']==0 || $_GET['flag']==1)) {
		if(tep_db_query("update products_packs set pack_status='". $_GET['flag'] ."' where pack_id=". $_GET['setFlag'])) {
			$messageStack->add('Statut bien prise en compte ', 'success');
		}
		else $messageStack->add('Erreur lors de la Modification du Statut', 'error');
	}
	
	if(isset($_POST) && !empty($_POST)) {
		if(isset($_POST['pack'])) {
			if(tep_db_query("update products_packs set products_id_1='". $_POST['article_1'] ."', products_id_2='". $_POST['article_2'] ."', pack_status='". $_POST['status'] ."', pack_remise='". $_POST['remiseHt'] ."' where pack_id=". $_POST['pack'])) {
				$messageStack->add('Modification bien prise en compte ', 'success');
			}
			else $messageStack->add('Erreur lors de la Modification', 'error');
		}
		elseif(isset($_POST['new_pack'])) {
			$product0=$_POST['article_0'];
			$product1=$_POST['article_1'];
			if($_POST['article_2']!=0){
				$product2=$_POST['article_2'];
				$nb_prod=3;
			}
			else{
				$product2='';
				$nb_prod=2;
			}
			$satus=$_POST['status'];
			$remise=$_POST['remiseHt'];
			if(tep_db_query("insert into products_packs(products_id, products_id_1, products_id_2, pack_status, pack_remise, pack_products_quantity  ) 
							 values('". $product0 ."', '". $product1 ."', '". $product2 ."', '". $status ."', '". $remise ."', '". $nb_prod ."')")) {
				$messageStack->add('Cr&eacute;ation du Pack bien prise en compte ', 'success');
			}
			else $messageStack->add('Erreur lors de la Cr&eacute;aion', 'error');
		}
	}
	elseif(isset($_GET['del_pack']) && $_GET['del_pack']>0) {
		if(tep_db_query("delete from products_packs where pack_id=". $_GET['del_pack'])) {
			$messageStack->add('Cr&eacute;ation du Pack bien prise en compte ', 'success');
		}
		else $messageStack->add('Erreur lors de la Supression', 'error');
	}
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/metadata.js"></script>
<script src="includes/lib/jquery.dimensions.js" type="text/javascript"></script>
<script src="includes/lib/jquery.tooltip.js" type="text/javascript"></script>
<script src="includes/lib/jquery.bgiframe.js" type="text/javascript"></script>
<script type="text/javascript">
tab=new Array();

function duplicate(num_id) {
	temp=$('#article_0').val();
	if($("#marque_0").val()!=$("#marque_"+ num_id).val() || $("#family_0").val()!=$("#family_"+ num_id).val()) {
		$("#marque_"+ num_id).val($("#marque_0").val());
		$("#family_"+ num_id).val($("#family_0").val());
		$.get("update_article.php",	{ manufacturers_id: $("#marque_"+ num_id).val(), family_id: $("#family_"+ num_id).val() },
									function(data){
										$("#article_"+ num_id).html(data);
										$('#article_'+ num_id).val(temp);
										if($('#article_'+ num_id).val()==temp) findPrice(tab[temp], "price"+ num_id);
									});
	}
	else {
		$('#article_'+ num_id).val(temp);
		if($('#article_'+ num_id).val()==temp) findPrice(tab[temp], "price"+ num_id);
	}
}

function findPrice(price, id) {
	$("#"+ id).val(price);
	calcTotalPriceHt()
}

function calcTotalPriceHt() {
	price=parseFloat($("#price0").val());
	price+=parseFloat($("#price1").val());
	if($("#price2").val()!='') price+=parseFloat($("#price2").val());
	priceTTC=Math.round(price*120)/100
	$("#priceNormalHt").html(Math.round(price*100)/100);
	$("#priceNormalTtc").html(priceTTC);
	calcPackPrice();
}

function calcPackPrice() {
	pricePackHT=parseFloat($("#priceNormalHt").html()) - parseFloat($("#remiseHt").val());
	pricePackHT=Math.round(pricePackHT*100)/100;

	pricePackTTC=parseFloat($("#priceNormalTtc").html()) - parseFloat($("#remiseTtc").val());
	pricePackTTC=Math.round(pricePackTTC*100)/100;
	
	$("#pricePackHt").html(pricePackHT);
	$("#pricePackTtc").html(pricePackTTC);
	temp=100-parseInt($("#pricePackTtc").html()*100) / parseFloat($("#priceNormalTtc").html());
	temp=Math.round(temp*100)/100;
	$("#remisePackPt").html(temp);
}

function delProduct(id_pack) {
	if(confirm("�tes-vous s�r de vouloir supprimer ce pack ?")) document.location.href='?del_pack='+ id_pack;
}

function verifArticle(formulaire, type) {
	if(type==1) formulaire.action="pack.php";
	else if(type==2) formulaire.action="pack.php?new_pack=1&manufacturers_id="+ $("#marque_0").val() + "&family_id="+ $("#family_0").val() + "&products_id_0="+ $("#article_0").val();
	else if(type==3) formulaire.action="pack.php?new_pack=1&manufacturers_id="+ $("#marque_0").val() + "&family_id="+ $("#family_0").val() + "&products_id_0="+ $("#article_0").val() + 
	"&manufacturers_id_1="+ $("#marque_1").val() + "&family_id_1="+ $("#family_0").val() + "&products_id_1="+ $("#article_1").val() ;
	
	if($("#article_0").val()==0 || $("#article_1").val()==0) {
		alert("les 2 produits principaux pour former le pack ne sont pas s�lectionn�s");
	}
	else formulaire.submit();
}

function setFlag(pack_id, flag) {

	URL='';
	if($("#marqueId").val()!='0') {
		URL+='&manufacturers_id='+ $("#marqueId").val();		
	}
	
	if($("#familyId").val()!='0') {
		URL+='&family_id='+ $("#familyId").val();		
	}
	
	document.location.href='?setFlag='+ pack_id +'&flag='+ flag +URL;
}

$(document).ready(function() {
	
	// call the tablesorter plugin
    $(".tablesorter").tablesorter();
	
	$("#remiseTtc").keyup(function() {
								   
		temp=parseFloat($("#remiseTtc").val())/1.20*10000;
		$("#remiseHt").val(Math.round(temp)/10000);
		calcPackPrice();
	});
	
	$("fieldset select").change(function() {
										 
		id=this.id;
		tp=id.split('_');
		num=tp[1];
		marque="#marque_"+ num;
		family="#family_"+ num;
		article="#article_"+ num;
		if(tp[0]!='article') {
			$.get("update_article.php", { manufacturers_id: $(marque).val(), family_id: $(family).val() },
										function(data){
											$(article).html(data);
										});
		}
	});
	
	$("#searchId").keyup(function() {
								  
		header="#tableHeader";
		$.get("list_packs.php", { article_id: $("#searchId").val() }, 
								function(data) {
									$(".main").remove();
									$(header).after(data);
								});
	});
	
	$("#marqueId").change(function() {
								   
		header="#tableHeader";
		$.get("list_packs.php", { manufacturers_id: $("#marqueId").val(), ajax: 1 }, 
								function(data) {
									$(".main").remove();
									$(header).after(data);
								});
	});
	
	$("#familyId").change(function() {
								   
		header="#tableHeader";
		$.get("list_packs.php", { family_id: $("#familyId").val(), ajax: 1 }, 
									function(data) {
										$(".main").remove();
										$(header).after(data);
									});
	});
});
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">

<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<form action="pack.php" method="post" onSubmit="return verifArticle(this.form)">
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top"> 
      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"> <?php 
					if (isset($_GET['pack']) && $_GET['pack']>0) 'Modification d\'un Pack';
					elseif(isset($_GET['new_pack'])) echo 'Cr&eacute;ation d\'un Pack';
					else echo 'Les Packs <input type="button" value="Nouveau Pack" onclick="document.location.href=\'?new_pack=1\'">' ?>
                </td>
                <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
              </tr>
            </table>
          </td>
        </tr>
	   <tr>
          <td align="left" class="mainBar">	
          	<?php
				$i=0;
				$query=tep_db_query("select m.manufacturers_id, manufacturers_name from ". TABLE_MANUFACTURERS ." m, ". TABLE_MANUFACTURERS_INFO ." mi where m.manufacturers_id=mi.manufacturers_id order by manufacturers_name");
				while($data=tep_db_fetch_array($query)) {
					$marque[$i]['index']=$data['manufacturers_id'];
					$marque[$i++]['name']=$data['manufacturers_name'];
				}
				$i=0;
				$query=tep_db_query("select family_id, family_name from ". TABLE_FAMILY ." order by family_name");
				while($data=tep_db_fetch_array($query)) {
					$family[$i]['index']=$data['family_id'];
					$family[$i++]['name']=$data['family_name'];
				}
				if(isset($_GET['products_id_0'])) {
					$i=0;
					$query=tep_db_query("select p.products_id, products_name, products_model, products_price from " . TABLE_PRODUCTS . " p, ". TABLE_PRODUCTS_DESCRIPTION ." pd where p.products_id=pd.products_id and manufacturers_id=". $_GET['manufacturers_id'] ." order by products_name");
					while($data=tep_db_fetch_array($query)) {
						$products_0[$i]['index']=$data['products_id'];
						$products_0[$i]['name']=$data['products_name'];
						$products_0[$i++]['model']=$data['products_model'];
					}
				}
				if(isset($_GET['products_id_1'])) {
					$i=0;
					$query=tep_db_query("select p.products_id, products_name, products_model, products_price from " . TABLE_PRODUCTS . " p, ". TABLE_PRODUCTS_DESCRIPTION ." pd where p.products_id=pd.products_id and manufacturers_id=". $_GET['manufacturers_id_1'] ." order by products_name");
					while($data=tep_db_fetch_array($query)) {
						$products_1[$i]['index']=$data['products_id'];
						$products_1[$i]['name']=$data['products_name'];
						$products_1[$i++]['model']=$data['products_model'];
					}
				}
				
				$i=0;
				$query=tep_db_query("select p.products_id, products_name, products_model, products_price from " . TABLE_PRODUCTS . " p, ". TABLE_PRODUCTS_DESCRIPTION ." pd where p.products_id=pd.products_id order by products_name");
				while($data=tep_db_fetch_array($query)) {
					$product[$i]['index']=$data['products_id'];
					$product[$i]['model']=$data['products_model'];
					$product[$i]['name']=$data['products_name'];
					$product[$i++]['price']=$data['products_price'];
				}
						
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ AJOUT D'UN PACK _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_--_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ AJOUT D'UN PACK _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
						
				if(isset($_GET['new_pack']) && $_GET['new_pack']==1) { //
					echo '&nbsp;</td>';
					echo '</tr>';
					
					echo '<tr>';
					echo '<td class="main">';	
					echo 'Actif / Inactif : ';
					echo '<input name="status" type="radio" checked value="1">&nbsp;&nbsp;';
					echo '<input name="status" type="radio" value="0">';
					echo '</td>';
					echo '</tr>';
						
					echo '<tr>';
					echo '<td>';
					
					$option_manufacturers_0=$option_manufacturers_1=$option_manufacturers_2='<option selected value="0">Choisir une Marque</option>';
					$option_family_0=$option_family_1=$option_family_2='<option value="0">Choisir une Famille</option>';
					$option_products_0=$option_products_1=$option_products_2='<option value="0">Choisir un Produit</option>';
								
					for($i=0; $i<count($marque); $i++) {
						if($_GET['manufacturers_id']==$marque[$i]['index']) $option_manufacturers_0.= '<option selected value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
						else $option_manufacturers_0.= '<option value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
									
						if($_GET['manufacturers_id_1']==$marque[$i]['index']) $option_manufacturers_1.= '<option selected value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
						else $option_manufacturers_1.= '<option value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
									
						$option_manufacturers_2.= '<option value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
					}
								
					for($i=0; $i<count($family); $i++) {
						if($_GET['family_id']==$family[$i]['index']) $option_family_0.= '<option value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>';
						else $option_family_0.= '<option value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>';
									
						if($_GET['family_id_1']==$family[$i]['index']) $option_family_1.= '<option value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>';
						else $option_family_1.= '<option value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>';
									
						$option_family_2.= '<option value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>';
					}
								
					for($i=0; $i<count($products_0); $i++) {
						if($_GET['products_id_0']==$products_0[$i]['index']) $option_products_0.= '<option selected value="'. $products_0[$i]['index'] .'">'. $products_0[$i]['name'] .' ('. $products_0[$i]['model'] .')</option>';
						else $option_products_0.= '<option value="'. $products_0[$i]['index'] .'"">'. $products_0[$i]['name'] .' ('. $products_0[$i]['model'] .')</option>';
					}
								
					for($i=0; $i<count($products_1); $i++) {
						if($_GET['products_id_1']==$products_1[$i]['index']) $option_products_1.= '<option selected value="'. $products_1[$i]['index'] .' ('. $products_1[$i]['model'] .')">'. $products_1[$i]['name'] .'</option>';
						else $option_products_1.= '<option value="'. $products_1[$i]['index'] .'">'. $products_1[$i]['name'] .' ('. $products_1[$i]['model'] .')</option>';
					}
								
					/* ON sauve les prix de produits dans un tableau javaScript */
					for($i=0; $i<count($product); $i++) {
						?><script type="text/javascript" language="javascript">
                  	      	tab[<?php echo $product[$i]['index']; ?>]= <?php echo $product[$i]['price']; ?>
                  	      </script><?php
					}
							
					echo '<fieldset>';	
					echo '	<legend>Article Principal</legend>';
					echo '	<p>';
					echo '	Marque : ';  
					echo '	<select id="marque_0" name=marque_0">';  
						echo $option_manufacturers_0;	
					echo '	</select>';
					echo '	Famille : ';	
					echo '	<select id="family_0" name=family_0">';	
						echo $option_family_0;	
					echo '	</select>';
					echo '	</p>';
					echo '	<p>';
					echo '		Article : ';	echo '<select onClick="findPrice(tab[this.value], \'price0\')" id="article_0" name="article_0">';	echo $option_products_0;	echo '</select>';
					echo '	</p>';
					echo '	<input type="hidden" id="price0" value="0">';
					echo '</fieldset>';
						
					echo '</td>';
					echo '</tr>';
						
					//_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_-_ Artice 1 du Pack _-_-_-_-_-_-_-_-_-_-_-_--_-_-_-_-_-_-_-_-_-_-_-_-//
					echo '<tr>';
						echo '<td>';
							echo '<fieldset>';
								echo '<legend>Article 1 du Pack</legend>';
								echo '<p>';
									echo 'Marque : ';  echo '<select id="marque_1" name=marque_1">';  echo $option_manufacturers_1;	echo '</select>';
									echo ' Famille : ';	echo '<select id="family_1" name=family_1">';	echo $option_family_1;	echo '</select>';
								echo '</p>';
								echo '<p>';
									echo '<img src="images/icons/copier.png" alt="Ajouter un article identique au premier" onclick="duplicate(\'1\')" style="cursor:pointer; margin-right:10px"/>';
									echo 'Article : ';	echo '<select onClick="findPrice(tab[this.value], \'price1\')" id="article_1" name="article_1">';	echo $option_products_1;	echo '</select>';
								echo '</p>';
								echo '<input type="hidden" id="price1" value="0">';
							echo '</fieldset>';
						echo '</td>';
					echo '</tr>';
						
					//_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_-_ Artice 2 du Pack (optionnel) _-_-_-_-_-_-_-_-_-_-_-_--_-_-_-_-_-_-_-_-_-_-_-_-//
					echo '<tr>';
						echo '<td>';
							echo '<fieldset>';
								echo '<legend>Article 2 du Pack (optionnel)</legend>';
								echo '<p>';
									echo 'Marque : ';  echo '<select id="marque_2" name=marque_2">';  echo $option_manufacturers_2;	echo '</select>';
									echo ' Famille : ';	echo '<select id="family_2" name=family_2">';	echo $option_family_2;	echo '</select>';
								echo '</p>';
								echo '<p>';
									echo '<img src="images/icons/copier.png" alt="Ajouter un article identique au premier" onclick="duplicate(\'2\')" style="cursor:pointer; margin-right:10px"/>';
									echo 'Article : ';	echo '<select onClick="findPrice(tab[this.value], \'price2\')" id="article_2" name="article_2">';	echo $option_products_2;	echo '</select>';
								echo '</p>';
								echo '<input type="hidden" id="price2" value="0">';
							echo '</fieldset>';
						echo '</td>';
					echo '</tr>';
						
					//_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_-_ R�capitulatif _-_-_-_-_-_-_-_-_-_-_-_--_-_-_-_-_-_-_-_-_-_-_-_-//
					$priceHT=0;
					$priceTTC=0;
					$remise=0;
					$remiseTTC=0;
					
					echo '<tr>';
						echo '<td>';
							echo '<fieldset>';
								echo '<legend>Informations sur le Pack</legend>';
								echo '<p>';
									echo 'Prix Normal : ';  
									echo '<span id="priceNormalHt">'. $priceHT .' HT</span>'; 
									echo '<span id="priceNormalTtc">'. $priceTTC .'</span> TTC';
								echo '</p>';
								echo '<p>';
									echo 'Montant de la Remise : ';	echo '<input class="remiseHt" type="text" id="remiseHt" name="remiseHt" size="5" value="'. $remise .'"> &euro; HT';	
									echo '<input class="remiseTtc" type="text" id="remiseTtc" name="remiseTtc" size="3" value="'. $remiseTTC .'"> &euro; TTC';
								echo '</p>';
					
								$pricePackHT=0;//convert_price(round(($priceHT-$remise)*100)/100);
								$pricePackTTC=0;//convert_price(round(($priceTTC-$remiseTTC)*100)/100);
								$pourcent=0;//convert_price(round((100-$pricePackTTC*100/$priceTTC)*100)/100);
								
								echo '<p>';
									echo 'Prix du Pack : ';	 echo '<span id="pricePackHt">'. $pricePackHT  .'</span> &euro; HT'; echo '<span id="pricePackTtc">'. $pricePackTTC .'</span> &euro; TTC';
								echo '</p>';
								echo '<p>';
									echo '% de Remise du Pack : ';	 echo '<span id="remisePackPt">'. $pourcent .'</span> %';
								echo '</p>';
							echo '</fieldset>';
						echo '</td>';
					echo '</tr>';
					echo '</table>';
						
					if (isset($_GET['pack']) && $_GET['pack']>0) {
						echo '<input type="hidden" name="pack" id="pack" value="'. $_GET['pack'] .'">';
						echo '<input type="submit" value="OK"/>';
					}
					elseif (isset($_GET['new_pack']) && $_GET['new_pack']==1) {
						echo '<input type="hidden" name="new_pack" id="new_pack" value="'. $_GET['new_pack'] .'" />';
						echo '<input type="button" value="Ajouter" onclick="verifArticle(this.form, \'1\')" />';
						echo '<input type="button" value="Ajouter + Nouveau Pack" onclick="verifArticle(this.form, \'2\')" />';
						echo '<input type="button" value="Ajouter + Pack identique" onclick="verifArticle(this.form, \'3\')" />';
					}
				}

/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-MODIFICATION D'UN PACK _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-MODIFICATION D'UN PACK _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
						
				elseif (isset($_GET['pack']) && $_GET['pack']>0) { // modification d'un pack
					echo '&nbsp;</td>';
					echo '</tr>';
					$pack_query = tep_db_query("select * from " . TABLE_PACK . " where pack_id =  '" . $_GET['pack'] . "'");
					$data = tep_db_fetch_array($pack_query);
					$arrayProducts[0]=$data['products_id'];
					$arrayProducts[1]=$data['products_id_1'];
					$arrayProducts[2]=$data['products_id_2'];
					$status=$data['pack_status'];
					$pack_id=$data['pack_id'];
					$remise=$data['pack_remise'];
					echo '<tr>';
						echo '<td class="main">';	
							echo 'Actif / Inactif : ';
							echo '<input name="status" type="radio" '; if($status) echo 'checked '; echo 'value="1">&nbsp;&nbsp;';
							echo '<input name="status" type="radio" '; if(!$status) echo 'checked '; echo 'value="0">';
						echo '</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<td>';
							$query=tep_db_query("select * from " . TABLE_PRODUCTS . " where products_id =  '" . $arrayProducts[0] . "' order by products_model");
							$data = tep_db_fetch_array($query);
							$query1=tep_db_query("select * from " . TABLE_PRODUCTS . " where products_id =  '" . $arrayProducts[1] . "' order by products_model");
							$data1 = tep_db_fetch_array($query1);
							$query2=tep_db_query("select * from " . TABLE_PRODUCTS . " where products_id =  '" . $arrayProducts[2] . "' order by products_model");
							$data2 = tep_db_fetch_array($query2);
							
							$arrayProductsInfos[$arrayProducts[0]]=array('products_price' => $data['products_price']);
							$arrayProductsInfos[$arrayProducts[1]]=array('products_price' => $data1['products_price']);
							$arrayProductsInfos[$arrayProducts[2]]=array('products_price' => $data2['products_price']);
							echo '<fieldset>';
								$option_cat=$option_cat_1=$option_cat_2='<option value="0">Choisir une Marque</option>';
								$option_family=$option_family_1=$option_family_2='<option value="0">Choisir une Famille</option>';
								for($i=0; $i<count($marque); $i++) {
									$option_cat.= ($marque[$i]['index']==$data['manufacturers_id']) ? '<option selected value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>' : '<option value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
									if ($marque[$i]['index']==$data1['manufacturers_id']) {
										$option_cat_1.= '<option selected value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
										$marque1_selected=$marque[$i]['index']; // on sauvegarde l'id de la marque dans laquelle se trouve le produit 1
									}
									else $option_cat_1.='<option value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
									
									if($marque[$i]['index']==$data2['manufacturers_id']) {
										$option_cat_2.= '<option selected value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
										$marque2_selected=$marque[$i]['index']; // on sauvegarde l'id de la marque dans laquelle se trouve le produit 2
									}
									else $option_cat_2.='<option value="'. $marque[$i]['index'] .'">'. $marque[$i]['name'] .'</option>';
								}
								
								for($i=0; $i<count($family); $i++) {
									$option_family.= ($family[$i]['index']==$data['family_id']) ? '<option selected value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>' : '<option value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>';
									$option_family_1.= ($family[$i]['index']==$data1['family_id']) ? '<option selected value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>' : '<option value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>';
									$option_family_2.= ($family[$i]['index']==$data2['family_id']) ? '<option selected value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>' : '<option value="'. $family[$i]['index'] .'">'. $family[$i]['name'] .'</option>';
								}
								
								/* ON sauve les prix de produits dans un tableau javaScript */
								for($i=0; $i<count($product); $i++) {
									$option_product.= ($product[$i]['index']==$data['products_id']) ? '<option selected value="'. $product[$i]['index'] .'">'. $product[$i]['name'] .' ('. $product[$i]['model'] .')</option>' : '<option value="'. $product[$i]['index'] .'">'. $product[$i]['name'] .' ('. $product[$i]['model'] .')</option>';
									?><script type="text/javascript" language="javascript">
                  						tab[<?php echo $product[$i]['index']; ?>]= <?php echo $product[$i]['price']; ?>
					                  </script><?php
								}
								
								$i=0;
								$product1=array();
								
								$queryProduct1=tep_db_query("select p.products_id, products_name, products_model from " . TABLE_PRODUCTS . " p, ". TABLE_PRODUCTS_DESCRIPTION ." pd, ". TABLE_MANUFACTURERS ." m
								 							 where p.products_id=pd.products_id and p.manufacturers_id=m.manufacturers_id and 
															 	   m.manufacturers_id=". $marque1_selected ." order by products_model");
									
								while($data=tep_db_fetch_array($queryProduct1)) {
									$product1[$i]['index']=$data['products_id'];
									$product1[$i]['model']=$data['products_model'];
									$product1[$i++]['name']=$data['products_name'];
								}
								
								for($i=0; $i<count($product1); $i++) {
									$option_product_1.= ($product1[$i]['index']==$data1['products_id']) ? '<option selected value="'. $product1[$i]['index'] .'">'. $product1[$i]['name'] .' ('. $product1[$i]['model'] .')</option>' : '<option value="'. $product1[$i]['index'] .'">'. $product1[$i]['name'] .' ('. $product1[$i]['model'] .')</option>';
								}
								
								$i=0;
								$product2=array();
								
								if($marque_selected!='') {
									$queryProduct2=tep_db_query("select p.products_id, products_name, products_model from " . TABLE_PRODUCTS . " p, ". TABLE_PRODUCTS_DESCRIPTION ." pd, ". TABLE_MANUFACTURERS ." m
								 								 where p.products_id=pd.products_id and p.manufacturers_id=m.manufacturers_id and 
																 	   m.manufacturers_id=". $marque2_selected ." order by products_model");
									while($data=tep_db_fetch_array($queryProduct2)) {
										$product2[$i]['index']=$data['products_id'];
										$product2[$i]['model']=$data['products_model'];
										$product2[$i++]['name']=$data['products_name'];
									}
								}
								else {
									$option_product_2='<option value="0">Choisir un article</option>';
								}
								
								for($i=0; $i<count($product2); $i++) {
									$option_product_2.= ($product2[$i]['index']==$data2['products_id']) ? '<option selected value="'. $product2[$i]['index'] .'">'. $product2[$i]['name'] .' ('. $product2[$i]['model'] .')</option>' : '<option value="'. $product2[$i]['index'] .'">'. $product2[$i]['name'] .' ('. $product2[$i]['model'] .')</option>';
								}
								
								echo '<legend>Article Principal</legend>';
								echo '<p>';
									echo 'Marque : ';  echo '<select id="marque_0" name=marque_0" disabled>';  echo $option_cat;	echo '</select>';
									echo ' Famille : ';	echo '<select id="family_0" name=family_0" disabled>';	echo $option_family;	echo '</select>';
								echo '</p>';
								echo '<p>';
									echo 'Article : ';	echo '<select id="article_0" name="article_0" disabled>';	echo $option_product;	echo '</select>';
								echo '</p>';
								echo '<input type="hidden" id="price0" value="'. $arrayProductsInfos[$arrayProducts[0]]['products_price'] .'">';
							echo '</fieldset>';
							echo '</td>';
						echo '</tr>';
						
						//_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_-_ Artice 1 du Pack _-_-_-_-_-_-_-_-_-_-_-_--_-_-_-_-_-_-_-_-_-_-_-_-//
						echo '<tr>';
							echo '<td>';
							echo '<fieldset>';
								echo '<legend>Article 1 du Pack</legend>';
								echo '<p>';
									echo 'Marque : ';  echo '<select id="marque_1" name=marque_1">';  echo $option_cat_1;	echo '</select>';
									echo ' Famille : ';	echo '<select id="family_1" name=family_1">';	echo $option_family_1;	echo '</select>';
								echo '</p>';
								echo '<p>';
									echo '<img src="images/icons/copier.png" alt="Ajouter un article identique au premier" onclick="duplicate(\'1\')" style="cursor:pointer; margin-right:10px" alt="Ajouter un article identique au premier" />';
									echo 'Article : ';	echo '<select onClick="findPrice(tab[this.value], \'price1\')" id="article_1" name="article_1">';	echo $option_product_1;	echo '</select>';
								echo '</p>';
								echo '<input type="hidden" id="price1" value="'. $arrayProductsInfos[$arrayProducts[1]]['products_price'] .'">';
							echo '</fieldset>';
							echo '</td>';
						echo '</tr>';
						
						//_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_-_ Artice 2 du Pack (optionnel) _-_-_-_-_-_-_-_-_-_-_-_--_-_-_-_-_-_-_-_-_-_-_-_-//
						echo '<tr>';
							echo '<td>';
							echo '<fieldset>';
								echo '<legend>Article 2 du Pack</legend>';
								echo '<p>';
									echo 'Marque : ';  echo '<select id="marque_2" name=marque_2">';  echo $option_cat_2;	echo '</select>';
									echo ' Famille : ';	echo '<select id="family_2" name=family_2">';	echo $option_family_2;	echo '</select>';
								echo '</p>';
								echo '<p>';
									echo '<img src="images/icons/copier.png" alt="Ajouter un article identique au premier" onclick="duplicate(\'2\')" style="cursor:pointer; margin-right:10px"/>';
									echo 'Article : ';	echo '<select onClick="findPrice(tab[this.value], \'price2\')" id="article_2" name="article_2">';	echo $option_product_2;	echo '</select>';
								echo '</p>';
								echo '<input type="hidden" id="price2" value="'. $arrayProductsInfos[$arrayProducts[2]]['products_price'] .'">';
							echo '</fieldset>';
							echo '</td>';
						echo '</tr>';
						
						//_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_-_ R�capitulatif _-_-_-_-_-_-_-_-_-_-_-_--_-_-_-_-_-_-_-_-_-_-_-_-//
						$price=$arrayProductsInfos[$arrayProducts[0]]['products_price'] + $arrayProductsInfos[$arrayProducts[1]]['products_price'] + $arrayProductsInfos[$arrayProducts[2]]['products_price'];
						$priceHT=convert_price(round($price*100)/100);
						$priceTTC=round($price*120)/100;
						$priceTTC=convert_price($priceTTC);
						$remiseTTC=round(1.2*$remise); // $prix_remise == remise TTC
						
						echo '<tr>';
							echo '<td>';
							echo '<fieldset>';
								echo '<legend>Informations sur le Pack</legend>';
								echo '<p>';
									echo 'Prix Normal : ';  
									echo '<span id="priceNormalHt">'. $priceHT .' HT</span>'; 
									echo '<span id="priceNormalTtc">'. $priceTTC .'</span> TTC';
								echo '</p>';
								echo '<p>';
									echo 'Montant de la Remise : ';	echo '<input class="remiseHt" type="text" id="remiseHt" name="remiseHt" size="5" value="'. $remise .'"> &euro; HT';	
									echo '<input class="remiseTtc" type="text" id="remiseTtc" name="remiseTtc" size="3" value="'. $remiseTTC .'"> &euro; TTC';
								echo '</p>';
								
								$pricePackHT=convert_price(round(($priceHT-$remise)*100)/100);
								$pricePackTTC=convert_price(round(($priceTTC-$remiseTTC)*100)/100);
								$pourcent=convert_price(round((100-$pricePackTTC*100/$priceTTC)*100)/100);
								
								echo '<p>';
									echo 'Prix du Pack : ';	 echo '<span id="pricePackHt">'. $pricePackHT  .'</span> &euro; HT'; echo '<span id="pricePackTtc">'. $pricePackTTC .'</span> &euro; TTC';
								echo '</p>';
								echo '<p>';
									echo '% de Remise du Pack : ';	 echo '<span id="remisePackPt">'. $pourcent .'</span> %';
								echo '</p>';
							echo '</fieldset>';
							echo '</td>';
						echo '</tr>';
						echo '</table>';
						
						if (isset($_GET['pack']) && $_GET['pack']>0) {
							
							echo '<input type="hidden" name="pack" id="pack" value="'. $_GET['pack'] .'">';
							echo '<input type="button" value="Modifier" onclick="return verifArticle(this.form), \'1\'"/>';
						}
						
					}
					
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_SUPPRESSION D'UN PACK _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_SUPPRESSION D'UN PACK _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
					
					
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ AFFICHAGE DES PACKS _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
/*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ AFFICHAGE DES PACKS _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_*/
						
					else { // page d'affichage de tous les packs
            	
						$option='<option value="0">Marque</option>';
						$query=tep_db_query("select m.manufacturers_id, manufacturers_name from ". TABLE_MANUFACTURERS ." m, ". TABLE_MANUFACTURERS_INFO ." mi where m.manufacturers_id=mi.manufacturers_id order by manufacturers_name");
						while($data=tep_db_fetch_array($query)) {
							if($_GET['manufacturers_id']==$data['manufacturers_id']) $option.='<option selected value="'. $data['manufacturers_id'] .'">'. $data['manufacturers_name'] .'</option>';
							else $option.='<option value="'. $data['manufacturers_id'] .'">'. $data['manufacturers_name'] .'</option>';
						}
						echo 'Tri par ';
						echo '<select name="marqueId" id="marqueId">';
							echo $option;
						echo '</select>';
							
						$option='<option selected value="0">Famille</option>';
						$query=tep_db_query("select family_id, family_name from ". TABLE_FAMILY ." order by family_name");
							
						while($data=tep_db_fetch_array($query)) {
							if($_GET['manufacturers_id']==$data['manufacturers_id']) $option.='<option selected value="'. $data['family_id'] .'">'. $data['family_name'] .'</option>';
							else $option.='<option value="'. $data['family_id'] .'">'. $data['family_name'] .'</option>';
						}
						        
            			echo ' et/ou par ';
						echo '<select name="familyId" id="familyId">';
							echo $option;
						echo '</select> ';
						?>
            
            - Recherche par ID : <input id="searchId" value="Id article" type="text" onClick="if(this.value=='Id article') this.value='';">  
            
          </td>
        </tr>
        <tr>
          <td align="left">&nbsp;</td>
        </tr>
		<tr>
			<td align="left">
            <?php 
				include('pack_list.php');
			} ?>
 		  		</td>
				</tr>
	   	</table>
		</td>
	</tr>
</table>
		</td>
  </tr>
</table>
</form>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>