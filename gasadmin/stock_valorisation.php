<?php
  require('includes/application_top.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

    <form name="inventaire" method="post">
	<table border="0" width="100%" cellspacing="2" cellpadding="2">
      <tr>
        <td width="100%" class="pageHeading">Valorisation du stock</td>
      </tr>
      <tr>
        <td><hr /></td>
      </tr>
      <tr>
      	<td> <?php 
			$produits="SELECT p.products_quantity as qte, p.products_cost as cout, p.products_price as prix, family_name, f.family_id from products p inner join family f on p.family_id=f.family_id order by family_name";
			$resultat_produit=tep_db_query($produits);
			$NB_PRODUIT=tep_db_num_rows($resultat_produit);
			$total_qte=0;
			$total_cout=0;
			$total_prix=0;
			$qte_total=0;
			$cout_total=0;
			$prix_total=0;
			$family_id='';
			if($NB_PRODUIT!=0){?>
				<table border="1" width="800" cellspacing="0" cellpadding="0">
				<tr>
                    <td align="center">Nom</td>
                    <td align="center">Qte</td>
                    <td align="center">Cout</td>
                    <td align="center">Valeur</td>
                </tr>
			<?php	
				while($PRODUIT=tep_db_fetch_array($resultat_produit)){
					if($PRODUIT["family_name"]!=$family_name && $family_name!=''){?>
                        <tr>
                            <td align="center"><?php echo $family_name;?></td>
                            <td align="center"><?php echo $qte_total;$total_qte+=$qte_total;?></td>
                            <td align="center"><?php echo number_format($cout_total,2,',',' ');$total_cout+=$cout_total;?></td>
                            <td align="center"><?php echo number_format($prix_total,2,',',' ');$total_prix+=$prix_total;?></td>
                        </tr>
             <?php 
			 			$qte_total=0;
						$cout_total=0;
						$prix_total=0;
					}
			 	   	$family_name=$PRODUIT["family_name"];
			 	   	$qte_total+=$PRODUIT["qte"];
					$cout_total+=$PRODUIT["cout"]*$PRODUIT["qte"];
					$prix_total+=$PRODUIT["prix"]*$PRODUIT["qte"];
				}
            }
			else{ echo "Pas de produits pour cette famille";}?>
            <tr>
                <td align="center"><?php echo $family_name;?></td>
                <td align="center"><?php echo $qte_total;$total_qte+=$qte_total;?></td>
                <td align="center"><?php echo number_format($cout_total,2,',',' ');$total_cout+=$cout_total;?></td>
                <td align="center"><?php echo number_format($prix_total,2,',',' ');$total_prix+=$prix_total;?></td>
            </tr>
            <tr>
                <td align="center">TOTAL</td>
                <td align="center"><?php echo number_format($total_qte,2,',',' ');?></td>
                <td align="center"><?php echo number_format($total_cout,2,',',' ');?></td>
                <td align="center"><?php echo number_format($total_prix,2,',',' ');?></td>
            </tr>
            </table>
      	</td>
      </tr>
    </table>
    </form></td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>