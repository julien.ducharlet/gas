<?php
/*
$Id:cron_suivi_commandes.php, v 1.0 11/03/2009 phocea (forum oscommerce-fr.info) $ 

osCommerce, Open Source E-Commerce Solutions
https://www.oscommerce.com

Copyright � 2002 osCommerce

Released under the GNU General Public License

Based on original idea by Regne
*/

require('includes/application_top.php'); 

include(DIR_WS_INCLUDES . 'reclamaposte.php');
include(DIR_WS_FUNCTIONS . 'reclamaposte.php');
include(DIR_WS_FUNCTIONS . 'reclamaposte_dates.php');
require(DIR_WS_LANGUAGES . $language . '/reclamaposte.php');

  if (COLIPOSTE_MAX_DAYS > 0) {
  	$coliposte_query = "select o.`orders_id`, o.`orders_status`, o.`customers_email_address`,  o.`date_purchased`, o.`customers_name`, os.`date_added`, os.`".TRACKING_DB_FIELD."`, os.`laposte_last_date`, os.`laposte_status`, os.`laposte_md5`, os.`laposte_notif_md5` from " . TABLE_ORDERS . " o, ". TABLE_ORDERS_STATUS_HISTORY ." os where o.orders_id = os.orders_id and SUBSTR(os.".TRACKING_DB_FIELD.", 1, 2) IN ('8N','8U','8L','8P', '8V','7D') and DATE_SUB(CURDATE(),INTERVAL ".COLIPOSTE_MAX_DAYS." DAY) <= o.date_purchased order by o.date_purchased desc, os.date_added asc;";
  } else {
  	$coliposte_query = "select o.`orders_id`, o.`orders_status`, o.`customers_email_address`,  o.`date_purchased`, o.`customers_name`, os.`date_added`, os.`".TRACKING_DB_FIELD."`, os.`laposte_last_date`, os.`laposte_status`, os.`laposte_md5`, os.`laposte_notif_md5` from " . TABLE_ORDERS . " o, ". TABLE_ORDERS_STATUS_HISTORY ." os where o.orders_id = os.orders_id and SUBSTR(os.".TRACKING_DB_FIELD.", 1, 2) IN ('8N','8U','8L','8P', '8V','7D') order by o.date_purchased desc, os.date_added asc;";
  }

  $coliposte_query_r =  tep_db_query($coliposte_query);
  
  while($qr = tep_db_fetch_array($coliposte_query_r)) {
  	
    $customer_email      = $qr['customers_email_address'] ;
    $date_purchased      = $qr['date_purchased'] ;
    $customers_name      = $qr['customers_name'] ;
    $orders_id           = $qr['orders_id'] ;
    $date_expedition     = substr($qr['date_added'], 0, strpos($qr['date_added'], ' '));
    $noColis           	 = $qr[TRACKING_DB_FIELD] ;
    $laposte_last_date   = $qr['laposte_last_date'] ;
    $laposte_status      = $qr['laposte_status'] ;
    $laposte_md5         = $qr['laposte_md5'] ;
    $laposte_notif_md5   = $qr['laposte_notif_md5'] ;
    
    if (COLIPOSTE_IN_TEST == 1) echo 'STARTING: '.$orders_id.'<br />';
    // Date exp�dition de la commande
		$y = substr($date_expedition, 0, 4); 
		$m = substr($date_expedition, 5, 2); 
		$d = substr($date_expedition, 8, 2); 
		$date_expedition = strtotime(sprintf('%04d-%02d-%02d', $y, $m, $d));
		
    $statuses_found = array();
    $laposte_new_md5 = '';
    $customer_notified = 0;
    $case = '';
    // Cas ou le status de la commande n'est plus � traiter
    if ( $laposte_status != COLIPOSTE_RECLAMATION_REMBOURSE 
    	&& $laposte_status != COLIPOSTE_ARCHIVE) {
    		if (COLIPOSTE_IN_TEST == 1) echo 'CONTINUING: '.$orders_id.' | Last Status'. $laposte_md5 .' | Last Notif'. $laposte_notif_md5 .'<br>';
    		// si la commande a �t� livr�, on v�rifie aussi si le client a �t� notifi�.
    		if ($laposte_status != COLIPOSTE_LIVRAISON_OK
    			&& $laposte_status != COLIPOSTE_LIVRAISON_RETARD
    			&& $laposte_status != COLIPOSTE_RECLAMATION_DEMANDE 
    			|| $laposte_md5 != $laposte_notif_md5) {
    		
			    // Si la commande date de + que le nb de jours sp�cifi�, nul besoin de demander...
					$age_commande = get_nb_open_days($date_expedition, time());
					if (COLIPOSTE_IN_TEST == 1) echo $orders_id.' - Age Commande: '.$age_commande .'<br />';
					if ($age_commande > COLIPOSTE_TO_BE_ARCHIVED) {
						# On archive la commande ici
						$case  = 10;			
					} else {
						// On r�cup�re les status sur le site de la poste
						$statuses_found = grab_coliposte_statuses($noColis);
						$laposte_new_md5 = $statuses_found[0];
						
						if (COLIPOSTE_IN_TEST == 1) echo $orders_id.' - old status: '.$laposte_md5.', new status: '.$laposte_new_md5.'<br />';
						// Mise � jour de l'historique de suivi avec les derni�res informations
						$case =  merci_la_poste($noColis, $statuses_found);
						// Le status a t il chang� depuis la derniere verification?
						if ($laposte_new_md5 != '' && $laposte_new_md5 != $laposte_notif_md5) {
							echo $orders_id.' - NEW STATUS FOUND!!<br />';
							$customer_notified = $status[$laposte_new_md5]['notification_client'];
							
						}
					}
		
					$laposte = '';
					$date_livraison  = '';
					$nb_jours_ouvres = '';
	
					switch( $case ) {
						// Le Colis a �t� livr�, process de v�rification des dates.
						case 1 :
							// OCR processing
							$date_livraison  = interpret_date();
							if (COLIPOSTE_IN_TEST == 1) echo $orders_id.' - CASE '.$case.': '.$date_expedition . '/'. date(DATE_FORMAT, $date_livraison) .'<br />';
							$nb_jours_ouvres = get_nb_open_days($date_expedition, $date_livraison);
			
			        // Probl�me de lecture
			        if ( $date_livraison < 0 ) break ;
			
							if ( $nb_jours_ouvres > 2) {
								$laposte = COLIPOSTE_LIVRAISON_RETARD;
							} else {
								$laposte = COLIPOSTE_LIVRAISON_OK;
							}
							break;
						// Cas dans lesquels la commande est encore en transit
						case 5 :
							// OCR processing
							$date_livraison  = interpret_date();
							if (COLIPOSTE_IN_TEST == 1) echo $orders_id.' - CASE '.$case.': '.$date_expedition . '/'. $date_livraison .'<br />';
							$nb_jours_ouvres = get_nb_open_days($date_expedition, $date_livraison);
							$laposte = COLIPOSTE_LIVRAISON_TRANSIT;
							break;
						// Cas dans lesquels la commande n'est pas remboursable
						case 9 :
							$date_livraison  = interpret_date();
							if (COLIPOSTE_IN_TEST == 1) echo $orders_id.' - CASE '.$case.': '.$date_expedition . '/'. $date_livraison .'<br />';
							$nb_jours_ouvres = get_nb_open_days($date_expedition, $date_livraison);
							$laposte = COLIPOSTE_REMBOURSEMENT_IMPOSSIBLE;
							break;
						// Cas dans lesquels on archive la commande
						case 10 :
							if (COLIPOSTE_IN_TEST == 1) echo $orders_id.' - CASE '.$case.': '.$date_expedition . '/'. $date_livraison .'<br />';
							$laposte = COLIPOSTE_ARCHIVE;
							break;
						// Le colis n'existe pas sur colisposte !
						case -2 :
							if (COLIPOSTE_IN_TEST == 1) echo $orders_id.' - CASE '.$case.': '.$date_expedition . '/'. $date_livraison .'<br />';
							$laposte = COLIPOSTE_NO_INFORMATION;
							break;
						// Cas dans lesquels l'image n'a pas �t� reconnu
						case -1 :
							$laposte = COLIPOSTE_ERREUR_MD5;
							break;
					}
						
					// si le statut est livr� on mets � jours le suivi de commande 
					$orders_insert_query = 'update ' . TABLE_ORDERS . ' set orders_status = "' . tep_db_input(STATUS_COMMANDE_DELIVREE) . '", last_modified = now() where orders_id = "' . $orders_id . '";';
					$orders_history_update_query = 'update ' . TABLE_ORDERS_STATUS_HISTORY . ' set laposte_status = "' . $laposte .'", laposte_last_date="' . $date_livraison .'", laposte_md5="' . $laposte_new_md5 .'", laposte_notif_md5="' . $laposte_new_md5 . '" where orders_id = "' . $orders_id . '" and ' . TRACKING_DB_FIELD . ' = "'.$noColis.'"  ; ' ;
					$orders_history_insert_query = 'insert into ' . TABLE_ORDERS_STATUS_HISTORY . ' (orders_id, orders_status_id, date_added, customer_notified, laposte_status, laposte_last_date, laposte_md5) values ("'. $orders_id .'", "'. tep_db_input(STATUS_COMMANDE_DELIVREE) .'", now(), "'. tep_db_input($customer_notified).'", NULL, NULL, NULL);' ;
	
					if ($laposte == COLIPOSTE_LIVRAISON_OK
	  				|| $laposte == COLIPOSTE_LIVRAISON_RETARD) {
	  					if (COLIPOSTE_IN_TEST == 1) {
	  						echo 'update order to livr� :'.$orders_insert_query.'<br />';
	  						echo 'update order history after livraison livr� :'.$orders_history_update_query.'<br />';
	  						tep_db_query($orders_history_update_query);
	  						echo 'new order history after livraison livr� :'.$orders_history_insert_query.'<br />';
	  					} else {
	  						tep_db_query($orders_insert_query);
	  						tep_db_query($orders_history_insert_query);
	  						tep_db_query($orders_history_update_query);
	  					}
	  			} else {
						if (COLIPOSTE_IN_TEST == 1) echo 'update order history pendant la livraison :'.$orders_history_update_query.'<br />';
						tep_db_query($orders_history_update_query);
					}
					
					// Notification du client par email
					if ($customer_notified == 1) {
						$template_email = $status[$laposte_new_md5]['notification_email_template'];
						if (COLIPOSTE_IN_TEST == 1) $customer_email = LAPOSTE_EMAIL_EXPEDITEUR;
							
						$email_subject = sprintf(constant("LAPOSTE_EMAIL_SUBJECT_$template_email"), $orders_id);
						if (COLIPOSTE_IN_TEST == 1) echo 'SUJET EMAIL :'.$email_subject.'<br />';
						$email_text = sprintf(constant("LAPOSTE_EMAIL_TEXT_$template_email"), $customers_name, $noColis, date(DATE_FORMAT, $date_livraison), $status[$laposte_new_md5]['description']);
						if (COLIPOSTE_IN_TEST == 1) echo 'TEXT EMAIL :'.$email_text.'<br />';
						tep_mail($customers_name, $customer_email, $email_subject, $email_subject, LAPOSTE_EMAIL_EXPEDITEUR, LAPOSTE_EMAIL_EXPEDITEUR);
					}
    		}
    }
  }
?>