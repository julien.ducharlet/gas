<?php
/*
  $Id: stats_products_viewed.php,v 1.29 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');
  
if(isset($HTTP_GET_VARS['raz_vue'])) {
  		if($HTTP_GET_VARS['raz_vue'] == 1) {
			 tep_db_query('UPDATE ' . TABLE_PRODUCTS_DESCRIPTION . ' SET products_viewed = 0');
			 $confirm_message_vue = mysql_affected_rows().' produit(s) remis � 0.';
		}
  }
else $raz_vue=0;

if(isset($HTTP_GET_VARS['raz_achete'])) {
  		if($HTTP_GET_VARS['raz_achete'] == 1) {
			 tep_db_query('UPDATE ' . TABLE_PRODUCTS . ' SET products_ordered = 0');
			 $confirm_message_achete = mysql_affected_rows().' produit(s) remis � 0.';
		}
  }
else $raz_achete=0;

//debut modification Polytech Montpellier
if(isset($_GET['sort']))  $sort = tep_db_prepare_input($HTTP_GET_VARS['sort']);
else $sort ='1d';

if(isset($_GET['manufacturers_id']) && !empty($_GET['manufacturers_id'])) $manufacturers_id = tep_db_prepare_input($HTTP_GET_VARS['manufacturers_id']);
else $manufacturers_id ='ALL';

if(isset($_GET['filter_id']))  $filter_id = tep_db_prepare_input($HTTP_GET_VARS['filter_id']);
else $filter_id ='ALL';


if(isset($_GET['nbmax']))  $nbmax = tep_db_prepare_input($HTTP_GET_VARS['nbmax']);
else $nbmax =20;
		
//valeurs de la liste deroulantes pour le nombre d element a afficher
$pages_array = array();$i=20;
while($i<=100){
$pages_array[] = array('id' => $i, 'text' => $i);
$i=$i+10;}


$filterlist_sql = "select distinct m.manufacturers_id as id, m.manufacturers_name as name 
from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, " . TABLE_MANUFACTURERS . " m 
where p.products_status = '1' and p.manufacturers_id = m.manufacturers_id and p.products_id = p2c.products_id order by m.manufacturers_name";


$filterlist_query = tep_db_query($filterlist_sql);
  
	if (tep_db_num_rows($filterlist_query) > 1) {
        
        
        $options = array(array('id' => '', 'text' => 'Tous les fabricants'));
        
		
        while ($filterlist = tep_db_fetch_array($filterlist_query)) {
          $options[] = array('id' => $filterlist['id'], 'text' => $filterlist['name']);
        }
      
      }
	  
	  $parametres = array('sort' => $sort,
                         'nbmax' => $nbmax,
                         'manufacturers_id' => $manufacturers_id,
                         'filter_id' => $filter_id);
	  
	  
	  
	  
	  
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	
      <tr>
        <td>
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="30%" class="pageHeading" ><?php echo 'Les produits les plus consult&eacute;s'; ?></td>
			<td width="30%" class="smallText" align="right"><form method="get"><?php echo "nombre d'elements par page :";
			echo tep_draw_pull_down_menu('nbmax', $pages_array, '', 'onChange="this.form.submit();"');
			?></td>
			 <td width="40%" class="smallText" align="right"><?php
			 echo "Afficher :";
			 //echo tep_draw_hidden_field('manufacturers_id', $_GET['manufacturers_id']);
			  
			  echo tep_draw_pull_down_menu('manufacturers_id', $options, $_GET['manufacturers_id'], 'onchange="this.form.submit()"');
        
       ?></form></td>
	          </tr>
        </table>
		</td>
      </tr>
	  <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><div align="center" class="main">
			<?php
				if(!empty($confirm_message_vue))  echo $confirm_message_vue;
				else echo '<a href="?raz_vue=1" onClick="return confirm(\''.'�tes vous certain(e)s de vouloir remettre � z�ro les compteurs ?'.'\')">'.'Remettre � z�ro les compteurs pour les vus'.'</a>';
			?>
			</div>
			<div align="center" class="main">
			<?php
				if(!empty($confirm_message_achete))  echo $confirm_message_achete;
				else echo '<a href="?raz_achete=1" onClick="return confirm(\''.'�tes vous certain(e)s de vouloir remettre � z�ro les compteurs ?'.'\')">'.'Remettre � z�ro les compteurs pour les achete'.'</a>';
			?>
			</div>
			
			<br>
			
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td width="4%" class="dataTableHeadingContent"><?php echo 'No.'; ?></td>
                <td width="29%" class="dataTableHeadingContent"><?php echo 'Produits'; ?></td>
				<td width="29%" class="dataTableHeadingContent"align="left"><?php echo "Reference"; ?></td>
                <td width="14%" class="dataTableHeadingContent" align="center"><?php echo 'Vu'; ?>&nbsp;
				<?php echo '<a href="' .tep_href_link(FILENAME_STATS_PRODUCTS_VIEWED, 'sort=1a&nbmax='.$nbmax.'&manufacturers_id='.$manufacturers_id.'&filter_id='.$filter_id) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_up.gif', "UP") . '</a>';?>     
				<?php echo '<a href="' .tep_href_link(FILENAME_STATS_PRODUCTS_VIEWED, 'sort=1d&nbmax='.$nbmax.'&manufacturers_id='.$manufacturers_id.'&filter_id='.$filter_id) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_down.gif', "DOWN") . '</a>';?></td>
				
				<td width="14%" class="dataTableHeadingContent" align="center"><?php echo 'Achet&eacute;'; ?>&nbsp;
				<?php echo '<a href="' .tep_href_link(FILENAME_STATS_PRODUCTS_VIEWED, 'sort=2a&nbmax='.$nbmax.'&manufacturers_id='.$manufacturers_id.'&filter_id='.$filter_id) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_up.gif', "UP") . '</a>';?>     
				<?php echo '<a href="' .tep_href_link(FILENAME_STATS_PRODUCTS_VIEWED, 'sort=2d&nbmax='.$nbmax.'&manufacturers_id='.$manufacturers_id.'&filter_id='.$filter_id) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_down.gif', "DOWN") . '</a>';?>
				</td>
              </tr>
<?php
  if (isset($HTTP_GET_VARS['page']) && ($HTTP_GET_VARS['page'] > 1)) $rows = $HTTP_GET_VARS['page'] * $nbmax - $nbmax;
  $rows = 0;
   
  
    
  
// show the products of a specified manufacturer
    if ($manufacturers_id !='ALL') {
// We are asked to show only a specific category
		if ($filter_id !='ALL'){
        $products_query_raw = "select p.products_id, pd.products_name, pd.products_viewed, p.products_ordered,p.products_model 
  from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_CATEGORIES . " cat," . TABLE_PRODUCTS_TO_CATEGORIES . " p2cat
  where p.products_id = pd.products_id and p.products_id= p2cat.products_id and p2cat.categories_id=cat.categories_id 
  and cat.categories_id='".$filter_id."' and p.manufacturers_id='".$manufacturers_id."'and pd.language_id = '" . $languages_id. "'";
		} else {
// We show them all
        $products_query_raw = "select p.products_id, pd.products_name, pd.products_viewed, p.products_ordered,p.products_model  
  from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd 
  where p.products_id = pd.products_id and p.manufacturers_id='".$manufacturers_id."'and pd.language_id = '" . $languages_id. "'";
  }
   }
if ($manufacturers_id =='ALL') {
	if ($filter_id !='ALL'){
// We show a specific category
$products_query_raw = "select p.products_id, pd.products_name, pd.products_viewed, p.products_ordered ,p.products_model 
  from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd, ".TABLE_PRODUCTS_TO_CATEGORIES." p2c 
  where p.products_id = pd.products_id and p2c.categories_id='" . $filter_id. "' and p2c.products_id =p.products_id and pd.language_id = '" . $languages_id. "'";

}else{
// We show them all
$products_query_raw = "select p.products_id, pd.products_name, pd.products_viewed, p.products_ordered ,p.products_model 
  from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd 
  where p.products_id = pd.products_id and pd.language_id = '" . $languages_id. "'";
}
}
   $products_query =$products_query_raw;
   
  switch ($sort) {
		  case '1d':
            $products_query_raw .=" order by pd.products_viewed DESC,pd.products_name";//pour le tri de maniere descandante
          break;
		  case '2d':
		    $products_query_raw .=" order by p.products_ordered DESC,pd.products_name";
            break;
		  case '1a':
            $products_query_raw .=" order by pd.products_viewed ASC,pd.products_name";//pour le tri de maniere ascendante
          break;
		  case '2a':
		    $products_query_raw .=" order by p.products_ordered ASC,pd.products_name";
          break;
	}
  
  $products_split = new splitPageResults($HTTP_GET_VARS['page'], $nbmax, $products_query_raw, $products_query_numrows);
  
  
  // pour fixer le nombre de clients en bas a gauche de la page
  $products_query_numrows = tep_db_query($products_query);
  $products_query_numrows = tep_db_num_rows($products_query_numrows);

  
  $products_query = tep_db_query($products_query_raw);
 
 while ($products = tep_db_fetch_array($products_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" onClick="document.location.href='<?php echo tep_href_link(FILENAME_ARTICLE_EDIT, 'action=new_product&pID=' . $products['products_id'], 'NONSSL'); ?>'">
                <td class="dataTableContent"><?php echo $rows; ?>.</td>
                <td class="dataTableContent"><?php 
				// EXTENSION POLYTECH 
				// fonction qui permet de decrypter la description du produit lors de la validation de la commande
				$id_modele = explode("_", $cPath);
				$products['products_name']=bda_product_name_transform($products['products_name'],$id_modele[1]);	
				echo '<a href="' . tep_href_link(FILENAME_ARTICLE_EDIT, 'action=new_product&pID=' . $products['products_id'], 'NONSSL') . '">' . $products['products_name'] . '</a>'; ?></td>
				<td class="dataTableContent" align="left"><?php echo $products['products_model']; ?>&nbsp;</td>
			   	<td class="dataTableContent" align="center"><?php echo $products['products_viewed']; ?>&nbsp;</td>
				<td class="dataTableContent" align="center"><?php echo $products['products_ordered']; ?>&nbsp;</td>
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, $nbmax, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
                <td class="smallText" align="right"><?php 
				echo $products_split->display_links_with_sort2($products_query_numrows, $nbmax, MAX_DISPLAY_PAGE_LINKS, 
				$HTTP_GET_VARS['page'],$parametres);

				?></td>
              </tr>
			  <tr>
			  <?php if ($manufacturers_id !='ALL') { ?>
			  <td align="left"><?php echo '<a href="' . tep_href_link(FILENAME_STATS_PRODUCTS_VIEWED, '', 'NONSSL') . '">' . tep_image_button('button_back.gif', 'retour menu principal') . '</a>'; ?></td>
                <?php } ?>
			   </tr>            
			</table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>