<?php
/* Page permettant de lister le module de livraison depuis l'admin */

  require('includes/application_top.php');
  
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">//
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->
<script language="javascript1.1" src="includes/javascript/ajax/module_livraison.js"></script>
<!-- body //-->
<div style="width:100%; height:600px;">

	<div style="font-size:25px;">Gestion du module des livraisons</div>
	<!-- Partie permettant de modifier ou d'ajouter un moyen de paiment -->

	<!-- Partie listant tous les modules quand on arrive sur la page -->
	<div id="les_moyens" style="display:block;">
       <?php     
           if(isset($_POST['ajout']) || isset($_POST['modif'])){
            echo "Ajouter";
        ?>
            <script language="javascript1.1" > document.getElementById("les_moyens").style.display="none"; </script>
        <?php }	?>
        <table style="background-color:#999; width:100%; margin-top:20px; text-align:center;">
            <tr>
            <td>ID</td>
            <td>Pays concern&eacute;s</td>
            <td>Actions</td>
            <td style="clear:both;"></td>
            </tr>
        </table>        
        <?php
        $req_moyen=tep_db_query("select distinct(zone_livraison_id) from ".TABLE_MODULE_LIVRAISON."");	
        $c=0;
        while($res_moyen=tep_db_fetch_array($req_moyen)){            
            if($c % 2==0){$couleur="#CCC";}
            else {$couleur="#DDD";}        
        ?>
        <table style="background-color:<?php echo $couleur; ?>; width:100%; height:30px; text-align:center;">
        <tr>
            <td style="float:left; width:10%;">
            	<a href="module_livraison_edit.php?z_id=<?php echo $res_moyen['zone_livraison_id']; ?>">
					<?php echo $res_moyen['zone_livraison_id']; ?>
                </a>
            </td>
            <td style="float:left; width:80%; text-align:left;">&nbsp;
				<?php 
				  $req_country=tep_db_query("select countries_name from countries where countries_id_zone_livraison=".$res_moyen['zone_livraison_id']." ORDER BY countries_name");
				  $nb=tep_db_num_rows($req_country);
				  $i=1;
				  while($countries=tep_db_fetch_array($req_country)){
					 if($nb>1 && $nb) $vir=" ||||| "; 
					  echo $countries['countries_name']. $vir;
					  $i++;
				  }
				  if($nb==0){ echo"<i><strong>Cette zone a des conditons de prix mais aucun pays rattach&eacute;s</strong></i>";}
			     ?>
            </td>
            <td style="float:left; width:10%; text-align:right;">
                <a href="module_livraison_edit.php?z_id=<?php echo $res_moyen['zone_livraison_id']; ?>">
                    <img border="none" src="images/icons/editer.png" title="editer" /></a> &nbsp; 
                <!--<a href="#" >
                	<img border="none" src="images/icons/supprimer.png" title="supprimer" onClick="supprime_zone(<?php //echo $res_moyen['zone_livraison_id']; ?>);" /></a>-->
             </td>
            <td style="clear:both;"></td>
            <tr>
        </table>
        <?php 
        $c++;
        }	
        ?>
        <div style="width:95%; margin-left:30px; height:30px; margin-top:10px; text-align:right;">
            <div style="float:right; width:300px;"><input type="button" id="add_zone" name="moyen" value="AJouter une zone de livraison" onClick="new_zone();" /></div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
<!-- FIN Partie listant tous les modules quand on arrive sur la page -->  
</div>

<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>