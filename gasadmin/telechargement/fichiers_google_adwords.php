<?php
/*
  $Id: fichiers_google_adwords.php - Version 1 - 11/11/2006

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

 
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="JavaScript" src="ajax/duplicate.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<div id="popupcalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Fichier Google Adwords </td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table>
		</td>
       </tr>
	   
	   <tr>
	  <td align="left">		
	  <form name="listes" action="fichiers_google_adwords.php" method="post"> 
        <br />
		<input name="av" type="text" value="#" size="10">
        <select name="categories">
          <?
				$requete_marques='SELECT cd.categories_name as nom, cd.categories_id as id '
								.'FROM categories_description cd, categories c '
								.'WHERE c.parent_id=0 '
								.'AND c.categories_id=cd.categories_id '
								.'ORDER by nom';
				$reponse_marques=mysql_query($requete_marques);
				while($fetch=mysql_fetch_object($reponse_marques))
					{
						echo '<option value="'.$fetch->id.'">'.$fetch->nom.'</option>';
					}									
			?>
        </select>
        <input name="ap" type="text" value="" size="10">
		&nbsp;&nbsp; prix au clic : 
  		<input type="text" name="prix" size="3"> 
  		&nbsp;&nbsp; ajouter l'URL : 
  		<input type="checkbox" name="url" value="url" <? if(@isset($_POST['url'])) { echo 'checked="checked"'; } ?> />
		<input type="submit" value="Lister les t&eacute;l&eacute;phones"  name="lister_telephones">
  		</form>		
  		</td>
	</tr>
	<tr align="left">
		<td>
			<?
				if(@isset($_POST['lister_telephones']))
					{

						$liste_telephones='SELECT cd.categories_name as nom, cd.categories_id as id '
										 .'FROM categories_description cd, categories c '
										 .'WHERE c.parent_id="'.$_POST['categories'].'" '
										 .'AND c.categories_id=cd.categories_id '
										 .'ORDER by nom'; 
						//echo $liste_telephones;
						$reponse_telephones=mysql_query($liste_telephones);
						echo '<textarea rows="'.mysql_num_rows($reponse_telephones).'" cols="100">';
						@$avant=$_POST['av'];
						@$apres=$_POST['ap'];
						
						while($fetch=mysql_fetch_object($reponse_telephones))
							{
								echo $avant.' '.$fetch->nom.' '.$apres.' ';
								if(@!empty($_POST['prix']))
									{
										echo '** ' . $_POST['prix'].' ';
									}
								
								if(@isset($_POST['url']))
									{
										echo '** https://'.WEBSITE.'/index.php?cPath='.$_POST['categories'].'_'.$fetch->id;
									}
								echo "\n";
							}
						echo '</textarea>';
					}
			?>		</td>
	</tr>
	   
	   
	  </table>
	 </td>
	</tr>

      </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
