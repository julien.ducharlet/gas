<?php
/*
  Page : telephone_sans_images.php

  Modifier le 05/06/2007
*/

  require('includes/application_top.php');

  $url_google="https://images.google.fr/images?svnum=10&um=1&hl=fr&sa=X&oi=spell&resnum=0&ct=result&cd=1&q=_RECHERCHE_&spell=1";
		
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td class="pageHeading">Articles cach&eacute;s sur <?php echo STORE_NAME; ?> </td>
       </tr>
	   
	   <tr>
	    <td>
	   		
			<table>
            	<tr>
                	<td>Articles cach�s avec stock</td>
                	<td>Articles cach�s sans stock</td>
                </tr>
				<tr>
                	<td style="vertical-align:top;">
                    
                    <?php
                    $query = 'select p.products_id, products_model, products_name, products_quantity_reel, products_date_available ';
					$query .= 'from '. TABLE_PRODUCTS .' p, '. TABLE_PRODUCTS_DESCRIPTION . ' pd ';
					$query .= 'where p.products_id=pd.products_id and products_status=0 and products_quantity_reel>0 order by products_model';
					
					$query = tep_db_query($query);
					
					if(tep_db_num_rows($query) > 0) {
						
						echo '<table class="infoBoxContent">';
						
							echo '<tr>';
								echo '<td>R�f�rence</td>';
								echo '<td>Article</td>';
								echo '<td>Stock</td>';
								echo '<td>Date de disponibilit�</td>';
								echo '<td>Action</td>';
							echo '</tr>';
						
							while($data = tep_db_fetch_array($query)) {
								
								echo '<tr>';
									echo '<td><a href="article_edit.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. $data['products_model'] .'</a></td>';
									echo '<td><a href="article_edit.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. bda_product_name_replace('', $data['products_name']).'</a></td>';
									echo '<td class="align_right"><a href="article_edit_stocks_options.php?pID='. $data['products_id'] .'&action=new_product">'. $data['products_quantity_reel'] .'</a></td>';
									echo '<td>'. tep_date_long($data['products_date_available']) .'</td>';
									
									echo '<td>';
										echo '<a href="article_edit.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. tep_image(DIR_WS_IMAGES . '/icons/editer.png', "Editer la cat�gorie") .'</a>&nbsp;&nbsp;';
										echo '<a href="article_list.php?pID=' . $data['products_id'] .'&action=delete_product' .'" target="blank">'. tep_image(DIR_WS_IMAGES . '/icons/supprimer.png', "Supprimer l\'article") .'</a>&nbsp;&nbsp;';
										echo '<a href="cmd_en_attente.php?pID=' . $data['products_id'] .'" target="blank">'. tep_image(DIR_WS_IMAGES . '/icons/commande_02.png', "Voir les commandes avec cet article") .'</a>';
									echo '</td>';
								echo '</tr>';
							}
						
						echo '</table>';
					}
                    
                    ?>
                    </td>
                	<td style="vertical-align:top;">
                    <?php
                    $query = 'select p.products_id, products_model, products_name, products_date_available ';
					$query .= 'from '. TABLE_PRODUCTS .' p, '. TABLE_PRODUCTS_DESCRIPTION . ' pd ';
					$query .= 'where p.products_id=pd.products_id and products_status=0 and products_quantity_reel<=0 order by products_model';
					
					$query = tep_db_query($query);
					
					if(tep_db_num_rows($query) > 0) {
						
						echo '<table class="infoBoxContent">';
						
							echo '<tr>';
								echo '<td>R�f�rence</td>';
								echo '<td>Article</td>';
								echo '<td>Date de disponibilit�</td>';
								echo '<td>Action</td>';
							echo '</tr>';
						
							while($data = tep_db_fetch_array($query)) {
								
								echo '<tr>';
									echo '<td><a href="article_edit.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. $data['products_model'] .'</a></td>';
									echo '<td><a href="article_edit.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. bda_product_name_replace('', $data['products_name']).'</a></td>';
									echo '<td>'. tep_date_long($data['products_date_available']) .'</td>';
									
									echo '<td>';
										echo '<a href="article_edit.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. tep_image(DIR_WS_IMAGES . '/icons/editer.png', "Editer la cat�gorie") .'</a>&nbsp;&nbsp;';
										echo '<a href="article_list.php?pID=' . $data['products_id'] .'&action=delete_product' .'" target="blank">'. tep_image(DIR_WS_IMAGES . '/icons/supprimer.png', "Supprimer l\'article") .'</a>';
									echo '</td>';
								echo '</tr>';
							}
						
						echo '</table>';
					}
                    
                    ?>
                    </td>
                </tr>
			</table>
		</td>
	   </tr>
	   <tr>
		<td align="left">
			
		</td>
	  </tr>
	  <tr>
		<td align="left">
			<?
				if(@$_POST['lister_telephones']) {
					
					///////////////////////////////////////////////////////////////////////////////////////
					////////////////////    TELEPHONES SANS IMAGE ///////////////   $tels[]   /////////////
					///////////////////////////////////////////////////////////////////////////////////////		   
					$requete_tel_sans_image = 'SELECT cd.categories_name as cat_nom, cd.categories_id as cat_id, parent_id '
										   .'FROM categories_description cd, categories c '
										   .'WHERE (categories_image2="no_picture_cat.png") '
										   .'AND c.categories_id=cd.categories_id '
										   .'AND sort_order="0" '
										   .'AND parent_id!="0"' 
										   .'ORDER BY cd.categories_name';
					$reponse_tel_sans_image = mysql_query($requete_tel_sans_image);
					$i = 1;
					
					while($fetcht = mysql_fetch_object($reponse_tel_sans_image)) {
						
						$tels[$i]['id'] = $fetcht->cat_id;
						$tels[$i]['nom'] = $fetcht->cat_nom;
						$tels[$i]['par'] = $fetcht->parent_id;
						$i++;
					}
								
					///////////////////////////////////////////////////////////////////////////////////////
					////////////////////    categories  /////////////////////////   $marq[]   /////////////
					///////////////////////////////////////////////////////////////////////////////////////		   
					$requete_liste_fabs = 'SELECT cd.categories_name as tel_nom, cd.categories_id as tel_id '
									   .'FROM categories c, categories_description cd '
									   .'WHERE c.parent_id="0" '
									   .'AND c.categories_id=cd.categories_id '
									   .'ORDER BY cd.categories_name';
					$reponse_liste_fabs = mysql_query($requete_liste_fabs);
						
					while($fetchl = mysql_fetch_object($reponse_liste_fabs)) {
						
						$marq[$fetchl->tel_id]['id']=$fetchl->tel_id;
						$marq[$fetchl->tel_id]['nom']=$fetchl->tel_nom;
					}
					
					echo '<table class="infoBoxContent" width="800" cellspacing="5">';
					
					for($i=1 ; $i<=count($tels) ; $i++) {
						
						echo '<tr><td>';
						echo '&nbsp;&nbsp;'.$marq[$tels[$i]['par']]['nom'].' -> '.$tels[$i]['nom'] . '</td><td>';
						echo '<a href=' . str_replace('_RECHERCHE_', str_replace(' ','%20',$tels[$i]['nom']),$url_google). ' target="_blank">Lien vers Google image pour "' . str_replace('&nbsp; ','%20',$tels[$i]['nom']) . '"</a>';
						echo '</td></tr>';
					}
					
					echo '</table>';
				}
			?>		
		</td>
	  </tr>
	   
	    
	   
	  </table>
	 </td>
	</tr>

      </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
