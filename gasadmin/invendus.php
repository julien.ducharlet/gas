<?php
/* Liste des produits invendus (ou peu vendus) Par Paul*/

require('includes/application_top.php');
require('includes/functions/date.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

if ($_POST['action'] == 'export'){
	if (isset($_POST['export_article'])){
		
		$contenu_fichier = "id;R�f�rence;Nom;Prix d'achat;Prix de vente;Marge;Qt� en stock;Statut\n";
		
		foreach($_POST['export_article'] as $id_article){
			$reponse = mysql_query('SELECT products_model, products_quantity, products_price, products_cost, products_status FROM products WHERE products_id = "' . $id_article . '"');
			$donnees = mysql_fetch_array($reponse);
			
			$reponse = mysql_query('SELECT products_name FROM products_description WHERE products_id = "' . $id_article . '"');
			$result_nom = mysql_fetch_array($reponse);
			$products_name = $result_nom['products_name'];
			
			$suppressions_surplus = array("pour", "#", "*", "[1]");
			$titre_description = str_replace($suppressions_surplus, "", $products_name);
			
			$marge = $currencies->format($donnees['products_price'] - $donnees['products_cost']);
			$prix_achat = $currencies->format($donnees['products_cost']);
			$prix_vente = $currencies->format($donnees['products_price']);
			
			if ($donnees['products_status'] == 0)
				$statut = "Hors Ligne";
			else
				$statut = "En Ligne";
			
			$contenu_fichier .= $id_article . ";" . $donnees['products_model'] . ";" . $titre_description . ";" . $prix_achat . ";" . $prix_vente . ";" . $marge . ";" . $donnees['products_quantity'] . ";" . $statut . "\n";
		}
		
		$fichier = fopen('telechargement/articles_supprimes.csv', 'w');
		
		fputs($fichier, $contenu_fichier);
		
		fclose($fichier);
		?>
		
        <script language="JavaScript"> 
		window.open("telechargement/articles_supprimes.csv","export","");
		</script>
		
        <?php	
	}
}

if ($_GET['action'] == 'deleteconfirm'){
	
	if (isset($_GET['products_id']) && $_GET['products_id'] != '') {
		$product_id = $_GET['products_id'];
		
		tep_remove_product($product_id);
	}
	
	echo '<META HTTP-EQUIV="Refresh" CONTENT="0; URL=' . $PHP_SELF . "?" . substr(tep_get_all_get_params(array('products_id', 'action')), 0, -1) . '">';
}
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
		<SCRIPT type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="includes/general.js"></script>
		<script language="javascript" src="includes/javascript/function.js"></script>
        <script language="javascript" src="includes/menu.js"></script>
    </head>

    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
            <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
            
            <?php
			if (isset($_GET['tri']) && $_GET['tri'] != '')
				$tri = 'order by products_quantity ' . $_GET['tri'] . ' , products_model';
			else
				$tri = 'order by products_model';
			
			$marge_orange = 10;
			$marge_rouge = 5;
			
			$ventes = $_GET['ventes'];
			if (!isset($ventes) || $ventes == '')
				$ventes = '0';
			
			if(empty($HTTP_GET_VARS["date_deb"])){
				$date_deb=date("d-m-Y");
			}
			if(empty($HTTP_GET_VARS["date_fin"])){
				$date_fin=date("d-m-Y");
			}
			
            $list_ventes[] = array('id' => '=', 'text' => '=');
			$list_ventes[] = array('id' => '<', 'text' => '<');
			$list_ventes[] = array('id' => '>', 'text' => '>');
            
            // Construction de la liste d�roulante des familles d'articles
            $reponse = mysql_query('SELECT family_id, family_name FROM family order by family_name'); // On interroge la base pour avoir les familles
            
			// On rempli un tableau avec les familles
            while ($donnees = mysql_fetch_array($reponse)){
                $list_family_array[] = array('id' => $donnees['family_id'], 'text' => ucwords(strtolower($donnees['family_name'])));
            }
			
			// Construction de la liste d�roulante des marques
            $reponse = mysql_query('SELECT manufacturers_id, manufacturers_name FROM manufacturers order by manufacturers_name');
            
            // On rempli un tableau avec les marques
            while ($donnees = mysql_fetch_array($reponse)){
                $list_manufacturers_array[] = array('id' => $donnees['manufacturers_id'], 'text' => $donnees['manufacturers_name']);
            }
			?>
            
            <div width="100%" style="padding: 4px;">
                <table border="0" width="100%" height="44" style="background-color: #e1e1f5;" class="smallText">
                    <tr>
                        <!-- Formulaire des filtres -->
                        <form id='Filtres' name='Filtres' method="get" action="<?php echo $PHP_SELF;?>" >
                            <td width="5%">&nbsp;</td>
							<td width="20%">     
							<?php	echo "Du : ".tep_draw_input_field('date_deb', $date_deb, 'size="10" id="focusorders"');?>
                            		<a href="#" onClick="displayCalendar(Filtres.date_deb,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
							<?php 	echo "Au&nbsp;".tep_draw_input_field('date_fin', $date_fin, 'size="10" id="focusorders"');?>
                            		<a href="#" onClick="displayCalendar(Filtres.date_fin,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
                            </td>
                            <td width="20%">
								<?php
									echo "Nombre de ventes : ";
									echo tep_draw_pull_down_menu('type_vente', $list_ventes, '', 'onChange="this.form.submit();"')."&nbsp;&nbsp;";
                                	echo tep_draw_input_field('ventes', $ventes, 'SIZE=2');
									
                                    /*$new_array[] = array('id' => "0", 'text' => "Sans ventes");
                                    $new_array[] = array('id' => "1", 'text' => "Avec peu de ventes");
                                    echo tep_draw_pull_down_menu('ventes', $new_array, '', 'onChange="this.form.submit();"');*/
								?>
                            </td>
							<td width="25%">
                                <?php
								echo "Famille article : ";
                                echo tep_draw_pull_down_menu('family', array_merge(array(array('id' => '0', 'text' => 'Aucune famille'), array('id' => '-1', 'text' => 'Toute famille')), $list_family_array), '', 'onChange="this.form.submit();"');
                                ?>
                            </td>
                            <td width="25%">
                                <?php
								echo "Marque : ";
                                echo tep_draw_pull_down_menu('manufacturers', array_merge(array(array('id' => '0', 'text' => 'Aucune marque'), array('id' => '-1', 'text' => 'Toute marque')), $list_manufacturers_array), '-1', 'onChange="this.form.submit();"');
                                ?>
                            </td>
                            <td width="5%"><input type=submit value="Go"></td>
                        </form>
                    </tr>
                </table>
                
				<br />
                
                <?php //Inverse le tri � chaque fois qu'on clique
					if ($_GET['tri'] == 'DESC')
						$href = $PHP_SELF . "?" . tep_get_all_get_params(array('tri')) . "tri=ASC";
					else
						$href = $PHP_SELF . "?" . tep_get_all_get_params(array('tri')) . "tri=DESC";
                ?>
                
                <form method="post" action='<?php echo $PHP_SELF . "?" . tep_get_all_get_params(array('action')); ?>'>
                
                <table border="0" align="center" width="100%" cellspacing="0">
                    <tr class="dataTableHeadingRow">
                    	<th class="dataTableHeadingContent" align="center"><input type="checkbox" value="Check All" onClick="this.value=checkAll(this.form.list,this.checked)"></th>
                        <th class="dataTableHeadingContent">ID</th>
                        <th class="dataTableHeadingContent">R�f�rence</th>
                        <th class="dataTableHeadingContent">Nom</th>
                        <th class="dataTableHeadingContent">Prix d'achat HT</th>
                        <th class="dataTableHeadingContent">Prix de vente HT</th>
                        <th class="dataTableHeadingContent">Marge</th>
                        <th class="dataTableHeadingContent">Ventes</th>
                        <th class="dataTableHeadingContent"><a href="<?php echo $href; ?>"><b>Qt� en stock</b></a></th>
                        <th class="dataTableHeadingContent">Statut</th>
                        <th class="dataTableHeadingContent">Date d'ajout</th>
                        <th class="dataTableHeadingContent">Actions</th>
                    </tr>
                    <?php
                        if (!isset($_GET['manufacturers']) || $_GET['manufacturers'] == '-1'){
							$rqt_marque = '';
						}
						else {
							$rqt_marque =  "and manufacturers_id = '" . $_GET['manufacturers'] . "'";
						}
						
						if ($_GET['family'] == '-1'){
							$rqt_famille = '';
						}
						else {
							$rqt_famille =  "and family_id = '" . $_GET['family'] . "'";
						}
						
						$date_deb = tep_db_prepare_input(date_fr_to_bdd($date_deb));
      					$date_fin = tep_db_prepare_input(date_fr_to_bdd($date_fin));
      						
						$query_rech="select products_id, family_id, products_date_added, products_price, products_cost, products_quantity, products_model, products_status, manufacturers_id
												 from products
												 where products_date_added <= '" . $date_deb . "' " . $rqt_famille . " " . $rqt_marque . "
												 " . $tri;
                        $resultat = mysql_query($query_rech);
                        //echo $query_rech;
                        $i=0;
                        $i_vendus=0;
                        while ($donnees = mysql_fetch_array($resultat))
                        {
                            $id_produit = $donnees['products_id'];
							
                            $date_ajout = $donnees['products_date_added'];
                            $timestamp_ajout = strtotime($date_ajout);
                            
                            $requete = "select sum(products_quantity) as total from orders_products P, orders O where P.orders_id=O.orders_id and products_id='" . $id_produit . "' and date_purchased >= '" . $date_deb . "' and date_purchased <= '" . $date_fin . "'";
                            $order_query = mysql_query($requete);	 
                            $total = mysql_fetch_array($order_query);
							$prod_vendus=$total['total'];
							if(empty($prod_vendus)){
								$prod_vendus=0;
							}
							
							if(($_REQUEST['type_vente']=="<" && $prod_vendus<$ventes) 
							|| ($_REQUEST['type_vente']=="=" && $prod_vendus==$ventes)
							|| ($_REQUEST['type_vente']==">" && $prod_vendus>$ventes)){
								
                                $total_prod_vendus+=$prod_vendus;
								$i++;
								if($prod_vendus>0){
									$i_vendus++;
								}
								$i % 2 ? $class = RCS_REPORT_EVEN_STYLE : $class = RCS_REPORT_ODD_STYLE;
								
								$prix_produit = $donnees['products_price'];
								$cout_produit = $donnees['products_cost'];
								$marge = $prix_produit - $cout_produit;
								$marge_couleur = '#000';
								
								if ($marge < $marge_orange)
									$marge_couleur = '#F60';
								if ($marge < $marge_rouge)
									$marge_couleur = '#F00';
								
								$qte_stock = $donnees['products_quantity'];
								$ref = $donnees['products_model'];
								$date_ajout_fr = date("d-m-Y", $timestamp_ajout);
								
								if ($donnees['products_status'] == 1){
									$image = tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10);
								}
								else{
									$image = tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN, 10, 10);
								}
								
								
								$reponse = mysql_query('SELECT products_name FROM products_description WHERE products_id = "' . $id_produit . '"');
								$result_nom = mysql_fetch_array($reponse);
								$products_name = $result_nom['products_name'];
								
								$suppressions_surplus = array("pour", "#", "*", "[1]");
								$titre_description = str_replace($suppressions_surplus, "", $products_name);
								
                                ?>
                                <tr class="<?php echo $class; ?>" style="text-align: center;">
                                	<td class="dataTableHeadingContent" align="center"><?php echo"<input id='list' type='checkbox' name='export_article[]' value='" . $id_produit . "' />"; ?></td>
                                    <td class="datatablecontent"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_EDIT, 'pID=' . $id_produit . '&action=new_product') . '" target="_blank" >' . $id_produit . '</a>' ?></td>
                                    <td class="datatablecontent"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_EDIT, 'pID=' . $id_produit . '&action=new_product') . '" target="_blank" >' . $ref . '</a>'; ?>&nbsp;</td>
                                    <td class="datatablecontent"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_EDIT, 'pID=' . $id_produit . '&action=new_product') . '" target="_blank" >' . $titre_description . '</a>'; ?>&nbsp;</td>
                                    <td class="datatablecontent"><?php echo $currencies->format($cout_produit); ?></td>
                                    <td class="datatablecontent"><?php echo $currencies->format($prix_produit); ?></td>
                                    <td class="datatablecontent" style="color: <?php echo $marge_couleur; ?>;"><b><?php echo $currencies->format($marge); ?></b></td>
                                    <td class="datatablecontent"><?php echo $prod_vendus; ?></td>
                                    <td class="datatablecontent"><?php echo $qte_stock; ?></td>
                                    <td class="datatablecontent"><?php echo $image; ?></td>
                                    <td class="datatablecontent" style="text-align: center;"><?php echo $date_ajout_fr; ?></td>
                                    <td class="datatablecontent" style="text-align: center;">
										<?php echo '
										<a href="' . tep_href_link(FILENAME_ARTICLE_EDIT, 'pID=' . $id_produit . '&action=new_product') . '" target="_blank" >' . tep_image(DIR_WS_IMAGES . 'icons/editer.png', 'Editer le produit') . '</a>
										<a href="' . $PHP_SELF . '?' . tep_get_all_get_params() . 'products_id=' . $id_produit . '&action=deleteconfirm' . '" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer  ' . $titre_description . '  DEFINITIVEMENT ?\')){return true;}else {return false;}">' . tep_image_button('supprimer.png', IMAGE_DELETE) . '</a>'
										; ?>
                                    </td>
                                </tr>
                                
                                <?php
							  
                            }
						  
						  
                        }
                    ?>
                    <tr>Total : <b><?php echo $i; ?></b> r�f�rences list�s dont <?php echo $i_vendus; ?></b> r�f�rences vendues&nbsp; pour un total de <?php echo $total_prod_vendus; ?> ventes</tr>
                </table>
                
                
                <br />
                <button onClick="this.form.submit();">Exporter les lignes coch�es dans un fichier .csv</button>
                <input type="hidden" name="action" value="export">
                
                </form>
            </div>
           
		<?php require(DIR_WS_INCLUDES . 'footer.php'); ?><br /> 
	</body>
</html>

<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>