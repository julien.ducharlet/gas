<?php
/* 2009/01/13 LL last modif by Ludo => refonte complete de la page order, pour la couper en 2*/
  require('includes/application_top.php');
  require('includes/functions/date.php');
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();
  
  

  include(DIR_WS_CLASSES . 'order.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/function.js"></script>
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	
	$(".send_data").click(function() {
		
		$(".loading form").hide();
		$(".loading").slideDown();
		
		var params = '';
		if($("#date_deb").val()!='' && $("#date_fin").val()!='') params = "&date_deb="+ $("#date_deb").val() +"&date_fin="+ $("#date_fin").val();
		
		$.ajax({
		   type: "GET",
		   url: "includes/javascript/ajax/liste_cmd_facture.php?type_client="+ $("#type_client").val() + params,
		   dataType: "text",
		   processData: false,
		   success: function(list_cmd) {
			   
				//$(".loading form").remove();
				$(".loading #id_com").val(list_cmd);
				$(".loading form input:submit").val("Cliquez ici pour exporter les "+ (list_cmd.split(' ').length-1) +" factures");
				$(".loading form").show();
				//close_lightbox("lightbox", redirect); 
		   }
		   
		});
	});
	
	
	$('.loading').ajaxError(function(e, xhr, settings, exception) {
		
		$(".loading form").hide();
		alert("Erreur lors de l'appel � la page (surement trop de commandes)");
	});
});

</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<div id="popupcalendar" class="text"></div>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr> 
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<?php 
            echo tep_draw_form('Filtres', 'export_facture_compta.php', '', 'get'); 
            
            ?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #e1e1f5;">
		<tr>
        	<td align="center">
            	<table width="90%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
                    
                        <td align="center" class="smallText">
                        <?php	$type_client_array = array();
                                $type_query = tep_db_query("select type_id, type_name from " . TABLE_CUSTOMERS_TYPE . " order by type_id  ");
                                $type_client_array[] = array('id' => "", 'text' => "Tous");
                                while ($type_values = tep_db_fetch_array($type_query)) {
                                  $type_client_array[] = array('id' => $type_values['type_id'], 'text' => $type_values['type_name']);
                                }
                                echo "&nbsp;&nbsp;&nbsp;&nbsp; Type de client :&nbsp;".tep_draw_pull_down_menu('type_client', $type_client_array, '', 'id="type_client"');?>
                        </td>
                        <td align="center" class="smallText" height="40">        
                        <?php	echo "Du : ".tep_draw_input_field('date_deb', '', 'size="10" id="date_deb"');?>
                                <a href="#" onClick="displayCalendar(Filtres.date_deb,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
                                <?php echo "Au&nbsp;".tep_draw_input_field('date_fin', '', 'size="10" id="date_fin"');?>
                                <a href="#" onClick="displayCalendar(Filtres.date_fin,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
                        </td>
                    	<td align="center" class="smallText"><input type="button" class="send_data" value="Go"></td>
                    </tr> 
                </table>
        	</td>
        </tr>
        </table>
        </form>
        </td>
	  </tr>
	  <tr>
		<td>
          <div class="loading" style="width:100%; background-color:#D8BFD8; text-align:center; display:none;"><form action="<?php echo WEBSITE . BASE_DIR ?>/compte/generation_pdf/facture_list.php" method="post" target="_blank"><input type="hidden" name="id_com" id="id_com" value=""><input type="submit" value=""></form></div>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?><br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>