<?php
  header('Content-type: text/html; charset=ISO-8859-1');
  require('includes/application_top.php');
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();
  
  function tep_db_update_totals($order_id) {
	  $currencies = new currencies();
	  $currency_query = tep_db_query("select currency, currency_value from " . TABLE_ORDERS . " where orders_id = '" . $order_id . "'");
	  $currency = tep_db_fetch_array($currency_query);
	  
	  //we have to update the orders_total table
	  $products_total_query = tep_db_query("select final_price, products_quantity, products_tax from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . $order_id . "'");
	  $price = 0;
	  $total = 0;
	  $taxes = array();
	  while ($products_total = tep_db_fetch_array($products_total_query)) {
		  $iva = round((100 + (float)$products_total['products_tax']) / 100, 4);
		  $price += (float)round(((float)$products_total['final_price'] * (int)$products_total['products_quantity']) * $iva, 2);
		  //fill the array of taxes to know which tax is used.
		  if (tep_not_null($products_total['products_tax'])) {
			  $tax_description_query = tep_db_query("select tax_description from " . TABLE_TAX_RATES . " where tax_rate = '" . $products_total['products_tax'] . "'");
			  $tax_description = tep_db_fetch_array($tax_description_query);
			  
			  if (sizeof($taxes)) {
				  $ya_esta = false;
				  for ($i=0; $i<sizeof($taxes); $i++) {
					  if (in_array($tax_description['tax_description'] . ':', $taxes[$i])) {
						  $ya_esta = $i;
					  }
				  }
				  if ($ya_esta === false) {
					  $taxes[] = array('description' => $tax_description['tax_description'] . ':', 'value' => round(((((float)$products_total['final_price'] * (int)$products_total['products_quantity']) * (float)$products_total['products_tax']) / 100), 4));
				  } else {
					  $taxes[$ya_esta]['value'] += round(((((float)$products_total['final_price'] * (int)$products_total['products_quantity']) * (float)$products_total['products_tax']) / 100), 4);
				  }
			  } 
			  else {
				  $taxes[] = array('description' => $tax_description['tax_description'] . ':', 'value' => round(((((float)$products_total['final_price'] * (int)$products_total['products_quantity']) * (float)$products_total['products_tax']) / 100), 4));
			  }
		  }
	  }
  }
  
  $action = $HTTP_GET_VARS['action'];
  if (($action == 'eliminate') || ($action == 'update_product')) {//eliminate or modify product
	  if ($action == 'eliminate') {
		  
		  $query_products_quantity = 'select products_quantity from '. TABLE_ORDERS_PRODUCTS .' where orders_products_id='. (int)$HTTP_GET_VARS['pID'];
		  $query_products_quantity = tep_db_query($query_products_quantity);
		  
		  $products_quantity = tep_db_fetch_array($query_products_quantity);
		  
		  tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS . " where orders_products_id = '" . $HTTP_GET_VARS['pID'] . "' limit 1");
		  $attributes_query = tep_db_query("select orders_products_attributes_id from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_products_id = '" . (int)$HTTP_GET_VARS['pID'] . "'");
		  
		  // GESTION DES STOCK LORS DE L'AJOUT D'UN ARTICLE
	      tep_db_query("update " . TABLE_PRODUCTS . " set
		  					products_quantity = products_quantity+'". $products_quantity['products_quantity'] ."',
							products_ordered = products_ordered-'". $products_quantity['products_quantity'] ."'
						where products_id = '" . $products_id . "'");
	  	  // FIN GESTION DES STOCK LORS DE L'AJOUT D'UN ARTICLE
		  
		  if (tep_db_num_rows($attributes_query)) {//if the products has attributes, eliminate them
			  while ($attributes = tep_db_fetch_array($attributes_query)) {
				  tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_products_attributes_id = '" . $attributes['orders_products_attributes_id'] . "' limit 1");
				  $update_option = 'update '. TABLE_PRODUCTS_ATTRIBUTES .' set
											options_quantity=options_quantity+'. $products_quantity['products_quantity'] .'
									  where products_id='. $data_products['products_id'] .'
									  and options_values_id='. $data_options['options_values_id'];
			  }
		  }
	  } 
	  else {
		  //get the price to change order totals
		  //but first, change it if we change price of attributes (so we get directly the good final_price)
		  $field = $HTTP_GET_VARS['field'];
		  if ($field == 'options') {
			  tep_db_query("update " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " set products_options_values = '" . $HTTP_GET_VARS['new_value'] . "',  options_values_price = '" . round((float)$HTTP_GET_VARS['option_price'], 4) . "' where orders_products_id = '" . $HTTP_GET_VARS['pID'] . "' and products_options = '" . $HTTP_GET_VARS['extra'] . "'");
			  tep_db_query("update " . TABLE_ORDERS_PRODUCTS . " set final_price = (products_price + '" . round((float)$HTTP_GET_VARS['option_price'], 4) . "') where orders_products_id = '" . $HTTP_GET_VARS['pID'] . "'");
		  } 
		  elseif (stristr($field, 'price')) {
			  $adapt_price_query = tep_db_query("select options_values_price from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_products_id = '" . $HTTP_GET_VARS['pID'] . "' and options_values_price != 0");
			  if (tep_db_num_rows($adapt_price_query)) {
				  $adapt_price = tep_db_fetch_array($adapt_price_query);
				  $option_price = (float)$adapt_price['options_values_price'];
			  } 
			  else $option_price = 0;
			  
			  if (stristr($field, '_excl')) {
				  $new_price = round((float)$HTTP_GET_VARS['new_value'], 4);
			  } 
			  else {
				  $tax_query = tep_db_query("select products_tax from " . TABLE_ORDERS_PRODUCTS . " where orders_products_id = '" . $HTTP_GET_VARS['pID'] . "' and products_tax != 0");
				  if (tep_db_num_rows($tax_query)) {
					  $tax_ = tep_db_fetch_array($tax_query);
					  $percent = (float)$tax_['products_tax'];
					  $percent = round(($percent/100), 4);
					  $percent = $percent + 1;
					  $new_price = round(round((float)$HTTP_GET_VARS['new_value']/$percent, 4), 4);
				  } 
				  else $new_price = round((float)$HTTP_GET_VARS['new_value'], 4);
			  }
			  tep_db_query("update " . TABLE_ORDERS_PRODUCTS . " set final_price = '" . $new_price . "', products_price = '" . ($new_price - $option_price) . "' where orders_products_id = '" . $HTTP_GET_VARS['pID'] . "'");
		  } 
		  else {
			  tep_db_query("update " . TABLE_ORDERS_PRODUCTS . " set " . $field . " = '" . tep_db_prepare_input($HTTP_GET_VARS['new_value']) . "' where orders_products_id = '" . $HTTP_GET_VARS['pID'] . "'");
		  }
	  }
	  //we have to update the orders_total table
	  tep_db_update_totals($HTTP_GET_VARS['order']);
	  echo (($action == 'eliminate') ? 'Article Efface.' : 'Commande actualisee.' ) . "\n" . 'Rafraichir le navigateur pour voir les changements.';
  } 
  elseif ($action == 'update_order_field') {
	  tep_db_query("update " . $HTTP_GET_VARS['db_table'] . " set " . $HTTP_GET_VARS['field'] . " = '" . tep_db_prepare_input(addslashes($HTTP_GET_VARS['new_value'])) . "' where orders_id = '" . $HTTP_GET_VARS['oID'] . "'");
	  echo 'Information actualisee.' . "\n" . 'Rafraichir le navigateur pour voir les modifications.';
  } 
  elseif ($action == 'search') {//search products in the db.
	  $products_query = tep_db_query("select distinct p.products_id, pd.products_name, p.products_model from " . TABLE_PRODUCTS_DESCRIPTION . " pd left join " . TABLE_PRODUCTS . " p on (p.products_id = pd.products_id) where (pd.products_name like '%" . tep_db_input($HTTP_GET_VARS['keyword']) . "%' or  p.products_model like '%" . tep_db_input($HTTP_GET_VARS['keyword']) . "%') and  pd.language_id = '" . $languages_id . "' and p.products_status = '1' order  by pd.products_name asc limit 20");
	  if (tep_db_num_rows($products_query)) {
		  while ($products = tep_db_fetch_array($products_query)) {
			  $results[] = '<a href="javascript:selectProduct(\'' .  $products['products_id'] . '\', \'' .  addslashes(tep_output_string_protected($products['products_name'])) . '\');">' .  $products['products_name'] . (($products['products_model'] != '') ? ' (' . $products['products_model'] . ')' : '') . '</a>' . "\n";
		  }
	  } 
	  else {
		  $results[] = PRODUCTS_SEARCH_NO_RESULTS;
	  }
	  echo implode('<br />' . "\n", $results);
  } 
  elseif ($action == 'attributes') {
	  //we create an AJAX form
	  $attributes = '<form name="attributes" id="attributes" action="" onsubmit="setAttr(); return false"><input type="hidden" name="products_id" value="' . (int)$HTTP_GET_VARS['prID'] . '">';
	  //this part comes integraly from OSC catalog/product_info.php
	  $products_attributes_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int)$HTTP_GET_VARS['prID'] . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int)$languages_id . "'");
	  $products_attributes = tep_db_fetch_array($products_attributes_query);
	  if ($products_attributes['total'] > 0) {
		  $attributes .= '<table border="0" cellspacing="0" cellpadding="2" class="dataTableRow" width="100%"><tr><td class="dataTableContent" colspan="2">' . TEXT_PRODUCT_OPTIONS . '</td>            </tr>';
		  $products_options_name_query = tep_db_query("select distinct popt.products_options_id, popt.products_options_name from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int)$HTTP_GET_VARS['prID'] . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int)$languages_id . "' order by popt.products_options_name");
		  while ($products_options_name = tep_db_fetch_array($products_options_name_query)) {
			  $products_options_array = array();
	          $products_options_query = tep_db_query("select pov.products_options_values_id, pov.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov where pa.products_id = '" . (int)$HTTP_GET_VARS['prID'] . "' and pa.options_id = '" . (int)$products_options_name['products_options_id'] . "' and pa.options_values_id = pov.products_options_values_id and pov.language_id = '" . (int)$languages_id . "'");
	          while ($products_options = tep_db_fetch_array($products_options_query)) {
		          $products_options_array[] = array('id' => $products_options['products_options_values_id'], 'text' => $products_options['products_options_values_name']);
		          if ($products_options['options_values_price'] != '0') {
			          $products_options_array[sizeof($products_options_array)-1]['text'] .= ' (' . $products_options['price_prefix'] . $currencies->display_price($products_options['options_values_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) .') ';
		          }
	          }
	          $attributes .= '<tr><td class="main">' . $products_options_name['products_options_name'] . ':</td><td class="main">' . tep_draw_pull_down_menu('atrid_' . $products_options_name['products_options_id'], $products_options_array, $selected_attribute) . '</td></tr>';
          }
          $attributes .= '<tr><td colspan="2">' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '</td></tr></table></form>';
      } 
	  else {
	      $attributes .= tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '</form>';
      }
      echo $attributes;
  } 
  elseif ($action == 'set_attributes') {
	  $attributes = array();
	  $products_id = 0;
	  $products_quantity = 0;
	  foreach($HTTP_POST_VARS as $key => $value) {
		  if ($key == 'products_id') {
			  $products_id = $value;
		  } 
		  elseif ($key == 'products_quantity') {
			  $products_quantity = $value;
		  } 
		  elseif (stristr($key, 'trid_')) {
			  $attributes[] = array(substr($key, 6), $value);
		  }
	  }
	  $orders_id = $HTTP_GET_VARS['oID'];
	  $product_info_query = tep_db_query("select p.products_model, pd.products_name, products_weight, p.products_price, p.products_tax_class_id from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.products_id = pd.products_id where p.products_id = '" . $products_id . "' and pd.language_id = '" . (int)$languages_id . "'");
	  $product_info = tep_db_fetch_array($product_info_query);
	  if (DISPLAY_PRICE_WITH_TAX == 'true') {
		  $tax_query = tep_db_query("select tax_rate, tax_description from " . TABLE_TAX_RATES . " where tax_rates_id = '" . $product_info['products_tax_class_id'] . "'");
		  $tax_ = tep_db_fetch_array($tax_query);
		  $tax = $tax_['tax_rate'];
		  $tax_desc = $tax_['tax_description'];
	  } 
	  else {
		  $tax = 0;
	  }
	  //OJO, hay que insertar primero el product para saber el orders_products_id...
	  $attribute_price_sum = 0;
	  $attribute_update = false;
	  if (sizeof($attributes) > 0) {
		  $attribute_update = true;
		  for ($j=0; $j<sizeof($attributes); $j++) {
			  $attribute_price_query = tep_db_query("select options_values_price, price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . $products_id . "' and options_id = '" . $attributes[$j][0] . "' and options_values_id = '" . $attributes[$j][1] . "'");
			  $attribute_price = tep_db_fetch_array($attribute_price_query);
			  if ($attribute_price['price_prefix'] == '+') {
				  $attribute_price_sum += (float)$attribute_price['options_values_price'];
			  } 
			  else {
				  $attribute_price_sum -= (float)$attribute_price['options_values_price'];
			  }
			  $attribute_name_query = tep_db_query("select products_options_name from " . TABLE_PRODUCTS_OPTIONS . " where products_options_id = '" . $attributes[$j][0] . "' and language_id = '" . (int)$languages_id . "'");
			  $attribute_name = tep_db_fetch_array($attribute_name_query);
			  $options_name_query = tep_db_query("select products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " where products_options_values_id = '" . $attributes[$j][1] . "' and language_id = '" . (int)$languages_id . "'");
			  $options_name = tep_db_fetch_array($options_name_query);
			  
			  tep_db_query("insert into " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " (orders_id, orders_products_id, products_options, products_options_values, options_values_price, price_prefix) values ('" . $orders_id . "', '0', '" . tep_db_input((string)$attribute_name['products_options_name']) . "', '" . tep_db_input((string)$options_name['products_options_values_name']) . "', '" . tep_db_input((float)$attribute_price['options_values_price']) . "', '" . tep_db_input((string)$attribute_price['price_prefix']) . "')");
		  }
	  }
	  $final_price = (float)$product_info['products_price'] + (float)$attribute_price_sum;
	  if (tep_not_null($tax)) {
		  $final_price = $final_price * (float)('1.' . (strlen((string)$tax['tax_rate'] < 6) ? '0' : '') . ((int)($tax['tax_rate'] * 100)));
	  }
	  tep_db_query("insert into " . TABLE_ORDERS_PRODUCTS . " (orders_id, products_id, products_model, products_name, products_weight, products_price, final_price, products_tax, products_quantity) values ('" . $orders_id . "', '" . $products_id . "', '" . $product_info['products_model'] . "', '" . $product_info['products_name'] . "', '". $product_info['products_weight'] ."', '" . $product_info['products_price'] . "', '" . $final_price . "', '" . $tax . "', '" . $products_quantity . "')");
	  
	  // GESTION DES STOCK LORS DE L'AJOUT D'UN ARTICLE
	  tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity - '" . $products_quantity . "' where products_id = '" . $products_id . "'");
	  // FIN GESTION DES STOCK LORS DE L'AJOUT D'UN ARTICLE
	   
	  $orders_products_id = tep_db_insert_id();
	  if ($attribute_update == true){
		  tep_db_query("update  " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " set orders_products_id = '" . $orders_products_id . "' where orders_products_id = '0'");
	  }
	  tep_db_update_totals($orders_id);
	  echo 'Article ajoute.' . "\n" . 'Rafraichir le navigateur pour voir les changements.';
  }  
  ?>