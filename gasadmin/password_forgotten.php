<?php
/*
  $Id: login.php,v 1.17 2003/02/14 12:57:29 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  //require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGIN);
  
  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'process')) {
    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email_address']);
    $firstname = tep_db_prepare_input($HTTP_POST_VARS['firstname']);
    $log_times = $HTTP_POST_VARS['log_times']+1;
    if ($log_times >= 4) {
      tep_session_register('password_forgotten');
    }
      
// Check if email exists
    $check_admin_query = tep_db_query("select admin_id as check_id, admin_firstname as check_firstname, admin_lastname as check_lastname, admin_email_address as check_email_address from " . TABLE_ADMIN . " where admin_email_address = '" . tep_db_input($email_address) . "'");
    if (!tep_db_num_rows($check_admin_query)) {
      $HTTP_GET_VARS['login'] = 'fail';
    } else {
      $check_admin = tep_db_fetch_array($check_admin_query);
      if ($check_admin['check_firstname'] != $firstname) {
        $HTTP_GET_VARS['login'] = 'fail';
      } else {
        $HTTP_GET_VARS['login'] = 'success';
        
        function randomize() {
          $salt = "ABCDEFGHIJKLMNOPQRSTUVWXWZabchefghjkmnpqrstuvwxyz0123456789";
          srand((double)microtime()*1000000); 
          $i = 0;
    
          while ($i <= 7) {
            $num = rand() % 33;
    	    $tmp = substr($salt, $num, 1);
    	    $pass = $pass . $tmp;
    	    $i++;
  	  }
  	  return $pass;
        }
        $makePassword = randomize();
      
        tep_mail($check_admin['check_firstname'] . ' ' . $check_admin['admin_lastname'], $check_admin['check_email_address'], ADMIN_EMAIL_SUBJECT, sprintf(ADMIN_EMAIL_TEXT, $check_admin['check_firstname'], HTTP_SERVER . DIR_WS_ADMIN, $check_admin['check_email_address'], $makePassword, STORE_NAME), STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);            
        tep_db_query("update " . TABLE_ADMIN . " set admin_password = '" . tep_encrypt_password($makePassword) . "' where admin_id = '" . $check_admin['check_id'] . "'");
      }
    }
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<style type="text/css"><!--
a { color:#080381; text-decoration:none; }
a:hover { color:#aabbdd; text-decoration:underline; }
a.text:link, a.text:visited { color: #ffffff; text-decoration: none; }
a:text:hover { color: #000000; text-decoration: underline; }
a.sub:link, a.sub:visited { color: #dddddd; text-decoration: none; }
A.sub:hover { color: #dddddd; text-decoration: underline; }
.sub { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; line-height: 1.5; color: #dddddd; }
.text { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #000000; }
.smallText { font-family: Verdana, Arial, sans-serif; font-size: 10px; }
.login_heading { font-family: Verdana, Arial, sans-serif; font-size: 12px; color: #ffffff;}
.login { font-family: Verdana, Arial, sans-serif; font-size: 12px; color: #000000;}
//--></style>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

<table border="0" width="600" height="100%" cellspacing="0" cellpadding="0" align="center" valign="middle">
  <tr>
    <td>
	
	<table border="0" width="600" height="440" cellspacing="0" cellpadding="1" align="center" valign="middle">
      <tr bgcolor="#000000">
        
		<td>
		
	<table border="0" width="600" height="440" cellspacing="0" cellpadding="0">
		<tr bgcolor="#ffffff" height="50">
			<td width="600" height="110" align="center" bgcolor="#ffffff">
				<img src="images/boite_boutique.gif" width="180" height="110" border="0">
			</td>
		</tr>
		<tr>
			<td height="1"></td>
		</tr>
		  
		<tr bgcolor="#080381">
			<td align="center" valign="middle" bgcolor="#FFFFFF" colspan="2">
			<?php echo tep_draw_form('login', FILENAME_PASSWORD_FORGOTTEN, 'action=process'); ?>
				<table width="330" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td align="center" valign="top" class="login">&nbsp;<b>Mot de passe oubli&eacute; ?</b><br \><br \>
					</tr>
					<tr>
						<td height="100%" width="100%" valign="top" align="center">
							<table border="0" height="100%" width="100%" cellspacing="0" cellpadding="1" bgcolor="#666666">
								<tr>
									<td>
										<table border="0" width="100%" height="100%" cellspacing="3" cellpadding="2" bgcolor="#CCCCCC">

<?php
  if ($HTTP_GET_VARS['login'] == 'success') {
    $success_message = 'Le nouveau mot de passe va &ecirc;tre envoy&eacute; &agrave; votre adresse email. Veuillez v&eacute;rifier votre email et essayer de nouveau une ouverture.<br />&nbsp;<br />&nbsp;';
  } elseif ($HTTP_GET_VARS['login'] == 'fail') {
    $info_message = '<font color="#ff0000"><b>ERREUR :</b></font> Probl&egrave;me de pr&eacute;nom ou d\'adresse email !';
  }
  if (tep_session_is_registered('password_forgotten')) {
?>
											<tr>
												<td class="smallText">Vous avez que 3 essais pour des raisons de s&eacute;curit&eacute;, contactez SVP votre administrateur pour obtenir un nouveau mot de passe.<br />&nbsp;<br />&nbsp;</td>
											</tr>
											<tr>
												<td align="center" valign="top"><?php echo '<a href="' . tep_href_link(FILENAME_LOGIN, '' , 'SSL') . '">' . tep_image_button('button_back.gif', 'Revenir') . '</a>'; ?></td>
											</tr>
<?php
  } elseif (isset($success_message)) {
?>
											<tr>
												<td class="smallText"><?php echo $success_message; ?></td>
											</tr>
											<tr>
												<td align="center" valign="top"><?php echo '<a href="' . tep_href_link(FILENAME_LOGIN, '' , 'SSL') . '">' . tep_image_button('button_back.gif', 'Revenir') . '</a>'; ?></td>
											</tr>
<?php
  } else {
    if (isset($info_message)) {
?>
                                    <tr>
                                      <td colspan="2" class="smallText" align="center"><?php echo $info_message; ?><?php echo tep_draw_hidden_field('log_times', $log_times); ?></td>
                                    </tr>
<?php
    } else {
?>
                                    <tr>
                                      <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?><?php echo tep_draw_hidden_field('log_times', '0'); ?></td>
                                    </tr>
<?php
    }
?>                                    
                                    <tr>
                                      <td class="login">Pr�nom :</td>
                                      <td class="login"><?php echo tep_draw_input_field('firstname'); ?></td>
                                    </tr>
                                    <tr>
                                      <td class="login">Adresse Email :</td>
                                      <td class="login"><?php echo tep_draw_input_field('email_address'); ?></td>
                                    </tr>
                                    <tr>
                                      <td colspan="2" align="center" valign="top"><?php echo tep_image_submit('button_confirm.gif', 'Confirmer la demande'); ?>&nbsp;</td>
                                    </tr>
<?php
  }
?>   
                                  </table></td></tr>
                                </table>
                                </td>
                              </tr>
                            </table>
                          </form>

            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php require(DIR_WS_INCLUDES . 'footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>

</body>

</html>