<?php
/*  Cette page met à jour le stock virtuel de tout les articles en fonction du stock réel
	La requête met à jour en fonction des commandes en statut :
	1 (Traitement en cours), 6 (En attente du Chèque), 7 (En attente du Virement), 27 (En attente CB), 28 (En attente Mandat Administratif) et 29 (En attente de Mandat Cash)*/

require('includes/application_top.php');
require('../gas/includes/fonctions/mail.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>

<script language="javascript">
function verif() {
	if (window.confirm("Voulez-vous vraiment mettre \340 jour le stock ?\n\nAttention INTERDICTION FORMELLE de s'en servir sans l'aval de l'administrateur du site !")){
		document.location.href="<?php echo $_SERVER['PHP_SELF']; ?>?lancer=true"
		return true;
	}
}
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<div style="text-align: center;">

    
    <?php if ($_GET['effectue']) { ?>
    <div style="background-color: #CFC; border: 1px solid #090; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Base de donn&eacute;es mise &agrave; jour avec <strong>succ&egrave;s</strong> !
    </div>
    <?php } else { ?>
    <div style="background-color: #FFC; border: 1px solid #FC0; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Cette page ajuste le stock virtuel de tous les articles dans la base de donn&eacute;es en fonction du stock r&eacute;el.<br />La requ&ecirc;te met &agrave; jour en fonction des commandes en statut 1 (Traitement en cours), 6 (En attente du Ch&egrave;que), 7 (En attente du Virement), 27 (En attente CB), 28 (En attente Mandat Administratif), 29 (En attente de Mandat Cash)<br /><strong style="color: #F00;">Attention action irr&eacute;versible !</strong>
    </div>
    
    <br />
    
    <input type="button" value="Lancer la mise &agrave; jour du stock virtuel de tout les articles" onClick="verif();"/>
    <?php } ?>

</div>
<?php
if ($_REQUEST['lancer']) {
	
	// Modifier le champ "products_ordered" par 0 quand inférieur à 0
	//tep_db_query("UPDATE ".TABLE_PRODUCTS." SET products_ordered = '0' WHERE products_ordered < '0' ");
	
	//mise a jour stock réél
	//tep_db_query("update ".TABLE_PRODUCTS." set products_quantity=products_quantity_reel");
	//tep_db_query("update ".TABLE_PRODUCTS_ATTRIBUTES." set options_quantity=options_quantity_reel");
	/*
	$requete_articles_sans_options = "select products_id, products_quantity from " . TABLE_ORDERS_PRODUCTS . " op inner join " . TABLE_ORDERS . " o on o.orders_id=op.orders_id where orders_status in ('1','6','7','27','28','29')";
	//echo $requete_articles_sans_options;
	$orders_products_query = tep_db_query($requete_articles_sans_options);
	while ($orders_products = tep_db_fetch_array($orders_products_query)) {
		tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity - " . $orders_products["products_quantity"] . " where products_id = '".$orders_products["products_id"]."'");
	}
	
	$requete_articles_avec_options = "select products_id, products_quantity, opa.products_options_values from " . TABLE_ORDERS_PRODUCTS . " op inner join " . TABLE_ORDERS . " o on o.orders_id=op.orders_id, " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " opa where orders_status in ('1','6','7','27','28','29') AND op.orders_products_id = opa.orders_products_id";
	//echo $requete_articles_avec_options;
	$orders_products_query = tep_db_query($requete_articles_avec_options);
	while ($orders_products = tep_db_fetch_array($orders_products_query)) {
		$recup_options_id_query = tep_db_query("SELECT products_options_values_id FROM " . TABLE_PRODUCTS_OPTIONS_VALUES . " WHERE products_options_values_name = '" . $orders_products_query['products_options_values'] . "'");
		$recup_options_id = tep_db_fetch_array($recup_options_id_query);
		
		tep_db_query("update " . TABLE_PRODUCTS_ATTRIBUTES . " set options_quantity = options_quantity - " . $orders_products["products_quantity"] . " where products_id = '".$orders_products["products_id"]."' and options_values_id = '" . $recup_options_id['products_options_values_id'] . "'");
	}
	
	$maj_stock_global_query = tep_db_query("SELECT SUM(options_quantity) as somme, SUM(options_quantity_reel) as somme_reel, products_id FROM " . TABLE_PRODUCTS_ATTRIBUTES . " GROUP BY products_id");
	while ($maj_stock_global = tep_db_fetch_array($maj_stock_global_query)) {
		tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = '" . $maj_stock_global['somme'] . "', products_quantity_reel = '" . $maj_stock_global['somme_reel'] . "' where products_id = '".$maj_stock_global["products_id"]."'");
	}*/
	
	
	
	//on sélectionne les produits pour lesquels il y a une différence entre le stock réel de l'ensemble des options et le stock réel du produit
	$query = 'select  from '. TABLE_PRODUCTS_ATTRIBUTES .'';
	
	$query = 'select p.products_id, p.products_quantity_reel, SUM(pa.options_quantity_reel) as reel_option from '. TABLE_PRODUCTS .' p, '. TABLE_PRODUCTS_ATTRIBUTES .' pa
			 where p.products_id=pa.products_id
			 group by p.products_id
			 having SUM(pa.options_quantity_reel)!=p.products_quantity_reel';
	
	$query = tep_db_query($query);
	
	while($data = tep_db_fetch_array($query)) {
		
		//echo '<a href="'. BASE_DIR_ADMIN .'/article_edit_stocks_options.php?pID='. $data['products_id'] .'&action=new_product">'. $data['products_id'] .'</a> ';
		$query_update = 'update '. TABLE_PRODUCTS .' set products_quantity_reel='. $data['reel_option'] .' where products_id='. $data['products_id'];
		tep_db_query($query_update);//on miet à jour le stock réel du produit par rapport au stock réel des options
	}
	
	//tep_db_query('update '. TABLE_PRODUCTS .' set products_quantity=products_quantity_reel');
	//tep_db_query('update '. TABLE_PRODUCTS_ATTRIBUTES .' set options_quantity=options_quantity_reel');
	
	
	//toutes les commandes sauf les devis
	$query = 'select SUM(products_quantity) as total_quantity, SUM(products_quantity_sent) as total_quantity_sent, products_id, o.orders_id
			  from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op
			  
			  where o.orders_id=op.orders_id
			  and products_quantity!=products_quantity_sent
			  and orders_status!=21
			  
			  group by products_id
			  having total_quantity != total_quantity_sent
			  order by op.products_id';
	//echo $query .'<br>';
	$query = tep_db_query($query);
	
	while($data = tep_db_fetch_array($query)) {
		
		$qte = $data['total_quantity'] - $data['products_quantity_sent'];
		
		$query_update = 'update '. TABLE_PRODUCTS .' set products_quantity=products_quantity_reel-'. $qte .' where products_id='. $data['products_id'];
		//echo $query_update .'<br/>';
		tep_db_query($query_update);
		
		//pour chaque option du produit on vérifie s'il y a des quantité non livrées parmi les commandes
		$query_option = 'select SUM(products_quantity) as products_quantity, SUM(products_quantity_sent) as products_quantity_sent, options_values_id
						 
						 from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op, '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES .' opa
						 
						 where o.orders_id=op.orders_id
						 and op.orders_products_id=opa.orders_products_id
						 and products_quantity!=products_quantity_sent
						 and o.orders_status!=21
						 and op.products_id='. $data['products_id'] .'
						 
						 group by options_values_id';
						 //echo $query_option;
		$query_option = tep_db_query($query_option);
		
		if(tep_db_num_rows($query_option)>0) {
			
			while($data_option = tep_db_fetch_array($query_option)) {
				
				if($data_option['options_values_id']==0) {
					
					echo nl2br('select SUM(products_quantity) as products_quantity, SUM(products_quantity_sent) as products_quantity_sent, options_values_id
						 
						 from '. TABLE_ORDERS_PRODUCTS .' op, '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES .' opa
						 
						 where op.orders_products_id=opa.orders_products_id
						 and products_quantity!=products_quantity_sent
						 and op.products_id='. $data['products_id'] .'
						 
						 group by options_values_id');
						 
					echo '<br>### Commande posant probl&egrave;me avec un produit dont l\'option est '. $data_option['options_values_id'] .'['. $data_option['products_quantity'] .' / '. $data_option['products_quantity_sent'] .']';
					echo ' => <a href="https://www.generalarmystore.fr/gasadmin/cmd_edit.php?oID='. $data['orders_id'] .'&action=edit">'. $data['orders_id'] .'</a><br>';
				}
				
				$qte = $data_option['products_quantity'] - $data_option['products_quantity_sent'];
				
				$query_update = 'update '. TABLE_PRODUCTS_ATTRIBUTES .'
								 
								 set options_quantity=options_quantity_reel-'. $qte .'
								 
								 where products_id='. $data['products_id'] .' and options_values_id='. $data_option['options_values_id'];
				//echo $query_update .'<br/>';
				tep_db_query($query_update);
			}
		}
		
		
		//if(estDevis($data['orders_id'])) $return .= 'TEST --------------------------- ';
		
		
		//echo '<a href="https://www.generalarmystore.fr/gasadmin/cmd_edit.php?oID='. $data['orders_id'] .'&action=edit">'. $data['orders_id'] .'</a><br><br/>';
	}
	
}
?>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>