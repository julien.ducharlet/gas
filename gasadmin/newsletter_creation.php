<?php
/*
  PAGE: newsletter_creation.php
*/

  require('includes/application_top.php');
  
  function verifyOptionNewsletter($action, $get, $value) {
	  
		return ($action=='edit' && $get==$value) ? true : false;
  }
?>
<?php
	
if(!empty($_POST)) {
	
	switch($_GET['action']) {
		
		case "new":
			
			$type_client_list = implode(',', $_POST['type_id_client']);
			
			$query = 'insert into '. TABLE_NEWSLETTERS .'(newsletters_title, newsletters_content, date_creation, last_modified, date_envoi, type_id_client, type_client, type_cmd, user)
				values(\''. addslashes($_POST['title']) .'\', 
					  \''. htmlentities(addslashes($_POST['content'])) .'\',
					  \''. date('Y-m-d H:i:s') .'\',
					  \''. date('Y-m-d H:i:s') .'\',
					  \''. $_POST['date_to_send'] .'\',
					  \''. $type_client_list .'\',
					  \''. str_replace('_', ' ', $_POST['type_client']) .'\', 
					  \''. $_POST['type_panier'] .'\', 
					  '. $_SESSION['login_id'] .')';
			
			if(tep_db_query($query)) {
				
				$news_id = mysql_insert_id();
				
				$query = 'update '. TABLE_NEWSLETTERS .' set newsletters_md5=\''. md5($news_id) .'\' where newsletters_id='. $news_id;
				tep_db_query($query);
				
				$messageStack->add_session('Enregistrement bien effectu�', 'success');
			}
			else $messageStack->add_session('Erreur lors de l\'enregistrement de la Newsletter', 'warning');
				
			break;
		
		case "edit":
			
			if(isset($_GET['newsletter']) && !empty($_GET['newsletter'])) {
				
				$type_client_list = implode(',', $_POST['type_id_client']);
				
				$query = 'update '. TABLE_NEWSLETTERS .' set newsletters_title=\''. addslashes($_POST['title']) .'\',
							newsletters_content = \''. htmlentities(addslashes($_POST['content'])) .'\',
							last_modified = \''. date('Y-m-d H:i:s') .'\',
							date_envoi = \''. $_POST['date_to_send'] .'\',
							type_id_client = \''. $type_client_list .'\', 
							type_client = \''. str_replace('_', ' ', $_POST['type_client']) .'\',
							type_cmd = \''. $_POST['type_panier'] .'\'
							
							where newsletters_id='. $_GET['newsletter'];
				
				if(tep_db_query($query)) $messageStack->add_session('Modification bien effectu�e', 'success');
				else $messageStack->add_session('Erreur lors de la modification de la Newsletter', 'warning');
			}
			
			break;
	}
	
	
	
	tep_redirect("newsletter_list.php");
}
	
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></link>
<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/general.js"></script>

<!-- Load jQuery -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("jquery", "1.3");
</script>

<script language="javascript" type="text/javascript" src="includes/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript">


</script>

<script type="text/javascript">

$(document).ready(function() {
	
	show_custom();
	
	tinyMCE.init({
				 
	// General options
	mode : "textareas",
	theme : "advanced",
	
	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,imagemanager,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
	
	// Theme options
	theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
	theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
	theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
	theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,insertimage",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing: true,
	language : "fr",
	
	relative_urls : false,
	remove_script_host : false,
	document_base_url : "<?php echo TINY_MCE_URL; ?>"

	// Example content CSS (should be your site CSS)
	// content_css : "includes/stylesheet.css",
	});
	
	
	$("#type_panier").change(function() {
		
		show_custom();
	});
	
	$(".type_id_client").change(function() {
		
		show_custom();
	});
	
	$("#type_client").change(function() {
		
		show_custom();
	});
	
	$('.loading').ajaxStart(function() {
		
		$("#answer").hide();
		$(".loading img").show();
	});
	
	$("#send_data").click(function() {
		
		if($("#title").val() != '') $("#myForm").submit();
		else  alert("Cette Newsletter n'a pas de titre");
	});
	
	$("#preview").click(function() {
		
		var content_news = tinyMCE.get('content');
		
		$("#zone_text").html(content_news.getContent());
	});
	
	
	function show_custom() {
		
		var type_id_client = '';
		temp = $(":checkbox").serializeArray();
		
		$.each(temp, function(i, field){
			
			if(i > 0) type_id_client += ',';
			
			type_id_client += field.value;
		  });
		
		nb_custom = $("#custom_"+ $("#type_client").val()).val();
		
		$.ajax({
		   type: "GET",
		   url: "includes/javascript/ajax/show_nb_customers_newsletter.php?type_client="+ $("#type_client").val() +"&type_id_client="+ type_id_client +"&type_panier="+ $("#type_panier").val(),
		   dataType: "text",
		   processData: false,
		   success: function(data){
				
				//if(data != '') nb_custom = data;
				
				$(".loading img").hide();
				$("#answer").html("Voici le nombre de mails qui seront envoy�s : <b>"+ data +"</b>").show("slow");
		   }
		});
	}
});
</script>

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">

<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->

    <td width="100%" valign="top">
		<?php
        if(isset($_GET['newsletter']) && !empty($_GET['newsletter'])) {
			
			echo '<form method="post" action="?newsletter='. $_GET['newsletter'] .'&action=edit" id="myForm">';
		}
		else {
			
			echo '<form method="post" action="?action=new" id="myForm">';
		}
		?>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
			
			<tr>
				<td width="100%" colspan="2">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td class="pageHeading">Cr�ation d'une Newsletter</td>
						</tr>
                        <tr>
							<td>
                            	<div style="color:#FF0000; font-weight:bold; margin-top:10px;">Attention ! Pour ajouter un lien, ne remplir que le champ <em><u>URL du lien</u></em> de Tiny MCE</div>
                                <div style="color:#FF0000; font-weight:bold">Les liens qui pointent sur un article doivent �tre r�cup�r�s dans l'administration</div>
                            </td>
						</tr>
					</table>
				</td>
			</tr>
            
			<tr>
				<td align="left" class="main" colspan="2">&nbsp;
				</td>
			</tr>
	   
			<tr>
			  	<td align="left" colspan="2">	
					<?php
					
					$type_client_array = array();
					
					if(isset($_GET['action']) && $_GET['action']=='edit' && isset($_GET['newsletter']) && !empty($_GET['newsletter'])) {
						
						$query = 'select * from '. TABLE_NEWSLETTERS .' where newsletters_id='. $_GET['newsletter'];
						$query = tep_db_query($query);
						
						$newsletter = tep_db_fetch_array($query);
						
						$type_client_array = explode(',', $newsletter['type_id_client']);
					}
					
					/* calcul du nombre d'utilisateurs abonn�s et non abonn�s � la newsLetter*/
					$query = 'select count(*) as inscrit from '. TABLE_CUSTOMERS .' where customers_newsletter=\'1\'';
					$query = tep_db_query($query);
					$data = tep_db_fetch_array($query);
					
					$customers['inscrit'] = $data['inscrit'];
					
					
					$query = 'select count(*) as inscrit from '. TABLE_CUSTOMERS .' where customers_newsletter_partenaire=\'1\'';
					$query = tep_db_query($query);
					$data = tep_db_fetch_array($query);
					
					$customers['partenaire'] = $data['inscrit'];
					
					
					$query = 'select count(*) as total from '. TABLE_CUSTOMERS;
					$query = tep_db_query($query);
					$data = tep_db_fetch_array($query);
					
					$customers['total'] = $data['total'];
					$customers['non_inscrit'] = $customers['total'] - $customers['inscrit'];
					?>
                    <div style="float:left;">
                        <select name="type_client" id="type_client" style="margin-right:20px;">
                            <option <?php  if(verifyOptionNewsletter($_GET['action'], $newsletter['type_client'], 'inscrit')) echo 'selected="selected" '; ?>value="inscrit">Client inscrit � la Newsletter (<?php echo $customers['inscrit']; ?>)</option>
                            <option <?php  if(verifyOptionNewsletter($_GET['action'], $newsletter['type_client'], 'partenaire')) echo 'selected="selected" '; ?>value="partenaire">Clients inscrit email Partenaire (<?php echo $customers['partenaire']; ?>)</option>
                            <option <?php  if(verifyOptionNewsletter($_GET['action'], $newsletter['type_client'], 'non inscrit')) echo 'selected="selected" '; ?>value="non_inscrit">Clients non inscrit � la Newsletter (<?php echo $customers['non_inscrit']; ?>)</option>
                            <option <?php  if(verifyOptionNewsletter($_GET['action'], $newsletter['type_client'], 'tous')) echo 'selected="selected" '; ?>value="tous">Tous les Clients (<?php echo $customers['total']; ?>)</option>
                        </select>
                        
                        <select name="type_panier" id="type_panier" style="margin-right:20px;">
                            <option <?php  if(verifyOptionNewsletter($_GET['action'], $newsletter['type_cmd'], 'tous')) echo 'selected="selected" '; ?>value="tous">Tous (Avec et sans commande)</option>
                            <option <?php  if(verifyOptionNewsletter($_GET['action'], $newsletter['type_cmd'], 'avec')) echo 'selected="selected" '; ?>value="avec">Avec commande</option>
                            <option <?php  if(verifyOptionNewsletter($_GET['action'], $newsletter['type_cmd'], 'sans')) echo 'selected="selected" '; ?>value="sans">Sans Commande</option>
                        </select>
                        <!--<select name="type_id_client" id="type_id_client">
                        	<option value="">Tous les clients</option>-->
                    </div> 
                    <div>
						<?php
                        
                        # type de client
                        $query = 'select type_id, type_name from '. TABLE_CUSTOMERS_TYPE;
                        $query = tep_db_query($query);
                        
                        while($data = tep_db_fetch_array($query)) {
                            
							$checked = (in_array($data['type_id'], $type_client_array) || empty($newsletter['newsletters_id'])) ? ' checked="checked"' : '';
                        /*	?><option <?php if(verifyOptionNewsletter($_GET['action'], $newsletter['type_id_client'] ,$data['type_id'])) echo 'selected="selected" '; ?>
                                    value="<?php echo $data['type_id']; ?>"><?php echo $data['type_name']; ?></option><?php*/
                            echo '<div style="float:left;margin-right:10px;">';
                                
                                echo '<input type="checkbox" class="type_id_client" name="type_id_client[]" value="'. $data['type_id'] .'"'. $checked .'>&nbsp;'. $data['type_name'];
                            
                            echo '</div>';
                        }
                        ?>
                       	<input type="hidden" id="custom_tous" value="<?php echo $customers['total']; ?>"><!--tous les clients -->
                        <input type="hidden" id="custom_partenaire" value="<?php echo $customers['partenaire']; ?>"><!--tous les clients -->
                        <input type="hidden" id="custom_inscrit" value="<?php echo $customers['inscrit']; ?>"><!--tous les clients -->
                        <input type="hidden" id="custom_non_inscrit" value="<?php echo $customers['non_inscrit']; ?>"><!--tous les clients -->
					</div>
                    <div style="clear:both;"></div>
			  	</td>
			</tr>
			
			<tr>
			  	<td align="left">
					<div class="loading" style="width:633px;margin-top:10px;">
                        <img src="images/loading.gif" alt="loading" style="display:none;">
                        <div id="answer" style="padding:2px 5px;">Voici le nombre de mails qui seront envoy�s : <b></b></div>
                   	</div>
				</td>
                <td align="left">&nbsp;
					
				</td>
			</tr>
			
            <tr>
			  	<td align="left" colspan="2">&nbsp;
				</td>
			</tr>
            
            <tr>
			  	<td align="left" colspan="2">Date d'envoi : <input type="text" id="date_to_send" name="date_to_send" value="<?php if(isset($_GET['newsletter']) && !empty($_GET['newsletter'])) echo $newsletter['date_envoi']; ?>"> <a href="#" onClick="displayCalendar(date_to_send,'yyyy-mm-dd',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>&nbsp;&nbsp;(<em>la newsletter sera envoy�e � 2h du matin</em>)
				</td>
			</tr>
            
			<tr>
			  	<td align="left" colspan="2">
					Titre de la Newsletter : <input type="text" id="title" name="title" size="50" value="<?php if(isset($_GET['newsletter']) && !empty($_GET['newsletter'])) echo $newsletter['newsletters_title']; ?>">
				</td>
			</tr>
			
			<tr>
			  	<td align="left" colspan="2">&nbsp;
					
				</td>
			</tr>
			
			<tr>
			  	<td align="center" width="50%" valign="top">
                	
						<textarea name="content" cols="120" rows="15"><?php if(isset($_GET['newsletter']) && !empty($_GET['newsletter'])) echo stripslashes($newsletter['newsletters_content']); else echo 'BONJOUR, <b>N\'oubliez pas de rempalcer mon texte !</b>'?></textarea>
						<br /><input type="button" value="Sauvegarder" id="send_data">&nbsp;&nbsp;<input type="button" value="Pr�visualiser" id="preview">
			  	</td>
                <td align="center" width="50%" valign="top">
                	<table class="mail_preview">
                    	<tr>
                        	<td><img style="border:0px none;height:121px;" src="<?php echo WEBSITE . BASE_DIR; ?>/template/base/mail/header800.jpg" alt="Activer l'affichage des images de votre service email."></td>
                        </tr>
                        <tr><td><div id="zone_text"><?php if(isset($_GET['newsletter']) && !empty($_GET['newsletter'])) echo html_entity_decode(stripslashes($newsletter['newsletters_content'])); ?></div></td></tr>
                        <tr><td><img src="<?php echo WEBSITE . BASE_DIR; ?>/template/base/mail/footer-vert.jpg" alt="Pensez � activer l'affichage des images de votre service mail." style="height:29px;"></td></tr>
                	</table>
			  	</div></td>
			</tr>	
			
			<tr>
			  	<td align="left" colspan="2">&nbsp;
					
				</td>
			</tr>	
			   
	   
		</table>
        </form>
	</td>
	
<!-- body_text_oef //-->
	
	
</tr>
</table>
</td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
