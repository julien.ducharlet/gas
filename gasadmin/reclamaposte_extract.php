<?php 
/*
  $Id: reclamaposte_extract.php,v 1.0 2009/01/24

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
  
  Written by Olivier B. (delete: forum oscommerce-fr.info) 
  
  updated 2009/02/01 by delete
  updated and merged by Phocea 11/03/2009

*/

/* Section de param�trage */

 $date = strftime('%d %B %Y');
 require('includes/application_top.php');

 include(DIR_WS_INCLUDES . 'reclamaposte.php');
 @include(DIR_WS_FUNCTIONS . 'oe_functions.php');
 include(DIR_WS_CLASSES . 'order.php');
 include(DIR_WS_FUNCTIONS . 'reclamaposte.php');
 
// Import du fichier
 if ( file_exists($tmp_dir . '/laposte.remboursements.txt' ) ) {
  $remboursements = file_get_contents($tmp_dir . '/laposte.remboursements.txt') ;
 } else {
 	echo "Aucune demande de Remboursement en Attente" ;
 	exit ;
 }

 $liste_colis = explode('|', $remboursements) ;

 if ( ! count($liste_colis) ) {
 	echo "Aucune demande de Remboursement en Attente" ;
 	exit ;
 }
 
 $format = $_GET['format'];
 if ($format == 'pdf') {
	 include(DIR_WS_CLASSES . 'fpdf/fpdf.php');
	 class PDF extends FPDF {
	 	function Header() {
	 		//Logo
	 		if ( file_exists('images/logopro.jpg') ) $this->Image('images/logopro.jpg',70,10,70);
		}
		
		function Footer() {
			$this->SetY(-25);
			$this->SetFont('Arial','',8);
			$this->MultiCell(200, 5, 'EURL MTP34 - SIRET : 489468728 00022 - RCS : 489468728 - NAF : 4791B', 0, 'C');
			$this->SetY(-25);
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}
	 }
	
	 function colisHeader() {
	 	global $pdf ;
	
		$pdf->SetY(40) ;
		$pdf->SetX(10) ;
			$pdf->MultiCell(50, 5, 'Num�ro du colis', 0, 'L');
		$pdf->SetY(40) ;
		$pdf->SetX(45) ;
			$pdf->MultiCell(50, 5, 'Date d\'envoi', 0, 'L');
		$pdf->SetY(40) ;
		$pdf->SetX(70) ;
			$pdf->MultiCell(50, 5, 'Pr�sent� le', 0, 'L');
		$pdf->SetY(40) ;
		$pdf->SetX(100) ;
			$pdf->MultiCell(50, 5, 'Jours', 0, 'L');
		$pdf->SetY(40) ;
		$pdf->SetX(115) ;
			$pdf->MultiCell(80, 5, 'R�ferences Internes, Adresse', 0, 'L');
		$pdf->SetY($y += 6) ;
	 }
	
	//Instanciation of inherited class
	 $pdf = new PDF();
	// Set the Page Margins
	 $pdf->SetMargins(6,2,6);
	// Add the first page
	 $pdf->AddPage();
	
	//Draw the invoice address text
	//Draw the invoice delivery address text
	 
	 $pdf->SetFont('Arial','B',10);
	 $pdf->SetTextColor(0);
	 $pdf->SetY(50);
	 $pdf->SetX(120);
	 	$pdf->MultiCell(70, 5, 'Service Clients ColiPoste - 14, Rue Antoine de Saint Exup�ry - BP 7500 - 88107 SAINT DIE CEDEX', 0, 'L');
	 $pdf->SetY(50);
	 $pdf->SetX(20);
	  	$pdf->MultiCell(70, 5, 'MOBILE-SHOP - Place Pierre Lav�ne - 34880 Lav�rune', 0, 'L');
	// Date
	 // $date = strftime('%d %B %Y');
	 $pdf->SetY(80);
	 $pdf->SetX(120);
	 $pdf->MultiCell(70, 5, 'LAVERUNE le ' . $date , 0, 'L');
	// R�f�rences
	 $pdf->SetY(80);
	 $pdf->SetX(20);
	 	$pdf->MultiCell(70, 5, 'Contrat N� 997495', 0, 'L');
	// Corps du document
	 $pdf->SetY(100);
	 $pdf->SetX(20);
	 	$pdf->MultiCell(180, 5, 'Madame, Monsieur,

Vous trouverez ci-joint la liste des colis pour lesquels nous portons r�clamation conform�ment � vos conditions de ventes.

Nous vous saurions gr� de bien vouloir proc�der � un avoir ou un remboursement rapide de ces colis.

Veuillez agr�er, Madame, Monsieur, l expression de nos salutations distingu�es.

                                                                                           Thierry POULAIN
																						   Tel : 0951 999 800', 0, 'L');
	// Add the first page
	 $pdf->AddPage();
	 $pdf->SetY(40) ;
	 $pdf->SetFont('Arial','',9);
	 colisHeader() ;
 } else {
 	require_once(DIR_WS_CLASSES.'WriteExcel/class.writeexcel_workbook.inc.php');
 	require_once(DIR_WS_CLASSES.'WriteExcel/class.writeexcel_worksheet.inc.php');
 	
 	$fname = tempnam($tmp_dir, "laposte.xls");
 	$workbook = &new writeexcel_workbook($fname);
 	$worksheet = &$workbook->addworksheet();
	// Entete : compte client
	$header =& $workbook->addformat();
	$header->set_bold();
	$header->set_size(12);
	$header->set_num_format(0) ;
	
	$worksheet->write(0, 0, 'Compte client :');
	$worksheet->write(0, 1, '="997495"', $header);
	
	$worksheet->write(0, 2, 'Raison sociale :');
	$worksheet->write(0, 3, '="EURL MTP34"', $header);
	
	$worksheet->write(1, 0, 'SIRET :');
	$worksheet->write(1, 1, '="489468728 00022"', $header);
	//
	// Format de l'ent�te du tableau de liste de colis
	//
	$tbl_header =& $workbook->addformat();
	$tbl_header->set_bold();
	$tbl_header->set_size(12);
	$tbl_header->set_border(1);
	//
	// Taille des colonnes
	//
	$worksheet->set_column(0, 0, 20);
	$worksheet->set_column(1, 1, 15);
	$worksheet->set_column(2, 2, 25);
	$worksheet->set_column(3, 3, 25);
	$worksheet->set_column(4, 4, 40);
	$worksheet->set_column(5, 5, 15);
	$worksheet->set_column(6, 6, 30);
	//
	// Ent�te liste des Colis
	//
	$worksheet->write(3, 0, 'Date d�p�t colis', $tbl_header);
	$worksheet->write(3, 1, 'N� colis', $tbl_header);
	$worksheet->write(3, 2, 'Nom Destinataire', $tbl_header);
	$worksheet->write(3, 3, 'Pr�nom Destinataire', $tbl_header);
	$worksheet->write(3, 4, 'Adresse ', $tbl_header);
	$worksheet->write(3, 5, 'Code postal', $tbl_header);
	$worksheet->write(3, 6, 'Ville', $tbl_header);
	//
	// Format liste des colis
	//
	$line_format =& $workbook->addformat();
	$line_format->set_border(1);

 }

 for($i = 0, $line=3, $demandes_de_remboursement = 0, $y = 50 ; $i < sizeof($liste_colis) ; $i++) {
      if ( ! tep_not_null($liste_colis[$i]) ) continue ;

        $str = explode(';', $liste_colis[$i]) ;

        $order_id        = $str[0] ;
        $date_expedition = $str[1] ;
        $date_livraison  = $str[2] ;
        $noColis         = $str[3] ;
        $nb_jours_ouvres = $str[4] ;

        $demandes_de_remboursement++;
        

        $order = new order($order_id) ;

        // Une soci�t� on remplit le champs Nom avec le nom de la boite et pr�nom avec le blaze du mec
        if ( isset($order->delivery['company']) && tep_not_null($order->delivery['company']) ) {
          $nom = tep_mb_strtoupper($order->delivery['company']) ;
          $prenom = tep_mb_strtoupper($order->delivery['name']) ;
        } else {
          // A changer dans la table orders nous n'avons pas le nom complet mais nom/pr�nom
          // Il faut aller piocher dans address_book, � faire ...
          $nom = tep_mb_strtoupper($order->delivery['name']) ; 
          $prenom = '' ;
        }
        
        $adresse = tep_mb_strtoupper($order->delivery['street_address']) ;
        
        if ( isset($order->delivery['suburb']) && tep_not_null($order->delivery['suburb']) ) {
          $adresse .= '\n' . tep_mb_strtoupper($order->delivery['suburb']) ;
        }

	// On g�n�re les lignes de r�clamation !
				if ($format == 'pdf') {        
					$item_count++;
					$pdf->SetY($y) ;
					$pdf->SetX(10) ;
						$pdf->MultiCell(50, 5, $noColis, 0, 'L');
					$pdf->SetY($y) ;
					$pdf->SetX(45) ;
						$pdf->MultiCell(50, 5, date('d-m-Y', $date_expedition), 0, 'L');
					$pdf->SetY($y) ;
					$pdf->SetX(70) ;
						$pdf->MultiCell(50, 5, date('d-m-Y', $date_livraison), 0, 'L');
					$pdf->SetY($y) ;
					$pdf->SetX(100) ;
						$pdf->MultiCell(50, 5, $nb_jours_ouvres, 0, 'L');
					$pdf->SetY($y) ;
					$pdf->SetX(115) ;
					   $pdf->MultiCell(50, 5, $order_id, 0, 'L');
					$pdf->SetY($y) ;
					$pdf->SetX(130) ;
					   $pdf->MultiCell(80, 5, tep_address_format($order->delivery['format_id'], tep_mb_strtoupper($order->delivery), '', '', "\n"), 0, 'L');
					$pdf->SetY($y += 26) ;
					if ( !( $item_count % 8 ) ) {
		  			$pdf->AddPage();
		  			colisHeader() ;
		  			$y = 50;
		  			if ($item_count == 8 )  $item_count = 1;
					}
				} else {
	        $line ++ ;
	        $worksheet->write($line, 0, '="' . date('d/m/Y', $date_expedition) . '"', $line_format);
	        $worksheet->write($line, 1, $noColis, $line_format);
	        $worksheet->write($line, 2, $nom, $line_format);
	        $worksheet->write($line, 3, $prenom, $line_format);
	        $worksheet->write($line, 4, $adresse, $line_format);
	        $worksheet->write($line, 5, '="' . tep_mb_strtoupper($order->delivery['postcode']). '"', $line_format);
	        $worksheet->write($line, 6, tep_mb_strtoupper($order->delivery['city']), $line_format);
	      }
 }

 if ( !$demandes_de_remboursement ) {
	echo "Aucune demande de Remboursement en Attente";
	exit ;
 }
if ($format == 'pdf') {
	// PDF's created now output the file
	$pdf->AliasNbPages();
	$pdf->Output();
	
} else {
	$workbook->close();
	
	$msg = "Si vous n'arrivez pas � lire ce message merci de prendre contact avec nous par retour de message ou en nous �crivant � cette adresse : " . MAIL_INFO . "\r\n";
	$msg .= "\r\n";
	$msg .= "Transf�rer ce message � : scsmb.clp@laposte.fr
o�
A l attention de :  
GENIN St�phanie
Service Clients ColiPoste
14 Rue Antoine de Saint Exup�ry
BP 7500
88107 SAINT DIE DES VOSGES Cedex
T�l : 0825 878 888
Fax : 03 29 42 15 91
--------------------------<br />
Madame, Monsieur,

Vous trouverez ci-joint la liste des colis pour lesquels nous portons r�clamation conform�ment � vos conditions de ventes.

Nous vous saurions gr� de bien vouloir proc�der � un avoir ou un remboursement rapide de ces colis.

Veuillez agr�er, Madame, Monsieur, l expression de nos salutations distingu�es.

                                                                                           La Direction." ;
	$msg .= "\r\n";
	
	$email_destinataire = LAPOSTE_EMAIL_DESTINATAIRE;
	if (COLIPOSTE_IN_TEST == 1) $email_destinataire = LAPOSTE_EMAIL_EXPEDITEUR;
	
	tep_mail_attachment(LAPOSTE_EMAIL_DESTINATAIRE, $email_destinataire, 'R�clamation Coliposte', $msg, LAPOSTE_EMAIL_EXPEDITEUR, LAPOSTE_EMAIL_EXPEDITEUR, $fname, 'matrice_coliposte', 'application/x-msexcel');
	
	unlink($fname);
	
	$messageStack->add_session('Email Envoy� � :' . $email_destinataire, 'success');
	
	tep_redirect(tep_href_link('reclamaposte.php')) ;
}
?>
