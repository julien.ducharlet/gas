<?php
require('includes/application_top.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title>Export Avis Verifi�s : CMD & Produits (5 jours)</title>

<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">

	<?php	
	$last=0;
	if (isset($HTTP_GET_VARS['commandes']) && strlen($HTTP_GET_VARS['commandes'])>0){
		$id_commandes=substr(ltrim($HTTP_GET_VARS['commandes']),0,-1);
		$cmd = str_replace(' ', "','", ($id_commandes));
		$cmd= "'".$cmd."'";
		$query_orders = 'select o.orders_id, o.delivery_country, o.customers_id, o.date_purchased, p.products_id, p.products_bimage, orders_products_id, products_name, p.products_code_barre, p.manufacturers_id, m.manufacturers_id, m.manufacturers_name, op.id_rayon, op.cpath
						from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS  .' op, '. TABLE_PRODUCTS .' p, ' . TABLE_MANUFACTURERS .' m
						where o.orders_id=op.orders_id
						and p.products_id=op.products_id
						and p.manufacturers_id=m.manufacturers_id
						and o.orders_id in ('. $cmd .')
						order by orders_id';
		
		$orders_query = tep_db_query($query_orders);
		
		// Fonction de localisation et d'ouverture dans le fichier 
		$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/tmp/export_avis-verifies_'. date('d-m-Y_H-i').'.csv','w+');
		//$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/tmp/export_avis-verifies.csv','w+');
		
		
		fwrite( $fp , "\xEF\xBB\xBFemail;order_ref;order_date;delay;lastname;firstname;id_product;name_product;url_product;url_image_product \n");		
		
		echo '<hr>';
		echo '<h2 style="text-align:center;">Les avis seront envoy�s 5 jours apr�s par Avis Verifi�s</h2>';
		echo '<hr>';
		
		echo '<br><table width="100%">
		<tr>
		<td align="center" width="10%">R�f�rence</td>
		<td align="center" width="15%">Date</td>
		<td width="30%">Pr�nom & Nom</td>
		<td align="center" width="5%">Id</td>
		<td width="40%">Nom produit</td>
		</tr>
		';
		while($tuple = tep_db_fetch_array($orders_query)) {
				
			$nom_produit = $tuple['products_name'];
			
			// S�parateur de commande
			if($tuple['orders_id']!=$last && $_REQUEST["export"]!="fournisseur") { 
				echo '<tr><td colspan="10" height="20" ></td></tr>'; 
				$last=$tuple['orders_id']; 
			}
			
			// Requ�te pour r�cup�rer les information du client
			$customers_type_query = tep_db_query("select customers_id, customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_id = '" . $tuple['customers_id'] . "'");
			$row_client = mysql_fetch_array($customers_type_query);	
			
			// On trasforme le pr�nom et le nom sans accent
			$prenom = ucfirst(strtolower($row_client['customers_firstname']));
			$nom = strtoupper($row_client['customers_lastname']);
			
			// On d�coupe le "cpath" pour r�cup�rer les ID 				
			$DecoupeCpath= explode("_",$tuple['cpath']);
			// Requetes pour r�cup�rer l'URL des rayons
			$requete_rayon = 	mysql_query("SELECT 
								rubrique_id, 
								rubrique_url
								FROM
								rubrique
								WHERE
								rubrique_id = '". $tuple['id_rayon'] ."'
								");
			$data_requete_rayon = mysql_fetch_array($requete_rayon);
			// Requetes pour r�cup�rer l'URL des marques et des mod�les
			$requete_marque = 	mysql_query("SELECT 
								categories_id, 
								categories_url
								FROM
								categories_description
								WHERE
								categories_id = '".$DecoupeCpath[0]."'
								");
			$data_requete_marque = mysql_fetch_array($requete_marque);
			// Requetes pour r�cup�rer l'URL des mod�les
			$requete_modele = 	mysql_query("SELECT 
								categories_id, 
								categories_url
								FROM
								categories_description
								WHERE
								categories_id = '".$DecoupeCpath[1]."'
								");
			$data_requete_modele = mysql_fetch_array($requete_modele);
			
			// On stock les r�sultats de requete pour transformation dans des variables
			$url_rayon=$data_requete_rayon['rubrique_id'].'-'.strtolower(encode_url_sitemap_prod($data_requete_rayon['rubrique_url'])).'/';
			$url_marque=$data_requete_marque['categories_id'].'-' .strtolower(encode_url_sitemap_prod($data_requete_marque['categories_url'])).'/';
			$url_modele=$data_requete_modele['categories_id'].'-' .strtolower(encode_url_sitemap_prod($data_requete_modele['categories_url'])).'/';
			$url_produit=$tuple['products_id'].'-'.encode_url_sitemap_prod(strtolower(tep_get_products_url($tuple['products_id'],1))).'.html';
			
			// Affichage
			echo '<tr>';
			echo '<td align="center">' . $row_client['customers_id'] .'-' .$tuple['orders_id']. '</td>';
			echo '<td align="center">' . $tuple['date_purchased'] . '</td>';
			echo '<td>' . strip_accents($prenom) . ' - ' .strip_accents($nom). '</td>';
			echo '<td align="center"><a href="https://generalarmystore.fr/gas/'.$url_rayon.$url_marque.$url_modele.$url_produit.'" target="_blank">' . $tuple['products_id']. '</a></td>';
			//echo '<td>' . strip_accents($nom_produit). '</td>';
			echo '<td>' . tep_get_products_url($tuple['products_id'],1) . '</td>';
			echo '</tr>';
			
			// Pour la marque du produit
			// echo $tuple['manufacturers_name'].';';
			// Pour le code barre du produit
			// echo $tuple['products_code_barre'].';';
			
			//echo strip_accents(tep_get_products_url($tuple['products_id'],1)).',';
			//echo 'https://generalarmystore.fr/gas/'.$url_rayon.$url_marque.$url_modele.$url_produit.', ';				
			//echo 'https://www.generalarmystore.fr/gas/images/products_pictures/normale/'.$tuple['products_bimage'].'<br>';
			
			$write = $row_client['customers_email_address']. ';';
			$write .= $row_client['customers_id'] .'-' .$tuple['orders_id']. ';';
			$write .= $tuple['date_purchased'] . ';';
			$write .= '5;'; // nombre de jours avant envoi
 			$write .= strip_accents($nom) . ';';	
			$write .= strip_accents($prenom) . ';';
			//$write .= '0;';
			$write .= $tuple['products_id']. ';';
			//$write .= strip_accents($nom_produit). ';';
			$write .= strip_accents(tep_get_products_url($tuple['products_id'],1)). ';';
			$write .= 'https://generalarmystore.fr/gas/'.$url_rayon.$url_marque.$url_modele.$url_produit. ';';	
			$write .= 'https://www.generalarmystore.fr/gas/images/products_pictures/normale/'.$tuple['products_bimage'];
			
			
			// Pour la marque du produit
			// $write .= $tuple['manufacturers_name']. ';';
			// Pour le code barre du produit
			// $write .= $tuple['products_code_barre']. ';';
			
			fwrite( $fp , $write . "\n");			
		}	
	}else{
		echo "<table><tr><td align=center><b>Aucune commande de s�lectionn�e</b></td></tr></table>";
	}
	echo '</table>';
	echo '<hr style="margin:20px 0 20px 0">';
	echo '&nbsp;&nbsp;&nbsp;T�l�charger le fichier CSV : <a href="https://www.generalarmystore.fr/gasadmin/tmp/export_avis-verifies_'. date('d-m-Y_H-i').'.csv">export_avis-verifies_'. date('d-m-Y_H-i').'.csv</a>';
	echo '<hr style="margin:20px 0 20px 0">';
	
	echo'&nbsp;&nbsp;&nbsp;Pour envoyer le fichier sur le FTP de Avis v�rifi�s 
	<a href="Avis_Avis-Verifies_Produits_upload-ftp.php?fichier=export_avis-verifies_'. date('d-m-Y_H-i').'.csv" target="_blank"><strong>Cliquez ici</strong></a>';
	
	/*
	// Acc�s au FTP
	$ftp = ftp_connect("global-ftp.netreviews.eu", 21) or exit ("Erreur : connexion au serveur FTP impossible.");
	// Connexion au serveur 
	ftp_login($ftp, "fr_generalarmystore.fr", "hXOKcIjT") or die ("Erreur : Connexion au compte FTP impossible");
	
	// Permet de lister le contenu du dossier
	//echo '&nbsp;&nbsp;&nbsp;Arborescence du FTP<br>'; 
	//$liste_fichiers = ftp_nlist($ftp, '.');
	//foreach($liste_fichiers as $fichier)
	//{ echo '&nbsp;&nbsp;&nbsp;<a href="?commande=' .$HTTP_GET_VARS['commandes'] . ' &action=TrustProduit&filename=' .$fichier. '">' .$fichier. '</a><br/>'; }
	
	// On t�l�charge un fichier (ftp_get($ftp, "dossier o� envoyer", "fichier � t�l�charger", "mode");)
	// $nom_fichier = 'export_avis-verifies_'. date('d-m-Y_H-i').'.csv';
	// ftp_get($ftp, "https://www.generalarmystore.fr/gasadmin/tmp/", $nom_fichier, FTP_BINARY);
	
	// On envoi un fichier (ftp_put($ftp, "dossier o� le fichier sera envoy� (serveur)", "Fichier local", type);)
	ftp_put($ftp, "orders/export_avis-verifies_". date('d-m-Y_H-i').".csv", "/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/tmp/export_avis-verifies_". date('d-m-Y_H-i').".csv", FTP_BINARY);
	
	ftp_close($ftp);
	*/
	
	echo '<hr style="margin:20px 0 20px 0">';
	echo '&nbsp;&nbsp;&nbsp;<strong>ACCES FTP AVIS VERIFIES</strong><br><br>';
	echo '<table>';
	echo '<tr><td>&nbsp;&nbsp;&nbsp;H�te :</td> <td> global-ftp.netreviews.eu </td></tr>';
    echo '<tr><td>&nbsp;&nbsp;&nbsp;Identifiant :</td> <td> fr_generalarmystore.fr </td></tr>';
    echo '<tr><td>&nbsp;&nbsp;&nbsp;Mot de passe :</td> <td> hXOKcIjT </td></tr>';
    echo '<tr><td>&nbsp;&nbsp;&nbsp;Port :</td> <td> 21 </td></tr>';
	echo '</table>';
	echo '<hr style="margin:20px 0 20px 0">';
	
	// info pour fonction FTP : 
	// https://openclassrooms.com/courses/interagir-avec-le-serveur-ftp
	// https://openclassrooms.com/forum/sujet/aide-script-php-upload-et-envoi-de-fichier-vers-ftp-35170
	?>
	
</body>
</html>