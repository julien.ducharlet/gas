<?php
/* Fonction : Listing des clients */
  require('includes/application_top.php');
  require('includes/functions/date.php');
  $Nombre_de_ligne_par_page=20;
  $action = (isset($_REQUEST['action']) ? $_REQUEST['action'] : '');
  
  if (isset($_REQUEST['action']) && $_REQUEST['action'] == "setflag"  && tep_not_null($_REQUEST['rID'])) {
	tep_db_query("update ". TABLE_RAYON ." set rubrique_status='".$_REQUEST["flag"]."' where rubrique_id='".$_REQUEST["rID"]."'");
  }
  elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == "new") {
	  
		tep_db_query("insert into ". TABLE_RAYON ."(rubrique_name, rubrique_status, rayon_status_tous, rayon_status_client, rayon_status_pro, rayon_status_adm, rayon_status_rev) values('Nouveau Rayon', '0', '0', '0', '0', '0', '0')");
	  	
		$id_rayon = tep_db_insert_id();
		tep_db_query('insert into '. TABLE_CATEGORIES .'(categories_status, date_added) values(0, NOW())');
		
		$id_cat = tep_db_insert_id();
		tep_db_query('insert into '. TABLE_CATEGORIES_DESCRIPTION .'(categories_id, categories_name) values('. $id_cat .', \'CATEGORIE A SUPPRIMER\')');
	  	tep_db_query('insert into '. TABLE_CATEGORIES_RAYON .'(categories_id, rubrique_id) values('. $id_cat .', '. $id_rayon .')');
		
		$messageStack->add_session('Ajout du rayon bien effectu�', 'success');
		tep_redirect(tep_href_link(FILENAME_RAYON));
  }
  
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/lightbox.css">
<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/javascript/scripts/prototype.js"></script>
<!--2020-02-13 -- <script language="javascript" src="includes/javascript/scripts/scriptaculous.js"></script>-->
<script language="javascript" src="includes/javascript/scripts/lightbox.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/function.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<table border="0" width="100%" cellspacing="2" cellpadding="2"> 
  <tr>
    <td width="100%" valign="top">
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
	  <!-- <tr><form name="myform2" action="<?php echo $PHP_SELF; ?>" method="get">
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0" class="smallText">
          	<tr align="center" style="background-color: #d8d8eb;" height="25">
                <td></td>
            </tr>
            </table>
            <table border="0" width="100%" cellspacing="0" cellpadding="0" class="smallText">
          	<tr align="center" style="background-color: #e1e1f5;" height="35">
            	<td></td>
                <td><input type=submit value="Go"></td>
            </tr>
        </table><br /></td>
      </form>
      </tr>-->
      <tr>
        <td>
		<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
          <tr> 
          <td valign="top"> 
          <table border="0" width="100%" cellspacing="0" cellpadding="2">
          	<tr class="dataTableHeadingRow">
			    <td class="dataTableHeadingContent">Nom</td>
			    <td class="dataTableHeadingContent">Status</td>
                <td class="dataTableHeadingContent" align="right">Action&nbsp;</td>
            </tr> 
<?php	
		$rayon_query_raw = "select rubrique_id, rubrique_name, rubrique_status from ".TABLE_RAYON."";
		$rayon_query_res = tep_db_query($rayon_query_raw);
		while ($rayon = tep_db_fetch_array($rayon_query_res)) {?>
			<tr class="dataTableRow" >
				<td class="dataTableContent"><?php echo $rayon['rubrique_name'];?></td> 
				<td class="dataTableContent">
				<?php 	if ($rayon['rubrique_status'] == '1') {
							echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_RAYON, 'action=setflag&flag=0&rID=' . $rayon['rubrique_id']) .'">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
		  				} 
						else {
							echo '<a href="' . tep_href_link(FILENAME_RAYON, 'action=setflag&flag=1&rID=' . $rayon['rubrique_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
		  				}?>
				</td>
              	<td class="dataTableContent" align="right">
				<?php
					echo '<a href="' . tep_href_link(FILENAME_RAYON_EDIT, tep_get_all_get_params(array('cID')) . 'rID=' . $rayon['rubrique_id']) . '&action=edit">' . tep_image(DIR_WS_IMAGES . 'icons/editer.png', IMAGE_ICON_INFO) . '</a>';  ?> 
					| <?php echo '<a href="' . tep_href_link(FILENAME_RAYON, tep_get_all_get_params(array('rID', 'action')) . 'cID=' . $rayon['rubrique_id'] . '&action=deleteconfirm') . '" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer  ce rayon DEFINITIVEMENT ?\')){return true;}else {return false;}">' . tep_image_button('supprimer.png', IMAGE_DELETE) . '</a>'; ?>	
				</td>
			</tr>
		<?php }//fin while rayons?>
          </table>
          
          </td>
          </tr>
    </table>
    	<form action="" style="display:block;margin-top:20px;text-align:right;" method="get">
        	<input type="hidden" name="action" value="new">
          	<input type="submit" value="Cr�er un nouveau Rayon">
          </form>
		</td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>