<?php
/*
  $Id: duplicate.php - Version 1 - 11/11/2006

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {
	   case 'confirm':
        if ( (isset($HTTP_POST_VARS['products_id']) && !empty($HTTP_POST_VARS['products_id']) && isset($HTTP_POST_VARS['categories_id'])) 
			|| (isset($HTTP_POST_VARS['id_article']) && !empty($HTTP_POST_VARS['id_article']) && isset($HTTP_POST_VARS['categories_id'])) ) {
			
		  $products_id = (isset($HTTP_POST_VARS['products_id'])) ? tep_db_prepare_input($HTTP_POST_VARS['products_id']) : tep_db_prepare_input($HTTP_POST_VARS['id_article']);
          $categories_id = tep_db_prepare_input($HTTP_POST_VARS['categories_id']);
		
		  if(!empty($HTTP_POST_VARS['verifCategorie'])) {
			  
				if($HTTP_POST_VARS['verifCategorie'][0] == true) $confirm_copy=1; else $confirm_copy = 0;
		  }
		  else $confirm_copy = 0;
		  
		  if(!empty($HTTP_POST_VARS['verifOption'])) {
			  
				if($HTTP_POST_VARS['verifOption'][0] == true) $verifOption=1; else $verifOption = 0;
		  }
		  else $verifOption = 0;
		  
			//echo 'Produit : '.$products_id.'<br />';
		  if(empty($products_id)) {
		  
		  	exit();
		  }
		  $_SESSION['message_log'] = "";
		  
	for($i = 0; $i <= sizeof($categories_id) ; $i ++) {
		$check_query = tep_db_query("
			select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " 
			where categories_id = '" . (int)$categories_id[$i] . "' LIMIT 1");
		while($check = tep_db_fetch_array($check_query)) { 
		     // choix de l'action � faire sur le produit pour chaque cat�gorie s�lectionn�e 
 		  	if ($HTTP_POST_VARS['typeOfAction'] == 'copy') {			  	
				$products_query = tep_db_query("select 1 from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$products_id . "' and categories_id = '" . (int)$categories_id[$i] . "' LIMIT 1");
              	if(tep_db_num_rows($products_query) >= 1) {
					$_SESSION['message_log'] .= '- '.$check['categories_name'].'  => <font color="red"><b>non copi�, existe d�j�</b></font><br />';
				} else {
					$_SESSION['message_log'] .= '- '.$check['categories_name'].'  => <i>copi�</i><br />';
					tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id . "', '" . (int)$categories_id[$i] . "')");
				}
			/*******************************************************************/				  
          } elseif ($HTTP_POST_VARS['typeOfAction'] == 'duplicate') {
			if(empty($product)) {
          		$product_query = tep_db_query("select products_quantity, products_model, products_bimage, products_price, products_cost, products_date_available, products_weight, products_tax_class_id, manufacturers_id from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "' order by products_model ");
          		$product = tep_db_fetch_array($product_query);
			}
			$copyEnCours = false;
			$products_query = tep_db_query("select 1 from " . TABLE_PRODUCTS_TO_CATEGORIES . " 
			where products_id = '" . (int)$products_id . "' and categories_id = '" . (int)$categories_id[$i] . "' 
			LIMIT 1");
            if(tep_db_num_rows($products_query) == 1) {
	
				if($confirm_copy==1) $_SESSION['message_log'] .= '- '.$check['categories_name'].'  => <i>non dupliqu�</i>';
				else {
					$_SESSION['message_log'] .= '- '.$check['categories_name'].' => <i>dupliqu�, attention produit en double</i>';
					$copyEnCours = true;
				}
			} else {
				$_SESSION['message_log'] .= '- '.$check['categories_name'].'  => <i>dupliqu�</i>';
				$copyEnCours = true;
			}
			if($copyEnCours) {
			
			// copie du produit
            tep_db_query("insert into " . TABLE_PRODUCTS . " (products_quantity, products_model, products_bimage, products_price, products_cost, products_date_added, products_date_available, products_weight, products_status, products_tax_class_id, manufacturers_id, family_id, family_client_id) values ('" . 			tep_db_input($product['products_quantity']) . "', '" . 
			tep_db_input($product['products_model']) . "', '" . 
			tep_db_input($product['products_bimage']) . "', '". 
			tep_db_input($product['products_price']) . "', '" . 
			tep_db_input($product['products_cost']) . "', now(), '" .
			tep_db_input($product['products_date_available']) . "', '" . 
			tep_db_input($product['products_weight']) . "', '0', '" . (int)$product['products_tax_class_id'] . "', '" . (int)$product['manufacturers_id'] . "', '" . (int)$product['family_id'] . "', '" . (int)$product['family_client_id'] . "')");
        	
            $dup_products_id = tep_db_insert_id();

			// copie des images
            $image_query = tep_db_query("select products_bimage from " . TABLE_PRODUCTS_IMAGE . " where products_id = '" . (int)$products_id . "'");
            while ($image = tep_db_fetch_array($image_query)) {
              tep_db_query("insert into " . TABLE_PRODUCTS_IMAGE . " (products_id, products_bimage) values ('" . (int)$dup_products_id . "', '" . tep_db_input($description['products_bimage']) . "')");
            }
			
			// copie des descriptions
            $description_query = tep_db_query("select language_id, products_name, products_description, products_url from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$products_id . "'");
            while ($description = tep_db_fetch_array($description_query)) {
              tep_db_query("insert into " . TABLE_PRODUCTS_DESCRIPTION . " (products_id, language_id, products_name, products_description, products_url, products_viewed) values ('" . (int)$dup_products_id . "', '" . (int)$description['language_id'] . "', '" . tep_db_input($description['products_name']) . "', '" . tep_db_input($description['products_description']) . "', '" . tep_db_input($description['products_url']) . "', '0')");
            }

			// insertion dans la cat�gorie
            tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$dup_products_id . "', '" . (int)$categories_id[$i] . "')");
			
			// ajout des options
			if($verifOption==1) {
			  	$option_query = tep_db_query('select * from ' . TABLE_PRODUCTS_ATTRIBUTES . ' WHERE products_id = '.$products_id);
				$_SESSION['message_log'] .= ' ... <b>'.tep_db_num_rows($option_query).'</b> option(s) dupliqu�e(s)';
           		while ($option = tep_db_fetch_array($option_query)) {
									
					$options_id = tep_db_prepare_input($option['options_id']);
					$options_values_id = tep_db_prepare_input($option['options_values_id']);
					$options_values_price = tep_db_prepare_input($option['options_values_price']);
					$price_prefix = tep_db_prepare_input($option['price_prefix']);
					$attributes_order = tep_db_prepare_input($option['attributes_order']);

        			tep_db_query("insert into " . TABLE_PRODUCTS_ATTRIBUTES . " ( `products_attributes_id` , `products_id` , `options_id` , `options_values_id` , `options_values_price` , `price_prefix` , `attributes_order` ) values ('', '" . (int)$dup_products_id . "', '" . (int)$options_id . "', '" . (int)$options_values_id . "', '" . $options_values_price . "', '" . $price_prefix . "', '" . (int)$attributes_order . "')");
		
				}  
			} else $_SESSION['message_log'] .= ' ... option(s) non dupliqu�e(s)';
			
			// fin du duplicate
            } // if($copyEnCours)
			$_SESSION['message_log'] .= '<br />';
          } // elseif ($HTTP_POST_VARS['typeOfAction'] == 'duplicate') 
  
        } // while($check = tep_db_fetch_array($check_query))
	} // for
			

	    if (USE_CACHE == 'true') {
            tep_reset_cache_block('categories');
            tep_reset_cache_block('also_purchased');
        }

		$_SESSION['confirm_copy'] = $confirm_copy;
		$_SESSION['verifOption'] = $verifOption;	 
		$_SESSION['typeOfAction'] = $HTTP_POST_VARS['typeOfAction'];
        tep_redirect(tep_href_link(FILENAME_DUPLICATE));
	}
    break;

    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/lib/jquery1.3.2.js"></script>
<script language="JavaScript" src="ajax/duplicate.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<div id="popupcalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Dupliquer un article</td>
          </tr>
        </table></td>
      </tr>
      <tr><td><form  <?php echo 'action="' . tep_href_link(FILENAME_DUPLICATE,'action=confirm', 'NONSSL') . '"'; ?> method="post" onSubmit="if(document.getElementById('products_id').value == 0) {
	  alert('Il faut choisir un produit !');
	  return false;
	  }
 if(document.getElementById('categories_id').value == 0) {
 		alert('Il faut choisir une ou plusieurs cat�gorie(s) !');
		return false;
	}
  ">
        
		
		<table border="0" cellspacing="0" cellpadding="5">
				
			<tr>
				<td colspan="2" class="main">
        <div id="ajax_chargement"></div><br />
				<?php
					if(!empty($_SESSION['message_log'])) {
						echo '<div style="border-style:ridge; border-color:#000000;border-width:1px;padding:5px">
						'.$_SESSION['message_log'].'</div>';
						$_SESSION['message_log'] = "";
					}
				?>
				</td>
			</tr>
          <tr>
		  	
            <td class="main">Selectionnez l'article :&nbsp;</td>
            <td class="main">
				<?php
				
				$cPath = explode('_', $_GET['cPath']);
				
				if (isset($_GET['pID']) && !empty($_GET['pID']) && sizeof($cPath)==2) {
					
					
					$marque = $cPath[0];
					$modele = $cPath[1];
					
					
					$rubrique = 'select rubrique_id from '. TABLE_CATEGORIES_RAYON .' where categories_id='. $marque;
					$rubrique = tep_db_query($rubrique);
					$rubrique = tep_db_fetch_array($rubrique);
					
					
					?>
					
                    <select name="rayon" id="rayon"><option value="0">Choisir un rayon</option>
                	<?php
					$reponse_rayon = tep_db_query("SELECT *
												   FROM rubrique
												   ORDER BY rubrique_id");
					while($donnees_rayon = tep_db_fetch_array($reponse_rayon)) {
						
						$selected = ($rubrique['rubrique_id']==$donnees_rayon['rubrique_id']) ? 'selected="selected"' : '';
						?>
						<option value="<?php echo $donnees_rayon['rubrique_id']; ?>" <?php echo $selected; ?>><?php echo $donnees_rayon['rubrique_name']; ?></option>
						<?php
					}
					
					?>
					</select>
                    
           			
					
					<!--echo tep_draw_products_specials_pull_down_specials('products_id', 'style="font-size:10px"', $specials_array);-->
					
					<select name="marque" id="marque">
                        <option value="">Choisissez une marque</option>
                        <?php
                        $reponse_marque = tep_db_query("SELECT cd.categories_name, cd.categories_id
													   FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id inner join categories_rubrique as cr on c.categories_id=cr.categories_id
													   WHERE c.parent_id='0' AND cr.rubrique_id = 1 and c.categories_status='1'
													   ORDER BY c.sort_order, cd.categories_name ASC");
                        while($donnees_marque = tep_db_fetch_array($reponse_marque)) {
							
							$selected = ($marque==$donnees_marque['categories_id']) ? 'selected="selected"' : '';
                            ?>
                            <option value="<?php echo $donnees_marque['categories_id']; ?>" <?php echo $selected;?>><?php echo $donnees_marque['categories_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
            
                    <select name="modele" id="modele">
                        <option value="">Choisissez un mod�le</option>
                        <?php
                        $reponse_modele = tep_db_query("SELECT cd.categories_name, cd.categories_id
													   FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id inner join categories_rubrique as cr on c.categories_id=cr.categories_id
													   WHERE c.parent_id='". $marque ."' AND cr.rubrique_id = 1 and c.categories_status='1'
													   ORDER BY c.sort_order, cd.categories_name ASC");
                        while($donnees_modele = tep_db_fetch_array($reponse_modele)) {
							
							$selected = ($modele==$donnees_modele['categories_id']) ? 'selected="selected"' : '';
                            ?>
                            <option value="<?php echo $donnees_modele['categories_id']; ?>" <?php echo $selected;?>><?php echo $donnees_modele['categories_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    
                    
                    <select name="products_id" id="products_id">
                        <option value="">Choisissez un article</option>
                        <?php
                        $reponse_article = tep_db_query("SELECT p.products_id, products_name
													   FROM ". TABLE_PRODUCTS ." as p, ". TABLE_PRODUCTS_DESCRIPTION ." pd, ". TABLE_PRODUCTS_TO_CATEGORIES ." ptc
													   WHERE pd.products_id=p.products_id and p.products_id=ptc.products_id and categories_id=". $modele ."
													   ORDER BY products_name ASC");
                        while($donnees_article = tep_db_fetch_array($reponse_article)) {
							
							$selected = ($_GET['pID']==$donnees_article['products_id']) ? 'selected="selected"' : '';
                            ?>
                            <option value="<?php echo $donnees_article['products_id']; ?>" <?php echo $selected;?>><?php echo $donnees_article['products_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
				<?php
				}
				elseif(isset($_GET['pID']) && !empty($_GET['pID'])) {
					
					$query = 'select products_name from '. TABLE_PRODUCTS_DESCRIPTION .' where products_id='. (int)$_GET['pID'];
					$query = tep_db_query($query);
					
					$data = tep_db_fetch_array($query);
					
					?>
					L'article <b><em><?php echo $data['products_name']; ?></em></b> sera dupliqu� dans les cat�gories suivantes
                    
                    <?php
				}
				else {
				?>
					
                    <select name="rayon" id="rayon">
                	<?php
					$reponse_rayon = tep_db_query("SELECT *
												   FROM rubrique
												   ORDER BY rubrique_id");
					while($donnees_rayon = tep_db_fetch_array($reponse_rayon)) {
						
						?>
						<option value="<?php echo $donnees_rayon['rubrique_id']; ?>"><?php echo $donnees_rayon['rubrique_name']; ?></option>
						<?php
					}
					
					?>
					</select>
                    
           			
					
					<!--echo tep_draw_products_specials_pull_down_specials('products_id', 'style="font-size:10px"', $specials_array);-->
					
					<select name="marque" id="marque">
                        <option value="">Choisissez une marque</option>
                        <?php
                        $reponse_marque = tep_db_query("SELECT cd.categories_name, cd.categories_id, cd.categories_url, cd.balise_title_lien_texte, categories_status_tous, categories_status_client, categories_status_rev, categories_status_pro, categories_status_adm, categories_status_vip
                                                         FROM categories as c inner join categories_description as cd on c.categories_id=cd.categories_id inner join categories_rubrique as cr on c.categories_id=cr.categories_id
                                                         WHERE c.parent_id='0' AND cr.rubrique_id = 1 and c.categories_status='1'
                                                         ORDER BY c.sort_order, cd.categories_name ASC");
                        while($donnees_marque = tep_db_fetch_array($reponse_marque)){
                            ?>
                            <option value="<?php echo $donnees_marque['categories_id']; ?>"><?php echo $donnees_marque['categories_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
            
                    <select name="modele" id="modele">
                        <option value="">Choisissez un mod�le</option>
                    </select>
                    
                    
                    <select name="products_id" id="products_id" disabled="disabled">
                        <option value="">Choisissez un article</option>
                    </select>
				<?php
                }
				?>
                
                <input type="hidden" id="id_article" name="id_article" value="<?php echo $_GET['pID']; ?>" />
				
			</td>
          </tr>
		  <tr>
		  	<td class="main">Selectionnez le mode :&nbsp;</td>
            <td class="main">
			  	Copier&nbsp;
  			  <input type="radio" name="typeOfAction" value="copy" checked="checked" />
  			  <!--  - Dupliquer&nbsp;
  			  <input type="radio" name="typeOfAction" value="duplicate" /> -->
			</td>
          </tr>
		  <tr>
		  	<td valign="top" class="main">Selectionnez les cat�gories :&nbsp;</td>
            <td class="main">
			 
				<?php 
				$category_tree_array = array();
				$category_tree_array[] = array('id' => 0, 'text' => 'S�l�ctionnez les cat�gories dans la liste');
				echo tep_draw_pull_down_menu('categories_id[]', tep_get_full_category_tree(0,'','',$category_tree_array ),0, ' size="40" multiple="multiple" id="categories_id"');
				
				?>
  			  
			</td>
          </tr>
		<!--<tr>
			<td valign="top" class="main">V�rifier si le produit existe dans <br />la ou les cat�gorie(s) s�lectionn�e(s) <br /> (pour le duplicate) :&nbsp;</td>
			<td class="main">
			<input type="hidden" name="verifCategorie" />
			<input type="checkbox" name="verifCategorie" /> 
			</td>
		</tr>
		<tr>
			<td valign="top" class="main">Dupliquer �galement les options<br /> (pour le duplicate) :&nbsp;</td>
			<td class="main">
			<input type="hidden" name="verifOption" />
			<input type="checkbox" name="verifOption" />
			</td>
		</tr> -->
		</table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
		    <td class="main" align="right" valign="top">
<?php echo tep_image_submit('button_insert.gif', IMAGE_INSERT) . '&nbsp;&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_DUPLICATE) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
		  <tr>
		  <tr>
            <td class="main"><br />
							<b>Informations :</b>
              <ul>
                <li>
                Pour seletionner plusieurs cat�gories, maintenez la touche CTRL appuy�e et cliquez pour selectionner.<br /><br />
                </li>
               <!-- <li>
                Si vous choisissez de "<b>Copier</b>" un article, il sera visible imm�diatement par vos visiteurs sur votre boutique.<br />
                Par cette fonction, vous ajouter un seul et m�me article dans diff�rentes cat�gories et si vous modifiez par exemple le titre il sera chang� dans tous les articles qui ont �t� copi�s<br /><br />
                </li>
                <li>Si vous choisissez de "<b>Dupliquer</b>" un article, il sera visible qu\'apr�s l\'avoir mit en service dans chaques cat�gories.
              	<br />
                Par cette fonction, vous ajoutez plusieurs nouveaux articles ind�pendants les uns des autres et c\'est pour cette raison qu\'il ne sont pas mis en ligne directement pour que vous puissiez les modifier avant de les afficher.
                </li> -->
              </ul>
            </td>
            
          </tr>
        </table></td>
      </form></tr>

      </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
