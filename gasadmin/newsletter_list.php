<?php
/* 2009/01/13 LL last modif by Ludo => refonte complete de la page order, pour la couper en 2*/
  require('includes/application_top.php');
  require('includes/functions/date.php');
  require('..'. BASE_DIR .'/includes/fonctions/mail.php');
  
  require('..'. BASE_DIR .'/includes/configure_mail.php');
  
  $action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
  
  if (isset($_GET['action']) && !empty($_GET['action']) && isset($_GET['newsletter']) && !empty($_GET['newsletter'])) {
	  
    switch ($action) {

	  case 'deleteconfirm':
        
		$query = 'delete from '. TABLE_NEWSLETTERS .' where newsletters_id='. $_GET['newsletter'];
		
		tep_db_query($query);
		
		$messageStack->add_session('La newsletter a ben �t� supprim�e', 'success');
		
        tep_redirect("newsletter_list.php");
		
        break;
	  
	  case 'send':
		
		$query = 'select * from '. TABLE_NEWSLETTERS .' where newsletters_id='. $_GET['newsletter'];
		$query = tep_db_query($query);
		
		$data = tep_db_fetch_array($query);
		
		email(MAIL_INFO, $data['newsletters_title'], parseMail($data['newsletters_id'], $data['newsletters_content']), $data['newsletters_id']);
		
		$messageStack->add_session('La newsletter a ben �t� envoy�e', 'success');
		
        tep_redirect("newsletter_list.php");
		
        break;
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title>Liste des Newsletters</title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></link>
<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/function.js"></script>
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/metadata.js"></script>
<script type="text/javascript">

$(document).ready(function() { 
     
    // call the tablesorter plugin
    $(".tablesorter").tablesorter();
});
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<div id="popupcalendar" class="text"></div>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr> 
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
	  <tr>
		<td>
          <table border="0" width="100%" cellspacing="0" cellpadding="0">
          	<tr>
                <td class="pageHeading">Liste des Newsletters</td>
            </tr>
            <tr>
          		<td valign="top">
                
					<table border="0" width="100%" cellspacing="0" cellpadding="2" class="tablesorter">
                    	<thead><tr>
                            <th>Titre</th>
                            <th width="55">Envoy�</th>
                            <th width="55">Lu</th>
                            <th width="80">Lu en ligne</th>
                            <th width="55">Cliqu�</th>
                            <th width="150">Auteur</th>
                            <th width="150">Type de Client</th>
                            <th width="150">Date de cr�ation</th>
                            <th width="200">Date d'envoi</th>
                            <th class="{sorter:false} actions">Action</th>
                    </tr></thead>
                    
                    <tbody>
<?php

	$query = "select * from ". TABLE_ADMIN ." , ". TABLE_NEWSLETTERS ." where user=admin_id order by date_envoi desc";
    $query = tep_db_query($query);
    while ($newsletter = tep_db_fetch_array($query)) {
		
		$type_id_client = explode(',', $newsletter['type_id_client']);
		$type_name_client = array();
		?>
        <tr>
        	<td><?php echo $newsletter['newsletters_title']; ?></td>
            
            <td style="text-align:center;"><?php echo $newsletter['mail_number_sent']; ?></td>
            <td style="text-align:center;"><?php echo $newsletter['mail_read']; ?></td>
            <td style="text-align:center;"><?php echo $newsletter['mail_read_online']; ?></td>
            <td style="text-align:center;"><?php echo $newsletter['mail_clicked']; ?></td>
            
            <td><?php echo $newsletter['admin_firstname'] ." ". $newsletter['admin_lastname']; ?></td>
            <td>
			<?php
			
			echo '<div style="text-align:center">'. $newsletter['type_client'] .' ';
			
			switch($newsletter['type_cmd']) {
				
            	case 'tous':
					echo 'Avec & Sans Commande';
					break;
				
				case 'avec':
					echo 'Avec commande';
					break;
				
				case 'sans':
					echo 'Sans commande';
					break;
			}
			
			echo '<br />';
			
			if(in_array('1', $type_id_client)) {
				
				$type_name_client[] = 'Part';
			}
			
			if(in_array('2', $type_id_client)) {
				
				$type_name_client[] = 'Pro';
			}
			
			if(in_array('3', $type_id_client)) {
				
				$type_name_client[] = 'Rev';
			}
			
			if(in_array('4', $type_id_client)) {
				
				$type_name_client[] = 'Admin';
			}
			
			echo '<b>{'. implode(' - ', $type_name_client) .'}</b>';
			
			echo '</div>';
			
			?></td>
            <td><?php 
			
				$date = explode(' ', $newsletter['date_creation']);
				echo date_fr($date[0]) .' � '. $date[1];
				
			?></td>
            <td style="text-align:center;">
			<?php
			if($newsletter['mail_sent'] && $newsletter['date_envoi']!='0000-00-00') {
				
				echo '<span style="color:#33CC33">envoy�e le '. date_fr($newsletter['date_envoi']) .'</span>';
			}
			elseif($newsletter['date_envoi']!='0000-00-00') {
				
				echo ($newsletter['date_envoi']<date('Y-m-d')) 
					? '<span style="font-weight:bold;color:#FF0000;text-decoration:underline;">non envoy�e le '. date_fr($newsletter['date_envoi']) .'</span>'
					: '<span>pr�vue le <b>'. date_fr($newsletter['date_envoi']) .'</b></span>';
			}
			else {
				
				echo '<span style="color:#FF0000">non pr�vu</span>';
			}
			?>
            </td>
            <td>
            <?php echo '<a href="newsletter_creation.php?newsletter='. $newsletter['newsletters_id'] .'&action=edit">'.tep_image(DIR_WS_IMAGES.'icons/editer.png', "Editer").'</a>';?>
            | 
            <?php // ICONE POUR SUPPRIMER
                
                echo '<a href="?newsletter='. $newsletter['newsletters_id'] .'&action=deleteconfirm" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer la newsletter : '.$newsletter['newsletters_title'].' ?\')){return true;} else {return false;}">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>';
                ?>
            | 
            <?php // ICONE POUR VISUALISER SUR LE SITE
                
                echo '<a href="'. WEBSITE . BASE_DIR .'/newsletter.php?news='. md5($newsletter['newsletters_id']) .'" target="_blank">' .  tep_image(DIR_WS_IMAGES . 'icons/visualiser.png', "Voir en ligne")  . '</a>';
                ?>
            | 
            <?php // ENVOYER LES MAILS DE TEST
                
                echo '<a href="?newsletter='. $newsletter['newsletters_id'] .'&action=send">' .  tep_image(DIR_WS_IMAGES . 'icons/icon_reclamaposte_reclamation.png', "Envoyer un Mail de Test")  . '</a>';
                ?>
            </td>
		</tr>
		<?php
	}?>
                    </tbody>
                </table>
                </td>
            </tr>
            </table></td>
          	</tr>
          </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?><br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>