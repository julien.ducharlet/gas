<?php
/* Page : article_edit_stocks_options.php */
	
require('includes/application_top.php');

/** AJAX Attribute Manager  **/ 
	require_once('attributeManager/includes/attributeManagerUpdateAtomic.inc.php'); 
/** AJAX Attribute Manager  end **/ 

if($_GET['action']=='maj_panier') {
	
	$query = 'delete from '. TABLE_CUSTOMERS_BASKET .' where products_id like \''. $_GET['pID'] .'_%\'';
	tep_db_query($query);
	$query = 'delete from '. TABLE_CUSTOMERS_BASKET_ATTRIBUTES .' where products_id like \''. $_GET['pID'] .'_%\'';
	tep_db_query($query);
	
	$messageStack->add_session('Ce produit n\'est plus pr�sent dans les paniers des clients', 'success');
	
	tep_redirect(tep_href_link('article_edit_stocks_options.php', 'cPath=' . $_GET['cPath'] . '&pID=' . $_GET['pID'] .'&action=new_product'));
}
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/lightbox.css">
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <script language="javascript" src="includes/general.js"></script>
        <script language="javascript" src="ajax/categories.js"></script>
        <!-- AJAX Attribute Manager  -->
        <?php require_once( 'attributeManager/includes/attributeManagerHeader.inc.php' )?>
        <!-- AJAX Attribute Manager  end -->
        
        <script src="includes/javascript/jQuery/jquery-1.4.js"></script>
		<script type="text/javascript">
        
        $(document).ready(function() {
            
            //�dition d'un produit
			$(".edit").click(function() {
				
				$("#overlay").show();
			
				$.ajax({
					type: "GET",
					url: 'includes/javascript/ajax/article_stock.php',
					
					data: 'action=select&products_id=<?php echo $_GET['pID']; ?>',
					dataType: "text",
					processData: false,
					success: function(data) {
						
						$("#lightbox").html(data);
						$("#lightbox").slideDown();
					}
				});
			});
			
			$("#lightbox #myForm").live('submit', function() {
				
				$.ajax({
					type: "POST",
					url: 'includes/javascript/ajax/article_stock.php?action=update&products_id=<?php echo $_GET['pID']; ?>',
					
					data: $(this).serialize(),
					dataType: "text",
					processData: false,
					success: function(data) {
						
						close_lightbox('lightbox');
						
						location.reload();
					}
				});
				
				return false;
			});
			
			$("#overlay").click(function() {
				
				close_lightbox('lightbox');
			});
			
			$('.ajaxZone').ajaxSend(function(evt, request, settings) {
				
				if(settings.data=='lightbox=no') {//pour les lightbox, pas de contr�le
				
					$("#overlay").show();
					
					$(this).show().html('<img alt="loading" src="images/loading.gif"> Envoi de la requ�te');
				}
			});
			
			
			$('.ajaxZone').ajaxSuccess(function(evt, request, settings) {
				
				if(settings.data=='lightbox=no') {//pour les lightbox, pas de contr�le
				
					$(this).html('Donn�es bien enregistr�es');
					$(this).fadeOut(1000, function() {
						
						$("#overlay").hide();
					});
				}
			});
			
			$(".ajaxZone").ajaxError(function(event, request, settings){
				
				if(settings.data=='lightbox=no') {//pour les lightbox, pas de contr�le
					
					$(this).html('Erreur lors de l\'appel � la page '+ settings.url);
				}
			});
			
		});
		
		function close_lightbox(box_id) {
			
			$("#"+ box_id).slideUp("slow").queue(function() {
														  
				$("#overlay").hide();
				$(this).dequeue();
			});
		}
		
		
		function globalUpdate(quantity, optionValueId) {
			
			var id_article = $('#id_article').val();
			var qte_reel = $('#quantity_reel_'+ optionValueId).val();
	
			$.ajax({
				
				type: "GET",
				cache: false,
				url: "attributeManager/javascript/liaison_stock_global.php?id_article="+id_article+"&optionValueId="+optionValueId+"&quantity="+quantity+"&quantity_reel="+ qte_reel,
				data: 'lightbox=no',
				dataType: "text",
				processData: false,
				success: function(data) {
					
					var str = data.split("_");
					
					$("#products_quantity_reel").val(str[0]);
					$("#products_quantity").val(str[1]);
				}
			});
		}
		
		function updateStock() {
			
			new_reel = $("#products_quantity_reel").val();
			old_reel = $("#products_quantity_reel_old").val();
			
			$("#products_quantity_reel_old").val(new_reel);
			
			stock = $("#products_quantity").val();
			
			$("#products_quantity").val(parseInt(stock) + parseInt(new_reel) - parseInt(old_reel));
		}
		
		function maj_stock_bdd(pID) {
			
			stock_reel = $("#products_quantity_reel").val();
			stock_virtuel = $("#products_quantity").val();
			
			$.ajax({
				type: "GET",
				cache: false,
				url: 'includes/article/article_stock_global_ajax.php?stock_reel='+ stock_reel +'&stock_virtuel='+ stock_virtuel +'&pID='+ pID,
				data: 'lightbox=no',
				dataType: "text",
				processData: false,
				success: function(data) {
					
				}
			});
		}
        </script>
    </head>
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="goOnLoad();">
    <input type="hidden" value="<?php echo $_GET['pID']; ?>" id="id_article" />
    	<?php
			$parameters = array('products_id' => '',
								'products_name' => '',
								'products_quantity' => '',
								'products_quantity_reel' => '',
								'products_price' => '',
								'products_cost' => '',
								'products_weight' => '',
								'products_date_added' => '',
								'products_last_modified' => '',
								'products_date_available' => '',
								'products_status' => '',
								'products_quantity_min' => '',
								'products_quantity_max' => '',
								'products_quantity_ideal' => '');
			
			$pInfo = new objectInfo($parameters);
			
			$tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
			$tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
			while ($tax_class = tep_db_fetch_array($tax_class_query)) {
				$tax_class_array[] = array('id' => $tax_class['tax_class_id'],
				'text' => $tax_class['tax_class_title']);
			}
			
			$languages = tep_get_languages();
			
			$product_query = tep_db_query("select pd.products_name, p.products_id, p.products_quantity, p.products_quantity_reel, p.products_model, p.products_ref_origine, p.products_garantie, p.products_code_barre, p.products_price, p.products_cost, p.products_weight, p.products_date_added, p.products_last_modified, date_format(p.products_date_available, '%Y-%m-%d') as products_date_available, p.products_status, p.products_tax_class_id, p.products_quantity_min, p.products_quantity_max, p.products_quantity_ideal from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$HTTP_GET_VARS['pID'] . "' AND p.products_id = pd.products_id");
			$product = tep_db_fetch_array($product_query);
			$pInfo->objectInfo($product);
			
			if (!isset($pInfo->products_status)) $pInfo->products_status = '1';
			
			switch ($pInfo->products_status) {
				case '0': $in_status = false; $out_status = true; break;
				case '1':
				default: $in_status = true; $out_status = false;
			}
		?>
        
        <script language="javascript">
			function updateStockOptions(optionValueId) {
				var new_reel = document.getElementById('quantity_reel_'+optionValueId).value;
				var old_reel = document.getElementById('quantity_reel_old_'+optionValueId).value;
				if (old_reel == '') {
					old_reel = 0;
				}
				document.getElementById('quantity_reel_old_'+optionValueId).value=new_reel;
				var stock = document.getElementById('quantity_naff_'+optionValueId).value;
				
				document.getElementById('quantity_naff_'+optionValueId).value = parseInt(stock) + parseInt(new_reel) - parseInt(old_reel);
				document.getElementById('quantity_'+optionValueId).value = parseInt(stock) + parseInt(new_reel) - parseInt(old_reel);
				
				//alert(new_reel+"-"+old_reel+"-"+stock)
			}
		</script>
        
        <?php
			require(DIR_WS_INCLUDES . 'header.php');
			require(DIR_WS_ARTICLE . 'article_menu.php');
			require(DIR_WS_ARTICLE . 'position_article.php');
			
			echo '<div style="text-align:center;"><a href="?cPath='. $_GET['cPath'] .'&pID='. $_GET['pID'] .'&action=maj_panier"><input type="button" value="Mise � jour panier"></a></div>';
		?>
        
        <span style="padding: 10px;" class="pageHeading">Ventes des 12 derniers mois</span>
        
        <?php require(DIR_WS_ARTICLE . 'article_ventes_sur_12_mois.php'); ?>
        
        <span style="padding: 10px;" class="pageHeading">Aper�u du stock de l'article</span>
        
		<?php
			$editable = true;
			require(DIR_WS_ARTICLE. 'article_stock_global.php');
		?>
                                            
        <?php 
			require_once( 'attributeManager/includes/attributeManagerPlaceHolder.inc.php' );
		?>
                            
        <div id="overlay"></div>
        <div id="lightbox"></div>
        
        <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
    </body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
