<?php
/*
  Page : cmd_bon_de_livraison.php
*/

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $oID = tep_db_prepare_input($HTTP_GET_VARS['oID']);
  $orders_query = tep_db_query("select * from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
  $resultat = tep_db_fetch_array($orders_query);
  
  
  include(DIR_WS_CLASSES . 'order.php');
  $order = new order($oID);
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- body_text //-->
<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td>
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td height="40" align="center" class="pageHeading">BON DE LIVRAISON <br \>
		<? echo 'Num�ro de commande : '.$oID ?> 
		&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; 
		<? echo 'Num�ro de client : '.$resultat['customers_id']; ?>
		<hr> 
 		</td>
      </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class="main"><b>Adresse du Client</b></td>
						</tr>
					
						<tr>
							<td class="main">
								<?php 
								$adresse_client = '';			
								if ($resultat['customers_company'] != '') $adresse_client .= $resultat['customers_company'].'<br>'; // Societe
								if ($resultat['customers_name'] != '') $adresse_client .= $resultat['customers_name'].'<br>'; // Nom + Pr�nom
								if ($resultat['customers_street_address_3'] != '') $adresse_client .= $resultat['customers_street_address_3'].'<br>'; //Adresse 1
								if ($resultat['customers_suburb'] != '') $adresse_client .= $resultat['customers_suburb'].'<br>'; //Adresse 2
								if ($resultat['customers_street_address'] != '') $adresse_client .= $resultat['customers_street_address'].'<br>'; //Adresse 3
								if ($resultat['customers_street_address_4'] != '') $adresse_client .= $resultat['customers_street_address_4'].'<br>'; //Adresse 4
								$adresse_client .= $resultat['customers_postcode'].' '.$resultat['customers_city'].'<br>'; // Code postal + ville
								$adresse_client .= $resultat['customers_country']; // Pays
								echo $adresse_client;
								?>
							</td>
						</tr>
						
						<tr>
							<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '5'); ?></td>
						</tr>
						<tr>
							<td class="main"><?php echo 'T�l�phone du client : ' . $resultat['customers_telephone']; ?></td>
						</tr>
						<tr>
							<td class="main"><?php echo '<a href="mailto:' . $resultat['customers_email_address'] . '"><u>' . $resultat['customers_email_address'] . '</u></a>'; ?></td>
						</tr>
					</table>
				</td>
				<td valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class="main"><b>Adresse de livraison</b></td>
						</tr>
						<tr>
							<td class="main">
								<?php 
								$adresse_livraison = '';

								if ($resultat['delivery_company'] != '') $adresse_livraison .= $resultat['delivery_company'].'<br>'; // Societe
								if ($resultat['delivery_name'] != '') $adresse_livraison .= $resultat['delivery_name'].'<br>'; // Nom + Pr�nom
								if ($resultat['delivery_street_address_3'] != '') $adresse_livraison .= $resultat['delivery_street_address_3'].'<br>'; //Adresse 1
								if ($resultat['delivery_suburb'] != '') $adresse_livraison .= $resultat['delivery_suburb'].'<br>'; //Adresse 2
								if ($resultat['delivery_street_address'] != '') $adresse_livraison .= $resultat['delivery_street_address'].'<br>'; //Adresse 3
								if ($resultat['delivery_street_address_4'] != '') $adresse_livraison .= $resultat['delivery_street_address_4'].'<br>'; //Adresse 4
								$adresse_livraison .= $resultat['delivery_postcode'] . ' ' . $resultat['delivery_city'] . '<br>'; // Code postal + ville
								$adresse_livraison .= $resultat['delivery_country']; // Pays
								echo $adresse_livraison;
								?>			
							</td>
						</tr>
					</table>
				</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
  </tr>
  <tr>
    <td><table border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td class="main"><b>Pay� par : </b></td>
        <td class="main"><?php echo $order->info['payment_method']; ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
  </tr>
  <tr>
    <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr class="dataTableHeadingRow">
        <td class="dataTableHeadingContent" colspan="2">Designation</td>
        <td class="dataTableHeadingContent">R�f�rence</td>
      </tr>
<?php
    for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
		
		$query = 'select products_ref_origine from '. TABLE_PRODUCTS .' where products_id='. $order->products[$i]['id'];
		$query = tep_db_query($query);
		if(tep_db_num_rows($query)>0) {
			
			$data = tep_db_fetch_array($query);
			
			if(empty($data['products_ref_origine'])) $data['products_ref_origine'] = $order->products[$i]['model'];
		}
		else $data['products_ref_origine'] = '###'. $order->products[$i]['model'];
		
      echo '      <tr class="dataTableRow">' . "\n" .
           '        <td class="dataTableContent" valign="top" align="right">' . $order->products[$i]['qty'] . '&nbsp;x</td>' . "\n" .
           '        <td class="dataTableContent" valign="top">' . $order->products[$i]['name'];

      if (isset($order->products[$i]['attributes']) && (sizeof($order->products[$i]['attributes']) > 0)) {
        for ($j=0, $k=sizeof($order->products[$i]['attributes']); $j<$k; $j++) {
          echo '<br><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
          echo '</i></small></nobr>';
        }
      }

      echo '        </td>' . "\n" .
           '        <td class="dataTableContent" valign="top">' . $data['products_ref_origine'] . '</td>' . "\n" .
           '      </tr>' . "\n";
    }
?>
    </table></td>
  </tr>
</table>
<!-- body_text_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
