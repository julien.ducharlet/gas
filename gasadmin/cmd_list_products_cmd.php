<?php
require('includes/application_top.php');
require('../' . DIR_WS_SITE . '/includes/fonctions/mail.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
	<title><?php echo TITLE; ?></title>
	<link href="includes/stylesheet.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
	<script language="javascript">	
		$("input[type='text']").click(function() {
			/*
			products_orders_id = $(this).atrr('name').split('_');
			products_orders_id = products_orders_id[1];
			*/
			//alert('test');
		});	
	</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">

<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
<div>
<?php

if(isset($_POST) && !empty($_POST)) {
	
	//mise � jour des quantit�s
	foreach($_REQUEST as $name => $val) {
		
		if(strpos($name, "ordersproducts")!==false) {
			
			$tab_name = explode("_",$name);
			$query_order_products = "update orders_products
									 set products_quantity_sent=products_quantity_sent+'".$val."', products_quantity_saisie='".$val."'
									 where orders_products_id='".$tab_name[1]."'";
			tep_db_query($query_order_products);
		}
	}
	
	$cmd = str_replace(' ', "','", ($id_commandes));
	$cmd = "'".$cmd."'";
	$commandes_a_envoyer = array();
	$orders_query = tep_db_query("select orders_id, SUM(products_quantity_sent) as somme from ".TABLE_ORDERS_PRODUCTS." op WHERE orders_id in (" .$cmd. ") GROUP BY orders_id");
	
	while($tuple = tep_db_fetch_array($orders_query)) {
		
		if ($tuple['somme'] == 0) {
			
			/*$message_query = tep_db_query("SELECT orders_status_comments FROM " . TABLE_ORDERS_STATUS . " WHERE orders_status_id = '15'");
			$message = tep_db_fetch_array($message_query);
			$message = str_replace("-- ", "\n", $message['orders_status_comments']);*/
			$message = 'Nous vous informons que avons un probl�me de rupture sur le ou les articles que vous avez command�.<br />
Nous attendons la livraison de notre fournisseur qui devrait se faire dans les jours � venir.<br />
Nous vous tenons de toutes fa�ons inform� de l\'envoi de votre commande.';
			
			$email_query = tep_db_query("SELECT customers_email_address, customers_name, origine, date_purchased FROM " . TABLE_ORDERS . " WHERE orders_id = '" . $tuple['orders_id'] . "'");
			$email_infos = tep_db_fetch_array($email_query);
			
			$email= 'Bonjour <span style="font-weight: bold; color: rgb(170, 180, 29);">'.$email_infos['customers_name'].'</span>,<br /><br />Num�ro de commande :' . ' <span style="font-weight:bold;color:#fe5700;">' . $tuple['orders_id'] . "</span><br /><br />" . 'Date de commande :' . ' <span style="font-weight:bold;color: rgb(170, 180, 29);">' . tep_date_long($email_infos['date_purchased']) . "</span><br /><br />" . $message . "<br /><br />" . SIGNATURE_MAIL;
			
			mail_client_commande($email_infos['customers_email_address'], SUJET_INFORMATION_CMD, $email);
			
			tep_db_query("UPDATE " . TABLE_ORDERS . " SET orders_status = 15 WHERE orders_id = '" . $tuple['orders_id'] . "'");
			$maj_orders_status_history = "INSERT INTO " . TABLE_ORDERS_STATUS_HISTORY . " (orders_id, orders_status_id, date_added, customer_notified, comments) VALUES ('" . $tuple['orders_id'] . "', '15', '" . date('Y-m-d H:i:s') . "', '1', '" . addslashes($message) . "')";
			tep_db_query($maj_orders_status_history);
			?>
			&nbsp;-&nbsp;Commande n&deg;<?php echo $tuple['orders_id']; ?> a &eacute;t&eacute; pass�e en rupture de stock<br /><br />
			<?php 
		} else {	
			$commandes_a_envoyer[] = $tuple['orders_id'];
		}
	}
	
	if(sizeof($_POST['orders_products'])>0) {
		
		foreach($_POST['orders_products'] as $orders_products_id => $checked) {
			
			//dans le cas d'une rupture pour un produit d'une commande, on met � jour le champ products_rupture dans la table ORDERS_PRODUCTS
			if($checked == 'on') {
				
				$query = 'update '. TABLE_ORDERS_PRODUCTS .' set products_rupture=1 where orders_products_id='. $orders_products_id;
				tep_db_query($query);
			}
		}
	}
	
	$_SESSION['commandes'] = $commandes_a_envoyer;
}
?>

</div>

<table border=0 width=100%>
    <tr>
        <td align=center class="dataTableHeadingRow"><b>Type client</b></td>
		<td align=center class="dataTableHeadingRow"><b>Pays</b></td>
        <td align=center class="dataTableHeadingRow"><b>ID commande</b></td>
        <td align=center class="dataTableHeadingRow"><b>Nom article</b></td>
        <td align=center class="dataTableHeadingRow"><b>Mod�le</b></td>
        <td align=center class="dataTableHeadingRow"><b>Exp�di�<br />ce jour</b></td>
        <td align=center class="dataTableHeadingRow"><b>Rupture</b></td>
        <td align=center class="dataTableHeadingRow"><b>Stock r�el</b></td>
    </tr>
	<?php	
		$last=0;
		if (isset($HTTP_GET_VARS['commandes']) && strlen($HTTP_GET_VARS['commandes'])>0){
			$id_commandes=substr(ltrim($HTTP_GET_VARS['commandes']),0,-1);
			$cmd = str_replace(' ', "','", ($id_commandes));
			$cmd= "'".$cmd."'";
			$query_orders = 'select o.orders_id, o.delivery_country, o.customers_id, p.products_id, products_quantity_reel as stock, orders_products_id, origine, products_name, op.products_model, op.products_quantity as order_qty, op.products_quantity_sent as order_qty_sent
							from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS  .' op, '. TABLE_PRODUCTS .' p
							where o.orders_id=op.orders_id
							and p.products_id=op.products_id
							and o.orders_id in ('. $cmd .')';
			if($_REQUEST["export"]=="fournisseur"){
				$query_orders.=" order by products_model";
			} else{
				$query_orders.=" order by orders_id, stock";
			}
			
			$orders_query = tep_db_query($query_orders);
			
			while($tuple = tep_db_fetch_array($orders_query)) {
				
				$qte_produit = $tuple['order_qty'] - $tuple['order_qty_sent'];
				
				if($qte_produit != 0) {
					
					$nom_produit = $tuple['products_name'];
					
					$select_attribute = 'select products_options_values as options, options_values_id
											from '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES .' opa
											where orders_products_id='.$tuple['orders_products_id'];
					$rep_attribute = tep_db_query($select_attribute);
					
					if(tep_db_num_rows($rep_attribute)>0) {
						
						$arr_attribute = tep_db_fetch_array($rep_attribute);
						
						$query = 'select options_quantity from '. TABLE_PRODUCTS_ATTRIBUTES .' where products_id='. $tuple['products_id'] .' and options_values_id='. $arr_attribute['options_values_id'];
						$query = tep_db_query($query);
						$data = tep_db_fetch_array($query);
						
						$stock_reel = $data['options_quantity'];
						
						$nom_produit .= ' <br /><span style="color:#FF0000; font-size:12px;"><b>'.$arr_attribute['options'].'</b></span>'; 
					}
					else $stock_reel = $tuple['stock'];
					
					$bgcolor = "#ffffff";
					
					if($qte_produit > 1) $bgcolor = '#dfdfdf';
					
					if($tuple['orders_id']!=$last && $_REQUEST["export"]!="fournisseur") { 
					
						echo '<tr><td colspan="10" height="2" bgcolor="#000000"></td></tr>'; 
						$last=$tuple['orders_id']; 
					}
					//$cmd 
					// Code pour mettre des icones en fontion du TYPE de client
					$customers_type_query = tep_db_query("select customers_type from " . TABLE_CUSTOMERS . " where customers_id = '" . $tuple['customers_id'] . "'");
					$row_client = mysql_fetch_array($customers_type_query);	
					
					echo '<tr>
						<td align=center width=5%>';
						  
							// Code pour mettre des icones en fontion du TYPE de client
							if($row_client['customers_type'] == '1') {
								echo '<img src="images/icons/icone_particulier.gif" alt="Client Particulier" title="Client Particulier">';
							} else if($row_client['customers_type'] == '2') {
								echo '<img src="images/icons/icone_professionnel.gif" alt="Client Professionnel" title="Client Professionnel">';
							} else if($row_client['customers_type'] == '3') {
								echo '<img src="images/icons/icone_revendeur.gif" alt="Client Revendeur" title="Client Revendeur">';
							} else if($row_client['customers_type'] == '4') {
								echo '<img src="images/icons/icone_administration.gif" alt="Client Administration" title="Client Administration">';
							}						  	
						  
					echo '</td>
						  
						  <td align=center width=3%>' . get_drapeau($tuple['delivery_country']) . '</td>
						  
						  <td align=center width=8% class="main"><a href="'.tep_href_link(FILENAME_CMD_EDIT, 'page=1&oID='.$tuple['orders_id'].'&action=edit').'" TARGET="_self">'.$tuple['orders_id'].'</a></td>					
						  <td align=center width=50%>'.$nom_produit.'</td>
						  <td align=center width=25% bgcolor="'.$bgcolor.'"><b>'.$tuple['products_model'].$info_qte_produit.'</b></td>
						  <td align=center width=7%><b><input id="'.$tuple['orders_products_id'].'" name="ordersproducts_'.$tuple['orders_products_id'].'" size="3" type="text" value="'.$qte_produit.'" /></b></td>
						  <td align="center"><input type="checkbox" name="orders_products['. $tuple['orders_products_id'] .']"></td>
						  <td align=center width=6%><b>'. $stock_reel .'</b></td>
						  </tr>';
				}
			}
			?>
			<tr>
        		<td colspan="10" height="30" align="right" valign="bottom"><?php echo tep_image_submit('button_update.gif', IMAGE_UPDATE); ?></td>
    		</tr>
		<?php	
		} else {
			echo "<tr><td colspan='9' align=center><b>Aucune commande</b></td></tr>";
			?>
            <tr>
        		<td colspan="10" height="30" align="right" valign="bottom"><input type="button" value="Exporter Vers Expeditor" onClick="javascript:location.href='OSC_Expeditor_process.php'" /></td>
    		</tr>
            <?php
		}?>
</table>
<input type="hidden" name="id_commandes" value="<?php echo $id_commandes; ?>" />
</form>
</body>
</html>