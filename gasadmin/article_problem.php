<?php
require('includes/application_top.php');


if (isset($_GET['action']) && !empty($_GET['action']) && isset($_GET['pID']) && !empty($_GET['pID'])) {
	
	switch ($action) {
		
		case 'init':
			
			$product = new product($_GET['pID']);
			
			$productsRegistered = $product->getAllBroken();
			
			$query = 'select sum( products_quantity - products_quantity_sent ) AS total_to_send
								from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op
								where o.orders_id=op.orders_id
								and products_quantity_sent<products_quantity
								and products_id='. $_GET['pID'] .'
								and orders_status!=21
								group by products_id';
			$query = tep_db_query($query);
			
			
			if(tep_db_num_rows($query)>0) {
				
				$data = tep_db_fetch_array($query);
				$total_ordered = $data['total_to_send'];
			}
			else $total_ordered = 0;
			
			
			$query = 'update '. TABLE_PRODUCTS .' set products_quantity=(products_quantity_reel-'. $total_ordered .')
					  where products_id='. (int)$_GET['pID'];
			
			if(tep_db_query($query)) {
				
				//on r�cup�re la cl� du produit dans le tableau des produits probl�mes provenant du fichier de stockage
				$productKey = array_search($_GET['pID'], $productsRegistered);
				unset($productsRegistered[$productKey]);
				
				$product->setBroken(implode(' ', $productsRegistered));
			}
			
			tep_redirect('article_problem.php');
		
			break;
	}
}
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<title><?php echo TITLE; ?></title>
<?php include('includes/meta.php'); ?>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script type="text/javascript">

$(document).ready(function() {
    
	<?php //include ('includes/headers/menuJS.php'); ?>
	
    // call the tablesorter plugin
    $(".tablesorter").tablesorter();
	
	$(".checkAll").change(function() {
		
		classToCheck = $(this).val();
		
		if($(this).attr("checked")) {
			
			$("."+ classToCheck).attr("checked", true);
			$(".checkAll").attr("checked", true);
		}
		else {
			
			$("."+ classToCheck).attr("checked", false);
			$(".checkAll").attr("checked", false);
		}
	});
	
	$("#generalAction").change(function() {
		
		data = '';
		alertToDelete = $("input:checkbox").serialize().split('&');
		
		for(i=0 ; i<alertToDelete.length ; i ++) {
			
			temp = alertToDelete[i].split('=');
			data += temp[1].substring(6) + ' ';
		}
		
		if(window.confirm("�tes-vous s�r de vouloir supprimer cette s�lection ?")) document.location.href='?pID='+ data +'&action=deleteconfirm<?php if(isset($_GET['date'])) echo '&date='. $_GET['date']; ?>';
	});
});
</script>
</head>
<body>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
	<br>
	
    <?php
	// TEST THIERRY pour ID37 dans les statut des commandes 
	/*
	
	echo '<div>';
	$query = tep_db_query('SELECT date_purchased FROM orders WHERE orders_id = 58365');
	// 58317
	$reponse = tep_db_fetch_array($query);
	
	$date_cmd = $reponse['date_purchased'];
	$date_paiement = strtotime(date("Y-m-d H:i:s"));
	
	echo 'Date commande : ' . $date_cmd;
	echo '<br>';
	echo 'Date paiement : ' . $date_paiement;
	echo '<br>';
	$difference_cmd_paiement = $date_paiement - strtotime($date_cmd);
	echo $difference_cmd_paiement;
	echo '<br>';
	
	if ($difference_cmd_paiement >= 2000) {
		echo 'sup�rieur a 2000 secondes';
	}
	
	echo '</div><br>'; */
	
	?>
	
    <div class="pageHeading">Articles ayant un stock virtuel sup�rieur au stock r�el</div>
        
	<table border="0" width="100%" cellspacing="2" cellpadding="2">
      </tr>
      <tr>
      	<td>
			<?php
			
			$productsRegistered = array();
			$productsList = array();
			
			$product = new product();
			
			$productsRegistered = $product->getAllBroken();
			
			$query = 'select *
					  from '. TABLE_PRODUCTS .' p,
					  '. TABLE_PRODUCTS_DESCRIPTION .' pd
					  where p.products_id=pd.products_id
					  and products_quantity>products_quantity_reel
					  order by products_model';
			$query = tep_db_query($query);
			?>
          
		  <table class="tablesorter">
                	<thead>
						<tr>
                    		<th class="{sorter:false}" style="width:20px;">Zone</th>
                    		<th align="center">R�f�rence</th>
                    		<th align="center">Nom</th>
                    		<th align="center" style="width:90px;">Qt� R�el</th>
                            <th align="center" style="width:90px;">Qt� Virtuel</th>
                            <th align="center" class="actions">Actions</th>
                		</tr>
					</thead>
                    <tbody>
			<?php
				while($data = tep_db_fetch_array($query)) {
					
					$productsList[] = $data['products_id'];
					
					$query_options = 'select count(*) as total from '. TABLE_PRODUCTS_ATTRIBUTES .' where products_id='. $data['products_id'];
					$query_options = tep_db_query($query_options);
					
					$data_options = tep_db_fetch_array($query_options);
					
					$link = tep_href_link(FILENAME_ARTICLE_EDIT_STOCKS_OPTIONS, 'pID=' . $data['products_id'] . '&action=new_product');
					
					echo '<tr>';
						echo '<td>'. $data['products_zone'] .'</td>';
						echo '<td><a href="'. $link .'" target="_blank">'. $data['products_model']. '</a></td>';
						echo '<td>';
							echo '<a href="'. $link .'" target="_blank">'. bda_product_name_transform($data['products_name']) .'</a>';
							
							if($data_options['total']>0) echo '<br/><a style="color:#7F7F7F;font-size:12px;font-weight:bold;">Avec options</a>';
						
						echo '</td>';
						echo '<td align="center">'. $data['products_quantity_reel']. '</td>';
						echo '<td align="center">'. ( ($data['products_quantity']<=0) ? '<a style="color:#FF0000;font-weight:bold;">' : '<a>') . $data['products_quantity'] .'</td>';
						echo '<td>';
							if($data_options['total']==0) echo '<a href="?pID='. $data['products_id'] .'&action=init"><input type="button" value="R�initialiser"></a>';
							echo '<a href="' . $link . '" target="_blank">' . tep_image(DIR_WS_IMAGES . '/icons/editer.png', "Editer") . '</a>&nbsp;&nbsp;';
							echo '<a onclick="if (window.confirm(\'Etes vous sur de vouloir effacer cette alerte ?\')){return true;} else {return false;}" href="?pID='. $data['products_id'] .'&action=deleteconfirm">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>';
						echo '</td>';
					echo '</tr>';
            }
			?>
            </tbody>
            </table>
            
            <div class="pageHeading">Articles ayant un stock d'options diff�rent du stock du produit</div>
            <?php
			$query = 'select p.products_id, p.products_model, pd.products_name, products_quantity_reel, products_quantity, sum(options_quantity_reel) as options_quantity_reel, sum(options_quantity) as options_quantity
					  from '. TABLE_PRODUCTS .' p,
					  '. TABLE_PRODUCTS_DESCRIPTION .' pd,
					  '. TABLE_PRODUCTS_ATTRIBUTES .' pa
					  
					  where p.products_id=pd.products_id
					  and pd.products_id=pa.products_id
					  
					  group by p.products_id
					  having (products_quantity_reel!=options_quantity_reel OR products_quantity!=options_quantity)
					  
					  order by products_model';
			$query = tep_db_query($query);
			?>
          
		  <table class="tablesorter">
                	<thead>
						<tr>
                    		<th class="{sorter:false}" style="width:20px;">Zone</th>
                    		<th align="center">R�f�rence</th>
                    		<th align="center">Nom</th>
                    		<th align="center" style="width:90px;">Qt� R�el</th>
                            <th align="center" style="width:90px;">Qt� Opt R�el</th>
                            <th align="center" style="width:90px;">Qt� Virtuel</th>
                            <th align="center" style="width:90px;">Qt� Opt Virtuel</th>
                            <th align="center" class="actions">Actions</th>
                		</tr>
					</thead>
                    <tbody>
			<?php
				while($data = tep_db_fetch_array($query)) {
					
					$productsList[] = $data['products_id'];
					
					$link = tep_href_link(FILENAME_ARTICLE_EDIT_STOCKS_OPTIONS, 'pID=' . $data['products_id'] . '&action=new_product');
					
					echo '<tr>';
						echo '<td>'. $data['products_zone'] .'</td>';
						echo '<td><a href="'. $link .'" target="_blank">'. $data['products_model']. '</a></td>';
						echo '<td>';
							echo '<a href="'. $link .'" target="_blank">'. bda_product_name_transform($data['products_name']) .'</a>';
							
						
						echo '</td>';
						echo '<td align="center">'. $data['products_quantity_reel']. '</td>';
						echo '<td align="center">'. $data['options_quantity_reel']. '</td>';
						echo '<td align="center">'. ( ($data['products_quantity']<=0) ? '<a style="color:#FF0000;font-weight:bold;">' : '<a>') . $data['products_quantity'] .'</td>';
						echo '<td align="center">'. $data['options_quantity']. '</td>';
						echo '<td>';
							//echo '<a href="?pID='. $data['products_id'] .'&action=init"><input type="button" value="R�initialiser"></a>';
							echo '<a href="' . $link . '" target="_blank">' . tep_image(DIR_WS_IMAGES . '/icons/editer.png', "Editer") . '</a>&nbsp;&nbsp;';
							echo '<a onclick="if (window.confirm(\'Etes vous sur de vouloir effacer cette alerte ?\')){return true;} else {return false;}" href="?pID='. $data['products_id'] .'&action=deleteconfirm">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>';
						echo '</td>';
					echo '</tr>';
            }
			?>
            </tbody>
            </table>
            
            <?php
			
			$productsRegistered = array_diff($productsRegistered, $productsList);
			
			?>
          
          <div class="pageHeading">Articles dont la quantit� command�e ne correspond pas � la diff�rence entre quantit� r�elle et virtuelle</div>
          <div style="color:#FF0000;font-size:12px;font-weight:bold;text-align:center;">V�rifier le stock � la main avant de r�initialiser</div>
		  <table class="tablesorter">
                	<thead>
						<tr>
                    		<th class="{sorter:false}" style="width:20px;">Zone</th>
                    		<th align="center">R�f�rence</th>
                    		<th align="center">Nom</th>
                    		<th align="center" style="width:90px;">Qt� R�el</th>
                            <th align="center" style="width:90px;">Qt� Virtuel</th>
                            <th align="center" style="width:90px;">Qt� � envoyer</th>
                            <th align="center" class="actions">Actions</th>
                		</tr>
					</thead>
                    <tbody>
			<?php
				
				foreach($productsRegistered as $prod) {
					
					if(!empty($prod)) {
						
						$query = 'select *
						  from '. TABLE_PRODUCTS .' p,
						  '. TABLE_PRODUCTS_DESCRIPTION .' pd
						  where p.products_id=pd.products_id
						  and p.products_id='. $prod;
						$query = tep_db_query($query);
						$data = tep_db_fetch_array($query);
						
						
						$query_count = 'select sum( products_quantity - products_quantity_sent ) AS total_to_send, op.*
									from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op
									where o.orders_id=op.orders_id
									and products_quantity_sent<products_quantity
									and products_id='. $prod .'
									and orders_status!=21
									group by products_id
									order by orders_id desc';
						$query_count = tep_db_query($query_count);
						if(tep_db_num_rows($query_count)>0) {
							
							$data_ordered = tep_db_fetch_array($query_count);
							$total_ordered = $data_ordered['total_to_send'];
						}
						else $total_ordered = 0;
						
						
						
						
						$productsList[] = $data['products_id'];
						
						$query_options = 'select count(*) as total from '. TABLE_PRODUCTS_ATTRIBUTES .' where products_id='. $data['products_id'];
						$query_options = tep_db_query($query_options);
						
						$data_options = tep_db_fetch_array($query_options);
					
						$link = tep_href_link(FILENAME_ARTICLE_EDIT_STOCKS_OPTIONS, 'pID=' . $data['products_id'] . '&action=new_product');
						
							
						echo '<tr>';
							echo '<td>'. $data['products_zone'] .'</td>';
							echo '<td><a href="'. $link .'" target="_blank">'. $data['products_model']. '</a></td>';
							echo '<td>';
								echo '<a href="'. $link .'" target="_blank">'. bda_product_name_transform($data['products_name']) .'</a>';
								
								if($data_options['total']>0) {
									
									echo '<br/><a style="color:#FF0000;font-size:12px;font-weight:bold;">Avec Options</a>';
								}
							
							echo '</td>';
							echo '<td align="center">'. $data['products_quantity_reel']. '</td>';
							echo '<td align="center">'. ( ($data['products_quantity']<=0) ? '<a style="color:#FF0000;font-weight:bold;">' : '<a>') . $data['products_quantity'] .'</td>';
							echo '<td>'. $total_ordered .'</td>';
							echo '<td>';
								
								echo '<a href="?pID='. $data['products_id'] .'&action=init"><input type="button" value="R�initialiser"></a>';
								echo '<a href="' . $link . '" target="_blank">' . tep_image(DIR_WS_IMAGES . '/icons/editer.png', "Editer") . '</a>&nbsp;&nbsp;';
								echo '<a onclick="if (window.confirm(\'Etes vous sur de vouloir effacer cette alerte ?\')){return true;} else {return false;}" href="?pID='. $data['products_id'] .'&action=deleteconfirm">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>';
							echo '</td>';
						echo '</tr>';
					}
				}
			?>
            </tbody>
          </table>            
      	</td>
      </tr>
    </table>
	</td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>