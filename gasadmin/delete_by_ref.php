<?php
/*
  $Id: duplicate.php - Version 1 - 11/11/2006

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

 
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="JavaScript" src="ajax/duplicate.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<div id="popupcalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Effacer une r�f�rence</td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table>
		</td>
       </tr>
	   
	   <tr>
	    <td>
	   		<? 	
		    	//$connex=mysql_connect("localhost","root","");
				//$db=mysql_select_db("mobilepro");
				
				$requete_family="SELECT * "
								   ."FROM family "
								   ."ORDER by family_name";
				$reponse_family=mysql_query($requete_family) or die('erreur'); $i=0;
				while($fetch_family=mysql_fetch_object($reponse_family))
					{
						$liste_family[$i]['id']=$fetch_family->family_id;
						$liste_family[$i]['nom']=$fetch_family->family_name;
						$i++;
					}
					
					
				echo '<form method="post" action="delete_by_ref.php">'.
					 '<select name="references">';
				
				for ($i=0;$i<=count($liste_family)-1;$i++)
					{
						echo '<option>---'.$liste_family[$i]['nom'].'</option>';
						$requete='SELECT p.products_model as salut '
								    .'FROM products p, family f '
									.'WHERE f.family_name="'.$liste_family[$i]['nom']
									.'" AND f.family_id=p.family_id '
									.'GROUP BY p.products_model';
							
							$reponse=mysql_query($requete);
							while($fetch=mysql_fetch_object($reponse))
								{ echo '<option value='.$fetch->salut.'>'.$fetch->salut.'</option'; }
						echo '<option>--------------------</option>';
					}
					
				echo '</select>
				<input type="submit" value="EFFACER" name="ok" />
				</form>';
				
				if ($ok)
					{
						$requete_suppression1='delete from products_to_categories where products_id IN ( select products_id
																							  from products
																							  where products_model = "'.$references.'"  ) ;';
						$requete_suppression2='delete from specials where products_id IN ( select products_id
																							  from products
																							  where products_model = "'.$references.'"  ) ;';
						$requete_suppression3='delete from products_description where products_id IN ( select products_id
																							  from products
																							  where products_model = "'.$references.'"  ) ; ';
						$requete_suppression4='delete from products_attributes where products_id IN ( select products_id
																							  from products
																							  where products_model = "'.$references.'"  ) ; ';
						$requete_suppression5='delete from customers_basket where products_id IN ( select products_id
																							  from products
																							  where products_model = "'.$references.'"  ) ; ';
						$requete_suppression6='delete from customers_basket_attributes where products_id IN ( select products_id
																							  from products
																							  where products_model = "'.$references.'"  ) ; ';
						$requete_suppression7='delete from reviews_description where reviews_id IN ( select r.reviews_id
																							  from products p,reviews r
																							  where p.products_model = "'.$references.'" 
																							  and p.products_id = r.products_id  ) ; ';
						$requete_suppression8='delete from reviews where products_id IN ( select products_id
																							  from products
																							  where products_model = "'.$references.'" ) ; ';
						$requete_suppression9='delete from products where products_model = "'.$references.'" ; ';
						
								$reponse_suppression1=mysql_query($requete_suppression1) or die('erreur1');
								$reponse_suppression2=mysql_query($requete_suppression2) or die('erreur2');
								$reponse_suppression3=mysql_query($requete_suppression3) or die('erreur3');
								$reponse_suppression4=mysql_query($requete_suppression4) or die('erreur4');
								$reponse_suppression5=mysql_query($requete_suppression5) or die('erreur5');
								$reponse_suppression6=mysql_query($requete_suppression6) or die('erreur6');
								$reponse_suppression7=mysql_query($requete_suppression7) or die(mysql_error());
								$reponse_suppression8=mysql_query($requete_suppression8) or die('erreur8');
								$reponse_suppression9=mysql_query($requete_suppression9) or die('erreur9');
								
						echo $references;
					}
				
			?>
	    </td>
	   </tr>
	   
	   
	  </table>
	 </td>
	</tr>

      </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
