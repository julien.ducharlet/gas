<?php
/*
Gestion des familles de produit - Affichage en Front Office
*/

  require('includes/application_top.php');

  $action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {
      case 'insert':
      case 'save':
        if (isset($HTTP_GET_VARS['mID'])) $family_client_id = tep_db_prepare_input($HTTP_GET_VARS['mID']);
		
		
			$family_client_name = tep_db_prepare_input($HTTP_POST_VARS['family_client_name']);
      $sql_data_array = array('family_client_name' => $family_client_name);
		
		
        if ($action == 'insert') {
          $insert_sql_data = array('date_added' => 'now()');

          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

          tep_db_perform(TABLE_CLIENT_FAMILY, $sql_data_array);
          $family_client_id = tep_db_insert_id();
        } elseif ($action == 'save') {
          $update_sql_data = array('last_modified' => 'now()');

          $sql_data_array = array_merge($sql_data_array, $update_sql_data);

          tep_db_perform(TABLE_CLIENT_FAMILY, $sql_data_array, 'update', "family_client_id = '" . (int)$family_client_id . "'");
        }
		
		//ici on fait la requete de mise à jour de l'image si on up une nouvelle image
        if ($family_client_image = new upload('family_client_image', DIR_FS_CATALOG_IMAGES)) {
		  //on met a jour la base de donnée et on upload l'image si le champ family_image n'est pas vide
		  if($family_client_image->filename != "")
		  
          tep_db_query("update " . TABLE_CLIENT_FAMILY . " set family_client_image = '" . $family_client_image->filename . "' where family_client_id = '" . (int)$family_client_id . "'");
        }

        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $family_client_url_array = $HTTP_POST_VARS['family_client_url'];
          
		  //HTC BOC
          $family_client_htc_title_array = $HTTP_POST_VARS['family_client_htc_title_tag'];
          $family_client_htc_desc_array = $HTTP_POST_VARS['family_client_htc_desc_tag'];
          $family_client_htc_keywords_array = $HTTP_POST_VARS['family_client_htc_keywords_tag'];
          $family_client_htc_description_array = $HTTP_POST_VARS['family_client_htc_description'];
          //HTC EOC          
		  
          $language_id = $languages[$i]['id'];

         //HTC BOC
          $sql_data_array = array('family_client_url' => tep_db_prepare_input($family_client_url_array[$language_id]),
           'family_client_htc_title_tag' => (tep_not_null($family_client_htc_title_array[$language_id]) ? tep_db_prepare_input($family_client_htc_title_array[$language_id]) : $family_client_name),
           'family_client_htc_desc_tag' => (tep_not_null($family_client_htc_desc_array[$language_id]) ? tep_db_prepare_input($family_client_htc_desc_array[$language_id]) : $family_client_name),
           'family_client_htc_keywords_tag' => (tep_not_null($family_client_htc_keywords_array[$language_id]) ? tep_db_prepare_input($family_client_htc_keywords_array[$language_id]) : $family_client_name),
           'family_client_htc_description' => tep_db_prepare_input($family_client_htc_description_array[$language_id]));
          //HTC EOC

          $sql_data_array = array('family_client_url' => tep_db_prepare_input($family_client_url_array[$language_id]));

          if ($action == 'insert') {
            $insert_sql_data = array('family_client_id' => $family_client_id,
                                     'languages_id' => $language_id);

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            tep_db_perform(TABLE_CLIENT_FAMILY_INFO, $sql_data_array);
          } elseif ($action == 'save') {
            tep_db_perform(TABLE_CLIENT_FAMILY_INFO, $sql_data_array, 'update', "family_client_id = '" . (int)$family_client_id . "' and languages_id = '" . (int)$language_id . "'");
          }
        }

        if (USE_CACHE == 'true') {
          tep_reset_cache_block('family_client');
        }

        tep_redirect(tep_href_link(FILENAME_CLIENT_FAMILY, (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . 'mID=' . $family_client_id));
        break;
      case 'deleteconfirm':
        $family_client_id = tep_db_prepare_input($HTTP_GET_VARS['mID']);

        if (isset($HTTP_POST_VARS['delete_image']) && ($HTTP_POST_VARS['delete_image'] == 'on')) {
          $family_client_query = tep_db_query("select family_client_image from " . TABLE_CLIENT_FAMILY . " where family_client_id = '" . (int)$family_client_id . "'");
          $family_client = tep_db_fetch_array($family_client_query);

          $image_location = DIR_FS_DOCUMENT_ROOT . DIR_WS_CATALOG_IMAGES . $family_client['family_client_image'];

          if (file_exists($image_location)) @unlink($image_location);
        }

        tep_db_query("delete from " . TABLE_CLIENT_FAMILY . " where family_client_id = '" . (int)$family_client_id . "'");
        tep_db_query("delete from " . TABLE_CLIENT_FAMILY_INFO . " where family_client_id = '" . (int)$family_client_id . "'");

        if (isset($HTTP_POST_VARS['delete_products']) && ($HTTP_POST_VARS['delete_products'] == 'on')) {
          $products_query = tep_db_query("select products_id from " . TABLE_PRODUCTS . " where family_client_id = '" . (int)$family_client_id . "'");
          while ($products = tep_db_fetch_array($products_query)) {
            tep_remove_product($products['products_id']);
          }
        } else {
          tep_db_query("update " . TABLE_PRODUCTS . " set family_client_id = '' where family_client_id = '" . (int)$family_client_id . "'");
        }

        if (USE_CACHE == 'true') {
          tep_reset_cache_block('family_client');
        }

        tep_redirect(tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page']));
        break;
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title>Gestion des familles Front Office</title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Gestion des familles Front Office</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent">Famille d'Article</td>
                <td class="dataTableHeadingContent" align="right">Action&nbsp;</td>
              </tr>
<?php

  //BOC HTC
  $family_client_query_raw = "select m.family_client_id, m.family_client_name, m.family_client_image, m.date_added, m.last_modified, mi.family_client_htc_title_tag from " . TABLE_CLIENT_FAMILY . " m LEFT JOIN " .  TABLE_CLIENT_FAMILY_INFO . " mi on m.family_client_id = mi.family_client_id where mi.languages_id = '".$languages_id ."' order by m.family_client_name";
  //EOC HTC

  $family_client_split = new splitPageResults($HTTP_GET_VARS['page'], 50, $family_client_query_raw, $family_client_query_numrows);
  $family_client_query = tep_db_query($family_client_query_raw);
  while ($family_client = tep_db_fetch_array($family_client_query)) {
    if ((!isset($HTTP_GET_VARS['mID']) || (isset($HTTP_GET_VARS['mID']) && ($HTTP_GET_VARS['mID'] == $family_client['family_client_id']))) && !isset($mInfo) && (substr($action, 0, 3) != 'new')) {
      $family_client_products_query = tep_db_query("select count(*) as products_count from " . TABLE_PRODUCTS . " where family_client_id = '" . (int)$family_client['family_client_id'] . "'");
      $family_client_products = tep_db_fetch_array($family_client_products_query);

      $mInfo_array = array_merge($family_client, $family_client_products);
      $mInfo = new objectInfo($mInfo_array);
    }

    if (isset($mInfo) && is_object($mInfo) && ($family_client['family_client_id'] == $mInfo->family_client_id)) {
      echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $family_client['family_client_id'] . '&action=edit') . '\'">' . "\n";
    } else {
      echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $family_client['family_client_id']) . '\'">' . "\n";
    }
?>
                <td class="dataTableContent"><?php echo $family_client['family_client_name']; ?></td>
                <td class="dataTableContent" align="right"><?php if (isset($mInfo) && is_object($mInfo) && ($family_client['family_client_id'] == $mInfo->family_client_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $family_client['family_client_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $family_client_split->display_count($family_client_query_numrows, 50, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_FAMILY_CLIENT); ?></td>
                    <td class="smallText" align="right"><?php echo $family_client_split->display_links($family_client_query_numrows, 50, MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
<?php
  if (empty($action)) {
?>
              <tr>
                <td align="right" colspan="2" class="smallText"><?php echo '<a href="' . tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->family_client_id . '&action=new') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>'; ?></td>
              </tr>
<?php
  }
?>
            </table></td>

<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<b>Nouvelle famille</b>');

      $contents = array('form' => tep_draw_form('family_client', FILENAME_CLIENT_FAMILY, 'action=insert', 'post', 'enctype="multipart/form-data"'));
      $contents[] = array('text' => 'Merci de compl&eacute;ter les informations sur la nouvelle famille');
      $contents[] = array('text' => '<br>Nom de la famille :<br>' . tep_draw_input_field('family_client_name'));
	  
      $contents[] = array('text' => '<br>Image de la famille :<br>' . tep_draw_file_field('family_client_image'));

      $family_client_inputs_string = '';
      $languages = tep_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $family_client_inputs_string .= '<br>' . tep_draw_input_field('family_client_url[' . $languages[$i]['id'] . ']');
      //BOC HTC 
        $family_client_htc_title_string .= '<br>' . tep_draw_input_field('family_client_htc_title_tag[' . $languages[$i]['id'] . ']');
        $family_client_htc_desc_string .= '<br>' . tep_draw_input_field('family_client_htc_desc_tag[' . $languages[$i]['id'] . ']');
        $family_client_htc_keywords_string .= '<br>' . tep_draw_input_field('family_client_htc_keywords_tag[' . $languages[$i]['id'] . ']');
        $family_client_htc_description_string .= '<br>' . tep_draw_textarea_field('family_client_htc_description[' . $languages[$i]['id'] . ']', 'hard', 30, 5, '');
      // EOC HTC        
      }

$contents[] = array('text' => '<br>URL de la famille : ' . $family_client_inputs_string);
      
      // HTC BOC
      $contents[] = array('text' => '<br>' . 'Balise META Title (Titre de la Famille)' . $family_client_htc_title_string);
      $contents[] = array('text' => '<br>' . 'Balise META Description (Description de la Famille)' . $family_client_htc_desc_string);
      $contents[] = array('text' => '<br>' . 'Balise META Keyword (Mots cl&eacute;s de la Famille)' . $family_client_htc_keywords_string);
      $contents[] = array('text' => '<br>' . 'Voir utilisation sinon supprimer' . $family_client_htc_description_string);
      // HTC EOC
	  
      $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $HTTP_GET_VARS['mID']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    case 'edit':
      $heading[] = array('text' => '<b>Editer famille</b>');

      $contents = array('form' => tep_draw_form('family_client', FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->family_client_id . '&action=save', 'post', 'enctype="multipart/form-data"'));
      $contents[] = array('text' => 'Merci de faire les changements n&eacute;cessaires');
      $contents[] = array('text' => '<br>Nom de la famille : <br>' . tep_draw_input_field('family_client_name', $mInfo->family_client_name,' size=\'50\''));	  
	  
      $contents[] = array('text' => '<br>Image de la famille :<br>' . tep_draw_file_field('family_client_image') . '<br>' . $mInfo->family_client_image);

      $family_inputs_string = '';
      $languages = tep_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $family_client_inputs_string .= '<br>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('family_client_url[' . $languages[$i]['id'] . ']', tep_get_family_client_url($mInfo->family_client_id, $languages[$i]['id']),' size=\'50\'');
		
       //BOC HTC 
        $family_client_htc_title_string .= '<br>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('family_client_htc_title_tag[' . $languages[$i]['id'] . ']', tep_get_family_client_htc_title($mInfo->family_client_id, $languages[$i]['id']),' size=\'50\'');
        $family_client_htc_desc_string .= '<br>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('family_client_htc_desc_tag[' . $languages[$i]['id'] . ']', tep_get_family_client_htc_desc($mInfo->family_client_id, $languages[$i]['id']),' size=\'50\'');
        $family_client_htc_keywords_string .= '<br>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('family_client_htc_keywords_tag[' . $languages[$i]['id'] . ']', tep_get_family_client_htc_keywords($mInfo->family_client_id, $languages[$i]['id']),' size=\'50\'');
        $family_client_htc_description_string .= '<br>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_textarea_field('family_client_htc_description[' . $languages[$i]['id'] . ']', 'hard', 30, 5, tep_get_family_client_htc_description($mInfo->family_client_id, $languages[$i]['id']));
       // EOC HTC        
      }

      $contents[] = array('text' => '<br>URL de la famille : ' . $family_client_inputs_string);
      // HTC BOC
      $contents[] = array('text' => '<br>' . 'Balise META Title (Titre de la Famille)' . $family_client_htc_title_string);
      $contents[] = array('text' => '<br>' . 'Balise META Description (Description de la Famille)' . $family_client_htc_desc_string);
      $contents[] = array('text' => '<br>' . 'Balise META Keyword (Mots cl&eacute;s de la Famille)' . $family_client_htc_keywords_string);
      $contents[] = array('text' => '<br>' . 'Voir utilisation sinon supprimer' . $family_client_htc_description_string);
      // HTC EOC

	  
      $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->family_client_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    case 'delete':
      $heading[] = array('text' => '<b>Supprimer famille</b>');

      $contents = array('form' => tep_draw_form('family_client', FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->family_client_id . '&action=deleteconfirm'));
      $contents[] = array('text' => 'Etes vous sur de vouloir supprimer cette famille ?');
      $contents[] = array('text' => '<br><b>' . $mInfo->family_client_name . '</b>');
      $contents[] = array('text' => '<br>' . tep_draw_checkbox_field('delete_image', '', true) . 'Suppprimer l\'image de la famille ?');

      if ($mInfo->products_count > 0) {
        $contents[] = array('text' => '<br>' . tep_draw_checkbox_field('delete_products') . ' Supprimer tous les Articles de cette famille ? (en incluant les critiques, produits en promotion, produits à venir)');
        $contents[] = array('text' => '<br>' . sprintf('<b>ATTENTION :</b> Il reste %s Article(s) liées à cette famille !', $mInfo->products_count));
      }

      $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->family_client_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    default:
      if (isset($mInfo) && is_object($mInfo)) {
        $heading[] = array('text' => '<b>' . $mInfo->family_client_name . '</b>');

        $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->family_client_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_CLIENT_FAMILY, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->family_client_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
        $contents[] = array('text' => '<br>Date d\'ajout : ' . tep_date_short($mInfo->date_added));
        if (tep_not_null($mInfo->last_modified)) $contents[] = array('text' => 'Derniere modification : ' . tep_date_short($mInfo->last_modified));
        $contents[] = array('text' => '<br><div align="center">' . tep_info_image($mInfo->family_client_image, $mInfo->family_client_name) . '</div>');
        $contents[] = array('text' => '<br>Nombre d\'article dans la Famille : ' . $mInfo->products_count);
      }
      break;
  }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
