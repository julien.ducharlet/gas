<?php
// Page qui g�n�re les lignes pour impression des �tiquettes d'exp�dition 
// Derni�re modification le 08/06/2018
// par Thierry 

$TRANCHE_SUP=74.99; // au DESSUS du montant on envoi AVEC remise contre signature
$TRANCHE_MOINS=75.00; // au DESSOUS du montant on envoi SANS remise contre signature

$TRANCHE_SUP_OM=0.01; // au DESSUS du montant on envoi AVEC remise contre signature
$TRANCHE_MOINS_OM=0.02; // au DESSOUS du montant on envoi SANS remise contre signature

$DOM="DOM";
$DOS="DOS";
$COLI="COLI";
$COM="COM";
$CDS="CDS";

// Get customer id
  	$oID = tep_db_prepare_input($oID);
  	$customers_query = tep_db_query("select customers_id, customers_email_address from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
	$customers = tep_db_fetch_array($customers_query);
	$customers_id = $customers['customers_id'];
	$customers_email_address = $customers['customers_email_address'];
	$oID = tep_db_prepare_input($oID);

	if(empty($_SESSION['commandes'])) {
		
		$shipping_total_weight_query = 'select poids
									from '. TABLE_ORDERS .'
									where orders_id = \''. (int)$oID .'\'
									group by orders_id';
	} else {
		
		$shipping_total_weight_query = 'select sum(products_weight*products_quantity_saisie) as poids
									from '. TABLE_ORDERS_PRODUCTS .'
									where orders_id = \''. (int)$oID .'\'
									and products_quantity_sent<=products_quantity
									and products_quantity_saisie>0
									group by orders_id';
	}
	
  	$shipping_total_weight_query = tep_db_query($shipping_total_weight_query);

	$shipping_total_weight = tep_db_fetch_array($shipping_total_weight_query);
	$shipping_total_weight = $shipping_total_weight['poids'];
	
	if($shipping_total_weight < 0.250) $shipping_total_weight = 0.250;

// Get order id and shipping infos
  $oID = tep_db_prepare_input($oID);
  $orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
$orders = tep_db_fetch_array($orders_query);
$order_id = $orders['orders_id'];
  $order = new order($oID);
	
// Get total of order
  $oID = tep_db_prepare_input($oID);
  $total = tep_db_query("select total from orders where orders_id = '" . (int)$oID . "'");
  $TOTAL= tep_db_fetch_array($total);
  $MONTANT_TOTAL = $TOTAL['total'];

?>
<?php echo $HTTP_GET_VARS['oID']; ?>
<?
if (isset($order->delivery['name']) && tep_not_null($order->delivery['name'])) {
   $name = $order->delivery['name'];
} elseif (isset($order->customer['name']) && tep_not_null($order->delivery['name'])) {
   $name = $order->customer['name'];
}
?>

<?php echo '"' . $name . '";"'; // Nom du destinataire ?>
<?php echo $order->delivery['company'] . '";"'; // Raison sociale du destinataire ?>
<?php echo $order->delivery['street_address'] . '";"'; // Adresse 1 du destinataire : Num�ro et libell� de voie ?>
<?php echo $order->delivery['suburb'] . '";"'; // Adresse 2 du destinataire : Etage, couloir, escalier, appartement ?>
<?php echo $order->delivery['postcode'] . '";"'; // Code postal du destinataire ?>
<?php echo $order->delivery['city'] . '";"'; // Commune du destinataire ?>
<?php 
//recuperation du code iso
$code_iso="select countries_iso_code_2  from countries where countries_name='".$order->delivery['country']."'";
$resultat_code_iso=mysql_query($code_iso);
$CODE_ISO=mysql_fetch_array($resultat_code_iso);
$ISO=$CODE_ISO['countries_iso_code_2'];
//fin recuperation
?>
<?php echo $ISO . '";"'; // Code pays du destinataire ?>
<?php echo $order->customer['telephone'] . '";"'; // Portable du destinataire ?>
<?php echo '";"'; // Telephone destinataire NON UTILISE ?>
<?php echo $customers_id.'-'.$order_id . '";"'; // R�f�rence destinataire ?>
<?php echo $order_id . '";"'; // R�f�rence de commande ?>
<?php echo $shipping_total_weight . '";"'; // Poids ?>
<?php //echo $TOTAL['total'] . '";"'; // Total commande qui n'est pas utilis� ?>
<?php 
if (($ISO=='FR' or $ISO=='MC' or $ISO=='AD') and $MONTANT_TOTAL<$TRANCHE_MOINS) {
	echo $DOM . '";"';
} else if (($ISO=='FR' or $ISO=='MC' or $ISO=='AD') and $MONTANT_TOTAL>$TRANCHE_SUP) {
	echo $DOS . '";"';
} else if (
	$ISO=='SP' or $ISO=='ZA' or $ISO=='AL' or $ISO=='DZ' or $ISO=='DE' or $ISO=='AO' or $ISO=='SA' or $ISO=='AR' or $ISO=='AM' or $ISO=='AW' or $ISO=='AC' or $ISO=='AU' or $ISO=='AT' or $ISO=='AZ' or $ISO=='BS' or $ISO=='BH' or $ISO=='BD' or $ISO=='BB' or $ISO=='BE' or $ISO=='BZ' or $ISO=='BJ' or $ISO=='BM' or $ISO=='BY' or $ISO=='MM' or $ISO=='BO' or $ISO=='BA' or $ISO=='BW' or $ISO=='BR' or $ISO=='BG' or $ISO=='BF' or $ISO=='BI' or $ISO=='KH' or $ISO=='CA' or $ISO=='CV' or $ISO=='CF' or $ISO=='CL' or $ISO=='CN' or $ISO=='CY' or $ISO=='CO' or $ISO=='KM' or $ISO=='CG' or $ISO=='KP' or $ISO=='KR' or $ISO=='CR' or $ISO=='CI' or $ISO=='HR' or $ISO=='CU' or $ISO=='DK' or $ISO=='DJ' or $ISO=='EG' or $ISO=='AE' or $ISO=='EC' or $ISO=='ER' or $ISO=='ES' or $ISO=='EE' or $ISO=='US' or $ISO=='ET' or $ISO=='FJ' or $ISO=='FI' or $ISO=='GA' or $ISO=='GM' or $ISO=='GE' or $ISO=='GH' or $ISO=='GI' or $ISO=='GR' or $ISO=='GD' or $ISO=='GL' or $ISO=='GT' or $ISO=='GG' or $ISO=='GN' or $ISO=='GQ' or $ISO=='GW' or $ISO=='HT' or $ISO=='HN' or $ISO=='HK' or $ISO=='HU' or $ISO=='IM' or $ISO=='CK' or $ISO=='FO' or $ISO=='IN' or $ISO=='ID' or $ISO=='IQ' or $ISO=='IR' or $ISO=='IS' or $ISO=='IL' or $ISO=='IT' or $ISO=='JM' or $ISO=='JP' or $ISO=='JE' or $ISO=='JO' or $ISO=='KZ' or $ISO=='KE' or $ISO=='KW' or $ISO=='LA' or $ISO=='LS' or $ISO=='LV' or $ISO=='LB' or $ISO=='LR' or $ISO=='LI' or $ISO=='LT' or $ISO=='LU' or $ISO=='LY' or $ISO=='MO' or $ISO=='MK' or $ISO=='MG' or $ISO=='MY' or $ISO=='MW' or $ISO=='MV' or $ISO=='ML' or $ISO=='MT' or $ISO=='MA' or $ISO=='MU' or $ISO=='MR' or $ISO=='MX' or $ISO=='FM' or $ISO=='MD' or $ISO=='MN' or $ISO=='ME' or $ISO=='MZ' or $ISO=='NA' or $ISO=='NP' or $ISO=='NI' or $ISO=='NE' or $ISO=='NG' or $ISO=='NO' or $ISO=='NZ' or $ISO=='OM' or $ISO=='UG' or $ISO=='UZ' or $ISO=='PA' or $ISO=='PY' or $ISO=='NL' or $ISO=='PE' or $ISO=='PH' or $ISO=='PL' or $ISO=='PR' or $ISO=='PT' or $ISO=='QA' or $ISO=='CD' or $ISO=='DO' or $ISO=='CZ' or $ISO=='RO' or $ISO=='GB' or $ISO=='RU' or $ISO=='RW' or $ISO=='KN' or $ISO=='SX' or $ISO=='VC' or $ISO=='SM' or $ISO=='SH' or $ISO=='LC' or $ISO=='SV' or $ISO=='WS' or $ISO=='ST' or $ISO=='SN' or $ISO=='RS' or $ISO=='SC' or $ISO=='SL' or $ISO=='SG' or $ISO=='SK' or $ISO=='SI' or $ISO=='SO' or $ISO=='SD' or $ISO=='SE' or $ISO=='CH' or $ISO=='SZ' or $ISO=='SY' or $ISO=='TZ' or $ISO=='TD' or $ISO=='TH' or $ISO=='TG' or $ISO=='TO' or $ISO=='TT' or $ISO=='TN' or $ISO=='TM' or $ISO=='TR' or $ISO=='TV' or $ISO=='UA' or $ISO=='UY' or $ISO=='VU' or $ISO=='VA' or $ISO=='VE' or $ISO=='VN' or $ISO=='YE' or $ISO=='ZM' or $ISO=='ZW'
	) 
	{ echo $COLI . '";"'; 
} else if(
	( $ISO=='GP' or $ISO=='GF' or $ISO=='MQ' or $ISO=='YT' or $ISO=='NC' or $ISO=='PF' or $ISO=='RE' or $ISO=='MF' or $ISO=='PM' or $ISO=='BL' or $ISO=='TF' or $ISO=='WF' ) and $MONTANT_TOTAL<$TRANCHE_MOINS_OM){ echo $COM . '";"'; 
} else if(
	( $ISO=='GP' or $ISO=='GF' or $ISO=='MQ' or $ISO=='YT' or $ISO=='NC' or $ISO=='PF' or $ISO=='RE' or $ISO=='MF' or $ISO=='PM' or $ISO=='BL' or $ISO=='TF' or $ISO=='WF' ) and $MONTANT_TOTAL>$TRANCHE_SUP_OM){
	echo $CDS . '";"';
}
?>
<?php echo $customers_email_address . '"'; // Adresse e-mail du destinataire ?>
<br/>

<? /*
Remplacer (a la fin) DOM" par une des ligne suivante : 
Pour trouver le code ISO il faut utiliser dans la table "orders" le champ "delivery_country" et rechercher l'equilalent dans la table "countries" champ "countries_name" et recup�rer le champ "countries_iso_code_2"

DOM : (Access France) quand le colis est a livrer pour les code ISO : FR et qui font moins/ou de 74.99 Euros 
DOS : (Expert France) quand le colis est a livrer pour les code ISO : FR et qui font plus de 75 Euros 


COLI : (Expert Inter) quand le colis est a livrer pour les code ISO : BE, DE, LU, NL, ES, IT, GB, AT, DK, IE, PT, FI, NO, SE, CH

COM : (Access OM) quand le colis est a livrer pour les code ISO : GP, MQ, GF, RE, YT, PM, NC, PF, WF qui font moins/ou de 80 Euros 
CDS : (Expert OM) quand le colis est a livrer pour les code ISO : GP, MQ, GF, RE, YT, PM, NC, PF, WF et qui font plus de 80 Euros 
 */ ?>

 
