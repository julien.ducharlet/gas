$(document).ready(function() {
	
	$rayon = $('#rayon');
	$marque = $('#marque');
	$modele = $('#modele');
	$article = $('#products_id');
	
	article_opt = '<option value="">Choisissez un article</option>';
	
	$rayon.change(function() {
		
		$.ajax({
		   type: "GET",
		   url: "nouvelle_commande_ajax.php?rayon_select="+ $(this).val(),
		  
		   dataType: "text",
		   processData: false,
		   success: function(data) {
			   
			$marque.html(data);
			$modele.html('<option value="">Choisissez un mod&egrave;le</option>');
			$article.html(article_opt);
		   }
		});
	});
	
	$marque.change(function() {
		
		$.ajax({
		   type: "GET",
		   url: "nouvelle_commande_ajax.php?marque_select="+ $(this).val(),
		  
		   dataType: "text",
		   processData: false,
		   success: function(data) {
			
			$modele.html(data);
			$article.html(article_opt);
		   }
		});
	});
	
	$modele.change(function() {
		
		$.ajax({
		   type: "GET",
		   url: "nouvelle_commande_ajax.php?modele_select="+ $(this).val(),
		  
		   dataType: "text",
		   processData: false,
		   success: function(data) {
			
			$article.html(article_opt + data);
			$article.removeAttr('disabled');
		   }
		});
	});
	
	
	
	$rayon_new = $('#rayon_new');
	$marque_new = $('#marque_new');
	$modele_new = $('#modele_new');
	$article_new = $('#products_new');
	
	article_opt_new = '<option value="">Choisissez un article</option>';
	
	$rayon_new.change(function() {
		
		$.ajax({
		   type: "GET",
		   url: "nouvelle_commande_ajax.php?rayon_select="+ $(this).val(),
		  
		   dataType: "text",
		   processData: false,
		   success: function(data) {
			   
			$marque_new.html(data);
			$modele_new.html('<option value="">Choisissez un mod&egrave;le</option>');
			$article_new.html(article_opt_new);
		   }
		});
	});
	
	$marque_new.change(function() {
		
		$.ajax({
		   type: "GET",
		   url: "nouvelle_commande_ajax.php?marque_select="+ $(this).val(),
		  
		   dataType: "text",
		   processData: false,
		   success: function(data) {
			
			$modele_new.html(data);
			$article_new.html(article_opt_new);
		   }
		});
	});
	
	$modele_new.change(function() {
		
		$.ajax({
		   type: "GET",
		   url: "nouvelle_commande_ajax.php?modele_select="+ $(this).val(),
		  
		   dataType: "text",
		   processData: false,
		   success: function(data) {
			
			$article_new.html(article_opt_new + data);
			$article_new.removeAttr('disabled');
		   }
		});
	});
});