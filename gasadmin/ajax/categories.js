function update_options(select_id_avant,select_id_apres,id) {
	var xhr_object = null;
	
	if(window.XMLHttpRequest) xhr_object = new XMLHttpRequest();
	else if(window.ActiveXObject) xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	else { // XMLHttpRequest non support� par le navigateur
	   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
	   return;
	}
	//document.getElementById('ajax_chargement').innerHTML = 'Chargement en cours ... ';
	
	
	var filename = "ajax_categories.php?type=1&id="+document.getElementById(select_id_avant).value;
	var i		 = 1;
	var data     = null;
		
	xhr_object.open("GET", filename, true);
	xhr_object.onreadystatechange = function() {
		
	   if(xhr_object.readyState == 4) {
		  var tmp = xhr_object.responseText.split("___");
		  var tmp2;
			//alert(xhr_object.responseText)
			
			document.getElementById(select_id_apres).length = tmp[0];
			for(i; i<= tmp[0]; i++) {
				//alert(tmp[i])
				if(typeof(tmp[i]) != "undefined" && tmp[i] != "") {
					tmp2 = tmp[i].split("##");
					document.getElementById(select_id_apres).options[i-1].value = tmp2[0];
					document.getElementById(select_id_apres).options[i-1].text = tmp2[1];
					if(tmp2[2] == 1) {
						document.getElementById(select_id_apres).options[i-1].disabled = 'disabled';
					}
					if(tmp2[2] == 2 || (typeof(id) != "undefined" && id==tmp2[0])) {
						document.getElementById(select_id_apres).options[i-1].selected = 'selected';
					}
				}
			}
	   }
	}
	
	xhr_object.send(data);
	//document.getElementById('ajax_chargement').innerHTML = '';

}
function delete_option (id_champ, options_id ,values_id) {
	
	var pos=0;
	var chaineA = "";
	var chaineP = "";
	
	var texteHidden = document.getElementById('liste_option_details').value;
	var listeTexte = texteHidden.split('�');

	for(i=0; i < listeTexte.length; i++) {
		texteHidden = listeTexte[i].split('#');
		if(texteHidden[0] == options_id && texteHidden[1] == values_id) {
			iOK = i
			break;
		}
	}
	document.getElementById('liste_option_details').value = '';
	for(i=0; i < listeTexte.length; i++) {		
		if(i != iOK){
			if(i==0)
				document.getElementById('liste_option_details').value = listeTexte[i];
			else
				document.getElementById('liste_option_details').value += '�'+listeTexte[i];
		}
	}
	
	pos1 = document.getElementById(id_champ).innerHTML.indexOf('<balise id="option-'+options_id+'-'+values_id+'">');
	pos2 = document.getElementById(id_champ).innerHTML.indexOf('</balise>',pos1);

	
	chaine = document.getElementById(id_champ).innerHTML.substr(0,pos1) + document.getElementById(id_champ).innerHTML.substr(pos2+9) 
	
	

	document.getElementById(id_champ).innerHTML =chaine
	
}
function insere_option (id_champ) {

	if(document.getElementById('products_options_order').value == "") {
		alert('L\'ordre d\'affichage est vide')
		return false;
	}
	if(isNaN(document.getElementById('products_options_order').value)) {
		alert('L\'ordre d\'affichage doit �tre un chiffre')
		return false;
	}
	if(isNaN(document.getElementById('value_price').value)) {
		alert('Le prix de l\'option doit �tre un chiffre')
		return false;
	}
	if(document.getElementById('options_id').value == 0) {
		alert('Option non choisie')
		return false;	
	}
	if(document.getElementById('values_id').value == 0) {
		alert('Valeur d\'option non choisie')
		return false;	
	}
	
	if(document.getElementById(id_champ).innerHTML == "") 
		document.getElementById(id_champ).innerHTML = "Liste des options enregistr�es : <br /><br />";
	
	if(window.XMLHttpRequest) xhr_object = new XMLHttpRequest();
	else if(window.ActiveXObject) xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	else { // XMLHttpRequest non support� par le navigateur
	   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
	   return;
	}

	var filename = "ajax_categories.php?type=2&options_id="+document.getElementById('options_id').value+"&values_id="+document.getElementById('values_id').value;
	var i		 = 1;
	var data     = null;
		
	xhr_object.open("GET", filename, true);
	xhr_object.onreadystatechange = function() {
		
		if(xhr_object.readyState == 4) {
			var tmp = xhr_object.responseText.split("#");
			var span;
			var pos=0;
			var texteHidden = document.getElementById('liste_option_details').value;
			var listeTexte = texteHidden.split('�');

			for(i=0; i < listeTexte.length; i++) {
				texteHidden = listeTexte[i].split('#');
				if(texteHidden[0] == document.getElementById('options_id').value 
							&& texteHidden[1] == document.getElementById('values_id').value) {
					alert('Vous avez d�j� entr� cette option');
					return;
				}
			}
			
			texteHidden = document.getElementById('options_id').value+'#'+document.getElementById('values_id').value+'#'+document.getElementById('price_prefix').value+'#'+document.getElementById('value_price').value+'#'+document.getElementById('products_options_order').value;
			if(document.getElementById('liste_option_details').value == '') 
				document.getElementById('liste_option_details').value = texteHidden;
			else
				document.getElementById('liste_option_details').value += '�'+texteHidden
			
			if(document.getElementById('price_prefix').value == "+") span = '<span style="color:#009933">';
			else span = '<span style="color:#FF0000">';
			
			document.getElementById(id_champ).innerHTML += '<balise id="option-'+document.getElementById('options_id').value+'-'+document.getElementById('values_id').value+'"><a href="#'+id_champ+'" onclick="delete_option(\''+id_champ+'\','+document.getElementById('options_id').value+','+document.getElementById('values_id').value+')"><img src="images/icons/delete.gif" border="0" /></a> ('+ document.getElementById('products_options_order').value +') <i>' + tmp[0]  + ' - '+ tmp[1] + ', '+span+ document.getElementById('price_prefix').value+document.getElementById('value_price').value + ' �</span></i><br /></balise>';
			
			document.getElementById('products_options_order').value = parseInt(document.getElementById('products_options_order').value) +1;
			//alert(document.getElementById(id_champ).innerHTML)
		}
	}
	
	xhr_object.send(data);
	
}

function delete_attribute (id) {
	
	if(window.XMLHttpRequest) xhr_object = new XMLHttpRequest();
	else if(window.ActiveXObject) xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	else { // XMLHttpRequest non support� par le navigateur
	   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
	   return;
	}

	var filename = "ajax_categories.php?type=3&id="+id;
	var data     = null;
		
	xhr_object.open("GET", filename, true);
	xhr_object.onreadystatechange = function() {
		
		if(xhr_object.readyState == 4) {
			if(xhr_object.responseText == 1) {
				alert('Attribut supprim�');
				document.getElementById('attributes-'+id).innerHTML = '<center><s><font color="#FF0000">'+id+'</font></s></center>';
			} else alert('Impossible de supprimer cet attribut (ID = '+id+') : '+xhr_object.responseText);
		}
	}
	
	xhr_object.send(data);
}

function update_attribute(id) {
		
	if(window.XMLHttpRequest) xhr_object = new XMLHttpRequest();
	else if(window.ActiveXObject) xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	else { // XMLHttpRequest non support� par le navigateur
	   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
	   return;
	}

	
	var data     = null;
	var price_prefix = document.getElementById('price_prefix-'+id).value;
	if(price_prefix == '+') price_prefix = 1; else price_prefix = 0;
	
	var options_values_price= document.getElementById('options_values_price-'+id).value;
	var values_id= document.getElementById('values_id-'+id).value;
	var options_id= document.getElementById('options_id-'+id).value;
	var attributes_order= document.getElementById('attributes_order-'+id).value;
	
	var filename = "ajax_categories.php?type=4&id="+id+'&price_prefix='+price_prefix+'&options_values_price='+options_values_price+'&values_id='+values_id+'&options_id='+options_id+'&attributes_order='+attributes_order;
	//alert(filename)
	
	//alert(filename)
	xhr_object.open("GET", filename, true);
	xhr_object.onreadystatechange = function() {
		
		if(xhr_object.readyState == 4) {
			if(xhr_object.responseText == 1) {
				alert('Attribut modifi�');
				document.getElementById('attributes-'+id).innerHTML = '<center><b><font color="#009933">'+id+'</font></b></center>';
			} else alert('Impossible de modifier cet attribut (ID = '+id+') : '+xhr_object.responseText);
		}
	}
	
	xhr_object.send(data);
}

function active_button(id) {
	document.getElementById(id).setAttribute('disabled', 'disabled'); 		
}
function desactive_button(id) {
	document.getElementById(id).removeAttribute('disabled'); 	
}


