<?php
  require('includes/application_top.php');
  require('includes/functions/date.php');
  require('../' . DIR_WS_SITE . '/includes/fonctions/mail.php');
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();
  
  $Nombre_de_ligne_par_page="110";
  if(isset($HTTP_GET_VARS['nb_resultats']) && !empty ($HTTP_GET_VARS['nb_resultats'])){
  	$Nombre_de_ligne_par_page = tep_db_prepare_input($HTTP_GET_VARS['nb_resultats']);
  }
  
  
  $action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
  if (tep_not_null($action)) {
    switch ($action) {
		
      case 'envoi_mail' :
	  	$oID = tep_db_prepare_input($HTTP_GET_VARS['oID']);
		$invoice_query = tep_db_query("select customers_name, customers_email_address, customers_id, date_purchased from " . TABLE_ORDERS . " where orders_id = '" . $oID . "'");
		$row_invoice = tep_db_fetch_array($invoice_query);					
		$facture= WEBSITE."/".DIR_WS_SITE.'/compte/generation_pdf/facture.php?id_com='.$oID.'&cli='.md5($row_invoice['customers_id']);
		
		$email = 'Bonjour <span style="font-weight: bold; color:#AAB41D;">'. ucwords(strtolower($row_invoice['customers_name'])) .'</span>,<br />';
		$email .= '<hr style="color:#AAB41D;">Num�ro de commande : <span style="font-weight:bold;color:#fe5700;">' . $oID . '</span><br />';
		$email .= 'Date de commande : <span style="font-weight:bold;color: rgb(170, 180, 29);">'. tep_date_long($row_invoice['date_purchased']) .'</span><hr style="color:#AAB41D;"><br />';
		
    	$email .= 'Vous pouvez acc�der � votre facture <a href="'. WEBSITE ."/". DIR_WS_SITE .'/compte/generation_pdf/facture.php?id_com='. $oID .'&cli='.md5($row_invoice['customers_id']) .'" style="color:#FE5700;font-weight:bold;">en cliquant ici</a><br /><br />'. SIGNATURE_MAIL;
		
		mail_client_commande($row_invoice['customers_email_address'], "Votre facture ". STORE_NAME, $email);
		
		break;
		
	  case 'deleteconfirm':
        $oID = tep_db_prepare_input($HTTP_GET_VARS['oID']);
		
		(estDevis($oID)) ? tep_remove_order($oID, FALSE) : tep_remove_order($oID, TRUE);
		
        tep_redirect(tep_href_link('cmd_list_no_customers.php', tep_get_all_get_params(array('oID', 'action'))));
        break;
		
	  case 'edit':
	  	tep_redirect(tep_href_link('cmd_list_no_customers.php', tep_get_all_get_params()));
		break;
    }
  }

  include(DIR_WS_CLASSES . 'order.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/function.js"></script>
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/metadata.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
    // call the tablesorter plugin
    $(".tablesorter").tablesorter();
});
</script>

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<div id="popupcalendar" class="text"></div>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<?php // DEBUT ZONE DES FILTRES DES COMMANDES ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr> 
    <td width="100%" valign="top">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #e1e1f5;">
		<tr>
        	<td align="center">
            	<table width="95%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="smallText" height="40">  
						<?php 	
								echo tep_draw_form('Filtres', 'cmd_list_no_customers.php', '', 'get'); 
								$nb_resultats_array = array();
								$nb_resultats_array[] = array('id' => 100, 'text' => "100");
								$nb_resultats_array[] = array('id' => 200, 'text' => "200");
								$nb_resultats_array[] = array('id' => 300, 'text' => "300");
								$nb_resultats_array[] = array('id' => 500, 'text' => "500");
								$nb_resultats_array[] = array('id' => 1000, 'text' => "1000");
								echo "Nombre de Commande par page :&nbsp;".tep_draw_pull_down_menu('nb_resultats', $nb_resultats_array, '', 'onChange="this.form.submit();"');?>
						</td>
						<td align="left" class="smallText">        
						<?php	echo 'Num�ro de Facture :&nbsp;'.tep_draw_input_field('num_facture', '', 'size="20" id="focusorders"');?>
						</td>
						<td align="left" class="smallText">        
						<?php	echo 'Nom soci�t� :&nbsp;'.tep_draw_input_field('customers_company', '', 'size="20" id="focusorders"');?> 
						
						</td>
						<td align="left" class="smallText">
							<?php	echo 'Nom et/ou pr�nom du client :&nbsp;'.tep_draw_input_field('customers_name', '', 'size="20" id="focusorders"');?>
							<input type=submit value="Go">
							</form>
						</td>
						
                    </tr> 
				</table>
        	</td>
        </tr>
        </table>
        </td>
	  </tr>
	  <?php // FIN ZONE DES FILTRES DES COMMANDES ?>
	  <tr>
		<td>
          <table border="0" width="100%" cellspacing="0" cellpadding="0">
          	<tr><td>&nbsp;</td></tr>
            <tr>
          		<td valign="top">
				<?php echo tep_draw_form('batch_orders', 'OSC_Expeditor_process.php', '', 'post', 'target="_blank"'); ?>
					<table class="tablesorter" cellspacing="0" cellpadding="2" border="0">
              		<thead>
                      <tr>
                      <th>Pays</th>
                      <th>IP cmd</th>
                      <th style="text-align:center;">ID</th>
                      <th>Client</th>
                      <th>Soci�t�</th>
                      <th class="{sorter:'digit'}">Total</th>
                      <th class="{sorter:'digit'}">Marge</th>
                      <th>Date d'achat</th>
                      <th>Mode Paiement</th>
                      <th>Statut</th>
                      <th class="{sorter:false}" align="center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
<?php
	
	if(isset($HTTP_GET_VARS['num_facture']) && !empty($HTTP_GET_VARS['num_facture'])) {
    	$num_facture = tep_db_prepare_input($HTTP_GET_VARS['num_facture']);
      	$where .= " and o.orders_numero_facture like '%".(int)$num_facture."%'";
    }
	
	if(isset($HTTP_GET_VARS['customers_company']) && !empty($HTTP_GET_VARS['customers_company'])) {
		$customers_company = tep_db_prepare_input($HTTP_GET_VARS['customers_company']);
      	$where .= " and o.customers_company like '%".$customers_company ."%'";
	}

	if(isset($HTTP_GET_VARS['customers_name']) && !empty($HTTP_GET_VARS['customers_name'])) {
		$customers_name = tep_db_prepare_input($HTTP_GET_VARS['customers_name']);
      	$where .= " and o.customers_name like '%".$customers_name ."%'";
	}
	
	$ordre = " order by o.orders_id DESC";
	
	if (isset($HTTP_GET_VARS['ordre']) && tep_not_null($HTTP_GET_VARS['ordre'])) {
      	$ordre = " order by ".tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['ordre']))." ".tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['type_ordre']));
	}
	
	$orders_query_raw="SELECT o.orders_id, o.customers_id, o.delivery_country, o.origine, o.customers_name, o.customers_company, o.customers_id, o.customer_ip_country, o.customer_ip, o.payment_method, o.total, o.marge, o.remise, o.date_purchased, o.last_modified, o.currency, o.currency_value, o.marge, s.orders_status_name, o.orders_numero_facture, o.orders_status
										 from " . TABLE_ORDERS . " o, " . $from . TABLE_ORDERS_STATUS . " s
										 where o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' ";
										 
	$orders_query_raw .= $where . $ordre;
    
	$orders_split = new splitPageResults($HTTP_GET_VARS['page'], $Nombre_de_ligne_par_page, $orders_query_raw, $orders_query_numrows);
    $orders_query = tep_db_query($orders_query_raw);
	
	$style = '';
	// on recupere toutes les commandes de la page

	while ($orders = tep_db_fetch_array($orders_query)) {	
	?>                      
                      <td class="dataTableContent" align="center">
						<?php
                        // Code pour mettre des icones en fontion du Pays de livraison 
                        echo get_drapeau($orders['delivery_country']);					
                        ?>
					  </td>
                      <!-- Affichage du pays de la commande d�termin� par l'IP -->
                      <td class="dataTableContent" align="center">
						<?php
                        // Code pour mettre des icones en fontion du Pays de la commande
						if (isset($orders['customer_ip_country']) && $orders['customer_ip_country'] != ''){
							echo "<a href=\"https://en.utrace.de/?query=" . $orders['customer_ip'] . "\" target=\"_blank\">".get_drapeau($orders['customer_ip_country'])."</a>";
						} 
						else {
							echo "<a href=\"https://en.utrace.de/?query=" . $orders['customer_ip'] . "\" target=\"_blank\">&nbsp;</a>";
						}
                        ?>
					  </td>
                      <td <?php echo $link; ?> class="dataTableContent" align="center">
						<?php echo '<a href="'.tep_href_link(FILENAME_CMD_EDIT, tep_get_all_get_params(array('oID', 'action')).'oID='.$orders['orders_id'].'&action=edit').'" '. $style .'>'.$orders['orders_id'].'</a>&nbsp;'; ?>
					  </td>
					  <td <?php echo $link; ?> class="dataTableContent"><?php echo $orders['customers_name']; ?></td>
					  <td class="dataTableContent">
						<?php echo '<a href="'.tep_href_link('cmd_list_no_customers.php', tep_get_all_get_params(array('cID'))).'&cID='.$orders['customers_id'].'" target="_blank" title="Voir Toutes les commandes du client">'.$orders['customers_company'].'</a>'; ?>
					  </td>
					  <td <?php echo $link; ?> class="dataTableContent" align="left"><?php echo $orders['total']; ?> � 
 					  	<?php // Code qui affiche le montant du coupon d'une commande.
						if($orders['remise']!=0.00){
							echo '<font color="RED">' . $orders['remise'] .'</font>';
						}?>
					  </td>
                      
                      <?php
					  $marge_couleur = "style=\"color: #000; font-weight: normal;\"";
                      if ($orders["marge"] < $marge_orange)
						  $marge_couleur = "style=\"color: #F60; font-weight: bold;\"";
					  if ($orders["marge"] < $marge_rouge)
						  $marge_couleur = "style=\"color: #F00; font-weight: bold;\"";
					  ?>
                      <td class="dataTableContent" align="left" <?php echo $marge_couleur; ?>><?php echo $orders["marge"];?> �</td>
					  <td <?php echo $link; ?> class="dataTableContent" align="left"><?php echo tep_datetime_short($orders['date_purchased']); ?></td>
					  <td <?php echo $link; ?> class="dataTableContent" align="center">				
						<?php // Code pour mettre des icones en fontion du mode de paiement 
							if(substr_count($orders['payment_method'],"Carte")>0){
								echo "<img src='images/icons/carte_bleue.gif' title='Paiement CB'>";
							}
							else if(substr_count(strtoupper($orders['payment_method']),strtoupper("Ch�que"))>0){
								echo "<img src='images/icons/cheque.gif' title='Paiement Ch�que'>";
							}
							else if(substr_count($orders['payment_method'],"Virement")>0){
								echo "<img src='images/icons/virement.gif' title='Paiement Virement'>";
							}
							else if(substr_count($orders['payment_method'],"PayPal")>0){
								echo "<img src='images/icons/paypal.gif' title='Paiement Paypal'>";
							}
							else if(substr_count($orders['payment_method'],"Esp�ce")>0){
								echo "<img src='images/icons/espece.gif' title='Paiement Esp�ce'>";
							}
							else if(substr_count($orders['payment_method'],"Mandat Cash")>0){
								echo "<img src='images/icons/mandat-cash.gif' title='Paiement Mandat Cash'>";
							}
							else if(substr_count($orders['payment_method'],"Porte")>0){
								echo "virtuel";
							}
						?>
					
					  </td>
					  <td <?php echo $link; ?> class="dataTableContent" align="left"><?php echo $orders['orders_status_name']; ?></td>
                      
					  <td class="dataTableContent" align="right">
						<?php echo '<a href="'.tep_href_link(FILENAME_CMD_EDIT, 'oID='.$orders['orders_id']).'&action=edit">'.tep_image(DIR_WS_IMAGES.'icons/editer.png', "Editer").'</a>';?>
    					| 
    					<?php // ICONE  POUR LA FACTURE PDF BOUTIQUE DES ACCESSOIRES 
    					echo  '<a href="/'.DIR_WS_SITE.'/compte/generation_pdf/facture.php?id_com='.$orders['orders_id'].'&cli='.md5($orders['customers_id']).'" target="_blank">' . tep_image(DIR_WS_IMAGES . 'icons/facture.gif',"Facture") . '</a>'; 
    					?>
                        |
    					<?php // ICONE POUR envoi mail 
    					echo '<a href="'.tep_href_link('cmd_list_no_customers.php', tep_get_all_get_params(array('oID', 'action')).'oID='.$orders['orders_id'].'&action=envoi_mail').'" onclick="if (window.confirm(\'Etes vous sur de vouloir envoyer la facture au client?\')){return true;} else {return false;}" title="Envoyer la facture par mail">						'. tep_image(DIR_WS_IMAGES . 'icons/facture_mail.png',"Envoyer la facture par mail") .'</a>'; ?>
                        |
						<?php echo '<a href="cmd_bon_de_livraison.php?oID='.$orders['orders_id'].'" target="_blank">' . tep_image(DIR_WS_IMAGES . 'icons/bon_de_livraison.gif',"Bon de livraison") . '</a>';?>
    					| 
    					<?php // ICONE POUR SUPPRIMER Visible UNIQUEMENT pour le compte administrateur 
							if ($orders['orders_numero_facture'] == 0 || $myAccount["admin_groups_id"]== 1) {
								echo '<a href="'.tep_href_link('cmd_list_no_customers.php', tep_get_all_get_params(array('oID', 'action')).'oID='.$orders['orders_id'].'&action=deleteconfirm').'" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer la commande : '.$orders['orders_id'].' (DEFINITIVEMENT) ?\')){return true;} else {return false;}">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>';
							} else {
								echo tep_image(DIR_WS_IMAGES . 'icons/supprimer_inactif.png', "Impossible de supprimer");
							}
						?>
					  </td>
					</tr>
		<?php
		
        $batch_order_numbers[] = $orders['orders_id'];
	}?>
					</tbody>
                    </table>
                    
                    </td>
            </tr>
			</form>
    		<tr>
            	<td colspan="12">
                  	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
          			<tr>
                      <td class="smallText" width="33%"><?php echo $orders_split->display_count($orders_query_numrows, $Nombre_de_ligne_par_page, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS); ?></td>
                      <td class="smallText" width="33%" align="center"><?php echo $orders_split->display_links($orders_query_numrows, $Nombre_de_ligne_par_page, MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page'], tep_get_all_get_params(array('page', 'oID', 'action'))); ?></td>
                      <td class="smallText" width="33%">&nbsp;</td>
                  	</tr>
                	</table>
                </td>
            </tr>
            </table></td>
          	</tr>
          </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?><br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>