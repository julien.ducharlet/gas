<?php
  require('includes/application_top.php');
  require(DIR_WS_CLASSES . 'currencies.php');

if (!isset($_REQUEST['cPath'])){ 
	//header('Location: categorie_list.php');
}
/////sami//////////

  $currencies = new currencies();

  $action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
  
  if (isset($_GET['cPath'])) $cPath=$_GET['cPath'];
  // If the action will affect the cache entries
  if ( eregi("(insert|update|setflag)", $action) ) include_once('includes/reset_seo_cache.php');

  if (tep_not_null($action)) {
	  
    switch ($action) {
		
    case 'beginsort':
        $sorting = true;
        tep_redirect(tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $HTTP_POST_VARS['cPath']));
        break;
    case 'setsortorder':
		for ($i=0, $n=sizeof($HTTP_POST_VARS['products_id']); $i<$n; $i++) {
        	tep_set_product_sort_order($HTTP_POST_VARS['products_id'][$i], $HTTP_POST_VARS['sortorder'][$i]);
        }
        $sorting = false;
        tep_redirect(tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $HTTP_POST_VARS['cPath']));
        break;
	case 'setflag':
    	if ( ($HTTP_GET_VARS['flag'] == '0') || ($HTTP_GET_VARS['flag'] == '1') ) {
        	if (isset($HTTP_GET_VARS['pID'])) {
            	tep_set_product_status($HTTP_GET_VARS['pID'], $HTTP_GET_VARS['flag']);
          	}
			
			if($HTTP_GET_VARS['flag'] == '0') {
				
				tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where products_id = '" . $cPath . "'");
			}
			
          	if (USE_CACHE == 'true') {
            	tep_reset_cache_block('categories');
            	tep_reset_cache_block('also_purchased');
          	}
        }
        tep_redirect(tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $HTTP_GET_VARS['cPath'] . '&pID=' . $HTTP_GET_VARS['pID']));
        break;
		
	case 'delete_product_confirm':
		
		$i = 0;
		$continueToDelete = false;
		
		$product_id = array();
		$product_categories = array();
		
		// suppression d'un article dans plusieurs cat�gories
    	if ( isset($_POST['products_id']) && !empty($_POST['products_id']) && isset($_POST['product_categories']) && is_array($_POST['product_categories']) ) {
			
          	$product_id[] = tep_db_prepare_input($_POST['products_id']);
          	$product_categories = $_POST['product_categories'];
			
			$continueToDelete = true;
		} // suppression de plusieurs articles dans une m�me cat�gorie
		elseif( isset($_POST['products_id']) && is_array($_POST['products_id']) && isset($_GET['cPath']) && !empty($_GET['cPath']) && !isset($_POST['products_categories']) ) {
			
			$product_id = tep_db_prepare_input($_POST['products_id']);
          	$temp_categorie = explode("_", $_GET['cPath']);
			$product_categories[] = $temp_categorie[sizeof($temp_categorie)-1];
			
			$continueToDelete = true;
		}
		else $continueToDelete = false;
		
		if($continueToDelete) {
			
			for($j=0 ; $j<sizeof($product_id); $j++) {
				
          		for ($i=0 ; $i<sizeof($product_categories) ; $i++) {
					
					tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$product_id[$j] . "' and categories_id = '" . (int)$product_categories[$i] . "'");
					
					$parent_id_query = tep_db_query("SELECT parent_id FROM " . TABLE_CATEGORIES . " WHERE categories_id = '" . (int)$product_categories[$i] . "'");
					$parent_id = tep_db_fetch_array($parent_id_query);
					
					$cPath = (int)$product_id[$j] . "_" . $parent_id['parent_id'] . "_" . (int)$product_categories[$i];
					
					tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where products_id = '" . $cPath . "'");
          		}
			
				$product_to_categories_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$product_id[$j] . "'");
				$product_to_categories = tep_db_fetch_array($product_to_categories_query);
				
				if ($product_to_categories['total'] == '0') {
					
					// code pour supprimer les images
					$query = 'select products_bimage as default_image from '. TABLE_PRODUCTS .' where products_id='. (int)$product_id[$j];
					$query = tep_db_query($query);
					
					$data = tep_db_fetch_array($query);
					
					//on v�rifie que l'image du produit � supprimer n'est pas l'image par d�faut
					if($data['default_image']!= DEFAULT_IMAGE) {
						
						/*$query_count = 'select products_id from '. TABLE_PRODUCTS .' where products_bimage=\''. $data['default_image'] .'\' and products_id!='. $product_id;
						$query_count = tep_db_query($query_count);
						
						if(tep_db_num_rows($query_count) == 0) {
							
							if(is_file(DIR_FS_PRODUCTS_IMAGES ."grande/". $data["default_image"])) unlink(DIR_FS_PRODUCTS_IMAGES ."grande/". $data['default_image']);
							if(is_file(DIR_FS_PRODUCTS_IMAGES ."normale/". $data["default_image"])) unlink(DIR_FS_PRODUCTS_IMAGES ."normale/". $data['default_image']);
							if(is_file(DIR_FS_PRODUCTS_IMAGES ."petite/". $data["default_image"])) unlink(DIR_FS_PRODUCTS_IMAGES ."petite/". $data['default_image']);
							if(is_file(DIR_FS_PRODUCTS_IMAGES ."mini/". $data["default_image"])) unlink(DIR_FS_PRODUCTS_IMAGES ."mini/". $data['default_image']);
							if(is_file(DIR_FS_PRODUCTS_IMAGES ."micro/". $data["default_image"])) unlink(DIR_FS_PRODUCTS_IMAGES ."micro/". $data['default_image']);
						}*/
					}
					
					$query_image = 'select products_bimage from '. TABLE_PRODUCTS_IMAGE .' where products_id='. (int)$product_id[$j];
					$query_image = tep_db_query($query_image);
					
					while($data_image = tep_db_fetch_array($query_image)) {
						
						if($data_image['products_bimage']!= DEFAULT_IMAGE) {
							
							/*$query_count = 'select products_id from '. TABLE_PRODUCTS_IMAGE .' where products_bimage=\''. $data_image['products_bimage'] .'\' and products_id!='. $product_id;
							$query_count = tep_db_query($query_count);
							
							if(tep_db_num_rows($query_count) == 0) {
								
								// on suppprime les images annexes
								if(is_file(DIR_FS_PRODUCTS_IMAGES ."grande/". $data_image["products_bimage"])) unlink(DIR_FS_PRODUCTS_IMAGES ."grande/". $data_image['products_bimage']);
								if(is_file(DIR_FS_PRODUCTS_IMAGES ."normale/". $data_image["products_bimage"])) unlink(DIR_FS_PRODUCTS_IMAGES ."normale/". $data_image['products_bimage']);
								if(is_file(DIR_FS_PRODUCTS_IMAGES ."micro/". $data_image["products_bimage"])) unlink(DIR_FS_PRODUCTS_IMAGES ."micro/". $data_image['products_bimage']);
							}*/
						}
					}
					// fin de suppression de l'image
					
					
				   	tep_remove_product($product_id[$j]);
				}
			}
		}
		
        if (USE_CACHE == 'true') {
			
          tep_reset_cache_block('categories');
          tep_reset_cache_block('also_purchased');
        }
		
		$messageStack->add_session('Success : Suppression des articles bien effectu�e', 'success');
		
        tep_redirect(tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $_GET['cPath']));
        break;
		
	case 'move_product_confirm':
		
        $products_id = tep_db_prepare_input($HTTP_POST_VARS['products_id']);
        $new_parent_id = tep_db_prepare_input($HTTP_POST_VARS['move_to_category_id']);
		
		if($new_parent_id > 0) {
			
			$duplicate_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$products_id . "' and categories_id = '" . (int)$new_parent_id . "'");
			$duplicate_check = tep_db_fetch_array($duplicate_check_query);
			if ($duplicate_check['total'] < 1) tep_db_query("update " . TABLE_PRODUCTS_TO_CATEGORIES . " set categories_id = '" . (int)$new_parent_id . "' where products_id = '" . (int)$products_id . "' and categories_id = '" . (int)$current_category_id . "'");
	
			if (USE_CACHE == 'true') {
			  tep_reset_cache_block('categories');
			  tep_reset_cache_block('also_purchased');
			}
		}
		
		tep_redirect(tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $new_parent_id . '&pID=' . $products_id));
		
        break;
		
	case 'copy_to_confirm':
        if (isset($HTTP_POST_VARS['products_id']) && isset($HTTP_POST_VARS['categories_id'])) {
        	
			$products_id = tep_db_prepare_input($HTTP_POST_VARS['products_id']);
        	$categories_id = tep_db_prepare_input($HTTP_POST_VARS['categories_id']);
			
			if($categories_id > 0) {
			
				if ($HTTP_POST_VARS['copy_as'] == 'link') {
					if ($categories_id != $current_category_id) {
					  
					  $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$products_id . "' and categories_id = '" . (int)$categories_id . "'");
					  $check = tep_db_fetch_array($check_query);
					  
					  if ($check['total'] < '1') {
						tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id . "', '" . (int)$categories_id . "')");
					  }
					}
					else {
					  $messageStack->add_session('Erreur : Impossible de lier des produits dans la m&ecirc;me cat&eacutegorie.', 'error');
					}
				}
				elseif ($HTTP_POST_VARS['copy_as'] == 'duplicate') {			
						
					$product_query = tep_db_query("select products_model, products_bimage, products_video , products_video_preview, products_price, products_cost, products_date_available, products_weight, products_tax_class_id, manufacturers_id, family_id, family_client_id, products_quantity_min, products_quantity_max, products_quantity_ideal from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
					
					
					$product = tep_db_fetch_array($product_query);
					
					tep_db_query("insert into " . TABLE_PRODUCTS . " (products_model, products_video , products_video_preview, products_price, products_cost, products_date_added, products_date_available, products_weight, products_status, products_tax_class_id, manufacturers_id, family_id, family_client_id, products_quantity_min, products_quantity_max, products_quantity_ideal) values ('" . 			
					
					tep_db_input($product['products_model']) . "', '" . 
					
					
					
					
					tep_db_input($product['products_video']) . "', '".
					tep_db_input($product['products_video_preview']) . "', '".						
					
					tep_db_input($product['products_price']) . "', '" . 
					tep_db_input($product['products_cost']) . "', now(), '" .
					tep_db_input($product['products_date_available']) . "', '" . 
					tep_db_input($product['products_weight']) . "', '0', '" . 
					(int)$product['products_tax_class_id'] . "', '" . 
					(int)$product['manufacturers_id'] . "', '" .
					(int)$product['family_id'] . "', '" .
					(int)$product['family_client_id'] . "', 1, 1, 1)");
					$dup_products_id = tep_db_insert_id();
					
					//ins�rer le produit dans la table des produits par quantit�
					tep_db_query("insert into ". TABLE_PRODUCTS_PRICE_BY_QUANTITY ." (products_id, part_price_by_1, pro_price_by_1, rev_price_by_1, adm_price_by_1, 
												  part_price_by_5, pro_price_by_5, rev_price_by_5, adm_price_by_5, 
												  part_price_by_10, pro_price_by_10, rev_price_by_10, adm_price_by_10) 
					values (". (int)$dup_products_id .", '". tep_db_input($product['products_price']) ."', '". tep_db_input($product['products_price']) ."', '". tep_db_input($product['products_price']) ."', '". tep_db_input($product['products_price']) ."'
																																																									  , '". tep_db_input($product['products_price']) ."', '". tep_db_input($product['products_price']) ."', '". tep_db_input($product['products_price']) ."', '". tep_db_input($product['products_price']) ."'
																																																									  , '". tep_db_input($product['products_price']) ."', '". tep_db_input($product['products_price']) ."', '". tep_db_input($product['products_price']) ."', '". tep_db_input($product['products_price']) ."')");
					
					
					
					
					$description_query = tep_db_query("select language_id, products_name, products_description, products_head_title_tag, products_head_desc_tag, products_head_keywords_tag, products_url, products_baseline, balise_title_lien_image, balise_title_lien_texte from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$products_id . "'");
					while ($description = tep_db_fetch_array($description_query)) {
					tep_db_query("insert into " . TABLE_PRODUCTS_DESCRIPTION . " (products_id, language_id, products_name, products_description, products_head_title_tag, products_head_desc_tag, products_head_keywords_tag, products_url, products_viewed, products_baseline, balise_title_lien_image, balise_title_lien_texte) values ('" . (int)$dup_products_id . "', '" . (int)$description['language_id'] . "', '" . tep_db_input($description['products_name']) . "', '" . tep_db_input($description['products_description']) . "', '" . tep_db_input($description['products_head_title_tag']) . "', '" . tep_db_input($description['products_head_desc_tag']) . "', '" . tep_db_input($description['products_head_keywords_tag']) . "', '" . tep_db_input($description['products_url']) . "', '0', '" . tep_db_input($description['products_baseline']) ."', '". tep_db_input($description['balise_title_lien_image']) ."', '". tep_db_input($description['balise_title_lien_texte']) ."')");
					}
					// Commenter car quand on duplique ca mettais les images secondaires sur le nouvel article
					/*$image_query = tep_db_query("select products_bimage from " . TABLE_PRODUCTS_IMAGE . " where products_id = '" . (int)$products_id . "'");
					while ($image = tep_db_fetch_array($image_query)) {
					tep_db_query("insert into " . TABLE_PRODUCTS_IMAGE . " (products_id, products_bimage) values ('" . (int)$dup_products_id . "', '" . tep_db_input($image['products_bimage']) . "')");
					}*/
					
					tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$dup_products_id . "', '" . (int)$categories_id . "')");
					$products_id = $dup_products_id;
					}
					
					if (USE_CACHE == 'true') {
						
						tep_reset_cache_block('categories');
						tep_reset_cache_block('also_purchased');
					}
				}
		}

        tep_redirect(tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $categories_id . '&pID=' . $products_id));
        break;
    }
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/stylesheet_line.css">
<!-- AJAX Attribute Manager  -->
<?php require_once( 'attributeManager/includes/attributeManagerHeader.inc.php' )?>
<!-- AJAX Attribute Manager  end -->
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/metadata.js"></script>
<script type="text/javascript" language="javascript">

$(document).ready(function() {
	
	$(".tablesorter").tablesorter();
	
	$(".checkAll").change(function() {
		
		classToCheck = $(this).val();
		
		if($(this).attr("checked")) {
			
			$("."+ classToCheck).attr("checked", true);
		}
		else {
			
			$("."+ classToCheck).attr("checked", false);
		}
	});
	
	$("#sendData").click(function() {
		
		switch($("#Action_orders").val()) {
			case "copy" :
				
				var products = "";
				
				$(".productChecked:checked").each(function(i) {
					
					products += (i == 0) ? $(this).val() : " "+ $(this).val();
				});
				
				$("form[name=myForm]").attr("action", "article_duplicate_to_cat.php?pID="+ products);
				$("form[name=myForm]").submit();
				break;
			
			case "delete" :
				
				if(confirm("�tes-vous s�r de vouloir effacer ces articles de la cat�gorie actuelle ?")) {
					
					$("form[name=myForm]").attr("action", "article_list.php?action=delete_product_confirm&cPath=<?php echo $_GET['cPath']; ?>");
					$("form[name=myForm]").submit();
					//alert($("form[name=myForm]").attr("action"));
				}
				else return false;
				break;
			
			case "move" :
				alert('pas encore g�r�');
				//$("form[name=myForm]").attr("action", "article_duplicate_to_cat.php?pID="+ products);
				break;
		}
	});
});
</script>

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<div id="spiffycalendar" class="text"></div>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<div id="global">
<!-- body //-->
<?php include('includes/article_filtre.php'); ?>
<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td valign="top">
		<form action="article_list.php?action=setsortorder" method="post" name="myForm"
            <input type="hidden" name="cPath" value="<?php echo $cPath; ?>">
            
<?php
$products_count = 0;

		
if (isset($HTTP_GET_VARS['search_article'])) {
	
	$query_search = ' and (products_name like \'%'. addslashes($_GET['search_article']) .'%\' or p.products_model like \'%' . addslashes($_GET['search_article']) . '%\')';
	
	$products_query = tep_db_query("select p.products_id, pd.products_name, pd.products_url, p.products_model, p.products_quantity, p.products_quantity_reel, p.products_bimage, p.products_video, p.products_video_preview, p.products_price, p.products_cost, p.products_date_added, p.products_last_modified, p.products_date_available, p.products_status, p.products_sort_order, p.products_manage_stock, p2c.categories_id, c.parent_id, cr.rubrique_id, s.specials_new_products_price, pvf.prix_vente
								   
								   from " . TABLE_PRODUCTS . " p
								   left join ". TABLE_SPECIALS ." as s on (p.products_id=s.products_id and (expires_date>NOW() or expires_date='0000-00-00 00:00:00'))
								   left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash='0000-00-00'))),
								" . TABLE_PRODUCTS_DESCRIPTION . " pd,
								" . TABLE_PRODUCTS_TO_CATEGORIES . " p2c,
								" . TABLE_CATEGORIES . " c,
								". TABLE_CATEGORIES_RAYON ." cr
								
								   where p2c.categories_id = c.categories_id
								   and p.products_id = pd.products_id
								   and pd.language_id = '" . (int)$languages_id . "'
								   and p.products_id = p2c.products_id
								   and cr.categories_id=c.parent_id
								   ". $query_search ."
								   order by p.products_sort_order, pd.products_name");
} 
else {
				
	$products_query = tep_db_query("select p.products_id, pd.products_name, pd.products_url, p.products_model, p.products_quantity, p.products_quantity_reel, p.products_bimage, p.products_video, p.products_video_preview, p.products_price, p.products_cost, p.products_date_added, p.products_last_modified, p.products_date_available, p.products_status, p.products_sort_order, p.products_manage_stock, s.specials_new_products_price, pvf.prix_vente, p2c.categories_id, c.parent_id, cr.rubrique_id
								   
								   from " . TABLE_PRODUCTS . " p
								   left join ". TABLE_SPECIALS ." as s on (p.products_id=s.products_id and (expires_date>NOW() or expires_date='0000-00-00 00:00:00'))
								   left join ". TABLE_PRODUCTS_VENTES_FLASH ." as pvf on (p.products_id=pvf.products_id and (debut_vente_flash<=NOW() and (fin_vente_flash >NOW() or fin_vente_flash='0000-00-00'))),
								   " . TABLE_PRODUCTS_DESCRIPTION . " pd,
								   " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c,
								   ". TABLE_CATEGORIES ." c,
								   ". TABLE_CATEGORIES_RAYON ." cr
								   
								   where p.products_id = pd.products_id
								   and pd.language_id = '" . (int)$languages_id . "'
								   and p.products_id = p2c.products_id
								   and p2c.categories_id = '" . (int)$current_category_id . "'
								   and p2c.categories_id = c.categories_id
								   and c.parent_id=cr.categories_id
								   
								   order by p.products_sort_order, pd.products_name");
}
			
if(tep_db_num_rows($products_query)>0) {?>

    <table border="0" width="100%" cellspacing="0" cellpadding="2" class="tablesorter">
    <thead>
    <tr>
        <th class="{sorter:false}"><input type="checkbox" value="productChecked" class="checkAll"></th>
        <th>Articles</th>
        <th align="center">R�f�rence</th>
        <th align="center">Date d'ajout</th>
        <th align="center">Infos</th>
        <th align="center">Prix d'achat</th>
        <th align="center">Prix de vente</th>
        <th align="center">Marge</th>
        <th align="center">Vente Moyenne</th>
        <th align="center">Stock R�el</th>
        <th align="center">Stock Virtuel</th>
        <th align="center">Statut</th>
        <th class="{sorter:false}" align="center">Action</th>
    </tr>
    </thead><?php
}


$tab_url_cpath=explode("_", $cPath);
$res_rayon=tep_db_query("select rubrique_id  from ".TABLE_CATEGORIES_RAYON." where categories_id='".$tab_url_cpath[0]."'");
$row_rayon=tep_db_fetch_array($res_rayon);
$query_url="SELECT cd.categories_id as id_modele, cd.categories_url as url_modele, rub.rubrique_id, rub.rubrique_name, rub.rubrique_url, cd2.categories_id as id_marque, cd2.categories_url as url_marque
			FROM " . TABLE_CATEGORIES . " as c,
				 " . TABLE_CATEGORIES_RAYON . " as cr,
				 " . TABLE_CATEGORIES_DESCRIPTION . " as cd,
				 " . TABLE_CATEGORIES_DESCRIPTION . " as cd2,
				 " . TABLE_RAYON . " as rub
				 
			WHERE c.categories_id=cr.categories_id
			and cd.categories_id=cr.categories_id
			and cd2.categories_id=c.parent_id
			and rub.rubrique_id=cr.rubrique_id
			and c.parent_id='".$tab_url_cpath[0]."'
			and cd.categories_id='".$tab_url_cpath[1]."'
			and cr.rubrique_id='".$row_rayon["rubrique_id"]."'";
					
		$res_url=tep_db_query($query_url);
		$row_url=tep_db_fetch_array($res_url);
    	while ($products = tep_db_fetch_array($products_query)) {
			
      		$products_count++;
      		$rows++;
			
      		if (isset($HTTP_GET_VARS['search_article'])) $cPath = $products['parent_id'] . "_" . $products['categories_id'];
      		if ( (!isset($HTTP_GET_VARS['pID']) && !isset($HTTP_GET_VARS['cID']) || (isset($HTTP_GET_VARS['pID']) && ($HTTP_GET_VARS['pID'] == $products['products_id']))) && !isset($pInfo) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
      			$reviews_query = tep_db_query("select (avg(reviews_rating) / 5 * 100) as average_rating from " . TABLE_REVIEWS . " where products_id = '" . (int)$products['products_id'] . "'");
        		$reviews = tep_db_fetch_array($reviews_query);
        		$pInfo_array = array_merge($products, $reviews);
        		$pInfo = new objectInfo($pInfo_array);
      		}

      		if (!$sorting) {
				
        		if (isset($pInfo) && is_object($pInfo) && ($products['products_id'] == $pInfo->products_id) ) {
        	  		echo '              <tr id="defaultSelected" class="dataTableRowSelected">' . "\n";
    	    	} 
				else {
          			echo '              <tr>' . "\n";
        		}
      		}
	  		else{
      			if (isset($pInfo) && is_object($pInfo) && ($products['products_id'] == $pInfo->products_id) ) {
          			echo '              <tr id="defaultSelected" class="dataTableRowSelected">' . "\n";
        		} 
				else {
          			echo '              <tr>' . "\n";
        		}
			}
?>
            	<td class="dataTableContent">
					<input id="list" type="checkbox" class="productChecked" value="<?php echo $products['products_id']; ?>" name="products_id[]"/>
				</td>
				<td class="dataTableContent">
				<?php
					$id_modele = explode("_", $cPath);
					$products['products_name']=bda_product_name_transform($products['products_name'],$id_modele[1]);				
					echo $products['products_name']; 
				?>
				</td>
        		<td class="dataTableContent" align="left"><?php echo $products['products_model']; ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_date_short($products['products_date_added']); ?>&nbsp;
				<img src="..<?php echo DIR_WS_ADMIN; ?>images/question.gif" width="18" height="18" title="Derni�re modification : <?php echo tep_date_short($products['products_last_modified']); ?>" class="infos">
				</td>
				<td class="dataTableContent" align="center">
                	<?php
					if(!empty($products['prix_vente'])) { ?>
						
						<img src="..<?php echo DIR_WS_ADMIN; ?>images/icons/icone_vente_flash.jpg" width="76" height="20" alt="Vente Flash" title="<?php echo round($products['prix_vente'], 2); ?> &euro;">
                    <?php
                    }
					elseif($products['products_manage_stock'] == 'destock_visible') { ?>
						
						<img src="..<?php echo DIR_WS_ADMIN; ?>images/icons/icone_vente_destockage.jpg" width="76" height="20" alt="D�stockage">
                    <?php
                    }
					elseif(!empty($products['specials_new_products_price'])) { ?>
						
						<img src="..<?php echo DIR_WS_ADMIN; ?>images/icons/icone_vente_promo.jpg" width="76" height="20" alt="PROMO" title="<?php echo round($products['specials_new_products_price'], 2); ?> &euro;">
                    <?php } ?>
				</td>
				
                <td class="dataTableContent" align="center"><?php echo $currencies->format($products['products_cost']); ?></td>
                <td class="dataTableContent" align="center"><?php echo $currencies->format($products['products_price']); ?></td>
                <td class="dataTableContent" align="center"><strong><?php echo $currencies->format($products['products_price']-$products['products_cost']); ?></strong></td>
        		<td class="dataTableContent" align="center">
                <?php
				
					//Changement du syst�me de calcul de moyenne
					$timestamp_debut = mktime(0, 0, 0, date("m")-11, 1, date("Y"));
					$timestamp_fin = time();
					
					$date_debut = date("Y-m-01 00:00:00", $timestamp_debut);
					$date_fin = date("Y-m-t 23:59:59", $timestamp_fin);
					
					$date_ajout = $products['products_date_added'];
					
					$timestamp_ajout = strtotime($date_ajout);
					
					$date_ajout_ts = date("Y-m-d 00:00:00", $timestamp_ajout);
					
					$nbr_mois = floor(($timestamp_ajout - $timestamp_debut)/(3600*24*30));
					
					$requete = "select SUM(products_quantity) as Total from orders O, orders_products P where P.orders_id=O.orders_id and products_id='" . $products['products_id'] . "' and date_purchased > '" . $date_debut . "' and date_purchased < '".$date_fin."' ";
					$order_query = mysql_query($requete);	 
					$total = mysql_fetch_array($order_query);
					
					if ($nbr_mois <= 0)
						$moyenne = $total['Total']/(12);
					else if ($nbr_mois == 12)
						$moyenne = 0;
					else
						$moyenne = $total['Total']/(12-$nbr_mois);
						
					$moyenne_arr = round($moyenne, 2);
									
					echo $moyenne_arr ;
				?>
                </td>
        		<td class="dataTableContent" align="center"> 
				<?php
				if ($products['products_quantity_reel']<=$moyenne_arr) {
					echo '<font size="2" color="red"><b>' . $products['products_quantity_reel'] . '</b></font>';
				} 
				else {
					echo '<font color="green">' . $products['products_quantity_reel'] . '</font>';
				}?>
				</td>
                <td class="dataTableContent" align="center"> 
				<?php
				if ($products['products_quantity']<=$moyenne_arr) {
					echo '<font size="2" color="red"><b>' . $products['products_quantity'] . '</b></font>';
				} 
				else {
					echo '<font color="green">' . $products['products_quantity'] . '</font>';
				}?>
				</td>
				<td class="dataTableContent" align="center">
					<?php
                    if ($products['products_status'] == '1') {
                        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_ARTICLE_LIST, 'action=setflag&flag=0&pID=' . $products['products_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
                    }	 
                    else {
                        echo '<a href="' . tep_href_link(FILENAME_ARTICLE_LIST, 'action=setflag&flag=1&pID=' . $products['products_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                    }?>
                </td>
				<td class="dataTableContent" align="center">
	          <?php 
					// Editer
					echo '<a href="' . tep_href_link(FILENAME_ARTICLE_EDIT, 'cPath=' . $cPath . '&pID=' . $products['products_id'] . '&action=new_product') . '">' . tep_image(DIR_WS_IMAGES . '/icons/editer.png', "Editer") . '</a>&nbsp;&nbsp;'; 
					// Copier
					echo '<a href="' . tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $cPath . '&pID=' . $products['products_id'] . '&action=copy_to') . '">' . tep_image(DIR_WS_IMAGES . '/icons/copier.png', "Copier le produit") . '</a>&nbsp;&nbsp;';
					// D�placer
					echo '<a href="' . tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $cPath . '&pID=' . $products['products_id'] . '&action=move_product') . '">' . tep_image(DIR_WS_IMAGES . '/icons/deplacer.png', "D�placer le produit") . '</a>&nbsp;&nbsp;'; 
					// Effacer
					echo '<a href="' . tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $cPath . '&pID=' . $products['products_id'] . '&action=delete_product') . '">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>&nbsp;&nbsp;'; 
					// Voir l'article sur le site client
					$tabCpath=explode("_",$cPath);
					//$url_clean = url("article",array('id_rayon' => "1", 'nom_rayon' => "toto", 'id_marque' => $tabCpath[0], 'url_marque' => "toto", 'id_modele' => $tabCpath[1], 'url_modele' => "bingo", 'id_article' => $products['products_id'], 'url_article' => "prod"));
					
					//echo '<a href="'.$url_clean.'" target="_blank">' .  tep_image(DIR_WS_IMAGES . 'icons/visualiser.png', "visualiser l'article sur le site client")  . '</a>';
					$url_clean = WEBSITE . BASE_DIR .'/produits_fiches.php?rayon='. $products['rubrique_id'] .'&marque='. $products['parent_id'] .'&modele='. $products['categories_id'] .'&produit='. $products['products_id'];
					echo '<a href="'. $url_clean .'" target="_blank">' .  tep_image(DIR_WS_IMAGES . 'icons/visualiser.png', "visualiser l'article sur le site client")  . '</a>';
				  ?>
        		</td>
              </tr>
<?php
    }//end while products
	?>
    
    </tbody>
    </table><?php
	
    $cPath_back = '';
	if (sizeof($cPath_array) > 0) {
    	for ($i=0, $n=sizeof($cPath_array)-1; $i<$n; $i++) {
        	if (empty($cPath_back)) {
          		$cPath_back .= $cPath_array[$i];
        	} 
			else {
          		$cPath_back .= '_' . $cPath_array[$i];
        	}
      	}
    }

    $cPath_back = (tep_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
	
	if(!isset($_GET['search_article']) && isset($_GET['cPath']) && !empty($_GET['cPath'])) {
		
?>
		<table width="100%">
			<tr>
				<td>&nbsp;
					
				</td>
			</tr>
            
			<tr>
				<td>
					Action sur les articles s�l�ctionn�es :
					<select id="Action_orders" name="Action_orders" size="1">
						<option value="copy">Copier les articles dans une autre categorie.</option>
						<option value="delete">Supprimer les articles.</option>
						<option value="move">D�placer les articles. (EN COURS)</option>
					</select>
					<input type="button" border="0" align="top" title="Confirmer" alt="Confirmer" id="sendData" class="confirm"/>
				</td>
			</tr>
<?php } ?>
			<tr>
				<td>&nbsp;
					
				</td>
			</tr>
			<tr>
				<td>
				
					<table border="0" width="100%" cellspacing="0" cellpadding="2">
						<tr>
							<td width="25%" class="smallText"><?php echo 'Nombre d\'Articles :&nbsp;' . $products_count; ?></td>
							<td width="50%" align="center" class="smallText">
								<?php
								if($products_count==0){
									echo '<a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&action=new_category') . '">' . tep_image_button('button_new_category.gif', IMAGE_NEW_CATEGORY) . '</a>&nbsp;';
								}
								if ($sorting){
								if (sizeof($cPath_array) > 0) echo '<a href="' . tep_href_link(FILENAME_ARTICLE_LIST, $cPath_back . 'cPath=' . $cPath) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>&nbsp;'; if (!isset($HTTP_GET_VARS['search_article'])) echo tep_image_submit('button_save_sort.gif', 'Sauver l\'ordre de tri') . '&nbsp;</td>';
								}else{
								
								if (!isset($HTTP_GET_VARS['search_article'])) echo '<a href="' . tep_href_link(FILENAME_ARTICLE_EDIT, 'cPath=' . $cPath . '&action=new_product') . '">' . tep_image_button('button_new_product.gif', IMAGE_NEW_PRODUCT) . '</a>&nbsp;'; if (sizeof($cPath_array) > 0) echo '<a href="' . tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $cPath . '&action=beginsort') . '">' . tep_image_button('button_sort_order.gif', 'Editer Ordre de tri') . '</a>&nbsp;
								   <input type="hidden" name="c_path_f" value="'.$cpath.'" />
								   <input type="hidden" name="c_path" value="'.$c.'" /></td>';
								}
								?>
							<td width="25%">&nbsp;</td>
						</tr>
					</table>
				
				
				</td>
              </tr>
            </table></form></td>
<?php
    $heading = array();
    $contents = array();
    switch ($action) {
		
		case 'delete_product':
			$heading[] = array('text' => '<b>Supprimer l\'article</b>');
			if(isset($_GET['pID']) && !empty($_GET['pID'])) {
				
				$contents = array('form' => tep_draw_form('products', FILENAME_ARTICLE_LIST, 'action=delete_product_confirm&cPath=' . $cPath) . tep_draw_hidden_field('products_id', $_GET['pID']));
			}
			else {
				
				$contents = array('form' => tep_draw_form('products', FILENAME_ARTICLE_LIST, 'action=delete_product_confirm&cPath=' . $cPath) . tep_draw_hidden_field('products_id', $pInfo->products_id));
			}
			$contents[] = array('text' => 'Etes vous sur de vouloir supprimer d�finitivement cet article des cat�gories ?<br /><br />Vous devez cocher les cases des cat�gories o� vous souhaitez effacer cet article !');
			$contents[] = array('text' => '<br />');
			$contents[] = array('text' => '<b>' . $pInfo->products_name . '</b><br/><br/><input type="checkbox" value="productToDeleteChecked" class="checkAll">&nbsp;Tout cocher/ Tout d�cocher');
			
			$product_categories_string = '';
			$product_categories = tep_generate_category_path($_GET['pID'], 'product');
			
			for ($i = 0, $n = sizeof($product_categories); $i < $n; $i++) {
				
				$category_path = '';
				
				for ($j = 0, $k = sizeof($product_categories[$i]); $j < $k; $j++) {
					
					$category_path .= $product_categories[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
				}
				
				$category_path = substr($category_path, 0, -16);
				$product_categories_string .= tep_draw_checkbox_field('product_categories[]', $product_categories[$i][sizeof($product_categories[$i])-1]['id'],false, '', 'class="productToDeleteChecked"') . '&nbsp;' . $category_path . '<br />';
			}
			
			$product_categories_string = substr($product_categories_string, 0, -4);
			
			$contents[] = array('text' => '<br />' . $product_categories_string);
			$contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
			break;
	
	// DEPLACER UN ARTICLE
    case 'move_product':
        $heading[] = array('text' => '<b>D�placer l\'article</b>');
        $contents = array('form' => tep_draw_form('products', FILENAME_ARTICLE_LIST, 'action=move_product_confirm&cPath=' . $cPath) . tep_draw_hidden_field('products_id', $pInfo->products_id));
        $contents[] = array('text' => sprintf('Merci de s�lectionner la cat�gorie ou vous voudriez que <b>%s</b> soit plac�', $pInfo->products_name));
        $contents[] = array('text' => '<br />Cat�gories o� est pr�sent l\'article : <br /><br /><b>' . tep_output_generated_category_path($pInfo->products_id, 'product') . '</b>');
        $contents[] = array('text' => '<br />' . sprintf('D�placer <b>%s</b> vers :', $pInfo->products_name) . '<br />' . tep_draw_pull_down_menu('move_to_category_id', tep_get_full_category_tree(), $current_category_id));
        $contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_move.gif', IMAGE_MOVE) . ' <a href="' . tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
		
	// DUPLIQUER LE PRODUIT
    case 'copy_to':
        $heading[] = array('text' => '<b>Copier vers</b>');
        $contents = array('form' => tep_draw_form('copy_to', FILENAME_ARTICLE_LIST, 'action=copy_to_confirm&cPath=' . $cPath) . tep_draw_hidden_field('products_id', $pInfo->products_id));
        $contents[] = array('text' => 'Veuillez choisir une nouvelle cat�gorie dans laquelle vous voulez copier cet article.');
        $contents[] = array('text' => '<br />Cat�gories o� est pr�sent l\'article : <br /><br /><b>' . tep_output_generated_category_path($pInfo->products_id, 'product') . '</b>');
        $contents[] = array('text' => '<br />Cat�gories<br />' . tep_draw_pull_down_menu('categories_id', tep_get_full_category_tree(), $current_category_id));
        $contents[] = array('text' => '<br />M�thode de copie : <br />' . tep_draw_radio_field('copy_as', 'link', true) . ' Lien article <br />' . tep_draw_radio_field('copy_as', 'duplicate') . ' Dupliquer l\'article');
        $contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_copy.gif', IMAGE_COPY) . ' <a href="' . tep_href_link(FILENAME_ARTICLE_LIST, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
    default:
        break;
    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";
      $box = new box;
      echo $box->infoBox($heading, $contents);
      echo '            </td>' . "\n";
    }?>
          </tr>
        </table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</div>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>