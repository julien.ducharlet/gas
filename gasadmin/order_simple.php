<?php
/*
  $Id: order_simple.php,v 1.112 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');

  include(DIR_WS_CLASSES . 'order.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php
  require(DIR_WS_INCLUDES . 'header.php');
?>
<!-- header_eof //-->

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<td width="100%" valign="top">
<!-- body //-->
<table border="0" width="100%">
<tr><td class="pageHeading" colspan="4"> Les derni�res Commande</td></tr>
<tr class="dataTableHeadingRow">
	<td class="dataTableHeadingContent">Num�ro de commande</td>
	<td class="dataTableHeadingContent">Date de la commande</td>
	<td class="dataTableHeadingContent">Prix total</td>
	<td class="dataTableHeadingContent">M�thode de Paiement</td></tr>
<tr class="dataTableRow">
	
<?php

$max_orders = tep_db_query("SELECT MAX(orders_id) as total FROM orders");
$max = tep_db_fetch_array($max_orders);

$orders_query = tep_db_query("SELECT * FROM orders WHERE orders_id >  ".($max['total']-11)." ORDER BY orders_id DESC");
  while ($orders = tep_db_fetch_array($orders_query)) {
   $orders_products = tep_db_query("SELECT * FROM orders_products WHERE orders_id='".$orders['orders_id']."'");
   $price = 0;
	while($row = tep_db_fetch_array($orders_products)){
	 $price = ($row['products_quantity']*$row['final_price']) + $price;
	}
		
	 echo "<td class='dataTableContent'><a href='invoice.php?oID=".$orders['orders_id']."' target=_blank>".$orders['orders_id']."</a></td>";
	 echo "<td class='dataTableContent'>".$orders['date_purchased']."</td>";
	 echo "<td class='dataTableContent'><b>".round($price*1.2,2)." �</b></td>";
	 echo "<td class='dataTableContent'>".$orders['payment_method']."</td></tr>";
  }
  
/************** REAL PART **********************/
?>

<tr><td class="pageHeading" colspan="4"> Statistique commande <font size="-2">(Les autres statuts que "Colis envoy�")</font></td></tr>
<tr class="dataTableHeadingRow">
	<td class="dataTableHeadingContent">Nombre par Jour</td>
	<td class="dataTableHeadingContent">Nombre par Semaine</td>
	<td class="dataTableHeadingContent">Nombre par Mois</td>
	<td class="dataTableHeadingContent">Nombre par Ann�e</td>
	
</tr>
<tr class="dataTableRow">
<?php
$stats_query = tep_db_query("SELECT count(*) as nbr FROM orders WHERE date_purchased < '".date('Y-m-d H:i:s')."' AND date_purchased > ('".date('Y-m-d H:i:s'). "' - INTERVAL 1 YEAR) AND orders_status <> 4");
  $statistiques = tep_db_fetch_array($stats_query);
$total_orders_products_query = tep_db_query("SELECT sum(final_price*products_quantity) as pricing FROM orders_products INNER JOIN orders ON orders.orders_id = orders_products.orders_id WHERE orders.orders_status <> 4 AND orders.date_purchased < '".date('Y-m-d H:i:s')."' AND orders.date_purchased > ('".date('Y-m-d H:i:s'). "' - INTERVAL 1 YEAR)");
  $total_orders_products = tep_db_fetch_array($total_orders_products_query);
$total_shipping_products_query = tep_db_query("SELECT sum(value) as shipping_total FROM orders_total INNER JOIN orders ON orders.orders_id = orders_total.orders_id WHERE class='ot_shipping' AND orders.orders_status <> 4 AND orders.date_purchased < '".date('Y-m-d H:i:s')."' AND orders.date_purchased > ('".date('Y-m-d H:i:s'). "' - INTERVAL 1 YEAR)");
  $total_shipping_products = tep_db_fetch_array($total_shipping_products_query);

  echo "<td class='dataTableContent'><b>".round(($statistiques['nbr']/365),2)."</b></td>";
  echo "<td class='dataTableContent'><b>".round(($statistiques['nbr']/52),2)."</b></td>";
  echo "<td class='dataTableContent'><b>".round(($statistiques['nbr']/12),0)."</b></td>";
  echo "<td class='dataTableContent'><b>".$statistiques['nbr']."</b></td>";

?>
</tr>
<tr><td class="pageHeading" colspan="4"> Statistique Financi�re <font size="-2">(Tous les statuts)</font></font></td></tr>
<tr class="dataTableHeadingRow">
	<td class="dataTableHeadingContent">Average Order Amount</td>
	<td class="dataTableHeadingContent">Total Order Amount For Actual Year</td>
	<td class="dataTableHeadingContent">Average Shipping Amount For Actual Year</td>
	<td class="dataTableHeadingContent">Total Shipping Amount For Actual Year</td>
</tr>	
<tr class="dataTableRow">
<?php
	echo "<td class='dataTableContent'><b>".round(($total_orders_products['pricing']/$statistiques['nbr'])*1.2,2)." �</b></td>";
	echo "<td class='dataTableContent'><b>".round($total_orders_products['pricing']*1.2,2)." �</b></td>";
	echo "<td class='dataTableContent'><b>".round($total_shipping_products['shipping_total']/$statistiques['nbr'],2)." �</b></td>";
	echo "<td class='dataTableContent'><b>".round($total_shipping_products['shipping_total'],2)." �</b></td>";
	
/************************ REAL + CANCELED PART *************************/	
?>
</tr>
<tr>
	<td colspan="4">&nbsp<br />&nbsp</td>
</tr>	

<tr><td class="pageHeading" colspan="4"> Statistique commande <font size="-2">(Les autres statuts que "Colis envoy�")</font></td></tr>
<tr class="dataTableHeadingRow">
	<td class="dataTableHeadingContent">Nombre par Jour</td>
	<td class="dataTableHeadingContent">Nombre par Semaine</td>
	<td class="dataTableHeadingContent">Nombre par Mois</td>
	<td class="dataTableHeadingContent">Nombre par Ann�e</td>
	
</tr>
<tr class="dataTableRow">
<?php
$stats_query = tep_db_query("SELECT count(*) as nbr FROM orders WHERE date_purchased < '".date('Y-m-d H:i:s')."' AND date_purchased > ('".date('Y-m-d H:i:s'). "' - INTERVAL 1 YEAR)");
  $statistiques = tep_db_fetch_array($stats_query);
$total_orders_products_query = tep_db_query("SELECT sum(final_price*products_quantity) as pricing FROM orders_products INNER JOIN orders ON orders.orders_id = orders_products.orders_id WHERE orders.date_purchased < '".date('Y-m-d H:i:s')."' AND orders.date_purchased > ('".date('Y-m-d H:i:s'). "' - INTERVAL 1 YEAR)");
  $total_orders_products = tep_db_fetch_array($total_orders_products_query);
$total_shipping_products_query = tep_db_query("SELECT sum(value) as shipping_total FROM orders_total INNER JOIN orders ON orders.orders_id = orders_total.orders_id WHERE class='ot_shipping' AND orders.date_purchased < '".date('Y-m-d H:i:s')."' AND orders.date_purchased > ('".date('Y-m-d H:i:s'). "' - INTERVAL 1 YEAR)");
  $total_shipping_products = tep_db_fetch_array($total_shipping_products_query);

  echo "<td class='dataTableContent'><b>".round(($statistiques['nbr']/365),2)."</b></td>";
  echo "<td class='dataTableContent'><b>".round(($statistiques['nbr']/52),2)."</b></td>";
  echo "<td class='dataTableContent'><b>".round(($statistiques['nbr']/12),0)."</b></td>";
  echo "<td class='dataTableContent'><b>".$statistiques['nbr']."</b></td>";

?>
</tr>
<tr><td class="pageHeading" colspan="4"> Statistique Financi�re <font size="-2">(Tous les statuts)</font></td></tr>
<tr class="dataTableHeadingRow">
	<td class="dataTableHeadingContent">Montant moyen des commandes</td>
	<td class="dataTableHeadingContent">Montant Total des ventes de cette ann�e</td>
	<td class="dataTableHeadingContent">Average Shipping Amount For Actual Year</td>
	<td class="dataTableHeadingContent">Total Shipping Amount For Actual Year</td>
</tr>	
<tr class="dataTableRow">
<?php
	echo "<td class='dataTableContent'><b>".round(($total_orders_products['pricing']/$statistiques['nbr'])*1.2,2)." �</b></td>";
	echo "<td class='dataTableContent'><b>".round($total_orders_products['pricing']*1.2,2)." �</b></td>";
	echo "<td class='dataTableContent'><b>".round($total_shipping_products['shipping_total']/$statistiques['nbr'],2)." �</b></td>";
	echo "<td class='dataTableContent'><b>".round($total_shipping_products['shipping_total'],2)." �</b></td>";
?>
</tr>

</table>

<p>
<table border=0 width="100%">
<tr>
  <td class="pageHeading" colspan=2 width="100%"> Heures de commande </td>
</tr>
</table>

<table border=0 width="100%"><tr class="dataTableHeadingRow">
<?php
for ($i=0;$i<24;$i++){
 echo "<td class='dataTableHeadingContent' width='10'>".$i."h</td>";
}
echo "</tr> <tr class='dataTableRow'>";
for ($i=0;$i<24;$i++){
$hours_query = tep_db_query("SELECT count(*) as hr_tot FROM orders WHERE (DATE_FORMAT(date_purchased,'%H')/1) = '".$i."' AND date_purchased < '".date('Y-m-d H:i:s')."' AND date_purchased > ('".date('Y-m-d H:i:s'). "' - INTERVAL 1 YEAR) AND orders_status <> 4");
  $hours_res = tep_db_fetch_array($hours_query);
  $hours_final = round($hours_res['hr_tot']*100/365,0);
   if($hours_final < 3){ $bgcolor="#FCDA3C"; }elseif($hours_final < 7 and $hours_final >= 3){ $bgcolor="#FCB63C"; } elseif($hours_final < 15 and $hours_final >= 7){ $bgcolor="#FC9B3C"; } elseif($hours_final >= 15 and $hours_final < 20){ $bgcolor="#FC5C3C"; } elseif($hours_final >= 20){ $bgcolor="#D00C02"; }else { $bgcolor="#FC5C3C"; }
 echo "<td class='dataTableContent' width='10' bgcolor='".$bgcolor."' align='center'><b>".$hours_final."</b></td>";
}
?>	

</tr>
</table>
<p>
<table border=0 width="100%">
<tr>
  <td class="pageHeading" colspan=3> Statistiques Mensuelle </td>
</tr>
<tr class="dataTableHeadingRow">
<?php
echo "<td class='dataTableHeadingContent' width='".(100/14)."%'></td>";
for ($i=1;$i<13;$i++){
 echo "<td class='dataTableHeadingContent' width='".(100/14)."%'>".strftime('%B',strtotime("$i/01/2005"))."</td>";
}
echo "</tr><tr class='dataTableRow'>";
echo "<td class='dataTableContent' valign='top'><b>Real total</b><br>Real amount<br>Real avg";
 for($i=1;$i<13;$i++){
 
  // Commande non annul�es
   $orders_query = tep_db_query("SELECT count(*) as nbr FROM orders WHERE date_purchased < ('".date('Y')."-".$i."-31') AND date_purchased >  ('".date('Y')."-".$i."-01') AND orders_status <> 4 ");
     $orders = tep_db_fetch_array($orders_query);
	  if($orders['nbr'] == 0){ $orders_nbr = 1; } else { $orders_nbr = $orders['nbr'];}

	$total_orders_products_query = tep_db_query("SELECT sum(final_price*products_quantity) as pricing FROM orders_products INNER JOIN orders ON orders.orders_id = orders_products.orders_id WHERE orders.orders_status <> 4 AND orders.date_purchased < ('".date('Y')."-".$i."-31') AND orders.date_purchased >  ('".date('Y')."-".$i."-01')");
  	 $total_orders_products = tep_db_fetch_array($total_orders_products_query);
  	 
		echo "<td class='dataTableContent' valign='top'><b>".$orders['nbr']."</b><br><i>".round(($total_orders_products['pricing']*1.2)/($orders_nbr),0)."�</i><br>".round(($orders['nbr']/(date("t",mktime(0,0,0,$i,1,date('Y'))))),2)."</td>";
	$real_tab[$i] = $orders['nbr'];
 }
?>
</tr>
<?php
echo "<tr class='dataTableRow'>";
echo "<td class='dataTableContent' valign='top'><b>Total</b><br>Amount<br>Avg";
 for($i=1;$i<13;$i++){

  // Commande non annul�es
   $orders_query = tep_db_query("SELECT count(*) as nbr FROM orders WHERE date_purchased < ('".date('Y')."-".$i."-31') AND date_purchased >  ('".date('Y')."-".$i."-01')");
     $orders = tep_db_fetch_array($orders_query);
	  if($orders['nbr'] == 0){ $orders_nbr = 1; } else { $orders_nbr = $orders['nbr'];}

	$total_orders_products_query = tep_db_query("SELECT sum(final_price*products_quantity) as pricing FROM orders_products INNER JOIN orders ON orders.orders_id = orders_products.orders_id WHERE orders.date_purchased < ('".date('Y')."-".$i."-31') AND orders.date_purchased >  ('".date('Y')."-".$i."-01')");
  	 $total_orders_products = tep_db_fetch_array($total_orders_products_query);
  	
		echo "<td class='dataTableContent' valign='top'><b>".$orders['nbr']."</b><br><i>".round(($total_orders_products['pricing']*1.2)/($orders_nbr),0)."�</i><br>".round(($orders['nbr']/(date("t",mktime(0,0,0,$i,1,date('Y'))))),2)."</td>";
	if($orders['nbr']==0){ $total_tab[$i] = 1; }else{ $total_tab[$i] = $orders['nbr']; }
 }
?>
</tr>
<?php
echo "<tr class='dataTableRow'>";
echo "<td class='dataTableContent' valign='top'><b>Cancel</b><br>Percentage";
 for($i=1;$i<13;$i++){
 	echo "<td class='dataTableContent' valign='top'>".($total_tab[$i]-$real_tab[$i])."<br><font color='red'><b>".round(((($total_tab[$i]-$real_tab[$i])/$total_tab[$i])*100),0)."%</b></font></td>";
 }
?>
</tr>
</table>
<p>
<table border=0 width="100%">
<tr>
  <td class="pageHeading"> Les M&eacute;thodes de Paiement </td>
</tr>
<tr><td><table border=0>
<?php
  $orders_payments = array();
  $orders_payment_array = array();
  $orders_payment_query = tep_db_query("select distinct payment_method, count(orders_id) as total  FROM orders WHERE date_purchased > '2005-07-01 00:00:00' GROUP BY payment_method");
  while ($orders_payment = tep_db_fetch_array($orders_payment_query)) {
    echo "<tr class='dataTableRow'><td class='dataTableContent'>".$orders_payment['payment_method']."</td>".
		 "<td class='dataTableContent'>".$orders_payment['total']."</td></tr>";
  }
?>
  </td></tr>

</table>


 </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
