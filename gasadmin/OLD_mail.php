<?php
/*
  $Id: mail.php,v 1.31 2003/06/20 00:37:51 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  $action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

  if ( ($action == 'send_email_to_user') && isset($HTTP_POST_VARS['customers_email_address']) && !isset($HTTP_POST_VARS['back_x']) ) {
    switch ($HTTP_POST_VARS['customers_email_address']) {
      case '***':
        $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS);
        $mail_sent_to = 'Tous les clients';
        break;
      case '**D':
        $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_newsletter = '1'");
        $mail_sent_to = 'Tous les abonn&eacute;s au bulletin d\'informations';
        break;
      default:
	  $customers_email_address = tep_db_prepare_input($HTTP_POST_VARS['customers_email_address']);
	  
	  // On compte le nombre de @ pour savoir si il s'agit d'un envoi group�
	  $nombre_arobase = substr_count($customers_email_address,"@");
	  
	  if($nombre_arobase<2){
	    $mail_query = tep_db_query("select * from " . TABLE_CUSTOMERS . 
							" where customers_email_address = '" . tep_db_input($customers_email_address) . "'");
        $mail_sent_to = $HTTP_POST_VARS['customers_email_address'];
	  }
        break;
    }

    $from = tep_db_prepare_input($HTTP_POST_VARS['from']);
    $subject = tep_db_prepare_input($HTTP_POST_VARS['subject']);
    $message = tep_db_prepare_input($HTTP_POST_VARS['message']);

	$mimemessage = new email(array('X-Mailer: osCommerce'));
	// add the message to the object
	$mimemessage->add_text($message);
	$mimemessage->build_message();
	
	
	//***********************************************************************************
	// EXTENSION POLYETCH
	// code qui permet d'envoyer un email a plusieurs clients en meme temps
	if($nombre_arobase>1){
		$addresses=$customers_email_address.';';
		$tableau_mail = explode(';', ($addresses));
			
		foreach($tableau_mail as $mail) {
		
			$client_query = tep_db_query("select * from " . TABLE_CUSTOMERS . 
				" where customers_email_address = '" . tep_db_input($mail) . "'");
			
			while ($tuple = mysql_fetch_array($client_query)){
				$nom_client= $tuple['customers_lastname'];
				$prenom_client= $tuple['customers_firstname'];
			};
		
			$mimemessage->send($nom_client.' '.$prenom_client,
								$mail, '', $from, $subject);			
		}
	}
	// FIN EXTENSION POLYTECH
	//***********************************************************************************
	
	else
    while ($mail = tep_db_fetch_array($mail_query)) {
      $mimemessage->send($mail['customers_firstname'] . ' ' . $mail['customers_lastname'], $mail['customers_email_address'], '', $from, $subject);
    }

	
    tep_redirect(tep_href_link(FILENAME_MAIL, 'mail_sent_to=' . urlencode($mail_sent_to)));
  }
  
  
  	//***********************************************************************************
	// EXTENSION POLYETCH
	// Envoi de mails collectifs

   	// FIN EXTENSION POLYTECH
	//***********************************************************************************
  
 
  if ( ($action == 'preview') && !isset($HTTP_POST_VARS['customers_email_address']) ) {
    $messageStack->add('Erreur : Aucun client n\'a &eacute;t&eacute; s&eacute;lectionn&eacute;.', 'error');
  }

  if (isset($HTTP_GET_VARS['mail_sent_to'])) {
    $messageStack->add(sprintf('Notification : Courrier &eacute;lectronique envoy&eacute; &agrave; : %s', $HTTP_GET_VARS['mail_sent_to']), 'success');
  }
  
  
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript">
function remplir_message(type){
	if(type=='coord_pro'){
		document.getElementById('message').value="BOUTIQUE DES ACCESSOIRES\nSp�cialiste de l�accessoire\n\nBonjour,\nSuite � notre conversation t�l�phonique, je vous communique de nouveau l'adresse de notre site internet�: www.boutiquedesaccessoires.fr\n\nProfitez vite de nos offres r�serv�es aux professionnels�!\n\nA partir d�une commande sup�rieure � 5 articles, nous vous proposons des tarifs adapt�s � votre statut de professionnel�! N�h�siter pas � nous contacter sur une ligne t�l�phonique qui vous est d�di�e au 0�951�999�800.\n\nNous pouvons aussi vous fournir de nombreux accessoires pour vos t�l�phones mobiles mais �galement ordinateurs (claviers, souris, batteries pour ordinateurs portables).\nNous pouvons aussi vour fournir des t�l�phones mobiles sans abonnements que nous vendons pour le moment uniquement sur demande.\n\nPour des besoins inf�rieurs � 5 articles ou bien tout simplement pour voir ce que nous proposons, rendez-vous sur : www.boutiquedesaccessoires.fr \n\n\nCordialement,\nThierry POULAIN\nResponsable client�le professionnels\nT�l�phone�: 0�951�999�800\n\nFax�: 04 34�090�399\nE-mail�: pro@boutiquedesaccessoires.fr \nsite internet : www.boutiquedesaccessoires.fr\n";
	}
	if(type=='coord_part'){
		document.getElementById('message').value="coordonn�es part";
	}
	if(type=='panier_pro'){
		document.getElementById('message').value="BOUTIQUE DES ACCESSOIRES\nSp�cialiste de l�accessoire\n\nBonjour,\nSuite � notre conversation t�l�phonique, voici l�adresse de notre site internet�: www.boutiquedesaccessoires.fr\nVous pourrez � pr�sent vous rendre � nouveau sur notre site internet afin de valider le panier que vous aviez rempli lors de votre derni�re visite et finaliser votre commande.\nProfitez vite de nos offres r�serv�es aux professionnels�!\n\nA partir d�une commande sup�rieure � 5 articles, nous vous proposons des tarifs adapt�s � votre statut de professionnel�! N�h�siter pas � nous contacter sur une ligne t�l�phonique qui vous est d�di�e au 0�951�999�800.\n\nNous pouvons aussi vous fournir de nombreux accessoires pour vos t�l�phones mobiles mais �galement ordinateurs (claviers, souris, batteries pour ordinateurs portables).\nNous fournissons aussi des t�l�phones mobiles sans abonnements, uniquement sur demande.\nPour des besoins inf�rieurs � 5 articles ou bien tout simplement pour voir ce que nous proposons, rendez-vous sur www.boutiquedesaccessoires.fr: \n\n\nCordialement,\nThierry POULAIN\nResponsable client�le professionnels\nT�l�phone�: 0�951�999�800\n\nFax�: 04 34�090�399\nE-mail�: pro@boutiquedesaccessoires.fr \nwww.boutiquedesaccessoires.fr\n";
	}
	if(type=='panier_part'){
		document.getElementById('message').value="BOUTIQUE DES ACCESSOIRES\nSp�cialiste de l�accessoire\n\nBonjour,\nSuite � notre conversation t�l�phonique, voici l�adresse de notre site internet�: www.boutiquedesaccessoires.fr\n\nVous pourrez � pr�sent vous rendre � nouveau sur notre site internet afin de valider le panier que vous aviez rempli lors de votre derni�re visite et finaliser votre commande.\n\nSi vous avez des questions, contactez nous au 0�951�800�800.\nNous sommes � votre �coute du lundi au vendredi de 9h00 � 17h00.\n\n\nCordialement\nThierry \nResponsable client�le\n\nT�l�phone�: 0�951�800�800\nE-mail�: info@boutiquedesaccessoires.fr\nwww.boutiquedesaccessoires.fr\n";
	}
}
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo 'Envoyer un courrier &eacute;lectronique aux clients'; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="4" width="100%" cellspacing="0" cellpadding="2">
<?php
  if ( ($action == 'preview') && isset($HTTP_POST_VARS['customers_email_address']) ) {
    switch ($HTTP_POST_VARS['customers_email_address']) {
      case '***':
        $mail_sent_to = 'Tous les clients';
        break;
      case '**D':
        $mail_sent_to = 'Tous les abonn&eacute;s au bulletin d\'informations';
        break;
      default:
        $mail_sent_to = $HTTP_POST_VARS['customers_email_address'];
        break;
    }
    if(($action == 'preview') && isset($HTTP_POST_VARS['customers_emails'])){
		$mail_sent_to = $HTTP_POST_VARS['customers_emails'];
	
    }
?>
          <tr><?php echo tep_draw_form('mail', FILENAME_MAIL, 'action=send_email_to_user'); ?>
            <td><table border="0" width="100%" cellpadding="0" cellspacing="2">
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo 'Client :'; ?></b><br><?php echo $mail_sent_to; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo 'De :'; ?></b><br><?php echo htmlspecialchars(stripslashes($HTTP_POST_VARS['from'])); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo 'Sujet :'; ?></b><br><?php echo htmlspecialchars(stripslashes($HTTP_POST_VARS['subject'])); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo 'Message :'; ?></b><br><?php echo nl2br(htmlspecialchars(stripslashes($HTTP_POST_VARS['message']))); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td>
<?php
/* Re-Post all POST'ed variables */
    reset($HTTP_POST_VARS);
    while (list($key, $value) = each($HTTP_POST_VARS)) {
      if (!is_array($HTTP_POST_VARS[$key])) {
        echo tep_draw_hidden_field($key, htmlspecialchars(stripslashes($value)));
      }
    }
?>
                <table border="0" width="100%" cellpadding="0" cellspacing="2">
                  <tr>
                    <td><?php echo tep_image_submit('button_back.gif', IMAGE_BACK, 'name="back"'); ?></td>
                    <td align="right"><?php echo '<a href="' . tep_href_link(FILENAME_MAIL) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_send_mail.gif', IMAGE_SEND_EMAIL); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </form></tr>
<?php
  } else {
?>

		<table border="0" cellpadding="0" cellspacing="2">
            <tr>
				<td>
				    <?php echo tep_draw_form('mail', FILENAME_MAIL, 'action=preview','post'); ?>
				    <table border="0" cellpadding="0" cellspacing="2">
						<tr>
							<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
<?php
	$customers = array();
    $customers[] = array('id' => '', 'text' => 'Choisissez un client');
    $customers[] = array('id' => '***', 'text' => 'Tous les clients');
    $customers[] = array('id' => '**D', 'text' => 'Tous les abonn&eacute;s au bulletin d\'informations');
    $mail_query = tep_db_query("select * from " . TABLE_CUSTOMERS . " order by customers_lastname");
    while($customers_values = tep_db_fetch_array($mail_query)) {
      $customers[] = array('id' => $customers_values['customers_email_address'],
                           'text' => $customers_values['customers_lastname'] . ', ' . $customers_values['customers_firstname'] . ' (' . $customers_values['customers_email_address'] . ')');
    }
?>	
			<tr>
                <td class="dataTableHeadingRow" align="right"><?php echo "<b>".'Client :'."</b>"; ?></td>
                <td>
				<?php 
				//***********************************************************************************
				// EXTENSION POLYETCH	
				  if(strcmp(($HTTP_GET_VARS['mails']),'on')==0){ // envoi group�
				  
				  	if (isset($HTTP_GET_VARS['mails']) && strlen(($HTTP_GET_VARS['commandes']))>0){
						$id_commandes = ltrim(($HTTP_GET_VARS['commandes']));
						$tableau_cmd = explode(' ', ($id_commandes));
					
						foreach($tableau_cmd as $cmd) {
							$query = tep_db_query("select * from " .TABLE_ORDERS. 
												  " where orders_id = '".$cmd ."'");
							while ($tuple = mysql_fetch_array($query)){
								$customers_name=$tuple['customers_name'];
								$customers_email=$tuple['customers_email_address'];
							}
							$text=$text.'; '.$customers_name.' ('.$customers_email.')';
							$liste_mails=$liste_mails.';'.$customers_email;
						}
					}
					
					$text=substr($text,1, strlen($text));
					$liste_mails=substr($liste_mails,1, strlen($liste_mails));
					echo tep_draw_input_field('customers_email_address',$liste_mails,'','','hidden');
					echo tep_draw_textarea_field('zone_adresse','', '67', '4',$text);
				  }
				  else if (isset($HTTP_GET_VARS['email'])){
					$destinataire=$HTTP_GET_VARS['name']." (".$HTTP_GET_VARS['email'].")";
					echo tep_draw_input_field('email_address',$destinataire,' size="70" '); 
					echo tep_draw_input_field('customers_email_address',$HTTP_GET_VARS['email'],'','','hidden');
				  }
				  else{
					echo tep_draw_pull_down_menu('customers_email_address', $customers, 
								(isset($HTTP_GET_VARS['customer']) ? $HTTP_GET_VARS['customer'] : ''), 
								' STYLE="width:100%"');
				  }
				// FIN EXTENSION POLYTECH
				//***********************************************************************************
				  
				?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="dataTableHeadingRow" align="right"><?php echo "<b>".'De :'."</b>"; ?></td>
                <td><?php echo tep_draw_input_field('from', EMAIL_FROM,' size="70" '); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="dataTableHeadingRow" align="right"><b><?php echo 'Type :'; ?></b><br></td>
				<td>
                	<select name="type">
                    <option value="">Choisir un type d'envoi</option>
                    <option value="coord_pro" onClick="remplir_message('coord_pro');">Coordonn�es PRO</option>
                    <option value="coord_part" onClick="remplir_message('coord_part');">Coordonn�es PART</option>
                    <option value="panier_pro" onClick="remplir_message('panier_pro');">Paniers perdus PRO</option>
                    <option value="panier_part" onClick="remplir_message('panier_part');">Paniers perdus PART</option>
                    </select>
              </td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="dataTableHeadingRow" align="right"><?php echo "<b>".'Sujet :'."</b>"; ?></td>
                <td><?php echo tep_draw_input_field('subject','',' size="70" '); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td valign="top" class="dataTableHeadingRow" align="right"><?php echo "<b>".'Message :'."</b>"; ?></td>
                <td><?php echo tep_draw_textarea_field('message', 'soft', '67', '15'); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td colspan="2" align="right"><?php echo tep_image_submit('button_send_mail.gif', IMAGE_SEND_EMAIL); ?></td>
              </tr>
            </table></form>	
			</td>
			 <td>
				<?php echo tep_image(DIR_WS_IMAGES . '/icons/News.png', "Supprimer"); ?>
			 </td>
			</tr>
		</table>

<?php
  }
?>
<!-- body_text_eof //-->
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
