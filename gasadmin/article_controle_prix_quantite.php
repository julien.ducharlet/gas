<?php
/*
  Page : telephone_sans_images.php

  Modifier le 05/06/2007
*/

  require('includes/application_top.php');

  $url_google="https://images.google.fr/images?svnum=10&um=1&hl=fr&sa=X&oi=spell&resnum=0&ct=result&cd=1&q=_RECHERCHE_&spell=1";
		
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<title><?php echo TITLE; ?></title>
<?php include('includes/meta.php'); ?>
<script language="javascript" src="includes/javascript/jQuery/tipsy.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script language="javascript">

$(document).ready(function() {
	  
	<?php //include ('includes/headers/menuJS.php'); ?>
	
	$(".tablesorter").tablesorter();
	
	$(".shortMenu").tipsy({
		html: true,
		gravity:'e'
	});
});

</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td class="pageHeading">Articles avec des probl�mes de prix</td>
       </tr>
	   
	   <tr>
	    <td>
	   		
			<table width="100%">
            	<tr>
                	<td width="50%"><div style="text-align:center">Articles dont le prix par quantit� est diff�rent du prix normal</div></td>
                    <td width="50%"><div style="text-align:center">Articles dont le prix revendeur est inf�rieur au prix normal</div></td>
                </tr>
				<tr>
                	<td style="vertical-align:top;">
                    
                    <?php
                    $query = 'SELECT 
						p.products_id, 
						products_model, 
						products_name, 
						products_quantity_reel, 
						products_date_available 
					FROM
						'. TABLE_PRODUCTS .' p, 
						'. TABLE_PRODUCTS_DESCRIPTION . ' pd, 
						'. TABLE_PRODUCTS_PRICE_BY_QUANTITY .' ppbq 
					WHERE 
						p.products_id=pd.products_id 
						AND 
						pd.products_id=ppbq.products_id 
						AND 
						(products_price!=part_price_by_1 or products_price!=pro_price_by_1) 
					ORDER BY 
						products_model';
					// or products_price!=adm_price_by_1
					$query = tep_db_query($query);
					
					if(tep_db_num_rows($query) > 0) {
						
						echo '<table class="infoBoxContent">';
						
							echo '<tr>';
								echo '<td>R�f�rence</td>';
								echo '<td>Article</td>';
								echo '<td class="actions">Action</td>';
							echo '</tr>';
						
							while($data = tep_db_fetch_array($query)) {
								
								echo '<tr>';
									echo '<td><a href="article_edit_prix_fournisseurs.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. $data['products_model'] .'</a></td>';
									echo '<td><a href="article_edit_prix_fournisseurs.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. bda_product_name_replace('', $data['products_name']).'</a></td>';
									echo '<td>';
										echo '<a href="article_edit_prix_fournisseurs.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. tep_image(DIR_WS_IMAGES . 'icons/editer.png', "Editer la cat�gorie") .'</a>';
									echo '</td>';
								echo '</tr>';
							}
						
						echo '</table>';
					}
                    
                    ?>
                    </td>
                    
                    <td style="vertical-align:top;">
                    
                    <?php
                    $query = 'SELECT p.*, products_name, ppbq.* 
					FROM 
						'. TABLE_PRODUCTS .' p, 
						'. TABLE_PRODUCTS_DESCRIPTION . ' pd, 
						'. TABLE_PRODUCTS_PRICE_BY_QUANTITY .' ppbq 
					WHERE 
						p.products_id = pd.products_id 
						AND pd.products_id = ppbq.products_id 
						AND rev_price_by_1 < products_cost 
					ORDER BY products_model';
					
					$query = tep_db_query($query);
					
					if(tep_db_num_rows($query) > 0) {
						
						echo '<table class="tablesorter">';
							echo '<thead>';
								echo '<tr>';
									echo '<th>R�f�rence</th>';
									echo '<th>Article</th>';
									echo '<th>Prix</th>';
									echo '<th>Prix par Qte</th>';
									echo '<th>Prix d\'achat</th>';
									echo '<th>Prix revendeur</th>';
									echo '<th>Action</th>';
								echo '</tr>';
							echo '</thead>';
							echo '<tbody>';
						
								while($data = tep_db_fetch_array($query)) {
									
									$hover = '<img src=\'../'. BASE_DIR .'/images/products_pictures/petite/'. $data['products_bimage'] .'\'>';
									
									echo '<tr>';
										echo '<td>';
											echo '<a href="article_edit_prix_fournisseurs.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. $data['products_model'] .'</a>';
										echo '</td>';
										echo '<td>';
											echo '<a href="article_edit_prix_fournisseurs.php?pID='. $data['products_id'] .'&action=new_product" target="_blank">'. bda_product_name_transform($data['products_name']).'</a>';
										echo '</td>';
										
										echo '<td>'. round($data['products_price'], 2) .'</td>';
										echo '<td><span '. (($data['products_price']!=$data['part_price_by_1']) ? 'class="red"' : '') .'>'. round($data['part_price_by_1'], 2) .'</span></td>';
										echo '<td>'. round($data['products_cost'], 2) .'</td>';
										echo '<td><span '. (($data['products_cost']>$data['rev_price_by_1']) ? 'class="red"' : '') .'>'. round($data['rev_price_by_1'], 2) .'</span></td>';
										
										echo '<td class="actionButtons">';
											echo '<a href="article_edit.php?pID='. $data['products_id'] .'&action=new_product" class="shortMenu" title="'. $hover .'"> ';
							echo '<img src="'. DIR_WS_IMAGES .'icons/editer.png" alt="Editer">';
						echo '</a>';
											
											echo '<a href="article_edit_prix_fournisseurs.php?pID='. $data['products_id'] .'&action=new_product"><img src="'. DIR_WS_IMAGES .'icons/icone_prix.png"></a>';
										echo '</td>';
									echo '</tr>';
								}
								
							echo '</tbody>';
						echo '</table>';
					}
                    
                    ?>
                    </td>
                </tr>
			</table>
		</td>
	   </tr>
	  </table>
	 </td>
	</tr>

      </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
