<?php
/*Auteur : Sami 
  Page permettant de visualiser le panier en cours du client avec possibilité de suppression et de passage de commande*/

require('includes/application_top.php'); 
require('../'.DIR_WS_SITE.'/includes/fonctions/fonctions_panier.php');

function taux_taxe() {
	if (!isset($_SESSION['customer_country_id']) || empty($_SESSION['customer_country_id'])) {
		return 1.2;
	} else {	
		$pays_client = $_SESSION['customer_country_id'];
		$type_client = $_SESSION['customers_type'];
		
		$req_zone_tva = tep_db_query("select geo_zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where zone_country_id=".$pays_client."");
		$tva = tep_db_fetch_array($req_zone_tva);
		
		if($tva['geo_zone_id']==GEO_ZONE_AVEC_TVA){
			return 1;
		}
		elseif($type_client==1){
			return 1.2;
		}
		else{
			$req_tva_intra=tep_db_query("select customers_tva from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['customer_id']."");
			$nb_res=tep_db_num_rows($req_tva_intra);
			
			if($nb_res==0){}
			elseif($pays_client==73){return 1.2; }
				else return 1;
		}
	}
}


function calculate_price_for_product($data) {
	
	if(isset($_SESSION) && !empty($_SESSION['customers_type'])) {
		
		$taxe = taux_taxe();
		
		switch($_SESSION['customers_type']) {
			
			case 1: //particulier
				$prix = ($data['part_price_by_1'] != "0.00") ? $data['part_price_by_1'] : $data['products_price'];
				$prix_promo = (!empty($data['specials_new_products_price'])) ? $data['specials_new_products_price'] : 0;
				$prix_flash = (!empty($data['prix_vente'])) ? $data['prix_vente'] : 0;
				
				$type = ($taxe==1) ? 'HT' : 'TTC';
				$tva = $taxe;//ht ou tcc
				//if(!empty($_SESSION) && $_SESSION['customer_id']==20281) echo $prix_flash .'<br>';
				if($prix_flash > 0) {
					
					$prixHT = $prix_flash;
					$prix_flash = format_to_money($data['prix_vente'] * $taxe);
				}
				elseif($prix_promo > 0) {
					
					$prixHT = $prix_promo;
					$prix_promo = format_to_money($data['specials_new_products_price'] * $taxe);
				}
				else $prixHT = $prix;
				
				$prix = format_to_money($prix * $taxe);
				break;
				
			case 2: //pro
				$prix = ($data['pro_price_by_1'] != "0.00") ? $data['pro_price_by_1'] : $data['products_price'];
				$prix_promo = (!empty($data['specials_new_products_price']) && $data['specials_new_products_price']<$prix) ? $data['specials_new_products_price'] : 0;
				$prix_flash = (!empty($data['prix_vente']) && $data['prix_vente']<$prix) ? $data['prix_vente'] : 0;
				
				if(!empty($_SESSION['cutomer_tva'])) {
					
					$type = 'HT';
					$tva = 1;
				}
				else {
					
					$type = 'TTC';
					$tva = 1.2;
				}
				
				if($prix_flash > 0) {
					
					$prixHT = $prix_flash;
					$prix_flash = format_to_money($data['prix_vente'] * $tva);
				}
				elseif($prix_promo > 0) {
					
					$prixHT = $prix_promo;
					$prix_promo = format_to_money($data['specials_new_products_price'] * $tva);
				}
				else $prixHT = $prix;
				
				$prix = format_to_money($prix * $tva);
				break;
			
			case 3: //revendeur
				$prix = ($data['rev_price_by_1'] != "0.00") ? $data['rev_price_by_1'] : $data['products_price'];
				$prix_promo = 0;
				$prix_flash = 0;
				
				$type = 'HT';
				$tva = 1;
				
				$prixHT = $prix;
				$prix = format_to_money($prix);
				break;
				
			case 4: //administration
				$prix = ($data['adm_price_by_1'] != "0.00") ? $data['adm_price_by_1'] : $data['products_price'];
				$prix_promo = (!empty($data['specials_new_products_price']) && $data['specials_new_products_price']<$prix) ? $data['specials_new_products_price'] : 0;
				$prix_flash = (!empty($data['prix_vente']) && $data['prix_vente']<$prix) ? $data['prix_vente'] : 0;
				
				$type = 'HT';
				$tva = 1;
				
				if($prix_flash > 0) {
					
					$prixHT = $prix_flash;
					$prix_flash = format_to_money($data['prix_vente']);
				}
				elseif($prix_promo > 0) {
					
					$prixHT = $prix_promo;
					$prix_promo = format_to_money($data['specials_new_products_price']);
				}
				else $prixHT = $prix;
				
				$prix = format_to_money($prix);
				break;
				
			default:
				$prix = $data['products_price'];
				$prix_promo = (!empty($data['specials_new_products_price'])) ? $data['specials_new_products_price'] : 0;
				$prix_flash = (!empty($data['prix_vente'])) ? $data['prix_vente'] : 0;
				
				$type = ($taxe==1) ? 'HT' : 'TTC';
				$tva = $taxe;//ht ou tcc
				
				if($prix_flash > 0) {
					
					$prixHT = $prix_flash;
					$prix_flash = format_to_money($data['prix_vente'] * $taxe);
				}
				elseif($prix_promo > 0) {
					
					$prixHT = $prix_promo;
					$prix_promo = format_to_money($data['specials_new_products_price'] * $taxe);
				}
				else $prixHT = $prix;
				
				$prix = format_to_money($prix * $taxe);
				break;
		}
	}
	else {
		
		$prix = $data['products_price'];
		$prixHT = $prix;
		$prix_promo = (!empty($data['specials_new_products_price'])) ? format_to_money($data['specials_new_products_price']*taux_taxe()) : 0;
		$prix = format_to_money($prix * taux_taxe());
	}
	
	return array('type' => $type, 'HT' => $prixHT, 'normal' => $prix, 'promo' => $prix_promo, 'flash' => $prix_flash, 'tva' => $tva);
}


?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<script language="javascript" src="includes/javascript/ajax/fiche_client.js"></script>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php');


$type_array=array(array('id' => '1', 'text' => 'Particulier'),array('id' => '2', 'text' => 'Professionnel'),array('id' => '3', 'text' => 'Revendeur'));

$req=tep_db_query("select * from ".TABLE_CUSTOMERS." c, ".TABLE_CUSTOMERS_INFO." ci where ci.customers_info_id=c.customers_id and c.customers_id=".$_GET['cID']."");
$res=tep_db_fetch_array($req);
$cust_type=$res['customers_type']-1;
$page="panier";
?>
<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$res['customers_type']; ?>" style="width:1400px;" >
<?php include('client_header.php');?>
</table>

<div style="margin-top:20px; margin-left:10px;">
<table border="0" cellspacing="2" cellpadding="2" class="<? echo 'formArea'.$res['customers_type']; ?>" width="100%">
       <tr><strong>Le panier</strong></tr>
       <tr>
         <td class="main">Qt&eacute;</td>
         <td class="main">Nom de l'article</td>
         <td class="main">R&eacute;f&eacute;rence</td>
         <td class="main">Stock<br /> r&eacute;el</td>
         <td class="main">Stock<br /> Virtuel</td>
         <td class="main">P. Achat<br /> Unitaire HT</td>
         <td class="main">P. Vente <br />unitaire HT</td>
         <td class="main">Marge<br /> Unitaire HT</td>
         <td class="main">P. Achat <br />Total HT</td>
         <td class="main">P. Vente<br /> Total HT</td>
         <td class="main">Marge<br />Totale HT</td>
         <td class="main"></td>
         <td class="main"></td>
         <td class="main"></td>
       </tr>
       <tr><td colspan="14"><hr></td></tr>
       <tr><td colspan="14"><strong>LES ARTICLES</strong></td></tr>
       <tr><td colspan="14"><hr></td></tr>
       
       <?php 
	  			 $panier_query = tep_db_query("SELECT cb.customers_basket_id, cb.products_id, cb.customers_basket_quantity, cb.id_rayon 
												  FROM " . TABLE_CUSTOMERS_BASKET . " as cb
												  WHERE cb.customers_id = '" . $_GET['cID'] . "' and cb.pack_id = ''");
					
					$i = 0;
					$nb_articles = tep_db_num_rows($panier_query);
					
					if ($nb_articles > 0) {
						while($panier_data = tep_db_fetch_array($panier_query)){
							$id_complet = $panier_data['products_id'];
							$i++;
							
							//on recupere l'id produit
							$articles_id=explode("_",$id_complet);
							$id_article=$articles_id[0];
							$id_categorie=$articles_id[1];
							$mod=explode("{",$articles_id[2]);
							
							$produit_info=tep_db_query("select * from ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd where p.products_id=pd.products_id and p.products_id='".$id_article."'");
							$res_produit=tep_db_fetch_array($produit_info);
							
							$req_cat_name=tep_db_query("select categories_name, categories_id from categories_description where categories_id='".$mod[0]."'");
							$res_cat_name=tep_db_fetch_array($req_cat_name);
						
							// On récupère les infos sur les options
							$article_options = explode("{", $panier_data['products_id']);
							$options = explode("}", $article_options[1]);
							$option_details = infos_options($options[0], $options[1]);			
							
							// On récupère les infos sur l'article
							$article = explode("_", $article_options[0]);
							$quantite = $panier_data['customers_basket_quantity'];
							$article_details = infos_article($article, $quantite);
							
							// On récupère des infos complémentaires
							$articles_sous_total_HT += $article_details['prix_ht']*$quantite." - ";
							
							echo'
								<tr>
								 <td class="main">'.$quantite.' x</td>
								 <td class="main">';
								 echo bda_product_name_transform($res_produit['products_name'], $res_cat_name['categories_id']);
								 if($options[0]!=''){
									echo "<br />".$option_details["nom"]." : ".$option_details["nom_valeur"];
								}								 
								 
						    $url_clean = url("article",array('id_rayon' => $panier_data['id_rayon'], 'nom_rayon' => 'pipo', 'id_marque' => $id_categorie, 'url_marque' => 'pipo', 'id_modele' => $mod[0], 'url_modele' => 'pipo', 'id_article' => $id_article, 'url_article' => 'pipo'));							
							
						    echo'</td>
								 <td class="main">'.$res_produit['products_model'].'</td>
								 <td class="main">'.$res_produit['products_quantity_reel'].'</td>
								 <td class="main">'.$res_produit['products_quantity'].'</td>
								 <td class="main">'.number_format($res_produit['products_cost'],2).'</td>
								 <td class="main">'.number_format($res_produit['products_price'],2).'</td>
								 <td class="main">'.number_format($res_produit['products_price']-$res_produit['products_cost'],2).'</td>
								 <td class="main">'.number_format($res_produit['products_cost']*$quantite,2).'</td>
								 <td class="main">'.number_format($res_produit['products_price']*$quantite,2).'</td>
								 <td class="main">'.number_format($res_produit['products_price']*$quantite-$res_produit['products_cost']*$quantite,2).'</td>
								 <td class="main"></td>
								 <td class="main"><a href="https://www.generalarmystore.fr/gasadmin/article_edit.php?cPath='.$id_categorie.'_'.$mod[0].'&pID='.$id_article.'&action=new_product" target="blank">Vu Admin</a></td>
								 <td class="main"><a href="#" onclick="supprimer_article(\''.$id_complet.'\','.$_GET['cID'].')"><img src="includes/languages/french/images/buttons/supprimer.png" border="0"/></a></td>
							   </tr>';
	   
						}
					}
				?>
             <tr><td colspan="14"><hr></td></tr>
       		 <tr><td colspan="14"><strong>LES PACKS</strong></td></tr>
             <tr><td colspan="14"><hr></td></tr>
       		    
                <?php	
					
					 $packs_query = tep_db_query("SELECT cb.customers_basket_id, cb.pack_id, cb.customers_basket_quantity, cb.id_rayon 
                                                      FROM " . TABLE_CUSTOMERS_BASKET . " as cb
                                                      WHERE cb.customers_id = '" . $_GET['cID'] . "' and cb.pack_id <> ''");
						
                        $nb_packs = tep_db_num_rows($packs_query);
						
                        if ($nb_packs > 0) {
                            while($packs_data = tep_db_fetch_array($packs_query)){

								$id_complet = $packs_data['pack_id'];
                                $i++;
							    
							    
								// On récupère les infos sur les options
								$pack_options = explode("{", $id_complet);
								$options = explode("}", $pack_options[1]);
								$option_details = infos_options($options[0], $options[1]);
								
								//on recupere des données pour l'url
								$articles_id=explode("_",$id_complet);
								$id_article=$articles_id[0];
								$id_categorie=$articles_id[1];
								$mod=explode("{",$articles_id[2]);
                                
								// On récupère les infos sur le pack
                                $pack = explode("_", $pack_options[0]);
								$pack_details = info_pack($pack, $option_details);
								 
								 $id_pack = $pack[0];
								 $cout_pack=0;
								 $articles_pack_query = tep_db_query("SELECT pp.pack_prix_normal, pp.pack_remise, pp.products_id as products_id_0, pp.products_id_1, pp.products_id_2, p.products_id, p.products_model, p.products_price, pd.products_name, p.products_cost, p.products_quantity, pack_remise, pack_products_quantity
											  FROM ".TABLE_PACK." as pp, ".TABLE_PRODUCTS_DESCRIPTION." as pd, ".TABLE_PRODUCTS." as p
											  WHERE pp.pack_id = " . $id_pack . " and pd.products_id = p.products_id
											   and (pp.products_id = pd.products_id or pp.products_id_1 = pd.products_id or pp.products_id_2 = pd.products_id)");
	
								while ($articles_pack_data = tep_db_fetch_array($articles_pack_query)) {
									
									$nb_prod_pack=$articles_pack_data['pack_products_quantity'];
									for($i=0;$i<$nb_prod_pack;$i++){
										$produit_info=tep_db_query("select * from ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd where p.products_id=pd.products_id and p.products_id=".$articles_pack_data['products_id_'.$i]."");
										$res_produit=tep_db_fetch_array($produit_info);
										$cout_pack=$cout_pack+$res_produit['products_cost'];
									}
										
									for($i=0;$i<$nb_prod_pack;$i++){
											
										$produit_info=tep_db_query("select * from ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd where p.products_id=pd.products_id and p.products_id=".$articles_pack_data['products_id_'.$i]."");
										$res_produit=tep_db_fetch_array($produit_info);
											
										$req_cat_name=tep_db_query("select categories_name, categories_id from categories_description where categories_id=".$pack_details['id_modele']."");
										$res_cat_name=tep_db_fetch_array($req_cat_name);
											
										$url_clean = url("article",array('id_rayon' => $packs_data['id_rayon'], 'nom_rayon' => 'pipo', 'id_marque' => $id_categorie, 'url_marque' => 'pipo', 'id_modele' => $mod[0], 'url_modele' => 'pipo', 'id_article' => $articles_pack_data['products_id_'.$i], 'url_article' => 'pipo'));
											
										echo'<tr>';
										if($i==0){
											 echo '<td class="main" rowspan="'.$nb_prod_pack.'">'.$packs_data['customers_basket_quantity'].' x</td>'; 
										}
										echo'<td class="main">';
											echo bda_product_name_transform($res_produit['products_name'], $res_cat_name['categories_id']);
											if($options[0]!=''){
												echo "<br />".$option_details["nom"]." : ".$option_details["nom_valeur"];
											}
												 
											echo'</td>';
											echo'<td class="main">'.$res_produit['products_model'].'</td>';
											echo'<td class="main">'.$res_produit['products_quantity_reel'].'</td>';
											echo'<td class="main">'.$res_produit['products_quantity'].'</td>';
											if($i==0){
												$quantite=$packs_data['customers_basket_quantity'];
												$prix_pack=number_format($articles_pack_data['pack_prix_normal']-$articles_pack_data['pack_remise'],2);
												$prix_total=$prix_pack*$quantite;
												$cout_total=$cout_pack*$quantite;
												echo'<td class="main" rowspan="'.$nb_prod_pack.'">'.$cout_pack.'</td>';
												echo'<td class="main" rowspan="'.$nb_prod_pack.'">'.$prix_pack.'</td>';
												echo'<td class="main" rowspan="'.$nb_prod_pack.'">'.($prix_pack-$cout_pack).'</td>';
												echo'<td class="main" rowspan="'.$nb_prod_pack.'">'.$cout_total.'</td>';
												echo'<td class="main" rowspan="'.$nb_prod_pack.'">'.$prix_total.'</td>';
												echo'<td class="main" rowspan="'.$nb_prod_pack.'">'.($prix_total-$cout_total).'</td>';
											}
											echo'<td class="main"></td>';
								 			echo'<td class="main"><a href="https://www.generalarmystore.fr/gasadmin/article_edit.php?cPath='.$id_categorie.'_'.$mod[0].'&pID='.$articles_pack_data['products_id_'.$i].'&action=new_product" target="blank">Vu Admin</a></td>';
											if($i==0){
												echo'<td class="main" rowspan="'.$nb_prod_pack.'">';
												echo '<a href="#" onclick="supprimer_pack(\''.$id_complet.'\','.$_GET['cID'].')"><img src="includes/languages/french/images/buttons/supprimer.png" border="0" /></a>';
												echo '</td>';
											}
											echo '</tr>';
												
										}
										echo'<tr><td>&nbsp;</td></tr>';
										
									}
							 }
                        }
	   ?>
</table>
<?php 
$req_info_cli=tep_db_query("select customers_email_address from ".TABLE_CUSTOMERS." where customers_id=".$_GET['cID']."");
$res_info_cli=tep_db_fetch_array($req_info_cli);
?>
<br /><br /><div style="text-align:center;"><a href="<?php echo WEBSITE . BASE_DIR; ?>/compte/connexion.php?panier_admin=true&connexion=ok&mail=<?php echo $res_info_cli['customers_email_address']; ?>&mdp=azertyuiopqsdfghjklm" target="_blank" style="margin-right:50px;">passer la commande sur le site</a>
<a href="nouvelle_commandeP1.php?cID=<?php echo $_GET['cID']; ?>&action=do_basket" target="_blank">passer la commande par l'admin</a></div><br />
</div>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>