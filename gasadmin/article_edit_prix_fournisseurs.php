<?php
/* 
Page : article_edit_prix_fournisseur.php
Version du : 08/07/2009
Par : Thierry POULAIN
*/
require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');

$currencies = new currencies();

$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
	
if (isset($_GET['cPath'])) $cPath=$_GET['cPath'];

if (tep_not_null($action)) {
	switch ($action) {
    	case 'update_product':
			if (isset($HTTP_POST_VARS['edit_x']) || isset($HTTP_POST_VARS['edit_y'])) {
			  $action = 'new_product';
			} 
			else {
				$pID=$_GET['pID'];
				$products_id = tep_db_prepare_input($HTTP_GET_VARS['pID']);
				$products_date_available = tep_db_prepare_input($HTTP_POST_VARS['products_date_available']);
				$products_date_available = (date('Y-m-d') < $products_date_available) ? $products_date_available : 'null';
				
				$sql_data_array = array('products_price' => tep_db_prepare_input($HTTP_POST_VARS['products_price']),
									  'products_cost' => tep_db_prepare_input($HTTP_POST_VARS['products_cost']),
									  'products_ecotaxe' => tep_db_prepare_input($HTTP_POST_VARS['products_ecotaxe']),
									  'products_tax_class_id' => tep_db_prepare_input($HTTP_POST_VARS['products_tax_class_id']));
		
				$re_del_supli='DELETE FROM products_to_suppliers WHERE products_id="'.$pID.'" ';
				tep_db_query($re_del_supli);
				  
				for($i=0;$i<$_POST['total_fournisseurs'];$i++){
					if(isset($_POST['supli'.$i]) && !empty($_POST['supli_prix'.$i])){ 
						$re_insert='INSERT INTO products_to_suppliers (products_id, suppliers_id, prix_achat, ref_supplier, url_supplier) '
								  .'VALUES ("'.$pID.'", "'.$_POST['supli'.$i].'", "'.$_POST['supli_prix'.$i].'", "'.$_POST['supli_ref'.$i].'", "'.$_POST['supli_url'.$i].'")';
						tep_db_query($re_insert) or die($re_insert);
					}
				}
				
				$update_sql_data = array('products_last_modified' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $update_sql_data);
				tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "'");
				
				/* Mise � jour des promos */
				if (isset($_REQUEST['specials_price']) && $_REQUEST['specials_price'] != '') {
					$prix_promo_ht = $_REQUEST['specials_price'];
					$date_expiration = explode("-", $_REQUEST['date_fin_promo']);
					$date_expiration = $date_expiration[2] . "-" . $date_expiration[1] . "-" . $date_expiration[0] . " 23:59:59";
					
					$recup_infos_promo = tep_db_query("SELECT * FROM " . TABLE_SPECIALS . " WHERE products_id = '" . $pID . "'");
					$existe = tep_db_num_rows($recup_infos_promo);
					
					if ($existe == 0) {
						tep_db_query("INSERT INTO " . TABLE_SPECIALS . " (products_id, specials_new_products_price, specials_date_added, expires_date, status) VALUES ('" . $pID . "', '" . $prix_promo_ht . "', now(), '" . $date_expiration . "', '1')");
					} else {
						tep_db_query("UPDATE " . TABLE_SPECIALS . " SET specials_new_products_price = '" . $prix_promo_ht . "', specials_last_modified = now(), expires_date = '" . $date_expiration . "' WHERE products_id = '" . $pID . "'");
					}
				}
				
				/* Mise � jour des coefs */
				$mise_a_jour_prices = array('part_price_by_1' => tep_db_prepare_input($HTTP_POST_VARS['price_part_1']),
										  'pro_price_by_1' => tep_db_prepare_input($HTTP_POST_VARS['price_pro_1']),
										  'rev_price_by_1' => tep_db_prepare_input($HTTP_POST_VARS['price_rev_1']),
										  'adm_price_by_1' => tep_db_prepare_input($HTTP_POST_VARS['price_adm_1']),
										  'part_price_by_5' => tep_db_prepare_input($HTTP_POST_VARS['price_part_5']),
										  'pro_price_by_5' => tep_db_prepare_input($HTTP_POST_VARS['price_pro_5']),
										  'rev_price_by_5' => tep_db_prepare_input($HTTP_POST_VARS['price_rev_5']),
										  'adm_price_by_5' => tep_db_prepare_input($HTTP_POST_VARS['price_adm_5']),
										  'part_price_by_10' => tep_db_prepare_input($HTTP_POST_VARS['price_part_10']),
										  'pro_price_by_10' => tep_db_prepare_input($HTTP_POST_VARS['price_pro_10']),
										  'rev_price_by_10' => tep_db_prepare_input($HTTP_POST_VARS['price_rev_10']),
										  'adm_price_by_10' => tep_db_prepare_input($HTTP_POST_VARS['price_adm_10']));
				tep_db_perform(TABLE_PRODUCTS_PRICE_BY_QUANTITY, $mise_a_jour_prices, 'update', "products_id = '" . (int)$products_id . "'");
				
				/* Mise � jour des ventes flash */
				if ($HTTP_POST_VARS['prix_vente_flash'] != '') {
					$date_deb_vente_flash = explode("-", $HTTP_POST_VARS['date_deb_flash']);
					$date_deb_vente_flash = $date_deb_vente_flash[2] . "-" . $date_deb_vente_flash[1] . "-" . $date_deb_vente_flash[0] . " " . $HTTP_POST_VARS['heure_deb_flash'] . ":" . $HTTP_POST_VARS['minutes_deb_flash'] . ":00";
					
					$date_fin_vente_flash = explode("-", $HTTP_POST_VARS['date_fin_flash']);
					$date_fin_vente_flash = $date_fin_vente_flash[2] . "-" . $date_fin_vente_flash[1] . "-" . $date_fin_vente_flash[0] . " " . $HTTP_POST_VARS['heure_fin_flash'] . ":" . $HTTP_POST_VARS['minutes_fin_flash'] . ":00";
					
					$prix_vente_flash = $HTTP_POST_VARS['prix_vente_flash'];
					
					$vente_flash_existe = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS_VENTES_FLASH . " WHERE products_id = '" . $pID . "'");
					
					if (tep_db_num_rows($vente_flash_existe) == 0) {
						tep_db_query("INSERT INTO " . TABLE_PRODUCTS_VENTES_FLASH . " VALUES ('" . $pID . "', '" . $date_deb_vente_flash . "', '" . $date_fin_vente_flash . "', '" . $prix_vente_flash . "')");
					} else {
						tep_db_query("UPDATE " . TABLE_PRODUCTS_VENTES_FLASH . " SET debut_vente_flash = '" . $date_deb_vente_flash . "', fin_vente_flash = '" . $date_fin_vente_flash . "', prix_vente = '" . $prix_vente_flash . "' WHERE products_id = '" . $pID . "'");
					}
				}
				
				tep_redirect(tep_href_link(FILENAME_ARTICLE_EDIT_PRIX_FOURNISSEURS, 'cPath=' . $cPath . '&pID=' . $products_id . '&action=new_product'));
			}
		break;
	}
}
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/lightbox.css">
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <script language="javascript" src="includes/general.js"></script>
        <script language="javascript" src="ajax/categories.js"></script>
        
        <!-- Pour le calendrier des promos -->
       	<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
		<link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
        <!-- Fin pour le calendrier des promos -->
        
        <!-- Pour les promos -->
        <link rel="stylesheet" type="text/css" href="includes/javascript/calendar.css">
		<script language="JavaScript" src="includes/javascript/calendarcode.js"></script>
        <script language="JavaScript" src="ajax/specials.js"></script>
		<!-- Fin pour les promos -->
        
        <script src="includes/javascript/jQuery/jquery-1.4.js"></script>
        <script type="text/javascript">
        
        $(document).ready(function() {
			
			
			$('.ajaxZone').ajaxSend(function(evt, request, settings) {
				
				if(settings.data=='lightbox=no') {//pour les lightbox, pas de contr�le
				
					$("#overlay").show();
					
					$(this).show().html('<img alt="loading" src="images/loading.gif"> Envoi de la requ�te');
				}
			});
			
			
			$('.ajaxZone').ajaxSuccess(function(evt, request, settings) {
				
				if(settings.data=='lightbox=no') {//pour les lightbox, pas de contr�le
				
					$(this).html('Donn�es bien enregistr�es');
					$(this).fadeOut(1000, function() {
						
						$("#overlay").hide();
					});
				}
			});
			
			$(".ajaxZone").ajaxError(function(event, request, settings){
				
				if(settings.data=='lightbox=no') {//pour les lightbox, pas de contr�le
					
					$(this).html('Erreur lors de l\'appel � la page '+ settings.url);
				}
			});
			
		});
		
		function close_lightbox(box_id) {
			
			$("#"+ box_id).slideUp("slow").queue(function() {
														  
				$("#overlay").hide();
				$(this).dequeue();
			});
		}
		
		function updateStock() {
			
			new_reel = $("#products_quantity_reel").val();
			old_reel = $("#products_quantity_reel_old").val();
			
			$("#products_quantity_reel_old").val(new_reel);
			
			stock = $("#products_quantity").val();
			
			$("#products_quantity").val(parseInt(stock) + parseInt(new_reel) - parseInt(old_reel));
		}
		
		</script>
        
    </head>
     
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
		<?php
        	require(DIR_WS_INCLUDES . 'header.php');
			$parameters = array('products_id' => '',
								'products_name' => '',
								'products_quantity' => '',
								'products_quantity_reel' => '',
								'products_price' => '',
								'products_cost' => '',
								'products_weight' => '',
								'products_date_added' => '',
								'products_last_modified' => '',
								'products_date_available' => '',
								'products_status' => '',
								'products_quantity_min' => '',
								'products_quantity_max' => '',
								'products_quantity_ideal' => '',
								'products_ecotaxe' => '');
			
			$pInfo = new objectInfo($parameters);
			
			$tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
			$tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
			while ($tax_class = tep_db_fetch_array($tax_class_query)) {
				$tax_class_array[] = array('id' => $tax_class['tax_class_id'],
				'text' => $tax_class['tax_class_title']);
			}
			
			$languages = tep_get_languages();
			
			$product_query = tep_db_query("select pd.products_name, p.products_id, p.products_quantity, p.products_quantity_reel, p.products_model, p.products_ref_origine, p.products_garantie, p.products_code_barre, p.products_price, p.products_cost, p.products_weight, p.products_date_added, p.products_last_modified, date_format(p.products_date_available, '%Y-%m-%d') as products_date_available, p.products_status, p.products_tax_class_id, p.products_ecotaxe, p.products_quantity_min, p.products_quantity_max, p.products_quantity_ideal from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$HTTP_GET_VARS['pID'] . "' AND p.products_id = pd.products_id");
			$product = tep_db_fetch_array($product_query);
			$pInfo->objectInfo($product);
			
			if (!isset($pInfo->products_status)) $pInfo->products_status = '1';
			
			switch ($pInfo->products_status) {
				case '0': $in_status = false; $out_status = true; break;
				case '1':
				default: $in_status = true; $out_status = false;
			}
		?>
 		
		<script language="javascript"><!--
			var tax_rates = new Array();
			<?php
			for ($i=0, $n=sizeof($tax_class_array); $i<$n; $i++) {
				if ($tax_class_array[$i]['id'] > 0) {
					echo 'tax_rates["' . $tax_class_array[$i]['id'] . '"] = ' . tep_get_tax_rate_value($tax_class_array[$i]['id']) . ';' . "\n";
				}
			}
			?>
			
			function doRound(x, places) {
				return Math.round(x * Math.pow(10, places)) / Math.pow(10, places);
			}
			
			function getTaxRate() {
				var selected_value = document.forms["new_product"].products_tax_class_id.selectedIndex;
				var parameterVal = document.forms["new_product"].products_tax_class_id[selected_value].value;
				
				if ( (parameterVal > 0) && (tax_rates[parameterVal] > 0) ) {
					return tax_rates[parameterVal];
				} else {
					return 0;
				}
			}
			
			function updateGross() {
				var taxRate = getTaxRate();
				var grossValue = document.forms["new_product"].products_price.value;
				
				if (taxRate > 0) {
					grossValue = grossValue * ((taxRate / 100) + 1);
				}
			
				document.forms["new_product"].products_price_gross.value = doRound(grossValue, 2);
			}
			
			function s_updateGross() {
				var taxRate = getTaxRate();
				var grossValue = document.forms["new_product"].specials_price.value;
				
				if (taxRate > 0) {
				grossValue = grossValue * ((taxRate / 100) + 1);
				}
				
				document.forms["new_product"].s_products_price_gross.value = doRound(grossValue, 2);
			}
			
			function updateNet() {
				var taxRate = getTaxRate();
				var netValue = document.forms["new_product"].products_price_gross.value;
				
				if (taxRate > 0) {
					netValue = netValue / ((taxRate / 100) + 1);
				}
				
				document.forms["new_product"].products_price.value = doRound(netValue, 4);
				document.forms["new_product"].products_price_aff.value = doRound(netValue, 4);
				updateMarge();
			}
			
			function s_updateNet() {
				  var taxRate = getTaxRate();
				  var netValue = document.forms["new_product"].s_products_price_gross.value;
				
				  if (taxRate > 0) {
					netValue = netValue / ((taxRate / 100) + 1);
				  }
				
				  document.forms["new_product"].specials_price.value = doRound(netValue, 4);
			}
			
			function updateStock() {
				var new_reel = document.forms["new_product"].products_quantity_reel.value;
				var old_reel = document.forms["new_product"].products_quantity_reel_old.value;
				document.forms["new_product"].products_quantity_reel_old.value=new_reel;
				var stock = document.forms["new_product"].products_quantity.value;
				
				document.forms["new_product"].products_quantity.value = parseInt(stock) + parseInt(new_reel) - parseInt(old_reel);
			}
			
			function updateMarge() {
				var margeValue = document.forms["new_product"].products_price.value-document.forms["new_product"].products_cost.value;
				document.forms["new_product"].products_marge_aff.value = doRound(margeValue, 2);
				var pourcentMargeValue = document.forms["new_product"].products_price.value/document.forms["new_product"].products_cost.value;
				document.forms["new_product"].products_marge_pourcent.value = doRound(pourcentMargeValue, 2);
			}
			
			function suppr_vente_flash(id_article) {
				var req;
				
				if(window.XMLHttpRequest)
					req = new XMLHttpRequest();
				else if (window.ActiveXObject)
					req  = new ActiveXObject(Microsoft.XMLHTTP);
			
				req.onreadystatechange = function(){  
					if(req.readyState == 4){
						if (req.status == 200){
							document.getElementById('date_deb_flash').value = '';
							document.getElementById('date_fin_flash').value = '';
							document.getElementById('prix_vente_flash').value = '';
							document.getElementById("heure_deb_flash").options[0].selected=true;
							document.getElementById("heure_fin_flash").options[0].selected=true;
							document.getElementById("minutes_deb_flash").options[0].selected=true;
							document.getElementById("minutes_fin_flash").options[0].selected=true;
							
							alert('Vente Flash correctement supprim�e');
						} else {
							alert("Error: returned status code " + req.status + " " + req.statusText);
						}
					}		
				};
				
				req.open("GET","includes/article/article_prix_promotion_ajax.php?id_article_vf="+ id_article, true);
				req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				req.send(null);
			}
			
			function suppr_promo(id_article) {
				var req;
				
				if(window.XMLHttpRequest)
					req = new XMLHttpRequest();
				else if (window.ActiveXObject)
					req  = new ActiveXObject(Microsoft.XMLHTTP);
			
				req.onreadystatechange = function(){  
					if(req.readyState == 4){
						if (req.status == 200){
							document.getElementById('specials_price').value = '';
							document.getElementById('s_products_price_gross').value = '';
							document.getElementById('date_fin_promo').value = '';
							
							alert('Promotion correctement supprim�e');
						} else {
							alert("Error: returned status code " + req.status + " " + req.statusText);
						}
					}		
				};
				
				req.open("GET","includes/article/article_prix_promotion_ajax.php?id_article="+ id_article, true);
				req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				req.send(null);
			}
			
			function prix_par_qte_defaut(id_article, column) {
				var prix_vente = document.getElementById("prix_vente").value;
				var prix_achat = document.getElementById("prix").value;
				var req;
				
				if(window.XMLHttpRequest)
					req = new XMLHttpRequest();
				else if (window.ActiveXObject)
					req  = new ActiveXObject(Microsoft.XMLHTTP);
			
				req.onreadystatechange = function(){  
					if(req.readyState == 4){
						if (req.status == 200){
							
							//document.getElementById('tab_prix_qte').innerHTML = req.responseText;
							
							result = req.responseText.split(',');
							
							if(result.length==3) {
								
								document.getElementById('price_'+ column +'_1').value = result[0];
								document.getElementById('price_'+ column +'_5').value = result[1];
								document.getElementById('price_'+ column +'_10').value = result[2];
							}
							
						} else {
							alert("Error: returned status code " + req.status + " " + req.statusText);
						}
					}		
				};
				
				req.open("GET","includes/article/article_prix_par_quantite_ajax.php?products_id="+ id_article +"&prix_vente="+ prix_vente +"&prix_achat="+ prix_achat +"&col="+column, true);
				req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				req.send(null);
			}
		//--></script>
        
        <?php
            $action_products = (isset($HTTP_GET_VARS['pID'])) ? 'update_product' : 'insert_product';
            echo tep_draw_form('new_product', FILENAME_ARTICLE_EDIT_PRIX_FOURNISSEURS, 'cPath=' . $cPath . (isset($HTTP_GET_VARS['pID']) ? '&pID=' . $HTTP_GET_VARS['pID'] : '') . '&action='.$action_products, 'post', 'enctype="multipart/form-data"');
        	
			require(DIR_WS_ARTICLE . 'article_menu.php');
			require(DIR_WS_ARTICLE . 'position_article.php');
		?>
            
		<span style="padding: 10px;" class="pageHeading">Ventes des 12 derniers mois</span>
        
        <?php require(DIR_WS_ARTICLE . 'article_ventes_sur_12_mois.php'); ?>
        
        <span style="padding: 10px;" class="pageHeading">Aper�u du stock de cet article</span>
			
		<?php 
			$editable = false;
			require(DIR_WS_ARTICLE. 'article_stock_global.php');
		?>
    
    <div style="float: left; width: 50%;">
        <span style="padding: 10px;" class="pageHeading">Tarifs</span>
        <?php require(DIR_WS_ARTICLE. 'article_prix_marge.php'); ?>
	</div>
    
	<div style="float: right; width: 50%;">
		<span style="padding: 10px;" class="pageHeading">Prix par quantit�s</span>
    	<?php require(DIR_WS_ARTICLE. 'article_prix_par_quantite.php');?>
    </div>
    
    <div style="clear: both;"></div>
    
    <div style="float: left; width: 50%;">
	    <span style="padding: 10px;" class="pageHeading">Promotions</span>
        <?php require(DIR_WS_ARTICLE. 'article_prix_promotion.php'); ?>
	</div>
    
	<div style="float: right; width: 50%;">
	    <span style="padding: 10px;" class="pageHeading">Ventes Flash</span>
        <?php require(DIR_WS_ARTICLE. 'article_vente_flash.php'); ?>
	</div>
    
    <div style="clear: both;"></div>

    <span style="padding: 10px;" class="pageHeading">Fournisseurs</span>
    
    <?php require(DIR_WS_ARTICLE. 'article_fournisseurs.php'); ?>
    
    
    <div style="margin: auto; width: 65px; margin-bottom: 10px; padding: 5px; border: 1px solid #090; background-color: #EFF; text-align: center;">
    <?php
        echo tep_draw_hidden_field('products_date_added', (tep_not_null($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d')));
        echo tep_image_submit('button_update.gif', IMAGE_UPDATE);
    ?>
    </div>
	<div id="overlay"></div>
        <div id="lightbox"></div>
		<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
    </body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
