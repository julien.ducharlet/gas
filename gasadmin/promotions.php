<?php
/*
  $Id: specials.php,v 1.41 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  $affichage_par_page = 100;

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {
      
      case 'deleteconfirm':
	  	
		if(isset($_GET['sID']) && !empty($_GET['sID'])) {
			
			$specials_id = tep_db_prepare_input($HTTP_GET_VARS['sID']);
	
			tep_db_query("delete from " . TABLE_SPECIALS . " where specials_id = '" . (int)$specials_id . "'");
		}
		elseif(isset($_GET['vfID']) && !empty($_GET['vfID'])) {
			
			$flash_id = tep_db_prepare_input($_GET['vfID']);
	
			tep_db_query("delete from " . TABLE_PRODUCTS_VENTES_FLASH . " where products__id = '" . (int)$flash_id . "'");
		}
        tep_redirect(tep_href_link(FILENAME_SPECIALS));
        break;
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/metadata.js"></script>
<script type="text/javascript">

$(document).ready(function() { 
     
    // call the tablesorter plugin
    $(".tablesorter").tablesorter();
});
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<div id="popupcalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
        <!-- body_text //-->
        <td width="100%" valign="top">
            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                <tr>
                    <td width="100%">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pageHeading">Ventes Flash</td>
                                <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
                            </tr>
                        </table>
        			</td>
                </tr>
                <tr>
                    <td> 
						<table border="0" cellspacing="0" cellpadding="5" class="tablesorter">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>D�but</th>
                                    <th>Fin</th>
                                    <th>Prix Normal</th>
                                    <th>Nouveau Prix</th>
                                    <th>Marge</th>
                                    <th class="{sorter:false}">Action</th>
                                </tr>
                            </thead>
                            <tbody>
          
<?php
	$flash_query_raw = "select products_tax_class_id, p.products_id, pd.products_name, p.products_model, p.products_price, p.products_cost, 
	prix_vente, debut_vente_flash, fin_vente_flash from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_VENTES_FLASH . " pvf, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and p.products_id = pvf.products_id order by p.products_model";
	$flash_query = tep_db_query($flash_query_raw);
	
	while ($flash = tep_db_fetch_array($flash_query)) { ?>
    	
        <tr>
            <td><?php echo $flash['products_name']; ?> - <b>( <?php echo $flash['products_model']; ?> )</b></td>
            <td><?php echo $flash['debut_vente_flash']; ?></td>
            <td><?php echo (date_fr($flash['fin_vente_flash'])!='') ? $flash['fin_vente_flash'] : 'illimit�'; ?></td>
            <td><?php echo $currencies->format($flash['products_price']) .'&nbsp;&nbsp;(HT) - '.  $currencies->format(1.2*$flash['products_price']) .'&nbsp;&nbsp;(TTC)'; ?></td>
            <td><?php echo $currencies->format($flash['prix_vente']) .'&nbsp;&nbsp;(HT) - <span class="specialPrice">'.  $currencies->format(1.2*$flash['prix_vente']) .'&nbsp;&nbsp;(TTC)</span>'; ?></td>
            <td><?php echo $currencies->format($flash['prix_vente']-$flash['products_cost']); ?></td>
            <td style="text-align:right;"><?php 
				echo '<a href="article_edit_prix_fournisseurs.php?pID='. $flash['products_id'] .'&action=new_product">'. tep_image(DIR_WS_IMAGES.'icons/editer.png', "Editer") .'</a>'; 
				echo ' | ';
                echo '<a href="?vfID='. $flash['products_id'] .'&action=deleteconfirm" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer cette vente flash ?\')){return true;} else {return false;}">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>';
			?></td>
		</tr>
<?php
  }
?>
							</tbody>
                        </table><!--fin table sorter-->
                    </td>
                </tr>
                <tr>
                    <td width="100%">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pageHeading">Promotions</td>
                                <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
                            </tr>
                        </table>
        			</td>
                </tr>
                <tr>
                	<td>

						<table border="0" cellspacing="0" cellpadding="5" class="tablesorter">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Fin</th>
                                    <th>Prix Normal</th>
                                    <th>Nouveau Prix</th>
                                    <th>Marge</th>
                                    <th class="{sorter:false}">Action</th>
                                </tr>
                            </thead>
                        	<tbody>
          
<?php
	$specials_query_raw = "select products_tax_class_id,p.products_id, pd.products_name, p.products_model, p.products_price, p.products_cost, s.specials_id, s.specials_new_products_price, s.specials_date_added, s.specials_last_modified, s.expires_date, s.date_status_change, s.status from " . TABLE_PRODUCTS . " p, " . TABLE_SPECIALS . " s, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and p.products_id = s.products_id order by p.products_model";
	$specials_query = tep_db_query($specials_query_raw);
	
	while ($specials = tep_db_fetch_array($specials_query)) { ?>
    	
        <tr>
            <td><?php echo $specials['products_name']; ?> - <b>( <?php echo $specials['products_model']; ?> )</b></td>
            <td><?php echo (date_fr($specials['expires_date'])!='') ? $specials['expires_date'] : 'illimit�'; ?></td>
            <td><?php echo $currencies->format($specials['products_price']) .'&nbsp;&nbsp;(HT) - '.  $currencies->format(1.2*$specials['products_price']) .'&nbsp;&nbsp;(TTC)'; ?></td>
            <td><?php echo $currencies->format($specials['specials_new_products_price']) .'&nbsp;&nbsp;(HT) - <span class="specialPrice">'.  $currencies->format(1.2*$specials['specials_new_products_price']) .'&nbsp;&nbsp;(TTC)</span>'; ?></td>
            <td><?php echo $currencies->format($specials['specials_new_products_price']-$specials['products_cost']); ?></td>
            <td style="text-align:right;"><?php 
				echo '<a href="article_edit_prix_fournisseurs.php?pID='. $specials['products_id'] .'&action=new_product">'. tep_image(DIR_WS_IMAGES.'icons/editer.png', "Editer") .'</a>'; 
				echo ' | ';
                echo '<a href="?sID='. $specials['specials_id'] .'&action=deleteconfirm" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer cette promotion ?\')){return true;} else {return false;}">' .  tep_image(DIR_WS_IMAGES . 'icons/supprimer.png', "Supprimer")  . '</a>';
			?></td>
		</tr>
<?php
  }
?>
                            </tbody>
                        </table><!--fin table sorter-->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
