<?php
/* Page d'�dition des articles : refonte par Paul */
	
	require('includes/application_top.php');
	require(DIR_WS_CLASSES . 'currencies.php');
	
	$currencies = new currencies();
	
	$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
		
	if (isset($_GET['cPath'])) $cPath=$_GET['cPath'];
	
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <script language="javascript" src="includes/general.js"></script>
        <!-- AJAX Attribute Manager  -->
        <?php require_once( 'attributeManager/includes/attributeManagerHeader.inc.php' )?>
        <!-- AJAX Attribute Manager  end -->
        <script language="JavaScript" src="ajax/categories.js"></script>
        
        <script language="JavaScript"><!--
            // script pour mettre a true ou false toutes les cases a cocher
			var checkflag = "false";
			function check(field) {
				if (checkflag == "false") {
					for (i = 0; i < field.length; i++) {
						field[i].checked = true;
					}
					checkflag = "true";
					return "Uncheck All";
				}	
				else {
					for (i = 0; i < field.length; i++) {
						field[i].checked = false; 
					}
					checkflag = "false";
					return "Check All"; 
				}
			}
		--></script>
    </head>
    
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="goOnLoad();">
        <div id="spiffycalendar" class="text"></div>
        <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
        
        <?php
        $query = 'select categories_name, parent_id from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd, '. TABLE_PRODUCTS_TO_CATEGORIES .' ptc ';
		$query .= 'where cd.categories_id=c.categories_id and c.categories_id=ptc.categories_id and ptc.products_id='. $_GET['pID'] .' order by categories_name';
		$query = tep_db_query($query);
		
		$nbre_col = 4;
		
		$nbre_result = ceil(tep_db_num_rows($query)/$nbre_col);
		$i = 0;
		
		?>
        <table border="0" width="100%" cellspacing="2" cellpadding="2">
			<tr>
                <td width="100%" valign="top">
                	
                        <link rel="stylesheet" type="text/css" href="includes/javascript/spiffyCal/spiffyCal_v2_1.css">
                        <!--<script language="JavaScript" src="includes/javascript/spiffyCal/spiffyCal_v2_1.js"></script>-->
                        
                      
						<table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr>
								<td colspan="2">
									<?php require(DIR_WS_ARTICLE. 'article_menu.php'); ?>
									<?php require(DIR_WS_ARTICLE. 'article_separateur.php'); ?>
									<?php require(DIR_WS_ARTICLE. 'article_dans_categorie.php'); ?>
									<?php require(DIR_WS_ARTICLE. 'article_separateur.php'); ?>
								</td>
							</tr>
						</table>
                        
                        <table width="100%">
                        	<tr>
                            <?php
                             while($data = tep_db_fetch_array($query)) {
								
								$parent = $data['parent_id'];
								$path_to_categorie = $data['categories_name'];
								//on r�cup�re l'arborescence
								
								while($parent != 0) {
									
									$query_parent = 'select c.categories_id, categories_name, parent_id from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd where c.categories_id=cd.categories_id and c.categories_id='. $parent;
									$query_parent = tep_db_query($query_parent);
									$data_parent = tep_db_fetch_array($query_parent);
									
									$parent = $data_parent['parent_id'];
									$categorie = $data_parent['categories_id'];
									
									$path_to_categorie = $data_parent['categories_name'] .' / '. $path_to_categorie;
								}
								
								if($parent == 0) {
									
									$query_rubrique = 'select rubrique_name from '. TABLE_RAYON .' r, '. TABLE_CATEGORIES_RAYON .' cr where r.rubrique_id=cr.rubrique_id and categories_id='. $categorie;
									$query_rubrique = tep_db_query($query_rubrique);
									$data_rubrique = tep_db_fetch_array($query_rubrique);
									
									$path_to_categorie = '<u>'. $data_rubrique['rubrique_name'] .'</u> / '. $path_to_categorie;
								}
								
                                if($i++%$nbre_result == 0) {
									
									if($i>1) echo '</td>';
									
									echo '<td style="vertical-align:top; width:'. round(100/$nbre_col) .'%;">';
								}
                                
								echo '<div>'. $path_to_categorie .'</div>';
							}
                            ?>
                            </tr>
                        </table>
				</td>
			</tr>
		</table>
                
        <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
        <br />
        
    </body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>