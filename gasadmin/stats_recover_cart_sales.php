<?php
/* 2009/01/13 LL last modif by Ludo => tri pour savoir si relance t�l ou email A FINIR*/
require('includes/application_top.php');
require('includes/functions/date.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
  
//fonction affichant la date avec des slash
function tep_date_order_stat($raw_date) {
    if ($raw_date == '') return false;
    $year = substr($raw_date, 2, 2);
    $month = (int)substr($raw_date, 4, 2);
    $day = (int)substr($raw_date, 6, 2);
    return date(DATE_FORMAT, mktime('', '', '', $month, $day, $year));
}

function seadate($day){
	$ts = date("U");
	$rawtime = strtotime("-".$day." days", $ts);
	$ndate = date("Ymd", $rawtime);
	return $ndate;
}

// Si la date de d�but est envoy�e en param�tre et n'est pas vide
if (isset($_GET['date_deb']) && !empty($_GET['date_deb'])) {
	// On l'explose pour en tirer une date compatible avec la BDD
	$tab_date=explode('-',$date_deb);
	$timestamp = mktime(0, 0, 0, $tab_date[1], $tab_date[0], $tab_date[2]);
	
	$date_deb_format = date("Ymd", $timestamp);
	$date_deb = date("d-m-Y", $timestamp);
	
	// Si la date de fin est d�finie en param�tre et n'est pas vide
	if (isset($_GET['date_fin']) && !empty($_GET['date_fin']))
	{
		// On proc�de pareil qu'avec la date de d�but
		$tab_date=explode('-',$date_fin);
		$timestamp = mktime(0, 0, 0, $tab_date[1], $tab_date[0], $tab_date[2]);
		
		$date_fin_format = date("Ymd", $timestamp);
		$date_fin = date("d-m-Y", $timestamp);
	}
	// Si la date de fin n'est pas d�finie en param�tre ou est vide
	else
	{
		// On renvoye la date du jour comme limite
		$date_fin_format = date('Ymd');
	}
}
// Si la date de d�but n'est pas d�finie en param�tre ou est vide
else 
{
	// On d�finit la date de d�but avec la date du jour
	$date_deb = date('d-m-Y');
	$date_deb_format = date('Ymd');
	// Et la date de fin aussi
	$date_fin_format = $date_deb_format;
}


// Construction de la liste d�roulante des Status
$reponse = mysql_query('SELECT orders_status_id, orders_status_name FROM orders_status'); // On interroge la base pour avoir les status

// On rempli un tableau avec les status
while ($donnees = mysql_fetch_array($reponse)){
	$list_status_array[] = array('id' => $donnees['orders_status_id'], 'text' => $donnees['orders_status_name']);
}

// Construction de la liste des membres ayant relanc�
$reponse = mysql_query('SELECT admin_id, admin_firstname FROM admin'); // On interroge la base pour avoir les noms des admins

// On rempli un tableau avec les membres
while ($donnees = mysql_fetch_array($reponse)){
	$list_admins_array[] = array('id' => $donnees['admin_id'], 'firstname' => $donnees['admin_firstname'], 'relances' => 0, 'ca_ttc' => 0, 'marge' => 0);
}


// Gestion des actions
$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
  
if (tep_not_null($action))
{
	switch ($action)
	{	
		case 'deleteconfirm':
		$scartID = tep_db_prepare_input($HTTP_GET_VARS['scartID']);
		tep_remove_scart($scartID);
		tep_redirect(tep_href_link(FILENAME_STATS_RECOVER_CART_SALES.'?month='.$month.'&year='.$year));
		break;
	}
}
?>


<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
        <SCRIPT type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="includes/general.js"></script>
        <script language="javascript" src="includes/menu.js"></script>
    </head>
    
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
    
        <div id="popupcalendar" class="text"></div>
        
        <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
        
        <!-- body //-->
        <table border="0" width="100%" cellspacing="2" cellpadding="2">
        	<tr>
                <td width="100%" valign="top">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    	<!-- Filtres -->
                    	<tr>
                            <td colspan="12">
                                <table border="0" width="100%" style="background-color: #e1e1f5;">
                                <tr>
                                    <!-- Formulaire des filtres -->
                                    <?php echo tep_draw_form('Recherche', FILENAME_STATS_RECOVER_CART_SALES, '', 'get'); ?>
                                    	<td width="27%">&nbsp;</td>
                                        <!-- Dates Limites + Calendriers -->
                                        <td align="center" class="smallText" height="40" width="20%">
                                            <?php	echo "Du : ".tep_draw_input_field('date_deb', '', 'size="10"');?>
                                            <a href="#" onClick="displayCalendar(Recherche.date_deb,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
                                            <?php echo "Au&nbsp;".tep_draw_input_field('date_fin', '', 'size="10"');?>
                                            <a href="#" onClick="displayCalendar(Recherche.date_fin,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
                                        </td>
                                        
                                        <!-- Choix du statut -->
                                        <td class="smallText" align="center" width="20%">
                                            <?php
                                            echo "Statut :";
                                            echo tep_draw_pull_down_menu('status', array_merge(array(array('id' => 'ALL', 'text' => 'Tous les status')), $list_status_array), '', 'onChange="this.form.submit();"');
                                            ?>
                                        </td>
                                        
                                        <td align="center" width="6%">
                                            <input type=submit value="Go">
                                        </td>
                                        <td width="27%">&nbsp;</td>
                                    </form>
                                </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- Fin des Filtres -->
                          
                    <?php
					// Init vars
					$custknt = 0;
					$total_recovered = 0;
					$marge_totale = 0;
					$custlist = '';
				   
					// Query database for abandoned carts within our timeframe
					$conquery_query="select * from ". TABLE_SCART ."";
					$select_status = '';
					
					// Morceau de requ�te pour filtrer les status
					if ($status != 'ALL'){
						$select_status = 'and o.orders_status = "' . $status . '"';
					}
					
					// Requ�te de s�lection des dates
					$conquery_query .=" where dateadded  <= '".$date_fin_format."' and dateadded  >= '".$date_deb_format."' order by dateadded ASC, type_modif";
					$conquery = tep_db_query( $conquery_query );
					
					// Nombre de commandes correspondant � la requ�te
					$rc_cnt = mysql_num_rows($conquery);
					
					// Loop though each one and process it
					for ($i = 0; $i < $rc_cnt; $i++){
						$inrec = tep_db_fetch_array($conquery);
						$cid = $inrec['customers_id'];
						$type_modif = $inrec['type_modif'];
						$count_type[$type_modif]++;
						// ID du membre ayant r�alis� la relance
						$id_modificateur = $inrec['id_modificateur'];
						
						// Mise � jour des relances de chaque membre
						$j = 0;
						// Identification du membre dans le tableau
						while ($list_admins_array[$j]['id'] != $id_modificateur && $j<sizeof($list_admins_array))
						{ $j++; }
						// On incr�mente son nombre de relances
						$list_admins_array[$j]['relances']++;
				
						// we have to get the customer data in order to better locate matching orders
						$query1 = tep_db_query("select c.customers_firstname, c.customers_lastname from ".TABLE_CUSTOMERS." c where c.customers_id ='".$cid."'");
						$crec = tep_db_fetch_array($query1);
				
						// Query DB for the FIRST order that matches this customer ID and came after the abandoned cart
						$orders_query_raw = "select o.orders_id, o.customers_id, o.date_purchased, s.orders_status_name, o.total, o.marge
											 from " . TABLE_ORDERS . " o, " . TABLE_ORDERS_STATUS . " s 
											 where (o.customers_id = " . (int)$cid . " OR o.customers_email_address like '" . $crec['customers_email_address'] ."' 
											 OR o.customers_name = \"" . $crec['customers_firstname'] . " " . $crec['customers_lastname'] . "\") 
											 and s.orders_status_id = o.orders_status " . $select_status . " 
											 and o.date_purchased >= " . $inrec['dateadded'];
						$orders_query = tep_db_query($orders_query_raw);
						$orders = tep_db_fetch_array($orders_query);
				
						// If we got a match, create the table entry to display the information
						if( $orders ){
							
							// Mise � jour du type de notification pour les stats
							$vente_count_type[$type_modif]++;
							
							// Mise � jour du CA du membre ayant r�alis� la vente
							$list_admins_array[$j]['ca_ttc'] += strip_tags($orders['total']);
							$list_admins_array[$j]['marge'] += strip_tags($orders['marge']);
							
							$custknt++;
							$total_recovered += strip_tags($orders['total']);
							$marge_totale += $orders['marge'];
							$custknt % 2 ? $class = RCS_REPORT_EVEN_STYLE : $class = RCS_REPORT_ODD_STYLE;
							
							// Affichage de la ligne
							$custlist .= "<tr class=".$class.">".
										"<td class=datatablecontent align=right>".
										'<a href="'.tep_href_link(FILENAME_CMD_EDIT, /*tep_get_all_get_params(array('oID', 'action')).*/'oID='.$orders['orders_id'].'&action=edit').'" target="_blank" title="Voir la commande">'.$orders['orders_id'].'</a>&nbsp;'.
										"</td>".   
										"<td>&nbsp;</td>".
										"<td class=datatablecontent align=center>". tep_date_order_stat($inrec['dateadded']) ."</td>".
										"<td>&nbsp;</td>".						
										"<td class=datatablecontent><a href='" . tep_href_link(FILENAME_CLIENT_EDIT, 'cID=' . $cid, 'NONSSL') . "&action=edit' target=\"_blank\" title=\"Voir la fiche du client\">".$crec['customers_firstname']." ".strtoupper($crec['customers_lastname'])."</a></td>".
										"<td class=datatablecontent align=\"center\">".$type_modif."</td>".
										"<td class=datatablecontent>".tep_date_short($orders['date_purchased'])."</td>".
										"<td class=datatablecontent align=center>".$orders['orders_status_name']."</td>".
										"<td class=datatablecontent align=right>".strip_tags($orders['total'])." �</td>".
										"<td class=datatablecontent align=right style='color:red'><b>" . $currencies->format($orders['marge']) . "</b></td>".
										"<td class=datatablecontent align=\"center\">" . $list_admins_array[$j]['firstname'] . "</td>".
										"<td class=datatablecontent align=\"center\">".
										'<a href="' . tep_href_link(FILENAME_STATS_RECOVER_CART_SALES, /*tep_get_all_get_params(array('oID', 'action')) . */'scartID=' . $inrec['scartid'] . '&action=deleteconfirm'). '" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer la notification : ' . $inrec['scartid'] . ' (DEFINITIVEMENT) ?\'))    
										{return true;} else {return false;}">' .  tep_image(DIR_WS_IMAGES . '/icons/supprimer.png', "Supprimer")  . '</a>&nbsp;'.
										"</td>".
										"</tr>";
						}
					}
					
					//D�tails des relances
					$relances_tel = 0; $relances_mail = 0;
					if(isset($count_type) && !empty($count_type)){
					  
						if ($count_type['t&eacute;l&eacute;phone'] != '')
							$relances_tel = $count_type['t&eacute;l&eacute;phone'];
						if ($count_type['email'] != '')
							$relances_mail = $count_type['email'];
					}
							
					//D�tails du total recouvr�
					$total_rec = 0; $total_rec_tel = 0; $total_rec_mail = 0;
					if(isset($vente_count_type) && !empty($vente_count_type)){
						
						$total_rec = $rc_cnt ? tep_round(($custknt / $rc_cnt) * 100, 2) : 0;
						$total_rec_tel = tep_round(($vente_count_type['t&eacute;l&eacute;phone'] / $rc_cnt) * 100, 2);
						$total_rec_mail = tep_round(($vente_count_type['email'] / $rc_cnt) * 100, 2);
						
					}
				  	
					//D�tails des validations panier
					$ventes_tel = 0; $ventes_mail = 0;
					if(isset($vente_count_type) && !empty($vente_count_type)){
						
						if ($vente_count_type['t&eacute;l&eacute;phone'] != '')
							$ventes_tel = $vente_count_type['t&eacute;l&eacute;phone'];
						if ($vente_count_type['email'] != '')
							$ventes_mail = $vente_count_type['email'];
					}
					
					//D�tails des types
					$ventes_tel_prct = 0; $ventes_mail_prct = 0; $ventes_tot_prct = 0;
					if(isset($vente_count_type) && !empty($vente_count_type)){
						
						if ($count_type['t&eacute;l&eacute;phone'] != 0)
							$ventes_tel_prct = tep_round(($vente_count_type['t&eacute;l&eacute;phone'] / $count_type['t&eacute;l&eacute;phone']) * 100, 2);
						if ($count_type['email'] != 0)
							$ventes_mail_prct = tep_round(($vente_count_type['email'] / $count_type['email']) * 100, 2);
							
						$ventes_tot_prct = tep_round($ventes_tel_prct + $ventes_mail_prct, 2);
					}
                    ?>
            
                    <!-- Tableaux de stats -->
                    <tr><td height="15" COLSPAN=8>&nbsp;</td></tr>
                    
                    <tr>
                    	<td>&nbsp;</td>
                        <td align=left COLSPAN=4 class=main>
                            <table border=1 width="100%" cellspacing="2" cellpadding="2">
                                <tr class="dataTableHeadingRow">
                                   <td class="dataTableHeadingContent">&nbsp;</td>
                                   <td class="dataTableHeadingContent">Globales</td>
                                   <td class="dataTableHeadingContent">Par T�l�phone</td>
                                   <td class="dataTableHeadingContent">Par eMail</td>
                                </tr>
                                <tr>
                                   <td class="dataTableHeadingContent">Relances</td>
                                   <td class="datatablecontent"><?php echo $rc_cnt; ?></td>
                                   <td class="datatablecontent"><?php echo $relances_tel; ?></td>
                                   <td class="datatablecontent"><?php echo $relances_mail; ?></td>
                                </tr>
								<tr>
                                   <td class="dataTableHeadingContent">Validation panier</td>
                                   <td class="datatablecontent"><?php echo $custknt; ?></td>
                                   <td class="datatablecontent"><?php echo $ventes_tel; ?></td>
                                   <td class="datatablecontent"><?php echo $ventes_mail; ?></td>
                                </tr>
                                <tr>
                                   <td class="dataTableHeadingContent">Validation panier / Relances globales</td>
                                   <td class="datatablecontent"><?php echo $total_rec . "%"; ?></td>
                                   <td class="datatablecontent"><?php echo $total_rec_tel . "%"; ?></td>
                                   <td class="datatablecontent"><?php echo $total_rec_mail . "%"; ?></td>
                                </tr>
                                <tr>
                                    <td class="dataTableHeadingContent">Validation panier / Relances par type</td>
                                    <td class="datatablecontent">&nbsp;</td>
                                    <td class="datatablecontent"><?php echo $ventes_tel_prct . "%"; ?></td>
                                    <td class="datatablecontent"><?php echo $ventes_mail_prct . "%"; ?></td>
                                </tr>
                            </table>
                        </td>
                                
                        <td>&nbsp;</td>
                                
                        <td align="right" COLSPAN=4 class=main>
                            <table border=1 width="100%" cellspacing="2" cellpadding="2">
                                <tr class="dataTableHeadingRow">
                                   <td class="dataTableHeadingContent">&nbsp;</td>
                                   <td class="dataTableHeadingContent">Nombre de relances</td>
                                   <td class="dataTableHeadingContent">C. Affaire TTC</td>
                                   <td class="dataTableHeadingContent">Marge HT</td>
                                   <td class="dataTableHeadingContent">Pourcentage</td>
                                </tr>
                                
                                <tr>
                                    <td class="dataTableHeadingContent">Global</td>
                                    <td class="dataTableHeadingContent"><?php echo $rc_cnt; ?></td>
                                    <td class="dataTableHeadingContent"><?php echo $currencies->format(tep_round($total_recovered, 2)); ?></td>
                                    <td class="dataTableHeadingContent" style="color: red;"><b><?php echo $currencies->format($marge_totale); ?></b></td>
                                    <td class="dataTableHeadingContent">&nbsp;</td>
                                </tr>
                                
                                <?php
                                //Calcul du nombre de vendeurs
                                $totVend = 0;
                                for ($i=0; $i<sizeof($list_admins_array)-1; $i++)
                                {
                                  $totVend += $list_admins_array[$i]['relances'];
                                }
                                
								// S'il y a au moins un vendeur on affiche le tableau
                                if ($totVend != 0)
                                {
                                    for ($i=0; $i<sizeof($list_admins_array); $i++)
                                    {
                                        if ($list_admins_array[$i]['firstname'] != '' && $list_admins_array[$i]['relances'] != 0)
                                        {
										?>
										<tr>
										   <td class="dataTableHeadingContent"><?php echo $list_admins_array[$i]['firstname']; ?></td>
										   <td class="datatablecontent"><?php echo $list_admins_array[$i]['relances']; ?></td>
										   <td class="datatablecontent"><?php echo $currencies->format($list_admins_array[$i]['ca_ttc']); ?></td>
										   <td class="datatablecontent" style="color: red;"><b><?php echo $currencies->format(tep_round($list_admins_array[$i]['marge'], 2)); ?></b></td>
										   <td class="datatablecontent" style="color: red;"><b><?php if ($marge_totale != 0){ echo tep_round((($list_admins_array[$i]['marge'])/$marge_totale)*100, 2); } else { echo '0'; } ?> %</b></td>
										</tr>
										<?php
                                        }
                                    }
                                }
								// S'il n'y a aucun vendeur on affiche "Aucune donn�e"
                                else {
									?>
									<tr>
									   <td class="dataTableHeadingContent" colspan="5" align="center">Aucune donn�e</td>
									</tr>
									<?php
                                }?>
                            </table>
                        </td>
                    </tr>
                        
                    <tr><td height="15" COLSPAN=6> </td></tr>
                    <!-- Header du tableau des commandes -->
                    <tr class="dataTableHeadingRow">
                        <td width="5%" class="dataTableHeadingContent" align="right" height="18">Commande</td>
                        <td width="1%" class="dataTableHeadingContent">&nbsp;</td>
                        <td width="10%" class="dataTableHeadingContent" align="center">Date de relance</td>
                        <td width="1%" class="dataTableHeadingContent">&nbsp;</td>
                        <td width="25%" class="dataTableHeadingContent">Nom</td>
                        <td width="10%" class="dataTableHeadingContent" align="center">Type rappel</td>
                        <td width="7%" class="dataTableHeadingContent">Date de commande</td>
                        <td width="10%" class="dataTableHeadingContent" align="center">Statut</td>
                        <td width="10%" class="dataTableHeadingContent" align="right">Chiffre d'affaire</td>
                        <td width="10%" class="dataTableHeadingContent" align="right">Marge</td>
                        <td width="5%" class="dataTableHeadingContent" align="center">Vendeur</td>
                        <td width="10%" class="dataTableHeadingContent" align="center">Action</td>
                    </tr>
                    
                    <?php echo $custlist; /* Affichage des donn�es */ ?>
                    
                    <tr>
                        <td colspan="12" valign="bottom"><hr width="100%" size="1" color="#800000" noshade></td>
                    </tr>
                    
                    <!-- Affichage des Totaux -->
                    <tr class="main">
                      <td align="right" valign="center" colspan=4 class="main"></td>
                      <td align="left" colspan=4 class="main"></td>
                      <td class="main" align="right"><b><?php echo $currencies->format(tep_round($total_recovered, 2)) ?></b></td>
                      <td class="main" align="right" style="color: red;"><b><?php echo $currencies->format($marge_totale) ?></b></td>
                    </tr>
                </table>
            </td>
         </tr>
    </table>

    <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
    <br/>
    </body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>