<?php
/*
  $Id: stats_customers.php,v 1.31 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
  

//debut modification Polytech Montpellier
		
//valeurs de la liste deroulantes pr e nombre d element a afficher
$pages_array = array();$i=20;
while($i<=100){
$pages_array[] = array('id' => $i, 'text' => $i);
$i=$i+10;}


// detecte les constantes ou initialise
if (isset($_GET['sort'])) $sort = tep_db_prepare_input($HTTP_GET_VARS['sort']); 
else $sort = '4d';
if (isset($_GET['nbmax'])) $nbmax = tep_db_prepare_input($HTTP_GET_VARS['nbmax']); 
else $nbmax = 20;
if (isset($_GET['page'])) $num_page = tep_db_prepare_input($HTTP_GET_VARS['num_page']); 
else $num_page=1;
 
$today = getdate();
if ($HTTP_GET_VARS['yearcust']) $yearcust = tep_db_prepare_input($HTTP_GET_VARS['yearcust']); 
else $yearcust = $today['year']; 
if ($HTTP_GET_VARS['monthcust']) $monthcust = tep_db_prepare_input($HTTP_GET_VARS['monthcust']); 
else $monthcust = $today['mon']; 	
			
		
		
//  liste deroulante des annees depuis le debut de l'oscommerce
$yearcust_begin_query = tep_db_query(" select startdate from counter");
$yearcust_begin = tep_db_fetch_array($yearcust_begin_query);
$yearcust_begin = substr($yearcust_begin['startdate'], 0, 4);
$current_yearcust = $yearcust_begin;
while ($current_yearcust != $today['year'] + 1) {
  $list_yearcust_array[] = array('id' => $current_yearcust,
                              'text' => $current_yearcust);
$current_yearcust++;
}

// liste deroulante des mois
  $list_monthcust = array(JAN, FEV, MAR, AVR, MAI, JUN, JUI, AOU, SEP, OCT, NOV, DEC);
  for ($i = 0, $n = sizeof($list_monthcust); $i < $n; $i++) {
    $list_monthcust_array[] = array('id' => $i+1,
                                'text' => $list_monthcust[$i]);
}		

$parametres = array('sort' => $sort,
                         'nbmax' => $nbmax,
                         'monthcust' => $monthcust,
                         'yearcust' => $yearcust,
                         'nbmax' => $nbmax);
//fin modification Polytech Montpellier

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
	<?php echo tep_draw_form('search', FILENAME_STATS_CUSTOMERS, '', 'get'); ?>
      <tr>
        <td>
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10%" class="pageHeading"><?php echo 'Les meilleurs clients'; ?></td>
            <td width="30%" class="smallText" align="right">
			<?php	$type_client_array = array();
					$type_client_array[] = array('id' => "", 'text' => "Tous");
					
				$query = 'select type_id, type_name from '. TABLE_CUSTOMERS_TYPE .' order by type_order';
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					
					$type_client_array[] = array('id' => $data['type_id'], 'text' => $data['type_name']);
				}
					echo "Type de client :&nbsp;".tep_draw_pull_down_menu('type_client', $type_client_array, '', 'onChange="this.form.submit();"');?>
			</td>
            <td width="30%" class="smallText" align="center">
			<?php echo "nombre d'elements par page :";
			echo tep_draw_pull_down_menu('nbmax', $pages_array, '', 'onChange="this.form.submit();"');?>&nbsp;</td>
			<td width="30%" class="smallText" align="right"><?php echo "afficher pour:";
			echo tep_draw_pull_down_menu('monthcust', array_merge(array(array('id' => 'ALL', 'text' => 'Tous les mois')),
			$list_monthcust_array), '', 'onChange="this.form.submit();"');?>&nbsp;
			<?php 
			echo tep_draw_pull_down_menu('yearcust', array_merge(array(array('id' => 'ALL', 'text' => 'Toutes les ann&eacute;es')),
			$list_yearcust_array), '', 'onChange="this.form.submit();"'); ?>&nbsp;</td>
          </tr>
        </table>
		</td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
			<!-- Extension Polytech'Montpellier -->
              <tr class="dataTableHeadingRow">
                <td width="5%" class="dataTableHeadingContent"><?php echo 'No.'; ?></td>
                <td width="35%" class="dataTableHeadingContent"><?php echo 'Clients'; ?></td>
				
				<td width="15%" class="dataTableHeadingContent" align="center"><?php echo 'Nb Commandes'; ?>
			<!-- 	 <?php //echo '<a href="' .tep_href_link(FILENAME_STATS_CUSTOMERS, 'page='.$_GET['page'].'&sort='.$_GET['sort'].'&nbmax='.$_GET['nbmax'].'&monthcust='.$_GET['monthcust'].'&yearcust='.$_GET['yearcust']) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_up.GIF', "UP") . '</a>';  ?> 
				<?php //echo '<a href="' .tep_href_link(FILENAME_STATS_CUSTOMERS, 'sort=1d') . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_down.GIF', "DOWN") . '</a>';  ?> -->
				</td><!-- nb commande //-->
				
				<td width="15%" class="dataTableHeadingContent" align="center"><?php echo 'Nb Produits'; ?>
				<?php echo '<a href="' .tep_href_link(FILENAME_STATS_CUSTOMERS, 'page='.$num_page.'&sort=2a&nbmax='.$nbmax.'&monthcust='.$monthcust.'&yearcust='.$yearcust) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_up.gif', "UP") . '</a>';?>     
				<?php echo '<a href="' .tep_href_link(FILENAME_STATS_CUSTOMERS, 'page='.$num_page.'&sort=2d&nbmax='.$nbmax.'&monthcust='.$monthcust.'&yearcust='.$yearcust) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_down.gif', "DOWN") . '</a>';?>
				</td> <!-- nb articles //-->     
				
				<td width="15%" class="dataTableHeadingContent" align="center"><?php echo 'Marge'; ?>
				<?php echo '<a href="' .tep_href_link(FILENAME_STATS_CUSTOMERS, 'page='.$num_page.'&sort=3a&nbmax='.$nbmax.'&monthcust='.$monthcust.'&yearcust='.$yearcust) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_up.gif', "UP") . '</a>';  ?>
			    <?php echo '<a href="' .tep_href_link(FILENAME_STATS_CUSTOMERS, 'page='.$num_page.'&sort=3d&nbmax='.$nbmax.'&monthcust='.$monthcust.'&yearcust='.$yearcust) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_down.gif', "DOWN") . '</a>';  ?>
				</td>  <!-- marge //-->
                
				<td width="15%" class="dataTableHeadingContent" align="center"><?php echo 'Total achet&eacute;'; ?>
				<?php echo '<a href="' .tep_href_link(FILENAME_STATS_CUSTOMERS, 'page='.$num_page.'&sort=4a&nbmax='.$nbmax.'&monthcust='.$monthcust.'&yearcust='.$yearcust) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_up.gif', "UP") . '</a>';  ?>
			    <?php echo '<a href="' .tep_href_link(FILENAME_STATS_CUSTOMERS, 'page='.$num_page.'&sort=4d&nbmax='.$nbmax.'&monthcust='.$monthcust.'&yearcust='.$yearcust) . '" TARGET="_self">'. tep_image(DIR_WS_IMAGES . '/icon_down.gif', "DOWN") . '</a>';  ?>
				</td><!-- total des achats //-->
				<!-- Fin Extension Polytech'Montpellier -->
              </tr>
<?php
	
  if (isset($HTTP_GET_VARS['page']) && ($HTTP_GET_VARS['page'] > 1)) $rows = $HTTP_GET_VARS['page'] * $nbmax - $nbmax;
  
    
  $customers_query_raw = "select o.customers_id, c.customers_firstname, c.customers_lastname,c.customers_email_address,
  sum(op.products_quantity) as nbProducts,
  sum(op.products_quantity * op.final_price)-sum(op.products_quantity * op.products_cost) as margin,
  sum(op.products_quantity * op.final_price) as totalorders 
  from " . TABLE_CUSTOMERS . " c, " . TABLE_ORDERS_PRODUCTS . " op, " . TABLE_ORDERS . " o 
  where c.customers_id = o.customers_id and o.orders_id = op.orders_id";
  
  if (isset($HTTP_GET_VARS['type_client']) && tep_not_null($HTTP_GET_VARS['type_client'])) {
	$customers_query_raw .= " and c.customers_type='" . tep_db_input(tep_db_prepare_input($HTTP_GET_VARS['type_client'])) . "'";
  }
  if ($monthcust != 'ALL') $customers_query_raw .= " and month(date_purchased) = " . $monthcust ;
  if ($yearcust != 'ALL') $customers_query_raw .= " and year(date_purchased) = " . $yearcust;
  
  $customers_query_raw .= " group by o.customers_id";
  
  $end_req =" order by totalorders DESC";

    
  switch ($sort) {
		  case '1d':
            $end_req =" order by nbOrders DESC";//pour le tri de maniere descandante
          break;
		  case '2d':
		    $end_req =" order by nbProducts DESC";
            break;
          case '3d':
		    $end_req =" order by margin DESC";
            break;  
		  case '4d':
            $end_req =" order by totalorders DESC";
          break;
		  case '1a':
            $end_req =" order by nbOrders ASC";//pour le tri de maniere ascendante
          break;
		  case '2a':
		    $end_req =" order by nbProducts ASC";
            break;
          case '3a':
		    $end_req =" order by margin ASC";
            break;  
		  case '4a':
            $end_req =" order by totalorders ASC";
          break;
	}
		
  $customers_query_raw=$customers_query_raw . $end_req;
 
  //requete permettant de recuper le nombre de commande par client
  $customers_query_nb_orders="select customers_id, count(customers_id) as nbOrders from orders group by customers_id";
  $customers_nb_orders = tep_db_query($customers_query_nb_orders);
  //tableau permettant d'associer l'ID d 'un client et son nombre de commande
  $commandes=array();
  while($nb_orders = tep_db_fetch_array($customers_nb_orders)) 
	{$commandes[$nb_orders['customers_id']]=$nb_orders['nbOrders'];}


  $customers_split = new splitPageResults($HTTP_GET_VARS['page'], $nbmax, $customers_query_raw,$customers_query_numrows2);
  
  // pour fixer le nombre de clients en bas a gauche de la page
  $customers_query_numrows="SELECT count(customers_id) as nb FROM orders";
  if (($monthcust != 'ALL') && ($yearcust != 'ALL')) {$customers_query_numrows .=" WHERE month(date_purchased)=".$monthcust." and year(date_purchased)=".$yearcust;}
  if (($monthcust != 'ALL')&& ($yearcust == 'ALL')) {$customers_query_numrows .=" WHERE month(date_purchased)=".$monthcust;}
  if (($yearcust != 'ALL')&&($monthcust == 'ALL')) {$customers_query_numrows .=" WHERE year(date_purchased)=".$yearcust;}
  $customers_query_numrows .=" group by customers_id";
  $customers_query_count_numrows = tep_db_query($customers_query_numrows);
  // pour fixer le nombre de dde produits differents en bas a gauche de la page
   while($nb_customers = tep_db_fetch_array($customers_query_count_numrows)) $customers_query_numrows2=$nb_customers['nb'];
  
  
  $rows = 0;
  $customers_query = tep_db_query($customers_query_raw);
   while ($customers = tep_db_fetch_array($customers_query)) {
    $rows++;
	
    if (strlen($rows) < 2) { $rows = '0' . $rows; }
?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" onClick="document.location.href='<?php echo tep_href_link(FILENAME_CLIENT_LIST, 'search=' . $customers['customers_lastname'], 'NONSSL'); ?>'">
                <td class="dataTableContent"><?php echo $rows; ?>.</td>
				<!-- Extension Polytech'Montpellier -->
				<!--   REDIRECTION VIA CODE CLIENT VERS LA FICHE CLIENT -->
                <td class="dataTableContent"><?php echo '<a href="' . tep_href_link(FILENAME_CLIENT_EDIT, tep_get_all_get_params(array('cID')) . 'cID=' . $customers['customers_id']) . '&action=edit">' 
				. strtoupper($customers['customers_lastname'])." ".$customers['customers_firstname'].'</a>';  ?></td>
				
				 <td class="dataTableContent" align="center"><?php echo '<a href="' . tep_href_link(FILENAME_CMD_LIST, tep_get_all_get_params(array('cID')) . 'cID=' . $customers['customers_id']) . '">
				' .$commandes[$customers['customers_id']] . '</a>';  ?> &nbsp;</td>
				 <td class="dataTableContent" align="center"><?php echo $customers['nbProducts']; ?> &nbsp;</td>
				 <td class="dataTableContent" align="center"><?php if ($customers['margin']<0){echo 'Prix d\'achat non renseigné';}
				else{ echo $currencies->format($customers['margin']); }?>&nbsp;</td>
				<!-- Fin Extension Polytech'Montpellier -->
			<td class="dataTableContent" align="center"><?php echo $currencies->format($customers['totalorders']); ?> &nbsp;</td>
              </tr>
<?php
  }
							
?>
    </table></td>
        </tr>
          <tr>
            <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php 
				echo $customers_split->display_count($customers_query_numrows, $nbmax, 
				$HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></td>
                <td class="smallText" align="right">
				<?php echo $customers_split->display_links_with_sort2($customers_query_count_numrows, 
				 $nbmax,MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page'],$parametres);?>				
				&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
