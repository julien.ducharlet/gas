<?php
/*
  $Id: orders_status.php,v 1.22 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  $action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {
      case 'insert':
      case 'save':
        if (isset($HTTP_GET_VARS['oID'])) $orders_status_id = tep_db_prepare_input($HTTP_GET_VARS['oID']);

        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $orders_status_name_array = $HTTP_POST_VARS['orders_status_name'];
          $language_id = $languages[$i]['id'];
		  
		  if(isset($HTTP_POST_VARS['marge'])){
			  $marge=$HTTP_POST_VARS['marge'];
		  }
		  else{
		  	$marge=0;
		  }
		  
          $sql_data_array = array('orders_status_name' => tep_db_prepare_input($orders_status_name_array[$language_id]),
								  'orders_status_marge' => tep_db_prepare_input($marge)
								 );

          if ($action == 'insert') {
            if (empty($orders_status_id)) {
              $next_id_query = tep_db_query("select max(orders_status_id) as orders_status_id from " . TABLE_ORDERS_STATUS . "");
              $next_id = tep_db_fetch_array($next_id_query);
              $orders_status_id = $next_id['orders_status_id'] + 1;
            }

            $insert_sql_data = array('orders_status_id' => $orders_status_id,
                                     'language_id' => $language_id);

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            tep_db_perform(TABLE_ORDERS_STATUS, $sql_data_array);
          } 
		  
		  // MISE A JOUR DE LA BASE DE DONNEE LORSQU'ON CLIQUE SUR LE BOUTON MAJ
		  elseif ($action == 'save') {
		    // Mise a jour des nouveau champs, commentaire et message 
			$insert_sql_data = array('orders_status_comments' => $_POST["mail"],
									 'orders_status_message' => $_POST["sms"] );
			$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
            tep_db_perform(TABLE_ORDERS_STATUS, $sql_data_array, 'update', "orders_status_id = '" . (int)$orders_status_id . "' and language_id = '" . (int)$language_id . "'");
          }
        }

        if (isset($HTTP_POST_VARS['default']) && ($HTTP_POST_VARS['default'] == 'on')) {
          tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($orders_status_id) . "' where configuration_key = 'DEFAULT_ORDERS_STATUS_ID'");
        }

        tep_redirect(tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $orders_status_id));
        break;
      case 'deleteconfirm':
        $oID = tep_db_prepare_input($HTTP_GET_VARS['oID']);

        $orders_status_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'DEFAULT_ORDERS_STATUS_ID'");
        $orders_status = tep_db_fetch_array($orders_status_query);

        if ($orders_status['configuration_value'] == $oID) {
          tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '' where configuration_key = 'DEFAULT_ORDERS_STATUS_ID'");
        }

        tep_db_query("delete from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . tep_db_input($oID) . "'");

        tep_redirect(tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page']));
        break;
      case 'delete':
        $oID = tep_db_prepare_input($HTTP_GET_VARS['oID']);

        $status_query = tep_db_query("select count(*) as count from " . TABLE_ORDERS . " where orders_status = '" . (int)$oID . "'");
        $status = tep_db_fetch_array($status_query);

        $remove_status = true;
        if ($oID == DEFAULT_ORDERS_STATUS_ID) {
          $remove_status = false;
          $messageStack->add('Erreur : Le statut par d&eacute;faut ne peut pas &ecirc;tre supprim&eacute;. Merci de choisir un autre statut par d&eacute;faut et de r&eacute;essayer', 'error');
        } elseif ($status['count'] > 0) {
          $remove_status = false;
          $messageStack->add('Erreur : Ce statut de commande est actuellement utilis&eacute;.', 'error');
        } else {
          $history_query = tep_db_query("select count(*) as count from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_status_id = '" . (int)$oID . "'");
          $history = tep_db_fetch_array($history_query);
          if ($history['count'] > 0) {
            $remove_status = false;
            $messageStack->add('Erreur : Ce statut de commande est d&eacute;j&agrave; utilis&eacute; dans l\'historique de commande.', 'error');
          }
        }
        break;
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
	<script language="JavaScript">
	// Fonction qui controle que le nombre de caractere du message ne depasse pas 160 
	function Calcul(letexte) 
	{
		nbcar = letexte.value.length
		if (nbcar > 160 ) {
			document.status.Name_Sms.value = letexte.value.substring(0, 160);
			alert("Vous avez atteint la limite de 160 caract�res.\nLa fin de votre message a �t� tronqu�e.\nVous pouvez encore le modifier avant de l'envoyer. ");
		valcar = 0;
		} 
		else  
			valcar = 160 - nbcar;
		document.status.carac.value = valcar;
	}
	</script>
	
	
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo 'Statut des commandes'; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" width="50%">
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent">Statut des commandes</td>
                <td class="dataTableHeadingContent">Marge</td>
                <td class="dataTableHeadingContent" align="right">Action&nbsp;</td>
              </tr>
<?php
  $orders_status_query_raw = "select * from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' order by orders_status_name";
  $orders_status_split = new splitPageResults($HTTP_GET_VARS['page'], 50, $orders_status_query_raw, $orders_status_query_numrows);
  $orders_status_query = tep_db_query($orders_status_query_raw);
  while ($orders_status = tep_db_fetch_array($orders_status_query)) {
    if ((!isset($HTTP_GET_VARS['oID']) || (isset($HTTP_GET_VARS['oID']) && ($HTTP_GET_VARS['oID'] == $orders_status['orders_status_id']))) && !isset($oInfo) && (substr($action, 0, 3) != 'new')) {
      $oInfo = new objectInfo($orders_status);
    }

    if (isset($oInfo) && is_object($oInfo) && ($orders_status['orders_status_id'] == $oInfo->orders_status_id)) {
      echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $oInfo->orders_status_id . '&action=edit') . '\'">' . "\n";
		
    } else {
      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $orders_status['orders_status_id']) . '\'">' . "\n";
    }

    if (DEFAULT_ORDERS_STATUS_ID == $orders_status['orders_status_id']) {
      echo '<td class="dataTableContent"><b>' . $orders_status['orders_status_name'] . ' (' . TEXT_DEFAULT . ')</b></td>' . "\n";
    } else {
      echo '<td class="dataTableContent" width="450">' . $orders_status['orders_status_name'] . '</td>' . "\n";
    }
?>

		<td class="dataTableContent">
		<?php 	if ($orders_status['orders_status_marge'] == 1){
					echo "Inclus dans la marge"; 
				} 
				else { 
					echo 'NON inclus dans la marge';
				} ?>
                &nbsp;
        </td>
        
        <!--  CODE POUR LE COLONNE ACTION  -->
        <td class="dataTableContent" align="right"><?php if (isset($oInfo) && is_object($oInfo) && ($orders_status['orders_status_id'] == $oInfo->orders_status_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $orders_status['orders_status_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
        </tr>
<?php
  }
?>
              <tr>
                <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $orders_status_split->display_count($orders_status_query_numrows, 50, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS_STATUS); ?></td>
                    <td class="smallText" align="right"><?php echo $orders_status_split->display_links($orders_status_query_numrows, 50, 50, $HTTP_GET_VARS['page']); ?></td>
                  </tr>
<?php
  if (empty($action)) {
?>
                  <tr>
                    <td colspan="2" align="right"><?php echo '<a href="' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&action=new') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>'; ?></td>
                  </tr>
<?php
  }
?>
                </table>
				
				</td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<b>' . 'Nouveau statut de commande' . '</b>');

      $contents = array('form' => tep_draw_form('status', FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&action=insert'));
      $contents[] = array('text' => 'Merci de compl&eacute;ter cette nouvelle commande avec les donn&eacute;es li&eacute;es');

      $orders_status_inputs_string = '';
      $languages = tep_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $orders_status_inputs_string .= '<br>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('orders_status_name[' . $languages[$i]['id'] . ']');
      }

      $contents[] = array('text' => '<br>' . 'Statut des commandes :' . $orders_status_inputs_string);
      $contents[] = array('text' => '<br>' . tep_draw_checkbox_field('marge','1') . ' Comptabilis� dans la marge');
	  $contents[] = array('text' => '<br>' . tep_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
      $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . ' <a href="' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
	  
      break;
	  
	  
	  
	// ACTION ASSOCIEE AU BOUTON EDIT !
    case 'edit':
      $heading[] = array('text' => '<b>' . 'Editer statut de commande' . '</b>');

      $contents = array('form' => tep_draw_form('status', FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $oInfo->orders_status_id  . '&action=save'));
	  
      $contents[] = array('text' => 'Merci de faire les changements n&eacute;cessaires');

      $orders_status_inputs_string = '';
      $languages = tep_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $orders_status_inputs_string .= '<br>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('orders_status_name[' . $languages[$i]['id'] . ']', tep_get_orders_status_name($oInfo->orders_status_id, $languages[$i]['id']), 'size=40');
      }

	  // Champs texte avec du texte a l interieur
      $contents[] = array('text' => '<br>' .'Statut des commandes :' . $orders_status_inputs_string);

      if (DEFAULT_ORDERS_STATUS_ID != $oInfo->orders_status_id) $contents[] = array('text' => '<br>' . tep_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
	  
	  $contents[] = array('text' => '<br>' . tep_draw_checkbox_field('marge','1',$oInfo->orders_status_marge==1) . ' Comptabilis� dans la marge');
	  
	// Champs commentaire avec le commantaire qui va avec
	$contents[] = array('text' => '<br><b>&nbsp;&nbsp;-'.Commentaire.'-</b>'
	.tep_draw_textarea_field('mail', 'soft', '60', '5',$oInfo->orders_status_comments)); 

	
	// Champs sms avec le message qui va avec 
	$contents[] = array('text' => '');  
	$contents[] = array('text' => '<br><b>&nbsp;&nbsp;-'.Message.		  
	'-</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(
	<input maxLength="3" name="carac" size="2" style="BORDER-RIGHT: #ffffff 1px solid;BORDER-TOP: 
			#ffffff 1px solid; FONT-SIZE: 11px; BORDER-LEFT: #ffffff 1px solid; COLOR: #800000; BORDER-BOTTOM:
			#ffffff 1px solid; FONT-FAMILY: Verdana" 
            value="160" ID="Text2"/> caract�re(s) restant(s))'
	.tep_draw_textarea_field('sms', 'soft', '60', '3',$oInfo->orders_status_message,"onkeydown=Calcul(this) onblur=Calcul(this) onkeyup=Calcul(this)  onfocus=Calcul(this) id='Textarea_sms' "));    
	
	// BOUTON MISE A JOUR ET ANNULER
    $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $oInfo->orders_status_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
	   
    break;
	  
	  
	  
    case 'delete':
      $heading[] = array('text' => '<b>' . 'Supprimer statut de commande' . '</b>');

      $contents = array('form' => tep_draw_form('status', FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $oInfo->orders_status_id  . '&action=deleteconfirm'));
      $contents[] = array('text' => 'Etes vous sur de vouloir supprimer ce statut de commande ?');
      $contents[] = array('text' => '<br><b>' . $oInfo->orders_status_name . '</b>');
      if ($remove_status) {
		  $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $oInfo->orders_status_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
		  }
	  else {$contents[] = array('align' => 'center', 'text' => '<br /><font color="red"> il n\'est pas possible de supprimer ce statut car il est toujours utilis� dans l\'historique de certaines commandes<br /> <br /><strong>TABLE : orders_status_history et orders</strong></font><br /><br />');}
	  	  
      break;
    default:
      if (isset($oInfo) && is_object($oInfo)) {
        $heading[] = array('text' => '<b>' . $oInfo->orders_status_name . '</b>');

        $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $oInfo->orders_status_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $HTTP_GET_VARS['page'] . '&oID=' . $oInfo->orders_status_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');

        $orders_status_inputs_string = '';
        $languages = tep_get_languages();
		
		// AFFICHAGE DU DRAPEAU DE LA LANGUE COURANTE
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $orders_status_inputs_string .= '<br>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_get_orders_status_name($oInfo->orders_status_id, $languages[$i]['id']);
        }

        $contents[] = array('text' => $orders_status_inputs_string);
		
		// Champs commentaire avec le commantaire qui va avec
		$mail;
		if(strlen($oInfo->orders_status_comments)!=0){
			$mail = $oInfo->orders_status_comments; 
		}
		else{
			$mail =' <i>Aucun commentaire</i>'; 
		}
		$contents[] = array('text' => '<br><b>&nbsp;&nbsp;-'.Commentaire.'-</b><br>'
		.$mail); 
		
		// Champs sms avec le commantaire qui va avec
		$sms;
		if(strlen($oInfo->orders_status_message)!=0){
			$sms = $oInfo->orders_status_message; 
		}
		else{
			$sms ='<i>Aucun sms</i>'; 
		};
		$contents[] = array('text' => '<br><br><b>&nbsp;&nbsp;-'.Message.'-</b><br>'
		.$sms.'<br><br>');
		
		
      }
      break;
  }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="30%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);
	
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
