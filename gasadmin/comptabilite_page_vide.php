<?php
/*
  PAGE: comptabilite_page_vide.php 
  Version du 25/07/2009
*/

  require('includes/application_top.php');

 
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">

<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->

    <td width="100%" valign="top">
		<table border="0" width="100%" cellspacing="0" cellpadding="2">
			
			<tr>
				<td width="100%">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td class="pageHeading">TITRE DE LA PAGE</td>
						</tr>
					</table>
				</td>
			</tr>
				   
			<tr>
				<td align="left" class="main">	
				TEXTE D'EXPLICATION	  
				</td>
			</tr>
	   
			<tr>
			  <td align="left">	
				ZONE 1	
			  </td>
			</tr>
			
			<tr>
			  <td align="left">
				ZONE 2
			  </td>
			</tr>	   
	   
		</table>
	</td>
	
<!-- body_text_oef //-->
	
	
</tr>
</table>
</td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
