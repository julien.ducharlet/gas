<?php
	require('includes/application_top.php');
	
	$pID=$HTTP_GET_VARS['pID'];	
	if (isset($_GET['cPath'])) $cPath=$_GET['cPath'];
	
	
	function getFullMonth($month) {
		
		switch((int)$month) {
			
			case 1: return "Janvier";
				break;
			case 2: return "F�vrier";
				break;
			case 3: return "Mars";
				break;
			case 4: return "Avril";
				break;
			case 5: return "Mai";
				break;
			case 6: return "Juin";
				break;
			case 7: return "Juillet";
				break;
			case 8: return "Ao�t";
				break;
			case 9: return "Septembre";
				break;
			case 10: return "Octobre";
				break;
			case 11: return "Novembre";
				break;
			case 12: return "D�cembre";
				break;
			default: return "Janvier";
				break;
		}
	}
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <script language="javascript" src="includes/general.js"></script>
    </head>
    
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="goOnLoad();">
		<?php 
			require(DIR_WS_INCLUDES . 'header.php');
			
			$parameters = array('products_id' => '',
								'products_name' => '',
								'products_quantity' => '',
								'products_quantity_reel' => '',
								'products_price' => '',
								'products_cost' => '',
								'products_weight' => '',
								'products_date_added' => '',
								'products_last_modified' => '',
								'products_date_available' => '',
								'products_status' => '',
								'products_quantity_min' => '',
								'products_quantity_max' => '',
								'products_quantity_ideal' => '');
			
			$pInfo = new objectInfo($parameters);
			
			$tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
			$tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
			while ($tax_class = tep_db_fetch_array($tax_class_query)) {
				$tax_class_array[] = array('id' => $tax_class['tax_class_id'],
				'text' => $tax_class['tax_class_title']);
			}
			
			$languages = tep_get_languages();
			
			$product_query = tep_db_query("select pd.products_name, p.products_id, p.products_quantity, p.products_quantity_reel, p.products_model, p.products_ref_origine, p.products_garantie, p.products_code_barre, p.products_price, p.products_cost, p.products_weight, p.products_date_added, p.products_last_modified, date_format(p.products_date_available, '%Y-%m-%d') as products_date_available, p.products_status, p.products_tax_class_id, p.products_quantity_min, p.products_quantity_max, p.products_quantity_ideal from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$HTTP_GET_VARS['pID'] . "' AND p.products_id = pd.products_id");
			$product = tep_db_fetch_array($product_query);
			$pInfo->objectInfo($product);
			
			if (!isset($pInfo->products_status)) $pInfo->products_status = '1';
			
			switch ($pInfo->products_status) {
			case '0': $in_status = false; $out_status = true; break;
			case '1':
			default: $in_status = true; $out_status = false;
			}
			
			require(DIR_WS_ARTICLE . 'article_menu.php');
			require(DIR_WS_ARTICLE . 'position_article.php');
		?>
        
        <span style="padding: 10px;" class="pageHeading">Ventes des 12 derniers mois</span>
        
        <?php require(DIR_WS_ARTICLE . 'article_ventes_sur_12_mois.php'); ?>
        
        <span style="padding: 10px;" class="pageHeading">Statistiques</span>
        
        <div style="margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
            <table border="0" width="50%" cellspacing="0" cellpadding="2">
                <tr>
                    <td style="font-weight: bold;">Date de cr&eacute;ation : </td>
                    <td class="main">
                        <?php 
                            $res_crea=tep_db_query("SELECT products_date_added as creation FROM products where products_id='".$pID."'");
                            $row_crea=tep_db_fetch_array($res_crea);
                            echo tep_date_short($row_crea["creation"]);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Nb ventes : </td>
                    <td class="main">
                        <?php 
                            $res_ventes=tep_db_query("SELECT count(1) as nb FROM ".TABLE_ORDERS_PRODUCTS." where products_id='".$pID."'");
                            $row_ventes=tep_db_fetch_array($res_ventes);
                            echo $row_ventes["nb"]." ventes";
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Ventes moyennes : </td>
                    <td class="main">
                        <?php 
                            $res_ventes=tep_db_query("SELECT date_purchased, final_price, count(1) as nb FROM ".TABLE_ORDERS_PRODUCTS." op inner join ".TABLE_ORDERS." o on o.orders_id=op.orders_id where products_id='".$pID."' group by final_price order by date_purchased");
                            while($row_ventes=tep_db_fetch_array($res_ventes)){
                            echo $row_ventes["nb"]." ventes � ".$row_ventes["final_price"]."<br />";
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Nb visites : </td>
                    <td class="main">
                        <?php 
                            $res_visites=tep_db_query("SELECT count(1) as nb, min(date_visite) as prem_visite FROM ".TABLE_VISITE." where products_id='".$pID."'");
                            $row_visites=tep_db_fetch_array($res_visites);
                            echo $row_visites["nb"]." visites depuis le ".tep_date_short($row_visites["prem_visite"]);
                        ?>
                    </td>
                </tr>
            </table>
        </div>
        
        <div style="width: 48.5%; float: left; margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
        	<span style="font-weight: bold;">Commandes par type de clients : </span>
            
            <table style="margin-left: 20px; width: 40%;">
            	<tr>
                	<td style="font-weight: bold;">Particuliers : </td>
                    <td>
					<?php 
						$res_ventes=tep_db_query("SELECT count(1) as nb FROM ".TABLE_ORDERS_PRODUCTS." op, ".TABLE_ORDERS." o, ".TABLE_CUSTOMERS." c where op.products_id='".$pID."' and op.orders_id = o.orders_id and o.customers_id = c.customers_id and c.customers_type = 1");
						$row_ventes=tep_db_fetch_array($res_ventes);
						echo $row_ventes["nb"]." commandes";
                    ?>
                    </td>
                </tr>
                <tr>
                	<td style="font-weight: bold;">Professionnels : </td>
                    <td>
					<?php 
						$res_ventes=tep_db_query("SELECT count(1) as nb FROM ".TABLE_ORDERS_PRODUCTS." op, ".TABLE_ORDERS." o, ".TABLE_CUSTOMERS." c where op.products_id='".$pID."' and op.orders_id = o.orders_id and o.customers_id = c.customers_id and c.customers_type = 2");
						$row_ventes=tep_db_fetch_array($res_ventes);
						echo $row_ventes["nb"]." commandes";
                    ?>
                    </td>
                </tr>
                <tr>
                	<td style="font-weight: bold;">Revendeurs : </td>
                    <td>
					<?php 
						$res_ventes=tep_db_query("SELECT count(1) as nb FROM ".TABLE_ORDERS_PRODUCTS." op, ".TABLE_ORDERS." o, ".TABLE_CUSTOMERS." c where op.products_id='".$pID."' and op.orders_id = o.orders_id and o.customers_id = c.customers_id and c.customers_type = 3");
						$row_ventes=tep_db_fetch_array($res_ventes);
						echo $row_ventes["nb"]." commandes";
                    ?>
                    </td>
				</tr>
                <tr>
                	<td style="font-weight: bold;">Administrations : </td>
                    <td>
					<?php 
						$res_ventes=tep_db_query("SELECT count(1) as nb FROM ".TABLE_ORDERS_PRODUCTS." op, ".TABLE_ORDERS." o, ".TABLE_CUSTOMERS." c where op.products_id='".$pID."' and op.orders_id = o.orders_id and o.customers_id = c.customers_id and c.customers_type = 4");
						$row_ventes=tep_db_fetch_array($res_ventes);
						echo $row_ventes["nb"]." commandes";
                    ?>
                    </td>
				</tr>
            </table>
        </div>
        
        <div style="width: 48.5%; float: right; margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
        	<span style="font-weight: bold;">Articles vendus par type de clients : </span>
            
            <table style="margin-left: 20px; width: 40%;">
            	<tr>
                	<td style="font-weight: bold;">Particuliers : </td>
                    <td>
					<?php 
						$res_ventes=tep_db_query("SELECT SUM(products_quantity) as sum FROM ".TABLE_ORDERS_PRODUCTS." op, ".TABLE_ORDERS." o, ".TABLE_CUSTOMERS." c where op.products_id='".$pID."' and op.orders_id = o.orders_id and o.customers_id = c.customers_id and c.customers_type = 1");
						$row_ventes=tep_db_fetch_array($res_ventes);
						if ($row_ventes["sum"] == '') {
							echo "Aucun article vendu";
						} else if ($row_ventes["sum"] == 1) {
							echo $row_ventes["sum"]." article vendu";
						} else {
							echo $row_ventes["sum"]." articles vendus";
						}
                    ?>
                    </td>
                </tr>
                <tr>
                	<td style="font-weight: bold;">Professionnels : </td>
                    <td>
					<?php 
						$res_ventes=tep_db_query("SELECT SUM(products_quantity) as sum FROM ".TABLE_ORDERS_PRODUCTS." op, ".TABLE_ORDERS." o, ".TABLE_CUSTOMERS." c where op.products_id='".$pID."' and op.orders_id = o.orders_id and o.customers_id = c.customers_id and c.customers_type = 2");
						$row_ventes=tep_db_fetch_array($res_ventes);
						if ($row_ventes["sum"] == '') {
							echo "Aucun article vendu";
						} else if ($row_ventes["sum"] == 1) {
							echo $row_ventes["sum"]." article vendu";
						} else {
							echo $row_ventes["sum"]." articles vendus";
						}
                    ?>
                    </td>
                </tr>
                <tr>
                	<td style="font-weight: bold;">Revendeurs : </td>
                    <td>
					<?php 
						$res_ventes=tep_db_query("SELECT SUM(products_quantity) as sum FROM ".TABLE_ORDERS_PRODUCTS." op, ".TABLE_ORDERS." o, ".TABLE_CUSTOMERS." c where op.products_id='".$pID."' and op.orders_id = o.orders_id and o.customers_id = c.customers_id and c.customers_type = 3");
						$row_ventes=tep_db_fetch_array($res_ventes);
						if ($row_ventes["sum"] == '') {
							echo "Aucun article vendu";
						} else if ($row_ventes["sum"] == 1) {
							echo $row_ventes["sum"]." article vendu";
						} else {
							echo $row_ventes["sum"]." articles vendus";
						}
                    ?>
                    </td>
				</tr>
                <tr>
                	<td style="font-weight: bold;">Administrations : </td>
                    <td>
					<?php 
						$res_ventes=tep_db_query("SELECT SUM(products_quantity) as sum FROM ".TABLE_ORDERS_PRODUCTS." op, ".TABLE_ORDERS." o, ".TABLE_CUSTOMERS." c where op.products_id='".$pID."' and op.orders_id = o.orders_id and o.customers_id = c.customers_id and c.customers_type = 4");
						$row_ventes=tep_db_fetch_array($res_ventes);
						if ($row_ventes["sum"] == '') {
							echo "Aucun article vendu";
						} else if ($row_ventes["sum"] == 1) {
							echo $row_ventes["sum"]." article vendu";
						} else {
							echo $row_ventes["sum"]." articles vendus";
						}
                    ?>
                    </td>
				</tr>
            </table>
        </div>
        <div style="clear:both;"></div>
        <div style="margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
        	<span style="font-weight: bold;">Produit pr�sent dans les commandes suivantes </span>
        	<?php
				$query = 'SELECT o.orders_id, date_purchased FROM '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op
									where o.orders_id=op.orders_id
									and products_id='. $pID .'
									and TO_DAYS(NOW())- TO_DAYS(date_purchased)<=360
									order by date_purchased';
                            $query = tep_db_query($query);
			?>
        	<table border="0" width="50%" cellspacing="0" cellpadding="2">
					<?php
                        
                        while($data = tep_db_fetch_array($query)) {
                            
                            $currentMonth = explode('-', $data['date_purchased']);
                            $currentMonth = $currentMonth[1];
                            
                            if($lastMonth != $currentMonth) {
                                
								if($lastMonth==0) echo '</td></tr>';
								echo '<tr><td width="250" valign="top">';
                                echo 'Commandes du mois de '. getFullMonth($currentMonth) .'</td><td>';
                                $lastMonth = $currentMonth;
                            }
                            echo '<a href="cmd_edit.php?oID='. $data['orders_id'] .'&action=edit">'. $data['orders_id'] .'</a><br>';
                        }
                    ?>
                </td></tr>
            </table>
        </div>
            
        <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
    </body>
</html>

<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>