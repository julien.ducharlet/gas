<?php
  	require('includes/application_top.php');
  	require(DIR_WS_CLASSES . 'currencies.php');
  	$currencies = new currencies();
  	$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
    
   	if (isset($_GET['cPath'])) $cPath=$_GET['cPath'];
   	if ( eregi("(insert|update|setflag)", $action) ) include_once('includes/reset_seo_cache.php');

	if (tep_not_null($action)) {
		switch ($action) {
		case 'setsortorder':
			for ($i=0, $n=sizeof($HTTP_POST_VARS['categories_id']); $i<$n; $i++) {
				tep_set_categorie_sort_order($HTTP_POST_VARS['categories_id'][$i], $HTTP_POST_VARS['sortorder'][$i]);
			}
			$sorting = false;
			tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $HTTP_POST_VARS['cPath']));
			break;
		case 'setflag':
			if ( ($HTTP_GET_VARS['flag'] == '0') || ($HTTP_GET_VARS['flag'] == '1') ) {
				if (isset($HTTP_GET_VARS['cID'])) {
					tep_set_categorie_status($HTTP_GET_VARS['cID'], $HTTP_GET_VARS['flag']);
				}
				if (USE_CACHE == 'true') {
					tep_reset_cache_block('categories');
					tep_reset_cache_block('also_purchased');
				}
			}
			tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $HTTP_GET_VARS['cPath'] . '&cID=' . $HTTP_GET_VARS['cID']));
			break;
		case 'insert_category':
		case 'update_category':
			if (isset($HTTP_POST_VARS['categories_id'])) $categories_id = tep_db_prepare_input($HTTP_POST_VARS['categories_id']);
			$categories_header_url = tep_db_prepare_input($HTTP_POST_VARS['categories_header_url']);
			$sort_order = tep_db_prepare_input($HTTP_POST_VARS['sort_order']);
			
			$categories_status_tous=$HTTP_POST_VARS['categories_status_tous'];
			if($categories_status_tous==""){$categories_status_tous=0;}
			$categories_status_client=$HTTP_POST_VARS['categories_status_client'];
			if($categories_status_client==""){$categories_status_client=0;}
			$categories_status_rev=$HTTP_POST_VARS['categories_status_rev'];
			if($categories_status_rev==""){$categories_status_rev=0;}
			$categories_status_pro=$HTTP_POST_VARS['categories_status_pro'];
			if($categories_status_pro==""){$categories_status_pro=0;}
			$categories_status_adm=$HTTP_POST_VARS['categories_status_adm'];
			if($categories_status_adm==""){$categories_status_adm=0;}
			$categories_status_vip=$HTTP_POST_VARS['categories_status_vip'];
			if($categories_status_vip==""){$categories_status_vip=0;}
			$categories_is_adwords=$HTTP_POST_VARS['categories_is_adwords'];
			if($categories_is_adwords==""){$categories_is_adwords=0;}
			$sql_data_array = array(
										'categories_header_url' => $categories_header_url, 
										'sort_order' => $sort_order, 
										'categories_status_tous' => $categories_status_tous, 
										'categories_status_adm' => $categories_status_adm, 
										'categories_status_rev' => $categories_status_rev, 
										'categories_status_client' => $categories_status_client, 
										'categories_status_pro' => $categories_status_pro, 
										'categories_status_vip' => $categories_status_vip, 
										'categories_is_adwords' => $categories_is_adwords
									);
	
			if ($action == 'insert_category') {
				$insert_sql_data = array('parent_id' => $current_category_id, 'date_added' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
				tep_db_perform(TABLE_CATEGORIES, $sql_data_array);
				$categories_id = tep_db_insert_id();
				$rubrique_id=$HTTP_POST_VARS['rubrique'];
				tep_db_query("insert into ". TABLE_CATEGORIES_RAYON." (categories_id,rubrique_id) values (".$categories_id.",".$rubrique_id.")");
			} elseif ($action == 'update_category') {
				$update_sql_data = array('last_modified' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $update_sql_data);
				tep_db_perform(TABLE_CATEGORIES, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "'");
				$rubrique_id=$HTTP_POST_VARS['rubrique'];
				tep_db_query("update ". TABLE_CATEGORIES_RAYON." set rubrique_id=".$rubrique_id." where categories_id=".$HTTP_POST_VARS['categories_id']);
			}
	
			$languages = tep_get_languages();
			for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
				$categories_name_array = $HTTP_POST_VARS['categories_name'];
				$category_inputs_string_adwords_words_array = $HTTP_POST_VARS['category_inputs_string_adwords_words'];
				$categories_htc_title_array = $HTTP_POST_VARS['categories_htc_title_tag'];
				$categories_htc_desc_array = $HTTP_POST_VARS['categories_htc_desc_tag'];
				$categories_htc_keywords_array = $HTTP_POST_VARS['categories_htc_keywords_tag'];
				$categories_htc_description_array = $HTTP_POST_VARS['categories_htc_description'];
				$balise_title_lien_image_array = $HTTP_POST_VARS['balise_title_lien_image'];
				$balise_title_lien_texte_array = $HTTP_POST_VARS['balise_title_lien_texte'];
				$categories_url_array = $HTTP_POST_VARS['categories_url'];
				$categories_baseline_array = $HTTP_POST_VARS['categories_baseline'];
											
				$language_id = $languages[$i]['id'];
				$sql_data_array = array(
								'categories_name' => tep_db_prepare_input($categories_name_array[$language_id]),
								'categories_adwords_words' => tep_db_prepare_input($category_inputs_string_adwords_words_array[$language_id]), 
								'categories_htc_title_tag' => (tep_not_null($categories_htc_title_array[$language_id]) ? tep_db_prepare_input($categories_htc_title_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_htc_desc_tag' => (tep_not_null($categories_htc_desc_array[$language_id]) ? tep_db_prepare_input($categories_htc_desc_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_htc_keywords_tag' => (tep_not_null($categories_htc_keywords_array[$language_id]) ? tep_db_prepare_input($categories_htc_keywords_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_htc_description' => tep_db_prepare_input($categories_htc_description_array[$language_id]),
								'balise_title_lien_image' => (tep_not_null($balise_title_lien_image_array[$language_id]) ? tep_db_prepare_input($balise_title_lien_image_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'balise_title_lien_texte' => (tep_not_null($balise_title_lien_texte_array[$language_id]) ? tep_db_prepare_input($balise_title_lien_texte_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_url' => (tep_not_null($categories_url_array[$language_id]) ? tep_db_prepare_input($categories_url_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_baseline' => (tep_not_null($categories_baseline_array[$language_id]) ? tep_db_prepare_input($categories_baseline_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])));
				
				if ($action == 'insert_category') {
					$insert_sql_data = array('categories_id' => $categories_id, 'language_id' => $languages[$i]['id']);
					$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
					tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
				} 
				elseif ($action == 'update_category') {
					tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "' and language_id = '" . (int)$languages[$i]['id'] . "'");
				}
			}
			
			//ici on fait la requete de mise � jour de l'image si on up une nouvelle image
			if ($categories_image2 = new upload('categories_image2', DIR_FS_CATEGORIE_IMAGES)) {
				//on met a jour la base de donn�e et on upload l'image si le champ categories_image2 n'est pas vide
				if($categories_image2->filename != "")	  
				  tep_db_query("update " . TABLE_CATEGORIES . " set categories_image2 = '" . tep_db_input($categories_image2->filename) . "' where categories_id = '" . (int)$categories_id . "'");
			}
			
			//ici on fait la requete de mise � jour de l'image si on up une nouvelle image
			if ($categories_header = new upload('categories_header', DIR_FS_CATEGORIE_HEADER_IMAGES)) {
				//on met a jour la base de donn�e et on upload l'image si le champ categories_header n'est pas vide
				if($categories_header->filename != "")	  
				  tep_db_query("update " . TABLE_CATEGORIES . " set categories_header = '" . tep_db_input($categories_header->filename) . "' where categories_id = '" . (int)$categories_id . "'");
			}
	
			if (USE_CACHE == 'true') {
				tep_reset_cache_block('categories');
				tep_reset_cache_block('also_purchased');
			}
			tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $categories_id));
			break;
			
		case 'delete_category_confirm':
			if (isset($HTTP_POST_VARS['categories_id'])) {
				$categories_id = tep_db_prepare_input($HTTP_POST_VARS['categories_id']);
				$categories = tep_get_category_tree($categories_id, '', '0', '', true);
				$products = array();
				$products_delete = array();
				$rubrique_id=$HTTP_POST_VARS['rubrique'];
				tep_db_query("delete from ". TABLE_CATEGORIES_RAYON." where categories_id=".$categories_id);
			  
				for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
					$product_ids_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$categories[$i]['id'] . "'");
					while ($product_ids = tep_db_fetch_array($product_ids_query)) {
					  $products[$product_ids['products_id']]['categories'][] = $categories[$i]['id'];
					}
				}
				reset($products);
			  
				while (list($key, $value) = each($products)) {
					$category_ids = '';
	
					for ($i=0, $n=sizeof($value['categories']); $i<$n; $i++) {
						$category_ids .= "'" . (int)$value['categories'][$i] . "', ";
					}
					$category_ids = substr($category_ids, 0, -2);
					$check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$key . "' and categories_id not in (" . $category_ids . ")");
					$check = tep_db_fetch_array($check_query);
					if ($check['total'] < '1') {
						$products_delete[$key] = $key;
					}
				}
				
				// removing categories can be a lengthy process
				tep_set_time_limit(0);
				for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
					tep_remove_category($categories[$i]['id']);
				}
	
				reset($products_delete);
				while (list($key) = each($products_delete)) {
					tep_remove_product($key);
				}
			}
	
			if (USE_CACHE == 'true') {
			  tep_reset_cache_block('categories');
			  tep_reset_cache_block('also_purchased');
			}
	
			tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath));
			break;
		  
		case 'move_category_confirm':
			if (isset($HTTP_POST_VARS['categories_id']) && ($HTTP_POST_VARS['categories_id'] != $HTTP_POST_VARS['move_to_category_id'])) {
				$categories_id = tep_db_prepare_input($HTTP_POST_VARS['categories_id']);
				$new_parent_id = tep_db_prepare_input($HTTP_POST_VARS['move_to_category_id']);
				$path = explode('_', tep_get_generated_category_path_ids($new_parent_id));
				if (in_array($categories_id, $path)) {
					$messageStack->add_session(ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT, 'error');
					tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $categories_id));
				} 
				else {
					tep_db_query("update " . TABLE_CATEGORIES . " set parent_id = '" . (int)$new_parent_id . "', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
					if (USE_CACHE == 'true') {
						tep_reset_cache_block('categories');
						tep_reset_cache_block('also_purchased');
					}
					tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $new_parent_id . '&cID=' . $categories_id));
				}
			}
			break;
		
		case 'copy_category_confirm':
			if (isset($_REQUEST['categories_id'])) {
				
				//insert categories
				$req_categories="SELECT * FROM ".TABLE_CATEGORIES." WHERE categories_id='".$_REQUEST['categories_id']."'";
				$res_categories=tep_db_query($req_categories);
				$row_categories=tep_db_fetch_array($res_categories);
				$sql_data_array = array(
										'categories_header_url' => $row_categories["categories_header_url"], 
										'sort_order' => $row_categories["sort_order"], 
										'categories_status_rev' => $row_categories["categories_status_rev"], 
										'parent_id' => $row_categories["parent_id"],
										'categories_status_tous' => $row_categories["categories_status_tous"], 'categories_status_adm' => $row_categories["categories_status_adm"],
										'categories_status_client' => $row_categories["categories_status_client"], 'categories_status_pro' => $row_categories["categories_status_pro"], 
										'categories_status_vip' => $row_categories["categories_status_vip"], 'categories_is_adwords' => $row_categories["categories_is_adwords"], 'date_added' => 'now()');
				tep_db_perform(TABLE_CATEGORIES, $sql_data_array);
				$categories_id = tep_db_insert_id();
				
				//insert categories_rubrique
				$req_categories_rubrique="select * from ".TABLE_CATEGORIES_RAYON." where categories_id='".$_REQUEST['categories_id']."'";
				$res_categories_rubrique=tep_db_query($req_categories_rubrique);
				while($row_categories_rubrique=tep_db_fetch_array($res_categories_rubrique)){
					tep_db_query("insert into ". TABLE_CATEGORIES_RAYON." (categories_id,rubrique_id) values (".$categories_id.",".$row_categories_rubrique["rubrique_id"].")");
				}
				
				//insert categories_desc
				$languages = tep_get_languages();
				for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
					$language_id = $languages[$i]['id'];
					$req_categories_desc="select * from ".TABLE_CATEGORIES_DESCRIPTION." where categories_id='".$_REQUEST['categories_id']."' && language_id='".$language_id."'";
					$res_categories_desc=tep_db_query($req_categories_desc);
					$row_categories_desc=tep_db_fetch_array($res_categories_desc);
					$sql_data_array = array('categories_name' => "AAA NEW CATEGORIE",
									'categories_htc_title_tag' => tep_db_prepare_input($row_categories_desc["categories_htc_title_tag"]),
									'categories_htc_desc_tag' => tep_db_prepare_input($row_categories_desc['categories_htc_desc_tag']),
									'categories_htc_keywords_tag' => tep_db_prepare_input($row_categories_desc['categories_htc_keywords_tag']),
									'categories_htc_description' => tep_db_prepare_input($row_categories_desc['categories_htc_description']),
									'balise_title_lien_image' => tep_db_prepare_input($row_categories_desc['balise_title_lien_image']),
									'balise_title_lien_texte' => tep_db_prepare_input($row_categories_desc['balise_title_lien_texte']),
									'categories_baseline' => tep_db_prepare_input($row_categories_desc['categories_baseline']),
									'categories_id' => $categories_id, 'language_id' => $language_id);
					tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
				}
				
				//insert products dans new cat
				$req_categories_prod="select * from ".TABLE_PRODUCTS_TO_CATEGORIES." where categories_id='".$_REQUEST['categories_id']."'";
				$res_categories_prod=tep_db_query($req_categories_prod);
				while($row_categories_prod=tep_db_fetch_array($res_categories_prod)){
					tep_db_query("insert into ". TABLE_PRODUCTS_TO_CATEGORIES." (categories_id,products_id) values (".$categories_id.",".$row_categories_prod["products_id"].")");
				}
			}
			break;
		}
	}
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/tablesorter.js"></script>
<script language="javascript" src="includes/javascript/jQuery/tableSorter/metadata.js"></script>
<script type="text/javascript">

$(document).ready(function() { 
     
    // call the tablesorter plugin
    $(".tablesorter").tablesorter();
});
</script>
</head>

<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<div id="global">
<?php include('includes/article_filtre.php'); ?>
<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td valign="top">
    	<form action="categorie_list.php?action=setsortorder" method="post" name="myForm"
            <input type="hidden" name="cPath" value="<?php echo $cPath; ?>">
            
				<?php
    			$categories_count = 0;
				$rows = 0;
				
				if(isset($_GET['search_cat']) && !empty($_GET['search_cat'])) {
					
					$query_search = ' and categories_name like \'%'. $_GET['search_cat'] .'%\'';
				}
				else $query_search = '';
				
				if(isset($_REQUEST['id_rayon'])) {
					
					$categories_query = "	SELECT 
												c.categories_id, 
												cd.categories_name, 
												c.categories_status, 
												c.categories_image2, 
												c.categories_header, 
												c.parent_id, 
												c.categories_header_url, 
												c.sort_order, 
												c.date_added, 
												c.last_modified, 
												cd.categories_htc_title_tag, 
												cd.categories_htc_desc_tag, 
												cd.categories_htc_keywords_tag, 
												cd.categories_htc_description, 
												cd.balise_title_lien_image, 
												cd.balise_title_lien_texte, 
												cd.categories_url, 
												cd.categories_baseline, 
												cr.rubrique_id, 
												rubrique_name 
											FROM " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd, " . TABLE_CATEGORIES_RAYON . " cr, ". TABLE_RAYON ." r 
											WHERE r.rubrique_id=cr.rubrique_id AND
											cr.categories_id=c.categories_id AND 
											c.parent_id = '" . (int)$current_category_id . "' AND 
											c.categories_id = cd.categories_id AND 
											cd.language_id = '" . (int)$languages_id . "' AND 
											cr.rubrique_id='".$_REQUEST["id_rayon"]."'". $query_search ." 
											ORDER BY c.sort_order, cd.categories_name";

				} elseif (isset($_GET['search_cat'])) {
					
					$categories_query = "	SELECT 
												c.categories_id, 
												c.categories_is_adwords, 
												cd.categories_name, 
												c.categories_status,
												c.categories_image2, 
												c.categories_header, 
												c.parent_id, 
												c.categories_header_url, 
												c.sort_order, 
												c.date_added, 
												c.last_modified,
												cd.categories_htc_title_tag, 
												cd.categories_htc_desc_tag, 
												cd.categories_htc_keywords_tag, 
												cd.categories_htc_description,
												cd.balise_title_lien_image, 
												cd.balise_title_lien_texte, 
												cd.categories_url, 
												cd.categories_baseline, 
												cr.rubrique_id,
												rubrique_name
										
											FROM " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd
											 
											left join ". TABLE_CATEGORIES_RAYON ." cr on cr.categories_id=cd.categories_id
											left join " . TABLE_RAYON . " r on r.rubrique_id=cr.rubrique_id
										
											WHERE c.categories_id = cd.categories_id AND 
											cd.language_id = '" . (int)$languages_id . "'
											". $query_search ."
											ORDER BY r.rubrique_ordre, c.sort_order, cd.categories_name";
				} else {
					
					$categories_query = "	SELECT 
												c.categories_id, 
												c.categories_is_adwords, 
												cd.categories_name, 
												c.categories_status,
												c.categories_image2, 
												c.categories_header, 
												c.parent_id, 
												c.categories_header_url, 
												c.sort_order, 
												c.date_added, 
												c.last_modified,
												cd.categories_htc_title_tag, 
												cd.categories_htc_desc_tag, 
												cd.categories_htc_keywords_tag, 
												cd.categories_htc_description,
												cd.balise_title_lien_image, 
												cd.balise_title_lien_texte, 
												cd.categories_url, 
												cd.categories_baseline, 
												cr.rubrique_id,
												rubrique_name
											FROM " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd
											left join ". TABLE_CATEGORIES_RAYON ." cr on cr.categories_id=cd.categories_id
											left join " . TABLE_RAYON . " r on r.rubrique_id=cr.rubrique_id
										WHERE 
											c.parent_id = '" . (int)$current_category_id . "' AND
											c.categories_id = cd.categories_id AND 
											cd.language_id = '" . (int)$languages_id . "'
											ORDER BY r.rubrique_ordre, c.sort_order, cd.categories_name";
				}
				
				$categories_query = tep_db_query($categories_query);
				
				$i = 0;
    
				while ($categories = tep_db_fetch_array($categories_query)) {
					
					//cas o� la recherche m�ne � une sous-cat�gorie => stocker l'id de la cat�gorie parente pour avoir le path
					if($categories['parent_id']!=$current_category_id) {
						
						$current_category_id = $categories['parent_id'];
					}
					
					if($categories['rubrique_id'] != $lastRubrique) {
						
						if($i++ > 0) echo '</tbody></table>';
						
						echo '<table border="0" width="100%" cellspacing="0" cellpadding="0" class="tablesorter">';
						echo '<thead><tr>';
							echo '<th style="width:20px;" class={sorter:false}">&nbsp;</th>';
							echo '<th>'. $categories['rubrique_name'] .'</th>';
							echo '<th style="width:150px;" class="{sorter:false}">Sous cat / produits</th>';
							echo '<th style="width:150px;" class="{sorter:false}">Adwords</th>';
							echo '<th style="width:150px;" class="{sorter:false}">Actions</th>';
							echo '<th style="width:70px;">Statut</th>';
							echo '<th style="width:100px;">Ordre de tri</th>';
						echo '</tr></thead>';
						
						echo '<tbody>';
						
						$lastRubrique = $categories['rubrique_id'];
					}
					
					$categories_count++;
					$rows++;
					
					// Get parent_id for subcategories if search
					if (isset($HTTP_GET_VARS['search_cat'])) $cPath = $categories['parent_id'];
			
					//retourne un tableau compos� du nombre de sous cat et du nombre de produits de la cat 
					$list_subcategories=tep_direct_subcategory_list($categories['categories_id']);
					$list_id_cat=implode(",", $list_subcategories);
					if(!empty($list_id_cat)){
						$list_id_cat=$categories['categories_id'].",".$list_id_cat;
					}
					else{
						$list_id_cat=$categories['categories_id'];
					}
					$nb_sous_cat=count($list_subcategories);
					$res_nb_product = tep_db_query("select count(1) as nb_product from " .TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id IN (".$list_id_cat.")");
					$row_nb_product = tep_db_fetch_array($res_nb_product);
					?>
					
				<tr class="dataTableRow">
                	<td>
                    <?php 
						
						if($categories['parent_id']!='0') $path = tep_get_path($categories['parent_id'] .'_'. $categories['categories_id']);
						else $path = tep_get_path($categories['categories_id']);
						
						if($nb_sous_cat > 0) {
							
							$lien = tep_href_link(FILENAME_CATEGORIE_LIST, $path);
						}
						else {
							
							$lien = tep_href_link(FILENAME_ARTICLE_LIST, $path);
						}
						echo '<a href="' . $lien . '">' . tep_image(DIR_WS_ICONS . 'folder.gif', ICON_FOLDER) . '</a>'; ?>
					</td>
					<td class="dataTableContent">
						<b><?php echo $categories['categories_name']; ?></b>
                    </td>
					<td class="dataTableContent" align="center"><?php echo $nb_sous_cat." / <font color=\"red\">".$row_nb_product["nb_product"]."</font>";?></td>
          			<td class="dataTableContent" align="center">
					<?php 
						if($categories['categories_is_adwords'] == '1'){
						echo 'ADWORDS';
						}
						
					?>
          			</td>
                    <td class="dataTableContent" align="center">				
					<?php 
						// Editer
						echo '<a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $categories['categories_id'] . '&action=edit_category') . '">' . tep_image(DIR_WS_IMAGES . '/icons/editer.png', "Editer la cat�gorie") . '</a>&nbsp;&nbsp;'; 
						//d�placer
						echo '<a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $categories['categories_id'] . '&action=move_category') . '">' . tep_image(DIR_WS_IMAGES . '/icons/deplacer.png', "D�placer la cat�gorie") . '</a>&nbsp;&nbsp;';
						//copier
						echo '<a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&categories_id=' . $categories['categories_id'] . '&action=copy_category_confirm') . '">' . tep_image(DIR_WS_IMAGES . '/icons/deplacer.png', "Copier la cat�gorie") . '</a>&nbsp;&nbsp;';
						// Effacer
						echo '<a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $categories['categories_id'] . '&action=delete_category') . '" >' .  tep_image(DIR_WS_IMAGES . '/icons/supprimer.png', "Supprimer la cat�gorie")  . '</a>&nbsp;&nbsp;';
						
						// Voir la cat�gorie sur le site client
						$url_clean = WEBSITE . BASE_DIR .'/';
						
						if(isset($_GET['id_rayon']) && !empty($_GET['id_rayon'])) {
							
							$url_clean .= 'modeles_liste.php?rayon='. $_GET['id_rayon'] .'&marque='. $categories['categories_id'];
						}
						elseif(isset($_GET['cPath']) && !empty($_GET['cPath'])) {
							
							$marque = $_GET['cPath'];
							$modele = $categories['categories_id'];
							
							$query_rubrique = 'select rubrique_id from '. TABLE_CATEGORIES_RAYON .' where categories_id='. $marque;
							$query_rubrique = tep_db_query($query_rubrique);
							
							$rubrique = tep_db_fetch_array($query_rubrique);
							
							$url_clean .= 'produits_liste.php?rayon='. $rubrique['rubrique_id'] .'&marque='. $marque .'&modele='. $modele;
						}
						else $url_clean .= 'modeles_liste.php?rayon=1&marque='. $categories['categories_id'];
						
						echo '<a href="'. $url_clean .'" target="_blank">' .  tep_image(DIR_WS_IMAGES . 'icons/visualiser.png', "visualiser la cat�gories sur le site client")  . '</a>';
						?>
					</td>
                    <td class="dataTableContent" align="center">
					<?php
                    	if ($categories['categories_status'] == '1') {
                        	echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'action=setflag&flag=0&cID=' . $categories['categories_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
                        } 
						else {
                        	echo '<a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'action=setflag&flag=1&cID=' . $categories['categories_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                        }?>
                    </td>
                    <td align="right"><?php echo tep_draw_input_field('sortorder[]', $categories['sort_order'],  'SIZE=3') . tep_draw_hidden_field('categories_id[]', $categories['categories_id']);?></td>
				</tr>
			<?php
		}//end while products
		?>
		
        </tbody>
        </table><?php

				$cPath_back = '';
				if (sizeof($cPath_array) > 0) {
					for ($i=0, $n=sizeof($cPath_array)-1; $i<$n; $i++) {
						if (empty($cPath_back)) {
							$cPath_back .= $cPath_array[$i];
						} 
						else {
							$cPath_back .= '_' . $cPath_array[$i];
						}
					}
				}
    			$cPath_back = (tep_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
?>
    <table width="100%">
        <tr>
            <td class="smallText"><?php echo 'Cat�gories : &nbsp;' . $categories_count;?></td>
            <td align="right" class="smallText">
            <?php
            if (sizeof($cPath_array) > 0){ 
            echo '<a href="'.tep_href_link(FILENAME_CATEGORIE_LIST, $cPath_back.'cPath='.$cPath).'">'.tep_image_button('button_back.gif', IMAGE_BACK).'</a>&nbsp;'; 
            echo '<a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&action=beginsort') . '">' . tep_image_button('button_sort_order.gif', 'Editer Ordre de tri') . '</a>&nbsp;';
            }
            if (!isset($HTTP_GET_VARS['search_cat'])){
            echo '<a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&action=new_category') . '">' . tep_image_button('button_new_category.gif', IMAGE_NEW_CATEGORY) . '</a>&nbsp;';
            echo '<a href="' . tep_href_link(FILENAME_ARTICLE_EDIT, 'cPath=' . $cPath . '&action=new_product') . '">' . tep_image_button('button_new_product.gif', IMAGE_NEW_PRODUCT) . '</a>&nbsp;'; 
            }
            if (!isset($HTTP_GET_VARS['search_cat'])) 
            echo tep_image_submit('button_save_sort.gif', IMAGE_BUTTON_SAVE_SORT).'&nbsp;</td>';
            ?>
        </tr>
    </table>
</form></td>
			<!--end sort order//-->
         
<?php 
    $heading = array();
    $contents = array();
    if(isset($cID) && !empty($cID)){
		$categories_selected_query = tep_db_query("SELECT c.categories_id, cd.categories_name, cr.rubrique_id , c.categories_status, c.categories_status_client, c.categories_status_rev, c.categories_status_tous, c.categories_status_adm, c.categories_status_pro, c.categories_status_vip, c.categories_image2, c.categories_header, c.parent_id, c.categories_header_url, c.sort_order, c.date_added, c.last_modified, c.categories_is_adwords, cd.categories_adwords_words, cd.categories_htc_title_tag, cd.categories_htc_desc_tag, cd.categories_htc_keywords_tag, cd.categories_htc_description, cd.balise_title_lien_image, cd.balise_title_lien_texte, cd.categories_url, cd.categories_baseline from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd, " . TABLE_CATEGORIES_RAYON . " cr where c.categories_id = '" . (int)$cID . "' and c.categories_id = cd.categories_id and c.categories_id = cr.categories_id and cd.language_id = '" . (int)$languages_id . "' order by c.sort_order, cd.categories_name");
		$categories_selected_row=tep_db_fetch_array($categories_selected_query);
		$category_childs = array('childs_count' => tep_childs_in_category_count($categories_selected_row['categories_id']));
		$category_products = array('products_count' => tep_products_in_category_count($categories_selected_row['categories_id']));
		$cInfo_array = array_merge($categories_selected_row, $category_childs, $category_products);
		$cInfo = new objectInfo($cInfo_array);			
	}
	
	switch ($action) {
      case 'new_category':
	  	//############# EXTENSION POLYTECH  #############  
		// On r�cup�re les diff�rentes rubriques (Telephonie, Console, etc ....) 					 
		$Rubrique_query = tep_db_query("select * from ".rubrique);
			
		$list_rubriques = array(); 
		while ($tuple = tep_db_fetch_array($Rubrique_query)){
			$list_rubriques[] = array('id' => $tuple['rubrique_id'], 'text' => $tuple['rubrique_name']);
		}
		//############# fin extension polytech ###############

        $heading[] = array('text' => '<b>Nouvelle cat�gorie</b>');
		$contents = array('form' => tep_draw_form('newcategory', FILENAME_CATEGORIE_LIST, 'action=insert_category&cPath=' . $cPath, 'post', 'enctype="multipart/form-data"'));
        $contents[] = array('text' => 'Merci de compl�ter les informations suivantes pour la nouvelle cat�gorie');

        $category_inputs_string = '';
        $languages = tep_get_languages();
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
		
          $category_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . 
		  '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
		  tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']','',' size=\'60\'');
		  
		  //$category_inputs_string_adwords_words .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . 
		  //'/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
		  //tep_draw_input_field('category_inputs_string_adwords_words[' . $languages[$i]['id'] . ']','',' size=\'60\'');
		  
		  // liste d�roulante pour choisir une rubrique
		  $category_inputs_string_rubrique .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . 
		  '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
		  tep_draw_pull_down_menu('rubrique', array_merge(array(),$list_rubriques)); 
				
		  // HTC BOC
          $category_htc_title_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 		  
		  tep_draw_input_field('categories_htc_title_tag[' . $languages[$i]['id'] . ']', '',' size=\'60\'');
		  
		  $category_htc_desc_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 
		  tep_draw_input_field('categories_htc_desc_tag[' . $languages[$i]['id'] . ']', '',' size=\'60\'' );
		  
          $category_htc_keywords_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 
		  tep_draw_input_field('categories_htc_keywords_tag[' . $languages[$i]['id'] . ']', '',' size=\'60\'' );
		  // HTC EOC
				
			// AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY				
			$balise_title_lien_image_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 		 		  	
			tep_draw_input_field('balise_title_lien_image[' . $languages[$i]['id'] . ']', '',' size=\'60\'' );
				
			$balise_title_lien_texte_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 		 		  	
			tep_draw_input_field('balise_title_lien_texte[' . $languages[$i]['id'] . ']', '',' size=\'60\'' );				
			// FIN AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY
			
			// AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY
			$category_url_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;'.tep_draw_input_field('categories_url[' . $languages[$i]['id'] . ']', '' );
			$category_baseline_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']).'&nbsp;'.tep_draw_textarea_field('categories_baseline[' . $languages[$i]['id'] . ']', 'hard', 55, 5, '');
			// FIN AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY
        }

        $contents[] = array('text' => '<br />Nom de la cat�gorie :' . $category_inputs_string);
		$contents[] = array('text' => '<br />Rayon de la cat�gorie :'.  $category_inputs_string_rubrique );
		
		$contents[] = array('text' => '<br />Visible seulement des :');
		$contents[] = array('text' => '<br />
										Non logu� :'.  tep_draw_checkbox_field('categories_status_tous','1',true).'&nbsp;&nbsp;
										Client :'.  tep_draw_checkbox_field('categories_status_client','1',true).'&nbsp;&nbsp;
										Revendeur :'.  tep_draw_checkbox_field('categories_status_rev','1',true).'&nbsp;&nbsp;
										Pro :' . tep_draw_checkbox_field('categories_status_pro','1',true) . '&nbsp;&nbsp;
										Administration :' . tep_draw_checkbox_field('categories_status_adm','1',true) . '&nbsp;&nbsp;
										VIP :'.  tep_draw_checkbox_field('categories_status_vip','1',true));
		
		//$contents[] = array('text' => '<br />Utilis�e dans adword :'. tep_draw_checkbox_field('categories_is_adwords','1',true));
		//$contents[] = array('text' => '<br />Mots cl�s adwords :' . $category_inputs_string_mots_cles);
		
		$contents[] = array('text' => '<br />Image de la Cat�gorie :<br />' . tep_draw_file_field('categories_image2'));
        $contents[] = array('text' => '<br />Header de la Cat�gorie :<br />' . tep_draw_file_field('categories_header'));
		
		// Url header
		$contents[] = array('text' => '<br />URL Header de la Cat�gorie :<br />' . tep_draw_input_field('categories_header_url', '', 'size="60"'));
		
        $contents[] = array('text' => '<br />Ordre de Tri : ' . tep_draw_input_field('sort_order', '', 'size="2"'));
		$contents[] = array('text' => '<br /> META TAG TITRE' . $category_htc_title_string);
        $contents[] = array('text' => '<br /> META TAG DESCRIPTION' . $category_htc_desc_string);
        $contents[] = array('text' => '<br /> META TAG MOTS CLES' . $category_htc_keywords_string);
		$contents[] = array('text' => '<br /> Balise TITLE au survole de cette cat�gorie pour les images' . $balise_title_lien_image_inputs_string);
		$contents[] = array('text' => '<br /> Balise TITLE au survole de cette cat�gorie pour le texte' . $balise_title_lien_texte_inputs_string);
		$contents[] = array('text' => '<br /> URL de cette cat�gorie pour le r�f�rencement' . $category_url_string); 
		$contents[] = array('text' => '<br /> BASELINE de cette cat�gorie pour le r�f�rencement' . $category_baseline_string);
		$contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_save.gif', 'Sauvegarder') . ' <a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath) . '">' . tep_image_button('button_cancel.gif', 'Annuler') . '</a>');
        break;
		
      case 'edit_category':
	  
	  	//############# EXTENSION POLYTECH  #############  
		// On r�cup�re les diff�rentes rubriques (Telephonie, Console, etc ....) 					 
		$Rubrique_query = tep_db_query("select * from rubrique");
		
		$list_rubriques = array(); 
		while ($tuple = tep_db_fetch_array($Rubrique_query)){
			$list_rubriques[] = array('id' => $tuple['rubrique_id'],
                              'text' => $tuple['rubrique_name']);
		}
		//############# fin extension polytech ###############
	    $heading[] = array('text' => '<b>Editer cat�gorie</b>');
        $contents = array('form' => tep_draw_form('categories', FILENAME_CATEGORIE_LIST, 'action=update_category&cPath=' . $cPath, 'post', 'enctype="multipart/form-data"') . tep_draw_hidden_field('categories_id', $cID));
        $contents[] = array('text' => 'Merci de faire les changements n�cessaires');

        $category_inputs_string = '';
        $languages = tep_get_languages();
		
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          	$category_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] .
		  	'/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
		  	tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']', tep_get_category_name($cID, $languages[$i]['id']),' size=\'60\'');
		  
			//$category_inputs_string_adwords_words .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] .
		  	//'/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
		  	//tep_draw_input_field('category_inputs_string_adwords_words[' . $languages[$i]['id'] . ']', $cInfo->categories_adwords_words ,' size=\'60\'');
		  
			//############# EXTENSION POLYTECH  #############  
			// liste d�roulante pour choisir une rubrique
			$category_inputs_string_rubrique .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . 
			'/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
			tep_draw_pull_down_menu('rubrique', array_merge(array(),$list_rubriques), $cInfo->rubrique_id); 
			//############# fin extension polytech ###############

          	// HTC BOC
          	$category_htc_title_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_htc_title_tag[' . $languages[$i]['id'] . ']', tep_get_category_htc_title($cID, $languages[$i]['id']),' size=\'60\'');
          	$category_htc_desc_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_htc_desc_tag[' . $languages[$i]['id'] . ']', tep_get_category_htc_desc($cID, $languages[$i]['id']),' size=\'60\'');
          	$category_htc_keywords_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_htc_keywords_tag[' . $languages[$i]['id'] . ']', tep_get_category_htc_keywords($cID, $languages[$i]['id']),' size=\'60\'');
          	// HTC EOC					
					
			// AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY
			$balise_title_lien_image_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' 
			. $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' 
			. tep_draw_input_field('balise_title_lien_image[' . $languages[$i]['id'] . ']', tep_get_balise_title_lien_image($cID, $languages[$i]['id']),' size=\'60\'');
			
			$balise_title_lien_texte_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' 
			. $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' 
			. tep_draw_input_field('balise_title_lien_texte[' . $languages[$i]['id'] . ']', tep_get_balise_title_lien_texte($cID, $languages[$i]['id']),' size=\'60\'');
			// FIN AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY

			// AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY					
			$category_url_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' 
			. $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' 
			. tep_draw_input_field('categories_url[' . $languages[$i]['id'] . ']', tep_get_category_url($cID, $languages[$i]['id']),' size=\'60\'');
			
			$category_baseline_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES_GAS . $languages[$i]['directory'] . '/images/' 
			. $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' 
			. tep_draw_textarea_field('categories_baseline[' . $languages[$i]['id'] . ']', 'hard', 55, 5, tep_get_category_baseline($cID, $languages[$i]['id']));
			// FIN AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY
					
        }

        $contents[] = array('text' => '<br />Nom de la cat�gorie : ' . $category_inputs_string);
		$contents[] = array('text' => '<br /> Rayon de la cat�gorie :'.  $category_inputs_string_rubrique);
		
		$contents[] = array('text' => '<br />Visible seulement des :');
		$contents[] = array('text' => '<br />
											Non logu� :'.  tep_draw_checkbox_field('categories_status_tous','1',$cInfo->categories_status_tous==1).'&nbsp;&nbsp;
											Client :'.  tep_draw_checkbox_field('categories_status_client','1',$cInfo->categories_status_client==1).'&nbsp;&nbsp;
											Revendeur :'.  tep_draw_checkbox_field('categories_status_rev','1',$cInfo->categories_status_rev==1).'&nbsp;&nbsp;
											Pro :' . tep_draw_checkbox_field('categories_status_pro','1',$cInfo->categories_status_pro==1) . '&nbsp;&nbsp;
											Administration :' . tep_draw_checkbox_field('categories_status_adm','1',$cInfo->categories_status_adm==1) . '&nbsp;&nbsp;
											VIP :'.  tep_draw_checkbox_field('categories_status_vip','1',$cInfo->categories_status_vip==1));
		
		//$contents[] = array('text' => '<br />Utilis�e dans adwords :'.  tep_draw_checkbox_field('categories_is_adwords','1',$cInfo->categories_is_adwords==1));
		//$contents[] = array('text' => '<br />Mots cl�s adwords :' . $category_inputs_string_adwords_words);
		
        $contents[] = array('text' => '<br />' . tep_image(DIR_WS_CATEGORIE_IMAGES . $cInfo->categories_image2, $cInfo->categories_name) . '<br />' . DIR_WS_CATEGORIE_IMAGES . '<br /><b>' . $cInfo->categories_image2 . '</b>');
        $contents[] = array('text' => '<br />Image de la cat�gorie : <br />' . tep_draw_file_field('categories_image2'));
        $contents[] = array('text' => '<br />' . tep_image(DIR_WS_CATEGORIE_HEADER_IMAGES . $cInfo->categories_header, $cInfo->categories_name,300) . '<br />' . DIR_WS_CATEGORIE_HEADER_IMAGES . '<br /><b>' . $cInfo->categories_header . '</b>');
        $contents[] = array('text' => '<br />Header de la cat�gorie : <br />' . tep_draw_file_field('categories_header'));
		
		// Url header
		$contents[] = array('text' => '<br />URL Header de la Cat�gorie (255 caract�res max) :<br />' . tep_draw_input_field('categories_header_url', $cInfo->categories_header_url, 'size="60"'));
		
        $contents[] = array('text' => '<br />Ordre de tri : ' . tep_draw_input_field('sort_order', $cInfo->sort_order, 'size="2"'));	
		$contents[] = array('text' => '<br />' . 'META TAG TITRE' . $category_htc_title_string);
        $contents[] = array('text' => '<br />' . 'META TAG DESCRIPTION' . $category_htc_desc_string);
        $contents[] = array('text' => '<br />' . 'META TAG MOTS CLES' . $category_htc_keywords_string);
        $contents[] = array('text' => '<br />' . 'TEXTE en haut de la Cat�gories' . $category_htc_description_string);
        $contents[] = array('text' => '<br />' . 'balise TITLE au survole de cette cat�gorie pour les images' . $balise_title_lien_image_inputs_string); 
		$contents[] = array('text' => '<br />' . 'balise TITLE au survole de cette cat�gorie pour le texte' . $balise_title_lien_texte_inputs_string);
		$contents[] = array('text' => '<br />' . 'URL de cette cat�gorie pour le r�f�rencement' . $category_url_string); 
		$contents[] = array('text' => '<br />' . 'BASELINE de cette cat�gorie pour le r�f�rencement' . $category_baseline_string);
		$contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $cID) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
		
      case 'delete_category':
        $heading[] = array('text' => '<b>Supprimer cat�gorie</b>');
        $contents = array('form' => tep_draw_form('categories', FILENAME_CATEGORIE_LIST, 'action=delete_category_confirm&cPath=' . $cPath) . tep_draw_hidden_field('categories_id', $cID));
        $contents[] = array('text' => 'Etes vous sur de vouloir supprimer cette cat�gorie ?');
        $contents[] = array('text' => '<br /><b>' . $cInfo->categories_name . '</b>');
        if ($cInfo->childs_count > 0) $contents[] = array('text' => '<br />' . sprintf('<b>ATTENTION :</b> Il y a %s (sous-)cat�gories li�es � cette cat�gorie !', $cInfo->childs_count));
        if ($cInfo->products_count > 0) $contents[] = array('text' => '<br />' . sprintf('<b>ATTENTION :</b> Il y a %s articles li�es � cette cat�gorie !', $cInfo->products_count));
        $contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $cID) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
		
      case 'move_category':
        $heading[] = array('text' => '<b>D�placer cat�gorie</b>');
        $contents = array('form' => tep_draw_form('categories', FILENAME_CATEGORIE_LIST, 'action=move_category_confirm&cPath=' . $cPath) . tep_draw_hidden_field('categories_id', $cID));
        $contents[] = array('text' => sprintf('Merci de s�lectionner la cat�gorie ou vous voudriez que <b>%s</b> soit plac�', $cInfo->categories_name));
        $contents[] = array('text' => '<br />' . sprintf('D�placer <b>%s</b> vers :', $cInfo->categories_name) . '<br />' . tep_draw_pull_down_menu('move_to_category_id', tep_get_full_category_tree(), $current_category_id));
        $contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_move.gif', IMAGE_MOVE) . ' <a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $cID) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      	echo '            <td width="25%" valign="top">' . "\n";
		$box = new box;
      	echo $box->infoBox($heading, $contents);
	    echo '            </td>' . "\n";
    }
	?>
      </tr>
    </table>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</div>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>