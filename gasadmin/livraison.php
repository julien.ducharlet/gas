<?php
/* PAGE: livraison.php
  Version du 27/06/2007
*/
  require('includes/application_top.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Livraison Fournisseur</td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table>
		</td>
       </tr>
	   <tr>
	  	  <td align="left" class="main">	
		  	Cette page permet de g�rer l'arrivage d'une commande d'un fournisseur : <br /><br />
			- Gestion des quantit�s d�j� en stock et celle � ajouter.<br />
			- Gestion de la r�f�rence du founisseur.<br />
			- Gestion du prix d'achat du fournisseur pour la mise a jour des donn�es.<br />
			- Remise en ligne automatique des articles hors ligne quand on entre du stock<br /><br />
		  </td>
       </tr>
   	   <tr>
	  	  <td align="left">	
<?php
	//requete pour faire une liste des types de produit
	$re_types='SELECT family_id as id, family_name as nom FROM family ORDER BY family_name ';
	$rep_types=tep_db_query($re_types); $i=1;
	
	//on stocke les resultats de la requete dans un tableau ($types)
	/*while($fe_types=mysql_fetch_object($rep_types)){
		$types[$i]['id']=$fe_types->id;
		$types[$i]['nom']=$fe_types->nom;
		$i++;
	}*/
			
	if(isset($_POST['ajouter']) && is_numeric($_POST['supli'])){
		$supplier=$_POST['supli']; //on note l'id du fournisseur principal
		$preview_clients_a_prevenir=array();
		$produits_rajoutes=array();
			
		for($i=1;$i<=$_POST['nombre'];$i++){
			//on regarde si l'article poss�de des options
			$possede_options = $_POST['options_'.$i];
			$status_a_change = false;
			
			//si les champs suivants sont remplis :
			if(!empty($_POST['model'.$i]) && (!empty($_POST['qte'.$i]) || $possede_options)){
				//on sauvegarde l'id du produit pour le reutiliser plus tard
				array_push($produits_rajoutes,$_POST['model'.$i]);
				
				//on initialise la quantit� ajout�e
				$qte_rajoutee_totale = 0;
				
				//var_dump($possede_options);
				
				//Si oui on r�cup�re les id des options
				if ($possede_options == "1") {
					$products_options_query = tep_db_query("SELECT pa.options_id, pa.options_values_id, pa.products_id, pa.options_dispo, pa.options_quantity, pa.options_quantity_reel, pov.products_options_values_name 
															FROM " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov 
															WHERE products_id = '" . $_POST['model'.$i] . "' AND pa.options_values_id = pov.products_options_values_id
															ORDER BY pa.options_id ASC, products_options_sort_order ASC");
					while ($products_options = tep_db_fetch_array($products_options_query)) {
						$qte_option = $_POST['qte_' . $i . "_" . $products_options['options_values_id']];
						
						if (!empty($qte_option)) {
							$qte_rajoutee_totale += $qte_option;
							
							if ($qte_option+$products_options['options_quantity'] <= 0 && $products_options['options_dispo'] == 0) {
								$status = 0;
							} else {
								$status = 1;
								$status_article = 1;
							}
							
							if ($products_options['options_dispo'] == 0 && $status == 1) {
								
								$status_a_change = true;
							}
							
							$re_update="UPDATE " . TABLE_PRODUCTS_ATTRIBUTES . " SET options_quantity='" . ($qte_option+$products_options['options_quantity']) . "', options_quantity_reel='" . ($qte_option+$products_options['options_quantity_reel']) . "', options_dispo='" . $status . "' WHERE products_id='" . $_POST['model'.$i] . "' AND options_values_id = '" . $products_options['options_values_id'] . "'";
							tep_db_query($re_update);
						}
					}
				} else {
					$qte_rajoutee_totale = $_POST['qte'.$i];
				}
						
				//on formate le prix en flottant pour mysql
				if(!empty($_POST['prix'.$i])) { $prix=str_replace(',','.',$_POST['prix'.$i]); }
				if(!empty($_POST['ref'.$i])) { $ref=$_POST['ref'.$i]; }
												
				//premiere requete pour voir si le produit est bien present chez le fournisseur
				$prod2supli='SELECT * FROM products_to_suppliers WHERE suppliers_id="'.$supplier.'" AND products_id="'.$_POST['model'.$i].'" ';
				$prod2supli_rep=tep_db_query($prod2supli);
					
				//on met a jour le stock qui est maintenant de : n = n + recus
				$quantity_now="SELECT products_quantity, products_quantity_reel, products_status FROM products where products_id=".$_POST['model'.$i];
				$resultat_quantity_now=tep_db_query($quantity_now);
				$Stock_now=tep_db_fetch_array($resultat_quantity_now);
				
				/*echo $qte_rajoutee_totale+$Stock_now['products_quantity'];
				
				var_dump($status_a_change);*/
				
				if ($possede_options != "1") {
					if (($qte_rajoutee_totale+$Stock_now['products_quantity']) <= 0 && $Stock_now['products_status'] == 0) {
						$status_article = 0;
					} else {
						$status_article = 1;
					}
				} else {
					if (!$status_a_change) {
						$status_article = $Stock_now['products_status'];
					}
				}
				//products_cost="'.$prix.'", 
				$re_update = 'UPDATE '. TABLE_PRODUCTS .' SET products_quantity="'.($qte_rajoutee_totale+$Stock_now['products_quantity']).'", 
				products_quantity_reel="'.($qte_rajoutee_totale+$Stock_now['products_quantity_reel']).'", 
				products_status="' . $status_article . '", 
				products_date_available = NULL 
				WHERE products_id="'.$_POST['model'.$i].'" ';
				tep_db_query($re_update);
							
				//si le produit existe chez le fournisseur
				if(tep_db_num_rows($prod2supli_rep)==1){
					if(isset($prix) && !isset($ref)){
						$re_update='UPDATE products_to_suppliers SET prix_achat="'.$prix.'" WHERE products_id="'.$_POST['model'.$i].'" AND suppliers_id="'.$supplier.'" ';
					}
					else if(isset($ref) && !isset($prix)){
						$re_update='UPDATE products_to_suppliers SET ref_supplier="'.$ref.'" WHERE products_id="'.$_POST['model'.$i].'" AND suppliers_id="'.$supplier.'" ';
					}
					else if(isset($prix) && isset($ref)){
						$re_update='UPDATE products_to_suppliers SET prix_achat="'.$prix.'", ref_supplier="'.$_POST['ref'.$i].'" WHERE products_id="'.$_POST['model'.$i].'" AND suppliers_id="'.$supplier.'" ';
					}
					if(isset($prix) || isset($ref)){
						tep_db_query($re_update);
					}
				}	
				//sinon le produit n'existe pas	dans la table products_to_suppliers				
				else{
					//requete pour ajouter le produit chez le fournisseur
					if(isset($prix) && isset($ref)){
						$re_insert='INSERT INTO products_to_suppliers (products_id,suppliers_id,prix_achat,ref_supplier) VALUES ("'.$_POST['model'.$i].'","'.$_POST['supli'].'","'.$prix.'","'.$ref.'") ';
						tep_db_query($re_insert);
					}
				}
								
				$nom_produit='SELECT pd.products_name as nom, p.products_quantity as qte FROM products_description pd, products p '
							.'WHERE p.products_id="'.$_POST['model'.$i].'" AND p.products_id=pd.products_id ';
				$rep_nom_prod=tep_db_query($nom_produit);
				$arr_nom_prod=tep_db_fetch_array($rep_nom_prod);
				echo 'Le <b>'.$arr_nom_prod['nom'].'</b> a &eacute;t&eacute; mis &agrave; jour (<span style="color:#2ABF00; font-weight:bold;">'.$arr_nom_prod['qte'].'</span>)<br />';
			}
		}		
		echo '<br /><button onclick="window.location.replace(\'livraison.php\');">Ok !</button>';
	}?>

<form method="post">
<?php
  	//si le compteur existe et qu'il contient bien un nombre
  	if(isset($_POST['nombre'])&&is_numeric($_POST['nombre'])){
		//et que le dernier champ de la derniere ligne est sp�cifi�
		if(isset($_POST['model'.$_POST['nombre']]) && is_numeric($_POST['model'.$_POST['nombre']])){
			//alors le nouveau compteur recois i=i+1
			echo '<input type="hidden" name="nombre" value="'.($_POST['nombre']+1).'" />';
			$nombre=$_POST['nombre']+1;
			$nouveau=true;
		}
		else{
			//sinon il reste ou il en est
			echo '<input type="hidden" name="nombre" value="'.$_POST['nombre'].'" />';
			$nombre=$_POST['nombre'];
			$nouveau=false;
		}
	}
  //mais si le compteur n'existe pas, c'est qu'on vient d'arriver sur la page
  	else{
		//alors il recoit comme valeur de depart i=1
		echo '<input type="hidden" name="nombre" value="1" />';
		$nombre=1;
	}
?>
<table width="100%" border="1" cellspacing="2" cellpadding="3" align="center">
<?php
	echo '<tr><td colspan="8" align="letf">&nbsp;&nbsp;&nbsp;';
	echo '<select name="supli" style="width:200px;" onChange="this.form.submit();">';
	echo '<option value="null">Selectionnez le fournisseur</option>';
				
	$re_supli='SELECT suppliers_name as nom, suppliers_id as id FROM suppliers ORDER BY nom ';
	$rep_supli=tep_db_query($re_supli);
	while($fe_supli=mysql_fetch_object($rep_supli)){
		if($fe_supli->id==$_POST['supli']) { echo '<option value="'.$fe_supli->id.'" selected>'.$fe_supli->nom.'</option>'; }
		else { echo '<option value="'.$fe_supli->id.'">'.$fe_supli->nom.'</option>'; }
	}
	echo '</select>';
	echo '</td></tr>'."\n";
	echo '<tr class="formareatitle" align="center">';
	echo ' <td align="center">N&deg;</td><td>Article</td><td>Prix Achat</td><td>Prix Vente</td><td>Statut</td><td>Ref</td><td>Qt&eacute;</td><td>Re&ccedil;u</td>';
	echo '</tr>'."\n";
	if(isset($_POST['supli']) && $_POST['supli']!='null') {
		
		for($i=1;$i<=$nombre;$i++){
			
			$possede_options = false;
			
			if (isset($_POST['model'.$i]) && $_POST['model'.$i] != '') {
				
				$products_options_query = tep_db_query("SELECT pa.options_id, pa.options_values_id, pa.products_id, pa.options_dispo, pa.options_quantity, pa.options_quantity_reel, pov.products_options_values_name 
														FROM " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov 
														WHERE products_id = '" . $_POST['model'.$i] . "' AND pa.options_values_id = pov.products_options_values_id
														ORDER BY pa.options_id ASC, products_options_sort_order ASC");
				if (tep_db_num_rows($products_options_query) > 0) {
					$possede_options = true;
				} else {
					$possede_options = false;
				}
			}
	
			echo '<tr class="main" align="center">';
			echo '  <td>';
			echo $i . '<input type="hidden" name="options_' . $i . '" value="' . $possede_options . '" />';
			echo '  </td>'."\n";
			/*echo '  <td>';
			echo '    <select name="type'.$i.'" style="width:200px;" onChange="this.form.submit();">';
			echo '      <option value="null">Selectionnez le type article</option>';
				
			for($j=1;$j<=count($types);$j++){
				$selected="";
				if($nouveau && $nombre==$i && isset($_POST['type'.$num]) && ($_POST['type'.$num]==$types[$j]['id'])){
					$_POST['type'.$i]=$_POST['type'.$num];
				}
				if(isset($_POST['type'.$i]) && ($_POST['type'.$i]==$types[$j]['id'])){
					$selected="selected";
				}
				$num=$i-1;
				echo '<option value="'.$types[$j]['id'].'" '.$selected.'>'.$types[$j]['nom'].'</option>'; 
			}
				
			echo '    </select>';
			echo '   </td>'."\n";*/
			echo '   <td>&nbsp;';
			if((isset($_POST['supli']) && is_numeric($_POST['supli']))) {
				
				echo '<select name="model'.$i.'" style="width:250px;" onChange="this.form.submit();">';
				echo '  <option value="null">Selectionnez la r�f�rence article</option>';
													
				$re_model='SELECT distinct(p.products_id) as id, products_model as model, products_quantity_reel as qte, products_name as name
				FROM products p, products_description pd, products_to_suppliers pts
				WHERE p.products_id=pd.products_id and pd.products_id=pts.products_id and suppliers_id="'.$_POST['supli'].'" ORDER BY model';
				$rep_model = tep_db_query($re_model);
				
				while($fe_model=mysql_fetch_object($rep_model)) {
					
					if($fe_model->id==$_POST['model'.$i]) {
					
						echo '<option value="'.$fe_model->id.'" selected>'.$fe_model->model .' - '. $fe_model->name .'</option>'; 
						$qte=$fe_model->qte; 
					}
					else { 
						echo '<option value="'.$fe_model->id.'">(x'.$fe_model->qte.') '.$fe_model->model .' - '. $fe_model->name .'</option>'; 
					}
				}
				echo '</select>';
			}
			echo '<input type="hidden" name="old_model'.$i.'" value="'.$_POST['model'.$i].'"/>';
			echo '  </td>'."\n";
			
			//bloc de preremplissage du prix et de la ref fournisseur
			$su_prix='';
			$su_ref='';
			if(isset($_POST['model'.$i]) && is_numeric($_POST['model'.$i])){
				$re_infos='SELECT prix_achat as prix, ref_supplier as ref FROM products_to_suppliers WHERE products_id="'.$_POST['model'.$i].'" AND suppliers_id="'.$_POST['supli'].'" ';
				$rep_infos=tep_db_query($re_infos);
				$fet_infos=mysql_fetch_object($rep_infos);
						
				if(tep_db_num_rows($rep_infos)==0) { $su_prix=''; }
				else { $su_prix=$fet_infos->prix; }
				if(tep_db_num_rows($rep_infos)==0) { $su_ref=''; }
				else { $su_ref=$fet_infos->ref; }
			}
			
			echo '  <td align="center">';
			if(isset($_POST['model'.$i]) && isset($_POST['old_model'.$i]) && $_POST['model'.$i]==$_POST['old_model'.$i]){
				$su_prix=$_POST['prix'.$i];
			}
			echo '    <input type="textfield" name="prix'.$i.'" size="8" value="'.$su_prix.'" />'; 
			echo '	</td>'."\n";
			echo '  <td align="center">';
			$query_info_produit='SELECT products_price, products_status FROM products WHERE products_id="'.$_POST['model'.$i].'"';
			$res_info_produit=tep_db_query($query_info_produit);
			$row_info_produit=tep_db_fetch_array($res_info_produit);
			echo $row_info_produit["products_price"];
			echo '	&nbsp;</td>'."\n";
			echo '  <td align="center">';
			if ($row_info_produit['products_status'] == '1') {
				echo tep_image(DIR_WS_IMAGES.'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10);
			} 
			else {
				echo tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10);
			}
			echo '	</td>'."\n";
			echo '  <td align="center">';
			if(isset($_POST['model'.$i]) && isset($_POST['old_model'.$i]) && $_POST['model'.$i]==$_POST['old_model'.$i]){
				$su_ref=$_POST['ref'.$i];
			}
			echo '    <input type="textfield" name="ref'.$i.'" size="20" value="'.$su_ref.'" />'; $su_ref='';
			echo '	</td>'."\n";
			echo '	<td align="right">&nbsp;';
			if(isset($qte)){ 
				echo '    (x'.$qte.')<input type="hidden" name="stock'.$i.'" value="'.$qte.'"/>'; 
				unset($qte); 
			}
			echo '  </td>'."\n";
			echo '	<td>';
			
			if ($possede_options) {
				$disabled = 'disabled';
			} else {
				$disabled = '';
			}
			
			if(isset($_POST['qte'.$i])){ 
				echo '    <input type="textfield" name="qte'.$i.'" maxlength="5" size="5" value="'.$_POST['qte'.$i].'" ' . $disabled . '/>'; 
			}
			else{ 
				echo '    <input type="textfield" name="qte'.$i.'" maxlength="5" size="5" ' . $disabled . '/>'; 
			}
			echo '  </td>'."\n";
			echo '</tr>'."\n";
			
			if ($possede_options) {
				while ($products_options = tep_db_fetch_array($products_options_query)) {
					?>
					<tr class="main" align="center">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<?php
								if ($products_options['options_dispo'] == '1') {
									echo tep_image(DIR_WS_IMAGES.'icon_status_green.gif', '', 10, 10);
								} else {
									echo tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', '', 10, 10);
								}
							?>
						</td>
						<td><?php echo $products_options['products_options_values_name']; ?></td>
						<td align="right"><?php echo $products_options['options_quantity_reel'] . "&nbsp;(" . $products_options['options_quantity'] . ")"; ?><input type="hidden" name="stock<?php echo $i . "_" . $products_options['options_values_id']; ?>" value="<?php echo $products_options['options_quantity_reel']; ?>"/></td>
						<td><input type="textfield" name="qte_<?php echo $i . "_" . $products_options['options_values_id']; ?>" maxlength="5" size="5" /></td>
					</tr>
					<?php
				}
			}
		}//fin for affichage ligne
	}//fin if on a bien un fournisseur
?>
</table>
<br />
<div align="center"><input type="submit" name="ajouter" value="Mettre � jour le Stock" /></div>
</form>
	  	  </td>
	   	</tr>  
	  </table>
	 </td>
	</tr>
      </table></td>
  </tr>
</table>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>