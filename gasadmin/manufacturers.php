<?php
/* Page qui liste les marques vendues sur le site */

require('includes/application_top.php');

$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

if (tep_not_null($action)) {
	switch ($action) {
		case 'insert':
		case 'save':
			if (isset($HTTP_GET_VARS['mID'])) $manufacturers_id = tep_db_prepare_input($HTTP_GET_VARS['mID']);
			
			$manufacturers_name = tep_db_prepare_input($HTTP_POST_VARS['manufacturers_name']);
			$manufacturers_url = tep_db_prepare_input($HTTP_POST_VARS['manufacturers_url']);
			$manufacturers_meta_title_array = $HTTP_POST_VARS['manufacturers_meta_title'];
			$manufacturers_meta_description_array = $HTTP_POST_VARS['manufacturers_meta_description'];
			$manufacturers_meta_keywords_array = $HTTP_POST_VARS['manufacturers_meta_keywords'];
			$manufacturers_description_array = $HTTP_POST_VARS['manufacturers_description'];
			$manufacturers_baseline_array = $HTTP_POST_VARS['manufacturers_baseline'];
			
			$sql_data_array = array(	'manufacturers_name' => $manufacturers_name, 
										'manufacturers_url' => $manufacturers_url,
										'manufacturers_meta_title' => (tep_not_null($manufacturers_meta_title_array) ? tep_db_prepare_input($manufacturers_meta_title_array) : $manufacturers_name),
										'manufacturers_meta_description' => (tep_not_null($manufacturers_meta_description_array) ? tep_db_prepare_input($manufacturers_meta_description_array) : $manufacturers_name),
										'manufacturers_meta_keywords' => (tep_not_null($manufacturers_meta_keywords_array) ? tep_db_prepare_input($manufacturers_meta_keywords_array) : $manufacturers_name),
										'manufacturers_description' => tep_db_prepare_input($manufacturers_description_array),
										'manufacturers_baseline' => tep_db_prepare_input($manufacturers_baseline_array)
									);
			
			if ($action == 'insert') {
				$insert_sql_data = array('date_added' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
				tep_db_perform(TABLE_MANUFACTURERS, $sql_data_array);
				$manufacturers_id = tep_db_insert_id();
			} elseif ($action == 'save') {
				$update_sql_data = array('last_modified' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $update_sql_data);
				tep_db_perform(TABLE_MANUFACTURERS, $sql_data_array, 'update', "manufacturers_id = '" . (int)$manufacturers_id . "'");
			}
			
			//ici on fait la requete de mise � jour de l'image si on up une nouvelle image
			if ($manufacturers_image = new upload('manufacturers_image', DIR_FS_CATALOG_IMAGES_MARQUES)) {
				//on met a jour la base de donn�e et on upload l'image si le champ manufacturers_image n'est pas vide
				if ($manufacturers_image->filename != "") {
					tep_db_query("update " . TABLE_MANUFACTURERS . " set manufacturers_image = '" . $manufacturers_image->filename . "' where manufacturers_id = '" . (int)$manufacturers_id . "'");
				}
			}		

			tep_redirect(tep_href_link(FILENAME_MANUFACTURERS, (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . 'mID=' . $manufacturers_id));
		break;
		
		case 'deleteconfirm':
			$manufacturers_id = tep_db_prepare_input($HTTP_GET_VARS['mID']);

			if (isset($HTTP_POST_VARS['delete_image']) && ($HTTP_POST_VARS['delete_image'] == 'on')) {
				$manufacturer_query = tep_db_query("select manufacturers_image from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$manufacturers_id . "'");
				$manufacturer = tep_db_fetch_array($manufacturer_query);

				$image_location = DIR_FS_DOCUMENT_ROOT . DIR_WS_CATALOG_IMAGES . $manufacturer['manufacturers_image'];
				if (file_exists($image_location)) @unlink($image_location);
			}

			tep_db_query("delete from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$manufacturers_id . "'");
			tep_db_query("delete from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$manufacturers_id . "'");

			if (isset($HTTP_POST_VARS['delete_products']) && ($HTTP_POST_VARS['delete_products'] == 'on')) {
				$products_query = tep_db_query("select products_id from " . TABLE_PRODUCTS . " where manufacturers_id = '" . (int)$manufacturers_id . "'");
				while ($products = tep_db_fetch_array($products_query)) {
					tep_remove_product($products['products_id']);
				}
			} else {
				tep_db_query("update " . TABLE_PRODUCTS . " set manufacturers_id = '' where manufacturers_id = '" . (int)$manufacturers_id . "'");
			}

			tep_redirect(tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page']));
		break;
	}
}
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
	<tr>
	<!-- body_text //-->
	<td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
			<td width="100%">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="pageHeading">Les Marques pr�sentes sur <? echo STORE_NAME; ?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="2">
								<tr class="dataTableHeadingRow">
									<td class="dataTableHeadingContent">&nbsp;&nbsp;Les Marques</td>
									<td class="dataTableHeadingContent" align="right">Action&nbsp;&nbsp;</td>
								</tr>
<?php
$manufacturers_query_raw = "SELECT 
								m.manufacturers_id, 
								m.manufacturers_name, 
								m.manufacturers_description,
								m.manufacturers_image, 
								m.date_added, 
								m.last_modified, 
								m.manufacturers_url, 
								m.manufacturers_meta_title,
								m.manufacturers_meta_description,
								m.manufacturers_meta_keywords,
								m.manufacturers_baseline from " . TABLE_MANUFACTURERS . " m LEFT JOIN " .  TABLE_MANUFACTURERS_INFO . " mi on m.manufacturers_id = mi.manufacturers_id  order by m.manufacturers_name";


$manufacturers_split = new splitPageResults($HTTP_GET_VARS['page'], 50, $manufacturers_query_raw, $manufacturers_query_numrows);
$manufacturers_query = tep_db_query($manufacturers_query_raw);
while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
	if ((!isset($HTTP_GET_VARS['mID']) || (isset($HTTP_GET_VARS['mID']) && ($HTTP_GET_VARS['mID'] == $manufacturers['manufacturers_id']))) && !isset($mInfo) && (substr($action, 0, 3) != 'new')) {
		$manufacturer_products_query = tep_db_query("SELECT count(*) as products_count FROM " . TABLE_PRODUCTS . " WHERE manufacturers_id = '" . (int)$manufacturers['manufacturers_id'] . "'");
		$manufacturer_products = tep_db_fetch_array($manufacturer_products_query);

		$mInfo_array = array_merge($manufacturers, $manufacturer_products);
		$mInfo = new objectInfo($mInfo_array);
	}

	if (isset($mInfo) && is_object($mInfo) && ($manufacturers['manufacturers_id'] == $mInfo->manufacturers_id)) {
		echo '<tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $manufacturers['manufacturers_id'] . '&action=edit') . '\'">' . "\n";
	} else {
		echo '<tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $manufacturers['manufacturers_id']) . '\'">' . "\n";
	}
?>
									<td class="dataTableContent">
										<?php echo $manufacturers['manufacturers_name']; ?>
									</td>
									<td class="dataTableContent" align="right">
										<?php 
										if (isset($mInfo) && is_object($mInfo) && ($manufacturers['manufacturers_id'] == $mInfo->manufacturers_id)) { 
											echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); 
										} else { 
											echo '<a href="' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $manufacturers['manufacturers_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; 
										} 
										?>
										&nbsp;
									</td>
								</tr>
<?php
}
?>
								<tr>
									<td colspan="2">
										<table border="0" width="100%" cellspacing="0" cellpadding="2">
											<tr>
												<td class="smallText" valign="top">
													<?php echo $manufacturers_split->display_count($manufacturers_query_numrows, 50, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS); ?>
												</td>
												<td class="smallText" align="right">
													<?php echo $manufacturers_split->display_links($manufacturers_query_numrows, 50, MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page']); ?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<?php
								if (empty($action)) {
								?>
									<tr>
										<td align="right" colspan="2" class="smallText"><?php echo '<a href="' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=new') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>'; ?></td>
									</tr>
								<?php
								}
								?>
							</table>
						</td>
<?php 
$heading = array();
$contents = array();

switch ($action) {
	case 'new':
		$heading[] = array('text' => '<b>Ajouter une nouvelle marque</b>');

		$contents = array('form' => tep_draw_form('manufacturers', FILENAME_MANUFACTURERS, 'action=insert', 'post', 'enctype="multipart/form-data"'));
		$contents[] = array('text' => 'Merci de compl�ter les informations sur cette nouvelle marque');
		$contents[] = array('text' => '<br>Nom de la Marque <br>' . tep_draw_input_field('manufacturers_name'));
		$contents[] = array('text' => '<br>URL de la Marque (sans point, espace, accent, caract�res sp�ciaux) <br>' . tep_draw_input_field('manufacturers_url'));
		$contents[] = array('text' => '<br>Image de la marque <br>' . tep_draw_file_field('manufacturers_image'));

		$contents[] = array('text' => '<br>Balise META Title (Titre de la marque)<br>' . tep_draw_input_field('manufacturers_meta_title'));
		$contents[] = array('text' => '<br>Balise META Description (Description de la marque)<br>' . tep_draw_input_field('manufacturers_meta_description'));
		$contents[] = array('text' => '<br>Balise META Keyword (Mots cl�s de la marque)<br>' . tep_draw_input_field('manufacturers_meta_keywords'));
		$contents[] = array('text' => '<br>Description de la Marque (au dessus des articles)<br>' . tep_draw_textarea_field('manufacturers_description', 'hard', 30, 5, ''));
		$contents[] = array('text' => '<br>BASELINE de cette Marque pour le r�f�rencement<br>' . tep_draw_textarea_field('manufacturers_baseline', 'hard', 30, 5, ''));
	  
		$contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $HTTP_GET_VARS['mID']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
	break;
	
	case 'edit':
		$heading[] = array('text' => '<b>' . 'Editer cette marque' . '</b>');

		$contents = array('form' => tep_draw_form('manufacturers', FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=save', 'post', 'enctype="multipart/form-data"'));
		$contents[] = array('text' => 'Merci de faire les changements n&eacute;cessaires');
		$contents[] = array('text' => '<br>Nom de la Marque<br>' . tep_draw_input_field('manufacturers_name', $mInfo->manufacturers_name));			
		$contents[] = array('text' => '<br>URL de la Marque (sans point, espace, accent, caract�res sp�ciaux)<br>' . tep_draw_input_field('manufacturers_url', $mInfo->manufacturers_url));
		$contents[] = array('text' => '<br>Image de la Marque<br>' . tep_draw_file_field('manufacturers_image') . '<br>' . $mInfo->manufacturers_image);
		$contents[] = array('text' => '<br>Balise META Title (Titre de la Marque)<br>' . tep_draw_input_field('manufacturers_meta_title', $mInfo->manufacturers_meta_title));
		$contents[] = array('text' => '<br>Balise META Description (Description de la Marque)<br>' . tep_draw_input_field('manufacturers_meta_description', $mInfo->manufacturers_meta_description));
		$contents[] = array('text' => '<br>Balise META Keyword (Mots cl�s de la Marque)<br>' . tep_draw_input_field('manufacturers_meta_keywords', $mInfo->manufacturers_meta_keywords));
		$contents[] = array('text' => '<br>Description de la Marque (au dessus des articles)<br>' . tep_draw_textarea_field('manufacturers_description', 'hard', 50, 20, $mInfo->manufacturers_description));
		$contents[] = array('text' => '<br>BASELINE de cette Marque pour le r�f�rencement<br>' . tep_draw_textarea_field('manufacturers_baseline', 'hard', 50, 5, $mInfo->manufacturers_baseline));
		$contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->manufacturers_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
	break;
	
	case 'delete':
		$heading[] = array('text' => '<b>Supprimer cette Marque </b>');

		$contents = array('form' => tep_draw_form('manufacturers', FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=deleteconfirm'));
		$contents[] = array('text' => 'Etes vous s�r de vouloir supprimer cette Marque ?');
		$contents[] = array('text' => '<br><b>' . $mInfo->manufacturers_name . '</b>');
		$contents[] = array('text' => '<br>' . tep_draw_checkbox_field('delete_image', '', true) . ' ' . 'Suppprimer l\'image de la marque ?');

		if ($mInfo->products_count > 0) {
			$contents[] = array('text' => '<br>' . tep_draw_checkbox_field('delete_products') . ' Supprimer tous les produits de cette marque ? (en incluant les commentaires, produits en promotion, produits � venir)');
			$contents[] = array('text' => '<br>' . sprintf('<b>ATTENTION :</b> Il reste %s produit(s) li&eacute;es &agrave; ce fabricant !', $mInfo->products_count));
		}

		$contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->manufacturers_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
	break;

	default:
		if (isset($mInfo) && is_object($mInfo)) {
			$heading[] = array('text' => '<b>' . $mInfo->manufacturers_name . '</b>');

			$contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', 'Supprimer') . '</a>');
			$contents[] = array('text' => '<br>Date d\'ajout : ' . tep_date_short($mInfo->date_added));
			if (tep_not_null($mInfo->last_modified)) $contents[] = array('text' => 'Derni�re modification : ' . tep_date_short($mInfo->last_modified));
			$contents[] = array('text' => '<br><div align="center">' . tep_info_image_manufacturers($mInfo->manufacturers_image, $mInfo->manufacturers_name) . '
																		</div>');
			$contents[] = array('text' => '<br>' . 'Produits : ' . $mInfo->products_count);
		}
	break;
}

if ((tep_not_null($heading)) && (tep_not_null($contents))) {
	echo '            <td width="25%" valign="top">' . "\n";
	
	$box = new box;
	echo $box->infoBox($heading, $contents);
	echo '</td>' . "\n";
}
?>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</td>
<!-- body_text_eof //-->
</tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
