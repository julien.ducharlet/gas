<?php
	
	$root = '/var/www/vhosts/generalarmystore.fr/httpdocs/';
	$url_site_sitemap ='https://www.generalarmystore.fr/gas/';
	
	include($root . "gasadmin/includes/configure.php");
	include($root . "gasadmin/includes/database_tables.php");
	include($root . "gasadmin/includes/functions/database.php");
	include($root . "gasadmin/includes/functions/general.php");

	tep_db_connect();

	include($root . "gasadmin/includes/functions/html_output.php");

#############################################################################################
// requ�te pour les categories de premier niveau
#############################################################################################
$query_marque = 'SELECT cd.categories_id, 
						cd.categories_name, 
						cd.categories_url, 
						r.rubrique_id, 
						r.rubrique_url  
				  
				  FROM categories c,
				  	   categories_description cd,
					   categories_rubrique cr,
					   rubrique r
					   
				  WHERE c.categories_id=cd.categories_id
					  and cd.categories_id=cr.categories_id
					  and cr.rubrique_id=r.rubrique_id
					  and parent_id=\'0\'
					  and categories_status=\'1\'
					  
				  ORDER BY r.rubrique_id, cd.categories_id';
				  
$query_marque = tep_db_query($query_marque);

$marqueXML = "";
$modeleXML = "";
while($data_marque = tep_db_fetch_array($query_marque)) {
		
	$url_marque = url("marque", array('id_rayon' => $data_marque["rubrique_id"],
									  'nom_rayon' => $data_marque["rubrique_url"],
									  'id_marque' => $data_marque["categories_id"],
									  'url_marque' => $data_marque["categories_url"]));
	
	$marqueXML .= "Redirect 301, ". $url_marque .", ";
	$marqueXML .= "___NOUVELLE_URL_CATEGORIE_COMPLETE___ \n";
	//echo $url_marque .'<br>';

	#############################################################################################
	// requ�te pour les categories au dela du premier niveau
	#############################################################################################
	$query_modele = 'SELECT 
							cd.categories_id, 
							cd.categories_name,
							cd.categories_url   
					  FROM 
							categories c,
							categories_description cd
					  WHERE 
							c.categories_id=cd.categories_id
							and parent_id=\''. $data_marque['categories_id'] .'\'
							and categories_status=\'1\'';
					  
	$query_modele = tep_db_query($query_modele);
		
	while($data_modele = tep_db_fetch_array($query_modele)) {
		
		$url_modele = url("modele", array('id_rayon' => $data_marque["rubrique_id"],
										  'nom_rayon' => $data_marque["rubrique_url"],
										  'id_marque' => $data_marque["categories_id"],
										  'url_marque' => $data_marque["categories_url"],
										  'id_modele' => $data_modele["categories_id"],
										  'url_modele' => $data_modele["categories_url"]));
		
		$modeleXML .= "Redirect 301, ". encode_url_sitemap_cat($url_modele) .", ";
		$modeleXML .= "___NOUVELLE_URL_CATEGORIE_COMPLETE___ \n";
		//echo '-----' . encode_url_sitemap_cat($url_modele) .'<br>';
	}
}

$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/transfert-301/htaccess301-categories.csv', 'w');
fwrite($fp, 'Fontion, Ancien URL sans domaine, Nouvelle URL Prestashop complete;'. "\n" .
					$marqueXML . "\n".
					$modeleXML . "\n".
				'');

fclose($fp);
	

################################################################################
// Produits transf�r�s par Evolutive Business
################################################################################

$nbArticleXML = 40000;
$query_count = 'select count(*) as total from '. TABLE_PRODUCTS_TO_CATEGORIES;
$query_count = tep_db_query($query_count);
$data_count = tep_db_fetch_array($query_count);
//echo $data_count['total'];
//echo '<br>'. ($data_count['total']/$nbArticleXML);

for($i=1 ; $i<= ceil($data_count['total']/$nbArticleXML) ; $i++) {
	
	$limit = ($i-1)*$nbArticleXML;
	
	$query = 'SELECT 	modele_desc.categories_name as nom_modele, 
						modele_desc.categories_url as url_modele, 
						modele_desc.categories_id as id_modele,
						marque_desc.categories_url as url_marque, 
						marque_desc.categories_id as id_marque,
						r.rubrique_id, 
						r.rubrique_url,
						pd.products_id, 
						pd.products_url as url_article, 
						p.products_last_modified
						  
				   FROM '. TABLE_RAYON .' r,
						'. TABLE_CATEGORIES_RAYON .' cr,
						'. TABLE_CATEGORIES_DESCRIPTION .' marque_desc,
						'. TABLE_CATEGORIES .' modele,
						'. TABLE_CATEGORIES_DESCRIPTION .' modele_desc,
						'. TABLE_PRODUCTS_TO_CATEGORIES .' ptc,
						'. TABLE_PRODUCTS .' p,
						'. TABLE_PRODUCTS_DESCRIPTION .' pd
					
					WHERE r.rubrique_id=cr.rubrique_id
						and cr.categories_id=modele.categories_id
						and modele.categories_id=modele_desc.categories_id
						and modele.parent_id=marque_desc.categories_id
						and modele.categories_id=ptc.categories_id
						and ptc.products_id=p.products_id
						and p.products_id=pd.products_id
						and cr.rubrique_id >=\'2\'						
						and p.products_status=\'1\' 
						and p.products_id <= \'8561\'
				
					ORDER BY r.rubrique_id, p.products_id 
					
					LIMIT '. $limit .','. $nbArticleXML;
					
	$query = tep_db_query($query);
	
	
	$productXML = '';	
	while($data = tep_db_fetch_array($query)) {
		
		$url = url("article", array('id_rayon' => $data["rubrique_id"],
									'nom_rayon' => $data["rubrique_url"],
									'id_marque' => $data["id_marque"],
									'url_marque' => $data["url_marque"],
									'id_modele' => $data["id_modele"],
									'url_modele' => $data["url_modele"],
									'id_article' => $data["products_id"],
									'url_article' => bda_product_name_transform($data["url_article"],$data["id_modele"])));
		
		$productXML .= "Redirect 301 ". $url ."\t\t";
		$productXML .= "https://preprod.grouparmystore.fr/". $data["products_id"] ."-". strtolower(encode_url_sitemap_prod($data["url_article"])) .".html\n"; 
	}
	
	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/transfert-301/htaccess301-anciens-produits'. $i .'.txt', 'w');
	fwrite($fp, ''. $productXML ."\n". '');
	fclose($fp);
}

################################################################################
// Produits cr��s apr�s le transfert des produits par Evolutive Business
################################################################################


for($i=1 ; $i<= ceil($data_count['total']/$nbArticleXML) ; $i++) {
	
	$limit = ($i-1)*$nbArticleXML;
	
	$query = 'SELECT 	modele_desc.categories_name as nom_modele, 
						modele_desc.categories_url as url_modele, 
						modele_desc.categories_id as id_modele,
						marque_desc.categories_url as url_marque, 
						marque_desc.categories_id as id_marque,
						r.rubrique_id, 
						r.rubrique_url,
						pd.products_id, 
						pd.products_url as url_article, 
						p.products_last_modified
						  
				   FROM '. TABLE_RAYON .' r,
						'. TABLE_CATEGORIES_RAYON .' cr,
						'. TABLE_CATEGORIES_DESCRIPTION .' marque_desc,
						'. TABLE_CATEGORIES .' modele,
						'. TABLE_CATEGORIES_DESCRIPTION .' modele_desc,
						'. TABLE_PRODUCTS_TO_CATEGORIES .' ptc,
						'. TABLE_PRODUCTS .' p,
						'. TABLE_PRODUCTS_DESCRIPTION .' pd
					
					WHERE r.rubrique_id=cr.rubrique_id
						and cr.categories_id=modele.categories_id
						and modele.categories_id=modele_desc.categories_id
						and modele.parent_id=marque_desc.categories_id
						and modele.categories_id=ptc.categories_id
						and ptc.products_id=p.products_id
						and p.products_id=pd.products_id
						and cr.rubrique_id BETWEEN \'2\' AND \'3\' 
						and p.products_status=\'1\' 
						and p.products_id >= \'8562\'
				
					ORDER BY r.rubrique_id, p.products_id 
					
					LIMIT '. $limit .','. $nbArticleXML;
					
	$query = tep_db_query($query);
	
	$productXML = '';	
	while($data = tep_db_fetch_array($query)) {
		
		$url = url("article", array('id_rayon' => $data["rubrique_id"],
									'nom_rayon' => $data["rubrique_url"],
									'id_marque' => $data["id_marque"],
									'url_marque' => $data["url_marque"],
									'id_modele' => $data["id_modele"],
									'url_modele' => $data["url_modele"],
									'id_article' => $data["products_id"],
									'url_article' => bda_product_name_transform($data["url_article"],$data["id_modele"])));
		
		$productXML .= "Redirect 301, ". $url;
		$productXML .= ", XXXXXXX, -". strtolower(encode_url_sitemap_prod($data["url_article"])) .".html;\n"; 
	}
	/* POUR EXPORT TXT
	$productXML .= "Redirect 301 ". $url ."\t\t";
	$productXML .= "https://www.grouparmystore.fr/XXXXXXX-". strtolower(encode_url_sitemap($data["url_article"])) .".html\n"; 
	PENSER A MODIFIER LE .CSV CI DESSOUS EN .TXT
	*/ 
	
	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/transfert-301/htaccess301-nouveaux-produits'. $i .'.csv', 'w');
	fwrite($fp, 'Fontion, Ancien URL sans domaine, Nouvelle ID sur Prestashop, Texte URL Prestashop ;  
				'. $productXML ."\n". '');
	fclose($fp);
}

?>