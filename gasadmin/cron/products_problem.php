<?php

$logTimeBegin = date('d-m-Y \� h:i:s');

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/html_output.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/fonctions/mail.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/configure_mail.php");


$productsRegistered = array();
$productsToAvoid = array();
$productsToRemove = array();
$productsList = array();
$queryList = array();




$content = '
	<table class="tablesorter">
                	<thead>
						<tr>
                    		<th align="center">R�f�rence</th>
                    		<th align="center">Nom</th>
                            <th align="center" style="width:90px;">Qt� r�elle</th>
                            <th align="center" style="width:90px;">Qt� virtuelle</th>
                    		<th align="center" style="width:200px;">Qt� command�e</th>
                            <th align="center" style="width:100px;">Actions</th>
                		</tr>
					</thead>
                    <tbody>';

/*

$query = 'select o.orders_id, op.products_id, op.products_name, p.products_model, p.products_quantity_reel, p.products_quantity,
					SUM(op.products_quantity-op.products_quantity_sent) as diff_virtuel,
					(p.products_quantity_reel-p.products_quantity) as diff_reel
				
				from '. TABLE_PRODUCTS .' p,
				'. TABLE_ORDERS_PRODUCTS .' op,
				'. TABLE_ORDERS .' o
				
				where p.products_id=op.products_id
				and op.orders_id=o.orders_id
				and op.products_quantity_sent<op.products_quantity
				and orders_status!=21 ';
 //if(!empty($productsToAvoid)) $query .= 'and p.products_id NOT IN('. $productsToAvoid .') ';
			$query .= 'group by op.products_id
				having diff_reel!=diff_virtuel
				order by p.products_model desc';
$query = tep_db_query($query);

while($data = tep_db_fetch_array($query)) {
	
	$productsList[] = $data['products_id'];
	
	$link = tep_href_link('article_edit_stocks_options.php', 'pID=' . $data['products_id'] . '&action=new_product');
	$link_cmd_attente = tep_href_link('cmd_en_attente.php', 'pID=' . $data['products_id']);
	
	$content .= '<tr>';
		$content .= '<td><a href="'. $link .'" target="_blank">'. $data['products_model']. ' '. $data['diff_total'] .'</a></td>';
		$content .= '<td><a href="'. $link .'" target="_blank">'. $data['products_name'] .'</a></td>';
		$content .= '<td align="center">'. $data['products_quantity_reel']. '</td>';
		$content .= '<td align="center">'. $data['products_quantity'] .'</a></td>';
		$content .= '<td align="center">'. $data['diff_virtuel'] .'</a></td>';
		$content .= '<td>';
			$content .= '<a href="'. $link .'" target="_blank">'. tep_image(WEBSITE . BASE_DIR_ADMIN .'/'. DIR_WS_IMAGES .'/icons/editer.png', "Editer") .'</a>&nbsp;&nbsp;';
			$content .= '<a href="'. $link_cmd_attente .'" target="_blank">'. tep_image(WEBSITE . BASE_DIR_ADMIN .'/'. DIR_WS_IMAGES .'/icone_commandes_client.png', "Voir les commandes avec cet article", '', '20') .'</a>';
		$content .= '</td>';
	$content .= '</tr>';
}

$content .= '<tr><td colspan="6"><hr></td></tr>';



foreach($productsRegistered as $productToVerify) {
	
	$query = 'select * from '. TABLE_PRODUCTS .' p1 where (products_quantity_reel-products_quantity) = (
						select sum( products_quantity - products_quantity_sent) from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op
					where o.orders_id=op.orders_id
					and op.products_id=p1.products_id
					and orders_status!=21)
					and p1.products_id='. $productToVerify;
	$query = tep_db_query($query);
	
	if(tep_db_num_rows($query)>0) {
		
		$productsToRemove[] = $productToVerify;
	}
}

$productsRegistered = array_diff($productsRegistered, $productsToRemove);

$productsToAvoid = implode(', ', array_merge($productsRegistered, $productsList));


$query = 'select p.products_id, products_model, products_name, products_quantity, products_quantity_reel, products_quantity_reel-products_quantity as diff_reel
			from '. TABLE_PRODUCTS .' p,
				 '. TABLE_PRODUCTS_DESCRIPTION .' pd
			 where p.products_id=pd.products_id and products_quantity_reel!=products_quantity ';
if(!empty($productsToAvoid)) $query .= 'and p.products_id NOT IN('. $productsToAvoid .') ';

$query = tep_db_query($query);

while($data = tep_db_fetch_array($query)) {
		
		//ne marchera pas pour les reliquats
		$query2 = 'select sum( products_quantity - products_quantity_sent) AS total_to_send from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op
					where o.orders_id=op.orders_id
					and products_quantity_sent<products_quantity
					and orders_status!=21
					and products_id='. $data['products_id'] .'
					group by products_id
					having total_to_send='. $data['diff_reel'];
		$queryList[] = $query2;
		$query2 = tep_db_query($query2);
		
		if(!tep_db_num_rows($query2)) {
			
			$data2 = tep_db_fetch_array($query2);
			
			$productsList[] = $data['products_id'];
			
			$link = tep_href_link('article_edit_stocks_options.php', 'pID=' . $data['products_id'] . '&action=new_product');
			$link_cmd_attente = tep_href_link('cmd_en_attente.php', 'pID=' . $data['products_id']);
			
			$content .= '<tr>';
				$content .= '<td><a href="'. $link .'" target="_blank">'. $data['products_model']. ' '. $data['diff_total'] .'</a></td>';
				$content .= '<td><a href="'. $link .'" target="_blank">'. $data['products_name'] .'</a></td>';
				$content .= '<td align="center">'. $data['products_quantity_reel']. '</td>';
				$content .= '<td align="center">'. $data['products_quantity'] .'</a></td>';
				$content .= '<td align="center">'. $data2['total_to_send'] .'</a></td>';
				$content .= '<td>';
					$content .= '<a href="'. $link .'" target="_blank">'. tep_image(WEBSITE . BASE_DIR_ADMIN .'/'. DIR_WS_IMAGES .'/icons/editer.png', "Editer") .'</a>&nbsp;&nbsp;';
					$content .= '<a href="'. $link_cmd_attente .'" target="_blank">'. tep_image(WEBSITE . BASE_DIR_ADMIN .'/'. DIR_WS_IMAGES .'/icone_commandes_client.png', "Voir les commandes avec cet article", '', '20') .'</a>';
				$content .= '</td>';
			$content .= '</tr>';
		}
}
*/

/*
$query = 'select o.orders_id, op.products_id, op.products_name, p.products_model, p.products_quantity_reel, p.products_quantity,
					SUM(op.products_quantity-op.products_quantity_sent) as diff_virtuel,
					SUM(options_quantity_reel) as 
					(p.products_quantity_reel-p.products_quantity) as diff_reel
				
				from '. TABLE_PRODUCTS .' p,
				'. TABLE_PRODUCTS_ATTRIBUTES .' pa,
				'. TABLE_ORDERS_PRODUCTS .' op,
				'. TABLE_ORDERS_PRODUCTS_ATTRIBUTES .' opa,
				'. TABLE_ORDERS .' o
				
				where p.products_id=op.products_id
				and p.products_id=pa.products_id
				and op.orders_id=o.orders_id
				and op.orders_products_id=opa.orders_products_id
				and op.products_quantity_sent<op.products_quantity
				and orders_status!=21 ';
 //if(!empty($productsToAvoid)) $query .= 'and p.products_id NOT IN('. $productsToAvoid .') ';
			$query .= 'group by op.products_id
				having diff_reel!=diff_virtuel
				order by p.products_model desc';
*/

/*
$query = 'select op.*, opa.options_values_id,
				 SUM(op.products_quantity-op.products_quantity_sent) as diff_virtuel,
					(p.products_quantity_reel-p.products_quantity) as diff_reel
				 
			from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op, '. TABLE_PRODUCTS .' p, '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES  .' opa
			where o.orders_id=op.orders_id
			and op.products_id=p.products_id
			and op.orders_products_id=opa.orders_products_id
			and orders_status!=21
			and op.products_quantity_sent<op.products_quantity
			
			group by op.products_id
			having diff_reel!=diff_virtuel';
$query = tep_db_query($query);

while($data = tep_db_fetch_array($query)) {
		
		$query2 = 'update '. TABLE_PRODUCTS .' set products_quantity = products_quantity_reel where products_id='. $data['products_id'];
		tep_db_query($query);
		
		$query2 = 'update '. TABLE_PRODUCTS_ATTRIBUTES .' set options_quantity = options_quantity_reel where products_id='. $data['products_id'] .' and options_values_id='. $data['options_values_id'];
		echo $query2 ."\n";
		
		//tep_db_query($query);
		
		$productsList[] = $data['products_id'];
		
		$link = tep_href_link('article_edit_stocks_options.php', 'pID=' . $data['products_id'] . '&action=new_product');
		$link_cmd_attente = tep_href_link('cmd_en_attente.php', 'pID=' . $data['products_id']);
		
		$content .= '<tr>';
			$content .= '<td><a href="'. $link .'" target="_blank">'. $data['products_model']. ' '. $data['diff_total'] .'</a></td>';
			//$content .= '<td><a href="'. $link .'" target="_blank">'. $data['products_name'] .'</a></td>';
			$content .= '<td align="center">'. $data['products_quantity_reel']. '</td>';
			$content .= '<td align="center">'. $data['products_quantity'] .'</a></td>';
			$content .= '<td align="center">'. $data2['total_to_send'] .'</a></td>';
			$content .= '<td>';
				$content .= '<a href="'. $link .'" target="_blank">'. tep_image(WEBSITE . BASE_DIR_ADMIN .'/'. DIR_WS_IMAGES .'/icons/editer.png', "Editer") .'</a>&nbsp;&nbsp;';
				$content .= '<a href="'. $link_cmd_attente .'" target="_blank">'. tep_image(WEBSITE . BASE_DIR_ADMIN .'/'. DIR_WS_IMAGES .'/icone_commandes_client.png', "Voir les commandes avec cet article", '', '20') .'</a>';
			$content .= '</td>';
		$content .= '</tr>';
}
*/
/*
$query = 'update '. TABLE_PRODUCTS_ATTRIBUTES .' set options_quantity = options_quantity_reel';
tep_db_query($query);*/

$query = 'select op.*, options_values_id, (op.products_quantity-op.products_quantity_sent) as diff_virtuel
				from '. TABLE_ORDERS .' o, '. TABLE_ORDERS_PRODUCTS .' op, '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES  .' opa
				where o.orders_id=op.orders_id
				and op.orders_products_id=opa.orders_products_id
				and orders_status!=21
				and op.products_quantity_sent<op.products_quantity
				';
$query = tep_db_query($query);
while($data = tep_db_fetch_array($query)) {
	
	$query2 = 'update '. TABLE_PRODUCTS_ATTRIBUTES .' set options_quantity = options_quantity-'. ($data['diff_virtuel']) .' where products_id='. $data['products_id'] .' and options_values_id='. $data['options_values_id'];
	echo $query2 ."\n";
	tep_db_query($query2);
}

echo "\n\n";

$query = 'select products_id, sum(options_quantity) as options_quantity, sum(options_quantity_reel) as options_quantity_reel from '. TABLE_PRODUCTS_ATTRIBUTES .' group by products_id';
$query = tep_db_query($query);
while($data = tep_db_fetch_array($query)) {
	
	
	if($data['options_quantity'] == $data['options_quantity_reel']) {
		
		$query2 = 'update '. TABLE_PRODUCTS .' set products_quantity='. $data['options_quantity'] .', products_quantity_reel='. $data['options_quantity_reel'] .' where products_id='. $data['products_id'];
	}
	else {
		
		$query2 = 'update '. TABLE_PRODUCTS .' set products_quantity='. $data['options_quantity'] .' where products_id='. $data['products_id'];
	}
	
	echo $query2 ."\n";
	tep_db_query($query2);
}



$content .= '</tbody>
	</table>';

?>