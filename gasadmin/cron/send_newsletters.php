<?php
// URL pour envoyer manuellement : 
// https://www.generalarmystore.fr/gasadmin/cron/send_newsletters.php

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/html_output.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/fonctions/mail.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/configure_mail.php");

$total_mail_sent = 0;

$query_newsletter = 'select * from '. TABLE_NEWSLETTERS .' where mail_sent=0 and date_envoi!=\'0000-00-00\' and date_envoi<=NOW()';
$query_newsletter = tep_db_query($query_newsletter);

while($data = tep_db_fetch_array($query_newsletter)) {
	
	$temp_type_id_client = (!empty($data['type_id_client'])) ? explode(',', $data['type_id_client']) : array();
	$newsletter = parseMail($data['newsletters_id'], $data['newsletters_content']);
	$newsletter .= '<img src="' . WEBSITE . BASE_DIR .'/image_lecture_newsletter.php?news='. md5($data['newsletters_id']) .'">';
	
	foreach($temp_type_id_client as $type_id_client) {
		
		$addQuery = (!empty($type_id_client)) ? ' and customers_type='. $type_id_client : '';
			
		switch($data['type_client']) {
			
		case "tous": //tous
			
			if($data['type_cmd']=="avec") {//avec commande
				
				$query = 'select distinct(c.customers_id), c.customers_email_address from '. TABLE_CUSTOMERS .' c, '. TABLE_ORDERS .' o where c.customers_id=o.customers_id'. $addQuery;
			}
			elseif($data['type_cmd']=="sans") {
				
				$query = 'select distinct(customers_id), customers_email_address from '. TABLE_CUSTOMERS .' where customers_id NOT IN(select customers_id from '. TABLE_ORDERS .')'. $addQuery;
			}
			else {
				
				$query = 'select distinct(customers_id), customers_email_address from '. TABLE_CUSTOMERS . str_replace('and', 'where', $addQuery);
			}
			break;
		
		case "inscrit": //inscrit
		
			if($data['type_cmd']=="avec") {//avec commande
				
				$query = 'select distinct(c.customers_id), c.customers_email_address from '. TABLE_CUSTOMERS .' c, '. TABLE_ORDERS .' o where c.customers_id=o.customers_id and customers_newsletter=\'1\''. $addQuery;
			}
			elseif($data['type_cmd']=="sans") {
				
				$query = 'select distinct(customers_id), customers_email_address from '. TABLE_CUSTOMERS .' where customers_newsletter=\'1\' and customers_id NOT IN(select customers_id from '. TABLE_ORDERS .')'. $addQuery;
			}
			else {
				
				$query = 'select distinct(customers_id), customers_email_address from '. TABLE_CUSTOMERS .' where customers_newsletter=\'1\''. $addQuery;
			}
			break;
		
		case "partenaire": //inscrit
		
			if($data['type_cmd']=="avec") {//avec commande
				
				$query = 'select distinct(c.customers_id), c.customers_email_address from '. TABLE_CUSTOMERS .' c, '. TABLE_ORDERS .' o where c.customers_id=o.customers_id and customers_newsletter_partenaire=\'1\''. $addQuery;
			}
			elseif($data['type_cmd']=="sans") {
				
				$query = 'select distinct(customers_id), customers_email_address from '. TABLE_CUSTOMERS .' where customers_newsletter_partenaire=\'1\' and customers_id NOT IN(select customers_id from '. TABLE_ORDERS .')'. $addQuery;
			}
			else {
				
				$query = 'select distinct(customers_id), customers_email_address from '. TABLE_CUSTOMERS .' where customers_newsletter_partenaire=\'1\''. $addQuery;
			}
			break;
		
		case "non inscrit": //non inscrit
			
			if($data['type_cmd']=="avec") {//avec commande
				
				$query = 'select distinct(c.customers_id), c.customers_email_address from '. TABLE_CUSTOMERS .' c, '. TABLE_ORDERS .' o where c.customers_id=o.customers_id and customers_newsletter=\'0\''. $addQuery;
			}
			elseif($data['type_cmd']=="sans") {
				
				$query = 'select distinct(customers_id), customers_email_address from '. TABLE_CUSTOMERS .' where customers_newsletter=\'0\' and customers_id NOT IN(select customers_id from '. TABLE_ORDERS .')'. $addQuery;
			}
			else {
				
				$query = 'select distinct(customers_id), customers_email_address from '. TABLE_CUSTOMERS .' where customers_newsletter=\'0\''. $addQuery;
			}
			break;
		}
		
		$query = tep_db_query($query);
		$total_mail_sent += tep_db_num_rows($query);
		
		while($total = tep_db_fetch_array($query)) {
			
			$newsletter_content = $newsletter;
			$newsletter_content .= '<img src="'. WEBSITE . BASE_DIR .'/image_lecture_newsletter.php?news='. $data['newsletters_md5'] .'" width="1px" height="1px">';
			
			email($total['customers_email_address'], $data['newsletters_title'], $newsletter, $data['newsletters_id']);
		}
		
		email(MAIL_INFO, $data['newsletters_title'], $newsletter, $data['newsletters_id']);
	} // foreach($temp_type_id_client as $type_id_client) {
	
	$query = 'update '. TABLE_NEWSLETTERS .' set mail_sent="1", mail_number_sent="'. $total_mail_sent .'" where newsletters_id="'. $data['newsletters_id'].'"';
	tep_db_query($query);
	/*@mail('thierry@mtp34.com','NEWS GAS DEBUG', 'total_mail_sent=' .  $total_mail_sent . "\n" . 'newsletters_id=' . $data['newsletters_id']);*/
	
} // while($data = tep_db_fetch_array($query_newsletter)) {
?>