<?php

$root = '/var/www/vhosts/generalarmystore.fr/httpdocs/';
$url_site_sitemap ='https://www.generalarmystore.fr/gas/';
	
include($root . "gasadmin/includes/configure.php");
include($root . "gasadmin/includes/database_tables.php");
include($root . "gasadmin/includes/functions/database.php");
include($root . "gasadmin/includes/functions/general.php");

tep_db_connect();

include($root . "gasadmin/includes/functions/html_output.php");

$query_marque = '	SELECT 
						cd.categories_id, 
						cd.categories_url, 
						r.rubrique_id, 
						r.rubrique_url, 
						c.last_modified  
					FROM 
						'. TABLE_CATEGORIES .' c,
						'. TABLE_CATEGORIES_DESCRIPTION .' cd,
						'. TABLE_CATEGORIES_RAYON .' cr,
						'. TABLE_RAYON.' r
					WHERE 
						c.categories_id=cd.categories_id
					AND 
						cd.categories_id=cr.categories_id
					AND 
						cr.rubrique_id=r.rubrique_id
					AND 
						parent_id=\'0\'
					AND 
						categories_status=\'1\'
					ORDER BY last_modified desc';
				  
$query_marque = tep_db_query($query_marque);

$marqueXML = "";
$modeleXML = "";	
while($data_marque = tep_db_fetch_array($query_marque)) {
	
	$url_marque = url("marque", array('id_rayon' => $data_marque["rubrique_id"],
									  'nom_rayon' => $data_marque["rubrique_url"],
									  'id_marque' => $data_marque["categories_id"],
									  'url_marque' => $data_marque["categories_url"]));
	
	$tab_mod = explode(' ', $data_marque["last_modified"]);
	if(empty($data_marque["last_modified"]) || $tab_mod[0]=='0000-00-00') {
		$tab_mod[0] = date("Y-m-d");
	}
	
	$marqueXML .= "\t<url>\n";
	$marqueXML .= "\t\t<loc>". $url_site_sitemap . $url_marque ."</loc>\n";
	$marqueXML .= "\t\t<lastmod>". $tab_mod[0] ."</lastmod>\n";
	$marqueXML .= "\t\t<changefreq>weekly</changefreq>\n";
	$marqueXML .= "\t</url>\n";
	
	$query_modele = '	SELECT 
							cd.categories_id, 
							cd.categories_url, 
							c.last_modified   
						FROM 
							'. TABLE_CATEGORIES .' c,
							'. TABLE_CATEGORIES_DESCRIPTION .' cd
						WHERE 
							c.categories_id=cd.categories_id
						AND 
							parent_id=\''. $data_marque['categories_id'] .'\'
						AND 
							categories_status=\'1\'
						ORDER BY last_modified desc';
					  
	$query_modele = tep_db_query($query_modele);
	
	while($data_modele = tep_db_fetch_array($query_modele)) {
		
		$url_modele = url("modele", array('id_rayon' => $data_marque["rubrique_id"],
										  'nom_rayon' => $data_marque["rubrique_url"],
										  'id_marque' => $data_marque["categories_id"],
										  'url_marque' => $data_marque["categories_url"],
										  'id_modele' => $data_modele["categories_id"],
										  'url_modele' => $data_modele["categories_url"]));
		$tab_mod=explode(" ", $data_modele["last_modified"]);
		
		if(empty($data_modele["last_modified"]) || $tab_mod[0]=='0000-00-00') {
			
			$tab_mod[0] = date("Y-m-d");
		}
				
		$modeleXML .= "\t<url>\n";
		$modeleXML .= "\t\t<loc>". $url_site_sitemap . $url_modele ."</loc>\n";
		$modeleXML .= "\t\t<lastmod>". $tab_mod[0] ."</lastmod>\n";
		$modeleXML .= "\t\t<changefreq>weekly</changefreq>\n";
		$modeleXML .= "\t</url>\n";
	}
}

$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gas/sitemapcategories.xml', 'w');
fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . 
				'<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">'. "\n\n".
					$marqueXML . "\n".
					$modeleXML ."\n".
				'</urlset>');
fclose($fp);

$sitemapIndex = '<sitemap>' . "\n". '
			<loc>'. $url_site_sitemap . 'sitemapcategories.xml</loc>'. "\n". '
			<lastmod>'. date("Y-m-d") .'</lastmod>'."\n".'
		</sitemap>'. "\n";

# SITEMAP ARTICLES LISTE 1

$nbArticleXML = 40000;

$query_count = 'select count(*) as total from '. TABLE_PRODUCTS_TO_CATEGORIES;
$query_count = tep_db_query($query_count);

$data_count = tep_db_fetch_array($query_count);

//echo $data_count['total'];
//echo '<br>'. ($data_count['total']/$nbArticleXML);

for($i=1 ; $i<= ceil($data_count['total']/$nbArticleXML) ; $i++) {
	
	$limit = ($i-1)*$nbArticleXML;
	
	$query = '	SELECT 
					modele_desc.categories_name as nom_modele, 
					modele_desc.categories_url as url_modele, 
					modele_desc.categories_id as id_modele,
					marque_desc.categories_url as url_marque, 
					marque_desc.categories_id as id_marque,
					r.rubrique_id, r.rubrique_url,
					pd.products_id, 
					pd.products_url as url_article,
					p.products_bimage,					
					p.products_last_modified  
			   FROM '. TABLE_RAYON .' r,
					'. TABLE_CATEGORIES_RAYON .' cr,
					'. TABLE_CATEGORIES_DESCRIPTION .' marque_desc,
					'. TABLE_CATEGORIES .' modele,
					'. TABLE_CATEGORIES_DESCRIPTION .' modele_desc,
					'. TABLE_PRODUCTS_TO_CATEGORIES .' ptc,
					'. TABLE_PRODUCTS .' p,
					'. TABLE_PRODUCTS_DESCRIPTION .' pd
				
				WHERE 
					
					r.rubrique_id=cr.rubrique_id
					and cr.categories_id=modele.categories_id
					and modele.categories_id=modele_desc.categories_id
					and modele.parent_id=marque_desc.categories_id
					and modele.categories_id=ptc.categories_id
					and ptc.products_id=p.products_id
					and p.products_id=pd.products_id
					and cr.rubrique_id=\'1\' 
					AND p.products_status = \'1\'
			
				ORDER BY p.products_last_modified desc
				
				LIMIT '. $limit .','. $nbArticleXML;
					
	$query = tep_db_query($query);
	
	$productXML = '';	
	while($data = tep_db_fetch_array($query)) {
		
		$url = url("article", array('id_rayon' => $data["rubrique_id"],
									'nom_rayon' => $data["rubrique_url"],
									'id_marque' => $data["id_marque"],
									'url_marque' => format_sitemap($data["url_marque"]),
									'id_modele' => $data["id_modele"],
									'url_modele' => format_sitemap($data["url_modele"]),
									'id_article' => $data["products_id"],
									'url_article' => format_sitemap($data["url_article"])));
		
		$tab_mod=explode(" ", $data["products_last_modified"]);
		
		if(empty($data["products_last_modified"])) {
			
			$tab_mod[0] = date("Y-m-d");
		}
		
		$productXML .= "\t<url>\n";
		$productXML .= "\t\t<loc>". $url_site_sitemap . $url ."</loc>\n";
		$productXML .= "\t\t<lastmod>". $tab_mod[0] ."</lastmod>\n";
		//$productXML .= "\t\t<changefreq>monthly</changefreq>\n";
		//$productXML .= "\t\t<priority>1</priority>\n";
		$productXML .= "\t\t<image:image>\n";
		$productXML .= "\t\t\t<image:loc>https://www.generalarmystore.fr/gas/images/products_pictures/grande/". $data["products_bimage"] ."</image:loc>\n";
		$productXML .= "\t\t</image:image>\n";
		
		
		/*
		<image:image><image:loc>https://www.generalarmystore.fr/gas/images/products_pictures/normale/
		". $data["products_bimage"] ."
		</image:loc></image:image>
		*/
		
		$productXML .= "\t</url>\n";
		##### RAJOTUER DES BALISES DE DETAIL (IMAGES, VIDEO...) #########
	}
	
	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gas/sitemapproducts'. $i .'.xml', 'w');
	fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . 
					'<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="https://www.google.com/schemas/sitemap-image/1.1">'. "\n\n".
						$productXML .
					'</urlset>');
	fclose($fp);
	
	$sitemapIndex .= '<sitemap>' . "\n". '
			<loc>'. $url_site_sitemap . 'sitemapproducts'. $i .'.xml</loc>'. "\n". '
			<lastmod>'. date("Y-m-d") .'</lastmod>'."\n".'
		</sitemap>'. "\n";
}
/*
$query_family = 'SELECT cf.family_client_id, family_client_url,
						marque.categories_id as marque_id, marque_desc.categories_url as marque_url,
						modele.categories_id as modele_id, modele_desc.categories_url as modele_url,
						r.rubrique_id, r.rubrique_url, cf.last_modified   
				  
				  FROM '. TABLE_CLIENT_FAMILY .' cf,
				  	   '. TABLE_CLIENT_FAMILY_INFO .' cfi,
				  	   '. TABLE_PRODUCTS .' p,
					   '. TABLE_PRODUCTS_TO_CATEGORIES .' ptc,
					   '. TABLE_CATEGORIES .' modele,
				  	   '. TABLE_CATEGORIES_DESCRIPTION .' modele_desc,
					   '. TABLE_CATEGORIES .' marque,
				  	   '. TABLE_CATEGORIES_DESCRIPTION .' marque_desc,
					   '. TABLE_CATEGORIES_RAYON .' cr,
					   '.TABLE_RAYON.' r
					   
				  WHERE cf.family_client_id=cfi.family_client_id
				  	  and cfi.family_client_id=p.family_client_id
					  and p.products_id=ptc.products_id
					  and ptc.categories_id=modele.categories_id
					  and modele.categories_id=modele_desc.categories_id
					  and modele.parent_id=marque.categories_id
					  and marque.categories_id=marque_desc.categories_id
					  and marque_desc.categories_id=cr.categories_id
					  and cr.rubrique_id=r.rubrique_id
				  
				  GROUP BY modele.categories_id, cf.family_client_id';
$query_family = tep_db_query($query_family);

while($data_family = tep_db_fetch_array($query_family)) {
	
	$url_family = url_sitemap('modele_famille', array('id_rayon' => $data_family['rubrique_id'],
										'nom_rayon' => $data_family['rubrique_url'],
										'id_marque' => $data_family['marque_id'],
										'url_marque' => $data_family['marque_url'],
										'id_modele' => $data_family['modele_id'],
										'url_modele' => $data_family['modele_url'],
										'id_famille' => $data_family['family_client_id'],
										'url_famille' => $data_family['family_client_url']));
	
	$tab_mod = explode(' ', $data_family["last_modified"]);
	if(empty($data_family["last_modified"])) {
		
		$tab_mod[0] = date("Y-m-d");
	}
	
	$familyXML .= "\t<url>\n";
	$familyXML .= "\t\t<loc>https://www.generalarmystore.fr/fr". $url_family ."</loc>\n";
	$familyXML .= "\t\t<lastmod>". $tab_mod[0] ."</lastmod>\n";
	$familyXML .= "\t\t<changefreq>weekly</changefreq>\n";
	$familyXML .= "\t</url>\n";
}

$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gas/sitemapfamilies.xml', 'w');
fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . 
				'<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">'. "\n\n".
					$familyXML ."\n".
				'</urlset>');
fclose($fp);

$sitemapIndex .= '<sitemap>' . "\n". '
			<loc>'. $url_site_sitemap . 'sitemapfamilies.xml</loc>'. "\n". '
			<lastmod>'. date("Y-m-d") .'</lastmod>'."\n".'
		</sitemap>'. "\n";
*/

#sitemap index ou l'on stocke les différents sitemap
$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gas/sitemapindex.xml', 'w');
fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>'."\n".'
	<sitemapindex xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">'."\n"
		. $sitemapIndex .
	'</sitemapindex>');
fclose($fp);
?>