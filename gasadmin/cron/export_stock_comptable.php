<?php
//
// Permet de faire un export du stock des produits avec un stock > a 0 et visible sur le site
// utile pour faire le stock comptable en fin d'ann�e.

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/html_output.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/fonctions/mail.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/configure_mail.php");

	function getReplace($string = ''){
		 return trim( str_replace( array(';',"\n","\r","\t"), '', $string) ); 	
	}
	
	$fp = fopen('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/tmp/export_stock_'. date('Y-m-d_H-i').'.csv','w+');
	
	$write = '';	
	$write .= 'R�f�rence' . ';' ; 
	$write .= 'Nom' . ';' ; 
	$write .= 'Option' . ';' ; 
	$write .= 'Quantit�' . ';' ; 
	$write .= 'PA HT Unitaire' . ';' ;
	$write .= 'PA HT Total' . ';' ; 
	fwrite( $fp , $write . "\n");
	
	// Requ�te produit
	$query_produit = tep_db_query("	SELECT 
										p.*, 
										pd.products_name 
									FROM 
										products p, 
										products_description pd 
									WHERE 
										pd.products_id = p.products_id 
									AND 
										p.products_status='1'
									AND 
										p.products_quantity_reel >'0'
									ORDER BY p.products_model
								  ");
	
	// Boucle qui va afficher les produits
 	while( $sql_produit = tep_db_fetch_array($query_produit) ) {
		
		// requ�te option
		$query_option = tep_db_query("
										SELECT 
											pov.products_options_values_name, 
											po.products_options_name, 
											pa.options_id, 
											pa.options_values_id , 
											pa.options_quantity_reel
										FROM 
											products_attributes pa,
											products_options po,
											products_options_values pov			
										WHERE 
											pa.products_id = '" . $sql_produit['products_id'] . "' 
										AND 
											pa.options_values_id = pov.products_options_values_id 
										AND 
											pa.options_id = po.products_options_id 
										AND 
											(pa.options_dispo = '1' OR (pa.options_dispo = '0' AND pa.options_quantity_reel > 0))
											
										ORDER BY CAST(pa.products_options_sort_order AS UNSIGNED) ASC");
									
									
		// on controle s'il y a des options dans le produit
		$option_disponible = tep_db_num_rows($query_option)	;
		if ($option_disponible>=1) { // s'il y a des options, on affiche pas les prix, ni la quantit� dans le produit
			$write = '';	
			$write .= getReplace($sql_produit['products_model']) . ';' ;
			$write .= getReplace($sql_produit['products_name']) . ';' ; 
			$write .= '' . ';' ; 
			$write .= '' . ';' ; 
			$write .= '' . ';' ; 
			$write .= '' . ';' ; 
			fwrite( $fp , $write . "\n");
		} else { // sinon on affiche les prix et la quantit�			
			$write = '';	
			$write .= getReplace($sql_produit['products_model']) . ';' ;
			$write .= getReplace($sql_produit['products_name']) . ';' ; 
			$write .= '' . ';' ;
			$write .= $sql_produit['products_quantity'] . ';' ; 
			$write .= number_format($sql_produit['products_cost'], 2, ',', '') . ' �;' ; 
			$write .= number_format($sql_produit['products_quantity'] * $sql_produit['products_cost'], 2, ',', '') . ' �;' ; 
			fwrite( $fp , $write . "\n");
		}
		
		// Boucle pour afficher les options d'un produit s'il en a		
		while ( $sql_option = tep_db_fetch_array($query_option) ) {
			$write = '';
			$write .= '' . ';' ;  
			$write .= '' . ';' ;  
			$write .= '' . $sql_option['products_options_values_name'] . ';' ;  
			$write .= $sql_option['options_quantity_reel'] . ';' ;
			$write .= number_format($sql_produit['products_cost'], 2, ',', '') . ' �;' ;  
			$write .= number_format($sql_option['options_quantity_reel'] * $sql_produit['products_cost'], 2, ',', '') . ' �;' ;
			fwrite( $fp , $write . "\n");
		}
	}
	
	// Ligne vide
	$write = '';	
	$write .= '' . ';' ; 
	$write .= '' . ';' ; 
	$write .= '' . ';' ; 
	$write .= '' . ';' ;
	$write .= '' . ';' ; 
	$write .= '' . ';' ; 
	fwrite( $fp , $write . "\n");
	
	// ligne de fin 
	//$write = '';	
	//$write .= 'Total' . ';' ; 
	//$write .= '' . ';' ; 
	//$write .= '' . ';' ;
	//$write .= '' . ';' ; 
	//$write .= number_format($total, 2, ',', ''); 
	//fwrite( $fp , $write . "\n");
	
?>