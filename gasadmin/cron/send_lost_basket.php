<?php
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");
tep_db_connect();
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/html_output.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/fonctions/mail.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/configure_mail.php");

$cust_ses_ids = _GetCustomerSessions();
$tdate = 2;// pour sélectionner les paniers perdus de la veille
$ndate = seadate($tdate);

$query = "select distinct(cus.customers_id), cus.customers_email_address
											 from   ". TABLE_CUSTOMERS_BASKET ." cb, ". TABLE_CUSTOMERS ." cus left join ". TABLE_ADDRESS_BOOK ." a on cus.customers_default_address_id = a.address_book_id
											 where  cb.customers_basket_date_added = '". $ndate ."'
												and	cus.customers_id not in ('" . implode(", ", $cust_ses_ids) . "') and cb.customers_id = cus.customers_id
												and cus.customers_id not in (select customers_id from ". TABLE_SCART .")
											order by cb.customers_basket_date_added desc, cb.customers_id";

$query = tep_db_query($query);

while($data = tep_db_fetch_array($query)) {
	
	envoi_mail_lost_basket($data['customers_id']);
}

function seadate($day) {
	
	$rawtime = strtotime("-".$day." days");
	$ndate = date("Ymd", $rawtime);
	return $ndate;
}

// Returns an empty array if the check sessions flag is not true (empty array means same SQL statement can be used)
function _GetCustomerSessions() {
	
	$cust_ses_ids = array();
	
	//if( RCS_CHECK_SESSIONS == 'true' ){
		//if (STORE_SESSIONS == 'mysql'){
			$sesquery = tep_db_query("select value from " . TABLE_SESSIONS . " where 1");
			while ($ses = tep_db_fetch_array($sesquery)){
				if ( ereg( "customer_id[^\"]*\"([0-9]*)\"", $ses['value'], $custval ) )
					$cust_ses_ids[] = $custval[1];
			}
		/*}
		else{
			if( $handle = opendir( tep_session_save_path())){
				while (false !== ($file = readdir( $handle ))){
					if ($file != "." && $file != ".."){
						$file = tep_session_save_path() . '/' . $file;	// create full path to file!
						if( $fp = fopen( $file, 'r' ) ){
							$val = fread( $fp, filesize( $file ) );
							fclose( $fp );
							if ( ereg( "customer_id[^\"]*\"([0-9]*)\"", $val, $custval ) )
								$cust_ses_ids[] = $custval[1];
						}
					}
				}
				closedir( $handle );
			}
		}*/
	//}
	return $cust_ses_ids;
}
?>