<?php
// VERSION GENERALARMYSTORE
// Les 2 watermark (350x350 et 800x800px) se trouve dans le dossier /gas/images/products_pictures
 		
	include('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php');
	include('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php');
	include('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php');
	include('/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php');

	tep_db_connect();
	
	// permet de calculer le temps d'ex�cution - partie 1 
	$mtime = microtime();
	$mtime = explode(" ",$mtime);
	$mtime = $mtime[1] + $mtime[0];
	$starttime = $mtime;

	// D�claration des variables 
	$compteur1 = 0;	
	
	/* ############################################## */
	/* ############################################## */
	/*   Pour le redimentionnement et le watermark    */
	/* ############################################## */
	/* ############################################## */
	
	// Boucle pour image principale 
	/*
	$query_image_principale = tep_db_query('SELECT products_bimage FROM products');
	while ($data_image = tep_db_fetch_array($query_image_principale)) {
	*/
	// Boucle pour image secondaire
	
	$query_image_secondaire = tep_db_query('SELECT products_bimage FROM products_image');
	// LIMIT :  WHERE products_id BETWEEN \'7999\' AND \'10000\'
	while ($data_image = tep_db_fetch_array($query_image_secondaire)) {
	
		// on incr�mente de 1 � chaque passage le nombre d'image trait�
		$compteur1++; 
		// On r�cup�re le nom de l'image dans la base
		$img = $data_image['products_bimage']; 	
		
		// avec la principale on traite les 5 tailles mais aves les secondaire on ne traite que les 800, 350, 53
		
		//resize_image_watermark($img,800,800,"grande/".$img);
		//resize_image_watermark($img,350,350,"normale/".$img);
		//resize_image_watermark($img,140,140,"petite/".$img);  // base 100x100
		//resize_image_watermark($img,100,100,"mini/".$img); // base 83x83
		resize_image_watermark($img,53,53,"micro/".$img); 
		
	}
	
	// permet de calculer le temps d'ex�cution - partie 2
	$mtime = microtime();
	$mtime = explode(" ",$mtime);
	$mtime = $mtime[1] + $mtime[0];
	$endtime = $mtime;
	$totaltime = ($endtime - $starttime);


	echo 'Traitement de '. $compteur1 .' images<br>';
	echo 'Page g�n�r�e en '.number_format($totaltime,4,',','').' secondes';
	
	
	
?>
