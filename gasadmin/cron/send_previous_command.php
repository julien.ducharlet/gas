<?php

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/configure.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/database.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/general.php");

tep_db_connect();

include("/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/functions/html_output.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/fonctions/mail.php");
include("/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/configure_mail.php");

$yesterday = date("Y-m-d", strtotime("-3 days"));
$customerProducts = array();

//orders_status = seules les commandes "en attente" sont s�lectionn�es
$query = 'select o1.orders_id, o1.orders_status, o1.date_purchased, o1.customers_id, o1.customers_name, o1.customers_email_address, o1.total from '. TABLE_ORDERS .' o1 ';
$query .= 'where (select count(*) 
							   from '. TABLE_ORDERS_STATUS_HISTORY .' osh, '. TABLE_ORDERS .' o2 
							   where osh.orders_id=o2.orders_id 
							   and o1.orders_id=o2.orders_id
							   and (o2.orders_status=6 or o2.orders_status=7 or o2.orders_status= 27 or o2.orders_status=29 or o2.orders_status=36)
					)=1 
			and o1.date_purchased like \''. $yesterday .'%\'';
$query = tep_db_query($query);

while($data = tep_db_fetch_array($query)) {
	
	$cmdTotalPrice = $data['total'];
	$products = array();
	$doubleCmd = false;
	$orderToDelete = 0;
	
	$query_products = 'select products_id from '. TABLE_ORDERS_PRODUCTS .' where orders_id='. $data['orders_id'];
	$query_products = tep_db_query($query_products);
	
	while($data_products = tep_db_fetch_array($query_products)) {
		
		$products[] = $data_products['products_id']; //on sauve tous les produits de la commande
	}
	
	//on v�rifie si les produits ont d�j� �t� command� par le m�me clients pour le m�me prix
	if(!empty($customerProducts[$data['customers_id']])) {
		//echo '<h3>';print_r($customerProducts);echo '</h3><hr><br>';
		foreach($customerProducts[$data['customers_id']]['orders'] as $orders_id => $orders) {
			
			if($orders['products']==$products && $cmdTotalPrice==$orders['total_price']) {
				
				$doubleCmd += true;
				$orderToDelete = $data['orders_id'];
			}
			else $doubleCmd += false;
		}
	}
	
	//d�sactivation de la suppression des commandes multiples
	/*if(!empty($customerProducts[$data['customers_id']]) && $doubleCmd) {//si la commande est pr�sente en double
		
		unset($customerProducts[$data['customers_id']]['orders'][$orderToDelete]);
		
		tep_db_query("delete from " . TABLE_ORDERS . " where orders_id = '" . $orderToDelete . "'");
		tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . $orderToDelete . "'");
		tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_id = '" . $orderToDelete . "'");
		tep_db_query("delete from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_id = '" . $orderToDelete . "'");
		tep_db_query("delete from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . $orderToDelete . "'");
	}
	else {*/
		
		$customerProducts[$data['customers_id']]['orders'][$data['orders_id']]['total_price'] = $data['total'];
		$customerProducts[$data['customers_id']]['orders'][$data['orders_id']]['products'] = $products;
		
		$date_order = substr($data['date_purchased'], 0, strpos($data['date_purchased'], ' '));
		$date_order = explode('-', $date_order);
		
		$date_order = array_reverse($date_order);
		$date_order = implode('/', $date_order);
		
		$sujet = NOM_DU_SITE .' - ';
		$message = 'Bonjour <span style="font-weight: bold; color:#AAB41D;">'. ucwords(strtolower($data['customers_name'])) .'</span>,<br /><br />';
		
		switch($data['orders_status']) {
			
			case 6: //ch�que
				
				$messageToSave = 'Le paiement par ch�que de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' ne nous est toujours pas parvenu.';
				
				$sujet .= 'En attente de votre cheque';
				
				$message .= 'Le paiement par ch�que de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' ne nous est toujours pas parvenu.<br />';
				$message .= 'Nous vous rappelons que cette commande ne sera exp�di�e qu\'apr�s r�ception de votre r�glement.<br /><br />';
				$message .= 'Merci de nous faire parvenir votre ch�que au plus vite afin que nous puissions traiter votre commande dans les meilleurs d�lais.<br />';
				
				$message .= '<hr style="color:#AAB41D;"><ul style="margin-bottom:0px;list-style-type: square;"><li>�tablir le ch�que � l\'ordre de : '. TITULAIRE .'</li>';
				$message .= '<li>L\'envoyer � : '. ADRESSE_LOCAUX .'</li></ul><hr style="color:#AAB41D;"><br />';
				
				$message .= 'Les commandes sont conserv�es 7 jours � compter de leur enregistrement. Pass� ce d�lai et sans r�ception de leur r�glement durant cette p�riode, elles sont purement et simplement annul�es.<br /><br />';
				
				$message .= 'Cet email peut avoir �t� envoy� apr�s que vous ayez post� votre ch�que et avant que nous ne l\'ayons re�u. Si tel est le cas, merci de ne pas tenir compte de ce message.';
				break;
			
			case 7://virement
				
				$messageToSave = 'Le paiement par virement de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' ne nous est toujours pas parvenu.';
				
				$sujet .= 'En attente de votre virement';
				
				$message .= 'Le paiement par virement de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' ne nous est toujours pas parvenu.<br />';
				$message .= 'Nous vous rappelons que cette commande ne sera exp�di�e qu\'apr�s r�ception de votre r�glement.<br /><br />';
				$message .= 'Merci de nous faire parvenir votre virement au plus vite afin que nous puissions traiter votre commande dans les meilleurs d�lais.<br />';
				
				
				$message .= '<hr style="color:#AAB41D;"><ul style="margin-bottom:0px;list-style-type: square;"><li>Code Etablissement : '. CODE_ETAB .'</li>';
				$message .= '<li>Code Guichet : '. CODE_GUICHET .'</li>';
				$message .= '<li>Num�ro de Compte : '. NUM_COMPTE .'</li>';
				$message .= '<li>Cl� RIB : '. RIB .'</li>';
				$message .= '<li>Domiciliation : '. DOMICILIATION .'</li>';
				$message .= '<li>Titulaire du Compte: '. TITULAIRE .'</li>';
				$message .= '<li>IBAN : '. IBAN .'</li>';
				$message .= '<li>Code SWIFT (BIC) : '. CODE_SWIFT .'</li></ul><hr style="color:#AAB41D;"><br />';
				
				$message .= 'Les commandes sont conserv�es 7 jours � compter de leur enregistrement. Pass� ce d�lai et sans r�ception de leur r�glement durant cette p�riode, elles sont purement et simplement annul�es.<br /><br />';
				
				$message .= 'Cet email peut avoir �t� envoy� apr�s que vous ayez effectu� votre virement et avant qu\'il ne nous ait �t� cr�dit�. Si tel est le cas, merci de ne pas tenir compte de ce message.';
				break;
				
			case 27://cb
				
				$messageToSave = 'Nous vous rappelons que le paiement par CB de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' n\'a pas �t� ent�rin�.';
				
				$sujet .= 'En attente de votre paiement CB';
				
				$message .= 'Nous vous rappelons que le paiement par CB de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' n\'a pas �t� ent�rin�.<br />';
				$message .= 'Par cons�quent, votre commande ne peut �tre trait�e et est actuellement en attente de validation.<br /><br />';
				
				$message .= 'Pour proc�der au r�glement, rendez vous sur notre site dans "Mon Compte", rubrique "Mes Commandes" et suivez les instructions.<br /><br />';
				
				$message .= 'Lors de la proc�dure de paiement par CB, vous serez redirig� vers le site s�curis� SPPLUS de notre partenaire la Caisse d\'Epargne.<br />';
				$message .= 'Apr�s avoir rempli les champs concernant votre Carte Bancaire, vous serez ensuite redirig� vers une page de votre banque.<br />';
				$message .= 'Cette page servant aux banques � contr�ler que vous �tes bien le titulaire de la carte utilis�e.<br /><br />';
				
				$message .= 'Si toutefois vous rencontrez toujours un probl�me de paiement par CB, vous pouvez nous envoyer un ch�que au dos duquel vous mentionnerez le num�ro de votre commande, ou bien proc�der � un virement bancaire.<br />';
				$message .= 'Si vous choisissez de changer votre mode de paiement, voici les informations n�cessaires pour effectuer le r�glement de votre commande :<br /><br />';
				
				$message .= '<u>Par Ch�que :</u><br /><br />';
				$message .= '<ul style="margin-bottom:0px;list-style-type: square;"><li>�tablir le ch�que � l\'ordre de : '. TITULAIRE .'</li>';
				$message .= '<li>L\'envoyer � : '. ADRESSE_LOCAUX .'</li></ul><hr style="color:#AAB41D;"><br />';
				
				$message .= '<u>Par Virement :</u><br /><br />';
				$message .= '<ul style="margin-bottom:0px;list-style-type: square;"><li>Code Etablissement : '. CODE_ETAB .'</li>';
				$message .= '<li>Code Guichet : '. CODE_GUICHET .'</li>';
				$message .= '<li>Num�ro de Compte : '. NUM_COMPTE .'</li>';
				$message .= '<li>Cl� RIB : '. RIB .'</li>';
				$message .= '<li>Domiciliation : '. DOMICILIATION .'</li>';
				$message .= '<li>Titulaire du Compte: '. TITULAIRE .'</li>';
				$message .= '<li>IBAN : '. IBAN .'</li>';
				$message .= '<li>Code SWIFT (BIC) : '. CODE_SWIFT .'</li></ul><hr style="color:#AAB41D;"><br />';
				
				$message .= 'Les commandes sont conserv�es 7 jours � compter de leur enregistrement. Pass� ce d�lai et sans r�ception de leur r�glement durant cette p�riode, elles sont purement et simplement annul�es.';
				break;
				
			
			case 29://mandat cash
				
				$messageToSave = 'Le paiement par mandat cash de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' ne nous est toujours pas parvenu.';
				
				$sujet .= 'En attente de votre mandat cash';
				
				$message .= 'Le paiement par mandat cash de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' ne nous est toujours pas parvenu.<br />';
				$message .= 'Nous vous rappelons que cette commande ne sera exp�di�e qu\'apr�s r�ception de votre r�glement.<br /><br />';
				$message .= 'Merci de nous faire parvenir votre mandat au plus vite afin que nous puissions traiter votre commande dans les meilleurs d�lais.';
				
				$message .= '<hr style="color:#AAB41D;"><ul style="list-style:square"><li>Faites �tablir le mandat � l\'ordre de : '. NOM_DU_SITE .'</li>';
				$message .= '<li>L\'envoyer � : '. ADRESSE_LOCAUX .'</li></ul>';
				$message .= '<hr style="color:#AAB41D;"><br />';
				
				$message .= 'Les commandes sont conserv�es 7 jours � compter de leur enregistrement. Pass� ce d�lai et sans r�ception de leur r�glement durant cette p�riode, elles sont purement et simplement annul�es.<br /><br />';
				
				$message .= 'Cet email peut avoir �t� envoy� apr�s que vous ayez post� votre mandat cash et avant que nous ne l\'ayons re�u. Si tel est le cas, merci de ne pas tenir compte de ce message.';
				break;
				
			case 36://PayPal
				
				$messageToSave = 'Le paiement par PayPal de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' ne nous est toujours pas parvenu.';
				
				$sujet .= 'En attente de votre paiement PayPal';
				
				$message .= 'Nous vous rappelons que le paiement par PayPal de votre commande num�ro '. $data['orders_id'] .' du '. $date_order .' n\'a pas �t� ent�rin�.<br />';
				$message .= 'Par cons�quent, votre commande ne peut �tre trait�e et est actuellement en attente de validation.<br /><br />';
				
				$message .= 'Pour proc�der au r�glement, rendez vous sur notre site dans "Mon Compte", rubrique "Mes Commandes" et suivez les instructions.<br /><br />';
				
				$message .= 'Lors de la proc�dure de paiement par PayPal, vous serez redirig� vers le site s�curis� de PayPal.<br />';
								
				$message .= 'Si toutefois vous rencontrez toujours un probl�me de paiement par par PayPal, vous pouvez nous envoyer un ch�que au dos duquel vous mentionnerez le num�ro de votre commande, ou bien proc�der � un virement bancaire.<br />';
				$message .= 'Si vous choisissez de changer votre mode de paiement, voici les informations n�cessaires pour effectuer le r�glement de votre commande :<br /><br />';
				
				$message .= '<u>Par Ch�que :</u><br /><br />';
				$message .= '<ul style="margin-bottom:0px;list-style-type: square;"><li>�tablir le ch�que � l\'ordre de : '. TITULAIRE .'</li>';
				$message .= '<li>L\'envoyer � : '. ADRESSE_LOCAUX .'</li></ul><hr style="color:#AAB41D;"><br />';
				
				$message .= '<u>Par Virement :</u><br /><br />';
				$message .= '<ul style="margin-bottom:0px;list-style-type: square;"><li>Code Etablissement : '. CODE_ETAB .'</li>';
				$message .= '<li>Code Guichet : '. CODE_GUICHET .'</li>';
				$message .= '<li>Num�ro de Compte : '. NUM_COMPTE .'</li>';
				$message .= '<li>Cl� RIB : '. RIB .'</li>';
				$message .= '<li>Domiciliation : '. DOMICILIATION .'</li>';
				$message .= '<li>Titulaire du Compte: '. TITULAIRE .'</li>';
				$message .= '<li>IBAN : '. IBAN .'</li>';
				$message .= '<li>Code SWIFT (BIC) : '. CODE_SWIFT .'</li></ul><hr style="color:#AAB41D;"><br />';
				
				$message .= 'Les commandes sont conserv�es 7 jours � compter de leur enregistrement. Pass� ce d�lai et sans r�ception de leur r�glement durant cette p�riode, elles sont purement et simplement annul�es.';
				break;
		}
		
		$message .= '<br /><br />'. SIGNATURE_MAIL;
		
		mail_client_commande($data['customers_email_address'], $sujet, $message, MAIL_INFO);
		$query_update = 'insert into '. TABLE_ORDERS_STATUS_HISTORY .'(orders_id, orders_status_id, date_added, customer_notified, comments)
		values('. $data['orders_id'] .', '. $data['orders_status'] .', \''. date('Y-m-d H:i:s') .'\', 1, \''. addslashes($messageToSave) .'\')';
		tep_db_query($query_update);
		//mail_client_commande('thierry@pimentbleu.fr', $sujet, $message, MAIL_INFO);
	//}
}