<?php
/*
  $Id: stats_customers_orders.php,v 1.2b 11 dec 2005 

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2005 osCommerce

  originally developed by xaglo v1.2 24 mars 2005
  
  Released under the GNU General Public License
*/

$list_mini = array("1", "2", "3", "4", "5", "10", "20");

require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

// detecte les constantes ou initialise
$today = getdate();
if ($HTTP_GET_VARS['yearcustord']) $yearcustord = tep_db_prepare_input($HTTP_GET_VARS['yearcustord']); 
else $yearcustord = $today['year']; //  else $year = 'ALL';
if ($HTTP_GET_VARS['monthcustord']) $monthcustord = tep_db_prepare_input($HTTP_GET_VARS['monthcustord']); 
else $monthcustord = $today['mon']; //  else $monthcustord = 'ALL';
if ($HTTP_GET_VARS['mini_ordered']) $mini_ordered = tep_db_prepare_input($HTTP_GET_VARS['mini_ordered']); 
else $mini_ordered = 1;
if ($HTTP_GET_VARS['no_status']) $no_status = tep_db_prepare_input($HTTP_GET_VARS['no_status']); 
if ($HTTP_GET_VARS['status']) $status = tep_db_prepare_input($HTTP_GET_VARS['status']); 

  
//  get list of years for dropdown selection
$yearcustord_begin_query = tep_db_query(" select startdate from counter");
$yearcustord_begin = tep_db_fetch_array($yearcustord_begin_query);
$yearcustord_begin = substr($yearcustord_begin['startdate'], 0, 4);
$current_yearcustord = $yearcustord_begin;
while ($current_yearcustord != $today['year'] + 1) {
	$list_yearcustord_array[] = array('id' => $current_yearcustord, 'text' => $current_yearcustord);
	$current_yearcustord++;
}

//  get list of monthtest for dropdown selection
$list_monthcustord = array('Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao�t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre');
for ($i = 0, $n = sizeof($list_monthcustord); $i < $n; $i++) {
	$list_monthcustord_array[] = array('id' => $i+1, 'text' => $list_monthcustord[$i]);
}

// get list of minimum names for dropdown selection
for ($i = 0, $n = sizeof($list_mini); $i < $n; $i++) {
	$list_mini_array[] = array('id' => $list_mini[$i], 'text' => $list_mini[$i]);
}


// get list of orders_status names for dropdown selection
$orders_statuses = array();
$orders_status_array = array();
$orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . $languages_id . "'");
while ($orders_status = tep_db_fetch_array($orders_status_query)) {
	$orders_statuses[] = array('id' => $orders_status['orders_status_id'], 'text' => $orders_status['orders_status_name']);
    $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
}

// Total clients inscrits
$total_clients_query_raw = "select count(customers_info_id) as tot_new_customers from " . TABLE_CUSTOMERS_INFO . "";
$total_clients_query_raw = tep_db_query($total_clients_query_raw);
$total_clients_array = tep_db_fetch_array($total_clients_query_raw);
$total_clients_count = $total_clients_array['tot_new_customers'];

// Total new_customers dans la periode
$new_customers_query_raw = "select count(customers_info_id) as tot_new_customers from " . TABLE_CUSTOMERS_INFO . " where 1=1";
if ($monthcustord != 'ALL') $new_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord ;
if ($yearcustord != 'ALL') $new_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
$new_customers_query = tep_db_query($new_customers_query_raw);
$new_customers = tep_db_fetch_array($new_customers_query);
$new_customers_count = $new_customers['tot_new_customers'];
 
// Total customers avant periode
if($monthcustord<10) $annee=$yearcustord."0" . $monthcustord ."01";
else $annee=$yearcustord."0" . $monthcustord ."01"; 
$new_customers_query_raw = "select count(customers_info_id) as tot_avant_customers from " . TABLE_CUSTOMERS_INFO . " where 1=1";
if (($monthcustord != 'ALL')&&($yearcustord != 'ALL')) $new_customers_query_raw .= " and DATE_FORMAT(customers_info_date_account_created,'%Y%m%d') < " . $annee."";
if (($monthcustord == 'ALL')&&($yearcustord != 'ALL')) $new_customers_query_raw .= " and YEAR(customers_info_date_account_created) < " . $yearcustord;
$new_customers_query = tep_db_query($new_customers_query_raw);
$new_customers = tep_db_fetch_array($new_customers_query);
$avant_customers_count = $new_customers['tot_avant_customers'];

//* Total distinct customers
$customers_query_raw = "select distinct(customers_id) from " . TABLE_ORDERS . " where 1=1";
if ($monthcustord != 'ALL') $customers_query_raw .= " and MONTH(date_purchased) = " . $monthcustord ;
if ($yearcustord != 'ALL') $customers_query_raw .= " and YEAR(date_purchased) = " . $yearcustord;
if ($no_status) $customers_query_raw .= " and orders_status <> " . $no_status;
if ($status) $customers_query_raw .= " and orders_status = " . $status;
$customers_query = tep_db_query($customers_query_raw);
$customers_id_array = array();
while ($customers = tep_db_fetch_array($customers_query)) {
	$customers_id_array[] = $customers['customers_id']; 
}
$customers_count = sizeof($customers_id_array);

//* Total new_customers_bought
$new_customers_bought_query_raw = "select distinct(o.customers_id) 
  								   from " . TABLE_ORDERS . " o, " . TABLE_CUSTOMERS_INFO . " ci 
  								   where ci.customers_info_id = o.customers_id";
if ($monthcustord != 'ALL') $new_customers_bought_query_raw .= " and MONTH(o.date_purchased) = " . $monthcustord . " and MONTH(ci.customers_info_date_account_created) = " . $monthcustord ;
if ($yearcustord != 'ALL') $new_customers_bought_query_raw .= " and YEAR(o.date_purchased) = " . $yearcustord .  " and YEAR(ci.customers_info_date_account_created) = " . $yearcustord;
if ($no_status) $new_customers_query_raw .= " and o.orders_status <> " . $no_status;
if ($status) $new_customers_query_raw .= " and o.orders_status = " . $status;
$new_customers_bought_query = tep_db_query($new_customers_bought_query_raw);
$new_customers_bought_id_array = array();
while ($new_customers_bought = tep_db_fetch_array($new_customers_bought_query)) {
	$new_customers_bought_id_array[] = $new_customers_bought['customers_id']; 
}
$new_customers_bought_count = sizeof($new_customers_bought_id_array);
if ($new_customers_bought_count > 0) $new_customers_bought_percent = tep_round($new_customers_bought_count/$new_customers_count*100, 0);

//* Total customers_bought
$customers_bought_query_raw = "select customers_id from " . TABLE_ORDERS;
if ($no_status) $customers_query_raw .= " where orders_status <> " . $no_status;
if ($status) $customers_query_raw .= " where orders_status = " . $status;
$customers_bought_query = tep_db_query($customers_bought_query_raw);
$customers_bought_id_array = array();
while ($customers_bought = tep_db_fetch_array($customers_bought_query)) { 
	$customers_bought_id_array[] = $customers_bought['customers_id']; 
}

$count_customers_again = 0;
foreach($customers_id_array as $value) {
	$key = sizeof(array_keys($customers_bought_id_array, $value));
    if ($key>$mini_ordered) $count_customers_again++;
}
if ($customers_count > 0) $percent_customers_again = tep_round($count_customers_again/$customers_count*100, 0);

//* Total orders
$orders_query_raw = "select count(*) as total from " . TABLE_ORDERS . " where 1=1 ";
if ($monthcustord != 'ALL') $orders_query_raw .= " and MONTH(date_purchased) = " . $monthcustord ;
if ($yearcustord != 'ALL') $orders_query_raw .= " and YEAR(date_purchased) = " . $yearcustord;
if ($no_status) $orders_query_raw .= " and orders_status <> " . $no_status;
if ($status) $orders_query_raw .= " and orders_status = " . $status;
$orders_query = tep_db_query($orders_query_raw);
$orders = tep_db_fetch_array($orders_query);
$count_orders = $orders['total'];

//* Total des ventes avec frais de port
$tot_sale_query_raw = "select sum(o.total) as total, count(o.total) as count 
from " . TABLE_ORDERS . " o 
where 1";
if ($monthcustord != 'ALL') $tot_sale_query_raw .= " and MONTH(o.date_purchased) = " . $monthcustord ;
if ($yearcustord != 'ALL') $tot_sale_query_raw .= " and YEAR(o.date_purchased) = " . $yearcustord;
if ($no_status) $tot_sale_query_raw .= " and o.orders_status <> " . $no_status;
if ($status) $tot_sale_query_raw .= " and o.orders_status = " . $status;
$tot_sale_query = tep_db_query($tot_sale_query_raw);
$tot_sale = tep_db_fetch_array($tot_sale_query);
$chiffre_affaire_avec_port=$tot_sale['total'];

//* Total des ventes sans frais de port
$tot_sale_query_raw = "select sum(o.ss_total) as total, count(o.ss_total) as count 
from " . TABLE_ORDERS . " o 
where 1";
if ($monthcustord != 'ALL') $tot_sale_query_raw .= " and MONTH(o.date_purchased) = " . $monthcustord ;
if ($yearcustord != 'ALL') $tot_sale_query_raw .= " and YEAR(o.date_purchased) = " . $yearcustord;
if ($no_status) $tot_sale_query_raw .= " and o.orders_status <> " . $no_status;
if ($status) $tot_sale_query_raw .= " and o.orders_status = " . $status;
$tot_sale_query = tep_db_query($tot_sale_query_raw);
$tot_sale = tep_db_fetch_array($tot_sale_query);
$chiffre_affaire_sans_port=$tot_sale['total'];
  
//* Total taxes TVA sur les commandes
$tot_taxes_query_raw = "select sum(o.tva_total) as total 
from " . TABLE_ORDERS . " o
where 1";
if ($monthcustord != 'ALL') $tot_taxes_query_raw .= " and MONTH(date_purchased) = " . $monthcustord ;
if ($yearcustord != 'ALL') $tot_taxes_query_raw .= " and YEAR(date_purchased) = " . $yearcustord;
if ($no_status) $tot_taxes_query_raw .= " and orders_status <> " . $no_status;
if ($status) $tot_taxes_query_raw .= " and orders_status = " . $status;
$tot_taxes_query = tep_db_query($tot_taxes_query_raw);
$tot_taxes = tep_db_fetch_array($tot_taxes_query);

//* Total shipping frais d'envoi sur les commandes
$tot_shipping_query_raw = "select sum(o.frais_port_client) as total 
from " . TABLE_ORDERS . " o 
where 1";
if ($monthcustord != 'ALL') $tot_shipping_query_raw .= " and MONTH(date_purchased) = " . $monthcustord ;
if ($yearcustord != 'ALL') $tot_shipping_query_raw .= " and YEAR(date_purchased) = " . $yearcustord;
if ($no_status) $tot_shipping_query_raw .= " and orders_status <> " . $no_status;
if ($status) $tot_shipping_query_raw .= " and orders_status = " . $status;
$tot_shipping_query = tep_db_query($tot_shipping_query_raw);
$tot_shipping = tep_db_fetch_array($tot_shipping_query);
$frais_de_portTTC=$tot_shipping['total'];
$TVA_sur_frais_port=20/100*$frais_de_portTTC;

//* Total products  vendus TTC
$tot_products_query_raw = "select sum(op.final_price*products_quantity) as total, sum(products_cost*products_quantity) as prix_achatTTC
  from " . TABLE_ORDERS . " o, " . TABLE_ORDERS_PRODUCTS . " op
  where o.orders_id=op.orders_id";
  if ($monthcustord != 'ALL') $tot_products_query_raw .= " and MONTH(date_purchased) = " . $monthcustord ;
  if ($yearcustord != 'ALL') $tot_products_query_raw .= " and YEAR(date_purchased) = " . $yearcustord;
  if ($no_status) $tot_products_query_raw .= " and orders_status <> " . $no_status;
  if ($status) $tot_products_query_raw .= " and orders_status = " . $status;
  $tot_products_query = tep_db_query($tot_products_query_raw);
  $tot_products = tep_db_fetch_array($tot_products_query);

$marge_query_raw = "select sum(marge) as margeHT, sum(nb_articles) as total_products from orders where 1=1";
if ($monthcustord != 'ALL') $marge_query_raw .= " and MONTH(date_purchased) = " . $monthcustord ;
if ($yearcustord != 'ALL') $marge_query_raw .= " and YEAR(date_purchased) = " . $yearcustord;
if ($no_status) $marge_query_raw .= " and orders_status <> " . $no_status;
if ($status) $marge_query_raw .= " and orders_status = " . $status;

$marge_query = tep_db_query($marge_query_raw);
$marge_products = tep_db_fetch_array($marge_query);
$marge_HT=$marge_products['margeHT'];
$count_orders_products = $marge_products['total_products'];
  
//* Total articles
$orders_products_query_raw = "select sum(op.products_quantity) as total,sum(round(final_price*(products_tax/100+1),2)) as prix_venteTTC,
sum(round(products_cost*(products_tax/100+1),2)) as prix_achatTTC 
from " . TABLE_ORDERS_PRODUCTS . " op,  " . TABLE_ORDERS . " o 
where o.orders_id=op.orders_id ";
if ($monthcustord != 'ALL') $orders_products_query_raw .= " and MONTH(o.date_purchased) = " . $monthcustord;
if ($yearcustord != 'ALL') $orders_products_query_raw .= " and YEAR(o.date_purchased) = " . $yearcustord;
if ($no_status) $orders_products_query_raw .= " and o.orders_status <> " . $no_status;
if ($status) $orders_products_query_raw .= " and o.orders_status = " . $status;
$orders_products_query = tep_db_query($orders_products_query_raw);
$orders_products = tep_db_fetch_array($orders_products_query);
if ($count_orders_products=='') $count_orders_products=0;
$prix_achat_totalTTC=$orders_products['prix_achatTTC'];
$prix_vente_totalTTC=$orders_products['prix_venteTTC'];



//MODIFICATION POLYTECH
//nombre d'articles par commande
$orders_products_query_raw2 = "SELECT sum(op.products_quantity) as sum 
from " . TABLE_ORDERS_PRODUCTS . " op,  " . TABLE_ORDERS . " o 
where o.orders_id=op.orders_id ";
if ($monthcustord != 'ALL') $orders_products_query_raw2 .= " and MONTH(o.date_purchased) = " . $monthcustord;
if ($yearcustord != 'ALL') $orders_products_query_raw2 .= " and YEAR(o.date_purchased) = " . $yearcustord;
if ($no_status) $orders_products_query_raw2 .= " and o.orders_status <> " . $no_status;
if ($status) $orders_products_query_raw2 .= " and o.orders_status = " . $status;
$orders_products_query_raw2 .=" group by op.orders_id order by sum asc";
$orders_products_query2 = tep_db_query($orders_products_query_raw2);

$i=0;
$tableau_nb_article=array();
while($orders_products2 = tep_db_fetch_array($orders_products_query2)){
$tableau_nb_article[$i]=$orders_products2['sum'];
$i+=1;
}
//tableau permettant de connaitre le nombre de commandes pour un nombre d'articles indices par le nombre d'article
$tableau_nb_article2 = array_count_values($tableau_nb_article);



//nombre de clients hommes et age moyen
  $man_customers_query_raw = "select count(c.customers_id) as total_count_customers_man,avg(YEAR( CURRENT_DATE ) - YEAR( c.customers_dob )) AS age_man 
  from " . TABLE_CUSTOMERS . " c," . TABLE_ORDERS . " o 
  where c.customers_gender='m' and c.customers_id=o.customers_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(o.date_purchased) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(o.date_purchased) = " . $yearcustord;
  if ($no_status) $man_customers_query_raw .= " and o.orders_status <> " . $no_status;
  if ($status) $man_customers_query_raw .= " and o.orders_status = " . $status;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_man = tep_db_fetch_array($man_customers_query);
  $count_customers_man = $array_customers_man['total_count_customers_man'];
  $age_customers_man = round($array_customers_man['age_man']);
  
//nombre de clients femmes et age moyen
  $woman_customers_query_raw = "select count(c.customers_id) as total_count_customers_woman,avg(YEAR( CURRENT_DATE ) - YEAR( c.customers_dob )) AS age_woman
  from " . TABLE_CUSTOMERS . " c," . TABLE_ORDERS . " o 
  where customers_gender='f' and c.customers_id=o.customers_id";
  if ($monthcustord != 'ALL') $woman_customers_query_raw .= " and MONTH(o.date_purchased) = " . $monthcustord;
  if ($yearcustord != 'ALL') $woman_customers_query_raw .= " and YEAR(o.date_purchased) = " . $yearcustord;
  if ($no_status) $woman_customers_query_raw .= " and o.orders_status <> " . $no_status;
  if ($status) $woman_customers_query_raw .= " and o.orders_status = " . $status;
  $woman_customers_query = tep_db_query($woman_customers_query_raw);
  $array_customers_woman = tep_db_fetch_array($woman_customers_query);
  $count_customers_woman = $array_customers_woman['total_count_customers_woman'];
  $age_customers_woman = round($array_customers_woman['age_woman']);

//pourcentage d'hommes et de femmes
  $total_clients=($count_customers_man+$count_customers_woman);
  if($total_clients !=0){ 
  $percent_man=round(($count_customers_man / $total_clients)*100);
  $percent_woman=round(($count_customers_woman/$total_clients)*100);
	}
  
//nombre  d'hommes  de moins de 20 ans
  $man_customers_query_raw = "select count(customers_id) as total
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='m' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))<=20 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_moins20 = tep_db_fetch_array($man_customers_query);
  $age_customers_man_moins20 = $array_customers_moins20['total'];
//nombre   de femmes  de moins de 20 ans
  $man_customers_query_raw = "select count(customers_id) as total
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='f' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))<=20 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_moins20 = tep_db_fetch_array($man_customers_query);
  $age_customers_woman_moins20 = $array_customers_moins20['total'];
//nombre  d'hommes  de 21 ans a 30 ans
  $man_customers_query_raw = "select count(customers_id) as total 
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='m' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))>20 and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))<=30 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_21_30 = tep_db_fetch_array($man_customers_query);
  $age_customers_man_21_30 = $array_customers_21_30['total'];
//nombre   de femmes  de 21 a 30 ans
  $man_customers_query_raw = "select count(customers_id) as total 
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='f' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))>20 and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))<=30 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_21_30 = tep_db_fetch_array($man_customers_query);
  $age_customers_woman_21_30 = $array_customers_21_30['total'];
  
//nombre  d'hommes  de 31 ans a 40 ans
  $man_customers_query_raw = "select count(customers_id) as total 
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='m' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))>30 and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))<=40 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_31_40 = tep_db_fetch_array($man_customers_query);
  $age_customers_man_31_40 = $array_customers_31_40['total'];
//nombre   de femmes  de 31 a 40 ans
  $man_customers_query_raw = "select count(customers_id) as total 
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='f' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))>30 and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))<=40 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_31_40 = tep_db_fetch_array($man_customers_query);
  $age_customers_woman_31_40 = $array_customers_31_40['total'];
  
//nombre  d'hommes  de 41 ans a 50 ans
  $man_customers_query_raw = "select count(customers_id) as total 
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='m' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))>40 and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))<=50 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_41_50 = tep_db_fetch_array($man_customers_query);
  $age_customers_man_41_50 = $array_customers_41_50['total'];
//nombre   de femmes  de 41 a 50 ans
  $man_customers_query_raw = "select count(customers_id) as total 
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='f' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))>40 and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))<=50 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_41_50 = tep_db_fetch_array($man_customers_query);
  $age_customers_woman_41_50 = $array_customers_41_50['total'];

//nombre  d'hommes  de plus de 50 ans
  $man_customers_query_raw = "select count(customers_id) as total 
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='m' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))>50 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_plus_50 = tep_db_fetch_array($man_customers_query);
  $age_customers_man_plus_50 = $array_customers_plus_50['total'];
//nombre   de femmes  de plus de 50 ans
  $man_customers_query_raw = "select count(customers_id) as total 
  from " . TABLE_CUSTOMERS . " ," . TABLE_CUSTOMERS_INFO . "
  where customers_gender='f' and (YEAR( CURRENT_DATE ) - YEAR( `customers_dob` ))>50 and customers_id=customers_info_id";
  if ($monthcustord != 'ALL') $man_customers_query_raw .= " and MONTH(customers_info_date_account_created) = " . $monthcustord;
  if ($yearcustord != 'ALL') $man_customers_query_raw .= " and YEAR(customers_info_date_account_created) = " . $yearcustord;
  $man_customers_query = tep_db_query($man_customers_query_raw);
  $array_customers_plus_50 = tep_db_fetch_array($man_customers_query);
  $age_customers_woman_plus_50 = $array_customers_plus_50['total'];  
 

 
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td class="pageHeading"><?php echo 'Les Statistiques clients'; ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
        <tr>
        <td>
		<table border="0" cellspacing="0" cellpadding="0"><?php echo tep_draw_form('search', FILENAME_STATS_CUSTOMERS_ORDERS, '', 'get'); ?>
          <tr>
            <td class="dataTableContent" align="center"><?php echo 'Afficher pour le mois :'; ?>&nbsp;</td>
            <td class="dataTableContent" align="center"><?php echo 'Afficher pour l\'ann&eacute;e :'; ?>&nbsp;</td>
            <td class="dataTableContent" align="center"><?php //echo 'Nombre d\'achats<br>minimum :'; ?>&nbsp;</td>
<?php if ($status == '') { ?>
            <td class="dataTableContent" align="center"><?php echo 'Masquer les commandes :'; ?>&nbsp;</td>
<?php } if ($no_status == '') { ?>
            <td class="dataTableContent" align="center"><?php echo 'N\'afficher que les commandes :'; ?></td>
<?php } ?>
          </tr>
          <tr>
            <td class="main" align="center">
			<?php 
			echo tep_draw_pull_down_menu('monthcustord', array_merge(array(array('id' => 'ALL', 'text' => 'Tous les mois')), $list_monthcustord_array),'', 'onChange="this.form.submit();"');?>&nbsp;</td>
            <td class="main" align="center">
			<?php echo tep_draw_pull_down_menu('yearcustord', array_merge(array(array('id' => 'ALL', 'text' => 'Toutes les ann&eacute;es')), $list_yearcustord_array), '', 'onChange="this.form.submit();"'); ?>&nbsp;</td>
            <td class="main" align="center">
			<?php //echo tep_draw_pull_down_menu('mini_ordered', $list_mini_array, '', 'onChange="this.form.submit();"'); ?>&nbsp;</td>
			<?php if ($status == '') { ?>
            <td class="main" align="center"><?php echo tep_draw_pull_down_menu('no_status', array_merge(array(array('id' => '', 'text' => 'Aucune')), $orders_statuses), '', 'onChange="this.form.submit();"'); ?>&nbsp;</td>
			<?php } if ($no_status == '') { ?>
            <td class="main" align="center"><?php echo tep_draw_pull_down_menu('status', array_merge(array(array('id' => '', 'text' => 'Toutes')), $orders_statuses), '', 'onChange="this.form.submit();"'); ?></td>
			<?php } ?>
          </tr>
		</table>
		</td>
		</tr>
      
		
          <tr>  
		  <td><table border="0" cellspacing="0" cellpadding="0" width="100%">
		  
		  
		  <td>
		  <table width="80%" border="0" cellspacing="0" cellpadding="5" align="left">
		  <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr class="dataTableHeadingRow">
            <td align="left" class="dataTableHeadingContent"><?php echo "COMMANDES"; ?></td>
            <td class="dataTableContent" align="center"><?php echo "Total"; ?></td>
			<td class="dataTableContent" align="center"><?php echo "%"; ?></td>
          </tr>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "nombre de commandes :"; ?></td>
            <td class="dataTableContent" align="center"><?php echo $count_orders; ?></td>
			<td class="dataTableContent" align="center"><?php echo ""; ?></td>
          </tr>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "clients d�j� inscrits :"; ?></td>
            <td class="dataTableContent" align="center"><?php echo $avant_customers_count; ?></td>
            <td class="dataTableContent" align="center"><?php if($total_clients_count!=0) echo round($avant_customers_count/$total_clients_count*100)."%"; ?></td>
		  </tr>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "clients inscrits dans la p�riode";?></td>
            <td align="center" class="dataTableContent"><?php echo $new_customers_count;?></td>
			<td class="dataTableContent" align="center"><?php  if($total_clients_count!=0) echo round($new_customers_count/$total_clients_count*100)."%"; ?></td>
          </tr>
          <tr class="dataTableHeadingRow">
            <td align="left" class="dataTableHeadingContent"><?php echo "CHIFFRES"; ?></td>
            <td class="dataTableContent" align="center"><?php echo "HT"; ?></td>
          <td class="dataTableContent" align="center"><?php echo "TTC"; ?></td>
		  </tr>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
			<td align="right" class="dataTableContent"><?php echo "Chiffre d'affaire (avec frais de port):"; ?></td>
			<td align="center" class="dataTableContent"><?php echo $currencies->format($chiffre_affaire_avec_port/1.2);  ?></td>           
            <td align ="center" class="dataTableContent"><?php echo $currencies->format($chiffre_affaire_avec_port); ?></td>
          </tr>
		   <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
			<td align="right" class="dataTableContent"><?php echo "Chiffre d'affaire (sans frais de port):"; ?></td>
			<td align="center" class="dataTableContent"><?php echo $currencies->format($chiffre_affaire_sans_port/1.2);  ?></td>           
            <td align ="center" class="dataTableContent"><?php echo $currencies->format($chiffre_affaire_sans_port); ?></td>
          </tr>
		    <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "Frais de port :"; ?></td>
			<td align ="center"  class="dataTableContent"><?php echo $currencies->format($chiffre_affaire_avec_port/1.2-$chiffre_affaire_sans_port/1.2); ?></td>
			<td align ="center"  class="dataTableContent"><?php echo $currencies->format($chiffre_affaire_avec_port-$chiffre_affaire_sans_port); ?></td>
		  </tr>
		   <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "Panier moyen (avec frais de port) :"; ?></td>
			<td class="dataTableContent" align="center"><?php  if ($count_orders > 0) echo $currencies->format(($chiffre_affaire_avec_port/$count_orders)/1.2);?></td>
            <td align ="center"  class="dataTableContent"><?php if ($count_orders > 0) echo $currencies->format($chiffre_affaire_avec_port/$count_orders); ?></td>
          </tr>
		      <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "Panier moyen (sans frais de port) :"; ?></td>
            <td align ="center"  class="dataTableContent"><?php if ($count_orders > 0) echo $currencies->format(($chiffre_affaire_sans_port/$count_orders)/1.2); ?></td>
            <td align="center" class="dataTableContent"><?php if ($count_orders > 0) echo $currencies->format($chiffre_affaire_sans_port/$count_orders);  ?></td>
		  </tr>
		    <tr class="dataTableHeadingRow">
            <td align="left" class="dataTableHeadingContent"><?php echo "DIVERS"; ?></td>
            <td class="dataTableContent" align="center"><?php echo "Total"; ?></td>
			<td class="dataTableContent" align="center"><?php echo ""; ?></td>			
		  </tr>
		  <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
           <td align="right" class="dataTableContent"><?php echo "marge :"; ?></td>
           <td align ="center"  class="dataTableContent"><?php echo $currencies->format($marge_HT); ?></td>
		   <td align="center" class="dataTableContent"><?php echo ""; ?></td>
         </tr>   
		  <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
           <td align="right" class="dataTableContent"><?php echo 'Article Moyen par Panier :'; ?></td>
           <td align ="center"  class="dataTableContent"><?php if  ($count_orders!=0)
		   echo tep_round(($count_orders_products/$count_orders),2); ?></td>
		   <td align="center" class="dataTableContent"><?php echo ""; ?></td>
         </tr>        
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "TVA :"; ?></td>
            <td align ="center"  class="dataTableContent"><?php echo $currencies->format($chiffre_affaire_avec_port-$chiffre_affaire_avec_port/1.2); ?></td>
          <td align ="center"  class="dataTableContent"><?php echo ""; ?></td>
		  </tr>
		    <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo 'Nouveaux clients ayant achet&eacute; :'; ?></td>
            <td class="dataTableContent"><?php echo $new_customers_bought_count . ' (' . $new_customers_bought_percent . "%)"; ?></td>
			<td align ="center"  class="dataTableContent"><?php echo ""; ?></td>
		  </tr>
		  </table>
		 </td>
		
		<td>
		  <table  width="90%" border="0" cellspacing="0" cellpadding="5">
		  <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
		  <tr class="dataTableHeadingRow">
            <td align="left" class="dataTableHeadingContent" ><?php echo "CLIENTS "; ?></td>
            <td align="center" class="dataTableContent"><?php echo "Total"; ?></td>
			<td align="center" class="dataTableContent"><?php echo "%"; ?></td>
			<td align="center" class="dataTableHeadingContent"><?php echo ($count_customers_man+$count_customers_woman); ?></td>
          </tr>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "nombre d'hommes :"; ?></td>
            <td class="dataTableContent" align="center"><?php echo $count_customers_man; ?></td>
			<td class="dataTableContent" align="center"><?php  echo $percent_man." %";?></td>
          <td class="dataTableContent" align="center"><?php  echo "";?></td>
		  </tr>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "nombre de femmes :"; ?></td>
            <td class="dataTableContent" align="center"><?php echo $count_customers_woman; ?></td>
			<td class="dataTableContent" align="center"><?php  echo $percent_woman." %"; ?></td>
			<td class="dataTableContent" align="center"><?php  echo "";?></td>
		  </tr>
		  <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "�ge moyen des hommes :"; ?></td>
            <td class="dataTableContent" align="center"><?php echo $age_customers_man; ?></td>
			<td class="dataTableContent" align="center"><?php  echo "";?></td>
			<td class="dataTableContent" align="center"><?php  echo "";?></td>
          </tr>
		  <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "�ge moyen des femmes :"; ?></td>
            <td class="dataTableContent" align="center"><?php echo $age_customers_woman; ?></td>
			<td class="dataTableContent" align="center"><?php  echo "";?></td>
			<td class="dataTableContent" align="center"><?php  echo "";?></td>
          </tr>
		 <tr class="dataTableHeadingRow">
            <td align="left" class="dataTableHeadingContent"><?php echo "CLIENTS DU MOIS"; ?></td>
			<td align="left" class="dataTableContent" align="center"><?php echo "Homme"; ?></td>
			<td align="left" class="dataTableContent" align="center"><?php echo "Femme"; ?></td>
			<td align="left" class="dataTableContent" align="center"><?php echo "Total"; ?></td>
          </tr>
		 <? $total= ($age_customers_woman_moins20+$age_customers_man_moins20)?>
		  <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
           <td align="right" class="dataTableContent"><?php echo "moins de 20 ans"; ?></td>
           <td class="dataTableContent" align="center"><?php echo $age_customers_man_moins20;
		   if ($total>0) echo  ' ('.round($age_customers_man_moins20*100/$total).'%)';?></td>
			<td class="dataTableContent" align="center"><?php echo $age_customers_woman_moins20; 
		if ($total>0) echo  ' ('.round($age_customers_woman_moins20*100/$total).'%)';			?></td>
			<td class="dataTableContent" align="center"><?php echo ($age_customers_woman_moins20+$age_customers_man_moins20);?></td>
         </tr>
		  <? $total= ($age_customers_man_21_30+$age_customers_woman_21_30);?>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "21 � 30 ans"; ?></td>
           <td align="center" class="dataTableContent"><?php echo $age_customers_man_21_30;
			if ($total>0) echo  ' ('.round($age_customers_man_21_30*100/$total).'%)';		   ?></td>
			<td align="center" class="dataTableContent"><?php echo $age_customers_woman_21_30;
			if ($total>0) echo  ' ('.round($age_customers_woman_21_30*100/$total).'%)';		   ?></td>
			<td align="center" class="dataTableContent"><?php echo $age_customers_man_21_30+$age_customers_woman_21_30;  ?></td>
          </tr>
		  <? $total= ($age_customers_man_31_40+$age_customers_woman_31_40);?>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "31 � 40 ans"; ?></td>
           <td align="center" class="dataTableContent"><?php  echo $age_customers_man_31_40;
			if ($total>0) echo  ' ('.round($age_customers_man_31_40*100/$total).'%)';		   ?></td>
			<td align="center" class="dataTableContent"><?php echo $age_customers_woman_31_40;
			if ($total>0) echo  ' ('.round($age_customers_woman_31_40*100/$total).'%)';			?></td>
			<td align="center" class="dataTableContent"><?php  echo $age_customers_man_31_40+$age_customers_woman_31_40;  ?></td>
          </tr>
		  <? $total= ($age_customers_man_41_50+$age_customers_woman_41_50);?>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
           <td align="right" class="dataTableContent"><?php echo "41 � 50 ans"; ?></td>
           <td align="center" class="dataTableContent"><?php echo $age_customers_man_41_50;
		if ($total>0) echo  ' ('.round($age_customers_man_41_50*100/$total).'%)';		   ?></td>
			<td align="center" class="dataTableContent"><?php echo $age_customers_woman_41_50; 
		if ($total>0) echo  ' ('.round($age_customers_woman_41_50*100/$total).'%)';			?></td>
			<td align="center" class="dataTableContent"><?php echo $age_customers_woman_41_50+$age_customers_man_41_50;  ?></td>
          </tr>
		  <? $total= ($age_customers_man_plus_50+$age_customers_woman_plus_50);?>
          <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
           <td align="right" class="dataTableContent"><?php echo "plus de 50 ans"; ?></td>
           <td align="center" class="dataTableContent"><?php echo $age_customers_man_plus_50;
			if ($total>0) echo  ' ('.round($age_customers_man_plus_50*100/$total).'%)';			   ?></td>
			<td align="center" class="dataTableContent"><?php echo $age_customers_woman_plus_50; 
			if ($total>0) echo  ' ('.round($age_customers_woman_plus_50*100/$total).'%)';				?></td>
			<td align="center" class="dataTableContent"><?php echo $age_customers_man_plus_50+$age_customers_woman_plus_50;  ?></td>
          </tr>
		   </table>
		
		</td>
		 
		
		<td>
		<table  width="90%" border="0" cellspacing="0" cellpadding="5" align="left">
		  <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr class="dataTableHeadingRow">
            <td align="left" class="dataTableHeadingContent"><?php echo "ARTICLES"; ?></td>
            <td class="dataTableContent"><?php echo "Total"; ?></td>
          </tr>
		   <tr class="dataTableRow" onMouseOver="this.className='dataTableRowOver';this.style.cursor='hand'" onMouseOut="this.className='dataTableRow'">
            <td align="right" class="dataTableContent"><?php echo "nombre d'articles vendus :"; ?></td>
           <td class="dataTableContent" align="center"><?php echo $count_orders_products; ?></td>
          </tr>
		   <?php
		  foreach($tableau_nb_article2 as $cle => $valeur){
		  if ($cle==1) echo
		  "<tr class=dataTableRow onMouseOver=this.className=dataTableRowOver;this.style.cursor=hand onMouseOut=this.className=dataTableRow>
            <td align=right class=dataTableContent>nombre de commandes � ".$cle." article :</td>
            <td class=dataTableContent align=center> ".$valeur."</td></tr>";
		    else 
		    echo 
		  "<tr class=dataTableRow onMouseOver=this.className=dataTableRowOver;this.style.cursor=hand onMouseOut=this.className=dataTableRow>
            <td align=right class=dataTableContent>nombre de commandes � ".$cle." articles :</td>
            <td class=dataTableContent align=center> ".$valeur."</td></tr>";
			}?>
		</table>	
		</td>

</tr>  
		  </td></table>

		
		</td>
		
      </tr>
</table>
	</td>
<!-- body_text_eof //-->
  </tr>
</table>

<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
</body>
</html>
