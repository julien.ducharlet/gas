<?php


	require('includes/application_top.php');
	require('includes/functions/spplus_code_etat.php');
	require('includes/functions/spplus_pays_iso.php');
	require('../' . DIR_WS_SITE . '/includes/configure_mail.php');
	require('../' . DIR_WS_SITE . '/includes/fonctions/mail.php');
	
	require(DIR_WS_CLASSES . 'currencies.php');
	$currencies = new currencies();
	
if (isset($_POST) && !empty($_POST) && !empty($_GET['oID'])) {
	
	/* CALCUL DU NUMERO DE FACTURE */
	$check_invoice_counter_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'COMPTEUR_FACTURE'");
	$check_invoice_counter = tep_db_fetch_array($check_invoice_counter_query);
	$numero_facture = $check_invoice_counter['configuration_value'];
				
	$query = 'select * from '. TABLE_ORDERS .' where orders_id='. $_GET['oID'];
	$query = tep_db_query($query);
	
	$data = tep_db_fetch_array($query);
	
	
	$nb_articles = 0;
	$prix_HT = 0;
	$prix_TTC = 0;
	$marge = 0;
	$products_array = array();
	
	for($i=0 ; $i<$_POST['nbre_product'] ; $i++) {
		
		$products_id = $_POST['orders_products_'. $i];
		
		$products_array[$products_id]['nb'] = $_POST['products_qte_'. $products_id];
		$products_array[$products_id]['prix_achat'] = $_POST['prixAchatHT_'. $i];
		$products_array[$products_id]['prix'] = $_POST['prixHT_'. $i];
		$products_array[$products_id]['retour_stock'] = ($_POST['remise_stock_'. $products_id] == 'on') ? true : false;
	}
	
	
	foreach($products_array as $prod) {
		
		$nb_articles += $prod['nb'];
		
		if($prod['retour_stock']) {
			
			$marge += ($prod['prix']+$prod['prix_achat']) * $prod['nb'];
		}
		else {
			
			$marge += $prod['prix'] * $prod['nb'];
		}
		
		$prix_HT += $prod['prix'] * $prod['nb'];
		$tva_total += $prod['prix'] * ($_POST['tva']-1) * $prod['nb'];
	}
	
	// on fait l'avoir uniquement si un produit est retourn�
	if($nb_articles > 0) {
		
		$prix_TTC = $prix_HT*$_POST['tva'];
		
		if($_POST['fdp'] == 'on') {
			
			$frais_port_client = $_POST['frais_port_client_HT'];
			$tva_port = $_POST['frais_port_client_TTC'] - $_POST['frais_port_client_HT'];
			
			$prix_TTC += $_POST['frais_port_client_TTC'];
		}
		else {
			
			$frais_port_client = 0;
			$tva_port = 0;
		}
		
		$remboursement = number_format(($prix_HT + $frais_port_client)*-1, 2);
		
		switch($_POST['type_paiement']) {
			
			case 'cheque' : 
				$order_status = 19;
				$type_rbs = 'Ch�que Bancaire';
				$history_comment = 'Nous venons de transmettre votre dossier � notre service comptable pour qu\'il effectue le remboursement. Remboursement concernant la commande '. $_GET['oID'];
				//pas de mail
				$mailToSend = false;
			break;
			
			case 'cb' :
				$order_status = 16;
				$type_rbs = 'Carte Bancaire';
				$history_comment = 'Le remboursement vient d\'�tre effectu� sur votre carte bleue sur la commande '. $_GET['oID'];
				
				$mailToSend = true;
				$mail = '<br /><br />Nous venons de cr�diter la somme de '. $remboursement .' Euros; HT ('. round($remboursement*1.2, 2) .' TTC) sur la carte bleue qui a �t� utilis�e lors de la commande';
			break;
			
			case 'paypal' :
				$order_status = 16;
				$type_rbs = 'PayPal';
				$history_comment = 'Le remboursement vient d\'�tre effectu� sur votre compte PayPal concernant la commande '. $_GET['oID'];
				
				$mailToSend = true;
				$mail = '<br /><br />Nous venons de cr�diter la somme de '. $remboursement .' Euros; HT ('. round($remboursement*1.2, 2) .' TTC) sur votre compte PayPal qui a �t� utilis�e lors de la commande';
			break;
			
			case 'pm' :
				$order_status = 16;
				$type_rbs = 'Porte Monnaie';
				$history_comment = 'Votre porte monnaie virtuel vient d\'�tre cr�dit� de la somme de '. $remboursement .' Euros HT ('. round($remboursement*1.2, 2) .' TTC) suite � l\'avoir fait sur la commande '. $_GET['oID'] .'.' . "\n";
				// THIERRY J'ai retir� un <br /> juste apr�s le point final de la ligne pr�c�dente.
				$update_customer = 'UPDATE '. TABLE_CUSTOMERS .' SET customers_argent_virtuel=customers_argent_virtuel+'. $remboursement .' WHERE customers_id='. $data['customers_id'];
				$update_porte_monnaie = 'INSERT INTO '. TABLE_CUSTOMERS_ARGENT .' (customers_id, somme, raison, raison_client, date_versement, user_id)
							VALUES ('. $data['customers_id'] .', '. $remboursement .', \'Avoir sur la commande '. $_GET['oID'] .'\', \'Nous venons de cr�diter la somme de '. $remboursement .' &euro; suite � un avoir sur la commande '. $_GET['oID'] .'\', \''. date('Y-m-d H:i:s') .'\',  '. $_SESSION['login_id'] .')';
				
				tep_db_query($update_customer);
				tep_db_query($update_porte_monnaie);
				$mailToSend = true;
				
				$mail = '<br />Vous pouvez utiliser cette somme lors d\'une prochaine commande.<br /><br />Plus d\'informations sur l\'utilisation du porte monnaie virtuel : <br /><a href="'. WEBSITE . BASE_DIR .'/faq.php?Faq_Fam=79" style="color:#aab41d; font-weight:bold; ">'. WEBSITE . BASE_DIR .'/faq.php?Faq_Fam=79</a>';
			break;
		}
	
		
		if($_POST['type_paiement']!='pm') {
			
			$tva_total += $frais_port_client * ($_POST['tva']-1);
			
			$insert_order = 'INSERT INTO '. TABLE_ORDERS .'(customers_id, 
											  customers_name, 
											  customers_company, 
											  customers_street_address, 
											  customers_suburb, 
											  customers_city, 
											  customers_postcode, 
											  customers_country, 
											  customers_state, 
											  customers_telephone, 
											  customers_email_address, 
											  customers_address_format_id, 
											  delivery_name, 
											  delivery_company,
											  delivery_street_address, 
											  delivery_suburb, 
											  delivery_city, 
											  delivery_postcode, 
											  delivery_state, 
											  delivery_country, 
											  delivery_address_format_id, 
											  
											  payment_method, 
											 
											  etat_paiement, 
											  
											  last_modified, 
											  date_purchased, 
											  orders_status, 
											  orders_date_finished, 
											  orders_date_fin_devis, 
											  currency, 
											  currency_value, 
											  orders_numero_facture, 
											  orders_date_facture, 
											  customer_ip, 
											  customer_ip_country, 
											  referer, 
											  referer_host, 
											  visiteur_payant, 
											  visiteur_mot_cle, 
											  nb_articles, 
											  total, 
											  ss_total, 
											  tva_total, 
											  
											  frais_port_client, 
											  tva_port, 
											  marge, 
											  poids) 
												
												values(\''. $data['customers_id'] .'\', 
											\''. $data['customers_name'] .'\', 
											\''. $data['customers_company'] .'\', 
											\''. $data['customers_street_address'] .'\', 
											\''. $data['customers_suburb'] .'\', 
											\''. $data['customers_city'] .'\', 
											\''. $data['customers_postcode'] .'\', 
											\''. $data['customers_country'] .'\', 
											\''. $data['customers_state'] .'\', 
											\''. $data['customers_telephone'] .'\', 
											\''. $data['customers_email_address'] .'\', 
											\''. $data['customers_address_format_id'] .'\', 
											
											\''. $data['delivery_name'] .'\', 
											\''. $data['delivery_company'] .'\', 
											\''. $data['delivery_street_address'] .'\', 
											\''. $data['delivery_suburb'] .'\', 
											\''. $data['delivery_city'] .'\', 
											\''. $data['delivery_postcode'] .'\', 
											\''. $data['delivery_state'] .'\', 
											\''. $data['delivery_country'] .'\', 
											\''. $data['delivery_address_format_id'] .'\', 
											
											\'rbs : '. $type_rbs .'\', 
											\'1\', 
											
											\''. date('Y-m-d H:i:s') .'\', 
											\''. date('Y-m-d H:i:s') .'\', 
											\''. $order_status .'\', 
											\'NULL\', 
											\'NULL\', 
											\''. $data['currency'] .'\', 
											\''. $data['currency_value'] .'\', 
											\''. $numero_facture .'\', 
											\''. date('Y-m-d H:i:s') .'\', 
											\'NULL\', 
											\'France\', 
											\''. $data['referer'] .'\', 
											\''. $data['referer_host'] .'\', 
											\''. $data['visiteur_payant'] .'\', 
											\''. $data['visiteur_mot_cle'] .'\', 
											\''. $nb_articles .'\', 
											\''. round($prix_TTC, 2) .'\', 
											\''. round($prix_HT, 2) .'\', 
											\''. round($tva_total, 2) .'\', 
											\''. $frais_port_client .'\', 
											\''. $tva_port .'\', 
											\''. round($marge, 2) .'\', 
											\''. $data['poids'] .'\'
											)';
	
			if(tep_db_query($insert_order)) {
				
				$order_id = mysql_insert_id();
				
				//on incr�mente le num�ro
				tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . ($numero_facture+1) . "' where configuration_key = 'COMPTEUR_FACTURE'");
				
				$insert_history = 'insert into '. TABLE_ORDERS_STATUS_HISTORY .'(orders_id, orders_status_id, date_added, customer_notified, comments)
												values(\''. $order_id .'\', \''. $order_status .'\', \''. date('Y-m-d H:i:s') .'\', \'1\', \''. addslashes($history_comment) .'\')';
				
				tep_db_query($insert_history);
			}
		}
		
		
		if($mailToSend) {			
			mail_client_commande($data['customers_email_address'], 'Remboursement - '. NOM_DU_SITE, 'Bonjour <span style="font-weight: bold; color: rgb(170, 180, 29);">'. $data['customers_name'] .'</span>, <br /><br />'. $history_comment . $mail .'<br /><br />'. SIGNATURE_MAIL);
		}
		
		
		$query_product = 'select * from '. TABLE_ORDERS_PRODUCTS .' where orders_id='. $data['orders_id'];
		$query_product = tep_db_query($query_product);
		
		$i = 0;
		
		while($data_products = tep_db_fetch_array($query_product)) {
			
			if($products_array[$data_products['orders_products_id']]['nb'] > 0) { // le produit n'est pr�sent dans la table orders_products que si sa quantit� est positive
				
				$query_options = 'select * from '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES .' where orders_id='. $data['orders_id'] .' and orders_products_id='. $data_products['orders_products_id'];
				$query_options = tep_db_query($query_options);
				$data_options = tep_db_fetch_array($query_options);
				
				$productHasOption = (tep_db_num_rows($query_options) > 0) ? true : false;
				
				//retour en stock des produits
				if($products_array[$data_products['orders_products_id']]['retour_stock']) {
					
					$update_product = 'update '. TABLE_PRODUCTS .' set
											products_quantity=products_quantity+'. ($data_products['products_quantity']-$data_products['products_quantity_sent']+$products_array[$data_products['orders_products_id']]['nb']) .',
											products_quantity_reel=products_quantity_reel+'. $products_array[$data_products['orders_products_id']]['nb'] .',
											products_ordered=products_ordered-'. $products_array[$data_products['orders_products_id']]['nb'] .'
										where products_id='. $data_products['products_id'];
				}
				else {
					
					$update_product = 'update '. TABLE_PRODUCTS .' set
											products_quantity=products_quantity+'. ($data_products['products_quantity']-$data_products['products_quantity_sent']) .',
											products_ordered=products_ordered-'. $products_array[$data_products['orders_products_id']]['nb'] .'
										where products_id='. $data_products['products_id'];
				}
				
				tep_db_query($update_product);
			
				//on remet � 0 le produit de la commande pr�c�dente
				$query = 'update '. TABLE_ORDERS_PRODUCTS .'
							set products_quantity_sent=products_quantity
							where orders_products_id='. $data_products['orders_products_id'];
				tep_db_query($query);
				
				if($productHasOption) {
					
					//retour en stock des produits
					if($products_array[$data_products['orders_products_id']]['retour_stock']) {
						
						$update_option = 'update '. TABLE_PRODUCTS_ATTRIBUTES .' set
												options_quantity=options_quantity+'. ($data_products['products_quantity']-$data_products['products_quantity_sent']+$products_array[$data_products['orders_products_id']]['nb']) .', 
												options_quantity_reel=options_quantity_reel+'. $products_array[$data_products['orders_products_id']]['nb'] .'
										  where products_id='. $data_products['products_id'] .'
										  and options_values_id='. $data_options['options_values_id'];
						
					}
					else {
						
						$update_option = 'update '. TABLE_PRODUCTS_ATTRIBUTES .' set
												options_quantity=options_quantity+'. ($data_products['products_quantity']-$data_products['products_quantity_sent']) .'
										  where products_id='. $data_products['products_id'] .'
										  and options_values_id='. $data_options['options_values_id'];
					}
					
					tep_db_query($update_option);
				}
				
				if($_POST['type_paiement']!='pm') {
					
					$insert_order_products = 'insert into '. TABLE_ORDERS_PRODUCTS .'(orders_id, 
																		  products_id, 
																		  id_rayon, 
																		  cpath, 
																		  products_model, 
																		  products_name, 
																		  products_price, 
																		  products_cost, 
																		  final_price, 
																		  products_tax, 
																		  products_quantity, 
																		  products_quantity_sent)
													values(
												\''. $order_id .'\', 
												\''. $data_products['products_id'] .'\', 
												\''. $data_products['id_rayon'] .'\', 
												\''. $data_products['cpath'] .'\', 
												\''. addslashes($data_products['products_model']) .'\', 
												\''. addslashes($data_products['products_name']) .'\', 
												\''. $data_products['products_price'] .'\', 
												\''. $data_products['products_cost'] .'\', 
												\''. $products_array[$data_products['orders_products_id']]['prix'] .'\', 
												\''. $data_products['products_tax'] .'\', 
												\''. $products_array[$data_products['orders_products_id']]['nb'] .'\', 
												\''. $products_array[$data_products['orders_products_id']]['nb'] .'\')';
					
					tep_db_query($insert_order_products);
					
					$last_order_product_id = mysql_insert_id();
					
					
					
					if($productHasOption) {
						
						$insert_order_products_attribute = 'insert into '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES .'(orders_id, 
																											   orders_products_id, 
																											   options_values_id,
																											   products_options, 
																											   products_options_values, 
																											   options_values_price, 
																											   price_prefix)
															values(
														\''. $order_id .'\', 
														\''. $last_order_product_id .'\', 
														\''. $data_options['options_values_id'] .'\', 
														\''. $data_options['products_options'] .'\', 
														\''. $data_options['products_options_values'] .'\', 
														\''. $data_options['options_values_price'] .'\', 
														\''. $data_options['price_prefix'] .'\'
														)';
						
						tep_db_query($insert_order_products_attribute);
					}
				}
				else {//porte monnaie virtuel
					
					if($i++ == 0) {
						
						$history_comment .= 'Le montant correspondant aux articles suivants a �t� cr�dit� dans votre porte-monnaie virtuel : '. "\n\n";
					}
					
					$history_comment .= $products_array[$data_products['orders_products_id']]['nb'] .' x '. $data_products['products_name'] ."\n";
				}
			}
		}
	}
	
	
	if($_POST['type_paiement']=='pm') {
		
		$insert_history = 'insert into '. TABLE_ORDERS_STATUS_HISTORY .'(orders_id, orders_status_id, date_added, customer_notified, comments)
											values(\''. $data['orders_id'] .'\', \''. $order_status .'\', \''. date('Y-m-d H:i:s') .'\', \'1\', \''. addslashes($history_comment) .'\')';
			
		if(tep_db_query($insert_history)) {
			
			//on met � jour le statut de la commande
			$query = 'update '. TABLE_ORDERS .' set orders_status=\''. $order_status .'\' where orders_id='. $data['orders_id'];
			tep_db_query($query);
		}
		
		tep_redirect(FILENAME_CMD_EDIT .'?oID='. $data['orders_id'] .'&action=edit');
	}
	else {
		
		tep_redirect(FILENAME_CMD_EDIT .'?oID='. $order_id .'&action=edit');
	}
}

include(DIR_WS_CLASSES . 'order.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title>Avoir sur la commande <?php echo $_GET['oID']; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" type="text/javascript" src="includes/lib/jquery1.3.2.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	
	var nbre_product = $("#nbre_product").val();
	
	$(".qte_to_return").keyup(function() { // changement de la quantit� d'un produit
		
		max_command = parseInt($(this).parent().prev().find(".qte_command").html());
		
		if($(this).val() <= max_command) {
			
			prix = 0;
			
			for(i=0 ; i<nbre_product ; i++) {
				
				if($("#remise_stock_"+ i).attr("checked")) { // retour en stock
					
					margeUnit = parseFloat($("#prixHT_"+ i).val()) + parseFloat($("#prixAchatHT_"+ i).val());
				}
				else {
					
					margeUnit = parseFloat($("#prixHT_"+ i).val());
				}
				
				prix += $("#products_qte_"+ i).val() * $("#prixHT_"+ i).val();
				
				margeHT = Math.round($("#products_qte_"+ i).val() * margeUnit*100)/100;
				totalHT = Math.round($("#products_qte_"+ i).val() * parseFloat($("#prixHT_"+ i).val())*100)/100;
				totalTTC = Math.round($("#products_qte_"+ i).val() * parseFloat($("#prixHT_"+ i).val()) * $("#tva").val() *100)/100;
				
				row_update(i, margeHT, totalHT, totalTTC);
			}
			
			maj_total(prix);
		}
		else {
			
			$(this).val(max_command);
		}
	});
	
	$(".remise_en_stock").change(function() {
		
		prix = 0;
		
		// num�ro de la ligne
		row = $(this).attr("id").split("_");
		row = row[row.length-1];
		
		if( $(this).attr("checked") ) {
			
			margeUnit = parseFloat($("#prixHT_"+ row).val()) + parseFloat($("#prixAchatHT_"+ row).val());
		}
		else {
			
			margeUnit = parseFloat($("#prixHT_"+ row).val());
		}
		
		margeHT = Math.round($("#products_qte_"+ row).val() * margeUnit*100)/100;
		totalHT = Math.round($("#products_qte_"+ row).val() * parseFloat($("#prixHT_"+ row).val())*100)/100;
		totalTTC = Math.round($("#products_qte_"+ row).val() * parseFloat($("#prixHT_"+ row).val()) * $("#tva").val() *100)/100;
		
		row_update(row, margeHT, totalHT, totalTTC);
		
		//Recacul du total
		for(i=0 ; i<nbre_product ; i++) {
			
			prix += $("#products_qte_"+ i).val() * $("#prixHT_"+ i).val();
		}
		
		maj_total(prix);
	});
	
	$("#fdp").change(function() {
		
		if( $(this).attr("checked") ) {
			
			 total = parseFloat($("#total").html()) + parseFloat($("#fdpTTC").val());
			 tva = parseFloat($("#tva_total").html()) + (parseFloat($("#fdpTTC").val()) - parseFloat($("#fdpHT").val()));
		}
		else {
			
			total = parseFloat($("#total").html()) - parseFloat($("#fdpTTC").val());
			tva = parseFloat($("#tva_total").html()) - (parseFloat($("#fdpTTC").val()) - parseFloat($("#fdpHT").val()));
		}
		
		$("#tva_total").html(tva.toFixed(2));
		$("#total").html(total.toFixed(2));
	});
	
	$(".send_data").click(function() {
		
		formChecked = false;
		
		for(i=0 ; i< document.forms["form_products"].elements["type_paiement"].length ; i++) {
			
			if(document.forms["form_products"].elements["type_paiement"][i].checked) formChecked = true;
		}
		
		if(formChecked) document.forms["form_products"].submit();
		else alert('Vous devez choisir un mode de rembousement');
		
	});

});

function maj_total(prix) {
	
	stotalHT = Math.round(prix * 100)/100;
	stotalTTC = Math.round(prix * $("#tva").val() * 100)/100;
	
	total = ($("#fdp").attr("checked")) ? parseFloat(stotalTTC) + parseFloat($("#fdpTTC").val()) : parseFloat(stotalTTC);
	
	tva_total = prix * ( $("#tva").val()-1 );
	
	//if($("#fdp").attr("checked")) tva_total += parseFloat($("#fdpTTC").val()) - parseFloat($("#fdpHT").val())
	
	$("#stotalHT").html(stotalHT.toFixed(2));
	$("#stotalTTC").html(stotalTTC.toFixed(2));
	$("#stotalHT").val(prix.toFixed(2));
	
	$("#tva_total").html(tva_total.toFixed(2));
	
	$("#total").html(total.toFixed(2));
}

function row_update(index, marge, totalHT, totlaTTC) {
	
	$("#margeHT_"+ index).html(marge.toFixed(2));
	$("#totalHT_"+ index).html(totalHT.toFixed(2));
	$("#totalTTC_"+ index).html(totalTTC.toFixed(2));
}
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

<?php require(DIR_WS_INCLUDES . 'header.php');?>

<?php
    	$order = new order($oID);		
		// on recupere toute la fiche (client nom,prenom, etc...)
		$customer_query = tep_db_query("select * from ".TABLE_CUSTOMERS." where  customers_id  = '" .$order->customer['id'] . "'");
		
		while ($tuple = mysql_fetch_array($customer_query)) {
			$option_sms= $tuple['customers_news_sms'];
			$portable_client= $tuple['customers_telephone'];
			$fixe_client= $tuple['customers_fixe'];
			$email_client= $tuple['customers_email_address'];
		}
?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
<tr>
	<td width="100%" valign="top">
		<table border="0" width="100%" cellspacing="2" cellpadding="2">
    	<tr>
            <td width="100%">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td class="pageHeading">
                        Avoir sur la commande : <?php echo $oID ?> - Code Client :
                        <!--  DEBUT REDIRECTION VIA CODE CLIENT VERS LA FICHE DU CLIENT -->
                        <?php echo '<a href="' . tep_href_link(FILENAME_CLIENT_EDIT . '?cID=' . $order->customer['id']) . '&action=edit" target="_blank" title="Voir la fiche du client">
                        <font class="pageHeading">'.$order->customer['id'].'</b></font></a>'?>
                        <!--  FIN REDIRECTION VIA CODE CLIENT VERS LA FICHE DU CLIENT -->
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                        // Code pour mettre des icones en fontion du TYPE de client
                        $customers_type_query = tep_db_query("SELECT customers_id, customers_type FROM " . TABLE_CUSTOMERS . " WHERE customers_id = '" . $order->customer['id'] . "'");
                        $type_client = mysql_fetch_array($customers_type_query);
                        
                        if($type_client['customers_type'] == '1') {
                            
                            echo '<img src="../gasadmin/images/icons/icone_particulier.gif" alt="Client Particulier" title="Client Particulier">';
                        }
                        elseif($type_client['customers_type'] == '2') {
                            
                            echo '<img src="../gasadmin/images/icons/icone_professionnel.gif" alt="Client Professionnel" title="Client Professionnel">';
                        }
                        elseif($type_client['customers_type'] == '3') {
                            
                            echo '<img src="../gasadmin/images/icons/icone_revendeur.gif" alt="Client Revendeur" title="Client Revendeur">';
                        }?>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/<?php echo DIR_WS_SITE;?>/compte/connexion.php?panier_admin=true&connexion=ok&mail=<?php echo $email_client;?>&mdp=azertyuiopqsdfghjklm" target="_blank"><?php echo tep_image(DIR_WS_IMAGES . 'icons/commande_01.png', $row_basket["nb_articles"]." article(s)");?></a>
                        &nbsp;&nbsp;&nbsp;
                        
					</td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
                    <td class="pageHeading" align="right">&nbsp;</td>
				</tr>
				</table>
                
                <table width="100%" border="0" cellspacing="1" cellpadding="1">
                    <tr><td colspan="3"><?php echo tep_draw_separator(); ?></td></tr>
                </table>
				<?php 
				$total_articles = 0;
				$total_price = 0;
				$total_cost = 0;
				//trouver les num�ros des commandes du cleint dans table "orders" en fonction de "customers_id"
				$IDcommande = tep_db_query("SELECT orders_id FROM orders WHERE customers_id = " .$order->customer['id'] . "");
				$nombre_de_commandes = mysql_num_rows($IDcommande);
				
				while($row = mysql_fetch_array($IDcommande)) {
					
					$total=tep_db_query("select products_quantity as QUANTITY, products_price as PRICE, products_cost as COST from orders_products where orders_id=". $row['orders_id']);
					
					while($data_total=mysql_fetch_object($total)) {
						
						$total_articles= $total_articles+$data_total->QUANTITY; 
						$total_price=$total_price+$data_total->PRICE*$data_total->QUANTITY;
						$total_cost=$total_cost+$data_total->COST*$data_total->QUANTITY;
					} 
				}
				
				$TVA = ($order->info['tva_total'] == 0) ? 1 :1.2;
					
				$total_marge = tep_db_query("select sum(marge) as total_marge from orders WHERE customers_id = " .$order->customer['id']);
				$row_marge = mysql_fetch_array($total_marge);
				$total_price = convert_price(round($total_price * $TVA, 2));
				
				foreach($order->products as $prod) {
					
					$total_article_commande += $prod["qty"];
				}
				
				?>
            </td>
		</tr>

        <tr><td> <?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td></tr>
        <tr>
            <td>
                <form method="post" name="form_products" action="<?php $_SERVER['PHP_SELF'];?>">
                    <input type="hidden" name="action_orders_products" value="ok">
                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                    <tr class="dataTableHeadingRow">
                        <td class="dataTableHeadingContent" style="text-align: center;">Qte command�e</td>
                        <td class="dataTableHeadingContent" style="text-align: center;">Qte � retourner</td>
                        <td class="dataTableHeadingContent" style="text-align: center;">Articles</td>
                        <td class="dataTableHeadingContent" style="text-align: center;">Remise en stock</td>
                        <td class="dataTableHeadingContent">R�f�rence</td>
                        <td class="dataTableHeadingContent" align="right">Prix d'achat HT</td>
                        <td class="dataTableHeadingContent" align="right">Prix Unit. HT</td>
                        <td class="dataTableHeadingContent" align="right">Prix Unit. TTC</td>
                        <td class="dataTableHeadingContent" align="right">Marge HT</td>
                        <td class="dataTableHeadingContent" align="right">Total HT</td>
                        <td class="dataTableHeadingContent" align="right">Total TTC</td>
                    </tr>

<?php
    for($i=0, $n=sizeof($order->products) ; $i<$n ; $i++) {
		
		$suppressions_dans_reference = array("#", "#####");
		$application_suppressions_dans_reference = str_replace($suppressions_dans_reference, "", $order->products[$i]['model']);?>
		<tr class="dataTableRow">
        	
             <?php 
				if($order->products[$i]['attributes'][0]['value'] != '') {
					
					$recup_orders_products_id_query = tep_db_query('SELECT opa.orders_products_id 
																	FROM ' . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . ' opa, '. TABLE_ORDERS_PRODUCTS .' op
																	WHERE opa.orders_products_id=op.orders_products_id
																	AND opa.orders_id = \'' . $oID . '\'
																	AND products_id=\''. $order->products[$i]['id'] .'\'
																	AND products_options_values = \'' . addslashes($order->products[$i]['attributes'][0]['value']) . '\'');
																	
					$recup_orders_products_id = tep_db_fetch_array($recup_orders_products_id_query);
					$orders_products_id = $recup_orders_products_id['orders_products_id'];
				}
				else {
					
					$recup_orders_products_id_query = tep_db_query('SELECT orders_products_id 
																	FROM ' . TABLE_ORDERS_PRODUCTS . ' 
																	WHERE orders_id = "' . $oID . '" AND products_id = "' . $order->products[$i]['id'] . '"');
					$recup_orders_products_id = tep_db_fetch_array($recup_orders_products_id_query);
					$orders_products_id = $recup_orders_products_id['orders_products_id'];
				}
				
			?>
            
	    	<td class="dataTableContent" valign="top" align="middle">
				<?php
				echo '<span class="qte_command">'. $order->products[$i]['qty'] .'</span>';
				?>
                &nbsp;<strong>X</strong>
            </td>
            <td class="dataTableContent" valign="top" align="center">
				<?php
				
				echo tep_draw_input_field('products_qte_'. $orders_products_id, $order->products[$i]['qty'], 'maxlength="3" size="3" style="text-align:center;" class="qte_to_return" id="products_qte_'. $i .'"');
				
				echo tep_draw_input_field('orders_products_'. $i, $orders_products_id, '', '', 'hidden');
				
				?>
                &nbsp;<strong>X</strong>
            </td>
            <td class="dataTableContent" valign="top">
            	<a href="<?php echo FILENAME_ARTICLE_EDIT;?>?cPath=<?php echo $order->products[$i]['cpath'];?>&pID=<?php echo $order->products[$i]['id'];?>&action=new_product" target="_blank"><?php echo $order->products[$i]['name'];?></a>
			<?php
			
            if(isset($order->products[$i]['attributes']) && (sizeof($order->products[$i]['attributes']) > 0)) {
				
                for($j=0, $k=sizeof($order->products[$i]['attributes']) ; $j<$k ; $j++) {
					
                    echo '<br /><nobr><small>&nbsp;<i> - <a href="javascript: updateProduct(\'' . $oID . '\', \'' . $order->products[$i]['id'] . '\', \'options\', \'update\', \'' . $order->products[$i]['attributes'][$j]['option'] . '\', \'' . $order->products[$i]['attributes'][$j]['value'] . '\');"><u>' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
                    if ($order->products[$i]['attributes'][$j]['price'] != '0') echo ' (' . $order->products[$i]['attributes'][$j]['prefix'] . $currencies->format($order->products[$i]['attributes'][$j]['price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . ')';
                    echo '</u></a></i></small></nobr>';
                }
            }?>
			</td>
            <!--Remise en stock -->
            <td class="dataTableContent" valign="top" align="center">
            <?php
				echo tep_draw_input_field('remise_stock_'. $orders_products_id, '', 'id="remise_stock_'. $i .'" class="remise_en_stock"', '', 'checkbox');
			?>
            </td>
            <!--R�f�rence -->
      		<td class="dataTableContent">
                <a href="javascript: updateProduct('<?php echo $oID;?>', '<?php echo $order->products[$i]['id'];?>', 'products_model', 'update', '', '<?php echo $order->products[$i]['model'];?>');">
                <u><?php echo $order->products[$i]['model'];?></u></a>
            </td>
            
		    
            <!--prix achat -->
            <td class="dataTableContent" align="right" valign="top">
            	<?php echo number_format($order->products[$i]['cost'], 2) .'&euro;';
				echo '<input type="hidden" name="prixAchatHT_'. $i .'" value="'. $order->products[$i]['cost'] .'" id="prixAchatHT_'. $i .'" />';
				?>
            </td>
            <!--prix unit HT -->
            <td class="dataTableContent" align="right" valign="top">
            <?php $prix = number_format("-". $order->products[$i]['final_price'],2);
				echo tep_draw_input_field('prixHT_'. $i, "-". $order->products[$i]['final_price'], 'id="prixHT_'. $i .'"', '', "hidden");
				echo $prix .'&nbsp&euro;';
			?>
            </td>
            <!--prix unit TTC -->
		    <td class="dataTableContent" align="right" valign="top">
            	<span class="bold"><?php echo $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']), true, $order->info['currency'], $order->info['currency_value']);?></span>
            </td>
            <!--marge HT -->
		    <td class="dataTableContent" align="right" valign="top"><span class="bold" id="margeHT_<?php echo $i; ?>">-<?php echo $currencies->format(($order->products[$i]['final_price'] * $order->products[$i]['qty']), true, $order->info['currency'], $order->info['currency_value']);?></span></td>
            <!--total HT -->
            <td class="dataTableContent" align="right" valign="top"><span class="bold" id="totalHT_<?php echo $i; ?>">-<?php echo $currencies->format($order->products[$i]['final_price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']);?></span></td>
            <!--total TTC -->
            <td class="dataTableContent" align="right" valign="top"><span class="bold" id="totalTTC_<?php echo $i; ?>">-<?php echo $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']) * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']);?></span></td>
      	</tr>
<?php }?>
        <tr>
            <td align="center" colspan="7" valign="top">
            <td align="right" colspan="5">
            <table border="0" cellspacing="0" cellpadding="2">
				<tr>
                    <td align="right" class="smallText">Sous Total HT : </td>
                    <td align="right" class="smallText"><?php echo '<span id="stotalHT">-'. $order->info['ss_total'] .'</span>'; ?> � (<?php echo '<span id="stotalTTC">-'.number_format($order->info['ss_total'] * $TVA,2) .'</span>'; ?> � TTC)</td>
                </tr>
                <tr>
                    <td align="right" class="smallText">Frais de port HT : </td>
                    <td align="right" class="smallText"><?php 	echo "-". $order->info['frais_port_client']; ?> &euro; (-<?php echo $order->info['frais_port_client']+$order->info['tva_port']; ?> � TTC)
                    <?php echo tep_draw_input_field('frais_port_client_HT', "-". $order->info['frais_port_client'], 'maxlength="4" size="4" style="text-align:center;" id="fdpHT"', '', "hidden");
					echo tep_draw_input_field('frais_port_client_TTC', "-". ($order->info['frais_port_client']+$order->info['tva_port']), 'maxlength="4" size="4" style="text-align:center;" id="fdpTTC"', '', "hidden"); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="smallText">Dont TVA Fran�aise : </td>
                    <td align="right" class="smallText"><span id="tva_total">-<?php echo $order->info['tva_total']-($order->info['tva_port']); ?></span> � </td>
                </tr>
                <?php if($order->info['remise']!=0){?>
                <tr>
                    <td align="right" class="smallText">Remise HT : </td>
                    <td align="right" class="smallText"><?php echo $order->info['remise']; ?> � ( <?php echo number_format($order->info['remise']*$order->info['tva_port'],2); ?> � TTC)</td>
                </tr>
                <?php }?>
                <?php if($order->info['remise_pourcent']!=0){?>
                <tr>
                    <td align="right" class="smallText">Remise Pourcentage HT : </td>
                    <td align="right" class="smallText"><?php echo $order->info['remise_pourcent']; ?> %</td>
                </tr>
                <?php }?>
                <?php if($order->info['remise_porte_monnaie']!=0){?>
                <tr>
                    <td align="right" class="smallText">Remise Porte Monnaie HT : </td>
                    <td align="right" class="smallText"><?php echo $order->info['remise_porte_monnaie']; ?> � ( <?php $avec_tva = correction_tva($order->info['remise_porte_monnaie']*1.2); echo $avec_tva['arrondie']; ?> � TTC)</td>
                </tr>
                <?php }?>
                <tr>
                    <td align="right" class="smallText">Total TTC : </td>
                    <?php 
					$total_final = number_format($order->info['ss_total'] * $TVA, 2);
					?>
                    <td align="right" class="smallText"><?php echo '<span id="total">-'. $total_final .'</span>'; ?> �</td>
                </tr>
                <tr>
                    <td align="right" class="smallText"></td>
                    <td align="right" class="smallText"><br ></td>
                </tr>
            </table></td>
        </tr>
       </table><hr>
       	<div>Remboursement frais de port : <?php echo tep_draw_input_field('fdp', '', 'id="fdp"', '', 'checkbox'); ?></div>
    	<div>Type de remboursement : Ch�que <?php echo '<input type="radio" name="type_paiement" value="cheque" />'; ?> - CB <?php echo '<input type="radio" name="type_paiement" value="cb" />'; ?> - PayPal <?php echo '<input type="radio" name="type_paiement" value="paypal" />'; ?> - Porte Monnaie <?php echo '<input type="radio" name="type_paiement" value="pm" />'; ?></div>
    
    <input type="hidden" id="tva" name="tva" value="<?php echo $TVA; ?>" />
    <input type="hidden" id="nbre_product" name="nbre_product" value="<?php echo sizeof($order->products); ?>" /><hr>
       <div class="align_right"><input type="button" class="send_data" value="Enregistrer l'avoir" /></div>
        </form>
        
	</td>
  </tr>
  
  <tr>
  	<td>
   
    </td>
  </tr>
  </table>
  </td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>