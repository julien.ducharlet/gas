<?php
/*
  Page : telephone_sans_images.php

  Modifier le 05/06/2007
*/

  require('includes/application_top.php');

  $url_google="https://images.google.fr/images?svnum=10&um=1&hl=fr&sa=X&oi=spell&resnum=0&ct=result&cd=1&q=_RECHERCHE_&spell=1";
		
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">T�l�phone sans image sur <?php echo STORE_NAME; ?> </td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table>
		</td>
       </tr>
	   
	   <tr>
	    <td>
			<div class="explaination">Permet de lister les Cat�gories qui n'ont pas encore de photo</div>
	     </td>
	   </tr>
	  <tr>
		<td align="left">
			<?
				
				///////////////////////////////////////////////////////////////////////////////////////
				////////////////////    CATEGORIES SANS IMAGE ///////////////   $cats[]   /////////////
				///////////////////////////////////////////////////////////////////////////////////////		   
				$query = 'SELECT cd.categories_name as cat_nom, cd.categories_id as cat_id, parent_id '
									   .'FROM categories_description cd, categories c '
									   .'WHERE (categories_image2="no_picture_cat.png") '
									   .'AND c.categories_id=cd.categories_id '
									   .'AND sort_order="0" '
									   .'AND parent_id!="0"' 
									   .'ORDER BY cd.categories_name';
				$query = tep_db_query($query);
				$i = 1;
				
				while($data = tep_db_fetch_array($query)) {
					
					$cats[$i]['id'] = $data['cat_id'];
					$cats[$i]['nom'] = $data['cat_nom'];
					$cats[$i]['par'] = $data['parent_id'];
					$i++;
				}
							
				///////////////////////////////////////////////////////////////////////////////////////
				////////////////////    categories  /////////////////////////   $marq[]   /////////////
				///////////////////////////////////////////////////////////////////////////////////////		   
				$requete_liste_fabs = 'SELECT cd.categories_name, cd.categories_id '
								   .'FROM categories c, categories_description cd '
								   .'WHERE c.parent_id="0" '
								   .'AND c.categories_id=cd.categories_id '
								   .'ORDER BY cd.categories_name';
				$reponse_liste_fabs = tep_db_query($requete_liste_fabs);
					
				while($data = tep_db_fetch_array($reponse_liste_fabs)) {
					
					$marq[$data['categories_id']]['id'] = $data['categories_id'];
					$marq[$data['categories_id']]['nom'] = $data['categories_name'];
				}
				
				echo '<table class="infoBoxContent" width="800" cellspacing="5">';
					
					echo '<tr>';
						echo '<td>Cat&eacute;gorie -> Article</td>';
						echo '<td>Lien vers Google</td>';
					echo '</tr>';
					
					for($i=1 ; $i<=count($cats) ; $i++) {
						
						echo '<tr><td>';
						echo '&nbsp;&nbsp;'. $marq[$cats[$i]['par']]['nom'] .' -> '. $cats[$i]['nom'] . '</td><td>';
						echo '<a href=' . str_replace('_RECHERCHE_', str_replace(' ', '%20', $cats[$i]['nom']), $url_google). ' target="_blank">Lien vers Google image pour "' . str_replace('&nbsp; ', '%20', $cats[$i]['nom']) .'"</a>';
						echo '</td></tr>';
					}
				
				echo '</table>';
			?>
		</td>
	  </tr>
	   
	    
	   
	  </table>
	 </td>
	</tr>

      </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
