<?php
/*  Page by Paul
	Cette page liste les articles avec plusieurs options*/

require('includes/application_top.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php
require(DIR_WS_INCLUDES . 'header.php');

$requete_articles = tep_db_query("SELECT p.products_id, p.products_model, pa.options_id, pd.products_name 
								  FROM products p, products_attributes pa, products_description pd 
								  WHERE p.products_id = pa.products_id AND p.products_id = pd.products_id
								  GROUP BY pa.options_id, p.products_id
								  ORDER BY p.products_id");

?>
<div style="margin: 20px 5px;">
    <table border="1" style="text-align: center; margin: auto;">
    	<tr>
        	<th width="200px">Ref article</th>
            <th width="300px">Nom article</th>
        </tr>
        <?php
			while ($liste_articles = tep_db_fetch_array($requete_articles)) {
				//echo $liste_articles['products_id'] . " - " . $liste_articles['products_name'] . " - " . $liste_articles['options_id'] . "<br />";
				if ($id_article == $liste_articles['products_id'] && !$deja_passe) {
					$deja_passe = true;
					?>
						<tr>
							<td style="text-align: right;">&nbsp;<?php echo $liste_articles['products_model']; ?>&nbsp;&nbsp;</td>
							<td style="text-align: left;">&nbsp;&nbsp;<?php echo $liste_articles['products_name']; ?>&nbsp;</td>
						</tr>
					<?php
				} else {
					$deja_passe = false;
				}
				$id_article = $liste_articles['products_id'];
			}
		?>
	</table>
</div>
    
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>