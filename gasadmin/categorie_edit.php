<?php
  	require('includes/application_top.php');
  	require(DIR_WS_CLASSES . 'currencies.php');
  	$currencies = new currencies();
  	$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
    
   	if (isset($_GET['cPath'])) $cPath=$_GET['cPath'];
   	if ( eregi("(insert|update|setflag)", $action) ) include_once('includes/reset_seo_cache.php');

	if (tep_not_null($action)) {
		switch ($action) {
		case 'setsortorder':
			for ($i=0, $n=sizeof($HTTP_POST_VARS['products_id']); $i<$n; $i++) {
				tep_set_product_sort_order($HTTP_POST_VARS['products_id'][$i], $HTTP_POST_VARS['sortorder'][$i]);
			}
			$sorting = false;
			tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $HTTP_POST_VARS['cPath']));
			break;
		case 'setflag':
			if ( ($HTTP_GET_VARS['flag'] == '0') || ($HTTP_GET_VARS['flag'] == '1') ) {
				if (isset($HTTP_GET_VARS['cID'])) {
					tep_set_categorie_status($HTTP_GET_VARS['cID'], $HTTP_GET_VARS['flag']);
				}
				if (USE_CACHE == 'true') {
					tep_reset_cache_block('categories');
					tep_reset_cache_block('also_purchased');
				}
			}
			tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $HTTP_GET_VARS['cPath'] . '&cID=' . $HTTP_GET_VARS['cID']));
			break;
		case 'insert_category':
		case 'update_category':
			if (isset($HTTP_POST_VARS['categories_id'])) $categories_id = tep_db_prepare_input($HTTP_POST_VARS['categories_id']);
			$sort_order = tep_db_prepare_input($HTTP_POST_VARS['sort_order']);
			
			$categories_status_client=$HTTP_POST_VARS['categories_status_client'];
			if($categories_status_client==""){$categories_status_client=0;}
			$categories_status_rev=$HTTP_POST_VARS['categories_status_rev'];
			if($categories_status_rev==""){$categories_status_rev=0;}
			$categories_status_pro=$HTTP_POST_VARS['categories_status_pro'];
			if($categories_status_pro==""){$categories_status_pro=0;}
			$categories_status_vip=$HTTP_POST_VARS['categories_status_vip'];
			if($categories_status_vip==""){$categories_status_vip=0;}
			$sql_data_array = array('sort_order' => $sort_order,'categories_status_rev' => $categories_status_rev, 'categories_status_client' => $categories_status_client,
									'categories_status_pro' => $categories_status_pro, 'categories_status_vip' => $categories_status_vip);
	
			if ($action == 'insert_category') {
				$insert_sql_data = array('parent_id' => $current_category_id, 'date_added' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
				tep_db_perform(TABLE_CATEGORIES, $sql_data_array);
				$categories_id = tep_db_insert_id();
				$rubrique_id=$HTTP_POST_VARS['rubrique'];
				tep_db_query("insert into ". categories_rubrique." (categories_id,rubrique_id) values (".$categories_id.",".$rubrique_id.")");
			} 
			elseif ($action == 'update_category') {
				$update_sql_data = array('last_modified' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $update_sql_data);
				tep_db_perform(TABLE_CATEGORIES, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "'");
				$rubrique_id=$HTTP_POST_VARS['rubrique'];
				tep_db_query("update ". categories_rubrique." set rubrique_id=".$rubrique_id." where categories_id=".$HTTP_POST_VARS['categories_id']);
			}
	
			$languages = tep_get_languages();
			for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
				$categories_name_array = $HTTP_POST_VARS['categories_name'];
				$categories_htc_title_array = $HTTP_POST_VARS['categories_htc_title_tag'];
				$categories_htc_desc_array = $HTTP_POST_VARS['categories_htc_desc_tag'];
				$categories_htc_keywords_array = $HTTP_POST_VARS['categories_htc_keywords_tag'];
				$categories_htc_description_array = $HTTP_POST_VARS['categories_htc_description'];
				$balise_title_lien_image_array = $HTTP_POST_VARS['balise_title_lien_image'];
				$balise_title_lien_texte_array = $HTTP_POST_VARS['balise_title_lien_texte'];
				$categories_url_array = $HTTP_POST_VARS['categories_url'];
				$categories_baseline_array = $HTTP_POST_VARS['categories_baseline'];
											
				$language_id = $languages[$i]['id'];
				$sql_data_array = array('categories_name' => tep_db_prepare_input($categories_name_array[$language_id]), 
								'categories_htc_title_tag' => (tep_not_null($categories_htc_title_array[$language_id]) ? tep_db_prepare_input($categories_htc_title_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_htc_desc_tag' => (tep_not_null($categories_htc_desc_array[$language_id]) ? tep_db_prepare_input($categories_htc_desc_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_htc_keywords_tag' => (tep_not_null($categories_htc_keywords_array[$language_id]) ? tep_db_prepare_input($categories_htc_keywords_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_htc_description' => tep_db_prepare_input($categories_htc_description_array[$language_id]),
								'balise_title_lien_image' => (tep_not_null($balise_title_lien_image_array[$language_id]) ? tep_db_prepare_input($balise_title_lien_image_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'balise_title_lien_texte' => (tep_not_null($balise_title_lien_texte_array[$language_id]) ? tep_db_prepare_input($balise_title_lien_texte_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_url' => (tep_not_null($categories_url_array[$language_id]) ? tep_db_prepare_input($categories_url_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])),
								'categories_baseline' => (tep_not_null($categories_baseline_array[$language_id]) ? tep_db_prepare_input($categories_baseline_array[$language_id]) :  tep_db_prepare_input($categories_name_array[$language_id])));
				
				if ($action == 'insert_category') {
					$insert_sql_data = array('categories_id' => $categories_id, 'language_id' => $languages[$i]['id']);
					$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
					tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
				} 
				elseif ($action == 'update_category') {
					tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "' and language_id = '" . (int)$languages[$i]['id'] . "'");
				}
			}
			
			//ici on fait la requete de mise � jour de l'image si on up une nouvelle image
			if ($categories_image2 = new upload('categories_image2', DIR_FS_CATEGORIE_IMAGES)) {
				//on met a jour la base de donn�e et on upload l'image si le champ categories_image2 n'est pas vide
				if($categories_image2->filename != "")	  
				  tep_db_query("update " . TABLE_CATEGORIES . " set categories_image2 = '" . tep_db_input($categories_image2->filename) . "' where categories_id = '" . (int)$categories_id . "'");
			}
			
			//ici on fait la requete de mise � jour de l'image si on up une nouvelle image
			if ($categories_header = new upload('categories_header', DIR_FS_CATEGORIE_HEADER_IMAGES)) {
				//on met a jour la base de donn�e et on upload l'image si le champ categories_header n'est pas vide
				if($categories_header->filename != "")	  
				  tep_db_query("update " . TABLE_CATEGORIES . " set categories_header = '" . tep_db_input($categories_header->filename) . "' where categories_id = '" . (int)$categories_id . "'");
			}
	
			if (USE_CACHE == 'true') {
				tep_reset_cache_block('categories');
				tep_reset_cache_block('also_purchased');
			}
			tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $categories_id));
			break;
			
		case 'delete_category_confirm':
			if (isset($HTTP_POST_VARS['categories_id'])) {
				$categories_id = tep_db_prepare_input($HTTP_POST_VARS['categories_id']);
				$categories = tep_get_category_tree($categories_id, '', '0', '', true);
				$products = array();
				$products_delete = array();
				$rubrique_id=$HTTP_POST_VARS['rubrique'];
				tep_db_query("delete from ". categories_rubrique." where categories_id=".$categories_id);
			  
				for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
					$product_ids_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$categories[$i]['id'] . "'");
					while ($product_ids = tep_db_fetch_array($product_ids_query)) {
					  $products[$product_ids['products_id']]['categories'][] = $categories[$i]['id'];
					}
				}
				reset($products);
			  
				while (list($key, $value) = each($products)) {
					$category_ids = '';
	
					for ($i=0, $n=sizeof($value['categories']); $i<$n; $i++) {
						$category_ids .= "'" . (int)$value['categories'][$i] . "', ";
					}
					$category_ids = substr($category_ids, 0, -2);
					$check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$key . "' and categories_id not in (" . $category_ids . ")");
					$check = tep_db_fetch_array($check_query);
					if ($check['total'] < '1') {
						$products_delete[$key] = $key;
					}
				}
				
				// removing categories can be a lengthy process
				tep_set_time_limit(0);
				for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
					tep_remove_category($categories[$i]['id']);
				}
	
				reset($products_delete);
				while (list($key) = each($products_delete)) {
					tep_remove_product($key);
				}
			}
	
			if (USE_CACHE == 'true') {
			  tep_reset_cache_block('categories');
			  tep_reset_cache_block('also_purchased');
			}
	
			tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath));
			break;
		  
		case 'move_category_confirm':
			if (isset($HTTP_POST_VARS['categories_id']) && ($HTTP_POST_VARS['categories_id'] != $HTTP_POST_VARS['move_to_category_id'])) {
				$categories_id = tep_db_prepare_input($HTTP_POST_VARS['categories_id']);
				$new_parent_id = tep_db_prepare_input($HTTP_POST_VARS['move_to_category_id']);
				$path = explode('_', tep_get_generated_category_path_ids($new_parent_id));
				if (in_array($categories_id, $path)) {
					$messageStack->add_session(ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT, 'error');
					tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $categories_id));
				} 
				else {
					tep_db_query("update " . TABLE_CATEGORIES . " set parent_id = '" . (int)$new_parent_id . "', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
					if (USE_CACHE == 'true') {
						tep_reset_cache_block('categories');
						tep_reset_cache_block('also_purchased');
					}
					tep_redirect(tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $new_parent_id . '&cID=' . $categories_id));
				}
			}
			break;
		}
	}
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>

<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="goOnLoad();">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top">
    
    
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td>
        
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
			<?php
				$cPath_back = '';
				if (sizeof($cPath_array) > 0) {
					for ($i=0, $n=sizeof($cPath_array)-1; $i<$n; $i++) {
						if (empty($cPath_back)) {
							$cPath_back .= $cPath_array[$i];
						} 
						else {
							$cPath_back .= '_' . $cPath_array[$i];
						}
					}
				}
    			$cPath_back = (tep_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
?>
			<!--end sort order//-->
         
<?php 
    $heading = array();
    $contents = array();
    if(isset($cID) && !empty($cID)){
		$categories_selected_query = tep_db_query("select c.categories_id, cd.categories_name, cr.rubrique_id , c.categories_status, c.categories_status_client, c.categories_status_rev, c.categories_status_pro, c.categories_status_vip, c.categories_image2, c.categories_header, c.parent_id, c.sort_order, c.date_added, c.last_modified, cd.categories_htc_title_tag, cd.categories_htc_desc_tag, cd.categories_htc_keywords_tag, cd.categories_htc_description, cd.balise_title_lien_image, cd.balise_title_lien_texte, cd.categories_url, cd.categories_baseline from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd, " . TABLE_CATEGORIES_RAYON . " cr where c.categories_id = '" . (int)$cID . "' and c.categories_id = cd.categories_id and c.categories_id = cr.categories_id and cd.language_id = '" . (int)$languages_id . "' order by c.sort_order, cd.categories_name");
		$categories_selected_row=tep_db_fetch_array($categories_selected_query);
		$category_childs = array('childs_count' => tep_childs_in_category_count($categories_selected_row['categories_id']));
		$category_products = array('products_count' => tep_products_in_category_count($categories_selected_row['categories_id']));
		$cInfo_array = array_merge($categories_selected_row, $category_childs, $category_products);
		$cInfo = new objectInfo($cInfo_array);			
	}
	
	switch ($action) {
      case 'new_category':
	  	//############# EXTENSION POLYTECH  #############  
		// On r�cup�re les diff�rentes rubriques (Telephonie, Console, etc ....) 					 
		$Rubrique_query = tep_db_query("select * from ".rubrique);
			
		$list_rubriques = array(); 
		while ($tuple = tep_db_fetch_array($Rubrique_query)){
			$list_rubriques[] = array('id' => $tuple['rubrique_id'], 'text' => $tuple['rubrique_name']);
		}
		//############# fin extension polytech ###############

        $heading[] = array('text' => '<b>Nouvelle cat�gorie</b>');
		$contents = array('form' => tep_draw_form('newcategory', FILENAME_CATEGORIE_LIST, 'action=insert_category&cPath=' . $cPath, 'post', 'enctype="multipart/form-data"'));
        $contents[] = array('text' => 'Merci de compl�ter les informations suivantes pour la nouvelle cat�gorie');

        $category_inputs_string = '';
        $languages = tep_get_languages();
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
		
          $category_inputs_string .= tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . 
		  '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
		  tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']');
		  
		  // liste d�roulante pour choisir une rubrique
		  $category_inputs_string_rubrique .= tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . 
		  '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
		  tep_draw_pull_down_menu('rubrique', array_merge(array(),$list_rubriques)); 
				
		  // HTC BOC
          $category_htc_title_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 		  
		  tep_draw_input_field('categories_htc_title_tag[' . $languages[$i]['id'] . ']', 'Accessoires T�l�phone portable [1] #' );
		  
		  $category_htc_desc_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 
		  tep_draw_input_field('categories_htc_desc_tag[' . $languages[$i]['id'] . ']', 'T�l�phone portable # - Voici la liste des accessoires pour le t�l�phone #' );
		  
          $category_htc_keywords_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 
		  tep_draw_input_field('categories_htc_keywords_tag[' . $languages[$i]['id'] . ']', '#' );
		  
          $category_htc_description_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 
		  tep_draw_textarea_field('categories_htc_description[' . $languages[$i]['id'] . ']', 'hard', 30, 5, '<b><h1>Les Accessoires
#</h1></b><br/>
Voici la liste des accessoires
pour le t�l�phone mobile #<br />
Vous cherchez un accessoire et
il n\'est pas pr�sent ? <b><a
href="/contact_us.php"
target="_self"><font
color="#ff7e00">contactez
nous</font></a></b> et nous
l\'ajouterons pour vous.');
        // HTC EOC
				
			// AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY				
			$balise_title_lien_image_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 		 		  	
			tep_draw_input_field('balise_title_lien_image[' . $languages[$i]['id'] . ']', 'telephone portable' );
				
			$balise_title_lien_texte_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . 		 		  	
			tep_draw_input_field('balise_title_lien_texte[' . $languages[$i]['id'] . ']', 'telephone mobile' );				
			// FIN AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY
			
			// AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY
			$category_url_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;'.tep_draw_input_field('categories_url[' . $languages[$i]['id'] . ']', '' );
			$category_baseline_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']).'&nbsp;'.tep_draw_textarea_field('categories_baseline[' . $languages[$i]['id'] . ']', 'hard', 30, 5, '');
			// FIN AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY
        }

        $contents[] = array('text' => '<br />Nom de la cat�gorie :' . $category_inputs_string);
		$contents[] = array('text' => '<br />Rayon de la cat�gorie :'.  $category_inputs_string_rubrique );
		
		$contents[] = array('text' => '<br />Visible seulement des :');
		$contents[] = array('text' => '<br />Client :'.  tep_draw_checkbox_field('categories_status_client','1',true).'&nbsp;&nbsp;Revendeur :'.  tep_draw_checkbox_field('categories_status_rev','1',false).'&nbsp;&nbsp;Pro :' . tep_draw_checkbox_field('categories_status_pro','1',false) . '&nbsp;&nbsp;VIP :'.  tep_draw_checkbox_field('categories_status_vip','1',false));
		
		$contents[] = array('text' => '<br />Image de la Cat�gorie :<br />' . tep_draw_file_field('categories_image2'));
        $contents[] = array('text' => '<br />Header de la Cat�gorie :<br />' . tep_draw_file_field('categories_header'));
        $contents[] = array('text' => '<br />Ordre de Tri :<br />' . tep_draw_input_field('sort_order', '', 'size="2"'));
		$contents[] = array('text' => '<br /> META TAG TITRE' . $category_htc_title_string);
        $contents[] = array('text' => '<br /> META TAG DESCRIPTION' . $category_htc_desc_string);
        $contents[] = array('text' => '<br /> META TAG MOTS CLES' . $category_htc_keywords_string);
        $contents[] = array('text' => '<br /> TEXTE en haut de la Cat�gories' . $category_htc_description_string);
		$contents[] = array('text' => '<br /> Balise TITLE au survole de cette cat�gorie pour les images' . $balise_title_lien_image_inputs_string);
		$contents[] = array('text' => '<br /> Balise TITLE au survole de cette cat�gorie pour le texte' . $balise_title_lien_texte_inputs_string);
		$contents[] = array('text' => '<br /> URL de cette cat�gorie pour le r�f�rencement' . $category_url_string); 
		$contents[] = array('text' => '<br /> BASELINE de cette cat�gorie pour le r�f�rencement' . $category_baseline_string);
		$contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_save.gif', 'Sauvegarder') . ' <a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath) . '">' . tep_image_button('button_cancel.gif', 'Annuler') . '</a>');
        break;
		
      case 'edit_category':
	  
	  	//############# EXTENSION POLYTECH  #############  
		// On r�cup�re les diff�rentes rubriques (Telephonie, Console, etc ....) 					 
		$Rubrique_query = tep_db_query("select * from rubrique");
		
		$list_rubriques = array(); 
		while ($tuple = tep_db_fetch_array($Rubrique_query)){
			$list_rubriques[] = array('id' => $tuple['rubrique_id'],
                              'text' => $tuple['rubrique_name']);
		}
		//############# fin extension polytech ###############
	     echo $cInfo->rubrique_id;
		$heading[] = array('text' => '<b>Editer la cat�gorie</b>');
        $contents = array('form' => tep_draw_form('categories', FILENAME_CATEGORIE_LIST, 'action=update_category&cPath=' . $cPath, 'post', 'enctype="multipart/form-data"') . tep_draw_hidden_field('categories_id', $cID));
        

        $category_inputs_string = '';
        $languages = tep_get_languages();
		
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          	$category_inputs_string .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] .
		  	'/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
		  	tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']', tep_get_category_name($cID, $languages[$i]['id']));
		  
			//############# EXTENSION POLYTECH  #############  
			// liste d�roulante pour choisir une rubrique
			$category_inputs_string_rubrique .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . 
			'/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' .
			tep_draw_pull_down_menu('rubrique', array_merge(array(),$list_rubriques), $cInfo->rubrique_id); 
			//############# fin extension polytech ###############

          	// HTC BOC
          	$category_htc_title_string .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_htc_title_tag[' . $languages[$i]['id'] . ']', tep_get_category_htc_title($cID, $languages[$i]['id']));
          	$category_htc_desc_string .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_htc_desc_tag[' . $languages[$i]['id'] . ']', tep_get_category_htc_desc($cID, $languages[$i]['id']));
          	$category_htc_keywords_string .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_htc_keywords_tag[' . $languages[$i]['id'] . ']', tep_get_category_htc_keywords($cID, $languages[$i]['id']));
          	$category_htc_description_string .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;<br />' . tep_draw_textarea_field('categories_htc_description[' . $languages[$i]['id'] . ']', 'hard', 30, 5, tep_get_category_htc_description($cID, $languages[$i]['id']));
          	// HTC EOC					
					
			// AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY
			$balise_title_lien_image_inputs_string .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' 
			. $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' 
			. tep_draw_input_field('balise_title_lien_image[' . $languages[$i]['id'] . ']', tep_get_balise_title_lien_image($cID, $languages[$i]['id']));
			
			$balise_title_lien_texte_inputs_string .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' 
			. $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' 
			. tep_draw_input_field('balise_title_lien_texte[' . $languages[$i]['id'] . ']', tep_get_balise_title_lien_texte($cID, $languages[$i]['id']));
			// FIN AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY

			// AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY					
			$category_url_string .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' 
			. $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' 
			. tep_draw_input_field('categories_url[' . $languages[$i]['id'] . ']', tep_get_category_url($cID, $languages[$i]['id']));
			
			$category_baseline_string .= '&nbsp;&nbsp;' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' 
			. $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;<br />' 
			. tep_draw_textarea_field('categories_baseline[' . $languages[$i]['id'] . ']', 'hard', 30, 5, '');
			// FIN AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY
					
        }

        $contents[] = array('text' => '<br />Nom de la cat�gorie 111' . $category_inputs_string);
		$contents[] = array('text' => '<br /> Rayon de la cat�gorie'.  $category_inputs_string_rubrique);
		
		$contents[] = array('text' => '<br />Visible seulement des :');
		$contents[] = array('text' => '<br />Client :'.  tep_draw_checkbox_field('categories_status_client','1',$cInfo->categories_status_client==1).'&nbsp;&nbsp;Revendeur :'.  tep_draw_checkbox_field('categories_status_rev','1',$cInfo->categories_status_rev==1).'&nbsp;&nbsp;Pro :' . tep_draw_checkbox_field('categories_status_pro','1',$cInfo->categories_status_pro==1) . '&nbsp;&nbsp;VIP :'.  tep_draw_checkbox_field('categories_status_vip','1',$cInfo->categories_status_vip==1));
																			  
        $contents[] = array('text' => '<br />' . tep_image(DIR_WS_CATEGORIE_IMAGES . $cInfo->categories_image2, $cInfo->categories_name) . '<br />' . DIR_WS_CATEGORIE_IMAGES . '<b>' . $cInfo->categories_image2 . '</b>');
        $contents[] = array('text' => '<br />Image de la cat�gorie : ' . tep_draw_file_field('categories_image2'));
        $contents[] = array('text' => '<br />' . tep_image(DIR_WS_CATEGORIE_HEADER_IMAGES . $cInfo->categories_header, $cInfo->categories_name,300) . '<br />' . DIR_WS_CATEGORIE_HEADER_IMAGES . '<b>' . $cInfo->categories_header . '</b>');
        $contents[] = array('text' => '<br />Header de la cat�gorie : ' . tep_draw_file_field('categories_header'));
        $contents[] = array('text' => '<br />Ordre de tri ' . tep_draw_input_field('sort_order', $cInfo->sort_order, 'size="2"'));
		$contents[] = array('text' => '<br />' . 'META TAG TITRE' . $category_htc_title_string);
        $contents[] = array('text' => '<br />' . 'META TAG DESCRIPTION' . $category_htc_desc_string);
        $contents[] = array('text' => '<br />' . 'META TAG MOTS CLES' . $category_htc_keywords_string);
        $contents[] = array('text' => '<br />' . 'TEXTE en haut de la Cat�gories' . $category_htc_description_string);
        $contents[] = array('text' => '<br />' . 'balise TITLE au survole de cette cat�gorie pour les images' . $balise_title_lien_image_inputs_string); 
		$contents[] = array('text' => '<br />' . 'balise TITLE au survole de cette cat�gorie pour le texte' . $balise_title_lien_texte_inputs_string);
		$contents[] = array('text' => '<br />' . 'URL de cette cat�gorie pour le r�f�rencement' . $category_url_string); 
		$contents[] = array('text' => '<br />' . 'BASELINE de cette cat�gorie pour le r�f�rencement' . $category_baseline_string);
		$contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $cID) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
		
      case 'delete_category':
        $heading[] = array('text' => '<b>Supprimer cat�gorie</b>');
        $contents = array('form' => tep_draw_form('categories', FILENAME_CATEGORIE_LIST, 'action=delete_category_confirm&cPath=' . $cPath) . tep_draw_hidden_field('categories_id', $cID));
        $contents[] = array('text' => 'Etes vous sur de vouloir supprimer cette cat�gorie ?');
        $contents[] = array('text' => '<br /><b>' . $cInfo->categories_name . '</b>');
        if ($cInfo->childs_count > 0) $contents[] = array('text' => '<br />' . sprintf('<b>ATTENTION :</b> Il y a %s (sous-)cat�gories li�es � cette cat�gorie !', $cInfo->childs_count));
        if ($cInfo->products_count > 0) $contents[] = array('text' => '<br />' . sprintf('<b>ATTENTION :</b> Il y a %s articles li�es � cette cat�gorie !', $cInfo->products_count));
        $contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $cID) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
		
      case 'move_category':
        $heading[] = array('text' => '<b>D�placer cat�gorie</b>');
        $contents = array('form' => tep_draw_form('categories', FILENAME_CATEGORIE_LIST, 'action=move_category_confirm&cPath=' . $cPath) . tep_draw_hidden_field('categories_id', $cID));
        $contents[] = array('text' => sprintf('Merci de s�lectionner la cat�gorie ou vous voudriez que <b>%s</b> soit plac�', $cInfo->categories_name));
        $contents[] = array('text' => '<br />' . sprintf('D�placer <b>%s</b> vers :', $cInfo->categories_name) . '<br />' . tep_draw_pull_down_menu('move_to_category_id', tep_get_full_category_tree(), $current_category_id));
        $contents[] = array('align' => 'center', 'text' => '<br />' . tep_image_submit('button_move.gif', IMAGE_MOVE) . ' <a href="' . tep_href_link(FILENAME_CATEGORIE_LIST, 'cPath=' . $cPath . '&cID=' . $cID) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      	echo '            <td width="25%" valign="top">' . "\n";
		$box = new box;
      	echo $box->infoBox($heading, $contents);
	    echo '            </td>' . "\n";
    }
	?>
          </tr>
        </table></td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<table width="100%" border="1" cellpadding="5" cellspacing="0" bordercolor="#CCCCCC">
  <tr>
    <td width="25%" align="right">Nom de la categorie : </td>
    <td width="25%">
			<input name="textfield" type="text" id="textfield" size="50" />		</td>
    <td width="25%" align="right">Rayon de la categorie : </td>
    <td width="25%">
      <select name="select" id="select">
        <option value="Telephonie" selected="selected">Telephonie</option>
        <option value="Console">Console</option>
        <option value="Appareil Photo">Appareil Photo</option>
      </select>		</td>
  </tr>
  <tr>
    <td align="right">Visible seulement des : </td>
    <td colspan="3" align="center">
      <input type="checkbox" name="checkbox" id="checkbox" />&nbsp;non connect�&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="checkbox" name="checkbox" id="checkbox" />&nbsp;particulier&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="checkbox" name="checkbox" id="checkbox" />&nbsp;Professionnel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="checkbox" name="checkbox" id="checkbox" />&nbsp;Revendeur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="checkbox" name="checkbox" id="checkbox" />&nbsp;VIP    </td>
  </tr>
  <tr>
    <td align="right">Image de la categorie : </td>
    <td>
      <input type="file" name="fileField" id="fileField" />    </td>
    <td colspan="2" align="center"><img src="" alt="image_cat_text" name="image_cat" width="60" height="60" id="image_cat" style="background-color: #CCCCCC" /></td>
  </tr>
  <tr>
    <td align="right">Haut de page de la categorie</td>
    <td>
      <input type="file" name="fileField2" id="fileField2" />    </td>
    <td colspan="2" align="center"><img src="" alt="" name="header_cat" width="300" height="44" id="header_cat" style="background-color: #CCCCCC" /></td>
  </tr>
  <tr>
    <td colspan="4" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="right">URL de la Categorie :</td>
    <td>
			<input name="textfield" type="text" id="textfield" size="50" />		</td>
    <td align="right">Titre de la Categorie (TITLE) : </td>
    <td>
			<input name="textfield" type="text" id="textfield" size="50" />		</td>
  </tr>
  <tr>
    <td align="right">Description de la Categorie (META DESCRIPTION) : </td>
    <td>
			<textarea name="textarea2" id="textarea2" cols="45" rows="5"></textarea>		</td>
    <td align="right">Mot cl� de la Categorie (META KEYWORD) : </td>
    <td>
			<textarea name="textarea2" id="textarea2" cols="45" rows="5"></textarea>		</td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">Survole de l'image de cette Cat�gorie (TITLE): </td>
    <td>
      <input name="textfield" type="text" id="textfield" size="50" />		</td>
    <td align="right">Survole du texte de cette Cat�gorie (TITLE): </td>
    <td>
			<input name="textfield" type="text" id="textfield" size="50" />		</td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">Bas de la page dans le footer (BASELINE) : </td>
    <td colspan="3">
      <textarea name="textarea" id="textarea" cols="140" rows="8"></textarea>    </td>
  </tr>
  <tr>
    <td colspan="4" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center">
    	<input type="submit" name="button" id="button" value="Enregistrer" />
      &nbsp;&nbsp;
      <input type="submit" name="button" id="button" value="Annuler" />    </td>
  </tr>
</table>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>