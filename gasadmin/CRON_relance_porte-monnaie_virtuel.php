<?php
/*  
	Page by Paul
*/

require('includes/application_top.php');
require('../' . DIR_WS_SITE . '/includes/fonctions/mail.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
    </head>
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
		<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
        
        <?php
			$envoi_ok = 0; $envoi_pas_ok = 0;
			
			/*$adresses_query = tep_db_query("SELECT customers_email_address, customers_firstname, customers_lastname, customers_argent_virtuel FROM customers WHERE customers_argent_virtuel > 0");*/
			$adresses_query = tep_db_query("SELECT customers_email_address, customers_firstname, customers_lastname, customers_argent_virtuel FROM customers WHERE customers_id = 6");
			while ($adresses = tep_db_fetch_array($adresses_query)) {
				
			$argent_virtuel = $adresses['customers_argent_virtuel'];
			$argent_virtuel_ttc = $adresses['customers_argent_virtuel']*1.2;
		
            $sujet = "Pensez a utiliser vos " . $argent_virtuel_ttc . " Euros TTC";
			
			// On construit le mail pour le client et on l'envoie			
			
			$explications_pm_virtuel = "Vous avez deux possibilit&eacute;s : <br />
										<br />
										Tout d'abord connectez-vous sur notre site en cliquant sur le lien suivant : <a href=\"https://www.generalarmystore.fr/\" style='font-weight:bold;color:#fe5700;'>www.generalarmystore.fr</a><br /><br />
										<strong>- Soit vous avez PLUS d'argent dans votre porte monnaie virtuel que le montant de votre panier </strong><br />
										Dans ce cas, lors du choix du mode de r&egrave;glement de votre commande vous trouverez le choix \"Paiement par Porte Monnaie Virtuel\" que vous devrez s&eacute;lectionner ! puis cochez la case \"J'ai lu et accept&eacute; les Conditions g&eacute;n&eacute;rales de vente\" et ensuite cliquez sur \"Finaliser la Commande\"<br />
										<br />
										<strong>- Soit vous avez MOINS d'argent dans votre porte monnaie virtuel que le montant de votre panier</strong><br />
										Lors du choix du mode de r&egrave;glement de votre commande vous trouverez le choix \"Paiement par Porte Monnaie Virtuel\" que vous devrez s&eacute;lectionner ! 
										puis cochez la case \"J'ai lu et accept&eacute; les Conditions g&eacute;n&eacute;rales de vente\" en ensuite cliquez sur \"Finaliser la Commande\"
										Vous devrez ensuite choisir un autre moyen de paiement pour payer la diff&eacute;rence.<br />";
										
			$texte_message = "Nous avons remarques que vous disposez de <span style='font-weight:bold;'>" . $argent_virtuel . " &euro;</span> HT ( <span style='font-weight:bold;color:#fe5700;'>" . $argent_virtuel_ttc . " &euro; TTC</span> ) dans votre porte-monnaie virtuel &agrave; d&eacute;penser sur notre site.<br /><br /><strong>Comment utiliser l'argent de mon porte-monnaie virtuel ?</strong><br />" . $explications_pm_virtuel . '<br /><br />';
			
			$message_mail = 'Bonjour <span style="font-weight: bold; color: rgb(170, 180, 29);">'.$adresses['customers_firstname']." ".$adresses['customers_lastname'].'</span>,<br /><br />' . $texte_message . SIGNATURE_MAIL;
			$result = mail_client_commande($adresses['customers_email_address'], $sujet, $message_mail);
			
			if ($result == 1) {
				$envoi_ok++;
			} else {
				$envoi_pas_ok++;
			}
		$i++;
	 }
	 
	 echo $envoi_ok . " mails ont &eacute;t&eacute; envoy&eacute;s avec succ&egrave;s.<br />" . $envoi_pas_ok . " mails ont retourn&eacute; une erreur.";
        ?>
        
        
        <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
    </body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>