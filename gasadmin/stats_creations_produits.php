<?php
/* Stats sur la cr�ation des produits */

  require('includes/application_top.php');

$date = mysql_query("SELECT curdate() as time");
$d = mysql_fetch_array($date, MYSQL_ASSOC);

// si recherche entre deux dates
if (isset($_GET['date']) && ($_GET['date'] == 'yes')) { 

	if (empty($_GET['edate'])) {
		$bet = date('Y-m-d H:i:s');
		$header = 'Ce rapport est g�n�r� depuis cette le '  . date('d/m/Y',strtotime($_GET['sdate'])) . ' jusqu\'� maintenant : ';
		$end_of_query = ' where products_date_added > \'' . $_GET['sdate'] . ' 00:00:01\' and products_date_added < \''. $d['time'] .' 23:59:59\'';		
	} else {
		$header = 'Ce rapport est pour les dates entre '  . date('d/m/Y',strtotime($_GET['sdate'])) . ' et ' . date('d/m/Y',strtotime($_GET['edate'])) . ' : ';
		$end_of_query = ' where products_date_added > \'' . $_GET['sdate'] . ' 00:00:01\' and products_date_added < \''. $_GET['edate'] .' 23:59:59\'';
	}
	
// sinon si pas de recherche
} else { 
		$header = 'Ce rapport est pour le jour : ';
		$bet = date('Y-m-d H:i:s');	
		$header_date_query = mysql_query("SELECT DATE_FORMAT('" . $d['time'] . "', '%d/%m/%Y ') as date");
		$header_date = mysql_fetch_array($header_date_query, MYSQL_ASSOC);
		$header .= $header_date['date'];
		$end_of_query = ' where products_date_added > \'' . $d['time'] . '\' and products_date_added < \''.$bet .'\'';
	
}

$query = 'select count(*) as nb_produit from '. TABLE_PRODUCTS . $end_of_query;
$query = tep_db_query($query);
$total = tep_db_fetch_array($query);

//affichage des choix pour les stats et les stats
$page_contents = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2" style="margin-bottom:10px; margin-top:10px">
				<tr>
					<td align=left> 
						<table bgcolor="#DDDDDD" width="500px">
							<tr>
								<td class="main">
									&nbsp;&nbsp;&nbsp;Nombre de produit qui ont �t� cr�es : <strong>' . $total['nb_produit'] .'</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="left" class="main">
						<table bgcolor="#DDDDDD" width="500px">
							<tr>
								<td class="main" align="center" colspan="2"><b>' . tep_draw_form('export_to_file_by_date', 'stats_creations_produits.php', 'get', '') . tep_draw_hidden_field('action', 'export') . tep_draw_hidden_field('date', 'yes') . 'Choisissez les dates<b></td>
							</tr>
							<tr>
								<td class="main" align="center" width="250px">Entre le :</td>
								<td align="center" width="250px"><input type="text" name="sdate" id="sdate" value="'.$_GET['sdate'].'"><a href="#" onClick="displayCalendar(sdate,\'yyyy-mm-dd\',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a></td>
							</tr>
							<tr>
								<td class="main" align="center">Et le : </td>
								<td align="center"><input type="text" name="edate" id="edate" value="'.$_GET['edate'].'"><a href="#" onClick="displayCalendar(edate,\'yyyy-mm-dd\',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><input type="image" name="submit" src="includes/languages/french/images/buttons/button_confirm.gif" alt="Exportation Excel" width="65" height="22"></form></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
		';
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title>GAS - Stats sur la cr�ation des produits</title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/javascript/dhtmlgoodies_calendar.css"></link>
<script type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td>
          <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td class="pageHeading"><?php echo $header; ?></td> 
            </tr>
            <tr>
              <td><?php echo $page_contents; ?></td>
            </tr>
          </table>
				</td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>