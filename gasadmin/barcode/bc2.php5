<?php
// Define variable to prevent hacking
define('IN_CB',true);

if (!isset($_GET['ref'])){
	$_GET['ref'] = "aucune ref";
}

// Including all required classes
require('class/index.php5');
require('class/Font.php5');
require('class/FColor.php5');
require('class/BarCode.php5');
require('class/FDrawing.php5');

// Including the barcode technology
include('class/code128.barcode.php5');

// Loading Font
$font = new Font('./class/font/Arial.ttf', 12);

// Creating some Color (arguments are R, G, B)
$color_black = new FColor(0,0,0);
$color_white = new FColor(255,255,255);

/* Here is the list of the arguments:
1 - Thickness
2 - Color of bars
3 - Color of spaces
4 - Resolution
5 - Text
6 - Text Font */
$code = new code128(20,$color_black,$color_white,2,strtoupper($_GET['ref']),$font);

/* Here is the list of the arguments
1 - Filename (empty : display on screen)
2 - Background color */
$drawing =& new FDrawing('',$color_white);
$drawing->setBarcode($code);
$drawing->draw();

// Header that says it is an image (remove it if you save the barcode to a file)
header('Content-Type: image/png');

// Draw (or save) the image into PNG format.
$drawing->finish(IMG_FORMAT_PNG);
?>