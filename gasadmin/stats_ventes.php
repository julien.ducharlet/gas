<?php
  require('includes/application_top.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<?php require(DIR_WS_INCLUDES . 'header.php'); 
$query_total="select count(1) as nb_ventes, sum(total) as total_vente, sum(marge) as total_marge, visiteur_payant, referer_host from orders group by visiteur_payant order by visiteur_payant";
$res_total = tep_db_query($query_total);
?>
<table border="0" width="50%" cellspacing="2" cellpadding="2">
	<?php
	while($res_total && $row_total=tep_db_fetch_array($res_total)){?>
    <tr>
		<td>
        <?php
        if($row_total["visiteur_payant"]==1){
            echo "<a href=\"stats_ventes.php?visiteur_payant=1\">Payant</a>";
        }
		elseif($row_total["visiteur_payant"]==''){
	        echo "Visiteur direct";
    	}
		else{
    	    echo "<a href=\"stats_ventes.php?visiteur_payant=0\">Gratuit</a>";
	    }
		?>
        </td>
        <td>Total ventes : <?php echo $row_total["nb_ventes"];?></td>
		<td>Total CA : <?php echo $row_total["total_vente"];?></td>
        <td>Total marge : <?php echo $row_total["total_marge"];?></td>
    </tr>
    <?php
	}?>
</table>
<br /><br />
<table border="0" width="60%" align="center" cellspacing="2" cellpadding="2">
<?php 
if(isset($_REQUEST["host"]) && !empty($_REQUEST["host"])){?>
	<tr>
    <td colspan="2">
    	Ventes provenant de 
    	<?php
		echo $_REQUEST["host"];
		if($_REQUEST["payant"]==1){
			echo " (payant)";
		}
		else{
			echo " (gratuit)";
		}
		?>
    </td>
    </tr>
    <tr>
    <td>Mot cl�</td>
  	<td>Ventes</td>
    <td>Visites</td>
    <td>CA Total</td>
    <td>Marge</td>
    <td align="center">Marge/visite</td>
    </tr>
<?php
	$where="1";
	$query_vente="select count(1) as nb_ventes, sum(total) as total_vente, sum(marge) as total_marge, visiteur_mot_cle, visiteur_payant from orders where ".$where." and referer_host='".$_REQUEST["host"]."' and visiteur_payant='".$_REQUEST["payant"]."' and visiteur_mot_cle<>'' group by visiteur_mot_cle order by  nb_ventes desc, total_marge desc, total_vente desc";
	$res_vente = tep_db_query($query_vente);
	while($res_vente && $row_vente=tep_db_fetch_array($res_vente)){?>
        <tr>
        <td>
            <?php 
            //echo "<a href=\"stats_ventes.php?mot_cle=".$row_vente["mot_cle"]."\">";
			echo $row_vente["visiteur_mot_cle"];
			//echo "</a>";
            echo " (<a href=\"https://".$_REQUEST["host"]."/search?hl=fr&q=".$row_vente["visiteur_mot_cle"]."+&btnG=Recherche+Google&meta=&aq=f&oq=\" target=\"_blank\">GG</a>)";
			?>
        </td>
        <td><?php echo $row_vente["nb_ventes"];?></td>
        <td>
        	<?php
            $query_visite="select count(1) as nb_visite from visite where mot_cle='".$row_vente["visiteur_mot_cle"]."' and payant='".$row_vente["visiteur_payant"]."'";
			$res_visite = tep_db_query($query_visite);
			$row_visite=tep_db_fetch_array($res_visite);
			if($row_visite["nb_visite"]==0){
				$nb_visite=1;
			}
			else{
				$nb_visite=$row_visite["nb_visite"];
			}
			echo $nb_visite;
			
			?>
        </td>
        <td><?php echo $row_vente["total_vente"];?></td>
        <td><?php echo $row_vente["total_marge"];?></td>
        <td align="center"><?php echo number_format($row_vente["total_marge"]/$nb_visite,4);?></td>
        </tr>	
    <?php 
    }
}
else{?>
	<tr>
    <td>Host</td>
  	<td>Ventes</td>
  	<td>Visites</td>
    <td>CA Total</td>
    <td>Marge</td>
    <td align="center">Marge/visite</td>
    </tr>
<?php
	$where="1";
	if(isset($_REQUEST["visiteur_payant"])){
		$where="visiteur_payant='".$_REQUEST["visiteur_payant"]."'";
	}
	$query_vente="select count(1) as nb_ventes, sum(total) as total_vente, sum(marge) as total_marge, referer_host, visiteur_payant from orders where ".$where." and referer_host<>'' group by referer_host, visiteur_payant order by nb_ventes desc, total_marge desc, total_vente desc";
	$res_vente = tep_db_query($query_vente);
	while($res_vente && $row_vente=tep_db_fetch_array($res_vente)){?>
        <tr>
        <td>
            <?php 
            echo "<a href=\"stats_ventes.php?host=".$row_vente["referer_host"]."&payant=".$row_vente["visiteur_payant"]."\">".$row_vente["referer_host"]."</a>";
            if($row_vente["visiteur_payant"]==1){
                echo " (payant)";
            }
            else{
                echo " (gratuit)";
            }?>
        </td>
        <td><?php echo $row_vente["nb_ventes"];?></td>
		<td>
        	<?php
            $query_visite="select count(1) as nb_visite from visite where referer_host='".$row_vente["referer_host"]."' and payant='".$row_vente["visiteur_payant"]."'";
			$res_visite = tep_db_query($query_visite);
			$row_visite=tep_db_fetch_array($res_visite);
			echo $row_visite["nb_visite"];
			?>
        </td>
        <td><?php echo $row_vente["total_vente"];?></td>
        <td><?php echo $row_vente["total_marge"];?></td>
        <td align="center"><?php echo number_format($row_vente["total_marge"]/$row_visite["nb_visite"],4);?></td>
        </tr>	
    <?php 
    }
}?>
</table>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>