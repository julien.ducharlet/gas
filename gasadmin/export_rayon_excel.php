<?php 

function mergeCells(&$sheet, $content, $format, $firstRow, $firstCol, $lastRow, $lastCol) {
	
	$sheet->merge_cells($firstRow, $firstCol, $lastRow, $lastCol);
	$sheet->write($firstRow, $firstCol, $content, $format);
	
	for($rowNum=1 ; $rowNum<=$lastCol ; $rowNum++) {
		
		if($lastRow > $firstRow) {//fusion sur plusieurs lignes
			
			for($colNum=$firstRow ; $colNum<=$lastRow ; $colNum++) {
				
				$sheet->write_blank($colNum, $rowNum, $format);
			}
		}
		else {
			
			$sheet->write_blank($firstRow, $rowNum, $format);
		}
	}
}


if(isset($_GET['rubrique']) && !empty($_GET['rubrique']) && isset($_GET['type_client']) && !empty($_GET['type_client'])) {
	
	require('includes/configure.php');
	require(DIR_WS_INCLUDES . 'database_tables.php');
	require(DIR_WS_FUNCTIONS . 'database.php');
	tep_db_connect() or die('Unable to connect to database server!');
	
	require('..'. BASE_DIR .'/compte/generation_excel/write_excel-0.3.0/class.writeexcel_workbook.inc.php');
	require('..'. BASE_DIR .'/compte/generation_excel/write_excel-0.3.0/class.writeexcel_worksheet.inc.php');
	
	
	
	
	/* VARIABLE GLOBALE */
	$TOTAL_COLS = 5;
	
	
	
	
	
	
	$rowNum = 0;
	
	$fname = tempnam("/tmp", "prix-produits.xls");
	$workbook = &new writeexcel_workbook($fname);
	$worksheet = &$workbook->addworksheet();
	
	$worksheet->set_column('A:A', 25);
	$worksheet->set_column('B:B', 50);
	$worksheet->set_column('C:C', 40);
	$worksheet->set_column('D:G', 20);
	
	/* COULEURS */
	$greenTitle = $workbook->set_custom_color(25, 0, 176, 80);
	$greenCat = $workbook->set_custom_color(26, 204, 255, 204);
	$greyColumn = $workbook->set_custom_color(27, 192, 192, 192);
	/* */
	
	/* FORMATS DE CELLULES */
	$title =& $workbook->addformat();
	$title->set_color('black');
	$title->set_bg_color($greenTitle);
	$title->set_bold();
	$title->set_size(12);
	$title->set_font('Calibri');
	$title->set_align('center');
	$title->set_align('vcenter');
	$title->set_merge();
	
	$catTitle =& $workbook->addformat();
	$catTitle->set_color('black');
	$catTitle->set_underline();
	$catTitle->set_size(11);
	$catTitle->set_font('Calibri');
	$catTitle->set_align('center');
	$catTitle->set_align('vcenter');
	$catTitle->set_merge();
	
	$subCatTitle =& $workbook->addformat();
	$subCatTitle->set_color('black');
	$subCatTitle->set_bg_color($greenCat);
	$subCatTitle->set_size(11);
	$subCatTitle->set_font('Calibri');
	$subCatTitle->set_align('center');
	$subCatTitle->set_align('vcenter');
	$subCatTitle->set_merge();
	
	$columnMenu =& $workbook->addformat();
	$columnMenu->set_color('black');
	$columnMenu->set_bg_color($greyColumn);
	$columnMenu->set_size(11);
	$columnMenu->set_font('Calibri');
	$columnMenu->set_align('center');
	$columnMenu->set_align('vcenter');
	
	//format des lignes produits
	$refFormat =& $workbook->addformat();
	$refFormat->set_color('black');
	$refFormat->set_size(11);
	$refFormat->set_font('Calibri');
	$refFormat->set_align('left');
	$refFormat->set_align('vcenter');
	
	$desFormat =& $workbook->addformat();
	$desFormat->copy($refFormat);
	$desFormat->set_align('vcenter');
	
	$optFormat =& $workbook->addformat();
	$optFormat->copy($refFormat);
	$optFormat->set_text_wrap();
	$optFormat->set_italic();
	
	$priceFormat =& $workbook->addformat();
	$priceFormat->set_color('black');
	$priceFormat->set_size(11);
	$priceFormat->set_font('Calibri');
	$priceFormat->set_align('right');
	$priceFormat->set_align('vcenter');
	
	
	/* ENT�TE */
	switch($_GET['type_client']) {
		
		case 1:	
			$type_name = 'Particulier';
			
			$tva = 1.2;
			$pre_price = 'part_';
			break;
		
		case 2:
			$type_name = 'Profesionnel';
			
			$tva = 1.2;
			$pre_price = 'pro_';
			break;
		
		case 3:
			$type_name = 'Revendeur';
			
			$tva = 1;
			$pre_price = 'rev_';
			break;
		
		case 4:
			$type_name = 'Administration';
			
			$tva = 1.2;
			$pre_price = 'adm_';
			break;
	}
	/* FIN ENT�TE */
	
	
	
	$query = 'select rubrique_name, c.categories_id, categories_name from '. TABLE_RAYON .' r, '. TABLE_CATEGORIES_RAYON .' ctr, '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd ';
	$query .= 'where r.rubrique_id=ctr.rubrique_id 
	and ctr.categories_id=c.categories_id 
	and c.categories_id=cd.categories_id and parent_id=0
	and r.rubrique_id='. intval($_GET['rubrique']) .' ';
	$query .= 'order by c.sort_order';
	
	$query = tep_db_query($query);
	
	while($data1 = tep_db_fetch_array($query)) {
		
		if($rowNum == 0) {
			
			//en-t�te du fichier
			mergeCells($worksheet, $data1['rubrique_name'] .' (Prix '.$type_name .')', $title, 0, 0, 0, $TOTAL_COLS);
		}
		$rowNum += 2;
		
		mergeCells($worksheet, $data1['categories_name'], $catTitle, $rowNum, 0, $rowNum, $TOTAL_COLS);
		
		$query2 = 'select c.categories_id, categories_name from '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd ';
		$query2 .= 'where c.parent_id='. $data1['categories_id'] .' and c.categories_id=cd.categories_id ';
		$query2 .= 'order by c.sort_order';
		$query2 = tep_db_query($query2);
		
		while($data2 = tep_db_fetch_array($query2)) {
			
			$rowNum += 2;
			
			mergeCells($worksheet, $data2['categories_name'], $subCatTitle, $rowNum, 0, $rowNum, $TOTAL_COLS);
			
			$rowNum++;
			
			/* MENU DES COLONNES */
			$worksheet->write($rowNum, 0, 'R�f�rence', $columnMenu);
			$worksheet->write($rowNum, 1, 'D�signation', $columnMenu);
			$worksheet->write($rowNum, 2, 'Options Disponibles', $columnMenu);
			$worksheet->write($rowNum, 3, 'Prix HT Unitaire', $columnMenu);
			$worksheet->write($rowNum, 4, 'Prix HT (x5 pces)', $columnMenu);
			$worksheet->write($rowNum, 5, 'Prix HT (x10 pces)', $columnMenu);
			
			
			//requ�tes produits
			$query_products = 'select p.products_id, products_model, products_name, 
								part_price_by_1, pro_price_by_1, rev_price_by_1, adm_price_by_1,
								part_price_by_5, pro_price_by_5, rev_price_by_5, adm_price_by_5,
								part_price_by_10, pro_price_by_10, rev_price_by_10, adm_price_by_10 ';
								
			$query_products .= 'from '. TABLE_PRODUCTS_TO_CATEGORIES .' pc, '. TABLE_PRODUCTS .' p, '. TABLE_PRODUCTS_DESCRIPTION .' pd, 
								'. TABLE_PRODUCTS_PRICE_BY_QUANTITY .' ppbq ';
								
			$query_products .= 'where pc.categories_id='. $data2['categories_id'] .' 
								and pc.products_id=p.products_id 
								and p.products_id=pd.products_id 
								and pd.products_id=ppbq.products_id
								and products_status <> 0 
								
								order by products_name';
			$query_products = tep_db_query($query_products);
			
			
			while($data_products = tep_db_fetch_array($query_products)) {
				
				$rowNum++;
				
				
				$worksheet->write($rowNum, 0, $data_products['products_model'], $refFormat);
				$worksheet->write($rowNum, 1, $data_products['products_name'], $desFormat);
				$worksheet->write($rowNum, 3, money_format("%i", $data_products[$pre_price .'price_by_1'] * $tva) ." �", $priceFormat);
				$worksheet->write($rowNum, 4, money_format("%i", $data_products[$pre_price .'price_by_5'] * $tva) ." �", $priceFormat);
				$worksheet->write($rowNum, 5, money_format("%i", $data_products[$pre_price .'price_by_10'] * $tva) ." �", $priceFormat);
				
				
				// requ�te options
				$query_options = 'select products_options_values_name ';
									
				$query_options .= 'from '. TABLE_PRODUCTS_ATTRIBUTES .' pa, '. TABLE_PRODUCTS_OPTIONS .' po, '. TABLE_PRODUCTS_OPTIONS_VALUES .' pov ';
									
				$query_options .= 'where pa.products_id='. $data_products['products_id'] .'
									and pa.options_id=po.products_options_id
									and pa.options_values_id=pov.products_options_values_id
									and pa.options_dispo = \'1\'
									
									order by pa.products_options_sort_order';
				$query_options = tep_db_query($query_options);
				
				if(tep_db_num_rows($query_options) > 0) {
					
					$optionProduct = '';
					$optionNum = 0;
					
					while($data_options = tep_db_fetch_array($query_options)) {
							
						//colonne des options (on va � ligne apr�s d�s la 2me option
						$optionProduct .= ($optionNum++ > 0) ? "\n" : '';
						$optionProduct .= '- '. $data_options['products_options_values_name'];
					}
					
					$worksheet->write($rowNum, 2, $optionProduct, $optFormat);
				}
			}
		}
	}
	
	
	$workbook->close();
	
	
	
	
	
	
	
	

	header("Content-Type: application/x-msexcel; name=\"prix-produits.xls\"");
	header("Content-Disposition: inline; filename=\"prix-produits.xls\"");
	$fh = fopen($fname, "rb");
	fpassthru($fh);
	unlink($fname);

}
else {
	
	require('includes/application_top.php');
	
	$option_custom = '';
	$option_rubrique = '';
	
	$query_custom = 'select type_id, type_name from '. TABLE_CUSTOMERS_TYPE;
	$query_custom = tep_db_query($query_custom);
	
	while($data = tep_db_fetch_array($query_custom)) {
		
		$option_custom .= '<option value="'. $data['type_id'] .'">'. $data['type_name'] .'</option>';
	}
	
	
	$query_rubiruqe = 'select rubrique_id, rubrique_name from '. TABLE_RAYON .' order by rubrique_ordre';
	$query_rubiruqe = tep_db_query($query_rubiruqe);
	
	while($data = tep_db_fetch_array($query_rubiruqe)) {
		
		$option_rubrique .= '<option value="'. $data['rubrique_id'] .'">'. $data['rubrique_name'] .'</option>';
	}
	
	?>
    <!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html <?php echo HTML_PARAMS; ?>>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <script language="javascript" src="includes/general.js"></script>
		</head>
    
        <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="goOnLoad();">
			<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
            <table border="0" width="100%" cellspacing="2" cellpadding="2">
                <tr>
                    <td width="100%" valign="top" style="padding-top:50px;">
                        <form method="get" action="">
                        <select name="rubrique" style="margin-right:20px;"><?php echo $option_rubrique; ?></select>
                        
                        <select name="type_client"><?php echo $option_custom; ?></select><br /><br />
                        
                        <input type="submit" value="G�n�rer le fichier Excel" />
                        </form>
                    </td>
                </tr>
            </table>
        </body>
    </html>
    <?php
}
?>