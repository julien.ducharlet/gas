<?php
/*  Page by Paul, Requête by Paul
	Cette page met à jour la table products_price_by_quantity en fonction des taux du fichier configure*/
	
require('includes/application_top.php');

function mysql_table_exists($table , $db) {
	$tables=mysql_list_tables($db);
	while (list($temp)=mysql_fetch_array($tables)) { if($temp == $table) { return 1; } }
	return 0;
}
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>

<script language="javascript">
function verif() {
	if (window.confirm("Voulez-vous vraiment mettre \340 jour la table ?\n\nINTERDICTION FORMELLE de s'en servir sans l'aval de l'administrateur du site !")){
		document.location.href="<?php echo $_SERVER['PHP_SELF']; ?>?lancer=true"
		return true;
	}
}
function verif_table() {
	if (window.confirm("Voulez-vous vraiment cr\351er la table ?\n\nINTERDICTION FORMELLE de s'en servir sans l'aval de l'administrateur du site !")){
		document.location.href="<?php echo $_SERVER['PHP_SELF']; ?>?creer_table=true"
		return true;
	}
}
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<div style="text-align: center;">

    
    <?php if ($_GET['effectue']) { ?>
    <div style="background-color: #CFC; border: 1px solid #090; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Base de donn&eacute;es mise &agrave; jour avec <strong>succ&egrave;s</strong> !
    </div>
    <?php } else { ?>
    	<?php if ($_GET['table_cree']) { 
				if ($_GET['succes']) {?>
                    <div style="background-color: #CFC; border: 1px solid #090; margin: 10px; padding: 5px;">
                        <strong>Informations :</strong> Table cr&eacute;e avec <strong>succ&egrave;s</strong> !
                    </div>
                <?php } else { ?>
                	<div style="background-color: #F99; border: 1px solid #F00; margin: 10px; padding: 5px;">
                        <strong>Informations :</strong> La table <strong>existe d&eacute;j&agrave;</strong> !
                    </div>
                <?php }
        	  } ?>
    <div style="background-color: #FFC; border: 1px solid #FC0; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Cette page met &agrave; jour la table la table products_price_by_quantity en fonction des taux du fichier configure.<br />Attention tous les articles seront trait&eacute;s !
    </div>
    
    <br />
    
    <input type="button" value="Cr&eacute;er la table dans la base" onClick="verif_table();"/>
    <input type="button" value="Lancer la mise &agrave; jour de la base" onClick="verif();"/>
    <?php } ?>

</div>
<?php
if ($_REQUEST['lancer']) {
	$liste_articles_query = tep_db_query("SELECT DISTINCT products_id, products_cost FROM " . TABLE_PRODUCTS . "");
	while ($liste_articles = tep_db_fetch_array($liste_articles_query)) {
		$prix = array("part_5" => $liste_articles['products_cost']*TAUX_PART_BY_5, "pro_5" => $liste_articles['products_cost']*TAUX_PRO_BY_5, "rev_5" => $liste_articles['products_cost']*TAUX_REV_BY_5, "adm_5" => $liste_articles['products_cost']*TAUX_ADM_BY_5,
					  "part_10" => $liste_articles['products_cost']*TAUX_PART_BY_10, "pro_10" => $liste_articles['products_cost']*TAUX_PRO_BY_10, "rev_10" => $liste_articles['products_cost']*TAUX_REV_BY_10, "adm_10" => $liste_articles['products_cost']*TAUX_ADM_BY_10);
		
		$mise_a_jour = tep_db_query("INSERT INTO " . TABLE_PRODUCTS_PRICE_BY_QUANTITY . " 
									 	(part_price_by_5, pro_price_by_5, rev_price_by_5, adm_price_by_5, part_price_by_10, pro_price_by_10, rev_price_by_10, adm_price_by_10, products_id)
									 VALUES (" . $prix['part_5'] . ", " . $prix['pro_5'] . ", " . $prix['rev_5'] . ", " . $prix['adm_5'] . ", " . $prix['part_10'] . ", " . $prix['pro_10'] . ", " . $prix['rev_10'] . ", " . $prix['adm_10'] . ", " . $liste_articles['products_id'] . ")");
	}
	?>
	<META HTTP-EQUIV="refresh" content="0;URL='<?php echo $_SERVER['PHP_SELF']; ?>?effectue=true'">
    <?php
}

if ($_REQUEST['creer_table']) {
	// On vérifie si la table existe, si elle n'existe pas on la crée
	if (!mysql_table_exists(TABLE_PRODUCTS_PRICE_BY_QUANTITY, DB_DATABASE)) {
		tep_db_query("CREATE TABLE `products_price_by_quantity` (
						`products_id` INT( 11 ) NOT NULL ,
						`part_price_by_5` DECIMAL( 15, 4 ) NOT NULL ,
						`pro_price_by_5` DECIMAL( 15, 4 ) NOT NULL ,
						`rev_price_by_5` DECIMAL( 15, 4 ) NOT NULL ,
						`adm_price_by_5` DECIMAL( 15, 4 ) NOT NULL ,
						`part_price_by_10` DECIMAL( 15, 4 ) NOT NULL ,
						`pro_price_by_10` DECIMAL( 15, 4 ) NOT NULL ,
						`rev_price_by_10` DECIMAL( 15, 4 ) NOT NULL ,
						`adm_price_by_10` DECIMAL( 15, 4 ) NOT NULL ,
						PRIMARY KEY ( `products_id` )
					  )");
		?>
		<META HTTP-EQUIV="refresh" content="0;URL='<?php echo $_SERVER['PHP_SELF']; ?>?table_cree=true&succes=true'">
		<?php
	} else {
		?>
        <META HTTP-EQUIV="refresh" content="0;URL='<?php echo $_SERVER['PHP_SELF']; ?>?table_cree=true'">
        <?php
	}
}
?>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>