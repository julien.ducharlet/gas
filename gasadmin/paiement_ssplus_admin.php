<?php
// penser à modifier le fichier gas/includes/modules/mod_paiement_spplus.php
function lien_paiement_spplus($id_magasin,$siret,$num_commande){
	
	// Chargement de la librairie SP PLUS si cela est nécessaire (API PHP)
    // Ligne à commenter si vous utiliser le kit marchand SP PLUS sous la forme d’un exécutable CGI.
    if ( !extension_loaded('SPPLUS') ) { dl('php_spplus.so'); }
	
	$info_client=tep_db_query("select * 
								from ".TABLE_CUSTOMERS." c, ".TABLE_ADDRESS_BOOK." a 
								where c.customers_id=a.customers_id 
									 and c.customers_id=".$_SESSION['id_client_panier']." 
									 and a.address_book_id = c.customers_default_address_id");
	$res=tep_db_fetch_array($info_client);
	$mail=$res['customers_email_address'];
	$id_client=$_SESSION['id_client_panier'];
	$nom=$res['customers_lastname'];
	$prenom=$res['customers_firstname'];
	$societe=$res['customers_company'];
	$mail=$res['customers_email_address'];
	
	$pays_adresse=tep_db_query("select countries_iso_code_2 from ".TABLE_COUNTRIES." where countries_id=".$res['entry_country_id']."");
	$res_pays=tep_db_fetch_array($pays_adresse);
	$language=$res_pays['countries_iso_code_2'];
	
	$montant=format_to_money($_SESSION['montant_panier_admin']);
    /* PARAMETRES SP PLUS OBLIGATOIRES
     * siret     : le code siret du site.
     * reference : la référence de la commande, unique pour chaque paiement effectué, limitée à 20 caractères.
     * langue    : la langue choisie par l’internaute.
     * devise    : la devise dans laquelle est exprimé le montant.
     * montant   : le montant total TTC de la commande, le séparateur décimal doit être le caractère point ".".
     * taxe      : la taxe appliquée, non utilisé mais obligatoirement présent avec la valeur 0.00.
     * validite  : la date de validité de la commande.
     * hmac      : la clé hmac calculée à partir des paramètres précédents.
     */ 
	 
	 // DETERMINATION DE LA REFERENCE UNIQUE
	 // NBen 1/ je récupère les 6 premières lettres de l'adresse email
     /*$email=$mail;
	
	 // Remplacement des caractères spéciaux
	 $email = str_replace("@", "", $email);
	 $email = str_replace(".", "", $email);
	 $email = str_replace("-", "", $email);
	 $email = str_replace("_", "", $email);
	 if (strlen($email) >= 6) {
		  $email = substr($email, 0, 6);
	 }			*/
	 // NBen 2/ je lui colle annéemoisjourheureminuteseconde, soit une longueur de 20 caractères
	 // par défaut : $reference=$email.date("YmdHis");  
	 $reference=$num_commande.date("dmYHis");
	 // Fin de le détermination de la référence
	
	 
	 // DETERMINATION DE LA LANGUE
    // (code à compléter suivant les langues disponibles sur votre site)
    switch ($language) {
      case 'EN':
        $langue = "EN";
        break;
      case 'DE':
        $langue = "DE";
        break;
      case 'ES':
        $langue = "ES";
        break;
      case 'FR':
        $langue = "FR";
        break;
      default:
        $langue = "FR";
        break;
    }
	$langue;
	
	 // DETERMINATION DE LA DEVISE ET DU CODE SIRET ASSOCIE
     // (code à compléter suivant les devises disponibles sur votre site en associant 
     //  le bon code siret à chaque devise, informations communiquées par SP PLUS)
	 $devise = "978";
	
	 // DETERMINATION DE LA VALIDITE = le lendemain (J+1 conseillé par SP PLUS)
     $validite = date("d/m/Y", mktime(0,0,0,date("m"),date("d")+1,date("Y")));
	
	 // DETERMINATION DE LA TAXE
     $taxe   = "0.00"; 
    
    // Détermination du moyen et de la modalité du paiement
    $moyen    = "CBS";
    $modalite = "1x";
	
	// DETERMINATION DE LA CLE HMAC
    // si la librairie SP PLUS est chargée (dynamiquement ou au niveau du serveur)
    $hmacspplus = "";
    if ( extension_loaded('SPPLUS') ) {
      $hmacspplus = calcul_hmac($id_magasin, $siret, $reference, $langue, $devise, $montant, $taxe, $validite);
    }
	
	$urlretour= WEBSITE . BASE_DIR_ADMIN .'/cmd_edit.php?oID='.$num_commande.'&action=edit&spplus=oui';
	$form_action_url = 'https://www.spplus.net/paiement/init.do';
	
	$process_form = 

	   '<form method="post" action="'.$form_action_url.'" name="spplus">
			<input type="hidden" name="siret" value="'.$siret.'" />
			<input type="hidden" name="reference" value="'.$reference.'" />
			<input type="hidden" name="langue" value="'.$langue.'" />
			<input type="hidden" name="devise" value="'.$devise.'" />
			<input type="hidden" name="montant" value="'.$montant.'" />
			<input type="hidden" name="taxe" value="'.$taxe.'" />
			<input type="hidden" name="validite" value="'.$validite.'" />
			<input type="hidden" name="hmac" value="'.$hmacspplus.'" />
			<input type="hidden" name="moyen" value="'.$moyen.'" />
			<input type="hidden" name="modalite" value="'.$modalite.'" />
			<input type="hidden" name="email" value="'.$mail.'" />
			<input type="hidden" name="urlretour" value="'.$urlretour.'" />
							
			<input type="hidden" name="arg1" value="'.$num_commande.'" />
			<input type="hidden" name="arg2" value="'.$id_client." ".$nom." ".$prenom.'" />
			<input type="hidden" name="arg3" value="'.$societe.'" />
			<input type="hidden" name="arg4" value="'.$id_client.'" />			
		</form>
		<script type="text/javascript">document.spplus.submit();</script> ';
	
	
    return $process_form;	
}
?>

