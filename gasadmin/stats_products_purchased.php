<?php
/*
  $Id: stats_products_purchased.php,v 1.29 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  
//nombre afficher dans la case sur la page par defaut
$nb_jours_par_default=70;
  
 //permet de convertir un nombre de jours en date
 function seadate($day) {
 $ts = date("U");
 $rawtime = strtotime("-".$day." days", $ts);
 $ndate = date("Y-m-d", $rawtime);
return $ndate;
} 
  if(isset($HTTP_GET_VARS['raz'])) {
  		if($HTTP_GET_VARS['raz'] == 1) {
			 tep_db_query('TRUNCATE TABLE ' . TABLE_RECHERCHE . '');
			 $confirm_message = mysql_affected_rows().' produit(s) remis � 0.';
			 tep_redirect(tep_href_link(FILENAME_STATS_PRODUCTS_PURCHASED));
		}
  }
  if(isset($HTTP_GET_VARS['tdate'])) $tdate = tep_db_prepare_input($HTTP_GET_VARS['tdate']);
  
  if(isset($HTTP_GET_VARS['filtre'])) $filtreQuery = tep_db_prepare_input($HTTP_GET_VARS['filtre']);
  else $filtreQuery = 0;

  if(isset($HTTP_GET_VARS['semaine'])) $semaine = tep_db_prepare_input($HTTP_GET_VARS['semaine']);
  
   if (tep_not_null($action)) {
    switch ($action) {
	
		case 'deleteconfirm':
        $rechercheID = tep_db_prepare_input($HTTP_GET_VARS['rechercheID']);

        tep_db_query("delete from " . TABLE_RECHERCHE . " where recherche_id='".$rechercheID."'");
        tep_redirect(tep_href_link(FILENAME_STATS_PRODUCTS_PURCHASED));
        break;
    }
  }
  

  if (tep_not_null($tdate)) {
		$ndate=seadate($tdate);
        tep_db_query("delete from " . TABLE_RECHERCHE . " where derniere_date<='".$ndate."'");
		tep_redirect(tep_href_link(FILENAME_STATS_PRODUCTS_PURCHASED));
  
  }
  	
	
//valeurs de la liste deroulantes semaines sur 10 SEMAINES
$oneweek = 60*60*24*7;
$premier_jour = mktime(0,0,0,date("m")-2,date("d")-date("w")+1,date("Y")); 
$d = $premier_jour;
$semaine_array = array();
$semaine_array[] = array('id' =>'','text' => 'choisir...');
for ($i = 0; $i < 10; $i++)
{
    $semaine_array[] = array('id' => date("Y-m-d",$d)."au".date("Y-m-d", $d + $oneweek), 'text' => date("d-m-Y",$d)." au ".date("d-m-Y", $d + $oneweek));
    $d += $oneweek;
}
if(isset($HTTP_GET_VARS['semaine'])) $parametres = array('semaine' => $semaine);
else $parametres = array();
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
	
      <tr>
        <td>
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading" width="40%"><?php echo "LES RECHERCHES DES CLIENTS";?></td>
			 
			 <td class="pageHeading" align="right">
	                	<?  $tdate = $_POST['tdate'];
   	                 	if ($tdate == '') $tdate = $nb_jours_par_default;
      	              	$ndate = seadate($tdate);?>
         	    <form method=post action=<? echo $PHP_SELF;?> >
            	   <table align="right" width="100%">
               	     <tr class="dataTableContent" align="right">
                  	    <td nowrap><?php echo "Supprimer les donn�es sup�rieurs �"; ?>
						<input type=text size=4 width=4 value=<? echo $tdate; ?> name=tdate>
						<?php echo "jours"; ?><input type="submit" value="<?php echo "GO"; ?>">
						</td>
   	                 </tr>
	               </table>
			    </form>
         	  </td>
			   
			<td class="smalltext" align="right" width="40%">
			<?php echo tep_draw_form('search', FILENAME_STATS_PRODUCTS_PURCHASED, '', 'get'); ?>
			<?php echo "choix de la semaine :";
			echo tep_draw_pull_down_menu('semaine', $semaine_array, '', 'onChange="this.form.submit();"');?>&nbsp;
			</td>
          </tr>
        </table>
		</td>
      </tr>
	  
	  <tr>
        <td valign="top"><div align="center" class="main">
			<?php
				if(!empty($confirm_message))  echo $confirm_message;
				else echo '<a href="?raz=1" onClick="return confirm(\''.'�tes vous certain(e)s de vouloir remettre � z�ro les compteurs ?'.'\')">Vider la table recherche</a>';
			?>
			</td>
			
		
		 
		<br>
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
		 
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" align="center"><?php echo 'No.'; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo  "RECHERCHE"; 
				?></td>
				<td class="dataTableHeadingContent" align="center"><?php echo  "DANS"; 
				?></td>
				<td class="dataTableHeadingContent" align="center"><?php echo "DERNIERE DATE"; ?></td>
				<td class="dataTableHeadingContent" align="center"><?php echo "RESULTAT"; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php 
				echo "FOIS"; 
				?>&nbsp;</td>
				<td class="dataTableHeadingContent" align="center"><?php 
				echo "SUPPRESSION"; ?>&nbsp;</td>
              </tr>
<?php
  if (isset($HTTP_GET_VARS['page']) && ($HTTP_GET_VARS['page'] > 1)) $rows = $HTTP_GET_VARS['page'] * 25 - 25;
  

  
  //modification polytech
  $products_query_raw = "select recherche_id, intitule, derniere_date, compteur,resultat,marque,modele
  from " . TABLE_RECHERCHE . "";
  $products_query_count = "select count(recherche_id) as total from " . TABLE_RECHERCHE . ""; 
  
  if(isset($semaine) and ($semaine!='')) {
  $semaine_debut = substr($semaine, 0,10 );
  $semaine_fin = substr($semaine, 12,10 );

 $products_query_raw .=" where derniere_date>='".$semaine_debut."' and derniere_date<='".$semaine_fin."'"; 
 $products_query_count .=" where derniere_date>='".$semaine_debut."' and derniere_date<='".$semaine_fin."'"; 
  }
  $products_query_raw .=" order by compteur desc";
  
  
  $products_split = new splitPageResults($HTTP_GET_VARS['page'], 25, $products_query_raw, $products_query_numrows);

   
  
  $rows = 0;
  $products_query = tep_db_query($products_query_raw);
  
  // pour fixer le nombre de dde produits differents en bas a gauche de la page
   $products_query_num = tep_db_query($products_query_count);
   while($products = tep_db_fetch_array($products_query_num)) $products_query_numrows=$products['total'];
     
  
  while ($products = tep_db_fetch_array($products_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
?>

              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)"  onMouseOut="rowOutEffect(this)"onClick="window.open('',blank')"'">
                <td class="dataTableContent" align="center"><?php echo $rows; ?>.</td>
                <td class="dataTableContent" align="center"><?php 
				if($products['resultat']>0)	
				echo '<a href="' .tep_server_href_link(FILENAME_ADVANCED_SEARCH_RESULT, 'search_in_description=1&keywords=' . str_replace(' ','+',$products['intitule']).'&incremente=1' ) . '" TARGET="_blank">'. $products['intitule']. '</a>';
				else echo $products['intitule'];
				
				
				//if($products['resultat']>0)			  
			  //echo tep_server_href_link(FILENAME_ADVANCED_SEARCH_RESULT, 
			  //'search_in_description=1&keywords=' . str_replace(' ','+',$products['intitule']).'&incremente=1'); 
				//echo $products['intitule'];
				
				?></td>
				
                <td class="dataTableContent" align="center"><?php 
				echo $products['marque']." ".$products['modele']; 
				?>&nbsp;</td>
				
                <td class="dataTableContent" align="center"><?php 
				 
				echo $products['derniere_date']; 
				?>&nbsp;</td>
				<td class="dataTableContent" align="center"><?php 
				 if($products['resultat']>0) echo $products['resultat'].' resultat(s)';
					else echo 'Aucun resultat';
				?>&nbsp;</td>
				<td class="dataTableContent" align="center"><?php 
				//echo $products['products_ordered']; 
				echo $products['compteur']; 
				?>&nbsp;</td>
				
				<td class="dataTableContent" align="center"><?php echo  
				'<a href="' . tep_href_link(FILENAME_STATS_PRODUCTS_PURCHASED, tep_get_all_get_params(array('oID', 'action')) . 'rechercheID=' . $products['recherche_id'] . '&action=deleteconfirm'). '" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer la notification : ' . $products['intitule'] . ' (DEFINITIVEMENT) ?\'))    
						{return true;} else {return false;}">' .  tep_image(DIR_WS_IMAGES . '/icons/supprimer.png', "Supprimer")  . '</a>&nbsp;';
						
					?>&nbsp;</td>	
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, 25, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
                <td class="smallText" align="right"><?php 
				echo $products_split->display_links_with_sort2($products_query_numrows,25,MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page'],$parametres); 
				//$products_split->display_links($products_query_numrows, 25, MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page']);
				?>&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
