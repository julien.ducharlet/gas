<?php
/*  Page permettant de gerer (edition/suppression) des parametres du module de livraison depuis l'admin */

  require('includes/application_top.php');
  
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->
<script language="javascript1.1" src="includes/javascript/ajax/module_livraison.js"></script>

<!-- body //-->
<div style="width:100%; height:600px;">

	<div>
    	<?php if($_GET['z_id']==0){ ?>
        Ajouter une zone de livraison
        <?php }
		else{ ?>
    	modification de la zone : <?php echo $_GET['z_id']; ?>
        <?php } ?>
    </div>
    <div style="background-color:#CCC; padding-bottom:30px;">
    <div style="margin-top:30px; margin-left:50px; float:left;">
        <div style="float:left;">
        	<div>liste pays non pr&eacute;sents dans la zone</div>
            <div id="pays_no">
                <select id="pays_not_use" size="30" style="width:300px;">
                    <?php
                    $req_pays_not=tep_db_query("select countries_id, countries_name from ".TABLE_COUNTRIES." where countries_id_zone_livraison <> ".$_GET['z_id']." order by countries_name");
                    while($res_pays_not=tep_db_fetch_array($req_pays_not)){
                    ?>
                   <option value="<?php echo $res_pays_not['countries_id']; ?>"><?php echo $res_pays_not['countries_name']; ?></option>
                    <?php } ?>
                 </select>
             </div>
        </div>
        <div style="float:left; margin-left:50px; width:100px; text-align:center;">
        <br /><br />
        <input type="button" id="add" name="ajouter" value="ajouter" onClick="ajouter_pays(<?php echo $_GET['z_id']; ?>)" /><br /><br /><br />
        <input type="button" id="add" name="supprimer" value="supprimer" onClick="supprime_pays(<?php echo $_GET['z_id']; ?>);" />
        </div>
        
        <div style="float:left; margin-left:50px;">
        	<div>liste pays pr&eacute;sents dans la zone</div>
            <div id="pays_yes">
                <select id="pays_use" size="31" style="width:300px;">
                    <?php
                    $req_pays=tep_db_query("select countries_id, countries_name from ".TABLE_COUNTRIES." where countries_id_zone_livraison=".$_GET['z_id']." order by countries_name");
                    while($res_pays=tep_db_fetch_array($req_pays)){
                    ?>
                   <option value="<?php echo $res_pays['countries_id']; ?>"><?php echo $res_pays['countries_name']; ?></option>
                    <?php }  ?>
                 </select>
           </div>
        </div>
        <div style="clear:both;"></div>
    </div>    
    
    <div style="float:left; margin-left:150px;">
        <div style="margin-top:30px; margin-left:50px;">
         Ajouter une condition de poids<br />
         Poids : <input type="text" id="nouv_poids" name="poids_n" value="" />
         &nbsp;&nbsp;&nbsp;
         Prix: <input type="text" id="nouv_prix" name="prix_n" value="" />
         <input type="button" id="ajouter_pp" name="a_pp" value="ajouter" onClick="ajout_pp(<?php echo $_GET['z_id']; ?>)" />
        </div>
        
        <div style="margin-top:30px; margin-left:50px; width:500px; background-color:#999; margin-bottom:30px;">
        <div>Liste des poids/prix d&eacute;j&agrave; present</div>
        <form method="post" action="module_livraison_edit.php?z_id=<?php echo $_GET['z_id']; ?>" name="poids_prix">
        <div id="liste_pp">
        <?php
            if(isset($_POST['mod'])){
                $i=$_POST['cpt'];
                
                $verif=true;
                for($j=0; $j<$i; $j++){
                    if($_POST['poids_n'.$j]=='' || $_POST['prix_n'.$j]=='')
                    {$verif=false;}
                }
                if($verif==false){
                    echo "Vous avez mal rempli un des champs";
                }
                else{
                    for($j=0; $j<$i; $j++){
                    $up=tep_db_query("update ".TABLE_MODULE_LIVRAISON." set prix_livraison=".$_POST['prix_n'.$j]." where poids_livraison=".$_POST['poids_n'.$j]." and zone_livraison_id=".$_GET['z_id']." ");
                }
                }
            }
            $req_prix=tep_db_query("select * from ".TABLE_MODULE_LIVRAISON." where zone_livraison_id=".$_GET['z_id']."");
            $i=0;
			echo "<span style='margin-left:30px;'></span><span>poids</span><span style='margin-left:122px;'>prix</span>";
            while($res_prix=tep_db_fetch_array($req_prix)){            
        ?>
         <br />
         <span style="margin-left:30px;"></span>
         <input type="text" id="poids<?php echo $i; ?>" name="poids_n<?php echo $i; ?>" value="<?php if(isset($_POST['mod'])){echo $_POST['poids_n'.$i];} else echo $res_prix['poids_livraison']; ?>"  />
         &nbsp;&nbsp;&nbsp;
         <input type="text" id="prix<?php echo $i; ?>" name="prix_n<?php echo $i; ?>" value="<?php if(isset($_POST['mod'])){echo $_POST['prix_n'.$i];} else echo $res_prix['prix_livraison']; ?>" />
         <input type="button" value="supprimer" name="suppr" onClick="supprime(<?php echo $i.','.$_GET['z_id']; ?>)" />
         <?php
            $i++;
            }
         ?>
         <input type="hidden" name="cpt" value="<?php echo $i; ?>" />
         </div>
         <div style="text-align:center; margin-top:10px;"><input type="submit" value="modifier" name="mod" /></div>
         </form>
        </div>
    </div>
    <div style="clear:both"></div>
	</div>
<div style="text-align:center; margin-top:15px;" ><a href="module_livraison.php"><input type="button" value="RETOUR" /></a></div>
</div>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>