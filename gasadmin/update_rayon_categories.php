<?php
/*  Page by Paul, Requête by Paul
	Cette page met à jour la table categories_rubrique avec les sous catégories
	Attention les marques (1er niveau de categories) doivent être entrées manuellement préalablement*/

require('includes/application_top.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>

<script language="javascript">
function verif() {
	if (window.confirm("Voulez-vous vraiment mettre \340 jour la table ?\nAttention les marques (1er niveau de categories) doivent \352tre entr\351es manuellement au pr\351alable\n\nINTERDICTION FORMELLE de s'en servir sans l'aval de l'administrateur du site !")){
		document.location.href="<?php echo $_SERVER['PHP_SELF']; ?>?lancer=true"
		return true;
	}
}
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<div style="text-align: center;">

    
    <?php if ($_GET['effectue']) { ?>
    <div style="background-color: #CFC; border: 1px solid #090; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Base de donn&eacute;es mise &agrave; jour avec <strong>succ&egrave;s</strong> !
    </div>
    <?php } else { ?>
    <div style="background-color: #FFC; border: 1px solid #FC0; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Cette page met &agrave; jour la table categories_rubrique avec les sous cat&eacute;gories.<br />Attention les marques (1er niveau de categories) doivent &ecirc;tre entr&eacute;es manuellement au pr&eacute;alable !
    </div>
    
    <br />
    
    <input type="button" value="Lancer la mise &agrave; jour de la base" onClick="verif();"/>
    <?php } ?>

</div>
<?php
if ($_REQUEST['lancer']) {
	$liste_categories_meres_query = tep_db_query("SELECT * FROM categories_rubrique");
	while ($liste_categories_meres = tep_db_fetch_array($liste_categories_meres_query)) {
		$id_categorie = $liste_categories_meres['categories_id'];
		$id_rayon_categorie = $liste_categories_meres['rubrique_id'];
		
		$liste_categories_filles_query = tep_db_query("SELECT categories_id, parent_id FROM categories WHERE parent_id = '" . $id_categorie . "'");
		while ($liste_categories_filles = tep_db_fetch_array($liste_categories_filles_query)) {
			$id_categorie_fille = $liste_categories_filles['categories_id'];
			$update_categories = tep_db_query("INSERT INTO categories_rubrique VALUES ('" . $id_categorie_fille . "', '" . $id_rayon_categorie . "')");
		}
	}
	?>
	<META HTTP-EQUIV="refresh" content="0;URL='<?php echo $_SERVER['PHP_SELF']; ?>?effectue=true'">
    <?php
}
?>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>