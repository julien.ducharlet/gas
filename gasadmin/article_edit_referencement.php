<?php
/* 
Page : article_edit_referencement2.php
Version du : 09/07/2009
Par : Thierry POULAIN
*/
	
require('includes/application_top.php');

$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
	
if (isset($_GET['cPath'])) $cPath=$_GET['cPath'];
if ( eregi("(insert|update|setflag)", $action) ) include_once('includes/reset_seo_cache.php');

if (tep_not_null($action)) {
	switch ($action) {
		case 'update_product':
			if (isset($HTTP_POST_VARS['edit_x']) || isset($HTTP_POST_VARS['edit_y'])) {
			  $action = 'new_product';
			} 
			else {
				if (isset($HTTP_GET_VARS['pID'])) $products_id = tep_db_prepare_input($HTTP_GET_VARS['pID']);
				
				if(empty($sql_data_array)) $sql_data_array = array();
				
				$update_sql_data = array('products_last_modified' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $update_sql_data);
				tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "'");
				
				$languages = tep_get_languages();
				for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
					
					$language_id = $languages[$i]['id'];
			
					$sql_data_array = array('products_id' => $products_id, 'language_id' => $language_id,
											'products_name' => tep_db_prepare_input($HTTP_POST_VARS['products_name'][$language_id]),
											'products_description' => tep_db_prepare_input($HTTP_POST_VARS['products_description'][$language_id]),
											'products_url' => tep_db_prepare_input($HTTP_POST_VARS['products_url'][$language_id]),
											'products_baseline' => tep_db_prepare_input($HTTP_POST_VARS['products_baseline'][$language_id]),
											'balise_title_lien_image' => tep_db_prepare_input($HTTP_POST_VARS['balise_title_lien_image'][$language_id]),
											'balise_title_lien_texte' => tep_db_prepare_input($HTTP_POST_VARS['balise_title_lien_texte'][$language_id]),
											'products_head_title_tag' => tep_db_prepare_input($HTTP_POST_VARS['products_head_title_tag'][$language_id]),
											'products_head_desc_tag' => tep_db_prepare_input($HTTP_POST_VARS['products_head_desc_tag'][$language_id]),
											'products_head_keywords_tag' => tep_db_prepare_input($HTTP_POST_VARS['products_head_keywords_tag'][$language_id]));                                     
			   
					tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "'");
				}
				tep_redirect(tep_href_link(FILENAME_ARTICLE_EDIT_REFERENCEMENT, 'cPath=' . $cPath . '&pID=' . $products_id . '&action=new_product'));
			}
		break;

		
		case 'new_product':
			$parameters = array('products_name' => '',
								'products_description' => '',
								'products_url' => '',
								'products_baseline' => '',
								'balise_title_lien_image' => '',
								'balise_title_lien_texte' => '',
								'products_id' => '',
								'products_quantity' => '',
								'products_quantity_reel' => '',
								'products_model' => '',
								'products_ref_origine' => '',
								'products_garantie' => '',
								'products_code_barre' => '',
								'products_price' => '',
								'products_cost' => '',
								'products_weight' => '',
								'products_date_added' => '',
								'products_last_modified' => '',
								'products_date_available' => '',
								'products_status' => '',
								'products_tax_class_id' => '',
								'manufacturers_id' => '',
								'family_id' => '',
								'family_client_id' => '',
								'products_quantity_min' => '',
								'products_quantity_max' => '',
								'products_quantity_ideal' => '');
			
			$pInfo = new objectInfo($parameters);
			
			if (isset ($HTTP_GET_VARS['pID']) && (!$HTTP_POST_VARS) ) {
				##with big image AND HEADER TAG
				$product_query = tep_db_query("select pd.products_name, pd.products_description, pd.products_head_title_tag, pd.products_head_desc_tag, pd.products_head_keywords_tag, pd.products_url, pd.products_baseline, pd.balise_title_lien_image, pd.balise_title_lien_texte, p.products_id, p.products_quantity, p.products_quantity_reel, p.products_model, p.products_ref_origine, p.products_garantie, p.products_code_barre, p.products_bimage, p.products_video, p.products_video_preview, p.products_price, p.products_cost, p.products_weight, p.products_date_added, p.products_last_modified, date_format(p.products_date_available, '%Y-%m-%d') as products_date_available, p.products_status, p.products_tax_class_id, p.manufacturers_id, p.family_id,  p.family_client_id, p.products_quantity_min, p.products_quantity_max, p.products_quantity_ideal from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$HTTP_GET_VARS['pID'] . "' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "'");
				$product = tep_db_fetch_array($product_query);
				// HEADER TAG
				$pInfo->objectInfo($product);
				$product_image_query = tep_db_query("select products_bimage from " . TABLE_PRODUCTS_IMAGE . " where products_id = '" . (int)$HTTP_GET_VARS['pID'] . "'");
				$i=2;
				while($product_image=tep_db_fetch_array($product_image_query)){
					$products_bimage_name_supp[$i]=$product_image["products_bimage"];
					$i++;
				}
			} elseif (tep_not_null($HTTP_POST_VARS)) {
				$pInfo->objectInfo($HTTP_POST_VARS);
				$products_name = $HTTP_POST_VARS['products_name'];
				$products_description = $HTTP_POST_VARS['products_description'];
				$products_url = $HTTP_POST_VARS['products_url'];
				$products_baseline = $HTTP_POST_VARS['products_baseline'];
				$balise_title_lien_image = $HTTP_POST_VARS['balise_title_lien_image'];
				$balise_title_lien_texte = $HTTP_POST_VARS['balise_title_lien_texte'];
			}
			
			$manufacturers_array = array(array('id' => '', 'text' => 'Selectionnez une marque'));
			$manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . " order by manufacturers_name");
			while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
				$manufacturers_array[] = array('id' => $manufacturers['manufacturers_id'],
				'text' => $manufacturers['manufacturers_name']);
			}
			
			$family_array = array(array('id' => '', 'text' => 'Selectionnez une famille article'));
			$family_query = tep_db_query("select family_id, family_name from " . TABLE_FAMILY . " order by family_name");
			while ($family = tep_db_fetch_array($family_query)) {
				$family_array[] = array('id' => $family['family_id'],
				'text' => $family['family_name']);
			}
			
			$family_client_array = array(array('id' => '', 'text' => 'Selectionnez une famille client'));
			$family_client_query = tep_db_query("select family_client_id, family_client_name from " . TABLE_CLIENT_FAMILY . " order by family_client_name");
			while ($family_client = tep_db_fetch_array($family_client_query)) {
				$family_client_array[] = array('id' => $family_client['family_client_id'],
				'text' => $family_client['family_client_name']);
			}
			
			$tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
			$tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
			while ($tax_class = tep_db_fetch_array($tax_class_query)) {
				$tax_class_array[] = array('id' => $tax_class['tax_class_id'],
				'text' => $tax_class['tax_class_title']);
			}
			
			$languages = tep_get_languages();
			
			if (!isset($pInfo->products_status)) $pInfo->products_status = '1';
			
			switch ($pInfo->products_status) {
				case '0': $in_status = false; $out_status = true; break;
				case '1':
				default: $in_status = true; $out_status = false;
			}
		break;
	}
}
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <script language="javascript" src="includes/general.js"></script>
        <script language="javascript" src="ajax/categories.js"></script>
    </head>
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
        <?php
            require(DIR_WS_INCLUDES . 'header.php');
            require(DIR_WS_ARTICLE . 'article_menu.php');
            require(DIR_WS_ARTICLE . 'position_article.php');
        
        $parameters = array('products_name' => '',
							'products_description' => '',
							'products_url' => '',
							'products_baseline' => '',
							'balise_title_lien_image' => '',
							'balise_title_lien_texte' => '',
							'products_date_added' => '',
							'products_last_modified' => '',
							'products_date_available' => '',
							'products_status' => '');
		
		$pInfo = new objectInfo($parameters);
		
		$product_query = tep_db_query("select pd.products_name, pd.products_description, pd.products_head_title_tag, pd.products_head_desc_tag, pd.products_head_keywords_tag, pd.products_url, pd.products_baseline, pd.balise_title_lien_image, pd.balise_title_lien_texte, p.products_id, p.products_date_added, p.products_last_modified from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$HTTP_GET_VARS['pID'] . "' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "'");
		$product = tep_db_fetch_array($product_query);
		$pInfo->objectInfo($product);
		
		$languages = tep_get_languages();
		
		echo tep_draw_form('new_product', FILENAME_ARTICLE_EDIT_REFERENCEMENT, 'cPath=' . $cPath . '&pID=' . $HTTP_GET_VARS['pID'] . '&action=update_product', 'post', 'enctype="multipart/form-data"');
		?>
    
      <div style="float: left; width: 50%;">
        <span style="padding: 10px;" class="pageHeading">Nom et Description de l'article</span>
      	<?php $hauteur = 360; require(DIR_WS_ARTICLE . 'article_name_description.php');	?>
      </div>
    
      <div style="float: right; width: 50%;">
          <span style="padding: 10px;" class="pageHeading">Metadonnées de l'article</span>
          
          <div style="margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
              <table>
                  <?php
                  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                  ?>
                    <tr>
                          <td><?php if ($i == 0) echo 'Balise TITLE au survol de ce produit pour les images'; ?></td>
                          <td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                      <td><?php echo tep_draw_input_field('balise_title_lien_image[' . $languages[$i]['id'] . ']', (isset($balise_title_lien_image[$languages[$i]['id']]) ? $products_url[$languages[$i]['id']] : tep_get_products_balise_title_lien_image($pInfo->products_id, $languages[$i]['id'])),'size="73"'); ?></td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td><?php if ($i == 0) echo 'Balise TITLE au survol de ce produit pour le texte'; ?></td>
                          <td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                      <td><?php echo tep_draw_input_field('balise_title_lien_texte[' . $languages[$i]['id'] . ']', (isset($balise_title_lien_texte[$languages[$i]['id']]) ? $products_url[$languages[$i]['id']] : tep_get_products_balise_title_lien_texte($pInfo->products_id, $languages[$i]['id'])),'size="73"'); ?></td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td valign="top"><?php if ($i == 0) echo 'BASELINE de cette cat&eacute;gorie pour le r&eacute;f&eacute;rencement'; ?></td>
                          <td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                      <td><?php echo tep_draw_textarea_field('products_baseline[' . $languages[$i]['id'] . ']', 'soft', '70', '2', (isset($products_baseline[$languages[$i]['id']]) ? stripslashes($products_baseline[$languages[$i]['id']]) : tep_get_products_baseline($pInfo->products_id, $languages[$i]['id']))); ?></td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style="width: 400px;"><?php if ($i == 0) echo 'URL de l\'article <span class="smalltext">(idem au nom si vide)</span>'; ?></td>
                          <td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                      <td><?php echo tep_draw_input_field('products_url[' . $languages[$i]['id'] . ']', (isset($products_url[$languages[$i]['id']]) ? $products_url[$languages[$i]['id']] : tep_get_products_url($pInfo->products_id, $languages[$i]['id'])),'size="73"'); ?></td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td valign="top"><?php if ($i == 0) echo 'Titre de la page de l\'article <i><span class="smalltext">( Pas plus de 100 caractères )</span></i> :'; ?></td>
                          <td>
          
                              <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                      <td><?php echo tep_draw_textarea_field('products_head_title_tag[' . $languages[$i]['id'] . ']', 'soft', '70', '1', (isset($products_head_title_tag[$languages[$i]['id']]) ? stripslashes($products_head_title_tag[$languages[$i]['id']]) : tep_get_products_head_title_tag($pInfo->products_id, $languages[$i]['id']))); ?></td>
                                  </tr>
                              </table>
                          </td>
                      </tr>         
                      <tr>
                          <td valign="top"><?php if ($i == 0) echo 'Description de l\'article <i><span class="smalltext">( Pas plus de 200 caractères )</span></i> : '; ?></td>
                          <td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                      <td><?php echo tep_draw_textarea_field('products_head_desc_tag[' . $languages[$i]['id'] . ']', 'soft', '70', '2', (isset($products_head_desc_tag[$languages[$i]['id']]) ? stripslashes($products_head_desc_tag[$languages[$i]['id']]) : tep_get_products_head_desc_tag($pInfo->products_id, $languages[$i]['id']))); ?></td>
                                  </tr>
                              </table>
                          </td>
                      </tr>        
                      <tr>
                          <td valign="top"><?php if ($i == 0) echo 'Mots cl&eacute;s de l\'article <i><span class="smalltext">( Pas plus de 1000 caract&egrave;res )</span></i> : '; ?></td>
                          <td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                      <td><?php echo tep_draw_textarea_field('products_head_keywords_tag[' . $languages[$i]['id'] . ']', 'soft', '70', '5', (isset($products_head_keywords_tag[$languages[$i]['id']]) ? stripslashes($products_head_keywords_tag[$languages[$i]['id']]) : tep_get_products_head_keywords_tag($pInfo->products_id, $languages[$i]['id']))); ?></td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  <?php
                  }
                  ?>
              </table>
						</div>
        </div>
        
        <div style="clear: both;"></div>
        
        <div style="margin: auto; width: 65px; margin-bottom: 10px; padding: 5px; border: 1px solid #090; background-color: #EFF; text-align: center;">
					<?php 
            echo tep_draw_hidden_field('products_date_added', (tep_not_null($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d')));
            echo tep_image_submit('button_update.gif', IMAGE_UPDATE);
          ?>
        </div>
        </form>	
        
        <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
    </body>
</html>

<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
