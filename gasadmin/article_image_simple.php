<?php
  require('includes/application_top.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
    </table></td>
    <td width="100%" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Produits avec une seule image sur <?php echo STORE_NAME; ?> </td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
          <tr>
          	<td colspan="2"><br />Famille :
            <?php
            $req_fam='SELECT family_id, family_name FROM family ORDER BY family_name';
  			$res_fam=tep_db_query($req_fam);
			?>
            <form name="form" action="produit_image_simple.php" method="post">
			<select name="family" onChange="document.form.submit()">
				<?php
				while($row_fam=tep_db_fetch_array($res_fam)){
					$selected="";
					if($row_fam["family_id"]==$_POST['family']){$selected="selected";}
					echo '<option value="'.$row_fam["family_id"].'" '.$selected.'>'.$row_fam["family_name"].'</option>';
				}?>
			</select>
			&nbsp;&nbsp;
			</form>	
            </td>
          </tr>
        </table>
		</td>
      </tr>
	  <tr>
		<td align="left">
			<?php
				$requete_prod_sans_image = "SELECT p.products_id, p.products_model, pd.products_name
										 FROM ". TABLE_PRODUCTS_DESCRIPTION ." pd inner join ". TABLE_PRODUCTS ." p on p.products_id=pd.products_id
										 LEFT JOIN ". TABLE_PRODUCTS_IMAGE ." pi ON pi.products_id = p.products_id
										 WHERE pi.index_image IS NULL";
				
				if(isset($_POST["family"]) && !empty($_POST['family'])) {
					
					$requete_prod_sans_image .= " AND p.family_id='".$_POST['family']."'";
				}
				$requete_prod_sans_image .= " order by products_model";
				$res_prod_sans_image = tep_db_query($requete_prod_sans_image);
				$i = 1;
				
				echo '<table class="infoBoxContent" width="800" cellspacing="5">';
				
				echo '<tr>';
					echo '<td>R�f�rence</td>';
					echo '<td>Article</td>';
					echo '<td>Action</td>';
				echo '</tr>';
				
				while($row = tep_db_fetch_array($res_prod_sans_image)) {
					
					$id_produit = $row['products_id'];
					//$cPath = $row["parent_id"] . "_" . $row["categories_id"];
					echo '<tr>';
					echo '<td>'. $row['products_model'] .'</td>';
					echo '<td>'. bda_product_name_replace('', $row['products_name']) .'</td>';
					echo '<td>';
						echo '<a href="article_edit.php?cPath='. $cPath .'&pID='. $id_produit .'&action=new_product" target="_blank">'. tep_image(DIR_WS_IMAGES .'icons/editer.png', 'Editer le produit') .'</a>';
					echo '</td>';
                   echo "</tr>\n";
				}
				echo '</table>';
			?>		
		</td>
	  </tr>
	  </table>
	 </td>
	</tr>
  </table></td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
