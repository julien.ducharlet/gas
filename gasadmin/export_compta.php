<?php require('includes/application_top.php');?>
<link href="includes/stylesheet.css" rel="stylesheet" type="text/css">

<?php

$dir = "telechargement/";     // chemin sur le serveur ou l'on veut créer le fichier
$nomFichier = "compta.csv";   //nom du fichier xls
$fp = fopen($dir.$nomFichier, "w+");      // ouverture et création du fichier

$line="Date,Journal,Compte,Piece,Libelle,Debit,Credit";
fputcsv2($fp, split(',', $line));

if ( (isset($HTTP_GET_VARS['commandes']) && strlen($HTTP_GET_VARS['commandes'])>0) || (isset($_POST['commandes']) && strlen($_POST['commandes'])>0)) {
	
	$id_commandes = (isset($HTTP_GET_VARS['commandes']) && strlen($HTTP_GET_VARS['commandes'])>0) ? ltrim($HTTP_GET_VARS['commandes']) : ltrim($_POST['commandes']);
	
	$tableau_cmd = explode(' ', ($id_commandes));
	$last_total = 0;
	
	foreach($tableau_cmd as $cmd) {
		
		$orders_query = "select o.orders_id, o.payment_method, o.customers_id, o.customers_country, o.frais_port_client, o.customers_name, o.orders_numero_facture, o.customers_company, o.tva_port, o.total, o.tva_total, o.orders_date_facture, o.marge, remise_porte_monnaie from " . TABLE_ORDERS . " o where orders_id = '" .$cmd. "' and o.orders_numero_facture>0";
		$orders_query_res = tep_db_query($orders_query);
		
    	while ($orders = tep_db_fetch_array($orders_query_res)) {
			
			$port = $orders["frais_port_client"];
			
			
			if($orders["total"]==0) {
				
				$ht = 0;
				$orders["tva_total"] = 0;
			}
			else {
				
				$ht = $orders["total"]-$orders["tva_total"]-$port;
			}
			$country_id = get_tva_country($orders["customers_country"]);
			$pay_meth = $orders['payment_method'];
			
			if(substr_count($orders['payment_method'],"Carte")>0) {
				
				$pay_meth = "CB";
			}
			elseif(substr_count(strtoupper($orders['payment_method']),strtoupper("Chèque"))>0) {
				
				//copier coller de cmd_list ou ca marche, mais la ca marche pas alors j'ai rajouté le if du dessous (LUDO)
				$pay_meth = "CHQ";
			}
			elseif(strpos($orders['payment_method'],"que")>0) {
				
				//si on teste sur chèque => problème avec l'accent
				$pay_meth = "CHQ";
			}
			elseif(substr_count($orders['payment_method'],"Virement")>0) {
				
				$pay_meth = "VIR";
			}				
			elseif(substr_count($orders['payment_method'],"Mandat Cash")>0) {
				
				$pay_meth = "ESP";
			}
			
			switch($country_id) {
				
				case "FR":$compte = "707100"; break;
				case "CEE":$compte = "707200"; break;
				case "AUTRE":$compte = "707300"; break;
				case "DOM":$compte = "707400"; break;
				case "TOM":$compte = "707500"; break;
			}
			
			$tab_date_facture = explode(' ',$orders["orders_date_facture"]);
			$date_facture = $tab_date_facture[0];
			
			if($last_total* $orders["total"]<0) {
				
				fputcsv2($fp, "");
			}
			
			$line = $date_facture.";VE;9A".$orders["customers_id"].";".$orders["orders_numero_facture"].";".$orders["customers_name"].";".str_replace('.',',',$orders["total"])."; ;E;".$pay_meth;
			
			/*if($orders['remise_porte_monnaie']!="0.00") {
				
				$line .= ' + Porte Monnaie';
			}*/
			
			fputcsv2($fp, explode(';', $line));
			$line = $date_facture.";VE;".$compte.";".$orders["orders_numero_facture"].";".$orders["customers_name"]."; ;".str_replace('.',',',$ht).";E;";
			fputcsv2($fp, explode(';', $line));
			$line = $date_facture.";VE;708500;".$orders["orders_numero_facture"].";".$orders["customers_name"]."; ;".str_replace('.',',',$port).";E;";
			fputcsv2($fp, explode(';', $line));
			$line = $date_facture.";VE;445711;".$orders["orders_numero_facture"].";".$orders["customers_name"]."; ;".str_replace('.',',',$orders["tva_total"]).";E;";
			fputcsv2($fp, explode(';', $line));
			
			$last_total = $orders["total"];
		}
	}
}

fclose($fp);
?>
<div style="margin:100px; text-align:center;"><form action="<?php echo $dir.$nomFichier; ?>"><input type="submit" value="Cliquez ici pour exporter" /></form></div>

<?php
// $delimiter = ';' permet de modifier le delimitateur
function fputcsv2 ($fh, $fields, $delimiter = ';', $enclosure = '"', $mysql_null = false) {
	
    $delimiter_esc = preg_quote($delimiter, '/');
    $enclosure_esc = preg_quote($enclosure, '/');

    $output = array();
	
    foreach ($fields as $field) {
		
        if ($field === null && $mysql_null) {
			
            $output[] = 'NULL';
            continue;
        }

        $output[] = preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) ? ($enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure) : $field;
    }

    fwrite($fh, join($delimiter, $output) . "\n");
} 
?>