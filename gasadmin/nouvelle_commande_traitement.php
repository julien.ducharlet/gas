<?php
// Page permettant de faire les enregistrement en base lors d"une commande de l'admin ou de la creation d'un devis*/
// Penser à modifier le fichier gas/panier_traitement.php

session_start();

require('/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/configure_spplus.php');
require("paiement_ssplus_admin.php");
function traitement_commande(){
	//Variables pour le paiement
	####Carte Bleue####
	$ID_MAGASIN=ID_MAGASIN_SPPLUS;
	$MAIL=STORE_OWNER_EMAIL_ADDRESS;
	$MAIL_TECHNIQUE=STORE_OWNER_EMAIL_ADDRESS;
	$SIRET=SIRET_SPPLUS;
	
	
	$today = date("Y-m-d H:i:s");
	  
	$info_client=tep_db_query("	SELECT * 
								FROM ".TABLE_CUSTOMERS." c, ".TABLE_ADDRESS_BOOK." a 
								WHERE c.customers_id=a.customers_id 
									 AND c.customers_id=".$_SESSION['id_client_panier']." 
									 AND a.address_book_id = c.customers_default_address_id");
	$res=tep_db_fetch_array($info_client);
	
	$pays_customer=tep_db_query("SELECT countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id=".$res['entry_country_id']."");
	$res_pays_customer=tep_db_fetch_array($pays_customer);
	
	$info_delivery=tep_db_query("SELECT * FROM ".TABLE_ADDRESS_BOOK." WHERE address_book_id=".$_SESSION['adresse_commande_admin']."");
	$res2=tep_db_fetch_array($info_delivery);
	
	$pays_adresse=tep_db_query("SELECT countries_name FROM ".TABLE_COUNTRIES." WHERE countries_id=".$res2['entry_country_id']."");
	$res_pays=tep_db_fetch_array($pays_adresse);
	
	//debut devis
	if (isset($_SESSION['devis_commande_admin']) && $_SESSION['devis_commande_admin']==1) {
		$nom_paiement='';
		$statut_commande=21;
		$date_fin_devis="'".$_SESSION['date_fin_devis']."'";
	} else {
		$moyen_p=tep_db_query("SELECT * FROM ".TABLE_MODULE_PAIEMENT." WHERE paiement_id=".$_SESSION['moyen_paiement_panier_admin']."");
		$res_paiement=tep_db_fetch_array($moyen_p);
		$nom_paiement=$res_paiement['paiement_nom'];
		$statut_commande=$res_paiement['paiement_statut'];
		$date_fin_devis="NULL";
	}
	//fin devis
	  		 
	$visite=tep_db_query("	SELECT * 
							FROM ".TABLE_VISITE."
							WHERE id_client=".$_SESSION['id_client_panier']." 
						   	AND id_visite = (	SELECT max(id_visite) 
												FROM visite 
												WHERE id_client=".$_SESSION['id_client_panier'].")");
	$res_visite=tep_db_fetch_array($visite);
	  
	// extraction adresse IP du visiteur 
	$ip = $_SERVER["REMOTE_ADDR"];
	// transformation IP 
	$dotted = preg_split( "/[.]+/", $ip);
	$ip2 = (double) ($dotted[0]*16777216)+($dotted[1]*65536)+($dotted[2]*256)+($dotted[3]);    
                    
	$country_query = tep_db_query("	SELECT country_name 
									FROM `".TABLE_IP_TO_COUNTRY."`
									WHERE ".$ip2." BETWEEN ip_from AND ip_to");
	$pays = tep_db_fetch_array($country_query);
	
	//$remise=$_SESSION['remise_panier']+$_SESSION['remise_panier_av'];
	 
	// Infos sur le client
	$customers_id = $_SESSION['id_client_panier'];
	//$customers_name = $res['customers_firstname'] . " " . $res['customers_lastname'];
	//$customers_compagny = $res['customers_company'];
	$customers_name = $res['entry_firstname'] . " " . $res['entry_lastname'];
	$customers_compagny = $res['entry_company'];
	$customers_street_address_3 = $res['entry_street_address_3'];
	$customers_suburb = $res['entry_suburb'];
	$customers_street_address = $res['entry_street_address'];
	$customers_street_address_4 = $res['entry_street_address_4'];
	$customers_city = $res['entry_city'];
	$customers_postcode = $res['entry_postcode'];
	$customers_country = $res_pays_customer['countries_name'];
	$customers_telephone = $res['customers_telephone'];
	$customers_email_address = $res['customers_email_address'];
	
	//Infos sur la livraison
	$delivery_name = $res2['entry_firstname']." ".$res2['entry_lastname'];
	$delivery_company = $res2['entry_company'];
	$delivery_street_address_3 = $res2['entry_street_address_3'];
	$delivery_suburb = $res2['entry_suburb'];
	$delivery_street_address = $res2['entry_street_address'];
	$delivery_street_address_4 = $res2['entry_street_address_4'];	
	$delivery_city = $res2['entry_city'];
	$delivery_postcode = $res2['entry_postcode'];
	$delivery_country = $res_pays['countries_name'];
	
	//Infos sur la commande
	$payment_method = $nom_paiement;
	$date_purchased = $today;
	$orders_status = $statut_commande;
	$orders_date_fin_devis = $date_fin_devis;
	
	//Infos stats
	$customer_ip = $_SERVER["REMOTE_ADDR"];
	$customer_ip_country = $pays['country_name'];
	$referer = $res_visite['referer'];
	$referer_host = $res_visite['referer_host'];
	$visiteur_payant = $res_visite['payant'];
	$visiteur_mot_cle = $res_visite['mot_cle'];
	
	//Infos paiement
	$ss_total = round($_SESSION['montant_ht_commande_admin'],2);
	$tva_total = correction_tva(($_SESSION['montant_ht_commande_admin']+$_SESSION['livraison_commande_admin'])*($_SESSION['taux_tva_commande_admin']-1));
	$tva_total = $tva_total['arrondie'];
	
	if(isset($_SESSION['remise_panier_pourcent']) && !empty($_SESSION['remise_panier_pourcent'])){
		$remise_pourcent=$_SESSION['remise_panier_pourcent'];
	} else {
		$remise_pourcent=0;
	}
	
	$remise_porte_monnaie = round($_SESSION['remise_porte_monnaie_admin'],2);
	$frais_port_client = round($_SESSION['livraison_commande_admin'],2);
	$tva_port = $_SESSION['tva_frais_de_port'];
	$poids = round($_SESSION['poids_commande_admin'],2);
	
	if ($remise_porte_monnaie > 0) {
		$total_remise_ttc = correction_tva($remise_porte_monnaie*1.2);
		$total_remise_ttc = $total_remise_ttc['arrondie'];
	} else {
		$total_remise_ttc = 0;
	} 
	
	$total = ($ss_total+$frais_port_client+$tva_total)-$total_remise_ttc;
	$total = round($total, 2);
	
	
	$order_info = tep_db_query("
									INSERT INTO ".TABLE_ORDERS." (
										customers_id, 
										customers_name, 
										customers_company, 
										customers_street_address_3, 
										customers_suburb, 
										customers_street_address, 
										customers_street_address_4, 
										customers_city, 
										customers_postcode, 
										customers_country, 
										customers_telephone, 
										customers_email_address, 
										customers_address_format_id, 
										delivery_name, 
										delivery_company, 
										delivery_street_address_3, 
										delivery_suburb, 
										delivery_street_address, 
										delivery_street_address_4, 
										delivery_city, 
										delivery_postcode, 
										delivery_country, 
										delivery_address_format_id, 
										payment_method, 
										date_purchased, 
										orders_status, 
										orders_date_fin_devis, 
										currency, 
										currency_value, 
										customer_ip, 
										customer_ip_country, 
										referer, 
										referer_host, 
										visiteur_payant, 
										visiteur_mot_cle,
										total, 
										ss_total, 
										tva_total, 
										remise, 
										remise_pourcent, 
										remise_porte_monnaie, 
										frais_port_client, 
										tva_port, 
										poids
									) VALUES (
										".$customers_id.", 
										'".tep_db_input($customers_name)."', 
										'".tep_db_input($customers_compagny)."', 
										'".tep_db_input($customers_street_address_3)."', 
										'".tep_db_input($customers_suburb)."', 
										'".tep_db_input($customers_street_address)."', 
										'".tep_db_input($customers_street_address_4)."', 
										'".tep_db_input($customers_city)."', 
										'".tep_db_input($customers_postcode)."', 
										'".tep_db_input($customers_country)."', 
										'".tep_db_input($customers_telephone)."', 
										'".$customers_email_address."', 
										1, 
										'".tep_db_input($delivery_name)."', 
										'".tep_db_input($delivery_company)."', 
										'".tep_db_input($delivery_street_address_3)."', 
										'".tep_db_input($delivery_suburb)."', 
										'".tep_db_input($delivery_street_address)."', 
										'".tep_db_input($delivery_street_address_4)."', 
										'".tep_db_input($delivery_city)."', 
										'".tep_db_input($delivery_postcode)."', 
										'".tep_db_input($delivery_country)."', 
										1, 
										'".$payment_method."', 
										'".$date_purchased."', 
										'".$orders_status."', 
										".$orders_date_fin_devis.", 
										'EUR', 
										1, 
										'".$customer_ip."', 
										'".$customer_ip_country."', 
										'".$referer."', 
										'".$referer_host."', 
										'".$visiteur_payant."', 
										'".$visiteur_mot_cle."', 
										".$total.", 
										".$ss_total.", 
										".$tva_total.", 
										0, 
										".$remise_pourcent.", 
										".$remise_porte_monnaie.", 
										".$frais_port_client.", 
										".$tva_port.", 
										".$poids."
									)
								") ;
	 
	 
				
	//recuperation de l'id de la commande	
	$order=tep_db_query("SELECT max(orders_id) as max FROM ".TABLE_ORDERS." WHERE customers_id=" . $_SESSION['id_client_panier'] . "");
	$oid=tep_db_fetch_array($order);
	$oid['max'];
	
	$nb_produits=0;
	for($i=0;$i<count($_SESSION['panier_admin']['products_id']);$i++){
		
		$id_produit = $_SESSION['panier_admin']['products_id'][$i];
		$option = $_SESSION['panier_admin']['option_id'][$i];
		$option_value = $_SESSION['panier_admin']['option_values_id'][$i];
		$qte = $_SESSION['panier_admin']['qte'][$i];
		$prix = $_SESSION['panier_admin']['products_price'][$i];
		
		$cPath_explode = explode("_", $_SESSION['panier_admin']['cPath'][$i]);
		$rayon = $cPath_explode[0];
		$cPath = $cPath_explode[1] . "_" . $cPath_explode[2];
		
		$article_query = tep_db_query("SELECT p.products_id, p.products_model, p.products_price, pd.products_name, p.products_cost, p.products_quantity, products_weight
                                      FROM ".TABLE_PRODUCTS_DESCRIPTION." as pd, ".TABLE_PRODUCTS." as p
                                      WHERE pd.products_id = '" . $id_produit . "' AND pd.products_id = p.products_id");
		$article_data = tep_db_fetch_array($article_query);
		
		$reponse_nom_categorie = tep_db_query("SELECT categories_name, categories_id
										   FROM categories_description
										   WHERE categories_id = '" . $cPath_explode[2] . "'");
		$categorie = tep_db_fetch_array($reponse_nom_categorie);
		
		$nom_article = bda_product_name_transform($article_data['products_name'], $categorie['categories_id']);
		
		//devis
		if ($_SESSION['devis_commande_admin']!=1 || empty($_SESSION['devis_commande_admin'])) {
			$products_quantity = tep_db_query("UPDATE ".TABLE_PRODUCTS." SET products_quantity=".($article_data['products_quantity']-$qte).", products_ordered=products_ordered + ". $qte ." WHERE products_id=".$article_data['products_id']."");
		}//fin devis
		
		$orders_products_panier = tep_db_query("INSERT INTO ".TABLE_ORDERS_PRODUCTS." (orders_id, products_id, id_rayon, cpath, products_model, products_name, products_weight, products_price, products_cost, final_price, products_tax, products_quantity) VALUES (".$oid['max'].", ".$article_data['products_id'].", '" . $rayon . "', '" . $cPath . "', '".addslashes($article_data['products_model'])."', '".addslashes($nom_article)."', '". $article_data['products_weight'] ."', ".$article_data['products_price'].", ".$article_data['products_cost'].", ".$prix.",  20, ".$qte.")");
		
	  
		if ($_SESSION['devis_commande_admin']!=1 || empty($_SESSION['devis_commande_admin'])) {// pas de devis
			
			$alert_stock=tep_db_query("SELECT products_quantity, products_quantity_min FROM ".TABLE_PRODUCTS." WHERE products_id=".$article_data['products_id']."");
			$res_alert=tep_db_fetch_array($alert_stock);
			if ($res_alert['products_quantity'] <= $res_alert['products_quantity_min']) {
				mail_stock_alert($article_data['products_id'],$article_data['products_name'],$article_data['products_model']);
			}
		}
	  
		if($option!=0) {
		
			$req1=tep_db_query("SELECT products_options_name FROM ".TABLE_PRODUCTS_OPTIONS." WHERE products_options_id='".$option."'");
			$res1=tep_db_fetch_array($req1);
			$req2=tep_db_query("SELECT products_options_values_name FROM ".TABLE_PRODUCTS_OPTIONS_VALUES." WHERE products_options_values_id='".$option_value."'");
			$res2=tep_db_fetch_array($req2);
			$op=$res1['products_options_name'];
			$op_val=$res2['products_options_values_name'];
			
			$products_details = tep_db_query("SELECT max(orders_products_id) as max FROM ".TABLE_ORDERS_PRODUCTS." WHERE orders_id=". $oid['max'] ."");
			$res_pdeatails=tep_db_fetch_array($products_details);
		   
			$products_options = tep_db_query('INSERT INTO '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES .' (orders_id, orders_products_id, options_values_id, products_options, products_options_values) value ('. $oid['max'] .', '. $res_pdeatails['max'] .', \''. $option_value .'\', \''. addslashes($op) .'\', \''. addslashes($op_val) .'\')');
		   
			if($_SESSION['devis_commande_admin']!=1 || empty($_SESSION['devis_commande_admin'])) {// pas de devis
	   			
				$query_update = 'UPDATE '. TABLE_PRODUCTS_ATTRIBUTES .' SET options_quantity=options_quantity-'. $qte .' WHERE products_id='. $article_data['products_id'] .' AND options_values_id='. $option_value;
				tep_db_query($query_update);
				
				//alerte de stock spécifique pour une option
				$alert_stock_options = 'SELECT options_quantity, options_quantity_min, products_options_name, products_options_values_name FROM '. TABLE_PRODUCTS_ATTRIBUTES .' pa, '. TABLE_PRODUCTS_OPTIONS .' po, '. TABLE_PRODUCTS_OPTIONS_VALUES .' pov
										WHERE pa.options_id=po.products_options_id
										and pa.options_values_id=pov.products_options_values_id
										and products_id=\''. $article_data['products_id'] .'\'
										and options_values_id = \''. $option_value .'\'';
				$alert_stock_options = tep_db_query($alert_stock_options);
				$res_alert_options = tep_db_fetch_array($alert_stock_options);
					
				if($res_alert_options['options_quantity']<=$res_alert_options['options_quantity_min']) {
					mail_stock_alert($article_data['products_id'], $article_data['products_name'], $article_data['products_model'] .' - '. $res_alert_options['products_options_name'] .' => '. $res_alert_options['products_options_values_name']);
				}
			}	
	   }
	   $qte_panier+=$qte;
	   $marge_panier+=($prix-$article_data['products_cost'])*$qte;			
	}
	 //fin while

// debut devis
if ($_SESSION['devis_commande_admin']!=1 || empty($_SESSION['devis_commande_admin'])) {
		
	if (isset($_SESSION['remise_porte_monnaie_admin']) && $_SESSION['remise_porte_monnaie_admin']>0) {
		$marge_finale=$marge_panier-$_SESSION['remise_porte_monnaie_admin'];
		$orders_quantity = tep_db_query("UPDATE ".TABLE_ORDERS." SET nb_articles=".$qte_panier.", marge=".$marge_finale." WHERE orders_id=".$oid['max']."");
	} else {
		$orders_quantity = tep_db_query("UPDATE ".TABLE_ORDERS." SET nb_articles=".$qte_panier.", marge=".$marge_panier." WHERE orders_id=".$oid['max']."");
	}
	
	if (isset($_SESSION['remise_porte_monnaie_admin']) && $_SESSION['remise_porte_monnaie_admin']>0) {
		$c="Utilisation du porte monnaie virtuel";
		$orders_status_history = tep_db_query("INSERT INTO ".TABLE_ORDERS_STATUS_HISTORY." (orders_id, orders_status_id, date_added, customer_notified, comments) VALUES (".$oid['max'].", ".$statut_commande.", '".$today."', 1, '".$c."' )");
	} else {
		if (isset($_SESSION["orders_comments"]) && !empty($_SESSION["orders_comments"])) {
			$com=$_SESSION["orders_comments"];
		} else {
			$com="";
		}
		$orders_status_history = tep_db_query("INSERT INTO ".TABLE_ORDERS_STATUS_HISTORY." (orders_id, orders_status_id, date_added, customer_notified, comments) VALUES (".$oid['max'].", ".$statut_commande.", '".$today."', 1, '".tep_db_input($com)."')");
		unset($_SESSION["orders_comments"]);
	}
} //fin devis
	 
	$panier_client_supr=tep_db_query("delete FROM ".TABLE_CUSTOMERS."_basket WHERE customers_id=".$_SESSION['id_client_panier']."");
	$panier_attributs_client_supr=tep_db_query("delete FROM ".TABLE_CUSTOMERS_BASKET_ATTRIBUTES." WHERE customers_id=".$_SESSION['id_client_panier']."");
	
	
	
	//mail client
	$mail=$res['customers_email_address'];
	if(isset($_SESSION['devis_commande_admin']) && $_SESSION['devis_commande_admin']==1){
		$sujet=SUJET_DEVIS_DISPO;
		$mess=mail_contenu_devis($oid['max']);
		$mess_admin=mail_contenu_devis_admin($oid['max']);
	} else { 
		$sujet = SUJET_TRAITEMENT_CMD; 
		$mess=mail_contenu_commande($oid['max']);
		//mail admin
		$mess_admin=mail_contenu_commande_admin($oid['max']);
	}
	 
//mail admin
$mess_admin=mail_contenu_commande_admin($oid['max']);

if($_SESSION['devis_commande_admin']!=1 || empty($_SESSION['devis_commande_admin'])){
	
	//maj client info
	$customers_info=tep_db_query("UPDATE ".TABLE_CUSTOMERS_INFO." SET customers_info_date_of_last_order='".$today."' WHERE customers_info_id=".$_SESSION['id_client_panier']."");
		
	if($_SESSION['moyen_paiement_panier_admin']==1) {
		echo lien_paiement_spplus($ID_MAGASIN,$SIRET,$oid['max']);
	} else {
		 
		if(isset($_SESSION['remise_porte_monnaie_admin']) && $_SESSION['remise_porte_monnaie_admin']>0 ){
			
			$somme_av=tep_db_query("SELECT customers_argent_virtuel FROM ".TABLE_CUSTOMERS." WHERE customers_id=".$_SESSION['id_client_panier']."");
			$res_av=tep_db_fetch_array($somme_av);
			
			$nouv_m=$res_av['customers_argent_virtuel']-$_SESSION['remise_porte_monnaie_admin'];
			$r='Suite a la commande : '.$oid['max'].' votre porte monnaie a ete debite de '.$_SESSION['remise_porte_monnaie_admin'].' Euro';
			
			$update_cli_av=tep_db_query("UPDATE ".TABLE_CUSTOMERS." SET customers_argent_virtuel=".$nouv_m." WHERE customers_id=".$_SESSION['id_client_panier']."");
			$inset_cli = tep_db_query("INSERT INTO ".TABLE_CUSTOMERS_ARGENT." (customers_id, somme, raison, raison_client, date_versement, user_id) VALUES ('".$_SESSION['id_client_panier']."','".$_SESSION['remise_porte_monnaie_admin']."','".$r."','".$r."','".$today."',999)");
		}
		
		mail_cmd_admin($sujet,$mess_admin);
		mail_client_commande($mail,$sujet,$mess);
		$_SESSION['id_client_panier']='';
		$_SESSION['remise_porte_monnaie_admin']='';
		$_SESSION['montant_panier_admin']='';
		$_SESSION['montant_ht_commande_admin']='';
		$_SESSION['livraison_commande_admin']='';
		$_SESSION['remise_panier_pourcent']='';
		$_SESSION['taux_tva_commande_admin']='';
		$_SESSION['livraison_commande_admin']='';
		$_SESSION['tva_frais_de_port']='';
		$_SESSION['poids_commande_admin']='';
		$_SESSION['devis_commande_admin']='';
		$_SESSION['adresse_commande_admin']='';
		$_SESSION['date_fin_devis']='';
		
		$_SESSION['panier_admin']=array();
		$_SESSION['panier_admin']['products_id']=array();
		$_SESSION['panier_admin']['option_id']=array();
		$_SESSION['panier_admin']['option_values_id']=array();
		$_SESSION['panier_admin']['qte']=array();
		$_SESSION['panier_admin']['products_price']=array();
		 
		echo '<meta http-equiv="refresh" content="0; URL=cmd_edit.php?oID='.$oid['max'].'&action=edit">';
	}
	
} else { //devis
	
	mail_cmd_admin($sujet,$mess_admin);
	mail_client_commande($mail,$sujet,$mess);
	$_SESSION['id_client_panier']='';
	$_SESSION['remise_porte_monnaie_admin']='';
	$_SESSION['montant_panier_admin']='';
	$_SESSION['montant_ht_commande_admin']='';
	$_SESSION['livraison_commande_admin']='';
	$_SESSION['remise_panier_pourcent']='';
	$_SESSION['taux_tva_commande_admin']='';
	$_SESSION['livraison_commande_admin']='';
	$_SESSION['tva_frais_de_port']='';
	$_SESSION['poids_commande_admin']='';
	$_SESSION['devis_commande_admin']='';
	$_SESSION['adresse_commande_admin']='';
	$_SESSION['date_fin_devis']='';
	 
	$_SESSION['panier_admin']=array();
	$_SESSION['panier_admin']['products_id']=array();
	$_SESSION['panier_admin']['option_id']=array();
	$_SESSION['panier_admin']['option_values_id']=array();
	$_SESSION['panier_admin']['qte']=array();
	$_SESSION['panier_admin']['products_price']=array();
	
	$orders_quantity = tep_db_query("UPDATE ".TABLE_ORDERS." SET nb_articles=".$qte_panier.", marge=".$marge_panier." WHERE orders_id=".$oid['max']."");
	
	
	if(isset($_SESSION["orders_comments"]) && !empty($_SESSION["orders_comments"])){
		$com=$_SESSION["orders_comments"];
	} else {
		$com="";
	}
	  
	$orders_status_history = tep_db_query("INSERT INTO ".TABLE_ORDERS_STATUS_HISTORY." (orders_id, orders_status_id, date_added, customer_notified, comments) VALUES (".$oid['max'].", 21, '".$today."', 1, '".tep_db_input($com)."')");
	unset($_SESSION["orders_comments"]);
	
	echo '<meta http-equiv="refresh" content="0; URL=cmd_edit.php?oID='.$oid['max'].'&action=edit">';
	
}
	
	//fin devis
}
?>