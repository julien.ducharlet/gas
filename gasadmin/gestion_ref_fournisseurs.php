<?php
/*
  PAGE: gestion_ref_fournisseurs.php
  Version du 27/06/2007
*/

  require('includes/application_top.php');

 
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">

<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
	
	<? if(isset($_POST['enregistrer']))
		 {
		   for($i=0;$i<$_POST['products'];$i++)
		     {
			   $prods.=$_POST['products-'.$i].',';
			 } 

		   $query='DELETE FROM products_to_suppliers 
		                        WHERE products_id IN ('.substr($prods,0,strlen($prods)-1).')
							    AND suppliers_id IN ('.$_POST['suppliers'].') ';
		   //echo $query.'<br />';
		   tep_db_query($query);
		   
		 
		   $suppliers=split(',',$_POST['suppliers']);
		   
		   for($i=0;$i<$_POST['products'];$i++)
		     {
				for($j=0;$j<count($suppliers);$j++)
				  {
				    if(($_POST['pa-'.$suppliers[$j].'-'.$i]!='') && ($_POST['ref-'.$suppliers[$j].'-'.$i]!='')) 
					  { 
					    $query='INSERT INTO products_to_suppliers '
						      .'            (products_id,
							                suppliers_id,
											prix_achat,
											ref_supplier ) '
							  .'VALUES      ("'.$_POST['products-'.$i].'",
							                 "'.$suppliers[$j].'",
											 "'.$_POST['pa-'.$suppliers[$j].'-'.$i].'",
											 "'.$_POST['ref-'.$suppliers[$j].'-'.$i].'"); ';
						//echo $query;
						tep_db_query($query);
					  }
				  }
			 }
			 echo '<strong style="width: 100%; text-align: center;">Les modifications ont bien �t� appliqu�es</strong>';
		 }
	
	
	
	
	
	
	?>
	
	
	<? if(isset($_POST['preparer'])) 
	     {
		   $products=0;
		   echo '<form method="post">';
		   echo '<table border="0" width="100%" cellspacing="0" cellpadding="2">';
		   
		   for($i=0;$i<count($suppliers);$i++)
		     { if(isset($suppliers[$i])) { $temp.=$suppliers[$i].','; } }
		   $select_suppliers='SELECT suppliers_id as id, suppliers_name as nom '
		   					.'FROM suppliers '
							.'WHERE suppliers_id IN ('.substr($temp,0,strlen($temp)-1).') '
							.'ORDER BY nom ';
		   $rep_suppliers=tep_db_query($select_suppliers); $fournisseurs=array();$j=0;
		   while($arr_suppliers=tep_db_fetch_array($rep_suppliers))
		     {
			   $fournisseurs[$j]['id']=$arr_suppliers['id'];
			   $fournisseurs[$j]['nom']=$arr_suppliers['nom'];
			   $j++;			   
			 }
		 
		 
		   $select_prods='SELECT p.products_id as id, pd.products_name as nom, '
		   				.'       p.products_model as model, p.products_bimage as image '
		   				.'FROM products p, products_description pd '
						.'WHERE p.family_id="'.$_POST['family'].'" '
						.'AND p.products_id=pd.products_id '
						.'ORDER BY model';
		   $rep_prods=tep_db_query($select_prods);
		   		 		   
		   $j=0;
		   while($arr_prods=tep_db_fetch_array($rep_prods))
		     {
			   echo '<tr class="dataTableHeadingRow">';
			   
			   echo '<td class="dataTableHeadingContent">'.$arr_prods['nom'].'&nbsp;&nbsp;&nbsp;
			   ( <a href="..' . BASE_DIR_ADMIN . '/article_edit.php?pID=' . $arr_prods['id'] . '&action=new_product" target="_blank"><strong>' . $arr_prods['model'] . '</strong> </a>)
			   <input type="hidden" name="products-'.$j.'" value="'.$arr_prods['id'].'" /></td>';
			   echo '<td class="dataTableHeadingContent" align="center">P.A</td>';
			   echo '<td class="dataTableHeadingContent" align="center">Ref</td>';
			   
			   echo '</tr>'."\n";
			   $products++;
			   
			   for($i=0;$i<count($fournisseurs);$i++)
				{
				  echo '<tr class="dataTableRow">';
				  
				  echo '<td class="dataTableContent">'.$fournisseurs[$i]['nom'].'</td>';
				  
				  $select='SELECT prix_achat, ref_supplier '
				  		 .'FROM products_to_suppliers '
						 .'WHERE products_id="'.$arr_prods['id'].'" '
						 .'AND suppliers_id="'.$fournisseurs[$i]['id'].'" ';
				  $reps=tep_db_query($select);
				  $arr_reps=tep_db_fetch_array($reps);
				  
				  echo '<td class="dataTableContent" align="center"><input type="text" name="pa-'.$fournisseurs[$i]['id'].'-'.$j.'" size="5" value="'.$arr_reps['prix_achat'].'" /></td>';
				  echo '<td class="dataTableContent" align="center"><input type="text" name="ref-'.$fournisseurs[$i]['id'].'-'.$j.'" value="'.(string)$arr_reps['ref_supplier'].'" /></td>';
				  
				  echo '</tr>'."\n";
				}	 
			 echo '<tr><td height="10" colspan="2"></td></tr>';
			   $j++;
			 }
		   echo '<tr>
		           <td colspan="3" align="center">
				     <input type="hidden" name="suppliers" value="'.substr($temp,0,strlen($temp)-1).'" />
					 <input type="hidden" name="products" value="'.$products.'" />
					 <input type="submit" name="enregistrer" value="Enregistrer les valeurs" />
				   </td>
				 </tr>'."\n";

		   echo '</table>';
		   echo '</form>';
				 
		 }
	?>

	
	<? if(!isset($_POST['preparer'])&&!isset($_POST['enregistrer'])) { ?>
	<form method="post">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">Mise � jour rapide (R�f�rences+Prix) des Fournisseurs</td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table>
		</td>
       </tr>
	   
	   <tr>
	  	  <td align="left" class="main"> 
		  	Cette page permet de faire une mise � jour des r�f�rences et des prix pour des fournisseurs selectionn�s.<br />
			- 1. Selectionnez un type d'accessoire<br />
			- 2. Cochez les cases des fournisseurs � afficher.<br />
			- 3. Cliquez sur le bouton "g�n�rer la liste".<br />
			- 4. Il ne reste plus qu'a remplir les informations.<br />
			<br />
	      </td>
       </tr>
	   
	   
	   	<tr>
	  	  <td align="left">Type d'article : 
	  	    <select name="family">
	  	      <?
			    $select_family='SELECT family_id as id, family_name as nom '
									 .'FROM family '
									 .'ORDER by nom ';
				$rep_family=tep_db_query($select_family);
				while($arr_family=tep_db_fetch_array($rep_family))
					{ echo '<option value="'.$arr_family['id'].'">'.$arr_family['nom'].'</option>'; }
			  ?>
  	        </select>
  	      </td>
	   	</tr>
		
		<tr>
	  	  <td align="left">
		    Selectionnez les fournisseurs a g�rer :<br /> 
			<?
			  $select_supplier='SELECT suppliers_id as id, suppliers_name as nom '
			  				  .'FROM suppliers '
							  .'ORDER by nom ';
			  $rep_supplier=tep_db_query($select_supplier);
			  while($arr_supplier=tep_db_fetch_array($rep_supplier))
			  	{ echo '<input name="suppliers[]" type="checkbox" value="'.$arr_supplier['id'].'"> '.$arr_supplier['nom'].'&nbsp;&nbsp;|&nbsp;&nbsp;'; }
			
			?>
  	      </td>
	   	</tr>
		
	   	<tr>
		  <td align="left">
		  	<input type="submit" name="preparer">
 		  </td>
		</tr>
	   
	   
	  </table>
	 </td>
	</tr>
       </form>
      </table>	
	  <? } ?>
</td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
