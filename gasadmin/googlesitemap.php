<?php
  require('includes/application_top.php');
  
/* INDEX */
if($_REQUEST["action"]=="regenerate"){
	$fp = fopen('../gas/sitemapindex.xml', 'w');
	fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>'."\n".'
		<sitemapindex xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">'."\n".'
			<sitemap>'."\n".'
				<loc>https://www.generalarmystore.fr/gas/sitemapcategories.xml</loc>'."\n".'
				<lastmod>'.date("Y-m-d").'</lastmod>'."\n".'
			</sitemap>'."\n".'
			<sitemap>'."\n".'
				<loc>https://www.generalarmystore.fr/gas/sitemapproducts1.xml</loc>'."\n".'
				<lastmod>'.date("Y-m-d").'</lastmod>'."\n".'
			</sitemap>'."\n".'
			<sitemap>'."\n".'
				<loc>https://www.generalarmystore.fr/gas/sitemapproducts2.xml</loc>'."\n".'
				<lastmod>'.date("Y-m-d").'</lastmod>'."\n".'
			</sitemap>'."\n".'
			<sitemap>'."\n".'
				<loc>https://www.generalarmystore.fr/gas/sitemapproducts3.xml</loc>'."\n".'
				<lastmod>'.date("Y-m-d").'</lastmod>'."\n".'
			</sitemap>'."\n".'
		</sitemapindex>');
	fclose($fp);

/* Categories */
	$fp = fopen('../gas/sitemapcategories.xml', 'w');
	fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>'."\n".'<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">'."\n\n");
	$query_cat="SELECT cd.categories_id, cd.categories_url, rub.rubrique_id, rub.rubrique_url, c.last_modified   
            	FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
			  				     inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
								 inner join ".TABLE_RAYON." as rub on rub.rubrique_id=cr.rubrique_id
				where parent_id='0' and categories_status='1' and categories_status_client='1' 
				and rub.rubrique_id='1'
				order by last_modified desc";
	$res_cat=tep_db_query($query_cat);
	while($res_cat && $row_cat=tep_db_fetch_array($res_cat)){
		$url_cat = url("marque",array('id_rayon' => $row_cat["rubrique_id"], 'nom_rayon' => $row_cat["rubrique_url"], 'id_marque' => $row_cat["categories_id"], 'url_marque' => $row_cat["categories_url"]));
		$tab_mod=explode(" ",$row_cat["last_modified"]);
		if(empty($row_cat["last_modified"])){
			$tab_mod[0]=date("Y-m-d");
		}
		fwrite($fp, "\t".'<url>'."\n");
		fwrite($fp, "\t\t".'<loc>https://www.generalarmystore.fr/gas/'.$url_cat.'</loc>'."\n");
		fwrite($fp, "\t\t".'<lastmod>'.$tab_mod[0].'</lastmod>'."\n");
		fwrite($fp, "\t\t".'<changefreq>weekly</changefreq>'."\n");
		fwrite($fp, "\t".'</url>'."\n");
		$query_sscat="SELECT cd.categories_id, cd.categories_url, c.last_modified   
            	FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=c.categories_id
				where parent_id='".$row_cat["categories_id"]."' and categories_status='1' and categories_status_client='1'
				order by last_modified desc";
		$res_sscat=tep_db_query($query_sscat);
		while($res_sscat && $row_sscat=tep_db_fetch_array($res_sscat)){
			$url_sscat = url("modele",array('id_rayon' => $row_cat["rubrique_id"], 'nom_rayon' => $row_cat["rubrique_url"], 'id_marque' => $row_cat["categories_id"], 'url_marque' => $row_cat["categories_url"], 'id_modele' => $row_sscat["categories_id"], 'url_modele' => $row_sscat["categories_url"]));
			$tab_mod=explode(" ",$row_sscat["last_modified"]);
			if(empty($row_sscat["last_modified"])){
				$tab_mod[0]=date("Y-m-d");
			}
			fwrite($fp, "\t".'<url>'."\n");
			fwrite($fp, "\t\t".'<loc>https://www.generalarmystore.fr/gas/'.$url_sscat.'</loc>'."\n");
			fwrite($fp, "\t\t".'<lastmod>'.$tab_mod[0].'</lastmod>'."\n");
			fwrite($fp, "\t\t".'<changefreq>weekly</changefreq>'."\n");
			fwrite($fp, "\t".'</url>'."\n");
		}
	}
	fwrite($fp, '</urlset>'."\n");
	fclose($fp);

/* SITEMAP ARTICLES LISTE 1 */
	$fp = fopen('../gas/sitemapproducts1.xml', 'w');
	fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>'."\n".'<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">'."\n\n");
	$query_prod="SELECT cd.categories_name as nom_modele, cd.categories_url as url_modele, cd.categories_id as id_modele, rub.rubrique_id, rub.rubrique_url, 
					   cd2.categories_url as url_marque, cd2.categories_id as id_marque, pd.products_id, pd.products_url as url_article, p.products_last_modified
				FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
												   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
												   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
												   inner join " . TABLE_RAYON . " as rub on rub.rubrique_id=cr.rubrique_id
												   inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " as pc on c.categories_id=pc.categories_id
												   inner join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on pd.products_id=pc.products_id
												   inner join " . TABLE_PRODUCTS . " as p on pd.products_id=p.products_id
				WHERE cr.rubrique_id='1' and p.products_id<9500
				Order by p.products_last_modified desc";
	$res_prod=tep_db_query($query_prod);
	while($res_prod && $row_prod=tep_db_fetch_array($res_prod)){
		$url_prod = url("article",array('id_rayon' => $row_prod["rubrique_id"], 'nom_rayon' => $row_prod["rubrique_url"], 'id_marque' => $row_prod["id_marque"], 'url_marque' => $row_prod["url_marque"], 
										'id_modele' => $row_prod["id_modele"], 'url_modele' => $row_prod["url_modele"], 'id_article' => $row_prod["products_id"], 'url_article' => bda_product_name_transform($row_prod["url_article"],$row_prod["id_modele"])));
		$tab_mod=explode(" ",$row_prod["products_last_modified"]);
		if(empty($row_prod["products_last_modifiedformat site"])){
			$tab_mod[0]=date("Y-m-d");
		}
		fwrite($fp, "\t".'<url>'."\n");
		fwrite($fp, "\t\t".'<loc>https://www.generalarmystore.fr/gas/'.$url_prod.'</loc>'."\n");
		fwrite($fp, "\t\t".'<lastmod>'.$tab_mod[0].'</lastmod>'."\n");
		fwrite($fp, "\t".'</url>'."\n");
	}
	fwrite($fp, '</urlset>'."\n");
	fclose($fp);


/* SITEMAP ARTICLES LISTE 2 */
	$fp = fopen('../gas/sitemapproducts2.xml', 'w');
	fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>'."\n".'<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">'."\n\n");
	$query_prod="SELECT cd.categories_name as nom_modele, cd.categories_url as url_modele, cd.categories_id as id_modele, rub.rubrique_id, rub.rubrique_url, 
					   cd2.categories_url as url_marque, cd2.categories_id as id_marque, pd.products_id, pd.products_url as url_article, p.products_last_modified
				FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
												   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
												   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
												   inner join " . TABLE_RAYON . " as rub on rub.rubrique_id=cr.rubrique_id
												   inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " as pc on c.categories_id=pc.categories_id
												   inner join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on pd.products_id=pc.products_id
												   inner join " . TABLE_PRODUCTS . " as p on pd.products_id=p.products_id
				WHERE cr.rubrique_id='2' 
				Order by p.products_last_modified desc";
	$res_prod=tep_db_query($query_prod);
	while($res_prod && $row_prod=tep_db_fetch_array($res_prod)){
		$url_prod = url("article",array('id_rayon' => $row_prod["rubrique_id"], 'nom_rayon' => $row_prod["rubrique_url"], 'id_marque' => $row_prod["id_marque"], 'url_marque' => $row_prod["url_marque"], 'id_modele' => $row_prod["id_modele"], 'url_modele' => $row_prod["url_modele"], 'id_article' => $row_prod["products_id"], 'url_article' => bda_product_name_transform($row_prod["url_article"],$row_prod["id_modele"])));
		$tab_mod=explode(" ",$row_prod["products_last_modified"]);
		if(empty($row_prod["products_last_modifiedformat site"])){
			$tab_mod[0]=date("Y-m-d");
		}
		fwrite($fp, "\t".'<url>'."\n");
		fwrite($fp, "\t\t".'<loc>https://www.generalarmystore.fr/gas/'.$url_prod.'</loc>'."\n");
		fwrite($fp, "\t\t".'<lastmod>'.$tab_mod[0].'</lastmod>'."\n");
		fwrite($fp, "\t".'</url>'."\n");
	}
	fwrite($fp, '</urlset>'."\n");
	fclose($fp);


/* SITEMAP ARTICLES LISTE 3 */

	$fp = fopen('../gas/sitemapproducts3.xml', 'w');
	fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>'."\n".'<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">'."\n\n");
	$query_prod="SELECT cd.categories_name as nom_modele, cd.categories_url as url_modele, cd.categories_id as id_modele, rub.rubrique_id, rub.rubrique_url, 
					   cd2.categories_url as url_marque, cd2.categories_id as id_marque, pd.products_id, pd.products_url as url_article, p.products_last_modified
				FROM " . TABLE_CATEGORIES . " as c inner join " . TABLE_CATEGORIES_RAYON . " as cr on c.categories_id=cr.categories_id
												   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd on cd.categories_id=cr.categories_id
												   inner join " . TABLE_CATEGORIES_DESCRIPTION . " as cd2 on cd2.categories_id=c.parent_id
												   inner join " . TABLE_RAYON . " as rub on rub.rubrique_id=cr.rubrique_id
												   inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " as pc on c.categories_id=pc.categories_id
												   inner join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on pd.products_id=pc.products_id
												   inner join " . TABLE_PRODUCTS . " as p on pd.products_id=p.products_id
				WHERE cr.rubrique_id='3'
				Order by p.products_last_modified desc";
	$res_prod=tep_db_query($query_prod);
	while($res_prod && $row_prod=tep_db_fetch_array($res_prod)){
		$url_prod = url("article",array('id_rayon' => $row_prod["rubrique_id"], 'nom_rayon' => $row_prod["rubrique_url"], 'id_marque' => $row_prod["id_marque"], 'url_marque' => $row_prod["url_marque"], 'id_modele' => $row_prod["id_modele"], 'url_modele' => $row_prod["url_modele"], 'id_article' => $row_prod["products_id"], 'url_article' => bda_product_name_transform($row_prod["url_article"],$row_prod["id_modele"])));
		$tab_mod=explode(" ",$row_prod["products_last_modified"]);
		if(empty($row_prod["products_last_modifiedformat site"])){
			$tab_mod[0]=date("Y-m-d");
		}
		fwrite($fp, "\t".'<url>'."\n");
		fwrite($fp, "\t\t".'<loc>https://www.generalarmystore.fr/gas/'.$url_prod.'</loc>'."\n");
		fwrite($fp, "\t\t".'<lastmod>'.$tab_mod[0].'</lastmod>'."\n");
		fwrite($fp, "\t".'</url>'."\n");
	}
	fwrite($fp, '</urlset>'."\n");
	fclose($fp);



}
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<table border="0" width="100%" cellspacing="2" cellpadding="2" class="main">
  <tr>
    <td class="pageHeading">Google XML Sitemap Administration</td>
    <td class="pageHeading" align="right"><img src="images/google-sitemaps.gif" width="110" height="48">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top">
        <p><strong>VUE D'ENSEMBLE :</strong></p>
        <p>Ce module produit automatiquement plusieurs plan du site XML conformes pour Google SiteMaps pour votre boutique :<br \><br \>
            - Un plan du site de la page d\'accueil (sitemapindex.xml).<br \>
            - Une page pour les catégories (sitemapcategories.xml).<br \>
            - Une page pour vos articles (sitemapproducts.xml).
        </p>
		
		<a href="googlesitemap.php?action=regenerate">Cliquez ici pour générer un nouveau sitemap (ne pas utiliser trop souvent)</a>
    </td>
  </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>