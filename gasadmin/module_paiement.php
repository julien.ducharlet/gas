<?php
/* Page de gestion des modes de paiement */

  require('includes/application_top.php');
  
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); 
// fonction qui va s�curis� le nom du fichier
function clean_file_name($var){
  $var = strtr($var,"�����������������������������������������������������","AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn");
  $var = eregi_replace("[^a-zA-Z0-9.-]","",$var);
  return $var;
}

?>
<!-- header_eof //-->
<script language="javascript1.1" src="includes/javascript/ajax/module_paiement.js"></script>
<!-- body //-->
<div style="width:100%; height:600px;">

<br><div style="font-size:25px; padding-left:20px;">Gestion du module des moyens de paiement</div>
<!-- Partie permettant de modifier ou d'ajouter un moyen de paiment -->
	<div id="action_moyen" style="display:none;">
     <form name="paiement-form" method="post" action="<?php echo $_SESSION['PHP_SERVER'];?>" enctype="multipart/form-data">
		<?php     
			// repertoire o� vont �tre plac� les fichiers
	 	    $content_dir = $_SERVER['DOCUMENT_ROOT'].'/'.DIR_WS_SITE.'/template/base/images_paiement/';
			
           if(isset($_POST['ajout'])){
			$verif=true;
			$err='';
			
			if($_POST['nom']==''){
			$err.="Vous devez donner un nom au moyen de paiement<br />";
			$verif=false;
			}
			if($_POST['statut']=='choix'){
			$err.="Vous devez sp&eacute;cifier un statut pour le moyen de paiement<br />";
			$verif=false;
			}
			if($_POST['description']==''){
			$err.="Vous devez sp&eacute;cifier une description pour le moyen de paiement<br />";
			$verif=false;
			}
			if($_POST['montant']==''){
			$err.="Vous devez sp&eacute;cifier un montant minimum pour le moyen de paiement<br />";
			$verif=false;
			}
			// est ce qu'un fichier a ete selectionn� ?
			if(empty($_FILES['paiement_image']['name'])){
				$err.="Vous devez ajouter une photo pour le moyen de paiement.";
				$verif=false;
			}
			
			if($verif==false){
				echo $err;
				?>
          		<script language="javascript1.1" > document.getElementById("action_moyen").style.display="block"; </script>
        		<?php
			}
			else{ 		
				  // on test si le fichier est upload�
				  $tmp_file = $_FILES['paiement_image']['tmp_name'];
				  if( !is_uploaded_file($tmp_file) ){
					exit("Le fichier est introuvable");
				  }	
				 // renommons le fichier pour plus de s�curit�
 				 $name_file = $_FILES['paiement_image']['name'];
				 
				 // upload
				  if(!move_uploaded_file($tmp_file,$content_dir.$name_file)){
					exit("Impossible de copier le fichier !");
				  }else{
					echo "Le fichier a bien �t� upload�";
				  }
				 
				if($_POST['visible']==''){ $vu=0; } else{ $vu=1; }
				if($_POST['detail']==''){ $detail=NULL; } else{ $detail=$_POST['detail']; }
				
				$req_moyen=tep_db_query("insert into ".TABLE_MODULE_PAIEMENT."
										(paiement_nom, paiement_description, paiement_ordre_affichage, paiement_statut, paiement_montant_max, paiement_visible, paiement_details, paiement_img)
						  values ('".$_POST['nom']."', '".$_POST['description']."', '0', '".$_POST['statut']."', '".$_POST['montant']."', '".$vu."', '".$detail."', '".$name_file."') ");
				?>
          		<!--<script language="javascript1.1" >document.location.href="module_paiement.php"; </script>-->
                 <script language="javascript1.1" > document.getElementById("action_moyen").style.display="block"; </script> 
        		<?php
                }        
        }
        if(isset($_POST['modif'])){
			$verif=true;
			$err='';
			if($_POST['nom']==''){
			$err.="Vous devez donner un nom au moyen de paiement<br />";
			$verif=false;
			}
			if($_POST['statut']=='choix'){
			$err.="Vous devez sp&eacute;cifier un statut pour le moyen de paiement<br />";
			$verif=false;
			}
			if($_POST['description']==''){
			$err.="Vous devez sp&eacute;cifier une description pour le moyen de paiement<br />";
			$verif=false;
			}
			if($_POST['montant']==''){
			$err.="Vous devez sp&eacute;cifier un montant minimum pour le moyen de paiement<br />";
			$verif=false;
			}
			if($verif==false){
				echo $err;
				?>
                <script language="javascript1.1" >document.location.href="module_paiement.php"; </script> 
                <!--<script language="javascript1.1" > document.getElementById("action_moyen").style.display="block"; </script>-->
        		<?php
			}
			else{ 			
				
				$img=$_POST['img_nm'];
				
				if(!empty($_FILES['paiement_image']['name'])){
				// on test si le fichier est upload�
				  $tmp_file = $_FILES['paiement_image']['tmp_name'];
				  if( !is_uploaded_file($tmp_file) ){
					exit("Le fichier est introuvable");
				  }	
				 // renommons le fichier pour plus de s�curit�
 				 $name_file = $_FILES['paiement_image']['name'];
				 
				 // upload
				  if(!move_uploaded_file($tmp_file,$content_dir.$name_file)){
					exit("Impossible de copier le fichier !");
				  }else{
					echo "Le fichier a bien �t� upload�";
				  }
				   $img=$name_file;
				}
				  
				  
				if($_POST['visible']==''){ $vu=0; } else{ $vu=1; }
				if($_POST['detail']==''){ $detail=NULL; } else{ $detail=$_POST['detail']; }
				
				$req_moyen=tep_db_query("update ".TABLE_MODULE_PAIEMENT." set
										paiement_nom ='".$_POST['nom']."',
										paiement_description='".$_POST['description']."',
										paiement_ordre_affichage='".$_POST['ordre']."',
										paiement_statut='".$_POST['statut']."',
										paiement_montant_max='".$_POST['montant']."',
										paiement_visible='".$vu."',
										paiement_img='".$img."',
										paiement_details='".$detail."'
										where paiement_id=".$_POST['id_p']." ");
										
				?>
          		<script language="javascript1.1" >document.location.href="module_paiement.php"; </script> 
                <!--<script language="javascript1.1" > document.getElementById("action_moyen").style.display="block"; </script>-->
        		<?php
                }      
        ?>
            <script language="javascript1.1" > document.getElementById("action_moyen").style.display="block"; </script>
        <?php
        } 
        ?>
        
        <div style="background-color:#CCC; width:95%; margin-top:20px; margin-left:30px; text-align:left; padding-bottom:30px;">
        	<div id="ajt_moyen" style="display:none;">Ajouter un nouveau moyen de paiement</div>
            <div  id="modif_moyen" style="display:none;">
				Modifier le moyen de paiement suivant :
			</div>
            <br />
            <div style="float:left;">
                <div>
                    <input type="hidden" id="id_paiement" name="id_p" value="<?php echo $_POST['id_p']; ?>" />
                    <div style="float:left;">
                        <div style="float:left; width:150px;">Nom : </div>
                        <div style="float:left;"><input type="text" id="nom_m" value="<?php echo $_POST['nom']; ?>" name="nom" /></div>
                        <div style="clear:both;"></div>
                    </div>
                    <div style="float:right; margin-right:30px; width:80px;">En ligne : 
                    <input <?php if($_POST['visible']==1) echo "checked='checked'"; ?> type="checkbox" id="visible_m" value="1" name="visible" /></div>
                    
                    <div style="clear:both;"></div>
                </div>
                <div id="ordre_af" style="margin-top:10px; display:none;">
                    <div style="float:left; width:150px;">Ordre d'affichage : </div>
                    <div style="float:left;"><input type="text" id="ordre_m" value="<?php echo $_POST['ordre']; ?>" name="ordre" size="5" /></div>
                    <div style="clear:both;"></div>
                </div>
                <?php
                if(isset($_POST['modif'])){
                ?>
                    <script language="javascript1.1" > document.getElementById("ordre_af").style.display="block"; </script>
                <?php
                } ?>
                <div style="margin-top:10px;">
                    <div style="float:left; width:150px;">Statut : </div>
                    <div style="float:left;">
                        <select id="liste_statut" name="statut" >
                        	<option value="choix">Choisir un statut</option>
							<?php 
								$req_satut=tep_db_query("select orders_status_id, orders_status_name from orders_status");
								while($statut=tep_db_fetch_array($req_satut)){
						     ?>
                            <option <?php if($_POST['statut']==$statut['orders_status_id']) echo'selected="selected"'; ?> value=<?php echo $statut['orders_status_id']; ?>><?php echo $statut['orders_status_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div style="margin-top:10px;">
                    <div style="float:left; width:150px;">Description : </div>
                    <div style="float:left;"><textarea id="description_m" name="description" rows="2" style="width:400px"><?php echo stripslashes($_POST['description']); ?></textarea></div>
                    <div style="clear:both;"></div>
                </div>
                <div style="margin-top:10px;">
                    <div style="float:left; width:150px;">Montant maximum : </div>
                    <div style="float:left;"><input type="text" id="montant_m" value="<?php echo $_POST['montant']; ?>" name="montant" /></div>
                    <div style="clear:both;"></div>
                </div>
                <div style="margin-top:10px;">
                    <div style="float:left; width:150px;">Image :</div> 
                    <div style="float:left;">
                        <div><input type="file" id="image_m" value="<?php echo $_FILES['paiement_image']; ?>" name="paiement_image" /></div>
                        <input type="hidden" id="img_n" name="img_nm" value="<?php echo $_POST['img_nm']; ?>" />
                        <div id="img_m_text"><?php echo $_POST['img_nm']; ?></div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
            <div style="float:left;">
                <div style="margin-left:150px;">
                    <div>Paiement detail sur la page de confirmation de commande (<span style="color:red;">nous pr�venir pour toute modif</span>)</div>
                    <div><textarea id="detail_m" name="detail" rows="10" style="width:700px"><?php echo stripslashes($_POST['detail']); ?></textarea></div>
                    
               </div>
           </div>           
           <div style="clear:both;"></div>
        </div>
        <div id="bt_ajt_moyen" style="display:none; text-align:center;"><input type="submit" id="bt_ajout" name="ajout" value="Ajouter" /></div>
        <div id="bt_modif_moyen" style="display:none; text-align:center;"><input type="submit" id="bt_modif" name="modif" value="Modifier" /></div>
        <div style="text-align:center;"><input type="button" id="bt_annul" value="Annuler" onClick="retour();" /></div>
           <?php     
             if(isset($_POST['ajout'])){
			?>
				<script language="javascript1.1" > document.getElementById("bt_ajt_moyen").style.display="block"; </script>
			<?php
			}
			if(isset($_POST['modif'])){
			?>
				<script language="javascript1.1" > document.getElementById("bt_modif_moyen").style.display="block"; </script>
			<?php
			} 
			?>
      </form>
    </div>
    <div style="clear:both;"></div>

<!-- Partie permettant de modifier ou d'ajouter un moyen de paiment -->

<!-- Partie listant tous les modules quand on arrive sur la page -->
	<div id="les_moyens" style="display:block;">
       <?php     
             if(isset($_POST['ajout']) || isset($_POST['modif'])){
            echo "Ajouter";
        ?>
            <script language="javascript1.1" > document.getElementById("les_moyens").style.display="none"; </script>
        <?php
        }
		?>

        <div style="background-color:#999; width:100%; margin-top:20px; text-align:center;">
            <div style="float:left; width:10%;">ID</div>
            <div style="float:left; width:30%;">Nom</div>
            <div style="float:left; width:10%;">Statut</div>
            <div style="float:left; width:10%;">Ordre affichage</div>
            <div style="float:left; width:15%;">Montant maximum</div>
            <div style="float:left; width:25%; text-align:center;">Actions</div>
            <div style="clear:both;"></div>
        </div>
        
        <?php
        $req_moyen=tep_db_query("select * from ".TABLE_MODULE_PAIEMENT."");
    
        $i=0;
        while($res_moyen=tep_db_fetch_array($req_moyen)){
            
            if($i % 2==0){$couleur="#CCC";}
            else {$couleur="#DDD";}
        
        ?>
        <div style="background-color:<?php echo $couleur; ?>; width:100%; height:30px; text-align:center;">
            <div style="float:left; width:10%;"><?php echo $res_moyen['paiement_id']; ?></div>
            <div style="float:left; width:30%;"><?php echo $res_moyen['paiement_nom']; ?></div>
            <div style="float:left; width:10%;">
				<?php
					if($res_moyen['paiement_visible']==1){
				?>
                	<img src="images/icon_status_green.gif" title="mettre en ligne" style="cursor:pointer;" />
                    <img src="images/icon_status_red_light.gif" title="mettre en Hors-ligne" style="cursor:pointer;" onClick="offline(<?php echo $res_moyen['paiement_id']; ?>)" />
                <?php
					}
					else{
				?>
                	<img src="images/icon_status_green_light.gif" title="mettre en ligne" style="cursor:pointer;" onClick="online(<?php echo $res_moyen['paiement_id']; ?>)" />
            		<img src="images/icon_status_red.gif" title="mettre en Hors-ligne" style="cursor:pointer;" />                
                <?php
					}
				?>
            </div>
            <div style="float:left; width:10%;"><?php echo $res_moyen['paiement_ordre_affichage']; ?></div>
            <div style="float:left; width:15%;"><?php echo $res_moyen['paiement_montant_max']; ?></div>
            <div style="float:left; width:25%; text-align:center;">
                <!--<a href="#" onClick="action(<?php echo $res_moyen['paiement_id']; ?>)">
                    <img border="none" src="images/icons/editer.png" title="editer <?php echo $res_moyen['paiement_nom'];  ?>" /></a>&nbsp;
                <a href="#" onClick="supprimer(<?php echo $res_moyen['paiement_id'].",'".$res_moyen['paiement_nom']."'";  ?>)">
                	<img border="none" src="images/icons/supprimer.png" title="supprimer <?php echo $res_moyen['paiement_nom'];  ?>" /></a>--> Demander au d�veloppeur
			</div>
            <div style="clear:both;"></div>
        </div>
        <?php 
        $i++;
        }	
        ?>        
        <div style="width:95%; margin-left:30px; height:30px; margin-top:10px; text-align:right;">
            <div style="float:right; width:300px;"><input type="button" id="add_moyen" name="moyen" value="AJouter une moyen de paiment" onClick="action(0);" /></div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
<!-- FIN Partie listant tous les modules quand on arrive sur la page -->    

</div>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>