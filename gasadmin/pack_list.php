<?php 
require('includes/configure.php');
require('includes/database_tables.php');
				
mysql_connect(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
mysql_select_db(DB_DATABASE);

if(!isset($_GET['ajax'])) {
?>
<table width="100%" border="1" cellspacing="0" cellpadding="3" class="tablesorter">
    <thead>
    <tr>
      <th>Article principal</th>
      <th class="{sorter:false}">Les article li&eacute; &agrave; l'article principal</th>
      <th align="center" width="90">Prix</th>
      <th align="center" width="90">Prix d'achat</th>
      <th align="center" width="90">Remise</th>
      <th align="center" width="90">Marge (HT)</th>
      <th align="center" width="60">ON / OFF</th>
      <th align="center" class="{sorter:false}" width="100">Action</th>
    </tr>
    </thead>
    <tbody>
<?php 
}

$end='';
if(isset($_GET['article_id']) && $_GET['article_id']>0) {
	$pack_query = mysql_query("select * from " . TABLE_PACK . " pa, ". TABLE_PRODUCTS ." pr  where pa.products_id=pr.products_id and pa.products_id=". $_GET['article_id']) or die(mysql_error());
}
elseif(isset($_GET['manufacturers_id']) && $_GET['manufacturers_id']>0) {
	$pack_query = mysql_query("select * from " . TABLE_PACK . " pa, ". TABLE_PRODUCTS ." pr  where pa.products_id=pr.products_id and manufacturers_id=". $_GET['manufacturers_id']) or die(mysql_error());
}
elseif(isset($_GET['family_id']) && $_GET['family_id']>0) {
	$pack_query = mysql_query("select * from " . TABLE_PACK . " pa, ". TABLE_PRODUCTS ." pr  where pa.products_id=pr.products_id and family_id=". $_GET['family_id']) or die(mysql_error());
}
else { 
	$pack_query = mysql_query("select * from " . TABLE_PACK . "");
	$end="</tbody></table>";		
}
$nombre_de_resultat=mysql_num_rows($pack_query);

if($nombre_de_resultat>0) {
	while($data = mysql_fetch_array($pack_query)) { // tableau qui contient la liste des id des produits de chaque pack ainsi que la remise
		$arrayProducts[]=array('pack_id' => $data['pack_id'], 'products_id' => $data['products_id'], 'products_id_1' => $data['products_id_1'], 
		'products_id_2' => $data['products_id_2'], 'pack_remise' => $data['pack_remise'], 'pack_status' => $data['pack_status']);
	}
		
	for($i=0; $i<$nombre_de_resultat; $i++) {
	
		$arrayTmp[0][$i]=$arrayProducts[$i]['products_id'];
		$arrayTmp[1][$i]=$arrayProducts[$i]['products_id_1'];
		$arrayTmp[2][$i]=$arrayProducts[$i]['products_id_2'];																	
	}
	
	$arrayProd0=array_unique($arrayTmp[0]);
	$keyProd0=array_keys($arrayProd0);
	$arrayProd1=array_unique($arrayTmp[1]);
	$keyProd1=array_keys($arrayProd1);
	$arrayProd2=array_unique($arrayTmp[2]);
	$keyProd2=array_keys($arrayProd2);
						
	for($i=0; $i<count($arrayProd0); $i++) {
		if($arrayProd0[$keyProd0[$i]]!='') {
			$liste_pack = mysql_query("select p.*, pd.products_name from " . TABLE_PRODUCTS . " p, ". TABLE_PRODUCTS_DESCRIPTION ." pd, tax_rates tr 
			where p.products_id=pd.products_id and p.products_tax_class_id=tr.tax_class_id and p.products_id =  '" . (int)$arrayProd0[$keyProd0[$i]] . "'");
			
			while($data = mysql_fetch_array($liste_pack)) {
			
				$arrayProductsInfos[$arrayProd0[$keyProd0[$i]]]=array('products_id' => $data['products_id'], 'products_name' => $data['products_name'], 'products_model' => $data['products_model'],
				'products_price' => $data['products_price'], 'products_cost' => $data['products_cost'], 'tax_rate' => $data['tax_rate'], 'tax_class_id' => $data['tax_class_id']);
			}
		}
	}	
						
	for($i=0; $i<count($arrayProd1); $i++) {
		
		if($arrayProd1[$keyProd1[$i]]!='') {
			
			$liste_pack = mysql_query("select * from " . TABLE_PRODUCTS . " p, ". TABLE_PRODUCTS_DESCRIPTION ." pd, tax_rates tr 
			where p.products_id=pd.products_id and p.products_tax_class_id=tr.tax_class_id and p.products_id =  '" . (int)$arrayProd1[$keyProd1[$i]] . "'");
			
			while($data = mysql_fetch_array($liste_pack)) {
				
				$arrayProductsInfos[$arrayProd1[$keyProd1[$i]]]=array('products_name' => $data['products_name'], 'products_model' => $data['products_model'] , 
				'products_price' => $data['products_price'], 'products_cost' => $data['products_cost'], 'tax_rate' => $data['tax_rate']);//print_r($arrayProductsInfos);
			}
		}
	}	
			
	for($i=0; $i<count($arrayProd2); $i++) {
	
		if($arrayProd2[$keyProd2[$i]]!='') {
			
			$liste_pack = mysql_query("select * from " . TABLE_PRODUCTS . " p, ". TABLE_PRODUCTS_DESCRIPTION ." pd, tax_rates tr
			where p.products_id=pd.products_id and p.products_tax_class_id=tr.tax_class_id and p.products_id =  '" . (int)$arrayProd2[$keyProd2[$i]] . "'");
		
			while($data = mysql_fetch_array($liste_pack)) {
				
				$arrayProductsInfos[$arrayProd2[$keyProd2[$i]]]=array('products_name' => $data['products_name'], 'products_model' => $data['products_model'] , 
				'products_price' => $data['products_price'], 'products_cost' => $data['products_cost'], 'tax_rate' => $data['tax_rate']);
			}	
		}
	}	
					
	for($i=0; $i<$nombre_de_resultat; $i++) { // BOUCLE POUR CHAQUE PACK
	
		$price1=0;
		$price2=0;
		
		$price=$arrayProductsInfos[$arrayProducts[$i]['products_id']]['products_price'] + $arrayProductsInfos[$arrayProducts[$i]['products_id_1']]['products_price'];
		$cost=$arrayProductsInfos[$arrayProducts[$i]['products_id']]['products_cost'] + $arrayProductsInfos[$arrayProducts[$i]['products_id_1']]['products_cost'];
		
		$prix_remise = $arrayProducts[$i]['pack_remise']; //remise HT
		
		
		echo '<tr class="main">';
			echo '<td><a href="'. BASE_DIR_ADMIN .'/article_edit.php?pID='. $arrayProductsInfos[$arrayProducts[$i]['products_id']]['products_id'] .'&action=new_product">'. $arrayProductsInfos[$arrayProducts[$i]['products_id']]['products_name'] .' ('. $arrayProductsInfos[$arrayProducts[$i]['products_id']]['products_model'] .')</a></td>';
			echo '<td valign="middle">';
				echo '<ul style="list-style:square">';
					echo '<li>'. $arrayProductsInfos[$arrayProducts[$i]['products_id_1']]['products_name'] .' ('. $arrayProductsInfos[$arrayProducts[$i]['products_id_1']]['products_model'] .')</li>';
					if(!empty($arrayProducts[$i]['products_id_2'])) {
						$price+=$arrayProductsInfos[$arrayProducts[$i]['products_id_2']]['products_price'];
						$cost+=$arrayProductsInfos[$arrayProducts[$i]['products_id_2']]['products_cost'];
						echo '<li>'. $arrayProductsInfos[$arrayProducts[$i]['products_id_2']]['products_name'] .' ('. $arrayProductsInfos[$arrayProducts[$i]['products_id_2']]['products_model'] .')</li>';
					}
				echo '</ul>';
			echo '</td>';
				
			$marge=$price - $prix_remise - $cost;
			$price=round($arrayProductsInfos[$arrayProducts[$i]['products_id']]['tax_rate']*$price/100 + $price, 2);
			$new_price=round($price - $prix_remise, 2);
			
			echo '<td align="center"><b>'. round(($price-$prix_remise)*1.2, 2) .' &euro; TTC</b><br /><span style="text-decoration:line-through;font-size:10px;margin-top:5px;">'. round($price*1.2, 2) .' &euro; TTC</span></td>';
			echo '<td align="center">'. $cost .' &euro; HT </td>';
			echo '<td align="center">'. round($prix_remise*1.2, 2) .' &euro; TTC<br />'. $prix_remise .' &euro; HT</td>';
			echo '<td align="center">';
			
			if($marge<10) {
				
				echo '<span style="color:#FF0000;font-weight:bold;">';
			}
			
			echo $marge.' &euro;'; 
			
			if($marge<10){
				echo '</span>';
			}
			echo '</td>';
			echo '<td align="center">';
			
			
			if($arrayProducts[$i]['pack_status']) {
				echo '<input name="status_'. $arrayProducts[$i]['pack_id'] .'" type="radio" checked value="1">';
				echo '<input name="status_'. $arrayProducts[$i]['pack_id'] .'" type="radio" value="0" onclick="setFlag(\''. $arrayProducts[$i]['pack_id'] .'\', \'0\')">';
			}
			else {
				echo '<input name="status_'. $arrayProducts[$i]['pack_id'] .'" type="radio" value="1" onclick="setFlag(\''. $arrayProducts[$i]['pack_id'] .'\', \'1\')">';
				echo '<input name="status_'. $arrayProducts[$i]['pack_id'] .'" type="radio" checked value="0">';
			}
			echo '</td>';
			echo '<td align="center">';
			echo '<a href="?pack='. $arrayProducts[$i]['pack_id'] .'" title="Modifier le Pack"><img src="images/icons/editer.png" width="20" height="20" border="0"></a> | ';
			echo '<a target="_blank" href="'. WEBSITE . BASE_DIR .'/produits_liste.php?recherche='. $arrayProductsInfos[$arrayProducts[$i]['products_id']]['products_model'] .'"><img border="0" title=" visualiser le pack sur le site client " alt="visualiser le pack sur le site client" src="images/icons/visualiser.png"></a> | ';
			echo '<a onclick="delProduct(\''. $arrayProducts[$i]['pack_id'] .'\')" title="Supprimer le Pack" style="cursor:pointer"><img src="images/icons/supprimer.png" width="20" height="20" border="0"></a>';
			echo '</td>';
		echo '</tr>';
	}
}
else {
	echo '<tr class="main">';
	echo 'Aucun R&eacute;sulat';
	echo '</tr>';
}
echo $end;
?>