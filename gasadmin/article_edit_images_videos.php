<?php 
/* Page : article_edit_images_videos.php
Version du : 31/08/2018 */
	
require('includes/application_top.php');

	$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
	
if (isset($_GET['cPath'])) $cPath=$_GET['cPath'];
if ( eregi("(insert|update|setflag)", $action) ) include_once('includes/reset_seo_cache.php');

if($_REQUEST["action2"]=="deleteimage"){
	echo "1";
	$verif_table_image_query = tep_db_query("SELECT COUNT(products_bimage) as nb_images FROM " . TABLE_PRODUCTS_IMAGE . " WHERE products_bimage='" . $_REQUEST["image_name"] . "' AND products_id <> '".$_REQUEST["pID"]."'");
	$verif_table_image = tep_db_fetch_array($verif_table_image_query);
	
	echo "2";
	$verif_table_products_query = tep_db_query("SELECT COUNT(products_bimage) as nb_images FROM " . TABLE_PRODUCTS . " WHERE products_bimage='" . $_REQUEST["image_name"] . "' AND products_id <> '".$_REQUEST["pID"]."'");
	$verif_table_products = tep_db_fetch_array($verif_table_products_query);
	
	
	echo "4";
	if ($verif_table_image['nb_images'] == 0 && $verif_table_products['nb_images'] == 0) {
		if(is_file(DIR_FS_CATALOG_IMAGES.$_REQUEST["image_name"])) {
			unlink(DIR_FS_CATALOG_IMAGES.$_REQUEST["image_name"]);
		}
		/*if(is_file(DIR_FS_PRODUCTS_IMAGES."ori/".$_REQUEST["image_name"])) {
			unlink(DIR_FS_PRODUCTS_IMAGES."ori/".$_REQUEST["image_name"]);
		}*/
		if(is_file(DIR_FS_PRODUCTS_IMAGES."grande/".$_REQUEST["image_name"])) {
			unlink(DIR_FS_PRODUCTS_IMAGES."grande/".$_REQUEST["image_name"]);
		}
		if(is_file(DIR_FS_PRODUCTS_IMAGES."normale/".$_REQUEST["image_name"])) {
			unlink(DIR_FS_PRODUCTS_IMAGES."normale/".$_REQUEST["image_name"]);
		}
		if(is_file(DIR_FS_PRODUCTS_IMAGES."micro/".$_REQUEST["image_name"])) {
			unlink(DIR_FS_PRODUCTS_IMAGES."micro/".$_REQUEST["image_name"]);
		}
	}
	$query_delimage="delete from ".TABLE_PRODUCTS_IMAGE." where products_bimage='".$_REQUEST["image_name"]."' and products_id='".$_REQUEST["pID"]."'";
	tep_db_query($query_delimage);
}

if (tep_not_null($action)) {
	switch ($action) {
		case 'insert_product':
		case 'update_product':
			
			/* UPDATE DES LOGOS */
			$query = 'delete from '. TABLE_PRODUCTS_TO_LOGOS .' where products_id='. $_GET['pID'];
			tep_db_query($query);
			
			for($i=0 ; $i<$_POST['nbre_logos'] ; $i++) {
				
				if($_POST['logo_'. $i] == 'on') {
					
					$query = 'insert into '. TABLE_PRODUCTS_TO_LOGOS .'(products_id, logo_id) values(\''. $_GET['pID'] .'\', \''. $_POST['logo_id_'. $i] .'\')';
					$query = tep_db_query($query);
				}
			}
			/* FIN DE L'UPDATE DES LOGOS */
			
			if (isset($HTTP_POST_VARS['edit_x']) || isset($HTTP_POST_VARS['edit_y'])) {
			  $action = 'new_product';
			} 
			else {
				$pID=$_GET['pID'];
				$products_id = tep_db_prepare_input($HTTP_GET_VARS['pID']);
				$products_date_available = tep_db_prepare_input($HTTP_POST_VARS['products_date_available']);
				$products_date_available = (date('Y-m-d') < $products_date_available) ? $products_date_available : 'null';
			
				if(isset($HTTP_GET_VARS['imageUpload'])) {
					
					// copy big image only if modified
					$products_bimage_1 = new upload('products_bimage');
					$products_bimage_1->set_destination(DIR_FS_PRODUCTS_IMAGES .'ori/');      
					if ($products_bimage_1->parse() && $products_bimage_1->save()) {
						$sql_data_array['products_bimage'] = $products_bimage_1->filename;
						
						resize_image($products_bimage_name,800,800,"grande/".$products_bimage_name);
						resize_image($products_bimage_name,350,350,"normale/".$products_bimage_name);
						resize_image($products_bimage_name,100,100,"petite/".$products_bimage_name);
						resize_image($products_bimage_name,83,83,"mini/".$products_bimage_name);
						resize_image($products_bimage_name,53,53,"micro/".$products_bimage_name);
						
					} else {
						$sql_data_array['products_bimage'] = (isset($HTTP_POST_VARS['products_previous_bimage']) ? $HTTP_POST_VARS['products_previous_bimage'] : '');
					}
					
					//LL a supprimer d�s que la gestion multi image sera mise en place			
					for($i=2;$i<6;$i++){
						$products_bimage_supp = new upload('products_bimage'.$i);
						$products_bimage_supp->set_destination(DIR_FS_PRODUCTS_IMAGES .'ori/');      
						if ($products_bimage_supp->parse() && $products_bimage_supp->save()) {
							$products_bimage_name_supp[$i] = $products_bimage_supp->filename;
							
							resize_image($products_bimage_name_supp[$i],800,800,"grande/".$products_bimage_name_supp[$i]);
							resize_image($products_bimage_name_supp[$i],350,350,"normale/".$products_bimage_name_supp[$i]);
							resize_image($products_bimage_name_supp[$i],53,53,"micro/".$products_bimage_name_supp[$i]);
						} else {
							$products_bimage_name_supp[$i] = (isset($HTTP_POST_VARS['products_previous_bimage'.$i]) ? $HTTP_POST_VARS['products_previous_bimage'.$i] : '');
						}
						
						if(!empty($products_bimage_name_supp[$i])){
							$res_verif_image=tep_db_query("select * from ".TABLE_PRODUCTS_IMAGE." where products_bimage='".$products_bimage_name_supp[$i]."' and products_id='".$pID."'");
							if(tep_db_num_rows($res_verif_image)==0){
								
								$verif_table_image_query = tep_db_query("SELECT COUNT(products_bimage) as nb_images FROM " . TABLE_PRODUCTS_IMAGE . " WHERE products_bimage='" . $HTTP_POST_VARS['products_previous_bimage'.$i] . "' AND products_id <> '".$pID."'");
								$verif_table_image = tep_db_fetch_array($verif_table_image_query);
								
								$verif_table_products_query = tep_db_query("SELECT COUNT(products_bimage) as nb_images FROM " . TABLE_PRODUCTS . " WHERE products_bimage='" . $HTTP_POST_VARS['products_previous_bimage'.$i] . "' AND products_id <> '".$pID."'");
								$verif_table_products = tep_db_fetch_array($verif_table_products_query);
								
								
								if ($verif_table_image['nb_images'] == 0 && $verif_table_products['nb_images'] == 0 ) {
									if(is_file(DIR_FS_CATALOG_IMAGES.$HTTP_POST_VARS['products_previous_bimage'.$i])) {
										unlink(DIR_FS_CATALOG_IMAGES.$HTTP_POST_VARS['products_previous_bimage'.$i]);
									}
									/*if(is_file(DIR_FS_PRODUCTS_IMAGES."ori/".$HTTP_POST_VARS['products_previous_bimage'.$i])) {
										unlink(DIR_FS_PRODUCTS_IMAGES."ori/".$HTTP_POST_VARS['products_previous_bimage'.$i]);
									}*/
									if(is_file(DIR_FS_PRODUCTS_IMAGES."grande/".$HTTP_POST_VARS['products_previous_bimage'.$i])) {
										unlink(DIR_FS_PRODUCTS_IMAGES."grande/".$HTTP_POST_VARS['products_previous_bimage'.$i]);
									}
									if(is_file(DIR_FS_PRODUCTS_IMAGES."normale/".$HTTP_POST_VARS['products_previous_bimage'.$i])) {
										unlink(DIR_FS_PRODUCTS_IMAGES."normale/".$HTTP_POST_VARS['products_previous_bimage'.$i]);
									}
									if(is_file(DIR_FS_PRODUCTS_IMAGES."micro/".$HTTP_POST_VARS['products_previous_bimage'.$i])) {
										unlink(DIR_FS_PRODUCTS_IMAGES."micro/".$HTTP_POST_VARS['products_previous_bimage'.$i]);
									}
								}
								$query_remove_old_image = "DELETE FROM ".TABLE_PRODUCTS_IMAGE." WHERE products_bimage='".$HTTP_POST_VARS['products_previous_bimage'.$i]."' and products_id='".$pID."'";
								tep_db_query($query_remove_old_image);
								$query_image="insert into ".TABLE_PRODUCTS_IMAGE." (products_bimage, products_id) values ('".$products_bimage_name_supp[$i]."', '".$pID."')";
								tep_db_query($query_image);
							}
						}
					}
					
					$products_video = new upload('products_video');
					$products_video->set_destination(DIR_FS_CATALOG_VIDEOS);
					if ($products_video->parse() && $products_video->save()) {
						$sql_data_array['products_video'] = $products_video->filename;
					} else {
						$sql_data_array['products_video'] = (isset($HTTP_POST_VARS['products_previous_video']) ? $HTTP_POST_VARS['products_previous_video'] : '');
					}
					
					
					
					
					
				} 
				else {
					
					if (isset($HTTP_POST_VARS['products_bimage']) && tep_not_null($HTTP_POST_VARS['products_bimage']) && ($HTTP_POST_VARS['products_bimage'] != 'none')) {
						$sql_data_array['products_bimage'] = tep_db_prepare_input($HTTP_POST_VARS['products_bimage']);
					}
					if (isset($HTTP_POST_VARS['products_video']) && tep_not_null($HTTP_POST_VARS['products_video']) && ($HTTP_POST_VARS['products_video'] != 'none')) {
						$sql_data_array['products_video'] = tep_db_prepare_input($HTTP_POST_VARS['products_video']);
					}
				}
				
				$update_sql_data = array('products_last_modified' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $update_sql_data);
				tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "'");
				
				tep_redirect(tep_href_link(FILENAME_ARTICLE_EDIT_IMAGES_VIDEOS, 'cPath=' . $cPath . '&pID=' . $products_id . '&action=new_product'));
			}
		break;
	}
}
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <script language="javascript" src="includes/general.js"></script>
        <script language="javascript" src="ajax/categories.js"></script>
    </head>
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
        <?php
        	require(DIR_WS_INCLUDES . 'header.php');
			$parameters = array('products_name' => '',
								'products_description' => '',
								'products_id' => '',
								'products_model' => '',
								'products_date_added' => '',
								'products_last_modified' => '',
								'products_date_available' => '',
								'products_status' => '');
                                
			$pInfo = new objectInfo($parameters);
			
			if (isset ($HTTP_GET_VARS['pID']) && (!$HTTP_POST_VARS) ) {
				##with big image AND HEADER TAG
				$product_query = tep_db_query("select pd.products_name, pd.products_description, pd.products_head_title_tag, pd.products_head_desc_tag, pd.products_head_keywords_tag, pd.products_url, pd.products_baseline, pd.balise_title_lien_image, pd.balise_title_lien_texte, p.products_id, p.products_quantity, p.products_quantity_reel, p.products_model, p.products_ref_origine, p.products_garantie, p.products_code_barre, p.products_bimage, p.products_video, p.products_video_1, p.products_video_1_largeur, p.products_video_1_hauteur, p.products_video_2, p.products_video_2_largeur, p.products_video_2_hauteur, p.products_vue_360, p.products_price, p.products_cost, p.products_weight, p.products_date_added, p.products_last_modified, date_format(p.products_date_available, '%Y-%m-%d') as products_date_available, p.products_status, p.products_tax_class_id, p.manufacturers_id, p.family_id,  p.family_client_id, p.products_quantity_min, p.products_quantity_max, p.products_quantity_ideal from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$HTTP_GET_VARS['pID'] . "' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "'");
				$product = tep_db_fetch_array($product_query);
				// HEADER TAG
				$pInfo->objectInfo($product);
				$product_image_query = tep_db_query("select products_bimage from " . TABLE_PRODUCTS_IMAGE . " where products_id = '" . (int)$HTTP_GET_VARS['pID'] . "'");
				$i=2;
				while($product_image=tep_db_fetch_array($product_image_query)){
					$products_bimage_name_supp[$i]=$product_image["products_bimage"];
					$i++;
				}
			} elseif (tep_not_null($HTTP_POST_VARS)) {
				$pInfo->objectInfo($HTTP_POST_VARS);
				$products_name = $HTTP_POST_VARS['products_name'];
				$products_description = $HTTP_POST_VARS['products_description'];
				$products_url = $HTTP_POST_VARS['products_url'];
				$products_baseline = $HTTP_POST_VARS['products_baseline'];
				$balise_title_lien_image = $HTTP_POST_VARS['balise_title_lien_image'];
				$balise_title_lien_texte = $HTTP_POST_VARS['balise_title_lien_texte'];
			}
			
			$languages = tep_get_languages();
			
			if (!isset($pInfo->products_status)) $pInfo->products_status = '1';
			
			switch ($pInfo->products_status) {
				case '0': $in_status = false; $out_status = true; break;
				case '1':
				default: $in_status = true; $out_status = false;
			}
	
			$action_products = 'update_product&imageUpload=1';
			echo tep_draw_form('new_product', FILENAME_ARTICLE_EDIT_IMAGES_VIDEOS, 'cPath=' . $cPath . (isset($HTTP_GET_VARS['pID']) ? '&pID=' . $HTTP_GET_VARS['pID'] : '') . '&action='.$action_products, 'post', 'enctype="multipart/form-data"');
			
			require(DIR_WS_ARTICLE . 'article_menu.php');
            require(DIR_WS_ARTICLE . 'position_article.php');
			?>
            
			<?php
			// A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER 
			// A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER 
			// A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER
			// A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER 
			?>
			
            <div style="width: 50%; float: left; display:none;">
                <span style="padding: 10px;" class="pageHeading">Gestion des images de l'article</span>
                <?php require(DIR_WS_ARTICLE. 'article_images.php'); ?>
            </div>            
			<div style="width: 50%; float: right; display:none;">
                <span style="padding: 10px;" class="pageHeading">Gestion des vid�os de l'article</span>
                <?php require(DIR_WS_ARTICLE. 'article_videos.php'); ?>
			</div>
            <?php
			// FIN A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER - A EFFACER 
			?>
			
            <div style="clear: both;"></div>
            
            <span style="padding: 10px;" class="pageHeading">Gestion des pictogrammes l'article</span>
            
            <?php require(DIR_WS_ARTICLE. 'article_logos.php'); ?>
            
        	<div style="margin: auto; width: 65px; margin-bottom: 10px; padding: 5px; border: 1px solid #090; background-color: #EFF; text-align: center;">
				<?php 
                    echo tep_draw_hidden_field('products_date_added', (tep_not_null($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d')));
                    echo tep_image_submit('button_update.gif', IMAGE_UPDATE);
                ?>
            </div>
            
			<?php
                require(DIR_WS_INCLUDES . 'footer.php');
		?>
    </body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
