<?php
/*
  FAQ system for OSC 2.2 MS2 v2.1i  23.08.2007
  Originally Created by: https://adgrafics.com admin@adgrafics.net
  Updated by: https://www.webandpepper.ch osc@webandpepper.ch v2.0 (03.03.2004)
  LLast Modified by Cydonian (cydonia@flashmail.com) (23.08.2007)
  Released under the GNU General Public License
  osCommerce, Open Source E-Commerce Solutions
  Copyright (c) 2004 osCommerce
*/
?>
<script language="JavaScript">
  var MaxLen = 100;
  function countMe(form) {
    inputStr = form.question.value;
    strlength= inputStr.length;
    if (strlength > MaxLen ) form.question.value = inputStr.substring(0,MaxLen);
    form.num.value = (MaxLen - form.question.value.length);
    form.question.focus();
  }
  
  function change_lang(lang) {
    <?php echo "window.location.href = '" . FILENAME_FAQ_MANAGER . '?faq_action=' . $HTTP_GET_VARS['faq_action'] . '&' . "faq_lang='+lang;"; ?>
  }
</script>
<tr class="pageHeading"><td><?php echo $title; ?></td></tr>
<tr><td>
<table border="0" cellpadding=0 cellspacing=2">
<tr><td width="150" class="main">Ordre de tri</td>
<td class="main">
<?php 
  if ($edit[v_order]) {
  	$no = $edit[v_order];
  };
  echo tep_draw_input_field('v_order', "$no", 'size=3 maxlength=4');
?>
</td>
</tr>
<tr>
<td valign="top" class="main">Visible</td>
<td valign="top" class="main">
<?php
  if ($edit[visible]) {
  	$checked = "checked";
  };
  echo tep_draw_checkbox_field('visible', '1', $checked);
  /*
  // Not needed, remove comments to show
  if ($edit[visible]==1) {
	echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', FAQ_ID_ACTIVE);
  }else{
	echo tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', FAQ_ID_DEACTIVE);
  }
  */
?>
</td>
<!--</tr>
<?php 
  if ($HTTP_GET_VARS['faq_action'] != 'Edit') {
?>
<tr>
<td valign="top" class="main">Langue</td>
<td valign="top" class="main">
<?php
	$lang_query = tep_db_query("select directory from " . TABLE_LANGUAGES . " order by languages_id desc");
	while ($get_lang = tep_db_fetch_array($lang_query)) {
	  $langs[] = array('id' => $get_lang['directory'], 'text' => $get_lang['directory']);
	}
	if ($HTTP_GET_VARS['faq_lang']) {
	  $def_lang = $HTTP_GET_VARS['faq_lang'];
	} else {
	  $def_lang = $language;
	}
	echo tep_draw_pull_down_menu('faq_language',$langs,$def_lang,'onchange="change_lang(this.value);"');
?>
</td>
</tr>
<?php
  }
?> -->

<tr><td>Famille de la question</td>
    <td><select name="family">
       <?php
			 //requete pour recuperer l'id de la faq famille pour l'option selected
			 $requete_faq_id="SELECT faq_family_id FROM faq WHERE faq_id=".$_GET['faq_id'];
			 $resultat_faq=mysql_query($requete_faq_id);
			 $ID_FAQ_FAM=mysql_fetch_array($resultat_faq);
			 
			 //requete normale pour effectuer la liste deroulante
			 $requete_family_faq="SELECT faq_family_id, faq_name_family FROM faq_family ORDER BY v_order";
			 $resultat_requete=mysql_query($requete_family_faq);
			 
			 
			 
			 while($FAQ=mysql_fetch_array($resultat_requete))
			  {
				  if($FAQ['faq_family_id'] == $ID_FAQ_FAM['faq_family_id'])
            {
               echo'<option value="'.$FAQ['faq_family_id'].'" selected="selected">'.$FAQ['faq_name_family'].'</option>';
             }
          else
          { echo'<option value="'.$FAQ['faq_family_id'].'">'.$FAQ['faq_name_family'].'</option>';}
				}
				
				//echo tep_draw_pull_down_menu('family',$resultat_requete,'','onchange="change_lang(this.value);"');
			 ?>
       </select></td>
</tr>



<tr><td valign="top" class="main">La Question<br />
<script>
  document.write("(max. "+MaxLen+" caract�res)");
</script>
<br />
<?php echo tep_draw_input_field('num', '', 'size=3 readonly STYLE="color: red" '); ?>
</td>
<td valign="top">
<?php echo tep_draw_textarea_field('question', '', '100', '2', $edit['question'], 'onChange="countMe(document.forms[0])" onKeyUp="countMe(document.forms[0])" '); ?>

</td>
</tr>
<tr>
<td valign="top" class="main">La R�ponse</td>
<td>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
  	<td>
    	<?php echo tep_draw_textarea_field('answer', '', '100', '10', $edit['answer']); ?>
		</td>
    <td class="main" valign="top">
    &nbsp;&nbsp;&nbsp; <u><b>Quelques balise de mise en forme de texte</b></u><br />
    <br />
    &nbsp;&nbsp;&nbsp; &lt;br /&gt; permet de faire un pasage � la ligne.<br />
    &nbsp;&nbsp;&nbsp; &lt;strong&gt; Votre texte &lt;/strong&gt; permet d'�crire en gras.<br />
    &nbsp;&nbsp;&nbsp; &lt;u&gt; Votre texte &lt;/u&gt; permet d'�crire en soulign�.<br />
    &nbsp;&nbsp;&nbsp; &lt;i&gt; Votre texte &lt;/i&gt; permet d'�crire en italique.<br />
		</td>
  </tr>
</table>

</td>
</tr>
<tr><td></td>
<td align="right">
<?php
  echo tep_image_submit('button_save.gif', IMAGE_SAVE);
  echo '<a href="' . tep_href_link(FILENAME_FAQ_MANAGER, '', 'NONSSL') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>';
?>
</td>
</tr>
</table>
</form>
</td></tr>