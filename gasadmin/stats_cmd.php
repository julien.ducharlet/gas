<?php
/* Liste des produits invendus (ou peu vendus) Par Paul*/

require('includes/application_top.php');
require('includes/functions/date.php');//contient tab_jour_francais
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

/*
if ($_POST['action'] == 'export'){
	if (isset($_POST['export_article'])){
		
		$contenu_fichier = "id;R�f�rence;Nom;Prix d'achat;Prix de vente;Marge;Qt� en stock;Statut\n";
		
		foreach($_POST['export_article'] as $id_article){
			$reponse = mysql_query('SELECT products_model, products_quantity, products_price, products_cost, products_status FROM products WHERE products_id = "' . $id_article . '"');
			$donnees = mysql_fetch_array($reponse);
			
			$reponse = mysql_query('SELECT products_name FROM products_description WHERE products_id = "' . $id_article . '"');
			$result_nom = mysql_fetch_array($reponse);
			$products_name = $result_nom['products_name'];
			
			$suppressions_surplus = array("pour", "#", "*", "[1]");
			$titre_description = str_replace($suppressions_surplus, "", $products_name);
			
			$marge = $currencies->format($donnees['products_price'] - $donnees['products_cost']);
			$prix_achat = $currencies->format($donnees['products_cost']);
			$prix_vente = $currencies->format($donnees['products_price']);
			
			if ($donnees['products_status'] == 0)
				$statut = "Hors Ligne";
			else
				$statut = "En Ligne";
			
			$contenu_fichier .= $id_article . ";" . $donnees['products_model'] . ";" . $titre_description . ";" . $prix_achat . ";" . $prix_vente . ";" . $marge . ";" . $donnees['products_quantity'] . ";" . $statut . "\n";
		}
		
		$fichier = fopen('telechargement/articles_supprimes.csv', 'w');
		
		fputs($fichier, $contenu_fichier);
		
		fclose($fichier);
		?>
		
        <script language="JavaScript"> 
		window.open("telechargement/articles_supprimes.csv","export","");
		</script>
		
        <?php	
	}
}*/
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <link type="text/css" rel="stylesheet" href="includes/javascript/dhtmlgoodies_calendar.css" media="screen"></LINK>
		<SCRIPT type="text/javascript" src="includes/javascript/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="includes/general.js"></script>
		<script language="javascript" src="includes/javascript/function.js"></script>
        <script language="javascript" src="includes/menu.js"></script>
    </head>

    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
            <?php require(DIR_WS_INCLUDES . 'header.php'); 
			
			$marge_min=350;
			if(empty($HTTP_GET_VARS["date_fin"])){
				$date_fin=date("d-m-Y");
			}
			if(empty($HTTP_GET_VARS["date_deb"])){
				//$date_deb=date("d-m-Y",mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
				$tab_fin=explode("-",$date_fin);
				$date_deb="01-".$tab_fin[1]."-".$tab_fin[2];
			}
			
			
            // Construction de la liste d�roulante des familles d'articles
            $reponse = mysql_query('SELECT family_id, family_name FROM family order by family_name'); // On interroge la base pour avoir les familles
            // On rempli un tableau avec les familles
            while ($donnees = mysql_fetch_array($reponse)){
                $list_family_array[] = array('id' => $donnees['family_id'], 'text' => ucwords(strtolower($donnees['family_name'])));
            }
			?>
            
            <div width="100%" style="padding: 4px;">
                <table border="0" width="100%" height="44" style="background-color: #e1e1f5;" class="smallText">
                    <tr>
                        <!-- Formulaire des filtres -->
                        <form id='Filtres' name='Filtres' method="get" action="<?php echo $PHP_SELF;?>" >
                            <td width="5%">&nbsp;</td>
							<td width="30%">     
							<?php	echo "Du : ".tep_draw_input_field('date_deb', $date_deb, 'size="10" id="focusorders"');?>
                            		<a href="#" onClick="displayCalendar(Filtres.date_deb,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
							<?php 	echo "Au&nbsp;".tep_draw_input_field('date_fin', $date_fin, 'size="10" id="focusorders"');?>
                            		<a href="#" onClick="displayCalendar(Filtres.date_fin,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" width="22" height="22" border="0" align="absmiddle"></a>
                            </td>
            				<!--<td width="30%">
                                <?php
								echo "Famille article : ";
                                echo tep_draw_pull_down_menu('family', array_merge(array(array('id' => '0', 'text' => 'Aucune famille'), array('id' => '-1', 'text' => 'Toute famille')), $list_family_array), '', 'onChange="this.form.submit();"');
                                ?>
                            </td>-->
                            <td width="5%"><input type=submit value="Go"></td>
                        </form>
                    </tr>
                </table>
                
				<br />
                <form method="post" action='<?php echo $PHP_SELF . "?" . tep_get_all_get_params(array('action')); ?>'>
                <table border="0" align="center" width="100%" cellspacing="0">
                    <tr class="dataTableHeadingRow">
                    	<th class="dataTableHeadingContent">Jour</th>
                        <th class="dataTableHeadingContent">CA HT</th>
                        <th class="dataTableHeadingContent">TVA</th>
                        <th class="dataTableHeadingContent">CA TTC</th>
                        <th class="dataTableHeadingContent">Marge</th>
                        <th class="dataTableHeadingContent">Nb Commandes</th>
                        <th class="dataTableHeadingContent">Nb Articles</th>
                    </tr>
                    <?php
                        $rqt_marque = '';
						if (isset($_GET['manufacturers']) && $_GET['manufacturers'] != '-1'){
							$rqt_marque =  "and manufacturers_id = '" . $_GET['manufacturers'] . "'";
						}
						
						$rqt_famille = '';
						if (isset($_GET['family']) && $_GET['family'] != '-1'){
							$rqt_famille =  "and family_id = '" . $_GET['family'] . "'";
						}
						
						$date_deb = tep_db_prepare_input(date_fr_to_bdd($date_deb));
      					$date_fin = tep_db_prepare_input(date_fr_to_bdd($date_fin));
      						
						$query_rech="Select substr(date_purchased,1,10) as jour, sum(ss_total) as ca_ht, sum(tva_total) as tva, sum(total) as ca_ttc, sum(marge) as marge_tot, count(1) as nb_cmd, sum(nb_articles) as nb_art
									 From orders
									 Where orders_numero_facture>0 and date_purchased>='".$date_deb."' and date_purchased<='".$date_fin."'". $rqt_famille . " " . $rqt_marque.
								   " GROUP BY SUBSTR(date_purchased,1,10)
								   	 Order by jour asc";
                        //echo $query_rech;
                        $resultat = mysql_query($query_rech);
						$i=0;
                        while ($donnees = mysql_fetch_array($resultat)){
							$i % 2 ? $class = RCS_REPORT_EVEN_STYLE : $class = RCS_REPORT_ODD_STYLE;
							$i++;
							?>
                        	<tr class="<?php echo $class; ?>" style="text-align: center;">
                            	<td class="dataTableHeadingContent" align="center">
									<?php 
									$tab_jour=explode("-",$donnees["jour"]);
									$jour=date("w",mktime(0, 0, 0, date($tab_jour[1]), date($tab_jour[2]), $tab_jour[0]));
									echo $tab_jour_francais[$jour]." ".$tab_jour[2]." ".$tab_jour[1]." ".$tab_jour[0];
									?>
                                </td>
                                <td class="datatablecontent"><?php echo $donnees["ca_ht"];?></td>
                                <td class="datatablecontent"><?php echo $donnees["tva"];?></td>
                                <td class="datatablecontent"><?php echo $donnees["ca_ttc"];?></td>
                                <td class="datatablecontent"><?php if($donnees["marge_tot"]<$marge_min){ echo "<font color=\"red\"><b>";} echo $donnees["marge_tot"]; if($donnees["marge_tot"]<$marge_min){ echo "</b></font>";}?></td>
                                <td class="datatablecontent"><?php echo $donnees["nb_cmd"];?></td>
                                <td class="datatablecontent"><?php echo $donnees["nb_art"];?></td>
                            </tr>
                  <?php 
							$ca_ht+=$donnees["ca_ht"];
							$tva+=$donnees["tva"];
							$ca_ttc+=$donnees["ca_ttc"];
							$marge_tot+=$donnees["marge_tot"];
							$nb_cmd+=$donnees["nb_cmd"];
							$nb_art+=$donnees["nb_art"];
						}?>
                  <tr style="text-align: center;">
                  	<td class="dataTableHeadingContent">Total</td>
                  	<td class="datatablecontent"><?php echo $ca_ht;?></td> 
                  	<td class="datatablecontent"><?php echo $tva;?></td>
                    <td class="datatablecontent"><?php echo $ca_ttc;?></td> 
                    <td class="datatablecontent"><?php echo $marge_tot;?></td> 
                    <td class="datatablecontent"><?php echo $nb_cmd;?></td>
                    <td class="datatablecontent"><?php echo $nb_art;?></td>
                  </tr>
                </table>
<!--            <br />
                <button onClick="this.form.submit();">Exporter les lignes coch�es dans un fichier .csv</button>
                <input type="hidden" name="action" value="export">
-->                
                </form>
            </div>
		<?php require(DIR_WS_INCLUDES . 'footer.php'); ?><br /> 
	</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>