<?php  /*
  $Id: reclamation_poste.php, v 2.2 11/03/2009 delete / phocea (forum oscommerce-fr.info) $

  This script is not included in the original version of osCommerce

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

*/

require('includes/application_top.php') ; 

include(DIR_WS_INCLUDES . 'reclamaposte.php');
include(DIR_WS_FUNCTIONS . 'reclamaposte.php');
include(DIR_WS_FUNCTIONS . 'reclamaposte_dates.php');


define (DIR_WS_ICONS, DIR_WS_IMAGES.'/icons/');

if (function_exists('ini_get') && function_exists('ini_set') ) {
   @set_time_limit(300) ; // Eviter le timeout PHP - Ne fonctionne pas en safe_mode
}
 
//
// On valide les commandes pour lesquelles la demande de remboursement a �t� faite
//
if ( isset($_POST['validation']) ) {
	if (COLIPOSTE_IN_TEST == 1) {
		$orders_query = 'update ' . TABLE_ORDERS_STATUS_HISTORY . ' set laposte_status = NULL, laposte_last_date= NULL, laposte_md5 = NULL, laposte_notif_md5 = NULL where ' . TRACKING_DB_FIELD . ' IS NOT NULL;' ;
		$orders_query_r =  tep_db_query($orders_query) ;
		$messageStack->add_session('RESET DONE', 'success');
	}
	if ( file_exists($fremboursements) )
	{
		$remboursements = file_get_contents($fremboursements) ;

		$liste_colis = explode('|', $remboursements) ;

		for($i = 0 ; $i < sizeof($liste_colis) ; $i++)
		{
			if ( ! tep_not_null($liste_colis[$i]) ) continue ;

			$str = explode(';', $liste_colis[$i]) ;

			$order_id        = $str[0] ;
			$date_expedition = $str[1] ;
			$date_livraison  = $str[2] ;
			$noColis         = $str[3] ;
			$nb_jours_ouvres = $str[4] ;

			if ( !tep_not_null($order_id) || COLIPOSTE_IN_TEST == 1)  continue ;

			$orders_query = 'update ' . TABLE_ORDERS_STATUS_HISTORY . ' set laposte_status = "'. COLIPOSTE_RECLAMATION_DEMANDE . '" where orders_id = "' . $order_id . '" and ' . TRACKING_DB_FIELD . ' = "'.$noColis.'"  ; ' ;
			$orders_query_r =  tep_db_query($orders_query) ;
		}
		unlink($fremboursements) ;
		if (COLIPOSTE_IN_TEST != 1) $messageStack->add_session('Table demande de remboursements mise � jour !', 'success');
	
	}
	else
	{
		$messageStack->add_session('faire une demande de remboursement avant !', 'error');
	}
  tep_redirect(basename(__FILE__)) ;
}
elseif( isset($_POST['verify']) ) {
	// Remise � zero du fichier de demande de remboursements
	file_put_contents($fremboursements, '');

	// Recup�ration des information du fichier
	$post = file_get_contents($flist) ; 

	$liste_colis = explode('|', $post) ; 

	for($i = 0 ; $i < sizeof($liste_colis) ; $i++)
	{
		if ( ! tep_not_null($liste_colis[$i]) ) continue ; 

		$str = explode(';', $liste_colis[$i]) ;

		$noColis = $str[0];
		$date_expedition = $str[1]; 
		$order_id = $str[2]; 
		$coliposte_status = $str[3];

		// Test Coh�rence N� Colis
		if (strlen($noColis) != 13) 	continue;

		// Date exp�dition de la commande
		$y = substr($date_expedition, 0, 4) ; 
		$m = substr($date_expedition, 5, 2) ; 
		$d = substr($date_expedition, 8, 2) ; 
		$date_expedition = strtotime(sprintf('%04d-%02d-%02d', $y, $m, $d)) ;

		// si statut deja � retard on ne test plus
		if ($coliposte_status == COLIPOSTE_LIVRAISON_RETARD) {
			$date_livraison = $str[4];
			$nb_jours_ouvres = $str[5];
			$remboursements = sprintf('%s;%s;%s;%s;%s|', $order_id, $date_expedition, $date_livraison, $noColis, $nb_jours_ouvres) ; 
			file_put_contents($fremboursements, $remboursements, FILE_APPEND);
			continue;
		}

		// Si la commande date de + que le nb de jours sp�cifi�, nul besoin de demander...
		$age_commande = get_nb_open_days($date_expedition, time());

		$laposte_md5 = '';
		$statuses_found = array();
		if ($age_commande > COLIPOSTE_TO_BE_ARCHIVED) {
			# On archive la commande ici
			$case  = 10 ;			
		} else {
			// On r�cup�re les status sur le site de la poste
			$statuses_found = grab_coliposte_statuses($noColis);
			$laposte_md5 = $statuses_found[0];
			if (array_key_exists($laposte_md5, $status)) {
				// on interprete les donn�es
				$case =  merci_la_poste($noColis, $statuses_found) ;	
			} else {
				// We don't know the status
				if (is_array($statuses_found) && sizeof($statuses_found) > 0) {
					$case = -1;
				} else {
					$case = -2;
				}
				
			}
		}

		$laposte = '';
		
		$date_livraison  = '';
		$nb_jours_ouvres = '';

		switch( $case ) {
			// Le Colis a �t� livr�, process de v�rification des dates.
			case 1 :
				// OCR processing
				$date_livraison  = interpret_date();
				$nb_jours_ouvres = get_nb_open_days($date_expedition, $date_livraison);

        // Probl�me de lecture
        //
        if ( $date_livraison < 0 ) break ;

				if ( $nb_jours_ouvres > 2) {
					// On g�n�re le fichier destin� � la poste ! 
					$remboursements = sprintf('%s;%s;%s;%s;%s|', $order_id, $date_expedition, $date_livraison, $noColis, $nb_jours_ouvres) ; 
					file_put_contents($fremboursements, $remboursements, FILE_APPEND);
					$laposte = COLIPOSTE_LIVRAISON_RETARD;
				} else {
					$laposte = COLIPOSTE_LIVRAISON_OK;
				}
				break;
			// Cas dans lesquels la commande est encore en transit
			case 5 :
				// OCR processing
				$date_livraison  = interpret_date();
				$nb_jours_ouvres = get_nb_open_days($date_expedition, $date_livraison);
				$laposte = COLIPOSTE_LIVRAISON_TRANSIT;
				break;
			// Cas dans lesquels la commande n'est pas remboursable
			case 9 :
				$date_livraison  = interpret_date();
				$nb_jours_ouvres = get_nb_open_days($date_expedition, $date_livraison);
				$laposte = COLIPOSTE_REMBOURSEMENT_IMPOSSIBLE;
				break;
			// Cas dans lesquels on archive la commande
			case 10 :
				$laposte = COLIPOSTE_ARCHIVE;
				break;
			// Le colis n'existe pas sur colisposte !
			case -2 :
				$laposte = COLIPOSTE_NO_INFORMATION;
				break;
			// Cas dans lesquels l'image n'a pas �t� reconnu
			case -1 :
				$laposte = COLIPOSTE_ERREUR_MD5;
				break;
		}
	
	$orders_query = 'update ' . TABLE_ORDERS_STATUS_HISTORY . ' set laposte_status = "' . $laposte .'", laposte_last_date="' . $date_livraison .'", laposte_md5="' . $laposte_md5 . '" where orders_id = "' . $order_id . '" and ' . TRACKING_DB_FIELD . ' = "'.$noColis.'"  ; ' ;
	$orders_query_r =  tep_db_query($orders_query) ;
	
	if ( $i % 10 ) usleep(300) ;

	}
  tep_redirect(basename(__FILE__)) ;
}


//
// Module d'affichge des commandes �ligibles
//
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/javascript/jQuery/contextMenu/jquery.contextMenu.css">
<script type="text/javascript" src="includes/general.js"></script>
<script type="text/javascript" src="includes/javascript/jQuery/jquery.js"></script>
<script type="text/javascript" src="includes/javascript/jQuery/contextMenu/jquery.contextMenu.js"></script>
<script type="text/javascript">
	$(document).ready( function() {
		
		$('#statusTableHeader').disableContextMenu();
		// Show menu when a row in the table is clicked
		$("#statusTable TR").contextMenu({
			menu: 'statusChangeMenu'
		},
			function(action, el, pos) {
				var id = ($(el).attr('id')).split('|');	
				$.ajax({
					url: 'reclamaposte_ajax_request.php',
					data: "orderid="+id[0]+"&tracknum="+id[1]+"&action="+action,
					type: 'GET',
					dataType: 'text',
					processData: false,
					error: function(){
						alert('Error loading reclamaposte_ajax_request.php');},
					success: function(responseText){
						$(el).find("td:first").html(decodeURIComponent(responseText));
					}
				});
		});
	});			
</script>

</head>
<body>
<!-- context menu layout //-->
<ul id="statusChangeMenu" class="contextMenu">
			<li class="late"><a href="#late">Flaguer en retard</a></li>
			<li class="intime separator"><a href="#intime">Flaguer en temps</a></li>
			<li class="cash"><a href="#cash">Flaguer rembours�</a></li>
			<li class="reset separator"><a href="#reset">Reset flag</a></li>
</ul>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" summary="" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" summary="" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<!-- Expeditor Begin //-->
    <tr><td class="pageHeading"><?php echo 'R�clamations La Poste' ; ?></td></tr>
        <tr>

        <td class="smallText"><?php echo 'T�l�chargement des �tats en cours' ; ?>
        <?php echo tep_draw_form('verify', basename(__FILE__)); ?>
        
        <input type="submit" name="verify" /> &nbsp;&nbsp;
        </form>
        </td>

        <td class="smallText"><?php echo 'G�n�rer la demande de remboursement' ; ?>
        <?php 
          if (CONTRAT_PRO) {
          	$format = 'xls';
          } else {
          	$format = 'pdf';
          }
          $file = 'reclamaposte_pdf.php' ;
          echo tep_draw_form('ok', 'reclamaposte_extract.php', 'format='.$format, 'POST', 'enctype="multipart/form-data" target="_blank"'); ?>
    		<input type="submit" /> &nbsp;&nbsp;
        </form>
        </td>

        <td class="smallText"><?php echo 'Valider la demande de remboursement' ; ?>
         <?php echo tep_draw_form('validation', basename(__FILE__)); ?>
                <input type="submit" name="validation" />
        </form>
        </td>


    </tr>
    </td>
<!-- Body Begin //-->

</table><table width="100%" border="0">
<?php 
//
// Import du fichier
//
if ( file_exists($fremboursements) ) {
  $mtime = date('d/m/Y', filemtime($fremboursements)) ; 
  $remboursements = file_get_contents($fremboursements);
  $liste_colis = explode('|', $remboursements) ;

	if (strlen($remboursements) > 0 && count($liste_colis)) {
	    
?>
    <tr class="dataTableHeadingRow">
        <td class="dataTableHeadingContent">Status</td>
        <td class="dataTableHeadingContent">Commande</td>
        <td class="dataTableHeadingContent">Colis</td>
        <td class="dataTableHeadingContent">Date Exp�dition</td>
        <td class="dataTableHeadingContent">Date Pr�sentation</td>
        <td class="dataTableHeadingContent">Jours</td>
        
    </tr>
<?php 

		echo '<h3>Derni�res Demandes de Remboursements G�n�r�es (' . $mtime . ') </h3>' ;    
	 
		for($i = 0, $y = 50 ; $i < sizeof($liste_colis) ; $i++) {
			if ( ! tep_not_null($liste_colis[$i]) ) continue ;
		        
		  $str = explode(';', $liste_colis[$i]) ;
		
		  $orders_id       = $str[0] ;
		  $date_expedition = $str[1] ;
		  $date_livraison  = $str[2] ;
		  $noColis         = $str[3] ;
		  $nb_jours_ouvres = $str[4] ;
		  
		  $image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_LIVRAISON_RETARD']['icon'], $etat['COLIPOSTE_LIVRAISON_RETARD']['description']) ; 
?>
<tr>
     <td class="dataTableContent" align="left" valign="top"><?php echo $image_status  ;?></td>
     <td class="dataTableContent" align="left" valign="top"><?php echo '<a href="' . tep_href_link(FILENAME_CMD_EDIT, 'action=edit&oID=' . $orders_id) . '" onclick="target=\'_blank\'">'.$orders_id.'</a>'; ?></td>
     <td class="dataTableContent" align="left" valign="top"><?php echo '<a href="'.$main_url . $url_params. $noColis. '"target=_blank">'. $noColis.'</a>'; ?></td>
     <td class="dataTableContent" align="left" valign="top"><?php echo date('d/m/Y', $date_expedition) ; ?></td>
     <td class="dataTableContent" align="left" valign="top"><?php echo $date_livraison ? date('d/m/Y',$date_livraison) : "Inconnue" ; ?></td>
     <td class="dataTableContent" align="left" valign="top"><?php echo $nb_jours_ouvres ; ?></td>
</tr>
<?php
		}
	} else {
	echo "Aucune demande de Remboursement en Attente" ;
	}
} else {
    echo "Aucune demande de Remboursement en Attente" ;
}
?>
</table>

<table id="statusTable" width="100%" border="0">
    <tr id="statusTableHeader" class="dataTableHeadingRow">
        <td class="dataTableHeadingContent">Status</td>
        <td class="dataTableHeadingContent">Commande</td>
        <td class="dataTableHeadingContent">Colis</td>
        <td class="dataTableHeadingContent">Date Exp�dition</td>
        <td class="dataTableHeadingContent">Date Status</td>
        <td class="dataTableHeadingContent">Jours</td>
    </tr>
<?php
        echo '<h3>Liste des colis envoy�s depuis le '.date('d-m-Y', strtotime("-".COLIPOSTE_MAX_DAYS." days")).'</h3>' ;

        $listing = '';
        if (COLIPOSTE_MAX_DAYS > 0) {
        	$orders_query = "select o.`orders_id`, o.`orders_status`, o.`customers_email_address`,  o.`date_purchased`, o.`customers_name`, os.`date_added`, os.`".TRACKING_DB_FIELD."`, os.`laposte_last_date`, os.`laposte_status`, os.`laposte_md5` from " . TABLE_ORDERS . " o, ". TABLE_ORDERS_STATUS_HISTORY ." os where o.orders_id = os.orders_id and SUBSTR(os.".TRACKING_DB_FIELD.", 1, 2) IN ('8N','8U','8L','8P', '8V','7D') and DATE_SUB(CURDATE(),INTERVAL ".COLIPOSTE_MAX_DAYS." DAY) <= o.date_purchased order by o.date_purchased desc, os.date_added asc;";
        } else {
        	$orders_query = "select o.`orders_id`, o.`orders_status`, o.`customers_email_address`,  o.`date_purchased`, o.`customers_name`, os.`date_added`, os.`".TRACKING_DB_FIELD."`, os.`laposte_last_date`, os.`laposte_status`, os.`laposte_md5` from " . TABLE_ORDERS . " o, ". TABLE_ORDERS_STATUS_HISTORY ." os where o.orders_id = os.orders_id and SUBSTR(os.".TRACKING_DB_FIELD.", 1, 2) IN ('8N','8U','8L','8P', '8V','7D') order by o.date_purchased desc, os.date_added asc;";
        }

        $orders_query_r =  tep_db_query($orders_query) ;
	
        $listing = '' ;
        $coliposte = array();
        
        while($qr = tep_db_fetch_array($orders_query_r)) {
          $customer_email      = $qr['customers_email_address'] ;
          $date_purchased      = $qr['date_purchased'] ;
          $customers_name      = $qr['customers_name'] ;
          $orders_id           = $qr['orders_id'] ;
          $last_event          = substr($qr['date_added'], 0, strpos($qr['date_added'], ' ')) ;
          $track_num           = $qr[TRACKING_DB_FIELD] ;
          $laposte_last_date   = $qr['laposte_last_date'] ;
          $laposte_status      = $qr['laposte_status'] ;
          $laposte_md5         = $qr['laposte_md5'] ;

          // On skip les doublons (tracking ajout�s plusieurs fois sur differents status de commande ou plusieurs commandes envoy�s sous le meme tracking)
          if (in_array($track_num, $coliposte)) continue;
          $coliposte[] = $track_num;

      	  // Sp�cifique la poste, tracking number fran�ais. sinon on skip.
      	  if (preg_match("/(8N|8U|8L|8V|8P|7D)[a-z0-9]{1,11}$/i", $track_num) == 0) continue;
      
      	  // On v�rifie l'�tat de v�rification des livraison
      	  $image_status = getImageforStatus($laposte_status);
          $nb_jours_ouvres = get_nb_open_days(strtotime($last_event), $laposte_last_date);
?>
      	  
          <tr id="<?php echo $orders_id.'|'.$track_num;?>" class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
           <td class="dataTableContent" align="left" valign="top"><?php echo COLIPOSTE_IN_TEST ? $image_status . '-'. $laposte_status .'-'. $laposte_md5  : $image_status;?></td>
           <td class="dataTableContent" align="left" valign="top"><?php echo '<a href="' . tep_href_link(FILENAME_CMD_EDIT, 'action=edit&oID=' . $orders_id) .  '" onclick="target=\'_blank\'">' .$orders_id.'</a>'; ?></td>
           <td class="dataTableContent" align="left" valign="top"><?php echo '<a href="'.$main_url . $url_params. $track_num. '"target=_blank">'. $track_num.'</a>'; ?></td>
           <td class="dataTableContent" align="left" valign="top"><?php echo date('d-m-Y', strtotime($last_event)) ; ?></td>
           <td class="dataTableContent" align="left" valign="top"><?php echo $laposte_last_date ? date('d/m/Y',$laposte_last_date) : "-" ; ?></td>
           <td class="dataTableContent" align="left" valign="top"><?php echo ($nb_jours_ouvres > 0) ? 'J+'.$nb_jours_ouvres : "-"; ?></td>
         </tr> 
<?php
          // Cas ou la commande n'est plus � traiter :
          if ($laposte_status != COLIPOSTE_LIVRAISON_OK
          && $laposte_status != COLIPOSTE_RECLAMATION_DEMANDE 
          && $laposte_status != COLIPOSTE_RECLAMATION_REMBOURSE 
          && $laposte_status != COLIPOSTE_REMBOURSEMENT_IMPOSSIBLE 
          && $laposte_status != COLIPOSTE_ARCHIVE) {
          	// On g�n�re un listing des commandes a exporter, pour remboursement
         	 $listing .= sprintf('%s;%s;%s;%s;%s;%s|', $track_num, $last_event, $orders_id, $laposte_status, $laposte_last_date, $nb_jours_ouvres);
        	}
        }
      	file_put_contents($flist, $listing) ;
?>
<!-- Body End //-->
    </tr></td>    </table>
<!-- Expeditor End //-->

    </table>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->