<?php
	$time_start = explode(' ', PAGE_PARSE_START_TIME);
	$time_end = explode(' ', microtime());
	$parse_time = number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3);
	
	if ($_REQUEST['DISPLAY_QUERIES'] == 'true') {
		$lien = $PHP_SELF . "?" . substr(tep_get_all_get_params(array('DISPLAY_QUERIES')), 0, -1);
	} else {
		$lien = $PHP_SELF . "?" . tep_get_all_get_params(array('DISPLAY_QUERIES')) . "DISPLAY_QUERIES=true";
	}
	echo '<br /><div align="center"><span class="smallText" style="color:#000000">Current Parse Time: <b>' . $parse_time . ' s</b> with <a href="' . $lien . '"><b>' . sizeof($debug['QUERIES']) . ' queries</b></a></span></div>';

	// Modif par Paul : affichage tableau avec les requ�tes plutot que la liste brute
	if ($_REQUEST['DISPLAY_QUERIES'] == 'true') {
	?>
    
    <div style="margin: 5px; margin-top: 10px;">
		<table border="1" width="100%" cellspacing="0">
        	<tr class="dataTableHeadingRow">
            	<th class="dataTableHeadingContent" width="2%">ID</th>
            	<th class="dataTableHeadingContent" width="96%">Requ&ecirc;tes</th>
                <th class="dataTableHeadingContent" width="2%">Dur&eacute;e</th>
            </tr>
            
		<?php 
		foreach($debug['QUERIES'] as $indice => $valeur){
			$temps = $debug['TIME'][$indice];
			
			//Coloration des requ�tes
			if ($temps >= 0.01) {
				$style = 'style="font-weight: bold; background-color: red;"';
				$image = '<img style="margin: 0px; vertical-align: middle;" src="' . DIR_WS_IMAGES . '/icons/attention.png" alt="/!\">';
			}
			else if ($temps >= 0.001) {
				$style = 'style="background-color: #FC6;"';
				$image = '<img style="margin: 0px; vertical-align: middle;" src="' . DIR_WS_IMAGES . '/icons/attention.png" alt="/!\">';
			}
			else {
				$style = '';
				$image = '';
			}
			
			?>
            <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)">
                <td <?php echo $style; ?> class="datatablecontent"><?php echo $indice . '&nbsp;' . $image; ?></td>
                <td <?php echo $style; ?> class="datatablecontent"><?php echo $valeur; ?></td>
                <td <?php echo $style; ?> class="datatablecontent"><?php echo $temps; ?></td>
            </tr>
        	<?php 
		}
		?>
		</table>
	</div>
     
	<?php
	}
    
unset($debug);
?>
