<?php
/*
  $Id: reclamaposte.php, v 1.7 24/01/2009 delete (forum oscommerce-fr.info) $

  This script is not included in the original version of osCommerce

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce
  
  Fichier commun de configuration 
*/

if(!function_exists('file_put_contents')) 
{
    function file_put_contents($filename, $data, $file_append = false) {
        $fp = fopen($filename, (!$file_append ? 'w+' : 'a+'));
        if(!$fp) {
            trigger_error('file_put_contents - erreur écriture dans.', E_USER_ERROR);
            return;
        }
        fputs($fp, $data);
        fclose($fp);
    }
}
// !! PARTIE IMPORTANTE !!
define('STATUS_COMMANDE_DELIVREE', '100008'); //numero du statut commande = a livre

//define('TRACKING_DB_FIELD', 'tracking_id'); // Nom du champ 'No De Colis' dans la table order_history - Merci à Phocea
define('TRACKING_DB_FIELD', 'track_num'); // Nom du champ 'No De Colis' dans la table order_history - Merci à Phocea

// Indiquer s'il s'agit d'un contrat PRO : génération d'un fichier XLS ou d'envois standards fichier de réclamation format PDF
define('CONTRAT_PRO', true);
//define('CONTRAT_PRO', false);

define('COLIPOSTE_MAX_DAYS', 10);// Nb de jours max à inclure dans la recherche - Mettre 0 pour pas de limite
define('COLIPOSTE_MAX_RECLAMATION', 30); //Indique le nombre max de colis à lister par réclamation
define('COLIPOSTE_TO_BE_ARCHIVED', 30); // Nombre de jours à partir duquel on considaire le colis a passer en archive 
define('COLIPOSTE_IN_TEST', 0); // Mettre à 1 pour activer le mode test. Les commandes ne seront pas avancé au status COLIPOSTE_RECLAMATION_DEMANDE

// Adresses email (contrat PRO) 
define('LAPOSTE_EMAIL_DESTINATAIRE', 'scsmb.clp@laposte.fr, info@boutiquedesaccessoires.fr') ; // Adresse du destinataire (scsmb.clp@laposte.fr)
define('LAPOSTE_EMAIL_EXPEDITEUR', MAIL_INFO) ; // Adresse de l'expéditeur
define('LAPOSTE_EMAIL_REPONSE', MAIL_INFO) ; // Adresse de réponse (en théorie identique à celle de l'expéditeur)

// FIN DE LA PARTIE IMPORTANTE //


// Librairie PDF
//
if (CONTRAT_PRO){
  // Répertoire contenant la librairie PHP Write Excel                            	
  $wxls_library_path = 'wxls/' ;
} else {
	 define('FPDF_FONTPATH','fpdf/font/');
}

// Répertoire Temporaire
//$tmp_dir = '/tmp' ; // en dédié
$tmp_dir = DIR_FS_ADMIN . 'tmp' ; // en mutualisé
 
// Urls and Parameters du site coliposte
$main_url = 'https://www.coliposte.net';
$url_params = '/gp/services/main.jsp?m=10003005&colispart=';

// Fichiers temporaires
$cookie_filename = $tmp_dir . '/laposte.txt' ;
$tmp_xls = $tmp_dir . '/tmp.xls' ;


// Fichiers de traitement  
$fremboursements = $tmp_dir . "/laposte.remboursements.txt" ; 
$flist = $tmp_dir . "/laposte.list.txt" ; 
$output_xls_name = 'laposte_' . date('Y-m-d', time()) . '.xls' ;

if ( ! file_exists($flist) ) file_put_contents($flist, '') ; // création si inexistant

define('COLIPOSTE_LIVRAISON_RETARD', 5); 
define('COLIPOSTE_LIVRAISON_OK', 10);
define('COLIPOSTE_LIVRAISON_TRANSIT', 15);
define('COLIPOSTE_LIVRAISON_UNKNOWN', 20);
define('COLIPOSTE_REMBOURSEMENT_IMPOSSIBLE', 25);
define('COLIPOSTE_RECLAMATION_DEMANDE', 35);
define('COLIPOSTE_RECLAMATION_REMBOURSE', 40);
define('COLIPOSTE_ARCHIVE', 50);
define('COLIPOSTE_NO_INFORMATION', 90);
define('COLIPOSTE_ERREUR_MD5', 99);
 
$etat['COLIPOSTE_LIVRAISON_RETARD']['description'] = 'Livraison effectu&eacute; en retard';
$etat['COLIPOSTE_LIVRAISON_RETARD']['icon'] = 'icon_reclamaposte_late.png';
$etat['COLIPOSTE_LIVRAISON_OK']['description'] = 'Livraison effectu&eacute; &agrave; temps';
$etat['COLIPOSTE_LIVRAISON_OK']['icon'] = 'icon_reclamaposte_ok.png';
$etat['COLIPOSTE_LIVRAISON_TRANSIT']['description'] = 'Livraison toujours en cours';
$etat['COLIPOSTE_LIVRAISON_TRANSIT']['icon'] = 'icon_reclamaposte_transit.png';
$etat['COLIPOSTE_NO_INFORMATION']['description'] = 'Etat de livraison inconnu - A v&eacute;rifier';
$etat['COLIPOSTE_NO_INFORMATION']['icon'] = 'icon_reclamaposte_unknown.png';
$etat['COLIPOSTE_REMBOURSEMENT_IMPOSSIBLE']['description'] = 'Pas de remboursement possible sur cette livraison';
$etat['COLIPOSTE_REMBOURSEMENT_IMPOSSIBLE']['icon'] = 'icon_reclamaposte_impossible.png';
$etat['COLIPOSTE_RECLAMATION_DEMANDE']['description'] = 'Livraison en retard - R&eacute;clamation effectu&eacute;';
$etat['COLIPOSTE_RECLAMATION_DEMANDE']['icon'] = 'icon_reclamaposte_reclamation.png';
$etat['COLIPOSTE_RECLAMATION_REMBOURSE']['description'] = 'Livraison en retard - Remboursement reçu';
$etat['COLIPOSTE_RECLAMATION_REMBOURSE']['icon'] = 'icon_reclamaposte_cash.png';
$etat['COLIPOSTE_NONE']['description'] = 'Colissimo &agrave; v&eacute;rifier';
$etat['COLIPOSTE_NONE']['icon'] = 'icon_reclamaposte_none.png';
$etat['COLIPOSTE_ARCHIVE']['description'] = 'Coliposte trop ancien et archiv&eacute;';
$etat['COLIPOSTE_ARCHIVE']['icon'] = 'icon_reclamaposte_archive.png';
$etat['COLIPOSTE_ERREUR_MD5']['description'] = 'Status pr&eacute;sent sur site de la poste non reconnu';
$etat['COLIPOSTE_ERREUR_MD5']['icon'] = 'icon_reclamaposte_md5_error.png';

/* A priori ne pas toucher les lignes suivantes */

$status= array();
// Status livré
$status['1f9c469ab21e5b64cbb31f45f01b5c77']['description'] = 'Livr&eacute;';
$status['1f9c469ab21e5b64cbb31f45f01b5c77']['notification_client'] = 1;
$status['1f9c469ab21e5b64cbb31f45f01b5c77']['notification_email_template'] = 1;
$status['1f9c469ab21e5b64cbb31f45f01b5c77']['remboursable'] = 'true';
$status['1f9c469ab21e5b64cbb31f45f01b5c77']['initial_transit'] = 'false';
$status['9cd60418e9cbad03ff699ed6504081c4']['description'] = 'Livr&eacute; Conforme';
$status['9cd60418e9cbad03ff699ed6504081c4']['notification_client'] = 1;
$status['9cd60418e9cbad03ff699ed6504081c4']['notification_email_template'] = 1;
$status['9cd60418e9cbad03ff699ed6504081c4']['remboursable'] = 'true';
$status['9cd60418e9cbad03ff699ed6504081c4']['initial_transit'] = 'false';
$status['01e49e7741634eb5617ebf63f0d40769']['description'] = 'Colis livr&eacute;';
$status['01e49e7741634eb5617ebf63f0d40769']['notification_client'] = 1;
$status['01e49e7741634eb5617ebf63f0d40769']['notification_email_template'] = 1;
$status['01e49e7741634eb5617ebf63f0d40769']['remboursable'] = 'true';
$status['01e49e7741634eb5617ebf63f0d407694']['initial_transit'] = 'false';
$status['d52afb959c5b85603ac401e83dba3ab9']['description'] = 'Votre colis est livr&eacute;';
$status['d52afb959c5b85603ac401e83dba3ab9']['notification_client'] = 1;
$status['d52afb959c5b85603ac401e83dba3ab9']['notification_email_template'] = 1;
$status['d52afb959c5b85603ac401e83dba3ab9']['remboursable'] = 'true';
$status['d52afb959c5b85603ac401e83dba3ab9']['initial_transit'] = 'false';
$status['5c47571b8e4ac17561496386f29b7794']['description'] = 'Votre colis est livr&eacute;';
$status['5c47571b8e4ac17561496386f29b7794']['notification_client'] = 1;
$status['5c47571b8e4ac17561496386f29b7794']['notification_email_template'] = 1;
$status['5c47571b8e4ac17561496386f29b7794']['remboursable'] = 'true';
$status['5c47571b8e4ac17561496386f29b7794']['initial_transit'] = 'false';



// Status présentés
$status['16b4a42d0412966610c5f7d9504a4414']['description'] = 'Colis en instance &agrave; la poste, destinataire avis&eacute; disposant de 15 jours pour aller le retirer';
$status['16b4a42d0412966610c5f7d9504a4414']['notification_client'] = 1;
$status['16b4a42d0412966610c5f7d9504a4414']['notification_email_template'] = 1;
$status['16b4a42d0412966610c5f7d9504a4414']['remboursable'] = 'true';
$status['16b4a42d0412966610c5f7d9504a4414']['initial_transit'] = 'false';
$status['59ead25a6f21d854c4c9499fac033040']['description'] = 'Colis en instance &agrave; la poste, destinataire avis&eacute; disposant de 15 jours pour aller le retirer';
$status['59ead25a6f21d854c4c9499fac033040']['notification_client'] = 1;
$status['59ead25a6f21d854c4c9499fac033040']['notification_email_template'] = 1;
$status['59ead25a6f21d854c4c9499fac033040']['remboursable'] = 'true';
$status['59ead25a6f21d854c4c9499fac033040']['initial_transit'] = 'false';
$status['e4b21bccc77644f76bbdb2b297297e3e']['description'] = 'Destinataire absent lors de la premi&egrave;re livraison, seconde pr&eacute;sentation pr&eacute;vue';
$status['e4b21bccc77644f76bbdb2b297297e3e']['notification_client'] = 1;
$status['e4b21bccc77644f76bbdb2b297297e3e']['notification_email_template'] = 1;
$status['e4b21bccc77644f76bbdb2b297297e3e']['remboursable'] = 'true';
$status['e4b21bccc77644f76bbdb2b297297e3e']['initial_transit'] = 'false';
$status['2785b7f3103de9c8778d9110a91427ef']['description'] = 'Colis &agrave; disposition du destinaire au bureau de poste';
$status['2785b7f3103de9c8778d9110a91427ef']['notification_client'] = 1;
$status['2785b7f3103de9c8778d9110a91427ef']['notification_email_template'] = 1;
$status['2785b7f3103de9c8778d9110a91427ef']['remboursable'] = 'true';
$status['2785b7f3103de9c8778d9110a91427ef']['initial_transit'] = 'false';
$status['2785b7f3103de9c8778d9110a91427ef']['description'] = 'Colis &agrave; disposition du destinataire';
$status['2785b7f3103de9c8778d9110a91427ef']['notification_client'] = 1;
$status['2785b7f3103de9c8778d9110a91427ef']['notification_email_template'] = 1;
$status['2785b7f3103de9c8778d9110a91427ef']['remboursable'] = 'true';
$status['2785b7f3103de9c8778d9110a91427ef']['initial_transit'] = 'false';
$status['347eae829981f03a1036e463103b1eab']['description'] = 'Colis ou accus&eacute; de r&eacute;ception &agrave; signer mis &agrave; disposition dans la boîte postale';
$status['347eae829981f03a1036e463103b1eab']['notification_client'] = 1;
$status['347eae829981f03a1036e463103b1eab']['notification_email_template'] = 1;
$status['347eae829981f03a1036e463103b1eab']['remboursable'] = 'true';
$status['347eae829981f03a1036e463103b1eab']['initial_transit'] = 'false';
$status['2c2482aac5701e5b297ab57a13e4eff0']['description'] = 'Colis &agrave; disposition du destinataire 5 jours dans le cityssimo';
$status['2c2482aac5701e5b297ab57a13e4eff0']['notification_client'] = 1;
$status['2c2482aac5701e5b297ab57a13e4eff0']['notification_email_template'] = 1;
$status['2c2482aac5701e5b297ab57a13e4eff0']['remboursable'] = 'true';
$status['2c2482aac5701e5b297ab57a13e4eff0']['initial_transit'] = 'false';
$status['e9c66a355d98c1e919be3a518d7e1ed6']['description'] = 'Colis r&eacute;expedi&eacute; &agrave; la demande du destinataire vers l\'adresse de son choix';
$status['e9c66a355d98c1e919be3a518d7e1ed6']['notification_client'] = 1;
$status['e9c66a355d98c1e919be3a518d7e1ed6']['notification_email_template'] = 1;
$status['e9c66a355d98c1e919be3a518d7e1ed6']['remboursable'] = 'true';
$status['e9c66a355d98c1e919be3a518d7e1ed6']['initial_transit'] = 'false';
$status['7b239af5fcb9e519b9b5045c00f9b7e7']['description'] = 'Votre colis est disponible au bureau de poste. Votre destinataire, une fois l\'avis d\'instance reçu, dispose de 10 jours ouvrés pour venir retirer le colis sur présentation d\'une pièce d\'identité.';
$status['7b239af5fcb9e519b9b5045c00f9b7e7']['notification_client'] = 1;
$status['7b239af5fcb9e519b9b5045c00f9b7e7']['notification_email_template'] = 1;
$status['7b239af5fcb9e519b9b5045c00f9b7e7']['remboursable'] = 'true';
$status['7b239af5fcb9e519b9b5045c00f9b7e7']['initial_transit'] = 'false';
$status['1939c6ac92bb0aafdee4a7d91a8e68d2']['description'] = 'Destinataire absent lors de la premi&egrave;re livraison. Seconde pr&eacute;sentation pr&eacute;vue';
$status['1939c6ac92bb0aafdee4a7d91a8e68d2']['notification_client'] = 1;
$status['1939c6ac92bb0aafdee4a7d91a8e68d2']['notification_email_template'] = 1;
$status['1939c6ac92bb0aafdee4a7d91a8e68d2']['remboursable'] = 'true';
$status['1939c6ac92bb0aafdee4a7d91a8e68d2']['initial_transit'] = 'false';
$status['50351273836c25e142bcf935d176aaf6']['description'] = 'Le destinataire était absent lors de la livraison. Votre colis sera présenté une nouvelle fois le prochain jour ouvré.';
$status['50351273836c25e142bcf935d176aaf6']['notification_client'] = 1;
$status['50351273836c25e142bcf935d176aaf6']['notification_email_template'] = 1;
$status['50351273836c25e142bcf935d176aaf6']['remboursable'] = 'true';
$status['50351273836c25e142bcf935d176aaf6']['initial_transit'] = 'false'; 
$status['e98240bb48cdbba81cbb92b159972f29']['description'] = 'Le destinataire était absent lors de la livraison. Votre colis sera présenté une nouvelle fois le prochain jour ouvré.';
$status['e98240bb48cdbba81cbb92b159972f29']['notification_client'] = 1;
$status['e98240bb48cdbba81cbb92b159972f29']['notification_email_template'] = 1;
$status['e98240bb48cdbba81cbb92b159972f29']['remboursable'] = 'true';
$status['e98240bb48cdbba81cbb92b159972f29']['initial_transit'] = 'false'; 
$status['ef80d2e921210bc661e9873e0b0df7bb']['description'] = 'Votre colis est disponible dans votre bureau de poste. Le destinataire dispose de 10 jours ouvrés pour retirer son colis sur présentation de son bon de retrait et d\'une pièce d\'identité.';
$status['ef80d2e921210bc661e9873e0b0df7bb']['notification_client'] = 1;
$status['ef80d2e921210bc661e9873e0b0df7bb']['notification_email_template'] = 1;
$status['ef80d2e921210bc661e9873e0b0df7bb']['remboursable'] = 'true';
$status['ef80d2e921210bc661e9873e0b0df7bb']['initial_transit'] = 'false'; 
$status['72e2b31e7335ee02e3fc972e1d278519']['description'] = 'Votre colis est livré au gardien ou à un des voisins.';
$status['72e2b31e7335ee02e3fc972e1d278519']['notification_client'] = 1;
$status['72e2b31e7335ee02e3fc972e1d278519']['notification_email_template'] = 1;
$status['72e2b31e7335ee02e3fc972e1d278519']['remboursable'] = 'true';
$status['72e2b31e7335ee02e3fc972e1d278519']['initial_transit'] = 'false'; 


// Status non remboursable
$status['55869ad6917e3ce8d4984381e1e12cf7']['description'] = 'Adresse compl&eacute;t&eacute;e ou corrig&eacute;e par nos services, colis redirig&eacute; vers le bon site de distribution';
$status['55869ad6917e3ce8d4984381e1e12cf7']['notification_client'] = 1;
$status['55869ad6917e3ce8d4984381e1e12cf7']['notification_email_template'] = 1;
$status['55869ad6917e3ce8d4984381e1e12cf7']['remboursable'] = 'false';
$status['55869ad6917e3ce8d4984381e1e12cf7']['initial_transit'] = 'false';
$status['96123b9900beb4f4678e5f68dd188fd3']['description'] = 'Colis retourn&eacute; &agrave; l\'exp&eacute;diteur car refus&eacute; par le destinataire';
$status['96123b9900beb4f4678e5f68dd188fd3']['notification_client'] = 1;
$status['96123b9900beb4f4678e5f68dd188fd3']['notification_email_template'] = 1;
$status['96123b9900beb4f4678e5f68dd188fd3']['remboursable'] = 'false';
$status['96123b9900beb4f4678e5f68dd188fd3']['initial_transit'] = 'false';
$status['7dbbaaddb321f689f440cc075f8fd762']['description'] = 'Colis retourn&eacute; &agrave; l\'exp&eacute;diteur car le destinataire du colis n\'habite pas &agrave; l\'adresse indiqu&eacute;e';
$status['7dbbaaddb321f689f440cc075f8fd762']['notification_client'] = 1;
$status['7dbbaaddb321f689f440cc075f8fd762']['notification_email_template'] = 1;
$status['7dbbaaddb321f689f440cc075f8fd762']['remboursable'] = 'false';
$status['7dbbaaddb321f689f440cc075f8fd762']['initial_transit'] = 'false';
$status['52a7f30e1efc9308be8672f3eed5f099']['description'] = 'Colis en cours de r&eacute;acheminement vers sont exp&eacute;diteur';
$status['52a7f30e1efc9308be8672f3eed5f099']['notification_client'] = 1;
$status['52a7f30e1efc9308be8672f3eed5f099']['notification_email_template'] = 1;
$status['52a7f30e1efc9308be8672f3eed5f099']['remboursable'] = 'false';
$status['52a7f30e1efc9308be8672f3eed5f099']['initial_transit'] = 'false';
$status['52a7f30e1efc9308be8672f3eed5f099']['description'] = 'Le destinataire du colis n\'habite pas &agrave; l\'adresse indiqu&eacute;e. Le colis est retourn&eacute; à l\'exp&eacute;diteur. ';
$status['52a7f30e1efc9308be8672f3eed5f099']['notification_client'] = 1;
$status['52a7f30e1efc9308be8672f3eed5f099']['notification_email_template'] = 1;
$status['52a7f30e1efc9308be8672f3eed5f099']['remboursable'] = 'false';
$status['52a7f30e1efc9308be8672f3eed5f099']['initial_transit'] = 'false';



// Status en transit
$status['df5635740e757b728e27a34afbb650d3']['description'] = 'Pris en charge de le r&eacute;seau Colissimo';
$status['df5635740e757b728e27a34afbb650d3']['notification_client'] = 1;
$status['df5635740e757b728e27a34afbb650d3']['notification_email_template'] = 1;
$status['df5635740e757b728e27a34afbb650d3']['remboursable'] = 'true';
$status['df5635740e757b728e27a34afbb650d3']['initial_transit'] = 'true';
$status['0c63b90af6406567388adb71ff695557']['description'] = 'Colis pris en charge dans le réseau';
$status['0c63b90af6406567388adb71ff695557']['notification_client'] = 1;
$status['0c63b90af6406567388adb71ff695557']['notification_email_template'] = 1;
$status['0c63b90af6406567388adb71ff695557']['remboursable'] = 'true';
$status['0c63b90af6406567388adb71ff695557']['initial_transit'] = 'true';
$status['1bceede347f1d3c9724cc1ca22646d2d']['description'] = 'Colis  pris en charge dans le réseau';
$status['1bceede347f1d3c9724cc1ca22646d2d']['notification_client'] = 1;
$status['1bceede347f1d3c9724cc1ca22646d2d']['notification_email_template'] = 1;
$status['1bceede347f1d3c9724cc1ca22646d2d']['remboursable'] = 'true';
$status['1bceede347f1d3c9724cc1ca22646d2d']['initial_transit'] = 'true';
$status['875b2a969f61ffa1dac744e5177771bb']['description'] = 'Colis pris en charge dans le réseau';
$status['875b2a969f61ffa1dac744e5177771bb']['notification_client'] = 1;
$status['875b2a969f61ffa1dac744e5177771bb']['notification_email_template'] = 1;
$status['875b2a969f61ffa1dac744e5177771bb']['remboursable'] = 'true';
$status['875b2a969f61ffa1dac744e5177771bb']['initial_transit'] = 'true';
$status['5b1092eb53574ac02768d57a0d4439da']['description'] = 'Trait&eacute; par le site de livraison';
$status['5b1092eb53574ac02768d57a0d4439da']['notification_client'] = 1;
$status['5b1092eb53574ac02768d57a0d4439da']['notification_email_template'] = 1;
$status['5b1092eb53574ac02768d57a0d4439da']['remboursable'] = 'true';
$status['5b1092eb53574ac02768d57a0d4439da']['initial_transit'] = 'true';
$status['c51b33cdba95ea6113435739b8259c82']['description'] = 'Colis arriv&eacute; dans le r&eacute;seau partenaire du pays destinataire';
$status['c51b33cdba95ea6113435739b8259c82']['notification_client'] = 1;
$status['c51b33cdba95ea6113435739b8259c82']['notification_email_template'] = 1;
$status['c51b33cdba95ea6113435739b8259c82']['remboursable'] = 'false';
$status['c51b33cdba95ea6113435739b8259c82']['initial_transit'] = 'true';
$status['92fa28d7867746aef5bec47b3df19baa']['description'] = 'Colis sorti du territoire de d&eacute;part';
$status['92fa28d7867746aef5bec47b3df19baa']['notification_client'] = 1;
$status['92fa28d7867746aef5bec47b3df19baa']['notification_email_template'] = 1;
$status['92fa28d7867746aef5bec47b3df19baa']['remboursable'] = 'false';
$status['92fa28d7867746aef5bec47b3df19baa']['initial_transit'] = 'true';
$status['9e21e939b22305a64b1a2f57fa25dd39']['description'] = 'Colis arriv&eacute; sur site, livraison en pr&eacute;paration';
$status['9e21e939b22305a64b1a2f57fa25dd39']['notification_client'] = 1;
$status['9e21e939b22305a64b1a2f57fa25dd39']['notification_email_template'] = 1;
$status['9e21e939b22305a64b1a2f57fa25dd39']['remboursable'] = 'true';
$status['9e21e939b22305a64b1a2f57fa25dd39']['initial_transit'] = 'true';
$status['aacb86be1581c96c891abd3b58c6ae75']['description'] = 'Colis arriv&eacute; sur site, livraison en pr&eacute;paration';
$status['aacb86be1581c96c891abd3b58c6ae75']['notification_client'] = 1;
$status['aacb86be1581c96c891abd3b58c6ae75']['notification_email_template'] = 1;
$status['aacb86be1581c96c891abd3b58c6ae75']['remboursable'] = 'true';
$status['aacb86be1581c96c891abd3b58c6ae75']['initial_transit'] = 'true';
$status['5b1092eb53574ac02768d57a0d4439da']['description'] = 'Colis trait&eacute; par le site de livraison';
$status['5b1092eb53574ac02768d57a0d4439da']['notification_client'] = 1;
$status['5b1092eb53574ac02768d57a0d4439da']['notification_email_template'] = 1;
$status['5b1092eb53574ac02768d57a0d4439da']['remboursable'] = 'true';
$status['5b1092eb53574ac02768d57a0d4439da']['initial_transit'] = 'true';
$status['4b24a332b2743e942c03ebb27fa10e09']['description'] = 'Colis en instance en douane';
$status['4b24a332b2743e942c03ebb27fa10e09']['notification_client'] = 1;
$status['4b24a332b2743e942c03ebb27fa10e09']['notification_email_template'] = 1;
$status['4b24a332b2743e942c03ebb27fa10e09']['remboursable'] = 'false';
$status['4b24a332b2743e942c03ebb27fa10e09']['initial_transit'] = 'true';
$status['5154c07d974c9c47f78570054f5ae923']['description'] = 'Colis d&eacute;pos&eacute; au bureau de Poste pour acheminement';
$status['5154c07d974c9c47f78570054f5ae923']['notification_client'] = 1;
$status['5154c07d974c9c47f78570054f5ae923']['notification_email_template'] = 1;
$status['5154c07d974c9c47f78570054f5ae923']['remboursable'] = 'true';
$status['5154c07d974c9c47f78570054f5ae923']['initial_transit'] = 'true';
$status['1b957501644bdc6db068c59343c592c6']['description'] = 'Colis d&eacute;pos&eacute; au bureau de Poste pour acheminement';
$status['1b957501644bdc6db068c59343c592c6']['notification_client'] = 1;
$status['1b957501644bdc6db068c59343c592c6']['notification_email_template'] = 1;
$status['1b957501644bdc6db068c59343c592c6']['remboursable'] = 'true';
$status['1b957501644bdc6db068c59343c592c6']['initial_transit'] = 'true';
$status['f5425b49ecb3c62d559d53ee5024a1df']['description'] = 'Colis en cours d\'acheminement dans notre r&eacute;seau';
$status['f5425b49ecb3c62d559d53ee5024a1df']['notification_client'] = 1;
$status['f5425b49ecb3c62d559d53ee5024a1df']['notification_email_template'] = 1;
$status['f5425b49ecb3c62d559d53ee5024a1df']['remboursable'] = 'true';
$status['f5425b49ecb3c62d559d53ee5024a1df']['initial_transit'] = 'true';
$status['3322af623f3fb3b9a19a9387320e4b07']['description'] = 'Colis en cours d\'acheminement dans notre r&eacute;seau';
$status['3322af623f3fb3b9a19a9387320e4b07']['notification_client'] = 1;
$status['3322af623f3fb3b9a19a9387320e4b07']['notification_email_template'] = 1;
$status['3322af623f3fb3b9a19a9387320e4b07']['remboursable'] = 'true';
$status['3322af623f3fb3b9a19a9387320e4b07']['initial_transit'] = 'true';
$status['f7c7f9ea682c979b0dae4b15814d2c1b']['description'] = 'Colis pr&eacute;par&eacute; pour en effectuer la livraison';
$status['f7c7f9ea682c979b0dae4b15814d2c1b']['notification_client'] = 1;
$status['f7c7f9ea682c979b0dae4b15814d2c1b']['notification_email_template'] = 1;
$status['f7c7f9ea682c979b0dae4b15814d2c1b']['remboursable'] = 'true';
$status['f7c7f9ea682c979b0dae4b15814d2c1b']['initial_transit'] = 'true';
$status['bf6056ab2447f9b18fa5d709db858276']['description'] = 'Colis pr&eacute;par&eacute; pour en effectuer la livraison';
$status['bf6056ab2447f9b18fa5d709db858276']['notification_client'] = 1;
$status['bf6056ab2447f9b18fa5d709db858276']['notification_email_template'] = 1;
$status['bf6056ab2447f9b18fa5d709db858276']['remboursable'] = 'true';
$status['bf6056ab2447f9b18fa5d709db858276']['initial_transit'] = 'true';
$status['6d8dc2dbc66bc05698427954949a5594']['description'] = 'Votre colis est pris en charge par La Poste. Il est en cours d\'acheminement.';
$status['6d8dc2dbc66bc05698427954949a5594']['notification_client'] = 1;
$status['6d8dc2dbc66bc05698427954949a5594']['notification_email_template'] = 1;
$status['6d8dc2dbc66bc05698427954949a5594']['remboursable'] = 'true';
$status['6d8dc2dbc66bc05698427954949a5594']['initial_transit'] = 'true';
$status['96ade23a372f91681bdfec4f6a782591']['description'] = 'Votre colis est arriv&eacute; sur son site de distribution';
$status['96ade23a372f91681bdfec4f6a782591']['notification_client'] = 1;
$status['96ade23a372f91681bdfec4f6a782591']['notification_email_template'] = 1;
$status['96ade23a372f91681bdfec4f6a782591']['remboursable'] = 'true';
$status['96ade23a372f91681bdfec4f6a782591']['initial_transit'] = 'true';
$status['09160652504bbe50ef4843c9b9c87992']['description'] = 'Votre colis a été déposé au bureau de poste d\'expedition.';
$status['09160652504bbe50ef4843c9b9c87992']['notification_client'] = 1;
$status['09160652504bbe50ef4843c9b9c87992']['notification_email_template'] = 1;
$status['09160652504bbe50ef4843c9b9c87992']['remboursable'] = 'true';
$status['09160652504bbe50ef4843c9b9c87992']['initial_transit'] = 'true';
$status['cb09443c764d34bfb32b6da565f146af']['description'] = 'Votre colis est arriv&eacute; par erreur sur un site. Il est en cours de r&eacute;acheminement vers son site de distribution.';
$status['cb09443c764d34bfb32b6da565f146af']['notification_client'] = 1;
$status['cb09443c764d34bfb32b6da565f146af']['notification_email_template'] = 1;
$status['cb09443c764d34bfb32b6da565f146af']['remboursable'] = 'true';
$status['cb09443c764d34bfb32b6da565f146af']['initial_transit'] = 'true';

// Status a la CON
$status['42bd4c0093b4bb137b1499c24eff2e3e']['description'] = 'La livraison de votre colis a &eacute;t&eacute; report&eacute;e pour absence du destinataire ou cas de force majeur.';
$status['42bd4c0093b4bb137b1499c24eff2e3e']['notification_client'] = 1;
$status['42bd4c0093b4bb137b1499c24eff2e3e']['notification_email_template'] = 1;
$status['42bd4c0093b4bb137b1499c24eff2e3e']['remboursable'] = 'true';
$status['42bd4c0093b4bb137b1499c24eff2e3e']['initial_transit'] = 'true';


?>
