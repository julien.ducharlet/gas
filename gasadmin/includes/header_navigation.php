<?php
/*
  $Id: header_navigation.php
*/
  $menu_dhtml = MENU_DHTML;
  
  $box_files_list = array();
  

  if (tep_admin_check_boxes('administrator.php') == true) {
      	$box_files_list[] = array("administrator", "administrator.php", 'Gestion', "admin_files.php");
  }
  if (tep_admin_check_boxes('configuration.php') == true) {
		$box_files_list[] = array("configuration", "configuration.php", 'Config.', "configuration.php?gID=1");
  }
  if (tep_admin_check_boxes('catalog.php') == true) {
		$box_files_list[] = array("catalog", "catalog.php", 'Les Articles', "categorie_list.php");
  }
	if (tep_admin_check_boxes('stock.php') == true) {
		$box_files_list[] = array("stock", "stock.php", 'Les Stocks / Forrnisseurs', "gestion_fournisseurs.php");
  }
	if (tep_admin_check_boxes('customers.php') == true) {
		$box_files_list[] = array("customers", "customers.php" , 'Les Clients', "client_list.php");
  }
  if (tep_admin_check_boxes('orders.php') == true) {
		$box_files_list[] = array("orders", "orders.php" , 'Les Commandes', "cmd_list.php");
  }
  if (tep_admin_check_boxes('reports.php') == true) {
		 $box_files_list[] = array("reports", "reports.php" , 'Stats', "margin_report.php");
  }	
  if (tep_admin_check_boxes('tools.php') == true) {
		$box_files_list[] = array("tools", "tools.php" , 'Les Outils', "whos_online.php");
  }
  if (tep_admin_check_boxes('referencement.php') == true) {
		$box_files_list[] = array("referencement", "referencement.php" , 'Référencements', "googlesitemap.php");
  }
  if (tep_admin_check_boxes('comptabilite.php') == true) {
		$box_files_list[] = array("comptabilite", "comptabilite.php" , 'Compta', "comptabilite_accueil.php");
  }
	if (tep_admin_check_boxes('newsletter.php') == true) {
		$box_files_list[] = array("newsletter", "newsletter.php" , 'Newsletter', "newsletter_list.php");
  }
	if (tep_admin_check_boxes('gv_admin.php') == true) {
		$box_files_list[] = array("gv_admin", "gv_admin.php" , 'Chèques Cadeaux', "coupon_admin.php");
  }
	if (tep_admin_check_boxes('faq.php') == true) {
		$box_files_list[] = array("faq", "faq.php" , 'FAQ', "faq_family.php");
  }
  	if (tep_admin_check_boxes('cron.php') == true) {
		$box_files_list[] = array("cron", "cron.php", "Cron", "cron.php");
  }
  	if (tep_admin_check_boxes('update.php') == true) {
		$box_files_list[] = array("update", "update.php", "Update", "update.php");
  }

   echo '<!-- Menu bar #2. --> <div class="menuBar" style="width:100%;">';
   
   foreach($box_files_list as $item_menu) {
     
     echo "<a class=\"menuButton\" href=\"".$item_menu[3]."\" onmouseover=\"buttonMouseover(event, '".$item_menu[0]."Menu');\">".$item_menu[2]."</a> | " ;
   }
   echo "</div>";
foreach($box_files_list as $item_menu) require(DIR_WS_BOXES. $item_menu[1] );


?>