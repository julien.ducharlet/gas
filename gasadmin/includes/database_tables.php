<?php
/*
  $Id: database_tables.php,v 1.1 2003/06/20 00:18:30 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License 
*/

// ###### Added CCGV Contribution #########

define('TABLE_COUPON_GV_QUEUE', 'coupon_gv_queue');
define('TABLE_COUPON_GV_CUSTOMER', 'coupon_gv_customer');
define('TABLE_COUPON_EMAIL_TRACK', 'coupon_email_track');
define('TABLE_COUPON_REDEEM_TRACK', 'coupon_redeem_track');
define('TABLE_COUPONS', 'coupons');
define('TABLE_COUPONS_DESCRIPTION', 'coupons_description');

// ###### end CCGV Contribution #########

//Admin begin
  define('TABLE_ADMIN', 'admin');
  define('TABLE_ADMIN_FILES', 'admin_files');
  define('TABLE_ADMIN_GROUPS', 'admin_groups');
//Admin end

// define the database table names used in the project
  define('TABLE_ADDRESS_BOOK', 'address_book');
  define('TABLE_ADDRESS_FORMAT', 'address_format');
  define('TABLE_BANNERS', 'banners');
  define('TABLE_BANNERS_HISTORY', 'banners_history');
  define('TABLE_CATEGORIES', 'categories');
  define('TABLE_CATEGORIES_DESCRIPTION', 'categories_description');
  define('TABLE_CATEGORIES_RAYON', 'categories_rubrique');
  
  define('TABLE_CONFIGURATION', 'configuration');
  define('TABLE_CONFIGURATION_GROUP', 'configuration_group');
  
  define('TABLE_COUNTER','counter');
  define('TABLE_COUNTRIES', 'countries');
  define('TABLE_CURRENCIES', 'currencies');
  
  define('TABLE_CUSTOMERS', 'customers');
  define('TABLE_CUSTOMERS_BASKET', 'customers_basket');
  define('TABLE_CUSTOMERS_BASKET_ATTRIBUTES', 'customers_basket_attributes');
  define('TABLE_CUSTOMERS_INFO', 'customers_info');
  define('TABLE_CUSTOMERS_ARGENT', 'customer_argent_virtuel');
  define('TABLE_CUSTOMERS_TYPE', 'customers_type');
  
  define('TABLE_FAMILY', 'family');
  define('TABLE_FAMILY_INFO', 'family_info');
  define('TABLE_CLIENT_FAMILY', 'family_client');
  define('TABLE_CLIENT_FAMILY_INFO', 'family_client_info');
	
  define('TABLE_INVOICE_COUNTER', 'invoice_counter');
  define('TABLE_LANGUAGES', 'languages');
  define('TABLE_MANUFACTURERS', 'manufacturers');
  define('TABLE_MANUFACTURERS_INFO', 'manufacturers_info');
  define('TABLE_MESSAGE_SMS', 'message_sms');
  define('TABLE_NEWSLETTERS', 'newsletters');
  
  define('TABLE_ORDERS', 'orders');
  define('TABLE_ORDERS_PRODUCTS', 'orders_products');
  define('TABLE_ORDERS_PRODUCTS_ATTRIBUTES', 'orders_products_attributes');
  define('TABLE_ORDERS_PRODUCTS_DOWNLOAD', 'orders_products_download');
  define('TABLE_ORDERS_STATUS', 'orders_status');
  define('TABLE_ORDERS_STATUS_HISTORY', 'orders_status_history');
  define('TABLE_ORDERS_TOTAL', 'orders_total');
  
  define('TABLE_PACK', 'products_packs');
  define('TABLE_PRODUCTS', 'products');
  define('TABLE_PRODUCTS_ATTRIBUTES', 'products_attributes');
  define('TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD', 'products_attributes_download');
  define('TABLE_PRODUCTS_DESCRIPTION', 'products_description');
  define('TABLE_PRODUCTS_IMAGE', 'products_image');//LL multi image
  define('TABLE_PRODUCTS_LOGOS', 'products_logos');
  define('TABLE_PRODUCTS_NOTIFICATIONS', 'products_notifications');
  define('TABLE_PRODUCTS_OPTIONS', 'products_options');
  define('TABLE_PRODUCTS_OPTIONS_VALUES', 'products_options_values');
  define('TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS', 'products_options_values_to_products_options');
  define('TABLE_PRODUCTS_TO_CATEGORIES', 'products_to_categories');
  define('TABLE_PRODUCTS_TO_LOGOS', 'products_to_logos');
  define('TABLE_RAYON', 'rubrique');
  define('TABLE_REVIEWS', 'reviews');
  define('TABLE_REVIEWS_DESCRIPTION', 'reviews_description');
  define('TABLE_SESSIONS', 'sessions');
  define('TABLE_SPECIALS', 'specials');
  define('TABLE_SCART', 'scart');
  define('TABLE_TAX_CLASS', 'tax_class');
  define('TABLE_TAX_RATES', 'tax_rates');
  define('TABLE_GEO_ZONES', 'geo_zones');
  define('TABLE_ZONES_TO_GEO_ZONES', 'zones_to_geo_zones');
  define('TABLE_WHOS_ONLINE', 'whos_online');
  define('TABLE_ZONES', 'zones');
  define('TABLE_INFOS_LIVRAISON', 'infos_livraison');
  define('TABLE_MODULE_LIVRAISON', 'module_livraison');
  define('ORDERS_EXPEDITOR', 'orders_expeditor');
  define('TABLE_VISITE', 'visite');
  define('TABLE_IP_TO_COUNTRY', 'ip-to-country');
  define('TABLE_PARRAINAGE', 'parrainage');
  
  // Table de stockage des recherches clients modification polytech
  define('TABLE_RECHERCHE', 'recherche');
	
	
	// FAQ SYSTEM 2.1
  define('TABLE_FAQ', 'faq');
  define('TABLE_FAQ_FAMILY', 'faq_family');
// FAQ SYSTEM 2.1
  
  //kgt - discount coupons
  define('TABLE_DISCOUNT_COUPONS', 'discount_coupons');
  define('TABLE_DISCOUNT_COUPONS_TO_ORDERS', 'discount_coupons_to_orders');
  define('TABLE_DISCOUNT_COUPONS_TO_CATEGORIES', 'discount_coupons_to_categories');
  define('TABLE_DISCOUNT_COUPONS_TO_PRODUCTS', 'discount_coupons_to_products');
  define('TABLE_DISCOUNT_COUPONS_TO_MANUFACTURERS', 'discount_coupons_to_manufacturers');
  define('TABLE_DISCOUNT_COUPONS_TO_CUSTOMERS', 'discount_coupons_to_customers');
  define('TABLE_DISCOUNT_COUPONS_TO_ZONES', 'discount_coupons_to_zones');
  //end kgt - discount coupons
  
// modules paiement livraison
  define('TABLE_MODULE', 'modules');
 // define('TABLE_MODULE_LIVRAISON', 'module_livraison');
  define('TABLE_MODULE_PAIEMENT', 'module_paiement');
// modules paiement livraison

//newsletter
  define('TABLE_NEWSLETTER_INFOS', 'newsletter_infos');
  define('TABLE_NEWSLETTER_PRODUCTS', 'newsletter_products');
//newsletter

// Remises quantitatives
  define('TABLE_PRODUCTS_PRICE_BY_QUANTITY', 'products_price_by_quantity');
// Remises quantitatives

// Emails migration lus
  define('TABLE_EMAIL_MIGRATION_LU', 'email_migration_lu');
// Emails migration lus

// Ventes flash
  define('TABLE_PRODUCTS_VENTES_FLASH', 'products_ventes_flash');
// Ventes flash
?>