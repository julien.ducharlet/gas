<?php
/*
  Fonction : pays_iso_spplus.php
*/

// permet d'afficher l'état du paiement  
	function spplus_code_etat($etat_spplus_paiement) {
		switch ($etat_spplus_paiement) {
			case '0': $etat_spplus='PAS ENCORE DE PAIEMENT CB';
				break;
			case '1': $etat_spplus='Autorisation de paiement ACCEPTEE';
				break;
			case '2': $etat_spplus='Autorisation de paiement REFUSEE';
				break;
			case '4': $etat_spplus='Ech&eacute;ance de paiement accept&eacute;e, EN ATTENTE DE REMISE';
				break;
			case '5': $etat_spplus='Ech&eacute;ance de paiement REFUSEE';
				break;
			case '6': $etat_spplus='Paiement par ch&egrave;que ACCEPTE';
				break;
			case '8': $etat_spplus='Ch&egrave;que ENCAISSE';
				break;
			case '10': $etat_spplus='Paiement TERMINE';
				break;
			case '11': $etat_spplus='Ech&eacute;ance du paiement ANNULEE PAR BOUTIQUE';
				break;
			case '12': $etat_spplus='ABANDON DU CLIENT';
				break;
			case '15': $etat_spplus='Remboursement ENREGISTRE';
				break;
			case '16': $etat_spplus='Remboursement ANNULE';
				break;
			case '17': $etat_spplus='Remboursement ACCEPTE';
				break;
			case '20': $etat_spplus='Ech&eacute;ance du paiement AVEC UN IMPAYE';
				break;
			case '21': $etat_spplus='Ech&eacute;ance du paiement AVEC UN IMPAYE et ATTENTE SPPLUS';
				break;
			case '30': $etat_spplus='Ech&eacute;ance de paiement remis&eacute;e';
				break;
			case '99': $etat_spplus='PAIEMENT DE TEST';
				break;
			
			
								 
		}
			return $etat_spplus;
		}