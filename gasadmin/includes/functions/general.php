<?php
/*
  $Id: general.php,v 1.160 2003/07/12 08:32:47 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
// Permet dans la fiche article de l'admin de remplacer les mois en Anglais par du Francais.
	function mois_francais($mois) {
		switch ($mois) {
			case '01': $mois_anglais_francais='Janvier';
				break;
			case '02': $mois_anglais_francais='Fevrier';
				break;
			case '03': $mois_anglais_francais='Mars';
				break;
			case '04': $mois_anglais_francais='Avril';
				break;
			case '05': $mois_anglais_francais='Mai';
				break;
			case '06': $mois_anglais_francais='Juin';
				break;
			case '07': $mois_anglais_francais='Juillet';
				break;
			case '08': $mois_anglais_francais='Aout';
				break;
			case '09': $mois_anglais_francais='Septembre';
				break;
			case '10': $mois_anglais_francais='Octobre';
				break;
			case '11': $mois_anglais_francais='Novembre';
				break;
			case '12': $mois_anglais_francais='Decembre';
				break;
								 
		}
			return $mois_anglais_francais;
		}
		
		
//Admin begin
////
//date_ajd
	function date_ajd()
		{
			$date=date("D j M Y");
			
			$date=str_replace('Mon','Lundi',$date);
			$date=str_replace('Tue','Mardi',$date);
			$date=str_replace('Wed','Mercredi',$date);
			$date=str_replace('Thu','Jeudi',$date);
			$date=str_replace('Fri','Vendredi',$date);
			$date=str_replace('Sat','Samedi',$date);
			$date=str_replace('Sun','Dimanche',$date);
			
			$date=str_replace('Jan','Janvier',$date);
			$date=str_replace('Feb','F&eacute;vrier',$date);
			$date=str_replace('Mar','Mars',$date);
			$date=str_replace('Apr','Avril',$date);
			$date=str_replace('May','Mai',$date);
			$date=str_replace('Jun','Juin',$date);
			$date=str_replace('Jul','Juillet',$date);
			$date=str_replace('Aug','Ao&ucirc;t',$date);
			$date=str_replace('Sep','Septembre',$date);
			$date=str_replace('Oct','Octobre',$date);
			$date=str_replace('Nov','Novembre',$date);
			$date=str_replace('Dec','D&eacute;cembre',$date);
			
			return $date;
		}

	//apercu de texte
	function apercu($texte,$longueur)
		{
			if(strlen($texte)<=$longueur)	{ return $texte; }
			
			$result=substr($texte,0,$longueur);
			$result.=' ...';
			$result=htmlspecialchars($result);
			return $result;
		}


####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################


	//fonction pour creer la newsletter
	function make_newsletter($news_id,$news_titre,$news_date,$news_texte,
							 $p1_nom,$p1_lien,$p1_image,$p1_prix,
							 $p2_nom,$p2_lien,$p2_image,$p2_prix,
							 $p3_nom,$p3_lien,$p3_image,$p3_prix,$id_client='',$pass='')
		{
			$contenu='';		
			$news_file=fopen('../newsletter/template/template.html','r');
			while($buff=fread($news_file,70))
				{
					$contenu.=$buff;		
				}			

			$contenu=str_replace('[[NEWS_ID]]',$news_id,$contenu);
			$contenu=str_replace('[[NEWS_TITRE]]',$news_titre,$contenu);
			$date=explode('-',substr($news_date,0,10));
			
			//$date[2].'/'.$date[1].'/'.$date[0]
			$contenu=str_replace('[[NEWS_DATE]]','Le '.date_ajd(date("D j M Y",mktime(0,0,0,$date[1],$date[2],$date[0]))),$contenu);
			$contenu=str_replace('[[NEWS_TEXTE]]',$news_texte,$contenu);
			$contenu=str_replace('[[P1_NOM]]',$p1_nom,$contenu);
			$contenu=str_replace('[[P1_LIEN]]',str_replace('&','&amp;',$p1_lien),$contenu);
			$contenu=str_replace('[[P1_IMAGE]]',$p1_image,$contenu);
			$contenu=str_replace('[[P1_PRIX]]',imgprixsmall($p1_prix),$contenu);
			$contenu=str_replace('[[P2_NOM]]',$p2_nom,$contenu);
			$contenu=str_replace('[[P2_LIEN]]',str_replace('&','&amp;',$p2_lien),$contenu);
			$contenu=str_replace('[[P2_IMAGE]]',$p2_image,$contenu);
			$contenu=str_replace('[[P2_PRIX]]',imgprixsmall($p2_prix),$contenu);
			$contenu=str_replace('[[P3_NOM]]',$p3_nom,$contenu);
			$contenu=str_replace('[[P3_LIEN]]',str_replace('&','&amp;',$p3_lien),$contenu);
			$contenu=str_replace('[[P3_IMAGE]]',$p3_image,$contenu);
			$contenu=str_replace('[[P3_PRIX]]',imgprixsmall($p3_prix),$contenu);
			
			$contenu=str_replace('[[ID_CLIENT]]',$id_client,$contenu);
			$contenu=str_replace('[[PASS_CLIENT]]',$pass,$contenu);
			
			return $contenu;
		}
		
	
	//imgprixsmall
	function imgprixsmall($prix)
		{
			$return='';
			$prix=sprintf('%.2f',$prix);
			for($i=0;$i<=strlen($prix)-1;$i++)
				{
					switch($prix[$i])
						{
							case '0':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_0.jpg" align="bottom" alt="0" height="20" width="12" />';break;
							case '1':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_1.jpg" align="bottom" alt="1" height="20" width="10" />';break;
							case '2':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_2.jpg" align="bottom" alt="2" height="20" width="11" />';break;
							case '3':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_3.jpg" align="bottom" alt="3" height="20" width="12" />';break;
							case '4':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_4.jpg" align="bottom" alt="4" height="20" width="13" />';break;
							case '5':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_5.jpg" align="bottom" alt="5" height="20" width="13" />';break;
							case '6':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_6.jpg" align="bottom" alt="6" height="20" width="13" />';break;
							case '7':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_7.jpg" align="bottom" alt="7" height="20" width="10" />';break;
							case '8':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_8.jpg" align="bottom" alt="8" height="20" width="13" />';break;
							case '9':$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_9.jpg" align="bottom" alt="9" height="20" width="13" />';break;
							default:break;
						}
					if ($prix[$i]=='.') { $return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_V.jpg" align="absbottom" height="20" width="6" />'; }
				}
			$return.= '<img src="https://www.mobile-shop.fr/images/prix/small3/prix_E.jpg" align="absbottom" height="20" width="14" />';
			return($return);
		}


####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################





//Check login and file access
function tep_admin_check_login() {
  global $PHP_SELF, $login_groups_id;
  if (!tep_session_is_registered('login_id')) {
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  } else {
    $filename = basename( $PHP_SELF );
    if ($filename != FILENAME_DEFAULT && $filename != FILENAME_FORBIDEN && $filename != FILENAME_LOGOFF && $filename != FILENAME_ADMIN_ACCOUNT && $filename != FILENAME_POPUP_IMAGE) {
      $db_file_query = tep_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_name = '" . $filename . "'");
      if (!tep_db_num_rows($db_file_query)) {
        tep_redirect(tep_href_link(FILENAME_FORBIDEN));
      }
    }
  }  
}

////
//Return 'true' or 'false' value to display boxes and files in index.php and column_left.php
function tep_admin_check_boxes($filename, $boxes='') {
  global $login_groups_id;
  
  $is_boxes = 1;
  if ($boxes == 'sub_boxes') {
    $is_boxes = 0;
  }
  $dbquery = tep_db_query("select admin_files_id from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_is_boxes = '" . $is_boxes . "' and admin_files_name = '" . $filename . "'");
  
  $return_value = false;
  if (tep_db_num_rows($dbquery)) {
    $return_value = true;
  }
  return $return_value;
}

////
//Return files stored in box that can be accessed by user
function tep_admin_files_boxes($filename, $sub_box_name) {
  global $login_groups_id;
  $sub_boxes = '';
  
  $dbquery = tep_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_is_boxes = '0' and admin_files_name = '" . $filename . "'");
  if (tep_db_num_rows($dbquery)) {
    $sub_boxes = '<a href="' . tep_href_link($filename) . '" class="menuBoxContentLink">' . $sub_box_name . '</a><br>';
  }
  return $sub_boxes;
}

////
//Get selected file for index.php
function tep_selected_file($filename) {
  global $login_groups_id;
  $randomize = FILENAME_ADMIN_ACCOUNT;
  
  $dbquery = tep_db_query("select admin_files_id as boxes_id from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_is_boxes = '1' and admin_files_name = '" . $filename . "'");
  if (tep_db_num_rows($dbquery)) {
    $boxes_id = tep_db_fetch_array($dbquery);
    $randomize_query = tep_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_is_boxes = '0' and admin_files_to_boxes = '" . $boxes_id['boxes_id'] . "'");
    if (tep_db_num_rows($randomize_query)) {
      $file_selected = tep_db_fetch_array($randomize_query);
      $randomize = $file_selected['admin_files_name'];
    }
  }
  return $randomize;
}
//Admin end

////
// Redirect to another page or site
  function tep_redirect($url) {
    global $logger;

    header('Location: ' . $url);

    if (STORE_PAGE_PARSE_TIME == 'true') {
      if (!is_object($logger)) $logger = new logger;
      $logger->timer_stop();
    }

    exit;
  }

////
// Parse the data used in the html tags to ensure the tags will not break
  function tep_parse_input_field_data($data, $parse) {
    return strtr(trim($data), $parse);
  }

  function tep_output_string($string, $translate = false, $protected = false) {
    if ($protected == true) {
      return htmlspecialchars($string);
    } else {
      if ($translate == false) {
        return tep_parse_input_field_data($string, array('"' => '&quot;'));
      } else {
        return tep_parse_input_field_data($string, $translate);
      }
    }
  }

  function tep_output_string_protected($string) {
    return tep_output_string($string, false, true);
  }

  function tep_sanitize_string($string) {
    $string = ereg_replace(' +', ' ', $string);

    return preg_replace("/[<>]/", '_', $string);
  }

  function tep_customers_name($customers_id) {
    $customers = tep_db_query("select customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customers_id . "'");
    $customers_values = tep_db_fetch_array($customers);

    return $customers_values['customers_firstname'] . ' ' . $customers_values['customers_lastname'];
  }

  function tep_get_path($current_category_id = '') {
    global $cPath_array;

    if ($current_category_id == '') {
      
	  $cPath_new = implode('_', $cPath_array);
    }
	elseif (sizeof($cPath_array) == 0) {
        
		$cPath_new = $current_category_id;
	}
	else {
		
        $cPath_new = '';
        $last_category_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$cPath_array[(sizeof($cPath_array)-1)] . "'");
        $last_category = tep_db_fetch_array($last_category_query);

        $current_category_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$current_category_id . "'");
        $current_category = tep_db_fetch_array($current_category_query);

        if ($last_category['parent_id'] == $current_category['parent_id']) {
          
		  for ($i = 0, $n = sizeof($cPath_array) - 1; $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        }
		else {
			
          for ($i = 0, $n = sizeof($cPath_array); $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        }

        $cPath_new .= '_' . $current_category_id;

        if (substr($cPath_new, 0, 1) == '_') {
          $cPath_new = substr($cPath_new, 1);
        }
    }

    return 'cPath=' . $cPath_new;
  }

  function tep_get_all_get_params($exclude_array = '') {
    global $HTTP_GET_VARS;

    if ($exclude_array == '') $exclude_array = array();

    $get_url = '';

    reset($HTTP_GET_VARS);
    while (list($key, $value) = each($HTTP_GET_VARS)) {
      if (($key != tep_session_name()) && ($key != 'error') && (!in_array($key, $exclude_array))) $get_url .= $key . '=' . $value . '&';
    }

    return $get_url;
  }

  function tep_date_long($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || ($raw_date == '') ) return false;

    $year = (int)substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    return strftime(DATE_FORMAT_LONG, mktime($hour, $minute, $second, $month, $day, $year));
  }

////
// Output a raw date string in the selected locale date format
// $raw_date needs to be in this format: YYYY-MM-DD HH:MM:SS
// NOTE: Includes a workaround for dates before 01/01/1970 that fail on windows servers
  function tep_date_short($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || ($raw_date == '') ) return false;

    $year = substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
      return date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
      return ereg_replace('2037' . '$', $year, date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, 2037)));
    }

  }
  
function date_fr($date){
  
	$tab=array('Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre');
	$d = $date;
	list($an, $mois, $jour) = split('-', $d);
	list($j,$h) = split(' ', $jour);
	
	$ret= $j." ".$tab[$mois-1]." ".$an;
	
	if((int)$ret==0) $ret = '';
	
	return $ret;
}
	
  function tep_datetime_short($raw_datetime) {
    if ( ($raw_datetime == '0000-00-00 00:00:00') || ($raw_datetime == '') ) return false;

    $year = (int)substr($raw_datetime, 0, 4);
    $month = (int)substr($raw_datetime, 5, 2);
    $day = (int)substr($raw_datetime, 8, 2);
    $hour = (int)substr($raw_datetime, 11, 2);
    $minute = (int)substr($raw_datetime, 14, 2);
    $second = (int)substr($raw_datetime, 17, 2);

    return strftime(DATE_TIME_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
  }

  function tep_get_category_tree($parent_id = '0', $spacing = '', $exclude = '', $category_tree_array = '', $include_itself = false) {
    global $languages_id;

    if (!is_array($category_tree_array)) $category_tree_array = array();
    if ( (sizeof($category_tree_array) < 1) && ($exclude != '0') ) $category_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    if ($include_itself) {
      $category_query = tep_db_query("select cd.categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " cd where cd.language_id = '" . (int)$languages_id . "' and cd.categories_id = '" . (int)$parent_id . "'");
      $category = tep_db_fetch_array($category_query);
      $category_tree_array[] = array('id' => $parent_id, 'text' => $category['categories_name']);
    }

    $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "' and c.parent_id = '" . (int)$parent_id . "' order by c.sort_order, cd.categories_name");
    while ($categories = tep_db_fetch_array($categories_query)) {
      if ($exclude != $categories['categories_id']) $category_tree_array[] = array('id' => $categories['categories_id'], 'text' => $spacing . $categories['categories_name']);
      //$category_tree_array = tep_get_category_tree($categories['categories_id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $category_tree_array);
    }

    return $category_tree_array;
  }
  
  function tep_get_full_category_tree($parent_id = '0', $spacing = '', $exclude = '', $category_tree_array = '', $include_itself = false) {
    global $languages_id;

    if (!is_array($category_tree_array)) $category_tree_array = array();
    if ( (sizeof($category_tree_array) < 1) && ($exclude != '0') ) $category_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    if ($include_itself) {
		
      $category_query = tep_db_query("select cd.categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " cd where cd.language_id = '" . (int)$languages_id . "' and cd.categories_id = '". (int)$parent_id ."'");
      $category = tep_db_fetch_array($category_query);
      $category_tree_array[] = array('id' => $parent_id, 'text' => $category['categories_name']);
    }

    $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "' and c.parent_id = '" . (int)$parent_id . "' order by c.sort_order, cd.categories_name");
	
    while ($categories = tep_db_fetch_array($categories_query)) {
		
      if ($exclude != $categories['categories_id']) $category_tree_array[] = array('id' => $categories['categories_id'], 'text' => $spacing . $categories['categories_name']);
      $category_tree_array = tep_get_category_tree($categories['categories_id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $category_tree_array);
    }

    return $category_tree_array;
  }

  function tep_draw_products_pull_down($name, $parameters = '', $exclude = '') {
    global $currencies, $languages_id;

    if ($exclude == '') {
      $exclude = array();
    }

    $select_string = '<select name="' . $name . '"';

    if ($parameters) {
      $select_string .= ' ' . $parameters;
    }

    $select_string .= '>';

    $products_query = tep_db_query("select p.products_id, pd.products_name, p.products_price from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' order by products_name");
    while ($products = tep_db_fetch_array($products_query)) {
      if (!in_array($products['products_id'], $exclude)) {
        $select_string .= '<option value="' . $products['products_id'] . '">' . $products['products_name'] . ' (' . $currencies->format($products['products_price']) . ')</option>';
      }
    }

    $select_string .= '</select>';

    return $select_string;
  }


  function tep_draw_products_specials_pull_down($name, $parameters = '', $exclude = '') {
    global $currencies, $languages_id;

  
    $select_string = '<select id="manufacturers_id" name="manufacturers_id" 
	onChange="update_products(\'manufacturers_id\',\''.$name.'\')">';
	
	
	$manufacturers_query = tep_db_query("select m.manufacturers_id,m.manufacturers_name 
	from " . TABLE_MANUFACTURERS . " m order by manufacturers_name asc");
	
	$select_string .= '<option value="-1">Choississez un fabricant</option>';
	$select_string .= '<option value="">Tous les fabricants</option>';
	while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
		  $select_string .= '<option value="'.$manufacturers['manufacturers_id'].'">
		  '.$manufacturers['manufacturers_name'].'</option>';
	}
	$select_string .= '</select>';
	
 	if ($exclude == '') $exclude = array();
    return $select_string;
  }

// FONCTION POUR AJAX DANS SPECIALS (PROMOTION) THIERRY

	function tep_draw_products_specials_pull_down_specials($name, $parameters = '', $exclude = '') {
    global $currencies, $languages_id;
  
    $select_string = '<select id="family_id" name="family_id" 
	onChange="update_products(\'family_id\',\''.$name.'\')">';
	
	$family_query = tep_db_query("select f.family_id, f.family_name 
	from " . TABLE_FAMILY . " f order by family_name asc");
	
	$select_string .= '<option value="-1">Choississez une Famille</option>';
	$select_string .= '<option value="">Toutes les Familles</option>';
	while ($family = tep_db_fetch_array($family_query)) {
		  $select_string .= '<option value="'.$family['family_id'].'">
		  '.$family['family_name'].'</option>';
	}
	$select_string .= '</select>';
	
 	if ($exclude == '') $exclude = array();
    return $select_string;
  }

// FIN FONCTION POUR AJAX DANS SPECIALS (PROMOTION) THIERRY

// FONCTION POUR AJAX DANS DUPLICATE THIERRY

	function tep_draw_products_specials_pull_down_duplicate($name, $parameters = '', $exclude = '') {
    global $currencies, $languages_id;
  
    $select_string = '<select id="manufacturers_id" name="manufacturers_id" 
	onChange="update_products(\'manufacturers_id\',\''.$name.'\')">';
	
	$manufacturers_query = tep_db_query("select m.manufacturers_id, m.manufacturers_name 
	from " . TABLE_MANUFACTURERS . " m order by manufacturers_name asc");
	
	$select_string .= '<option value="-1">Choississez une Marque</option>';
	$select_string .= '<option value="">Toutes les Marques</option>';
	while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
		  $select_string .= '<option value="'.$manufacturers['manufacturers_id'].'">
		  '.$manufacturers['manufacturers_name'].'</option>';
	}
	$select_string .= '</select>';
	
 	if ($exclude == '') $exclude = array();
    return $select_string;
  }

// FIN FONCTION POUR AJAX DANS DUPLICATE THIERRY


  function tep_options_name($options_id) {
    global $languages_id;

    $options = tep_db_query("select products_options_name from " . TABLE_PRODUCTS_OPTIONS . " where products_options_id = '" . (int)$options_id . "' and language_id = '" . (int)$languages_id . "'");
    $options_values = tep_db_fetch_array($options);

    return $options_values['products_options_name'];
  }

  function tep_values_name($values_id) {
    global $languages_id;

    $values = tep_db_query("select products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " where products_options_values_id = '" . (int)$values_id . "' and language_id = '" . (int)$languages_id . "'");
    $values_values = tep_db_fetch_array($values);

    return $values_values['products_options_values_name'];
  }

  function tep_info_image($image, $alt, $width = '', $height = '') {
    if (tep_not_null($image) && (file_exists(DIR_FS_CATALOG_IMAGES . $image)) ) {
      $image = tep_image(DIR_WS_CATALOG_IMAGES . $image, $alt, $width, $height);
    } else {
      $image = 'L\'IMAGE N\'EXISTE PAS';
    } 

    return $image;
  }
   
  function tep_info_image_manufacturers($image, $alt, $width = '', $height = '') {
    if (tep_not_null($image) && (file_exists(DIR_FS_CATALOG_IMAGES_MARQUES . $image)) ) {
      $image = tep_image(DIR_WS_CATALOG_IMAGES_MARQUES . $image, $alt, $width, $height);
    } else {
      $image = 'L\'IMAGE N\'EXISTE PAS';
    }

    return $image;
  }

  function tep_break_string($string, $len, $break_char = '-') {
    $l = 0;
    $output = '';
    for ($i=0, $n=strlen($string); $i<$n; $i++) {
      $char = substr($string, $i, 1);
      if ($char != ' ') {
        $l++;
      } else {
        $l = 0;
      }
      if ($l > $len) {
        $l = 1;
        $output .= $break_char;
      }
      $output .= $char;
    }

    return $output;
  }

  function tep_get_country_name($country_id) {
    $country_query = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$country_id . "'");

    if (!tep_db_num_rows($country_query)) {
      return $country_id;
    } else {
      $country = tep_db_fetch_array($country_query);
      return $country['countries_name'];
    }
  }

  function tep_get_zone_name($country_id, $zone_id, $default_zone) {
    $zone_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' and zone_id = '" . (int)$zone_id . "'");
    if (tep_db_num_rows($zone_query)) {
      $zone = tep_db_fetch_array($zone_query);
      return $zone['zone_name'];
    } else {
      return $default_zone;
    }
  }

  function tep_not_null($value) {
    if (is_array($value)) {
      if (sizeof($value) > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      if ( (is_string($value) || is_int($value)) && ($value != '') && ($value != 'NULL') && (strlen(trim($value)) > 0)) {
        return true;
      } else {
        return false;
      }
    }
  }

  function tep_browser_detect($component) {
    global $HTTP_USER_AGENT;

    return stristr($HTTP_USER_AGENT, $component);
  }

  function tep_tax_classes_pull_down($parameters, $selected = '') {
    $select_string = '<select ' . $parameters . '>';
    $classes_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
    while ($classes = tep_db_fetch_array($classes_query)) {
      $select_string .= '<option value="' . $classes['tax_class_id'] . '"';
      if ($selected == $classes['tax_class_id']) $select_string .= ' SELECTED';
      $select_string .= '>' . $classes['tax_class_title'] . '</option>';
    }
    $select_string .= '</select>';

    return $select_string;
  }

  function tep_geo_zones_pull_down($parameters, $selected = '') {
    $select_string = '<select ' . $parameters . '>';
    $zones_query = tep_db_query("select geo_zone_id, geo_zone_name from " . TABLE_GEO_ZONES . " order by geo_zone_name");
    while ($zones = tep_db_fetch_array($zones_query)) {
      $select_string .= '<option value="' . $zones['geo_zone_id'] . '"';
      if ($selected == $zones['geo_zone_id']) $select_string .= ' SELECTED';
      $select_string .= '>' . $zones['geo_zone_name'] . '</option>';
    }
    $select_string .= '</select>';

    return $select_string;
  }

  function tep_get_geo_zone_name($geo_zone_id) {
    $zones_query = tep_db_query("select geo_zone_name from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$geo_zone_id . "'");

    if (!tep_db_num_rows($zones_query)) {
      $geo_zone_name = $geo_zone_id;
    } else {
      $zones = tep_db_fetch_array($zones_query);
      $geo_zone_name = $zones['geo_zone_name'];
    }

    return $geo_zone_name;
  }
  
  
  //<!-- ORDERS EDITOR AJAX -->
  /*function tep_address_format($address_format_id, $address, $html, $boln, $eoln) {
    $address_format_query = tep_db_query("select address_format as format from " . TABLE_ADDRESS_FORMAT . " where address_format_id = '" . (int)$address_format_id . "'");
    $address_format = tep_db_fetch_array($address_format_query);

    $company = tep_output_string_protected($address['company']);
    if (isset($address['firstname']) && tep_not_null($address['firstname'])) {
      $firstname = tep_output_string_protected($address['firstname']);
      $lastname = tep_output_string_protected($address['lastname']);
    } elseif (isset($address['name']) && tep_not_null($address['name'])) {
      $firstname = tep_output_string_protected($address['name']);
      $lastname = '';
    } else {
      $firstname = '';
      $lastname = '';
    }
    $street = tep_output_string_protected($address['street_address']);
    $suburb = tep_output_string_protected($address['suburb']);
    $city = tep_output_string_protected($address['city']);
    $state = tep_output_string_protected($address['state']);
    if (isset($address['country_id']) && tep_not_null($address['country_id'])) {
      $country = tep_get_country_name($address['country_id']);

      if (isset($address['zone_id']) && tep_not_null($address['zone_id'])) {
        $state = tep_get_zone_code($address['country_id'], $address['zone_id'], $state);
      }
    } elseif (isset($address['country']) && tep_not_null($address['country'])) {
      $country = tep_output_string_protected($address['country']);
    } else {
      $country = '';
    }
    $postcode = tep_output_string_protected($address['postcode']); */
	
	function tep_address_format($address_format_id, $address, $html, $boln, $eoln, $ajax, $order_id) {
		$address_format_query = tep_db_query("select address_format as format from " . TABLE_ADDRESS_FORMAT . " where address_format_id = '" . (int)$address_format_id . "'");
		$address_format = tep_db_fetch_array($address_format_query);
	
		$company = tep_output_string_protected($address['company']);
	
		/*
		$company = (($address['company'] != '') ? (($ajax != '') ? '<a href="javascript: updateOrderField(\'' 
		. $order_id . '\', \'orders\', \'' . $ajax . 'company\', \'' . addslashes(tep_output_string_protected($address['company'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['company']) . (($ajax != '') ? '</a>' : '') : '');
		*/
		
		if($address['company'] != ''){
			if($ajax != ''){
				$company = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'company\',\''.addslashes(tep_output_string_protected($address['company'])).'\');" class="ajaxLink">';
			}
			$company .= tep_output_string_protected($address['company']);
			if ($ajax != '') {
				$company.='</a>';
			}
		
		} else {
		
			if ($ajax != '') {
				$company = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'company\',\'\');" class="ajaxLink"><font color="#999999">+ (Soci�t�, Admin, Assos, ...)</font></a>';
			}
		}
	
		if (isset($address['firstname']) && tep_not_null($address['firstname'])) {
			$firstname = tep_output_string_protected($address['firstname']);
			$lastname = tep_output_string_protected($address['lastname']);
		} elseif (isset($address['name']) && tep_not_null($address['name'])) {
			$firstname = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'name\', \'' . addslashes(tep_output_string_protected($address['name'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['name']) . (($ajax != '') ? '</a>' : '');
			$lastname = '';
		} else {
			$firstname = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'name\',\'\');" class="ajaxLink"><font color="#999999">+ NOM & Pr�nom</font></a>';
			$lastname = '';
		}
		
		// Appt, Etage, Couloir, Esc
		$street3="";
		if($address['street_address_3'] != ''){
			if($ajax != ''){
				$street3 = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'street_address_3\',\''.addslashes(tep_output_string_protected($address['street_address_3'])).'\');" class="ajaxLink">';
			}
			$street3 .= tep_output_string_protected($address['street_address_3']);
			if ($ajax != '') {
				$street3.='</a>';
			}
		} else {
			if ($ajax != '') {
				$street3 = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'street_address_3\',\'\');" class="ajaxLink"><font color="#999999">+ (Appt, Etage, Couloir, Esc, ...)</font></a>';
			}
		}
		
		// Entr�e - Bat - Imm - R�s
		$suburb="";
		if($address['suburb'] != ''){
			if($ajax != ''){
				$suburb = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'suburb\',\''.addslashes(tep_output_string_protected($address['suburb'])).'\');" class="ajaxLink">';
			}
			$suburb .= tep_output_string_protected($address['suburb']);
			if ($ajax != '') {
				$suburb.='</a>';
			}
		} else {
			if ($ajax != '') {
				$suburb = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'suburb\',\'\');" class="ajaxLink"><font color="#999999">+ (Entr�e, Bat, Imm, R�s, ...)</font></a>';
			}
		}
		
		// N� + voie (rue, avenue, ...)
		$street="";
		if($address['street_address'] != ''){
			if ($ajax != '') {
				$street = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'street_address\',\''.addslashes(tep_output_string_protected($address['street_address'])).'\');" class="ajaxLink">';
			}
			$street .= tep_output_string_protected($address['street_address']);
			if ($ajax != '') {
				$street.='</a>';
			}
		} else {
			if ($ajax != '') {
				$street = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'street_address\',\'\');" class="ajaxLink"><font color="#999999">+ (N� + voie (rue, avenue, ...))</font></a>';
			}
		}
		
		// Boite postale, Lieu dit
		$street4="";
		if($address['street_address_4'] != ''){
			if ($ajax != '') {
				$street4 = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'street_address_4\',\''.addslashes(tep_output_string_protected($address['street_address_4'])).'\');" class="ajaxLink">';
			}
			$street4 .= tep_output_string_protected($address['street_address_4']);
			if ($ajax != '') {
				$street4.='</a>';
			}
		} else {
			if ($ajax != '') {
				$street4 = '<a href="javascript: updateOrderField(\''.$order_id.'\',\'orders\',\''.$ajax.'street_address_4\',\'\');" class="ajaxLink"><font color="#999999">+ (Boite postale, Lieu dit, ...)</font></a>';
			}
		}
		
	
    $city = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'city\', \'' . addslashes(tep_output_string_protected($address['city'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['city']) . (($ajax != '') ? '</a>' : '');
    $state = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'state\', \'' . addslashes(tep_output_string_protected($address['state'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['state']) . (($ajax != '') ? '</a>' : '');
    if (isset($address['country_id']) && tep_not_null($address['country_id'])) {
      $country = tep_get_country_name($address['country_id']);

      if (isset($address['zone_id']) && tep_not_null($address['zone_id'])) {
        $state = tep_get_zone_code($address['country_id'], $address['zone_id'], $state);
      }
    } elseif (isset($address['country']) && tep_not_null($address['country'])) {
      $country = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'country\', \'' . addslashes(tep_output_string_protected($address['country'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['country']) . (($ajax != '') ? '</a>' : '');
    } else {
      $country = '';
    }
    $postcode = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'postcode\', \'' . addslashes(tep_output_string_protected($address['postcode'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['postcode']) . (($ajax != '') ? '</a>' : '');
	//<!-- ORDERS EDITOR AJAX -->	
	
    $zip = $postcode;

    if ($html) {
// HTML Mode
      $HR = '<hr>';
      $hr = '<hr>';
      if ( ($boln == '') && ($eoln == "\n") ) { // Values not specified, use rational defaults
        $CR = '<br>';
        $cr = '<br>';
        $eoln = $cr;
      } else { // Use values supplied
        $CR = $eoln . $boln;
        $cr = $CR;
      }
    } else {
// Text Mode
      $CR = $eoln;
      $cr = $CR;
      $HR = '----------------------------------------';
      $hr = '----------------------------------------';
    }

    $statecomma = '';
    //$streets = $street;
	//if ($suburb != '') $streets = $suburb . $cr . $street;
	$streets = '';
	if ($street3!= '') $streets = $street3 . $cr;
	if ($suburb != '') $streets .= $suburb . $cr;
	if ($street != '') $streets .= $street . $cr;
	if ($street4!= '') $streets .= $street4;
	
    if ($country == '') $country = tep_output_string_protected($address['country']);
	if ($state != '') $statecomma = $state . ' ';

    $fmt = $address_format['format'];
    eval("\$address = \"$fmt\";");

    if ( (ACCOUNT_COMPANY == 'true') && (tep_not_null($company)) ) {
      $address = $company . $cr . $address;
    }

    return $address;
  }

  
  
  
  
  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // Function    : tep_get_zone_code
  //
  // Arguments   : country           country code string
  //               zone              state/province zone_id
  //               def_state         default string if zone==0
  //
  // Return      : state_prov_code   state/province code
  //
  // Description : Function to retrieve the state/province code (as in FL for Florida etc)
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////
  function tep_get_zone_code($country, $zone, $def_state) {

    $state_prov_query = tep_db_query("select zone_code from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and zone_id = '" . (int)$zone . "'");

    if (!tep_db_num_rows($state_prov_query)) {
      $state_prov_code = $def_state;
    }
    else {
      $state_prov_values = tep_db_fetch_array($state_prov_query);
      $state_prov_code = $state_prov_values['zone_code'];
    }
    
    return $state_prov_code;
  }

  function tep_get_uprid($prid, $params) {
    $uprid = $prid;
    if ( (is_array($params)) && (!strstr($prid, '{')) ) {
      while (list($option, $value) = each($params)) {
        $uprid = $uprid . '{' . $option . '}' . $value;
      }
    }

    return $uprid;
  }

  function tep_get_prid($uprid) {
    $pieces = explode('{', $uprid);

    return $pieces[0];
  }

  function tep_get_languages() {
    $languages_query = tep_db_query("select languages_id, name, code, image, directory from " . TABLE_LANGUAGES . " order by sort_order");
    while ($languages = tep_db_fetch_array($languages_query)) {
      $languages_array[] = array('id' => $languages['languages_id'],
                                 'name' => $languages['name'],
                                 'code' => $languages['code'],
                                 'image' => $languages['image'],
                                 'directory' => $languages['directory']);
    }

    return $languages_array;
  }

  function tep_get_category_name($category_id, $language_id) {
    $category_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_name'];
  }

  function tep_get_orders_status_name($orders_status_id, $language_id = '') {
    global $languages_id;

    if (!$language_id) $language_id = $languages_id;
    $orders_status_query = tep_db_query("select orders_status_name from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . (int)$orders_status_id . "' and language_id = '" . (int)$language_id . "'");
    $orders_status = tep_db_fetch_array($orders_status_query);

    return $orders_status['orders_status_name'];
  }

  function tep_get_orders_status() {
    global $languages_id;

    $orders_status_array = array();
    $orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' order by orders_status_id");
    while ($orders_status = tep_db_fetch_array($orders_status_query)) {
      $orders_status_array[] = array('id' => $orders_status['orders_status_id'],
                                     'text' => $orders_status['orders_status_name']);
    }

    return $orders_status_array;
  }

  function tep_get_products_name($product_id, $language_id = 0) {
    global $languages_id;

    if ($language_id == 0) $language_id = $languages_id;
    $product_query = tep_db_query("select products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_name'];
  }

  function tep_get_products_description($product_id, $language_id) {
    $product_query = tep_db_query("select products_description from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_description'];
  }

  function tep_get_products_url($product_id, $language_id) {
    $product_query = tep_db_query("select products_url from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_url'];
  }

////
// Return the manufacturers URL in the needed language
// TABLES: manufacturers_info
  function tep_get_manufacturer_url($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_url from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_url'];
  }

////
// Return the family URL in the needed language
// TABLES: family_info
  function tep_get_family_url($family_id, $language_id) {
    $family_query = tep_db_query("select family_url from " . TABLE_FAMILY_INFO . " where family_id = '" . (int)$family_id . "' and languages_id = '" . (int)$language_id . "'");
    $family = tep_db_fetch_array($family_query);

    return $family['family_url'];
  }
	
////
// Return the family client URL in the needed language
// TABLES: family_client_info
  function tep_get_family_client_url($family_client_id, $language_id) {
    $family_client_query = tep_db_query("select family_client_url from " . TABLE_CLIENT_FAMILY_INFO . " where family_client_id = '" . (int)$family_client_id . "' and languages_id = '" . (int)$language_id . "'");
    $family_client = tep_db_fetch_array($family_client_query);

    return $family_client['family_client_url'];
  }

////
// Wrapper for class_exists() function
// This function is not available in all PHP versions so we test it before using it.
  function tep_class_exists($class_name) {
    if (function_exists('class_exists')) {
      return class_exists($class_name);
    } else {
      return true;
    }
  }

////
// Count how many products exist in a category
// TABLES: products, products_to_categories, categories
  function tep_products_in_category_count($categories_id, $include_deactivated = false) {
    $products_count = 0;

    if ($include_deactivated) {
      $products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = p2c.products_id and p2c.categories_id = '" . (int)$categories_id . "'");
    } else {
      $products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = p2c.products_id and p.products_status = '1' and p2c.categories_id = '" . (int)$categories_id . "'");
    }

    $products = tep_db_fetch_array($products_query);

    $products_count += $products['total'];

    $childs_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$categories_id . "'");
    if (tep_db_num_rows($childs_query)) {
      while ($childs = tep_db_fetch_array($childs_query)) {
        $products_count += tep_products_in_category_count($childs['categories_id'], $include_deactivated);
      }
    }

    return $products_count;
  }

////
// Count how many subcategories exist in a category
// TABLES: categories
  function tep_childs_in_category_count($categories_id) {
    $categories_count = 0;

    $categories_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$categories_id . "'");
    while ($categories = tep_db_fetch_array($categories_query)) {
      $categories_count++;
      $categories_count += tep_childs_in_category_count($categories['categories_id']);
    }

    return $categories_count;
  }
  
  function tep_direct_subcategory_list($categories_id) {
    $categories_list=array();
	$query_cat="select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$categories_id . "'";
	$categories_query = tep_db_query($query_cat);
    while ($categories = tep_db_fetch_array($categories_query)) {
      $categories_list[]=$categories['categories_id'];
    }

    return $categories_list;
  }

//
// Returns an array with countries
// TABLES: countries
  function tep_get_countries($default = '') {
    $countries_array = array();
    if ($default) {
      $countries_array[] = array('id' => '',
                                 'text' => $default);
    }
    $countries_query = tep_db_query("select countries_id, countries_name from " . TABLE_COUNTRIES . " order by countries_name");
    while ($countries = tep_db_fetch_array($countries_query)) {
      $countries_array[] = array('id' => $countries['countries_id'],
                                 'text' => $countries['countries_name']);
    }

    return $countries_array;
  }

////
// return an array with country zones
  function tep_get_country_zones($country_id) {
    $zones_array = array();
    $zones_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' order by zone_name");
    while ($zones = tep_db_fetch_array($zones_query)) {
      $zones_array[] = array('id' => $zones['zone_id'],
                             'text' => $zones['zone_name']);
    }

    return $zones_array;
  }

  function tep_prepare_country_zones_pull_down($country_id = '') {
// preset the width of the drop-down for Netscape
    $pre = '';
    if ( (!tep_browser_detect('MSIE')) && (tep_browser_detect('Mozilla/4')) ) {
      for ($i=0; $i<45; $i++) $pre .= '&nbsp;';
    }

    $zones = tep_get_country_zones($country_id);

    if (sizeof($zones) > 0) {
      $zones_select = array(array('id' => '', 'text' => PLEASE_SELECT));
      $zones = array_merge($zones_select, $zones);
    } else {
      $zones = array(array('id' => '', 'text' => TYPE_BELOW));
// create dummy options for Netscape to preset the height of the drop-down
      if ( (!tep_browser_detect('MSIE')) && (tep_browser_detect('Mozilla/4')) ) {
        for ($i=0; $i<9; $i++) {
          $zones[] = array('id' => '', 'text' => $pre);
        }
      }
    }

    return $zones;
  }

////
// Get list of address_format_id's
  function tep_get_address_formats() {
    $address_format_query = tep_db_query("select address_format_id from " . TABLE_ADDRESS_FORMAT . " order by address_format_id");
    $address_format_array = array();
    while ($address_format_values = tep_db_fetch_array($address_format_query)) {
      $address_format_array[] = array('id' => $address_format_values['address_format_id'],
                                      'text' => $address_format_values['address_format_id']);
    }
    return $address_format_array;
  }

////
// Alias function for Store configuration values in the Administration Tool
  function tep_cfg_pull_down_country_list($country_id) {
    return tep_draw_pull_down_menu('configuration_value', tep_get_countries(), $country_id);
  }

  function tep_cfg_pull_down_zone_list($zone_id) {
    return tep_draw_pull_down_menu('configuration_value', tep_get_country_zones(STORE_COUNTRY), $zone_id);
  }

  function tep_cfg_pull_down_tax_classes($tax_class_id, $key = '') {
    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
    $tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
    while ($tax_class = tep_db_fetch_array($tax_class_query)) {
      $tax_class_array[] = array('id' => $tax_class['tax_class_id'],
                                 'text' => $tax_class['tax_class_title']);
    }

    return tep_draw_pull_down_menu($name, $tax_class_array, $tax_class_id);
  }

////
// Function to read in text area in admin
 function tep_cfg_textarea($text) {
    return tep_draw_textarea_field('configuration_value', false, 35, 5, $text);
  }

  function tep_cfg_get_zone_name($zone_id) {
    $zone_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_id = '" . (int)$zone_id . "'");

    if (!tep_db_num_rows($zone_query)) {
      return $zone_id;
    } else {
      $zone = tep_db_fetch_array($zone_query);
      return $zone['zone_name'];
    }
  }

////
// Sets the status of a banner
  function tep_set_banner_status($banners_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_BANNERS . " set status = '1', expires_impressions = NULL, expires_date = NULL, date_status_change = NULL where banners_id = '" . $banners_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_BANNERS . " set status = '0', date_status_change = now() where banners_id = '" . $banners_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the status of a product
  function tep_set_product_status($products_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_PRODUCTS . " set products_status = '1', products_last_modified = now() where products_id = '" . (int)$products_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_PRODUCTS . " set products_status = '0', products_last_modified = now() where products_id = '" . (int)$products_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the status of a category
  function tep_set_categorie_status($categories_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_CATEGORIES . " set categories_status = '1', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
    } 
	elseif ($status == '0') {
      return tep_db_query("update " . TABLE_CATEGORIES . " set categories_status = '0', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
    } 
	else {
      return -1;
    }
  }

////
// Sets the status of a product on special
  function tep_set_specials_status($specials_id, $status) {
// ###### Added CCGV Contribution #########
// Sets the status of a coupon
  function tep_set_coupon_status($coupon_id, $status) {
    if ($status == 'Y') {
      return tep_db_query("update " . TABLE_COUPONS . " set coupon_active = 'Y', date_modified = now() where coupon_id = '" . (int)$coupon_id . "'");
    } elseif ($status == 'N') {
      return tep_db_query("update " . TABLE_COUPONS . " set coupon_active = 'N', date_modified = now() where coupon_id = '" . (int)$coupon_id . "'");
    } else {
      return -1;
    }
  }
// ###### end CCGV Contribution #########

    if ($status == '1') {
      return tep_db_query("update " . TABLE_SPECIALS . " set status = '1', expires_date = NULL, date_status_change = NULL where specials_id = '" . (int)$specials_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_SPECIALS . " set status = '0', date_status_change = now() where specials_id = '" . (int)$specials_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets timeout for the current script.
// Cant be used in safe mode.
  function tep_set_time_limit($limit) {
    if (!get_cfg_var('safe_mode')) {
      set_time_limit($limit);
    }
  }

////
// Alias function for Store configuration values in the Administration Tool
  function tep_cfg_select_option($select_array, $key_value, $key = '') {
    $string = '';

    for ($i=0, $n=sizeof($select_array); $i<$n; $i++) {
      $name = ((tep_not_null($key)) ? 'configuration[' . $key . ']' : 'configuration_value');

      $string .= '<br><input type="radio" name="' . $name . '" value="' . $select_array[$i] . '"';

      if ($key_value == $select_array[$i]) $string .= ' CHECKED';

      $string .= '> ' . $select_array[$i];
    }

    return $string;
  }

////
// Alias function for module configuration keys
  function tep_mod_select_option($select_array, $key_name, $key_value) {
    reset($select_array);
    while (list($key, $value) = each($select_array)) {
      if (is_int($key)) $key = $value;
      $string .= '<br><input type="radio" name="configuration[' . $key_name . ']" value="' . $key . '"';
      if ($key_value == $key) $string .= ' CHECKED';
      $string .= '> ' . $value;
    }

    return $string;
  }

////
// Retreive server information
  function tep_get_system_information() {
    global $HTTP_SERVER_VARS;

    $db_query = tep_db_query("select now() as datetime");
    $db = tep_db_fetch_array($db_query);

    list($system, $host, $kernel) = preg_split('/[\s,]+/', @exec('uname -a'), 5);

    return array('date' => tep_datetime_short(date('Y-m-d H:i:s')),
                 'system' => $system,
                 'kernel' => $kernel,
                 'host' => $host,
                 'ip' => gethostbyname($host),
                 'uptime' => @exec('uptime'),
                 'http_server' => $HTTP_SERVER_VARS['SERVER_SOFTWARE'],
                 'php' => PHP_VERSION,
                 'zend' => (function_exists('zend_version') ? zend_version() : ''),
                 'db_server' => DB_SERVER,
                 'db_ip' => gethostbyname(DB_SERVER),
                 'db_version' => 'MySQL ' . (function_exists('mysql_get_server_info') ? mysql_get_server_info() : ''),
                 'db_date' => tep_datetime_short($db['datetime']));
  }

  function tep_generate_category_path($id, $from = 'category', $categories_array = '', $index = 0) {
    global $languages_id;

    if (!is_array($categories_array)) $categories_array = array();

    if ($from == 'product') {
      $categories_query = tep_db_query("select cd.categories_name, p.categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " p, " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where cd.categories_id=c.categories_id and c.categories_id=p.categories_id and  products_id = '" . (int)$id . "' order by cd.categories_name");
      while ($categories = tep_db_fetch_array($categories_query)) {
        if ($categories['categories_id'] == '0') {
          $categories_array[$index][] = array('id' => '0', 'text' => TEXT_TOP);
        } else {
          $category_query = tep_db_query("select cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$categories['categories_id'] . "' and c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "'");
          $category = tep_db_fetch_array($category_query);
          $categories_array[$index][] = array('id' => $categories['categories_id'], 'text' => $category['categories_name']);
          if ( (tep_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = tep_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
          $categories_array[$index] = array_reverse($categories_array[$index]);
        }
        $index++;
      }
    } elseif ($from == 'category') {
      $category_query = tep_db_query("select cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$id . "' and c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "'");
      $category = tep_db_fetch_array($category_query);
      $categories_array[$index][] = array('id' => $id, 'text' => $category['categories_name']);
      if ( (tep_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = tep_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
    }

    return $categories_array;
  }

  function tep_output_generated_category_path($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = tep_generate_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -16) . '<br>';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -4);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }

  function tep_get_generated_category_path_ids($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = tep_generate_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['id'] . '_';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -1) . '<br>';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -4);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }

  function tep_remove_category($category_id) {
    $category_image_query = tep_db_query("select categories_image from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    $category_image = tep_db_fetch_array($category_image_query);

    $duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_CATEGORIES . " where categories_image = '" . tep_db_input($category_image['categories_image']) . "'");
    $duplicate_image = tep_db_fetch_array($duplicate_image_query);

    if ($duplicate_image['total'] < 2) {
      if (file_exists(DIR_FS_CATALOG_IMAGES . $category_image['categories_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $category_image['categories_image']);
      }
    }

    tep_db_query("delete from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    tep_db_query("delete from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");

    if (USE_CACHE == 'true') {
      tep_reset_cache_block('categories');
      tep_reset_cache_block('also_purchased');
    }
  }

  function tep_remove_product($product_id) {
    $product_image_query = tep_db_query("select products_bimage from " . TABLE_PRODUCTS_IMAGE . " where products_id = '" . (int)$product_id . "'");
    while($product_image_query && $product_image = tep_db_fetch_array($product_image_query)){
		
		$verif_table_image_query = tep_db_query("SELECT COUNT(products_bimage) as nb_images FROM " . TABLE_PRODUCTS_IMAGE . " WHERE products_bimage='" . $product_image['products_bimage'] . "' AND products_id <> '".(int)$product_id."'");
		$verif_table_image = tep_db_fetch_array($verif_table_image_query);
		
		$verif_table_products_query = tep_db_query("SELECT COUNT(products_bimage) as nb_images FROM " . TABLE_PRODUCTS . " WHERE products_bimage='" . $product_image['products_bimage'] . "' AND products_id <> '".(int)$product_id."'");
		$verif_table_products = tep_db_fetch_array($verif_table_products_query);
		
		if ($verif_table_image['nb_images'] == 0 
							   && $verif_table_products['nb_images'] == 0 
							   && $product_image['products_bimage'] != DEFAULT_IMAGE) {
			
		  	if (file_exists(DIR_FS_CATALOG_IMAGES . $product_image['products_bimage'])) {
				@unlink(DIR_FS_CATALOG_IMAGES . $product_image['products_bimage']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."grande/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."grande/".$product_image['products_bimage']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."normale/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."normale/".$product_image['products_bimage']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."micro/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."micro/".$product_image['products_bimage']);
			}
			/*if (file_exists(DIR_FS_PRODUCTS_IMAGES."mini/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."mini/".$product_image['products_bimage']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."petite/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."petite/".$product_image['products_bimage']);
			}*/
		}
	}
	
    $product_image_query = tep_db_query("select products_bimage from " . TABLE_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
	
    while($product_image_query && $product_image = tep_db_fetch_array($product_image_query)) {
		
		$verif_table_image_query = tep_db_query("SELECT COUNT(products_bimage) as nb_images FROM " . TABLE_PRODUCTS_IMAGE . " WHERE products_bimage='" . $product_image['products_bimage'] . "' AND products_id <> '".(int)$product_id."'");
		$verif_table_image = tep_db_fetch_array($verif_table_image_query);
		
		$verif_table_products_query = tep_db_query("SELECT COUNT(products_bimage) as nb_images FROM " . TABLE_PRODUCTS . " WHERE products_bimage='" . $product_image['products_bimage'] . "' AND products_id <> '".(int)$product_id."'");
		$verif_table_products = tep_db_fetch_array($verif_table_products_query);
		
		if ($verif_table_image['nb_images'] == 0 
							   && $verif_table_products['nb_images'] == 0 
							   && $product_image['products_bimage'] != DEFAULT_IMAGE) {
			
			if (file_exists(DIR_FS_CATALOG_IMAGES . $product_image['products_bimage'])) {
				@unlink(DIR_FS_CATALOG_IMAGES . $product_image['products_bimage']);
			}
			
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."grande/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."grande/".$product_image['products_bimage']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."normale/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."normale/".$product_image['products_bimage']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."micro/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."micro/".$product_image['products_bimage']);
			}
			/*if (file_exists(DIR_FS_PRODUCTS_IMAGES."mini/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."mini/".$product_image['products_bimage']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."petite/".$product_image['products_bimage'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."petite/".$product_image['products_bimage']);
			}*/
			
			/*if (file_exists(DIR_FS_CATALOG_IMAGES . $product_image['products_image'])) {
				@unlink(DIR_FS_CATALOG_IMAGES . $product_image['products_image']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."grande/".$product_image['products_image'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."grande/".$product_image['products_image']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."normale/".$product_image['products_image'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."normale/".$product_image['products_image']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."micro/".$product_image['products_image'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."micro/".$product_image['products_image']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."mini/".$product_image['products_image'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."mini/".$product_image['products_bimage']);
			}
			if (file_exists(DIR_FS_PRODUCTS_IMAGES."petite/".$product_image['products_image'])) {
				@unlink(DIR_FS_PRODUCTS_IMAGES."petite/".$product_image['products_bimage']);
			}*/
		}
	}
    
	tep_db_query("delete from " . TABLE_PACK . " where products_id = '" . (int)$product_id . "' or products_id_1 = '" . (int)$product_id . "' or products_id_2 = '" . (int)$product_id . "'");
	tep_db_query("delete from " . TABLE_SPECIALS . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_IMAGE . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where products_id = '" . (int)$product_id . "'");
	tep_db_query("delete from " . TABLE_PRODUCTS_PRICE_BY_QUANTITY . " where products_id = '" . (int)$product_id . "'");

    $product_reviews_query = tep_db_query("SELECT reviews_id FROM " . TABLE_REVIEWS . " WHERE products_id = '" . (int)$product_id . "'");
    while ($product_reviews = tep_db_fetch_array($product_reviews_query)) {
      tep_db_query("delete from " . TABLE_REVIEWS_DESCRIPTION . " where reviews_id = '" . (int)$product_reviews['reviews_id'] . "'");
    }
    tep_db_query("delete from " . TABLE_REVIEWS . " where products_id = '" . (int)$product_id . "'");

    if (USE_CACHE == 'true') {
      tep_reset_cache_block('categories');
      tep_reset_cache_block('also_purchased');
    }
  }

function tep_remove_order($order_id, $restock = false) {
	// si on doit remettre en stock
	if ($restock == 'on') {
		$order_query = tep_db_query("	SELECT 
											orders_products_id, 
											products_id, 
											products_quantity 
										FROM " . TABLE_ORDERS_PRODUCTS . " 
										WHERE orders_id = '" . (int)$order_id . "'
									");
	  
		while ($order = tep_db_fetch_array($order_query)) {
			  
			$order_attribute_id_query = tep_db_query("	SELECT 
															pov.products_options_values_id 
														FROM 
															" . TABLE_PRODUCTS_OPTIONS_VALUES . " pov, 
															" . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " opa 
														WHERE 
															opa.options_values_id = pov.products_options_values_id 
														AND 
															orders_products_id = '" . $order['orders_products_id'] . "'
													");
			$order_attribute_id = tep_db_fetch_array($order_attribute_id_query);
			  
			if (tep_db_num_rows($order_attribute_id_query) != 0) {
				
				tep_db_query("	UPDATE " . TABLE_PRODUCTS_ATTRIBUTES . " SET 
									options_quantity = options_quantity + " . $order['products_quantity'] . " 
								WHERE 
									products_id = '" . (int)$order['products_id'] . "' 
								AND 
									options_values_id = '" . $order_attribute_id['products_options_values_id'] . "'
							");
			}
			  
			tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity + " . $order['products_quantity'] . ", products_ordered = products_ordered - " . $order['products_quantity'] . " WHERE products_id = '" . (int)$order['products_id'] . "'");
		}
	}

    tep_db_query("DELETE FROM " . TABLE_ORDERS . " WHERE orders_id = '" . (int)$order_id . "'");
    tep_db_query("DELETE FROM " . TABLE_ORDERS_PRODUCTS . " WHERE orders_id = '" . (int)$order_id . "'");
    tep_db_query("DELETE FROM " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " WHERE orders_id = '" . (int)$order_id . "'");
    tep_db_query("DELETE FROM " . TABLE_ORDERS_STATUS_HISTORY . " WHERE orders_id = '" . (int)$order_id . "'");
    tep_db_query("DELETE FROM " . TABLE_ORDERS_TOTAL . " WHERE orders_id = '" . (int)$order_id . "'");
}
  
  
   function tep_remove_scart($scartid) 
  {
    tep_db_query("delete from " . TABLE_SCART . " WHERE scartid = '" . (int)$scartid . "'");
  }
  
  

  function tep_reset_cache_block($cache_block) {
    global $cache_blocks;

    for ($i=0, $n=sizeof($cache_blocks); $i<$n; $i++) {
      if ($cache_blocks[$i]['code'] == $cache_block) {
        if ($cache_blocks[$i]['multiple']) {
          if ($dir = @opendir(DIR_FS_CACHE)) {
            while ($cache_file = readdir($dir)) {
              $cached_file = $cache_blocks[$i]['file'];
              $languages = tep_get_languages();
              for ($j=0, $k=sizeof($languages); $j<$k; $j++) {
                $cached_file_unlink = ereg_replace('-language', '-' . $languages[$j]['directory'], $cached_file);
                if (ereg('^' . $cached_file_unlink, $cache_file)) {
                  @unlink(DIR_FS_CACHE . $cache_file);
                }
              }
            }
            closedir($dir);
          }
        } else {
          $cached_file = $cache_blocks[$i]['file'];
          $languages = tep_get_languages();
          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $cached_file = ereg_replace('-language', '-' . $languages[$i]['directory'], $cached_file);
            @unlink(DIR_FS_CACHE . $cached_file);
          }
        }
        break;
      }
    }
  }

  function tep_get_file_permissions($mode) {
// determine type
    if ( ($mode & 0xC000) == 0xC000) { // unix domain socket
      $type = 's';
    } elseif ( ($mode & 0x4000) == 0x4000) { // directory
      $type = 'd';
    } elseif ( ($mode & 0xA000) == 0xA000) { // symbolic link
      $type = 'l';
    } elseif ( ($mode & 0x8000) == 0x8000) { // regular file
      $type = '-';
    } elseif ( ($mode & 0x6000) == 0x6000) { //bBlock special file
      $type = 'b';
    } elseif ( ($mode & 0x2000) == 0x2000) { // character special file
      $type = 'c';
    } elseif ( ($mode & 0x1000) == 0x1000) { // named pipe
      $type = 'p';
    } else { // unknown
      $type = '?';
    }

// determine permissions
    $owner['read']    = ($mode & 00400) ? 'r' : '-';
    $owner['write']   = ($mode & 00200) ? 'w' : '-';
    $owner['execute'] = ($mode & 00100) ? 'x' : '-';
    $group['read']    = ($mode & 00040) ? 'r' : '-';
    $group['write']   = ($mode & 00020) ? 'w' : '-';
    $group['execute'] = ($mode & 00010) ? 'x' : '-';
    $world['read']    = ($mode & 00004) ? 'r' : '-';
    $world['write']   = ($mode & 00002) ? 'w' : '-';
    $world['execute'] = ($mode & 00001) ? 'x' : '-';

// adjust for SUID, SGID and sticky bit
    if ($mode & 0x800 ) $owner['execute'] = ($owner['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x400 ) $group['execute'] = ($group['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x200 ) $world['execute'] = ($world['execute'] == 'x') ? 't' : 'T';

    return $type .
           $owner['read'] . $owner['write'] . $owner['execute'] .
           $group['read'] . $group['write'] . $group['execute'] .
           $world['read'] . $world['write'] . $world['execute'];
  }

  function tep_remove($source) {
    global $messageStack, $tep_remove_error;

    if (isset($tep_remove_error)) $tep_remove_error = false;

    if (is_dir($source)) {
      $dir = dir($source);
      while ($file = $dir->read()) {
        if ( ($file != '.') && ($file != '..') ) {
          if (is_writeable($source . '/' . $file)) {
            tep_remove($source . '/' . $file);
          } else {
            $messageStack->add(sprintf(ERROR_FILE_NOT_REMOVEABLE, $source . '/' . $file), 'error');
            $tep_remove_error = true;
          }
        }
      }
      $dir->close();

      if (is_writeable($source)) {
        rmdir($source);
      } else {
        $messageStack->add(sprintf(ERROR_DIRECTORY_NOT_REMOVEABLE, $source), 'error');
        $tep_remove_error = true;
      }
    } else {
      if (is_writeable($source)) {
        unlink($source);
      } else {
        $messageStack->add(sprintf(ERROR_FILE_NOT_REMOVEABLE, $source), 'error');
        $tep_remove_error = true;
      }
    }
  }

////
// Output the tax percentage with optional padded decimals
  function tep_display_tax_value($value, $padding = TAX_DECIMAL_PLACES) {
    if (strpos($value, '.')) {
      $loop = true;
      while ($loop) {
        if (substr($value, -1) == '0') {
          $value = substr($value, 0, -1);
        } else {
          $loop = false;
          if (substr($value, -1) == '.') {
            $value = substr($value, 0, -1);
          }
        }
      }
    }

    if ($padding > 0) {
      if ($decimal_pos = strpos($value, '.')) {
        $decimals = strlen(substr($value, ($decimal_pos+1)));
        for ($i=$decimals; $i<$padding; $i++) {
          $value .= '0';
        }
      } else {
        $value .= '.';
        for ($i=0; $i<$padding; $i++) {
          $value .= '0';
        }
      }
    }

    return $value;
  }

  function tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address) {
    if (SEND_EMAILS != 'true') return false;

    // Instantiate a new mail object
    $message = new email(array('X-Mailer: osCommerce'));

    // Build the text version
    $text = strip_tags($email_text);
    if (EMAIL_USE_HTML == 'true') {
      $message->add_html($email_text, $text);
    } else {
      $message->add_text($text);
    }

    // Send message
    $message->build_message();
    $message->send($to_name, $to_email_address, $from_email_name, $from_email_address, $email_subject);
  }

  function tep_get_tax_class_title($tax_class_id) {
    if ($tax_class_id == '0') {
      return TEXT_NONE;
    } else {
      $classes_query = tep_db_query("select tax_class_title from " . TABLE_TAX_CLASS . " where tax_class_id = '" . (int)$tax_class_id . "'");
      $classes = tep_db_fetch_array($classes_query);

      return $classes['tax_class_title'];
    }
  }

  function tep_banner_image_extension() {
    if (function_exists('imagetypes')) {
      if (imagetypes() & IMG_PNG) {
        return 'png';
      } elseif (imagetypes() & IMG_JPG) {
        return 'jpg';
      } elseif (imagetypes() & IMG_GIF) {
        return 'gif';
      }
    } elseif (function_exists('imagecreatefrompng') && function_exists('imagepng')) {
      return 'png';
    } elseif (function_exists('imagecreatefromjpeg') && function_exists('imagejpeg')) {
      return 'jpg';
    } elseif (function_exists('imagecreatefromgif') && function_exists('imagegif')) {
      return 'gif';
    }

    return false;
  }

////
// Wrapper function for round() for php3 compatibility
  function tep_round($value, $precision) {
    if (PHP_VERSION < 4) {
      $exp = pow(10, $precision);
      return round($value * $exp) / $exp;
    } else {
      return round($value, $precision);
    }
  }

////
// Add tax to a products price
  function tep_add_tax($price, $tax) {
    global $currencies;

    if (DISPLAY_PRICE_WITH_TAX == 'true') {
      return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']) + tep_calculate_tax($price, $tax);
    } else {
      return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
    }
  }

// Calculates Tax rounding the result
  function tep_calculate_tax($price, $tax) {
    global $currencies;

    return tep_round($price * $tax / 100, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
  }

////
// Returns the tax rate for a zone / class
// TABLES: tax_rates, zones_to_geo_zones
  function tep_get_tax_rate($class_id, $country_id = -1, $zone_id = -1) {
    global $customer_zone_id, $customer_country_id;

    if ( ($country_id == -1) && ($zone_id == -1) ) {
      if (!tep_session_is_registered('customer_id')) {
        $country_id = STORE_COUNTRY;
        $zone_id = STORE_ZONE;
      } else {
        $country_id = $customer_country_id;
        $zone_id = $customer_zone_id;
      }
    }

    $tax_query = tep_db_query("select SUM(tax_rate) as tax_rate from " . TABLE_TAX_RATES . " tr left join " . TABLE_ZONES_TO_GEO_ZONES . " za ON tr.tax_zone_id = za.geo_zone_id left join " . TABLE_GEO_ZONES . " tz ON tz.geo_zone_id = tr.tax_zone_id WHERE (za.zone_country_id IS NULL OR za.zone_country_id = '0' OR za.zone_country_id = '" . (int)$country_id . "') AND (za.zone_id IS NULL OR za.zone_id = '0' OR za.zone_id = '" . (int)$zone_id . "') AND tr.tax_class_id = '" . (int)$class_id . "' GROUP BY tr.tax_priority");
    if (tep_db_num_rows($tax_query)) {
      $tax_multiplier = 0;
      while ($tax = tep_db_fetch_array($tax_query)) {
        $tax_multiplier += $tax['tax_rate'];
      }
      return $tax_multiplier;
    } else {
      return 0;
    }
  }

////
// Returns the tax rate for a tax class
// TABLES: tax_rates
  function tep_get_tax_rate_value($class_id) {
    $tax_query = tep_db_query("select SUM(tax_rate) as tax_rate from " . TABLE_TAX_RATES . " where tax_class_id = '" . (int)$class_id . "' group by tax_priority");
    if (tep_db_num_rows($tax_query)) {
      $tax_multiplier = 0;
      while ($tax = tep_db_fetch_array($tax_query)) {
        $tax_multiplier += $tax['tax_rate'];
      }
      return $tax_multiplier;
    } else {
      return 0;
    }
  }

  function tep_call_function($function, $parameter, $object = '') {
    if ($object == '') {
      return call_user_func($function, $parameter);
    } elseif (PHP_VERSION < 4) {
      return call_user_method($function, $object, $parameter);
    } else {
      return call_user_func(array($object, $function), $parameter);
    }
  }

  function tep_get_zone_class_title($zone_class_id) {
    if ($zone_class_id == '0') {
      return TEXT_NONE;
    } else {
      $classes_query = tep_db_query("select geo_zone_name from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$zone_class_id . "'");
      $classes = tep_db_fetch_array($classes_query);

      return $classes['geo_zone_name'];
    }
  }

  function tep_cfg_pull_down_zone_classes($zone_class_id, $key = '') {
    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $zone_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
    $zone_class_query = tep_db_query("select geo_zone_id, geo_zone_name from " . TABLE_GEO_ZONES . " order by geo_zone_name");
    while ($zone_class = tep_db_fetch_array($zone_class_query)) {
      $zone_class_array[] = array('id' => $zone_class['geo_zone_id'],
                                  'text' => $zone_class['geo_zone_name']);
    }

    return tep_draw_pull_down_menu($name, $zone_class_array, $zone_class_id);
  }

  function tep_cfg_pull_down_order_statuses($order_status_id, $key = '') {
    global $languages_id;

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $statuses_array = array(array('id' => '0', 'text' => TEXT_DEFAULT));
    $statuses_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' order by orders_status_name");
    while ($statuses = tep_db_fetch_array($statuses_query)) {
      $statuses_array[] = array('id' => $statuses['orders_status_id'],
                                'text' => $statuses['orders_status_name']);
    }

    return tep_draw_pull_down_menu($name, $statuses_array, $order_status_id);
  }

  function tep_get_order_status_name($order_status_id, $language_id = '') {
    global $languages_id;

    if ($order_status_id < 1) return TEXT_DEFAULT;

    if (!is_numeric($language_id)) $language_id = $languages_id;

    $status_query = tep_db_query("select orders_status_name from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . (int)$order_status_id . "' and language_id = '" . (int)$language_id . "'");
    $status = tep_db_fetch_array($status_query);

    return $status['orders_status_name'];
  }

////
// Return a random value
  function tep_rand($min = null, $max = null) {
    static $seeded;

    if (!$seeded) {
      mt_srand((double)microtime()*1000000);
      $seeded = true;
    }

    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }

// nl2br() prior PHP 4.2.0 did not convert linefeeds on all OSs (it only converted \n)
  function tep_convert_linefeeds($from, $to, $string) {
    if ((PHP_VERSION < "4.0.5") && is_array($from)) {
      return ereg_replace('(' . implode('|', $from) . ')', $to, $string);
    } else {
      return str_replace($from, $to, $string);
    }
  }

  function tep_string_to_int($string) {
    return (int)$string;
  }

////
// Parse and secure the cPath parameter values
  function tep_parse_category_path($cPath) {
// make sure the category IDs are integers
    $cPath_array = array_map('tep_string_to_int', explode('_', $cPath));

// make sure no duplicate category IDs exist which could lock the server in a loop
    $tmp_array = array();
    $n = sizeof($cPath_array);
    for ($i=0; $i<$n; $i++) {
      if (!in_array($cPath_array[$i], $tmp_array)) {
        $tmp_array[] = $cPath_array[$i];
      }
    }

    return $tmp_array;
  }
  
  // Function to reset SEO URLs database cache entries 
// Ultimate SEO URLs v2.1
function tep_reset_cache_data_seo_urls($action){	
	switch ($action){
		case 'reset':
			tep_db_query("DELETE FROM cache WHERE cache_name LIKE '%seo_urls%'");
			tep_db_query("UPDATE configuration SET configuration_value='false' WHERE configuration_key='SEO_URLS_CACHE_RESET'");
			break;
		default:
			break;
	}
	# The return value is used to set the value upon viewing
	# It's NOT returining a false to indicate failure!!
	return 'false';
}
// FIN Ultimate SEO URLs v2.1
  
  // HTC BEGIN
  function tep_get_category_htc_title($category_id, $language_id) {
    $category_query = tep_db_query("select categories_htc_title_tag from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_htc_title_tag'];
  }
    
  function tep_get_category_htc_desc($category_id, $language_id) {
    $category_query = tep_db_query("select categories_htc_desc_tag from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_htc_desc_tag'];
  }
   
  function tep_get_category_htc_keywords($category_id, $language_id) {
    $category_query = tep_db_query("select categories_htc_keywords_tag from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_htc_keywords_tag'];
  }
  
  function tep_get_category_htc_description($category_id, $language_id) {
    $category_query = tep_db_query("select categories_htc_description from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_htc_description'];
  }
	
	// AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY
	function tep_get_balise_title_lien_image($category_id, $language_id) {
    $category_query = tep_db_query("select balise_title_lien_image from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['balise_title_lien_image'];
  }
	
	function tep_get_balise_title_lien_texte($category_id, $language_id) {
    $category_query = tep_db_query("select balise_title_lien_texte from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['balise_title_lien_texte'];
  }
	// FIN AJOUT POUR BALISE TITLE DANS LA LISTE DES CATEGORIES THIERRY	
	
	// AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY
	function tep_get_category_url($category_id, $language_id) {
    $category_query = tep_db_query("select categories_url from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_url'];
  }
	
	function tep_get_category_baseline($category_id, $language_id) {
    $category_query = tep_db_query("select categories_baseline from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_baseline'];
  }
	// FIN AJOUT URL ET BASELINE DANS LES CATEGORIES THIERRY		
	
  function tep_get_products_head_title_tag($product_id, $language_id) {
    $product_query = tep_db_query("select products_head_title_tag from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_head_title_tag'];
  }

  function tep_get_products_head_desc_tag($product_id, $language_id) {
    $product_query = tep_db_query("select products_head_desc_tag from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_head_desc_tag'];
  }

  function tep_get_products_head_keywords_tag($product_id, $language_id) {
    $product_query = tep_db_query("select products_head_keywords_tag from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_head_keywords_tag'];
  }
  
  function tep_get_products_baseline($product_id, $language_id) {
    $product_query = tep_db_query("select products_baseline from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_baseline'];
  }
  
  function tep_get_products_balise_title_lien_image($product_id, $language_id) {
    $product_query = tep_db_query("select balise_title_lien_image from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['balise_title_lien_image'];
  }
  
  function tep_get_products_balise_title_lien_texte($product_id, $language_id) {
    $product_query = tep_db_query("select balise_title_lien_texte from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['balise_title_lien_texte'];
  }
  
  function tep_get_manufacturer_htc_title($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_htc_title_tag from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_htc_title_tag'];
  }
    
  function tep_get_manufacturer_htc_desc($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_htc_desc_tag from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_htc_desc_tag'];
  }
   
  function tep_get_manufacturer_htc_keywords($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_htc_keywords_tag from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_htc_keywords_tag'];
  } 
   
  function tep_get_manufacturer_htc_description($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_htc_description from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_htc_description'];
  }
	
	function tep_get_manufacturer_htc_url($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_htc_url from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_htc_url'];
  }
	
	  function tep_get_manufacturer_htc_baseline($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_htc_baseline from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_htc_baseline'];
  }
	
  // HTC END


//family
function tep_get_family_htc_title($family_id, $language_id) {
    $family_query = tep_db_query("select family_htc_title_tag from " . TABLE_FAMILY_INFO . " where family_id = '" . (int)$family_id . "' and languages_id = '" . (int)$language_id . "'");
    $family = tep_db_fetch_array($family_query);

    return $family['family_htc_title_tag'];
  }
    
  function tep_get_family_htc_desc($family_id, $language_id) {
    $family_query = tep_db_query("select family_htc_desc_tag from " . TABLE_FAMILY_INFO . " where family_id = '" . (int)$family_id . "' and languages_id = '" . (int)$language_id . "'");
    $family = tep_db_fetch_array($family_query);

    return $family['family_htc_desc_tag'];
  }
   
  function tep_get_family_htc_keywords($family_id, $language_id) {
    $family_query = tep_db_query("select family_htc_keywords_tag from " . TABLE_FAMILY_INFO . " where family_id = '" . (int)$family_id . "' and languages_id = '" . (int)$language_id . "'");
    $family = tep_db_fetch_array($family_query);

    return $family['family_htc_keywords_tag'];
  } 
   
  function tep_get_family_htc_description($family_id, $language_id) {
    $family_query = tep_db_query("select family_htc_description from " . TABLE_FAMILY_INFO . " where family_id = '" . (int)$family_id . "' and languages_id = '" . (int)$language_id . "'");
    $family = tep_db_fetch_array($family_query);

    return $family['family_htc_description'];
  }
//family

//family Client
function tep_get_family_client_htc_title($family_client_id, $language_id) {
    $family_client_query = tep_db_query("select family_client_htc_title_tag from " . TABLE_CLIENT_FAMILY_INFO . " where family_client_id = '" . (int)$family_client_id . "' and languages_id = '" . (int)$language_id . "'");
    $family_client = tep_db_fetch_array($family_client_query);

    return $family_client['family_client_htc_title_tag'];
  }
    
  function tep_get_family_client_htc_desc($family_client_id, $language_id) {
    $family_client_query = tep_db_query("select family_client_htc_desc_tag from " . TABLE_CLIENT_FAMILY_INFO . " where family_client_id = '" . (int)$family_id . "' and languages_id = '" . (int)$language_id . "'");
    $family_client = tep_db_fetch_array($family_client_query);

    return $family_client['family_client_htc_desc_tag'];
  }
   
  function tep_get_family_client_htc_keywords($family_client_id, $language_id) {
    $family_client_query = tep_db_query("select family_client_htc_keywords_tag from " . TABLE_CLIENT_FAMILY_INFO . " where family_client_id = '" . (int)$family_client_id . "' and languages_id = '" . (int)$language_id . "'");
    $family_client = tep_db_fetch_array($family_client_query);

    return $family_client['family_client_htc_keywords_tag'];
  } 
   
  function tep_get_family_client_htc_description($family_client_id, $language_id) {
    $family_client_query = tep_db_query("select family_client_htc_description from " . TABLE_CLIENT_FAMILY_INFO . " where family_client_id = '" . (int)$family_client_id . "' and languages_id = '" . (int)$language_id . "'");
    $family_client = tep_db_fetch_array($family_client_query);

    return $family_client['family_client_htc_description'];
  }
//family Client


  // WHOS ONLINE
  function tep_get_ip_address() {
    if (isset($_SERVER)) {
      if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
      } else {
        $ip = $_SERVER['REMOTE_ADDR'];
      }
    } else {
      if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
      } elseif (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
      } else {
        $ip = getenv('REMOTE_ADDR');
      }
    }

    return $ip;
  }
  // WHOS ONLINE
  
  
////
// Return a product's special price (returns nothing if there is no offer)
// TABLES: products
  function tep_get_products_special_price($product_id) {
    $product_query = tep_db_query("select specials_new_products_price from " . TABLE_SPECIALS . "  where products_id = '" . $product_id . "'");
    $product = tep_db_fetch_array($product_query);
  
    return $product['specials_new_products_price'];
  }
  
	function convert_price($value) {
		$decimal=explode(".", $value);
		$val = ($decimal[1]); //echo $val[1] .' lol';
		
		if($val=="") $val="00";
		elseif( ($val<10) && ($val[0]>0) )
		$val=$val."0"; 
		
		$result=($decimal[0] .".". $val);			
		return $result;
	}

function vider($champ){
		if ($champ==0)
			$champ="";
		return $champ;
	}
	
	//// sort order ////
// Sets the sort order of a product
  function tep_set_product_sort_order($products_id, $sort_order) {
    return tep_db_query("update " . TABLE_PRODUCTS . " set products_sort_order = '" . $sort_order . "', products_last_modified = now() where products_id = '" . (int)$products_id . "'");
  }
  function tep_set_categorie_sort_order($categories_id, $sort_order) {
    return tep_db_query("update " . TABLE_CATEGORIES . " set sort_order = '" . $sort_order . "', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
  }
	
// AJOUT PERMETTANT D'AFFICHER LES REQUETES DANS LE CATALOG	
	function print_array ($array, $exit = false) {
		print "<pre>";
		print_r ($array);
		print "</pre>";
		if ($exit) exit();
	}

	function nettoie_tel($string){
		$string=str_replace(".","",$string);
		$string=str_replace(' ',"",$string);
		$string=str_replace("-","",$string);
		$string=str_replace("_","",$string);
		$string=str_replace("/","",$string);
		$string=str_replace('"','',$string);
		$string=str_replace('{','',$string);
		$string=str_replace('}','',$string);
		$string=str_replace('(','',$string);
		$string=str_replace(')','',$string);
		$string=str_replace('[','',$string);
		$string=str_replace(']','',$string);
		$string=str_replace('o','0',$string);
		$string=str_replace('O','0',$string);
		return $string;
	}
	
	function resize_image($fichierSource,$largeurDestination,$hauteurDestination,$destination){
		$im = imagecreatetruecolor ($largeurDestination, $hauteurDestination) or die ("Erreur lors de la cr�ation de l'image"); 
		
		$file=DIR_FS_PRODUCTS_IMAGES."ori/".$fichierSource;
		list($width, $height, $image_type) = getimagesize($file);
		switch ($image_type){
			case 1: $source = imagecreatefromgif($file); break;
			case 2: $source = imagecreatefromjpeg($file);  break;
			case 3: $source = imagecreatefrompng($file); break;
			default: return '';  break;
		}
		
		$largeurSource = imagesx($source);
		$hauteurSource = imagesy($source); 
		imagecopyresampled($im, $source, 0, 0, 0, 0, $largeurDestination, $hauteurDestination, $largeurSource, $hauteurSource);
		
		if($largeurDestination>300){
			if($largeurDestination<500){
				$watermark = imagecreatefrompng(DIR_FS_PRODUCTS_IMAGES."watermark_p.png");  
			}
			else{
				$watermark = imagecreatefrompng(DIR_FS_PRODUCTS_IMAGES."watermark_g.png");  
			}
			imagealphablending($im, TRUE);
            imagecopy($im, $watermark, 0, 0, 0, 0, $largeurDestination, $hauteurDestination);
		}
		
		$miniature = DIR_FS_PRODUCTS_IMAGES.$destination;
		switch ($image_type){
			case 1: imagegif ($im, $miniature);  break;
			case 2: imagejpeg ($im, $miniature);  break;
			case 3: imagepng ($im, $miniature); break;
			default: return '';  break;
		}
	}
	
	// Fonction pour changement de watermark dans dossier "products_pictures_news"
	function resize_image_watermark($fichierSource,$largeurDestination,$hauteurDestination,$destination){
		$im = imagecreatetruecolor ($largeurDestination, $hauteurDestination) or die ("Erreur lors de la cr�ation de l'image"); 
		
		$file="/var/www/vhosts/generalarmystore.fr/httpdocs/gas/images/products_pictures_news/ori/".$fichierSource;
		list($width, $height, $image_type) = getimagesize($file);
		switch ($image_type){
			case 1: $source = imagecreatefromgif($file); break;
			case 2: $source = imagecreatefromjpeg($file);  break;
			case 3: $source = imagecreatefrompng($file); break;
			default: return '';  break;
		}
		
		$largeurSource = imagesx($source);
		$hauteurSource = imagesy($source); 
		imagecopyresampled($im, $source, 0, 0, 0, 0, $largeurDestination, $hauteurDestination, $largeurSource, $hauteurSource);
		
		if($largeurDestination>300){
			if($largeurDestination<500){
				$watermark = imagecreatefrompng(DIR_FS_PRODUCTS_IMAGES."watermark_p.png");  
			}
			else{
				$watermark = imagecreatefrompng(DIR_FS_PRODUCTS_IMAGES."watermark_g.png");  
			}
			imagealphablending($im, TRUE);
            imagecopy($im, $watermark, 0, 0, 0, 0, $largeurDestination, $hauteurDestination);
		}
		
		$miniature = "/var/www/vhosts/generalarmystore.fr/httpdocs/gas/images/products_pictures_news/".$destination;
		switch ($image_type){
			case 1: imagegif ($im, $miniature);  break;
			case 2: imagejpeg ($im, $miniature);  break;
			case 3: imagepng ($im, $miniature); break;
			default: return '';  break;
		}
	}
	
	// Redimensionnement d'images pour l'ancien site
	function resize_image_MS($fichierSource,$largeurDestination,$hauteurDestination,$destination){
		$im = imagecreatetruecolor ($largeurDestination, $hauteurDestination) or die ("Erreur lors de la cr�ation de l'image"); 
		
		$file=DIR_FS_CATALOG_IMAGES.$fichierSource;
		list($width, $height, $image_type) = getimagesize($file);
		switch ($image_type){
			case 1: $source = imagecreatefromgif($file); break;
			case 2: $source = imagecreatefromjpeg($file);  break;
			case 3: $source = imagecreatefrompng($file); break;
			default: return '';  break;
		}
		
		$largeurSource = imagesx($source);
		$hauteurSource = imagesy($source); 
		imagecopyresampled($im, $source, 0, 0, 0, 0, $largeurDestination, $hauteurDestination, $largeurSource, $hauteurSource);
		
		$miniature = DIR_FS_CATALOG_IMAGES.$destination;
		switch ($image_type){
			case 1: imagegif ($im, $miniature);  break;
			case 2: imagejpeg ($im, $miniature);  break;
			case 3: imagepng ($im, $miniature); break;
			default: return '';  break;
		}
	}
	
	function get_tva_country($country){
		if(substr_count($country,"France")>0){
			return "FR";
		}
		else if(substr_count(strtoupper($country),strtoupper("Allemagne"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Autriche"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Belgique"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Danemark"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Espagne"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Finlande"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Grande Bretagne"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Guadeloupe"))>0){
			return "DOM";
		}
		else if(substr_count(strtoupper($country),strtoupper("Guyane Francaise"))>0){
			return "DOM";
		}
		else if(substr_count(strtoupper($country),strtoupper("Irlande"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Italie"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Luxembourg"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Martinique"))>0){
			return "DOM";
		}
		else if(substr_count(strtoupper($country),strtoupper("Mayotte"))>0){
			return "TOM";
		}
		else if(substr_count(strtoupper($country),strtoupper("Norv�ge"))>0){
			return "AUTRE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Nouvelle Cal�donie"))>0){
			return "TOM";
		}
		else if(substr_count(strtoupper($country),strtoupper("Pays-bas"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Polyn�sie Fran�aise"))>0){
			return "TOM";
		}
		else if(substr_count(strtoupper($country),strtoupper("Portugal"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("R�union"))>0){
			return "DOM";
		}
		else if(substr_count(strtoupper($country),strtoupper("Saint Pierre et Miquelon"))>0){
			return "TOM";
		}
		else if(substr_count(strtoupper($country),strtoupper("Su�de"))>0){
			return "CEE";
		}
		else if(substr_count(strtoupper($country),strtoupper("Suisse"))>0){
			return "AUTRE";
		}
		else if(substr_count($country,"Wallis et Futuna")>0){
			return "TOM";
		}							
	}
	
	function get_drapeau($pays){
		if(substr_count($pays,"France")>0){
			return "<img src='images/icons/drapeaux/fr.gif' title='France' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Allemagne"))>0){
			return "<img src='images/icons/drapeaux/de.gif' title='Allemagne' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Autriche"))>0){
			return "<img src='images/icons/drapeaux/at.gif' title='Autriche' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Belgique"))>0){
			return "<img src='images/icons/drapeaux/be.gif' title='Belgique' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Danemark"))>0){
			return "<img src='images/icons/drapeaux/dk.gif' title='Danemark' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Espagne"))>0){
			return "<img src='images/icons/drapeaux/es.gif' title='Espagne' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Finlande"))>0){
			return "<img src='images/icons/drapeaux/fi.gif' title='Finlande' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Grande Bretagne"))>0){
			return "<img src='images/icons/drapeaux/gb.gif' title='Grande Bretagne' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Guadeloupe"))>0){
			return "<img src='images/icons/drapeaux/dom.gif' title='Guadeloupe' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Guyane Francaise"))>0){
			return "<img src='images/icons/drapeaux/dom.gif' title='Guyane Francaise' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Irlande"))>0){
			return "<img src='images/icons/drapeaux/ie.gif' title='Irlande' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Italie"))>0){
			return "<img src='images/icons/drapeaux/it.gif' title='Italie' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Luxembourg"))>0){
			return "<img src='images/icons/drapeaux/lu.gif' title='Luxembourg' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Martinique"))>0){
			return "<img src='images/icons/drapeaux/dom.gif' title='Martinique' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Mayotte"))>0){
			return "<img src='images/icons/drapeaux/tom.gif' title='Mayotte' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Norv�ge"))>0){
			return "<img src='images/icons/drapeaux/no.gif' title='Norv�ge' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Nouvelle Cal�donie"))>0){
			return "<img src='images/icons/drapeaux/tom.gif' title='Nouvelle Cal�donie' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Pays-bas"))>0){
			return "<img src='images/icons/drapeaux/nl.gif' title='Pays-bas' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Polyn�sie Fran�aise"))>0){
			return "<img src='images/icons/drapeaux/tom.gif' title='Polyn�sie Fran�aise' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Portugal"))>0){
			return "<img src='images/icons/drapeaux/pt.gif' title='Portugal' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("R�union"))>0){
			return "<img src='images/icons/drapeaux/dom.gif' title='R�union' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Saint Pierre et Miquelon"))>0){
			return "<img src='images/icons/drapeaux/tom.gif' title='Saint Pierre et Miquelon' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Su�de"))>0){
			return "<img src='images/icons/drapeaux/se.gif' title='Su�de' border='0'>";
		}
		else if(substr_count(strtoupper($pays),strtoupper("Suisse"))>0){
			return "<img src='images/icons/drapeaux/ch.gif' title='Suisse' border='0'>";
		}
		else if(substr_count($pays,"Wallis et Futuna")>0){
			return "<img src='images/icons/drapeaux/tom.gif' title='Wallis et Futuna' border='0'>";
		}
		else{
			return "<img src='images/icons/drapeaux/autre.gif' title='Autre' border='0'>";
		}
	}

// Retire les accents d'une chaine de caract�re 
function strip_accents($string){
	return strtr($string,'���������������������������������������������������',
						 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}
	
// Retire tout les caract�res sp�ciaux d'une url 
function encode_url($url){
	$url=str_replace("�","",$url);
	$url=str_replace('%',"",$url);
	$url=str_replace("#","",$url);
	$url=str_replace("?","",$url);
	$url=str_replace("�","",$url);
	$url=str_replace("�","2",$url);
	//$url=str_replace("/","",$url);
	$url=str_replace(" ","-",$url);
	$url=str_replace("'","_",$url);
	$url=str_replace("`","_",$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('^','',$url);
	$url=str_replace('"','',$url);
	$url=str_replace('{','',$url);
	$url=str_replace('}','',$url);
	$url=str_replace('[','',$url);
	$url=str_replace(']','',$url);
	$url=str_replace('<','',$url);
	$url=str_replace('>','',$url);
	$url=str_replace("__","_",$url);
	return strip_accents($url);
}

// Retire tout les caract�res sp�ciaux d'une url (by Thierry)
function encode_url_sitemap_cat($url){
	$url=str_replace("�","",$url);
	$url=str_replace('%',"",$url);
	$url=str_replace("#","",$url);
	$url=str_replace("?","",$url);
	$url=str_replace("�","",$url);
	$url=str_replace("�","2",$url);
	$url=str_replace(" ","-",$url);
	$url=str_replace("'","_",$url);
	$url=str_replace("`","_",$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('^','',$url);
	$url=str_replace('"','',$url);
	$url=str_replace('{','',$url);
	$url=str_replace('}','',$url);
	$url=str_replace('[','',$url);
	$url=str_replace(']','',$url);
	$url=str_replace('<','',$url);
	$url=str_replace('>','',$url);
	//$url=str_replace('.','',$url);
	$url=str_replace("(","",$url);
	$url=str_replace(")","",$url);
	$url=str_replace(",","-",$url);
	$url=str_replace("__","_",$url);
	return strip_accents($url);
}

// Retire tout les caract�res sp�ciaux d'une url (by Thierry)
function encode_url_sitemap_prod($url){
	$url=str_replace("�","",$url);
	$url=str_replace('%',"",$url);
	$url=str_replace("#","",$url);
	$url=str_replace("?","",$url);
	$url=str_replace("�","",$url);
	$url=str_replace("�","2",$url);
	$url=str_replace(" ","-",$url);
	$url=str_replace("'","_",$url);
	$url=str_replace("`","_",$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('^','',$url);
	$url=str_replace('"','',$url);
	$url=str_replace('{','',$url);
	$url=str_replace('}','',$url);
	$url=str_replace('[','',$url); 
	$url=str_replace(']','',$url);
	$url=str_replace('<','',$url);
	$url=str_replace('>','',$url);
	$url=str_replace(':','',$url);
	$url=str_replace('=','',$url);
	$url=str_replace('.','',$url);
	$url=str_replace("(","",$url);
	$url=str_replace(")","",$url);
	$url=str_replace(",","-",$url);
	$url=str_replace("__","-",$url);
	$url=str_replace("_","-",$url);
	$url=str_replace('--','-',$url);
	$url=str_replace('+-','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('/','',$url);
	return strip_accents($url);
}

// Retire tout les caract�res sp�ciaux dans le WEB SERVICE 
function retirer_les_caracteres_speciaux($url){
	$url=str_replace("�","",$url);
	$url=str_replace('%',"",$url);
	$url=str_replace("#","",$url);
	$url=str_replace("?","",$url);
	$url=str_replace("�","",$url);
	$url=str_replace("�","",$url);
	$url=str_replace("+","",$url);
	$url=str_replace("=","",$url);
	$url=str_replace("'"," ",$url);
	$url=str_replace("�"," ",$url);
	$url=str_replace("`"," ",$url);
	$url=str_replace("�"," ",$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('^','',$url);
	$url=str_replace('"',' ',$url);
	$url=str_replace('{','',$url);
	$url=str_replace('}','',$url);
	$url=str_replace('[','',$url);
	$url=str_replace(']','',$url);
	$url=str_replace('<','',$url);
	$url=str_replace('>','',$url);
	$url=str_replace(':','',$url);
	$url=str_replace('=','',$url);
	$url=str_replace('.','',$url);
	$url=str_replace("(","",$url);
	$url=str_replace(")","",$url);
	$url=str_replace(",","",$url);
	$url=str_replace("-"," ",$url);
	$url=str_replace("__"," ",$url);
	$url=str_replace("_"," ",$url);
	$url=str_replace('--',' ',$url);
	$url=str_replace('+-','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('/',' ',$url);
	return strip_accents($url);
}

// Retire tout les caract�res sp�ciaux dans le WEB SERVICE 
function format_sitemap($url){
	$url=str_replace("�","",$url);
	$url=str_replace('%',"",$url);
	$url=str_replace("#","",$url);
	$url=str_replace("?","",$url);
	$url=str_replace("�","",$url);
	$url=str_replace("�","",$url);
	$url=str_replace("+","",$url);
	$url=str_replace("=","",$url);
	$url=str_replace("'","",$url);
	$url=str_replace("�","",$url);
	$url=str_replace("`","",$url);
	$url=str_replace("�","",$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('^','',$url);
	$url=str_replace('"','',$url);
	$url=str_replace('{','',$url);
	$url=str_replace('}','',$url);
	$url=str_replace('[','',$url);
	$url=str_replace(']','',$url);
	$url=str_replace('<','',$url);
	$url=str_replace('>','',$url);
	$url=str_replace(':','',$url);
	$url=str_replace('=','',$url);
	$url=str_replace('.','',$url);
	$url=str_replace("(","",$url);
	$url=str_replace(")","",$url);
	$url=str_replace(",","",$url);
	$url=str_replace("-","",$url);
	$url=str_replace("__","",$url);
	$url=str_replace("_","",$url);
	$url=str_replace('--','',$url);
	$url=str_replace('+-','',$url);
	$url=str_replace('�','',$url);
	$url=str_replace('/','',$url);
	$url=str_replace('&','',$url);
	return strip_accents($url);
}

// Cr�e une URL en fonction des param�tres envoy�s 
function url($type,$args){
	switch($type) {
		case "rayon" :
			$url = $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . ".html";
			break;
		case "marque" :
			$url = $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . "/" . $args["id_marque"] . "-" . str_replace("/","",$args["url_marque"]) . ".html";
			break;
		case "modele" :
			$url = $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . "/" . $args["id_marque"] . "-" . str_replace("/","",$args["url_marque"]) . "/" . $args["id_modele"] . "-" . str_replace("/","",$args["url_modele"]) . ".html";
			break;
		case "modele_famille" :
			$url = $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . "/" . $args["id_marque"] . "-" . str_replace("/","",$args["url_marque"]) . "/" . $args["id_modele"] . "-" . $args["id_famille"] . "-" . str_replace("/","",$args["url_famille"]) . "-" . str_replace("/","",$args["url_modele"]) . ".html";
			break;
		case "article" :
			$url = $args["id_rayon"] . "-" . str_replace("/","",$args["nom_rayon"]) . "/" . $args["id_marque"] . "-" . str_replace("/","",$args["url_marque"]) . "/" . $args["id_modele"] . "-" . str_replace("/","",$args["url_modele"]) . "/" . $args["id_article"] . "-" . str_replace("/","",$args["url_article"]) . ".html";
			break;
		case "erreur" :
			$url = "/url_error.php";
			break;
	}

	return encode_url($url);
}

function correction_tva($tva_exacte) {
	$tva_arrondie = format_to_money($tva_exacte);
	
	if (substr($tva_exacte, strpos($tva_exacte, '.')+3, 1) < 5) {
		$tva_arrondie += 0.01;
	}
	
	$tva = array('arrondie' => format_to_money($tva_arrondie), 'exacte' => $tva_exacte);
	
	return $tva;
}

function format_to_money($prix) {
	$prix = round($prix, 2);
	
	if (!strpos($prix, '.')) {
		$nb_decimales = 0;
	} else {
		$nb_decimales = strlen($prix) - (strpos($prix, '.')+1);
	}
	
	if ($nb_decimales == 1) {
		$prix .= '0';
	} else if ($nb_decimales == 0) {
		$prix .= '.00';
	}
	
	return $prix;
}

/* */
function get_set($table, $column) {
	
    $sql = "SHOW COLUMNS FROM $table LIKE '". $column ."'";
    if (!($ret = mysql_query($sql))) die("Error: Could not show columns". $column ." from table ". $table);

    $line = mysql_fetch_assoc($ret);
    $set  = $line['Type'];
    $set  = substr($set,5,strlen($set)-7); // Remove "set(" at start and ");" at end
    return preg_split("/','/",$set); // Split into and array
}

function set_session_for_customer($customer) {
	
	$panier_query = tep_db_query("SELECT customers_basket_id, products_id, customers_basket_quantity, id_rayon, final_price
												  FROM " . TABLE_CUSTOMERS_BASKET . "
												  WHERE customers_id = '" . $customer . "' and pack_id = ''");
	
	$i = 0;
	$nb_articles = tep_db_num_rows($panier_query);
	
	if ($nb_articles > 0) {
		
		while($panier_data = tep_db_fetch_array($panier_query)){
			$id_complet = $panier_data['products_id'];
			
			
			//on recupere l'id produit
			$articles_id = explode("_",$id_complet);
			$id_article = $articles_id[0];
			$id_categorie = $articles_id[1];
			$mod = explode("{",$articles_id[2]);
			
			$produit_info = tep_db_query("select * 
									   from ". TABLE_PRODUCTS ." p, ". TABLE_PRODUCTS_DESCRIPTION ." pd 
									   where p.products_id=pd.products_id and p.products_id='". $id_article ."'");
			$res_produit = tep_db_fetch_array($produit_info);
			
			$req_cat_name = tep_db_query("select categories_name, categories_id from categories_description where categories_id='". $mod[0] ."'");
			$res_cat_name = tep_db_fetch_array($req_cat_name);
		
			// On r�cup�re les infos sur les options
			$article_options = explode("{", $panier_data['products_id']);
			$options = explode("}", $article_options[1]);
			
			// On r�cup�re les infos sur l'article
			$article = explode("_", $article_options[0]);
			
			// On r�cup�re des infos compl�mentaires
			$articles_sous_total_HT += $article_details['prix_ht']*$quantite." - ";
			
			////////////////////
			$group_query = tep_db_query("select customers_type
									   from customers
									   where customers_id ='". $customer ."'");
			$group = tep_db_fetch_array($group_query);
			
			$pre = 'part';
			switch($group['customers_type'])
			{
				case 1: $pre ='part';
				break;
				case 2: $pre = 'pro';
				break;
				case 3: $pre = 'rev';
				break;
				case 4: $pre = 'adm';
				break;
			}
			
			
			$by_quantity_query = tep_db_query("select ".$pre."_price_by_1 
									   from products_price_by_quantity
									   where products_id ='". $id_article ."'");
			$by_quantity = tep_db_fetch_array($by_quantity_query);
			$products_price = $by_quantity[$pre.'_price_by_1'];
			//products_price_by_quantity ::  products_id 	part_price_by_1 	pro_price_by_1 	rev_price_by_1 	adm_price_by_1
			/////////////////////
			$_SESSION['panier_admin']['products_id'][$i] = $id_article;
			$_SESSION['panier_admin']['option_id'][$i] = ($options[0]!='') ? $options[0] : 0;
			$_SESSION['panier_admin']['option_values_id'][$i] = ($options[0]!='') ? $options[1] : 0;
			$_SESSION['panier_admin']['qte'][$i] = $panier_data['customers_basket_quantity'];
			$_SESSION['panier_admin']['products_price'][$i] = $products_price;//$produit_info['products_price']; // $panier_data['final_price'];
			$_SESSION['panier_admin']['cPath'][$i] = $panier_data['id_rayon'] .'_'. $id_categorie .'_'. $mod[0];
			
			$i++;
		}
	}
}

function printr($array) {
	
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

function roundNumber($number, $precision) {
	
	$number = round($number, $precision);
	if(strpos($number, '.')>0) {
		
		$temp = explode('.', $number);
	
		return (strlen($temp[1])<$precision) ? $number .'0' : $number;
	}
	else return $number;
}

function estDevis($order) {
	
	$query = 'SELECT * FROM '. TABLE_ORDERS_STATUS_HISTORY .' WHERE orders_id='. $order .' order by date_added';
	$query = tep_db_query($query);
	
	$i = 0;	
	$devis = false;
	
	while($data = tep_db_fetch_array($query)) {
		
		// si le dernier statut de la commande est "Cr�ation de votre Devis" alors $devis = true; 
		if ($i==0 && $data['orders_status_id']==21) { 
			$devis = true;
		// si le dernier statut de la commande est "Commande Annul�e" alors $devis = true;
		//} elseif ($i==1 && $data['orders_status_id']==11) {
		//	$devis *= true;
		} elseif ($i==1) {
			$devis = false;
		}
		$i++;
	}
	
	return $devis;
}


// Pour le webservice EB on remplace le champs "product_manage_stock" par l'information de prestashop "out_of_stock" 
function product_manage_stock($type){
	switch($type) {
		case "tout" :
			$out_of_stock = 1;
			break;
		case "sur_cmd" :
			$out_of_stock = 1;
			break;
		case "destock_visible" :
			$out_of_stock = 0;
			break;
		case "destock_cache" :
			$out_of_stock = 0;
			break;
		case "destock_cache_qte" :
			$out_of_stock = 0;
			break;
	}
	return $out_of_stock;
}


?>