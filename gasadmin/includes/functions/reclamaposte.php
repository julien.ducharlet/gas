<?php  /*
  $Id: reclamation_poste.php, v 2.2 24/01/2009 delete / phocea(forum oscommerce-fr.info) $

  This script is not included in the original version of osCommerce

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce
*/
include(DIR_WS_INCLUDES . 'reclamaposte.php');
include(DIR_WS_FUNCTIONS . 'reclamaposte_ocr.php');
require_once(DIR_WS_CLASSES . 'phpMailer/class.phpmailer.php');

if(!function_exists('tep_mb_strtoupper')) 
{
  function tep_mb_strtoupper($input)
  {
          if ( is_array($input) )
          {
              foreach ($input as $key => $val)
              {
                  if ( ! tep_not_null($input[$key]) ) continue;
  
                  $input[$key] = mb_strtoupper($input[$key]);
              }
              return $input;
          }
          else
          {
                  return mb_strtoupper($input);
          }
  }
} 
if(!function_exists('file_put_contents')) 
{
    function file_put_contents($filename, $data, $file_append = false) {
        $fp = fopen($filename, (!$file_append ? 'w+' : 'a+'));
        if(!$fp) {
            trigger_error('file_put_contents - erreur �criture dans.', E_USER_ERROR);
            return;
        }
        fputs($fp, $data);
        fclose($fp);
    }
}
function curl_file_get_contents($URL, $header = 0)
{
	global $ch, $cookie_filename ;

	$agent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)" ;
	curl_setopt($ch, CURLOPT_USERAGENT, $agent);
	curl_setopt($ch, CURLOPT_HEADER, $header);
	curl_setopt($ch, CURLOPT_URL, $URL) ;
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_COOKIE, 1);
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_filename) ;
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_filename) ;
	$data = curl_exec($ch);

	return $data ;
}

function grab_coliposte($noColis) {
	global $ch, $main_url, $url_params;
	$ch = curl_init();
	$status_page =  curl_file_get_contents($main_url . $url_params . $noColis);

	return $status_page;
	
}

function grab_coliposte_statuses($noColis, $firstOnly = false) {
	global $status, $status_page, $main_url;
	
	$status_page =  grab_coliposte($noColis);
	// Statut de la livraison
	$count = preg_match_all('#<img src="(/gp/servlet\?id=' . $noColis . '_desc_[0-9].*?)"#', $status_page, $result);

	if (!$count)
	{
		if ( preg_match_all('#Aucun colis ne correspond#', $status_page, $result) )
			return(-2) ;
		else return -1;
	}

	// We retrieve all statuses displayed on the tracking page
	$statuses_found= array();
	for($x = 0; $x < sizeof( $result[1]); $x++) {
		if ( ! $img = @curl_file_get_contents($main_url . $result[1][$x], 0, 1) ) {
			return -1;
		} else {
			$statuses_found[]=md5($img);
			if ($firstOnly) break;
		}
  }
  return $statuses_found;
} 

function grab_coliposte_date($noColis, $last_status) {
	global $ch, $main_url, $status_page;
	
	preg_match_all('#<img src="(/gp/servlet\?id=' . $noColis . '_date_[0-9].*?)"#', $status_page, $result);
	if (!$date = @curl_file_get_contents($main_url . $result[1][$last_status], 0, 1) ) return -1;

	return $date;
}

function interpret_date() {
		global $tmpfname, $tmp_dir;
		// Processus OCR sur l'image 'date' ;
		if ( ! $img = @imagecreatefrompng($tmpfname) ) return(-1) ;

		// Masquage des slashes sur l'image
		$yellow = imagecolorallocate($img, 255, 234, 153);
		imagefilledrectangle($img, 12, 0, 14, 12, $yellow);
		imagefilledrectangle($img, 26, 0, 29, 12, $yellow);

		// Fichier de sortie temporaire
		$comparefile = tempnam($tmp_dir, "laposte");
		if ( ! @imagepng($img, $comparefile) ) return(-1) ;

		/* PHP OCR */
		//This is the main function. Format of the output array is $retmas[$line_number][$letter_number][$type]
		//where $type is 0 for digit and 1 for relative closeness
		$retmas = parse_image($comparefile, DIR_WS_FUNCTIONS . 'reclamaposte_matrix.php');
		//
		$date = print_output_text($retmas,$tmpfname, 0) ;
		unlink($comparefile) ;
		unlink($tmpfname) ;
		
		$d = substr($date, 0, 2) ;
		$m = substr($date, 2, 2) ;
		$y = substr($date, 4, 2) ;

		$date_livraison  = strtotime(sprintf('20%02d-%02d-%02d', $y, $m, $d));
		
		return $date_livraison;
}

function merci_la_poste($noColis, $statuses_found) {
	global $status, $status_page, $ch, $tmp_dir, $tmpfname, $main_url, $url_params;
	
	$last_status = 0;
	// now we treat all statuses found
	if (is_array($statuses_found) && sizeof($statuses_found) >= 1) {
		for($y = 0; $y < sizeof( $statuses_found); $y++) {
			// remboursable ou pas?
			if (${status}[$statuses_found[$y]]['remboursable'] == 'false') {
				$case = 9;
				break;
			}
			// encore en transit
			if (${status}[$statuses_found[$y]]['initial_transit'] == 'true') {
				$case = 5;
				break;
			}
			if (${status}[$statuses_found[$y]]['initial_transit'] == 'false') {
				$last_status = $y;
				$case = 0;
			}
		}
//	} elseif (is_array($statuses_found) && sizeof($statuses_found) < 1) {
//		$case = 1;
//	} elseif ( $statuses_found == -2 ) {// Pas encore d'info
//			$case = -2 ;
//	} else {
//		$case = -1;
	}
          
	switch( $case ) {
		// Cas dans lesquels on a trouv� une date de pr�sentation
		case 0 :
			// La date
			$date = grab_coliposte_date($noColis, $last_status);
			$tmpfname = tempnam($tmp_dir, "laposte");
			file_put_contents($tmpfname, $date);

			curl_close($ch);
			return(1);
			break;
		// Cas dans lesquels on ne reconnait pas les informations trouv�s (md5 manquant)
//		case 1 :
//			curl_close($ch);
//			return(10);
		// Cas dans lesquels la commande est encore en transit
		case 5 :
			// La date
			$date = grab_coliposte_date($noColis, $last_status);
			$tmpfname = tempnam($tmp_dir, "laposte");
			file_put_contents($tmpfname, $date);
			curl_close($ch);
			return(5);
		// Cas dans lesquels la commande n'est pas remboursable
		case 9 :
			// La date
			$date = grab_coliposte_date($noColis, $last_status);
			$tmpfname = tempnam($tmp_dir, "laposte");
			file_put_contents($tmpfname, $date);

			curl_close($ch);
			return(9);
		// Cas dans lesquels on a pas encore d'info
		default :
			curl_close($ch);
			return($case);
	}
}

function getImageforStatus($laposte_status) {
	global $etat;
	
	  switch( $laposte_status) {
	  	// Le colis est en retard, r�clamation pas encore faite
	  	case COLIPOSTE_LIVRAISON_RETARD:
	  		$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_LIVRAISON_RETARD']['icon'], $etat['COLIPOSTE_LIVRAISON_RETARD']['description']);
				break;
	  	// Le colis est arriv�e � temps
	  	case COLIPOSTE_LIVRAISON_OK :
	  		$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_LIVRAISON_OK']['icon'], $etat['COLIPOSTE_LIVRAISON_OK']['description']); 
				break;
	  	// Le colis est encore en transit      	  	
	  	case COLIPOSTE_LIVRAISON_TRANSIT :
	  		$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_LIVRAISON_TRANSIT']['icon'], $etat['COLIPOSTE_LIVRAISON_TRANSIT']['description']); 
				break;
			// La demande de remboursement a �t� faite
	  	case COLIPOSTE_RECLAMATION_DEMANDE :
	  		$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_RECLAMATION_DEMANDE']['icon'], $etat['COLIPOSTE_RECLAMATION_DEMANDE']['description']); 
				break;
			// Le remboursement a �t� fait par La Poste
	  	case COLIPOSTE_RECLAMATION_REMBOURSE :
	  		$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_RECLAMATION_REMBOURSE']['icon'], $etat['COLIPOSTE_RECLAMATION_REMBOURSE']['description']); 
				break;
			case COLIPOSTE_NO_INFORMATION:
	  		$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_NO_INFORMATION']['icon'], $etat['COLIPOSTE_NO_INFORMATION']['description']); 
				break;
	  	case COLIPOSTE_ARCHIVE :
	  		$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_ARCHIVE']['icon'], $etat['COLIPOSTE_ARCHIVE']['description']); 
				break;
	  	case COLIPOSTE_REMBOURSEMENT_IMPOSSIBLE:
	  	$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_REMBOURSEMENT_IMPOSSIBLE']['icon'], $etat['COLIPOSTE_REMBOURSEMENT_IMPOSSIBLE']['description']); 
				break;
	  	// Pas d'info ou image non trouv�
	  	case COLIPOSTE_ERREUR_MD5 :
	  		$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_ERREUR_MD5']['icon'], $etat['COLIPOSTE_ERREUR_MD5']['description']); 
				break;
	  	// Colis pas encore v�rifi�
	  	default:
	  		$image_status = tep_image(DIR_WS_ICONS . $etat['COLIPOSTE_NONE']['icon'], $etat['COLIPOSTE_NONE']['description']); 
				break;
	  }
	  
	  return $image_status;
}

  // Added Phocea. New wrapper around email class to handle string attachments. Allows emailing of pdf to customers
  // Additional parameters:
  // $to_name           The name of the recipient, e.g. "Jan Wildeboer"
  // $to_email_address  The eMail address of the recipient,
  //                    e.g. jan.wildeboer@gmx.de
  // $email_subject     The subject of the eMail
  // $email_text        The text of the eMail, may contain HTML entities
  // $from_email_name   The name of the sender, e.g. Shop Administration
  // $from_email_adress The eMail address of the sender,
  //                    e.g. info@mytepshop.com
  // $string = string data (ascii or binary)
  // $filename = target filename for attached data
  // $filetype = filetype for attached data (application/pdf, application/x-msexcel...)
  
  function tep_mail_attachment($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address, $attachment_file, $attachment_filename, $attachment_filetype='application/octet-stream') {
  	if (SEND_EMAILS != 'true') return false;
  	// Instantiate a new mail object
  	
  	$mail = new PHPMailer();
  	
  	if (EMAIL_TRANSPORT == 'smtp') {
  		$mail->IsSMTP(); // telling the class to use SMTP
  		$mail->Host = SMTP_MAIL_SERVER; // SMTP server
  		if( !tep_not_null($from_email_address) ) {
  			$from_email_address = SMTP_SENDMAIL_FROM;
  		}
  		if( !tep_not_null($from_email_name) ) {
  			$from_email_name = SMTP_FROMEMAIL_NAME;
  		}  		
  		//$mail->SMTPAuth = true;
      //$mail->Username = SMTP_USERNAME;
      //$mail->Password = SMTP_PASSWORD;
  	}
  	
  	$mail->FromName = $from_email_name;
  	$mail->From = $from_email_address;

  	$mail->AddAddress($to_email_address, $to_name);
  	$mail->Subject = $email_subject;

  	// Build the text version
  	$text = strip_tags($email_text);
  	if (EMAIL_USE_HTML == 'true') {
  		$mail->Body = tep_convert_linefeeds(array("\r\n", "\n", "\r"), '<br>', $email_text);
  		$mail->AltBody = $text;
  		$mail->IsHTML(true);
  	} else {
  		$mail->Body = $text;
  		$mail->IsHTML(false);
  	}
    
    // Now add attachment
    if(file_exists($attachment_file)) {
    	$mail->AddAttachment($attachment_file, $attachment_filename, 'base64', $attachment_filetype);
    }

  	// Send message
  	if(!$mail->Send()){
  		return false;
  	}
  }
?>