<?php 
/*
  Fonction : spplus_pays_iso.php
*/

// permet d'afficher le pays de la carte Bleue qui a été utilisé pour le paiement 
	function spplus_pays_iso($pays_cb_paiement) {
		switch ($pays_cb_paiement) {
			case '004': $spplus_pays_iso='Afghanistan';
				break;
			case '008': $spplus_pays_iso='Albani';
				break;
			case '012': $spplus_pays_iso='Algerie';
				break;
			case '016': $spplus_pays_iso='American Samoa';
				break;
			case '020': $spplus_pays_iso='Andorre';
				break;
			case '024': $spplus_pays_iso='Angola';
				break;
			case '660': $spplus_pays_iso='Anguilla';
				break;
			case '028': $spplus_pays_iso='Antigua and Barbuda';
				break;
			case '032': $spplus_pays_iso='Argentine';
				break;
			case '051': $spplus_pays_iso='Armeni';
				break;
			case '533': $spplus_pays_iso='Aruba';
				break;
			case '036': $spplus_pays_iso='Australie';
				break;
			case '040': $spplus_pays_iso='Austria';
				break;
			case '031': $spplus_pays_iso='Azerbaijan';
				break;
			case '044': $spplus_pays_iso='Bahamas';
				break;
			case '048': $spplus_pays_iso='Bahrain';
				break;
			case '050': $spplus_pays_iso='Bangladesh';
				break;
			case '052': $spplus_pays_iso='Barbados';
				break;
			case '112': $spplus_pays_iso='Belarus';
				break;
			case '056': $spplus_pays_iso='Belgique';
				break;
			case '084': $spplus_pays_iso='Belize';
				break;
			case '204': $spplus_pays_iso='Benin';
				break;
			case '060': $spplus_pays_iso='Bermuda';
				break;
			case '064': $spplus_pays_iso='Bhutan';
				break;
			case '068': $spplus_pays_iso='Bolivie';
				break;
			case '070': $spplus_pays_iso='Bosnie';
				break;
			case '072': $spplus_pays_iso='Botswana';
				break;
			case '076': $spplus_pays_iso='Bresil';
				break;
			case '096': $spplus_pays_iso='Brunei Darussalam';
				break;
			case '100': $spplus_pays_iso='Bulgarie';
				break;
			case '854': $spplus_pays_iso='Burkina Faso';
				break;
			case '108': $spplus_pays_iso='Burundi';
				break;
			case '116': $spplus_pays_iso='Cambodia';
				break;
			case '120': $spplus_pays_iso='Cameroun';
				break;
			case '124': $spplus_pays_iso='Canada';
				break;
			case '132': $spplus_pays_iso='Cap Vert';
				break;
			case '136': $spplus_pays_iso='Iles Caimans';
				break;
			case '140': $spplus_pays_iso='Republique de centre Afrique';
				break;
			case '148': $spplus_pays_iso='Chad';
				break;
			case '152': $spplus_pays_iso='Chili';
				break;
			case '156': $spplus_pays_iso='Chine';
				break;
			case '170': $spplus_pays_iso='Colombie';
			
			
				break;
			case '174': $spplus_pays_iso='Comores';
				break;
			case '178': $spplus_pays_iso='Congo';
				break;
			case '180': $spplus_pays_iso='Republique democratique du congo';
				break;
			case '184': $spplus_pays_iso='Cook Island';
				break;
			case '188': $spplus_pays_iso='Costa Rica';
				break;	
			case '384': $spplus_pays_iso='Cote d Ivoire';
				break;	
			case '191': $spplus_pays_iso='Croatie';
				break;	
			case '192': $spplus_pays_iso='Cuba';
				break;
			case '196': $spplus_pays_iso='Chypre';
				break;	
			case '203': $spplus_pays_iso='Republique tcheque';
				break;	
			case '208': $spplus_pays_iso='Danemark';
				break;	
			case '262': $spplus_pays_iso='Djibouti';
				break;	
			case '212': $spplus_pays_iso='Dominica';
				break;
			case '214': $spplus_pays_iso='Republique dominicaine';
				break;
			case '218': $spplus_pays_iso='Equateur';
				break;		
			case '818': $spplus_pays_iso='Egypte';
				break;	
			case '222': $spplus_pays_iso='El Salvador';
				break;
			case '226': $spplus_pays_iso='Guinée Equatoriale';
				break;	
			case '232': $spplus_pays_iso='Erythree';
				break;
			case '233': $spplus_pays_iso='Estonie';
				break;		
			case '231': $spplus_pays_iso='Ethiopie';
				break;		
			case '238': $spplus_pays_iso='Falkland Island Malvinas';
				break;
			case '234': $spplus_pays_iso='Iles Feroes';
				break;	
			case '242': $spplus_pays_iso='Fidji';
				break;					
			case '246': $spplus_pays_iso='Finland';
				break;	
			case '250': $spplus_pays_iso='FRANCE';
				break;	
			case '254': $spplus_pays_iso='Guinee Francaise';
				break;	
			case '258': $spplus_pays_iso='Polynesie Francaise';
				break;	
			case '266': $spplus_pays_iso='Gabon';
				break;			
			case '270': $spplus_pays_iso='Gambie';
				break;	
			case '268': $spplus_pays_iso='Georgie';
				break;	
			case '276': $spplus_pays_iso='Allemagne';
				break;	
			case '288': $spplus_pays_iso='Ghana';
				break;	
			case '292': $spplus_pays_iso='Gibraltar';
				break;	
			case '300': $spplus_pays_iso='Grece';
				break;	
			case '304': $spplus_pays_iso='Groenland';
				break;	
			case '308': $spplus_pays_iso='Grenade';
				break;					
			case '312': $spplus_pays_iso='Guadeloupe';
				break;	
			case '316': $spplus_pays_iso='Guam';
				break;	
			case '320': $spplus_pays_iso='Guatemala';
				break;	
			case '324': $spplus_pays_iso='Guinee';
				break;
			case '624': $spplus_pays_iso='Guinee Bissau';
				break;	
			case '328': $spplus_pays_iso='Guyane';
				break;	
			case '332': $spplus_pays_iso='Haiti';
				break;	
			case '336': $spplus_pays_iso='Cite du Vatican';
				break;
			case '340': $spplus_pays_iso='Honduras';
				break;	
			case '344': $spplus_pays_iso='Hong Kong';
				break;	
			case '348': $spplus_pays_iso='Hongrie';
				break;
			case '352': $spplus_pays_iso='Islande';
				break;	
			case '356': $spplus_pays_iso='Inde';
				break;		
			case '360': $spplus_pays_iso='Indonesie';
				break;		
			case '364': $spplus_pays_iso='Iran';
				break;		
			case '368': $spplus_pays_iso='Iraq';
				break;		
			case '372': $spplus_pays_iso='Ireland';
				break;		
			case '376': $spplus_pays_iso='Israel';
				break;		
			case '380': $spplus_pays_iso='Italie';
				break;		
			case '388': $spplus_pays_iso='Jamaique';
				break;	
			case '392': $spplus_pays_iso='Japon';
				break;		
			case '400': $spplus_pays_iso='Jordanie';
				break;	
			case '398': $spplus_pays_iso='Kazakhstan';
				break;		
			case '404': $spplus_pays_iso='Kenya';
				break;	
			case '296': $spplus_pays_iso='Kiribati';
				break;	
			case '408': $spplus_pays_iso='Koree';
				break;
			case '410': $spplus_pays_iso='Republique de Koree';
				break;	
			case '414': $spplus_pays_iso='Koweit';
				break;	
			case '417': $spplus_pays_iso='Kyrgystan';
				break;	
			case '418': $spplus_pays_iso='Lao Peoples Democratic Republic';
				break;
			case '428': $spplus_pays_iso='Lettonie';
				break;	
			case '422': $spplus_pays_iso='Liban';
				break;	
			case '426': $spplus_pays_iso='Lesotho';
				break;
			case '430': $spplus_pays_iso='Liberia';
				break;	
			case '434': $spplus_pays_iso='Libyan Arab Jamahiriya';
				break;
			case '438': $spplus_pays_iso='Liechtenstein';
				break;
			case '440': $spplus_pays_iso='Lituanie';
				break;	
			case '442': $spplus_pays_iso='Luxembourg';
				break;	
			case '446': $spplus_pays_iso='Macao';
				break;	
			case '807': $spplus_pays_iso='Macedoine';
				break;	
			case '450': $spplus_pays_iso='Madagascar';
				break;	
			case '454': $spplus_pays_iso='Malawi';
				break;	
			case '458': $spplus_pays_iso='Malaisie';
				break;			
			case '462': $spplus_pays_iso='Maldives';
				break;
			case '466': $spplus_pays_iso='Mali';
				break;	
			case '470': $spplus_pays_iso='Malta';
				break;	
			case '584': $spplus_pays_iso='Marshall Islands';
				break;	
			case '474': $spplus_pays_iso='Martinique';
				break;	
			case '478': $spplus_pays_iso='Mauritanie';
				break;	
			case '480': $spplus_pays_iso='Mauritius';
				break;	
			case '484': $spplus_pays_iso='Mexico';
				break;	
			case '583': $spplus_pays_iso='Micronesia Federated states of';
				break;			
			case '498': $spplus_pays_iso='Moldavie';
				break;	
			case '492': $spplus_pays_iso='Monaco';
				break;		
			case '496': $spplus_pays_iso='Mongolia';
				break;
			case '500': $spplus_pays_iso='Monserrat';
				break;	
			case '504': $spplus_pays_iso='Morocco';
				break;	
			case '508': $spplus_pays_iso='Mozambique';
				break;	
			case '104': $spplus_pays_iso='Myanmar';
				break;	
			case '516': $spplus_pays_iso='Namibia';
				break;			
			case '520': $spplus_pays_iso='Nauru';
				break;
			case '524': $spplus_pays_iso='Nepal';
				break;	
			case '528': $spplus_pays_iso='Netherlands';
				break;	
			case '530': $spplus_pays_iso='Netherlands Antilles';
				break;	
			case '540': $spplus_pays_iso='Nouvelle Caledonie';
				break;
			case '554': $spplus_pays_iso='Nouvelle Zelande';
				break;	
			case '558': $spplus_pays_iso='Nicaragua';
				break;	
			case '562': $spplus_pays_iso='Niger';
				break;	
			case '566': $spplus_pays_iso='Nigeria';
				break;	
			case '570': $spplus_pays_iso='Niue';
				break;	
			case '574': $spplus_pays_iso='Norfolk Island';
				break;	
			case '580': $spplus_pays_iso='Northern Mariana Islands';
				break;	
			case '578': $spplus_pays_iso='Normay';
				break;	
			case '512': $spplus_pays_iso='Oman';
				break;		
			case '586': $spplus_pays_iso='Pakistan';
				break;
			case '585': $spplus_pays_iso='Palau';
				break;	
			case '591': $spplus_pays_iso='Panama';
				break;	
			case '598': $spplus_pays_iso='Papua New Guinea';
				break;	
			case '600': $spplus_pays_iso='Paraguay';
				break;	
			case '604': $spplus_pays_iso='Perou';
				break;	
			case '608': $spplus_pays_iso='Philippines';
				break;
			case '612': $spplus_pays_iso='Pitcaim';
				break;	
			case '616': $spplus_pays_iso='Poland';
				break;	
			case '620': $spplus_pays_iso='Portugal';
				break;	
			case '630': $spplus_pays_iso='Porto Rico';
				break;		
			case '634': $spplus_pays_iso='Qatar';
				break;
			case '638': $spplus_pays_iso='Reunion';
				break;	
			case '642': $spplus_pays_iso='Roumanie';
				break;	
			case '643': $spplus_pays_iso='Russie';
				break;	
			case '646': $spplus_pays_iso='Rwanda';
				break;
			case '654': $spplus_pays_iso='Sainte Helene';
				break;	
			case '659': $spplus_pays_iso='Saint Kitts and Nevis';
				break;	
			case '662': $spplus_pays_iso='Sainte lucie';
				break;	
			case '666': $spplus_pays_iso='Saint Pierre et Miquelon';
				break;	
			case '670': $spplus_pays_iso='Saint vincent and the Grenadines';
				break;
			case '882': $spplus_pays_iso='Samoa';
				break;	
			case '674': $spplus_pays_iso='San Marino';
				break;	
			case '678': $spplus_pays_iso='Sao Tome and Principe';
				break;		
			case '682': $spplus_pays_iso='Saudi Arabia';
				break;			
			case '686': $spplus_pays_iso='Senegal';
				break;			
			case '690': $spplus_pays_iso='Seychelles';
				break;		
			case '694': $spplus_pays_iso='Sierra Leone';
				break;		
			case '702': $spplus_pays_iso='Singapour';
				break;	
			case '703': $spplus_pays_iso='Slovaquie';
				break;		
			case '705': $spplus_pays_iso='Slovenie';
				break;		
			case '090': $spplus_pays_iso='Ile Salomon';
				break;		
			case '706': $spplus_pays_iso='Somalie';
				break;		
			case '710': $spplus_pays_iso='Afrique du Sud';
				break;	
			case '724': $spplus_pays_iso='Espagne';
				break;	
			case '144': $spplus_pays_iso='Sri Lanka';
				break;		
			case '736': $spplus_pays_iso='Soudan';
				break;	
			case '740': $spplus_pays_iso='Surinam';
				break;	
			case '744': $spplus_pays_iso='Svalbard and Jan Mayen';
				break;
			case '748': $spplus_pays_iso='Swaziland';
				break;	
			case '752': $spplus_pays_iso='Suede';
				break;	
			case '756': $spplus_pays_iso='SUISSE';
				break;	
			case '760': $spplus_pays_iso='Syrian Arab Republic';
				break;
			case '158': $spplus_pays_iso='Taiwan';
				break;	
			case '762': $spplus_pays_iso='Tajikistan';
				break;		
			case '834': $spplus_pays_iso='Tanzanie';
				break;
			case '764': $spplus_pays_iso='Thailande';
				break;				
			case '768': $spplus_pays_iso='Togo';
				break;			
			case '772': $spplus_pays_iso='Tokelau';
				break;
			case '776': $spplus_pays_iso='Tonga';
				break;	
			case '780': $spplus_pays_iso='Trinidad et Tobago';
				break;	
			case '788': $spplus_pays_iso='Tunisie';
				break;
			case '792': $spplus_pays_iso='Turquie';
				break;	
			case '795': $spplus_pays_iso='Turkmenistan';
				break;	
			case '796': $spplus_pays_iso='Turks and Caicos Islands';
				break;
			case '798': $spplus_pays_iso='Tuvalu';
				break;	
			case '800': $spplus_pays_iso='Uganda';
				break;	
			case '804': $spplus_pays_iso='Ukraine';
				break;	
			case '784': $spplus_pays_iso='United Arab Emirates';
				break;	
			case '826': $spplus_pays_iso='Royaume-Uni';
				break;
			case '840': $spplus_pays_iso='Etats-Uni';
				break;	
			case '858': $spplus_pays_iso='Uruguay';
				break;	
			case '860': $spplus_pays_iso='Uzbekistan';
				break;	
			case '548': $spplus_pays_iso='Vanuatu';
				break;	
			case '862': $spplus_pays_iso='Venezuela';
				break;	
			case '704': $spplus_pays_iso='Vietnam';
				break;	
			case '092': $spplus_pays_iso='Virgin Island British';
				break;				
			case '850': $spplus_pays_iso='Virgin Island US';
				break;
			case '876': $spplus_pays_iso='Wallis et Futuna';
				break;	
			case '732': $spplus_pays_iso='Western Sahara';
				break;	
			case '887': $spplus_pays_iso='Yemen';
				break;	
			case '894': $spplus_pays_iso='Zambie';
				break;	
			case '716': $spplus_pays_iso='Zimbabwe';
				break;	
								 
		}
			return $spplus_pays_iso;
		}
?>