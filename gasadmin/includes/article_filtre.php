<?php
$rayon = (isset($_GET['id_rayon']) && !empty($_GET['id_rayon'])) ? (int)$_GET['id_rayon'] : 0;
$marque = 0;
$modele = 0;

$path = explode('_', $_GET['cPath']);
$marque = $path[0];
$modele = $path[1];


if($rayon==0 && $marque>0) {//on calcule l'ID du rayon
	
	$query = 'select rubrique_id from '. TABLE_CATEGORIES_RAYON .' where categories_id='. $marque;
	$query = tep_db_query($query);
	$rayon = tep_db_fetch_array($query);
	
	$rayon = $rayon['rubrique_id'];
}

?>
<div class="filtre">
	<div>
    <form method="get" action="article_list.php">
        Rechercher article : <input type="text" name="search_article" value="<?php echo $_GET['search_article']; ?>"/>
    </form>
    <form method="get" action="categorie_list.php">
		Rechercher catégorie : <input type="text" name="search_cat" value="<?php echo $_GET['search_cat']; ?>"/>
    </form>
    <div style="float:left;margin-right:50px;">&nbsp;</div>
    <form action="">
        <select name="id_rayon" onchange="document.location.href='categorie_list.php?id_rayon='+ this.value;">
        	<option value="0">Tous les rayons</option>
            <?php
			$query = 'select rubrique_id, rubrique_name from '. TABLE_RAYON .' order by rubrique_ordre';
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$selected = ($rayon==$data['rubrique_id']) ? 'selected="selected"' : '';
				echo '<option value="'. $data['rubrique_id'] .'" '. $selected .'>'. $data['rubrique_name'] .'</option>';
			}
			?>
        </select>
        <select name="marque" onchange="document.location.href='categorie_list.php?cPath='+ this.value;">
        	<option value="0">Catégorie Premier niveau</option>
            <?php
			
			$query = 'select distinct(c.categories_id), categories_name from '. TABLE_CATEGORIES_RAYON .' cr, '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd where cr.categories_id=c.categories_id and c.categories_id=cd.categories_id and parent_id=\'0\' and rubrique_id='. $rayon .' order by categories_name';
			$query = tep_db_query($query);
			
			while($data = tep_db_fetch_array($query)) {
				
				$selected = ($marque==$data['categories_id']) ? 'selected="selected"' : '';
				echo '<option value="'. $data['categories_id'] .'" '. $selected .'>'. $data['categories_name'] .'</option>';
			}
			
			?>
        </select>
        <select name="modele" onchange="document.location.href='article_list.php?cPath='+ this.form.marque.value +'_'+ this.value;">
        	<option value="0">Catégorie Second niveau</option>
            <?php
			
			if($marque>0) {
				
				$query = 'select distinct(c.categories_id), categories_name from '. TABLE_CATEGORIES_RAYON .' cr, '. TABLE_CATEGORIES .' c, '. TABLE_CATEGORIES_DESCRIPTION .' cd where cr.categories_id=c.categories_id and c.categories_id=cd.categories_id and parent_id=\''. $marque .'\' and rubrique_id='. $rayon .' order by categories_name';
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$selected = ($modele==$data['categories_id']) ? 'selected="selected"' : '';
					echo '<option value="'. $data['categories_id'] .'" '. $selected .'>'. $data['categories_name'] .'</option>';
				}
			}
			?>
        </select>
    </form>
	<div></div>
    </div>
</div>