<?php

require('../../filenames.php');
require('../../database_tables.php');
require('../../configure.php');
require('../../functions/database.php');
require('../../functions/date.php');

tep_db_connect() or die('Unable to connect to database server!');

$cmd_to_return = '';

$orders_query_raw = "select o.orders_id, o.customers_id, o.delivery_country, o.origine, o.customers_name, o.customers_company, o.customers_id, o.customer_ip_country, o.customer_ip, o.payment_method, o.total, o.marge, o.remise, o.date_purchased, o.last_modified, o.currency, o.currency_value, o.marge, s.orders_status_name, o.orders_numero_facture 
										 from " . TABLE_ORDERS . " o, " . TABLE_ORDERS_STATUS . " s, " . TABLE_CUSTOMERS . " c 
										 where o.orders_status = s.orders_status_id and s.language_id = '1' and o.customers_id = c.customers_id";
    
	
	if(isset($HTTP_GET_VARS['date_deb']) && !empty($HTTP_GET_VARS['date_deb'])) {
		$date_deb = tep_db_prepare_input(date_fr_to_bdd($HTTP_GET_VARS['date_deb']));
      	$orders_query_raw .= " and o.orders_date_facture >= '".$date_deb ."'";
	}
	if(isset($HTTP_GET_VARS['date_fin']) && !empty($HTTP_GET_VARS['date_fin'])) {
		$date_fin = tep_db_prepare_input(date_fr_to_bdd($HTTP_GET_VARS['date_fin']));
      	$orders_query_raw .= " and o.orders_date_facture <= '".$date_fin." 23:59:59'";
	}
	
	if(isset($HTTP_GET_VARS['type_client']) && !empty($HTTP_GET_VARS['type_client'])) {
		$type_client = tep_db_prepare_input($HTTP_GET_VARS['type_client']);
		$orders_query_raw .= " and c.customers_type='" . $type_client . "'";
	}
	
    $orders_query = tep_db_query($orders_query_raw ." and ss_total>0 order by o.orders_id ASC");
	
    while ($orders = tep_db_fetch_array($orders_query)) {
		
		$cmd_to_return .= $orders['orders_id'] ." ";
	}
	
	$orders_query = tep_db_query($orders_query_raw ." and ss_total<0 order by o.orders_id ASC");
	
    while ($orders = tep_db_fetch_array($orders_query)) {
		
		$cmd_to_return .= $orders['orders_id'] ." ";
	}
	
	echo $cmd_to_return;
?>