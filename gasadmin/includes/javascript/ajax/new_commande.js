/* Auteur : Sami 
   Page permettant les appels ajax dans la page de creation d'un devis ou d'une nouvelle depuis l'admin*/

function transfert_date(){	
	document.getElementById("dd").value=document.getElementById("date_devis").value;
}

function devis(id_client){
   var checkBox = document.getElementById("id_devis");
   var devis;
	if(checkBox.checked == false){
		devis="non";
		//alert("non");
	}else{
		devis="oui";
		//alert("oui");
	}					 
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
					//alert(rep=req.responseText);
				    document.location.href="nouvelle_commandeP1.php?cID="+id_client;			  
				}
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?devis="+devis, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}

function n_paiement(id_client){
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{ document.location.href="nouvelle_commandeP1.php?cID="+id_client; }
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?recharge_paiement=oui", true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}

function paiement(id,id_client){
	 var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{ document.location.href="nouvelle_commandeP1.php?cID="+id_client; }
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?moyen_paiement="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}

function aff_montant(){
	var nb_ligne=document.getElementById("nb_ligne_panier").value;
	var adresse=document.getElementById("adresse_id").value;
	var montant=0;
	var poids=0;
	
	for(i=0;i<nb_ligne;i++){
		var qte=document.getElementById("qte"+i).value;
		var price=document.getElementById("price"+i).value;
		var poids_p=document.getElementById("poids"+i).value;
		montant+=(qte*price);
		poids+=(qte*poids_p);
	}	
	//alert(poids);
	
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{
					var rep=req.responseText;
					var str;
    							
							
								
					/*
					str=rep.split("§!§");
					var taxe=parseFloat(str[0]);
					var frais=parseFloat(str[1]);
					var remise=parseFloat(str[2]);
					*/
					str=rep.split("!");
					
					//alert(rep);	
					
					var taxe=parseFloat(str[0]);
					var frais=parseFloat(str[1]);
					var remise=parseFloat(str[2]);
					
					if(frais=='' && frais!=0){
						frais="Vous devez choisir une adressse de livraison";
					}
					//alert(parseFloat(str[0]) + '-' + (str[1])+ '-' + (str[2]));
					// inox
					//if(isNaN(frais)){ frais = 0; }
					//if(frais == 'NaN'){ frais = 0; }
					// inox //alert(frais);
					
					var Montant_ht=parseFloat(montant);
					Montant_ht=Math.round(Montant_ht*100)/100;
					if(Montant_ht==0){
						var Livraison=0;
						var Montant_ht_remise=0;
						var remise=0;
					}else{
					var Livraison=parseFloat(frais);
					var Montant_ht_remise=Montant_ht+Livraison-remise;
					}
					if(taxe==1.2){
					var TVA=(Montant_ht_remise)*0.2;
					}
					else {TVA=0;}
					TVA=Math.round(TVA*100)/100;
					
					var Montant_Total=Math.round((Montant_ht_remise+TVA)*100)/100;
					
					
					
					document.getElementById("affiche_montant").innerHTML="Montant HT : "+Montant_ht+"<br />Livraison HT : <input type='text' value='"+Livraison+"' id='input_fdp' onkeyup='mise_a_jour_fdp();' /><input type='button' value='Appliquer les frais de port HT' onclick='window.location.reload();' /><br />Remise : "+remise+"<br />TVA : "+ TVA +"<br />Montant Total : "+Montant_Total;
				}
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?adresse_livraison="+adresse+"&poids="+poids+"&montant="+montant, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	
}

//pour l'adresse de livraison
function choisse(id,id_client){	 
	 var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
				    document.location.href="nouvelle_commandeP1.php?cID="+id_client;
				}
				else
				{alert(req.statusText);}
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?adresse="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}
  
 //pour les produits
function rech(){
	var keyword=document.getElementById("keyword").value;
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
				    document.getElementById("resultat").innerHTML=req.responseText;				  
				}
				else
				{alert(req.statusText);}
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?keyword="+keyword, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}

// Fonctions : AJAX pour les menus déroulants pour sélectionner l'article

function marque_search() {
	document.getElementById('add_prod').disabled = true;
	document.getElementById('qte_prod').disabled = true;
	document.getElementById('liste_articles').disabled = true;
	document.getElementById('liste_articles').innerHTML = "<option value=''>Veuillez d'abord remplir les champs ci-dessus</option>";
	document.getElementById('liste_options').innerHTML = "<option value=''>Pas d'options</option>";
	document.getElementById('modele').innerHTML = "<option value=''>Choisissez un modèle</option>";
	
	var menu_rayon = document.getElementById('rayon');
	var id_rayon = menu_rayon.options[rayon.selectedIndex].value;
	
	var req;
	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject(Microsoft.XMLHTTP);
	
	req.onreadystatechange = function()
	{
		if(req.readyState == 4)
		{
			if(req.status == 200)
			{
				document.getElementById("marque").innerHTML=req.responseText;
			}
			else
			{alert(req.statusText);}
		}
	};
	req.open("GET", "nouvelle_commande_ajax.php?rayon_select="+id_rayon, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function modele_search() {
	document.getElementById('add_prod').disabled = true;
	document.getElementById('qte_prod').disabled = true;
	document.getElementById('liste_articles').disabled = true;
	document.getElementById('liste_articles').innerHTML = "<option value=''>Veuillez d'abord remplir les champs ci-dessus</option>";
	
	var menu_marque = document.getElementById('marque');
	var id_marque = menu_marque.options[marque.selectedIndex].value;
	
	var req;
	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject(Microsoft.XMLHTTP);

	req.onreadystatechange = function()
	{ 
		if(req.readyState == 4)
		{  
			if(req.status == 200)
			{
				document.getElementById("modele").innerHTML=req.responseText;
			}
			else
			{alert(req.statusText);}
		}
	};
	req.open("GET", "nouvelle_commande_ajax.php?marque_select="+id_marque, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function article_search() {
	document.getElementById('add_prod').disabled = true;
	document.getElementById('qte_prod').disabled = true;
	
	var menu_modele = document.getElementById('modele');
	var id_modele = menu_modele.options[modele.selectedIndex].value;
	
	var menu_marque = document.getElementById('marque');
	var id_marque = menu_marque.options[marque.selectedIndex].value;
	
	var menu_rayon = document.getElementById('rayon');
	var id_rayon = menu_rayon.options[rayon.selectedIndex].value;
	
	var req;
	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject(Microsoft.XMLHTTP);

	req.onreadystatechange = function()
	{ 
		if(req.readyState == 4)
		{  
			if(req.status == 200)
			{
				if (req.responseText != "\n") {
					document.getElementById("liste_articles").innerHTML=req.responseText;
					document.getElementById('liste_articles').disabled = false;
				} else {
					document.getElementById("liste_articles").innerHTML="<option value=''>Aucun article disponible</option>";
				}
				
				document.getElementById("cPath").value = id_rayon + "_" + id_marque + "_" + id_modele;
			}
			else
			{alert(req.statusText);}
		}
	};
	
	if (id_modele != "") {
		req.open("GET", "nouvelle_commande_ajax.php?modele_select="+id_modele, true);
	} else {
		req.open("GET", "nouvelle_commande_ajax.php?modele_select="+id_marque, true);
	}
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function options_search(){
	var menu_articles = document.getElementById('liste_articles');
	var id_article = menu_articles.options[liste_articles.selectedIndex].value;
	
	get_products_infos(id_article);
		
	var req;
	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req = new ActiveXObject(Microsoft.XMLHTTP);

	req.onreadystatechange = function()
	{ 
		if(req.readyState == 4)
		{  
			if(req.status == 200) 
			{	
				var str;
				var rep=req.responseText;
				str=rep.split("$|$");
				if(str[0]==0){
					document.getElementById("recup_info").value=str[1];
					document.getElementById("liste_options").disabled=true;
					document.getElementById('qte_prod').disabled = false;
					document.getElementById("liste_options").innerHTML="<option value=\"\">Pas d\'options</option>";
					
					if (document.getElementById('qte_prod').value != '') {
						document.getElementById("add_prod").disabled = false;
					}
				}else{
				//alert(req.responseText);
				document.getElementById("add_prod").disabled = true;
				document.getElementById('qte_prod').disabled = true;
				document.getElementById("liste_options").disabled=false;
				document.getElementById("liste_options").innerHTML=str[1];			  
				}
			}
			else
			{alert(req.statusText);}
		}
	};
	req.open("GET", "nouvelle_commande_ajax.php?article_select="+id_article, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function verif_qte_2(){
	document.getElementById('qte_prod').disabled = false;
	
	if (document.getElementById('qte_prod').value != '') {
		document.getElementById("add_prod").disabled = false;
	} else {
		document.getElementById("add_prod").disabled = true;
	}
	var id_produit=document.getElementById("liste_options").options[document.getElementById("liste_options").selectedIndex].value;
	str=id_produit.split("*-*");
	document.getElementById("recup_info").value=str[0]+" /*--*/ "+str[1]+" /*--*/ "+str[2];
}

function get_products_infos(id_article) {
	var req;
	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject(Microsoft.XMLHTTP);

	req.onreadystatechange = function()
	{ 
		if(req.readyState == 4)
		{  
			if(req.status == 200)
			{	
				document.getElementById('article_infos').innerHTML = req.responseText;
			}
			else
			{alert(req.statusText);}
		}
	};
	req.open("GET", "nouvelle_commande_ajax.php?get_infos="+id_article, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function verif_option(){	
		var id_produit=document.getElementById("liste_produit").options[document.getElementById("liste_produit").selectedIndex].value;
		
		var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
					var str;
                    var rep=req.responseText;
                    str=rep.split("$|$");
					if(str[0]==0){
						document.getElementById("recup_info").value=str[1];
						document.getElementById("resultat_option").innerHTML="";
						document.getElementById("add_prod").disabled = false;
					}else{
					//alert(req.responseText);
					document.getElementById("resultat_qte").style.display="none";
				    document.getElementById("resultat_option").innerHTML=str[1];			  
					}
				}
				else
				{alert(req.statusText);}
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?option="+id_produit, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}

function verif_qte(){
	document.getElementById("resultat_qte").style.display="block";
	var id_produit=document.getElementById("liste_option").options[document.getElementById("liste_option").selectedIndex].value;
	str=id_produit.split("*-*");
	document.getElementById("recup_info").value=str[0]+" /*--*/ "+str[1]+" /*--*/ "+str[2];
}

function verif_numeric(variable){
 var exp = new RegExp("^[0-9]+$","g");
 return exp.test(variable);
} 

function verif(){
	var qte=document.getElementById("qte_prod").value;
	if(verif_numeric(qte)){
		alert("C'est bien un entier");
	}
	else{
		alert("C'est pas un entier");
	}
}

function ajout_prod(id_client){
	
	var infos=document.getElementById("recup_info").value;
	var qte=document.getElementById("qte_prod").value;
	
	if (document.getElementById("cPath") != null){
		var cPath = document.getElementById("cPath").value;
	}
	
	var str;
    str=infos.split("/*--*/");
	if(typeof str[1] == 'undefined'){
		//alert("infos :"+str[0]+" qte :"+qte);
		var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
					
					document.location.href="nouvelle_commandeP1.php?cID="+id_client;
				    //alert(req.responseText);				  
				}
				else
				{alert(req.statusText);}
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?ajout_produit="+str[0]+"&qte="+qte+"&cPath="+cPath, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
		
		
	}else
	{
	//alert("infos :"+str[0]+" "+str[1]+" "+str[2]+" qte :"+qte);	
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
					document.location.href="nouvelle_commandeP1.php?cID="+id_client;
				    //alert(req.responseText);				  
				}
				else
				{alert(req.statusText);}
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?ajout_produit="+str[0]+"&option_id="+str[2]+"&option_value_id="+str[1]+"&qte="+qte+"&cPath="+cPath, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);	
	}
}

function qte_modif_prod(id,id_client){
	
	var qte=document.getElementById("qte"+id).value;
	
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
					document.location.href="nouvelle_commandeP1.php?cID="+id_client;
				    //alert(req.responseText);				  
				}
				else
				{alert(req.statusText);}
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?id_ligne="+id+"&qte_mod="+qte, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}


function suppr_prod(id,id_client){		
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
					document.location.href="nouvelle_commandeP1.php?cID="+id_client;
				    //salert(req.responseText);				  
				}
				else
				{alert(req.statusText);}
			}
		};
		req.open("GET", "nouvelle_commande_ajax.php?id_ligne_suppr="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}

function mise_a_jour_fdp() {
	var fdp = parseFloat(document.getElementById('input_fdp').value);
	
	var req;
	if(window.XMLHttpRequest)
		req = new XMLHttpRequest();
	else if (window.ActiveXObject)
		req  = new ActiveXObject(Microsoft.XMLHTTP);

	req.onreadystatechange = function()
	{ 
		if(req.readyState == 4)
		{  
			if(req.status == 200)
			{	
				
			}
			else
			{alert(req.statusText);}
		}
	};
	req.open("GET", "nouvelle_commande_ajax.php?changement_fdp="+fdp, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}