<?php

include("../../database_tables.php");
require('../../configure.php');
require('../../functions/database.php');

tep_db_connect() or die('Unable to connect to database server!');

if(isset($_GET['action']) && isset($_GET['products_id']) && $_GET['action']=='update' && !empty($_GET['products_id']) && isset($_POST) && !empty($_POST)) {
	
	$query = 'update '. TABLE_PRODUCTS .' SET 
					products_quantity='. $_POST['products_quantity'] .',
					products_quantity_reel='. $_POST['products_quantity_reel'] .' where products_id='. $_GET['products_id'];
	
	if(tep_db_query($query) && $_POST['nb_options']>0) {
		
		foreach($_POST['options_quantity_reel'] as $key => $value) {
			
			$option = explode('_', $key);
			
			$options_id = $option[0];
			$options_values_id = $option[1];
			 
			$query = 'update '. TABLE_PRODUCTS_ATTRIBUTES .' SET 
					options_quantity=\''. $_POST['options_quantity'][$key] .'\',
					options_quantity_reel=\''. $_POST['options_quantity_reel'][$key] .'\' 
					where products_id='. $_GET['products_id'] .' and options_id='. $options_id .' and options_values_id='. $options_values_id;
					
			tep_db_query($query);
		}
	}
} elseif(isset($_GET['action']) && isset($_GET['products_id']) && $_GET['action']=='select' && !empty($_GET['products_id'])) {
	
	$query = 'select products_quantity, products_quantity_reel from '. TABLE_PRODUCTS .' where products_id='. $_GET['products_id'];
	$query = tep_db_query($query);
	
	$data = tep_db_fetch_array($query);
	
	$return = '<h1>Modification du stock d\'un produit</h1><hr>';
	
	$return .= '<form id="myForm">';
	
	$return .= '<table style="margin:auto;"><tr><td width="100px">Stock r&eacute;el</td><td width="100px">Stock virtuel</td></tr>
					<tr><td><input type="text" name="products_quantity_reel" value="'. $data['products_quantity_reel'] .'" size="10"></td>
					<td><input type="text" name="products_quantity" value="'. $data['products_quantity'] .'" size="10"></td></table>';
	
	
	$query = 'SELECT * FROM '. TABLE_PRODUCTS_ATTRIBUTES . ' pa, '. TABLE_PRODUCTS_OPTIONS .' po, '. TABLE_PRODUCTS_OPTIONS_VALUES .' pov 
	WHERE pa.options_id=po.products_options_id and pa.options_values_id=pov.products_options_values_id and products_id = ' . $_GET['products_id'] .' order by pa.products_options_sort_order';
	$query = tep_db_query($query);
	
	$nb_options = tep_db_num_rows($query);
	
	if($nb_options>0) {
		
		$return .= '<br /><div style="border-top: 1px solid #333333; border-bottom: 1px solid #333333; margin: 5px; padding: 5px; background-color:#FFFFFF;">';
		$return  .= 'Les options du produit';
		$return .= '</div>';
		
		$return .= '<table style="margin:auto;">';
		
		$i = 0;
		
		while($data = tep_db_fetch_array($query)) {
			
			if($i++ == 0) {				
				$return .= '<tr><td width="200px">'. $data['products_options_name'] .'</td><td width="100px">Stock R&eacute;el</td><td width="100px">Stock Virtuel</td><tr>';
			}
			
			$return .= '<tr>';
				$return .= '<td>'. utf8_encode($data['products_options_values_name']) .'</td>';
				$return .= '<td><input type="text" name="options_quantity_reel['. $data['options_id'] .'_'. $data['options_values_id'] .']" value="'. $data['options_quantity_reel'] .'" size="10"></td>';
				$return .= '<td><input type="text" name="options_quantity['. $data['options_id'] .'_'. $data['options_values_id'] .']" value="'. $data['options_quantity'] .'" size="10"></td>';
				$difference = $data['options_quantity_reel']-$data['options_quantity']; 
				if ($difference != 0) { $difference_ok = '!!!'; } else { $difference_ok = ' '; }
				$return .= '<td>' . $difference_ok . '</td>';
			$return .= '</tr>';
			
		}
		
		$return .= '</table>';
	}
	
	$return .= '<input type="hidden" name="nb_options" value="'. $nb_options .'">';
	$return .= '<input type="submit" value="Enregistrer le nouveau stock">';
	
	$return .= '</form>';
	echo $return;
}
?>