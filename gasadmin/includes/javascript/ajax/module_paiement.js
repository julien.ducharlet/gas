/* Page permettant les appels ajax dans la page de gestion du module de paiement */
   
function raz(){
	document.getElementById("nom_m").value='';
	document.getElementById("visible_m").checked==false;
	document.getElementById("ordre_m").value='';
	document.getElementById("statut_m").value='';
	document.getElementById("description_m").value='';
	document.getElementById("montant_m").value='';
	document.getElementById("image_m").value='';
	document.getElementById("img_n").value='';	
	document.getElementById("img_m_text").innerHTML='';
	document.getElementById("detail_m").value='';	
}

function online(id){
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{ document.location.href="module_paiement.php";	}
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "includes/javascript/ajax/module_paiement.php?online="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	
}

function offline(id){
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{ document.location.href="module_paiement.php";	}
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "includes/javascript/ajax/module_paiement.php?offline="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
}


function action(id){
	document.getElementById("action_moyen").style.display="block";
	if(id==0){
		document.getElementById("ajt_moyen").style.display="block";
		document.getElementById("bt_ajt_moyen").style.display="block";
		document.getElementById("modif_moyen").style.display="none";
		document.getElementById("bt_modif_moyen").style.display="none";
		document.getElementById("les_moyens").style.display="none";
		document.getElementById("ordre_af").style.display="none";
		raz();
		
	}else{
		document.getElementById("ajt_moyen").style.display="none";
		document.getElementById("bt_ajt_moyen").style.display="none";
		document.getElementById("modif_moyen").style.display="block";
		document.getElementById("bt_modif_moyen").style.display="block";
		document.getElementById("les_moyens").style.display="none";
		document.getElementById("ordre_af").style.display="block";
		
		var req;
		if (window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function() { 
			if (req.readyState == 4) {  
				if (req.status == 200) {					
                    var str;
                    var rep=req.responseText;
                    str=rep.split("�|�");
					document.getElementById("id_paiement").value=str[0];
					document.getElementById("nom_m").value=str[1];
					document.getElementById("description_m").value=str[2];
					document.getElementById("ordre_m").value=str[3];
					
					var selectBox = document.getElementById("liste_statut");
                    for (var i=0; i<selectBox.options.length; i++) {
                        if (selectBox.options[i].value==str[4]) {
							selectBox.options[i].selected = true;
						}
                   }
				   
					document.getElementById("montant_m").value=str[5];
					var checkBox = document.getElementById("visible_m");
				    if (str[6]==0) {
						checkBox.checked = false;
					} else {
						checkBox.checked = true;
					}
					 
					document.getElementById("img_m_text").innerHTML=str[7];
					document.getElementById("img_n").value=str[7];
					document.getElementById("detail_m").value=str[8];
									  
				} else { 
					alert(req.statusText); 
				}
			}
		};
		req.open("GET", "includes/javascript/ajax/module_paiement.php?id_paiement="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	}
}
	
function supprimer(id,nom){
	check = confirm("�tes vous sur de vouloir supprimer ce moyen de paiement ( " + nom + " ) ?");
	if (check==true){
		var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{					
                    var rep=req.responseText;
					alert ("Suppression OK");
					document.location.href="module_paiement.php";
									  
				}
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "includes/javascript/ajax/module_paiement.php?id_suppr="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	}
}
	
function retour(){
	document.getElementById("action_moyen").style.display="none";
	document.getElementById("les_moyens").style.display="block";
	raz();
}