<?php


include("../../database_tables.php");
require('../../configure.php');
require('../../functions/database.php');

tep_db_connect() or die('Unable to connect to database server!');

if(isset($_GET['action']) && isset($_GET['orders_products_id']) && $_GET['action']=='update' && !empty($_GET['orders_products_id']) && isset($_POST) && !empty($_POST)) {
	//enregistrement des informations du produit d'une commande
	
	//on r�cup�re l'id du produit
	$query = 'select products_id from '. TABLE_ORDERS_PRODUCTS .' where orders_products_id='. (int)$_GET['orders_products_id'];
	$query = tep_db_query($query);
	$data = tep_db_fetch_array($query);
	
	
	
	## MISE A JOUR des quantit�s envoy�es, du prix d'achat HT et du prix de vente HT ##
	$query = 'update '. TABLE_ORDERS_PRODUCTS .' SET
				products_quantity_sent='. (int)$_POST['products_quantity_sent'] .', 
				products_cost='. $_POST['products_cost'] .', 
				final_price='. $_POST['final_price'] .'
				
				where orders_products_id='. (int)$_GET['orders_products_id'];
	
	if(tep_db_query($query)) {//on recalcule le stock dans les tables products et products_options s'il y a des options
		
		$query = 'update '. TABLE_PRODUCTS .' SET 
				products_quantity = products_quantity + '. $new_quantity .',
				products_ordered = products_ordered - '. $new_quantity .'
				where products_id='. $data['products_id'];
		tep_db_query($query);
		
		
		if(isset($_POST['products_options'])) {
			
			$query = 'update '. TABLE_PRODUCTS_ATTRIBUTES .' SET
			options_quantity = options_quantity + '. $new_quantity .'
			where products_id='. $data['products_id'] .' and options_values_id='. (int)$_POST['products_options'];
			tep_db_query($query);
		}
	}
}
//elseif(isset($_GET['action']) && isset($_GET['products_id']) && isset($_GET['orders_id']) && $_GET['action']=='select' && !empty($_GET['products_id']) && !empty($_GET['orders_id'])) {
elseif(isset($_GET['action']) && isset($_GET['orders_products_id']) && $_GET['action']=='select' && !empty($_GET['orders_products_id'])) {
	//on r�cup�re les infos du produit d'une commande
	
	$query = 'select products_cost, final_price, products_quantity, products_quantity_sent from '. TABLE_ORDERS_PRODUCTS .' where orders_products_id='. (int)$_GET['orders_products_id'];
	$query = tep_db_query($query);
	
	$data = tep_db_fetch_array($query);
	
	//on rajoute un champ cach� si le produit poss�de une option
	$query_opt = 'select options_values_id
				from '. TABLE_ORDERS_PRODUCTS .' op, '. TABLE_ORDERS_PRODUCTS_ATTRIBUTES .' opa
				where op.orders_products_id=opa.orders_products_id and opa.orders_products_id='. (int)$_GET['orders_products_id'];
	$query_opt = tep_db_query($query_opt);
	
	if(tep_db_num_rows($query_opt) > 0) {
		
		$data_opt = tep_db_fetch_array($query_opt);
		
		$input_opt = '<input type="hidden" name="products_options" value="'. $data_opt['options_values_id'] .'">';
	}
	else $input_opt = '';
	
	$return = '<h1>Modification du produit pour cette commande uniquement</h1><hr>';
	
	$return .='<form id="myForm" name="myForm">';
	
		$return .= '<table width="100%"><tr><td>'. utf8_encode('Quantit� command�e') . '</td><td>' . utf8_encode('Quantit� envoy�e') . '</td><td>' . utf8_encode('Co�t d\'achat HT') . '</td><td>Prix de Vente HT</td></tr>';
		$return .= '<tr>
						<td><input type="text" value="'. $data['products_quantity'] .'" maxlength="3" size="3" name="products_quantity" disabled="disabled"></td>
						<td><input type="text" value="'. $data['products_quantity_sent'] .'" maxlength="3" size="3" name="products_quantity_sent"></td>
						<td><input type="text" value="'. $data['products_cost'] .'" maxlength="10" size="7" name="products_cost"></td>
						<td><input type="text" value="'. $data['final_price'] .'" maxlength="10" size="7" name="final_price"></td>
					</tr>
					</table>';
		
		$return .= $input_opt;
		
		$return .= '<input type="hidden" id="orders_products_id" value="'. (int)$_GET['orders_products_id'] .'">';
		$return .= '<input type="submit" value="Enregistrer les modifications">';
		
	$return .= '</form>';
	
	echo $return;
}
elseif(isset($_GET['action']) && isset($_GET['orders_id']) && $_GET['action']=='update' && !empty($_GET['orders_id']) && isset($_POST) && !empty($_POST)) {
	
	$query = 'update '. TABLE_ORDERS .' SET
		frais_port_client='. $_POST['frais_port_client'] .', 
		marge='. $_POST['marge'] .', 
		poids='. $_POST['poids'] .', 
		remise='. $_POST['remise'] .', 
		remise_porte_monnaie='. $_POST['remise_porte_monnaie'] .', 
		remise_pourcent='. $_POST['remise_pourcent'] .', 
		ss_total='. $_POST['ss_total'] .', 
		total='. $_POST['total'] .', 
		tva_port='. $_POST['tva_port'] .', 
		tva_total='. $_POST['tva_total'] .' where orders_id='. $_GET['orders_id'];
	
	tep_db_query($query);
}
elseif(isset($_GET['action']) && isset($_GET['orders_id']) && $_GET['action']=='select' && !empty($_GET['orders_id'])) {
	
	$query = 'select total, ss_total, tva_total, remise, remise_pourcent, remise_porte_monnaie, frais_port_client, tva_port, marge, poids from '. TABLE_ORDERS .' where orders_id='. $_GET['orders_id'];
	$query = tep_db_query($query);
	
	$data = tep_db_fetch_array($query);
	
	$return = '<h1>Modification de la commande</h1><hr>';
	
	$return .='<form id="myOrderForm">';
	
		$return .= '<table width="100%" class="total"><tr><td>Sous-total article HT</td><td>Frais de Port HT</td><td>TVA Frais de Port</td><td>TVA Total</td><td>Total CMD</td></tr>';
		$return .= '<tr><td><input type="text" value="'. $data['ss_total'] .'" maxlength="7" size="5" name="ss_total"></td>
						<td><input type="text" value="'. $data['frais_port_client'] .'" maxlength="7" size="5" name="frais_port_client"></td>
						<td><input type="text" value="'. $data['tva_port'] .'" maxlength="7" size="5" name="tva_port"></td>
						<td><input type="text" value="'. $data['tva_total'] .'" maxlength="10" size="5" name="tva_total"></td>
						<td><input type="text" value="'. $data['total'] .'" maxlength="10" size="5" name="total"></td></tr></table>';
		
		$return .= '<br /><br /><table style="float:right"><tr><td>Marge</td><td>Poids</td></tr>';
		$return .= '<tr><td><input type="text" value="'. $data['marge'] .'" maxlength="7" size="5" name="marge"></td>
						<td><input type="text" value="'. $data['poids'] .'" maxlength="7" size="5" name="poids"></td></tr></table>';
		
		$return .= '<table><tr><td>Remise</td><td>Remise Pourcentage</td><td>Remise Porte Monnaie</td></tr>';
		$return .= '<tr><td><input type="text" value="'. $data['remise'] .'" maxlength="7" size="5" name="remise"></td>
						<td><input type="text" value="'. $data['remise_pourcent'] .'" maxlength="7" size="5" name="remise_pourcent"></td>
						<td><input type="text" value="'. $data['remise_porte_monnaie'] .'" maxlength="7" size="5" name="remise_porte_monnaie"></td></tr></table>';
						
		$return .= '<input type="submit" value="Enregistrer les modifications">';
		
	$return .= '</form>';
	
	echo $return;
}
?>