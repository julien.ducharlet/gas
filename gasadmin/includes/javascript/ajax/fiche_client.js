/* Page permettant les appels ajax dans les pages du client de l'admin {client_edit.php, client_panier.php} */

//permet de supprimer une adresse
function supprime(id_client,id_address){
   check = confirm('�tes-vous s�r de vouloir supprimer cette adresse ?')
   if(check) {

	var req;
			if(window.XMLHttpRequest)
				req = new XMLHttpRequest();
			else if (window.ActiveXObject)
				req  = new ActiveXObject(Microsoft.XMLHTTP);
	
			req.onreadystatechange = function()
			{  
				if(req.readyState == 4)
				{  
					if(req.status == 200)
					{	
						//alert(req.responseText);
						//document.location.href="client_edit.php?id_client="+id_client+"&nom=&prenom=&nom_societe=&mail=&tel_mobile=&departement=&date_deb=&date_fin=&nb_resultats=20&type_client=&nbr_commandes=&argent_virtuel=&page=1&cID="+id_client+"&action=edit";		  
					}
					else
					{ alert(req.statusText); }
				}
			};
			req.open("GET", "includes/javascript/ajax/fiche_client.php?add_suppr="+id_address, true);
			req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			req.send(null);
   }

}

//permet de supprimer un article
function supprimer_article(id,id_client){
   check = confirm('�tes-vous s�r de vouloir supprimer cet article ?')
   if(check==true) {
	var req;
			if(window.XMLHttpRequest)
				req = new XMLHttpRequest();
			else if (window.ActiveXObject)
				req  = new ActiveXObject(Microsoft.XMLHTTP);
	
			req.onreadystatechange = function()
			{ 
				if(req.readyState == 4)
				{  
					if(req.status == 200)
					{	
						//alert(req.responseText);
						document.location.href="client_panier.php?cID="+id_client;		  
					}
					else
					{ alert(req.statusText); }
				}
			};
			req.open("GET", "includes/javascript/ajax/fiche_client.php?article="+id+"&client="+id_client, true);
			req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			req.send(null);
   }

}

//permet de supprimer un pack 
function supprimer_pack(id,id_client){
	
   check = confirm('�tes-vous s�r de vouloir supprimer ce pack ?')
   if(check==true) { 
	var req;
			if(window.XMLHttpRequest)
				req = new XMLHttpRequest();
			else if (window.ActiveXObject)
				req  = new ActiveXObject(Microsoft.XMLHTTP);
	
			req.onreadystatechange = function()
			{ 
				if(req.readyState == 4)
				{  
					if(req.status == 200)
					{	
						//alert(req.responseText);
						document.location.href="client_panier.php?cID="+id_client;		  
					}
					else
					{ alert(req.statusText); }
				}
			};
			req.open("GET", "includes/javascript/ajax/fiche_client.php?pack="+id+"&client="+id_client, true);
			req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			req.send(null);
			
   }

}