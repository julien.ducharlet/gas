/* Auteur : Sami 
   Page permettant les appels ajax dans la page de gestion du module de livraison*/
   
function new_zone(){
	document.location.href="module_livraison_edit.php?z_id=0";
}

function supprime_pays(z_id){
	if(document.getElementById("pays_use").value==''){
		 alert("Vous devez d'abord selectionner un pays pour pouvoir l'effacer");
		}
		else{		
		check = confirm("�tes vous sur de vouloir supprimer ce pays de la zone de livraison ?");
		if (check==true){
			var id_pays=document.getElementById("pays_use").options[document.getElementById("pays_use").selectedIndex].value;
			
			var req;
			if(window.XMLHttpRequest)
				req = new XMLHttpRequest();
			else if (window.ActiveXObject)
				req  = new ActiveXObject(Microsoft.XMLHTTP);
	
			req.onreadystatechange = function()
			{ 
				if(req.readyState == 4)
				{  
					if(req.status == 200)
					{	
						var str;
						var rep=req.responseText;
						str=rep.split("!|!");
						document.getElementById("pays_no").innerHTML=str[0];
						document.getElementById("pays_yes").innerHTML=str[1];		  
					}
					else
					{ alert(req.statusText); }
				}
			};
			req.open("GET", "includes/javascript/ajax/module_livraison.php?pays_suppr="+id_pays+"&z_id="+z_id, true);
			req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			req.send(null);
			}
		}	
}

function supprime_zone(id){
	check = confirm("�tes vous sur de vouloir supprimer cette zone de livraison "+id+" ?");
	if (check==true){
		var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{ document.location.href="module_livraison.php"; }
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "includes/javascript/ajax/module_livraison.php?supprime_zone="+id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	}
}


function ajouter_pays(z_id){
		if(document.getElementById("pays_not_use").value==''){
		 alert("Vous devez d'abord selectionner un pays");
		}
		else{		
		var id_pays=document.getElementById("pays_not_use").options[document.getElementById("pays_not_use").selectedIndex].value;
		
		var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
					var str;
                    var rep=req.responseText;
                    str=rep.split("!|!");
					if(!isNaN(str[2])){
						document.location.href="module_livraison_edit.php?z_id="+str[2];
					}
					else{
					document.getElementById("pays_no").innerHTML=str[0];
					document.getElementById("pays_yes").innerHTML=str[1];		  
					}
				}
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "includes/javascript/ajax/module_livraison.php?pays_ajout="+id_pays+"&z_id="+z_id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
		}
}

function ajout_pp(z_id){
	var poids=document.getElementById("nouv_poids").value;
	var prix=document.getElementById("nouv_prix").value;
	var verif=true;
	var err='';
	
	if(isNaN(poids) || poids==''){
		verif=false;
		err+='Saisissez une valeur numerique dans le champ poids\n';
	}
	if(isNaN(prix) || prix==''){
		verif=false;
		err+='Saisissez une valeur numerique dans le champ prix';
	}
	
	if(verif==false){
		alert(err);
	}
	else{
	
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
					var str;
                    var rep=req.responseText;
					str=rep.split("!|!");
					if(str[1]=="present"){
					alert("Le poids est d�j� present dans les conditions !!!");
					}
					else{
					document.getElementById("liste_pp").innerHTML=rep;
					}
				}
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "includes/javascript/ajax/module_livraison.php?poids="+poids+"&prix="+prix+"&z_id="+z_id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	}	
}

function supprime(ligne,z_id){
	var poids=document.getElementById("poids"+ligne).value;
	var prix=document.getElementById("prix"+ligne).value;
	check = confirm("�tes vous sur de vouloir supprimer ce couple poids "+ poids +" | prix "+prix+" ?");
	if (check==true){	
	var req;
		if(window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req  = new ActiveXObject(Microsoft.XMLHTTP);

		req.onreadystatechange = function()
		{ 
			if(req.readyState == 4)
			{  
				if(req.status == 200)
				{	
					var str;
                    var rep=req.responseText;
					document.getElementById("liste_pp").innerHTML=rep;	  
				}
				else
				{ alert(req.statusText); }
			}
		};
		req.open("GET", "includes/javascript/ajax/module_livraison.php?supprime="+ligne+"&poids_s="+poids+"&prix_s="+prix+"&z_id="+z_id, true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);
	}
	
}