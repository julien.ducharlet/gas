<?php

require('../../filenames.php');
require('../../database_tables.php');
require('../../configure.php');
require('../../functions/database.php');
require('../../functions/date.php');

tep_db_connect() or die('Unable to connect to database server!');

if(isset($_GET['type_client']) && isset($_GET['type_panier'])) {
	
	$temp_type_id_client = (!empty($_GET['type_id_client'])) ? explode(',', $_GET['type_id_client']) : array();
	$total = 0;
	
	foreach($temp_type_id_client as $type_id_client) {
		
		$addQuery = (!empty($type_id_client)) ? ' and customers_type='. $type_id_client : '';
		
		switch($_GET['type_client']) {
			
		case "tous": //tous
			
			if($_GET['type_panier']=="avec") {//avec commande
				
				$query = 'select distinct(c.customers_id) from '. TABLE_CUSTOMERS .' c, '. TABLE_ORDERS .' o where c.customers_id=o.customers_id'. $addQuery;
			}
			elseif($_GET['type_panier']=="sans") {
				
				$query = 'select distinct(customers_id) from '. TABLE_CUSTOMERS .' where customers_id NOT IN(select customers_id from '. TABLE_ORDERS .')'. $addQuery;
			}
			else {
				
				$query = 'select distinct(customers_id) from '. TABLE_CUSTOMERS . str_replace('and', 'where', $addQuery);
			}	
			break;
		
		case "inscrit": //inscrit
		
			if($_GET['type_panier']=="avec") {//avec commande
				
				$query = 'select distinct(c.customers_id) from '. TABLE_CUSTOMERS .' c, '. TABLE_ORDERS .' o where c.customers_id=o.customers_id and customers_newsletter=\'1\''. $addQuery;
			}
			elseif($_GET['type_panier']=="sans") {
				
				$query = 'select distinct(customers_id) from '. TABLE_CUSTOMERS .' where customers_newsletter=\'1\' and customers_id NOT IN(select customers_id from '. TABLE_ORDERS .')'. $addQuery;
			}
			else {
				
				$query = 'select distinct(customers_id) from '. TABLE_CUSTOMERS .' where customers_newsletter=\'1\''. $addQuery;
			}
			break;
		
		case "partenaire": //inscrit
		
			if($_GET['type_panier']=="avec") {//avec commande
				
				$query = 'select distinct(c.customers_id) from '. TABLE_CUSTOMERS .' c, '. TABLE_ORDERS .' o where c.customers_id=o.customers_id and customers_newsletter_partenaire=\'1\''. $addQuery;
			}
			elseif($_GET['type_panier']=="sans") {
				
				$query = 'select distinct(customers_id) from '. TABLE_CUSTOMERS .' where customers_newsletter_partenaire=\'1\' and customers_id NOT IN(select customers_id from '. TABLE_ORDERS .')'. $addQuery;
			}
			else {
				
				$query = 'select distinct(customers_id) from '. TABLE_CUSTOMERS .' where customers_newsletter_partenaire=\'1\''. $addQuery;
			}
			break;
		
		case "non_inscrit": //non inscrit
			if($_GET['type_panier']=="avec") {//avec commande
				
				$query = 'select distinct(c.customers_id) from '. TABLE_CUSTOMERS .' c, '. TABLE_ORDERS .' o where c.customers_id=o.customers_id and customers_newsletter=\'0\''. $addQuery;
			}
			elseif($_GET['type_panier']=="sans") {
				
				$query = 'select distinct(customers_id) from '. TABLE_CUSTOMERS .' where customers_newsletter=\'0\' and customers_id NOT IN(select customers_id from '. TABLE_ORDERS .')'. $addQuery;
			}
			else {
				
				$query = 'select distinct(customers_id) from '. TABLE_CUSTOMERS .' where customers_newsletter=\'0\''. $addQuery;
			}
			break;
		}
		
		$query = tep_db_query($query);
		$total += tep_db_num_rows($query);
	}
	
	echo $total;
}
?>