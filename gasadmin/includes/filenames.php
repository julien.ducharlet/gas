<?php 


// ###### BOX COMPTABILITE #########
	define('FILENAME_COMPTABILITE_ACCUEIL', 'comptabilite_accueil.php');
	define('FILENAME_EXPORT_FACTURE_COMPTABILITE', 'export_facture_compta.php');
	define('FILENAME_COMPTABILITE_LIST_FACTURE', 'export_facture_list_compta.php');
	define('FILENAME_COMPTABILITE_PAGE_VIDE', 'comptabilite_page_vide.php');
// ###### BOX COMPTABILITE #########


// ###### Added MODULE LIVRAISON + PAIEMENT #########
	define('FILENAME_MODULE_LIVRAISON', 'module_livraison.php');
	define('FILENAME_MODULE_PAIEMENT', 'module_paiement.php');
// ###### Added MODULE LIVRAISON + PAIEMENT #########

	define('FILENAME_COUPON_ADMIN', 'coupon_admin.php');
	define('FILENAME_GV_QUEUE', 'gv_queue.php');
	define('FILENAME_GV_MAIL', 'gv_mail.php');
	define('FILENAME_GV_SENT', 'gv_sent.php');
	define('FILENAME_STATS_CREDITS', 'stats_credits.php'); 
	
	define('FILENAME_CRON', 'cron.php');
	


  define('FILENAME_MARGIN_COMPLETE', 'order_simple.php');

  
  
  define('FILENAME_DEFAULT', '#');
 
  define('FILENAME_ADVANCED_SEARCH', 'advanced_search.php');
  define('FILENAME_ADVANCED_SEARCH_RESULT', 'advanced_search_result.php');

  define('FILENAME_ADMIN_ACCOUNT', 'admin_account.php');
  define('FILENAME_ADMIN_FILES', 'admin_files.php');
  define('FILENAME_ADMIN_MEMBERS', 'admin_members.php'); 
  
  define('FILENAME_ARTICLES_INVENDUS', 'invendus.php');
  define('FILENAME_ARTICLE_EDIT', 'article_edit.php');
  define('FILENAME_ARTICLE_TO_CATEGORIES', 'article_to_categories.php');
  define('FILENAME_ARTICLE_EDIT_IMAGES_VIDEOS', 'article_edit_images_videos.php');
  define('FILENAME_ARTICLE_EDIT_IMAGES_VIDEOS2', 'article_edit_images_videos2.php');
  define('FILENAME_ARTICLE_EDIT_PRIX_FOURNISSEURS', 'article_edit_prix_fournisseurs.php');
  define('FILENAME_ARTICLE_EDIT_STOCKS_OPTIONS', 'article_edit_stocks_options.php');
  define('FILENAME_ARTICLE_EDIT_REFERENCEMENT', 'article_edit_referencement.php');
  define('FILENAME_ARTICLE_EDIT_STATISTIQUES', 'article_edit_statistiques.php');
  define('FILENAME_ARTICLES_A_AJOUTER', 'articles_a_ajouter.php');
  define('FILENAME_ARTICLE_LIST', 'article_list.php');
  define('FILENAME_ARTICLE_CACHE', 'article_cache.php');
  
  define('FILENAME_BACKUP', 'backup.php');
  define('FILENAME_BANNER_MANAGER', 'banner_manager.php');
  define('FILENAME_BANNER_STATISTICS', 'banner_statistics.php');
  define('FILENAME_CACHE', 'cache.php');
  
  define('FILENAME_CATALOG_ACCOUNT_HISTORY_INFO', 'account_history_info.php');
  define('FILENAME_CATALOG_LOGIN', 'login.php');
  define('FILENAME_CATALOG_PRODUCT_INFO', 'product_info.php');
  
  define('FILENAME_CATEGORIES', 'categories.php');
  define('FILENAME_CATEGORIE_LIST', 'categorie_list.php');
  define('FILENAME_CATEGORIES_2', 'categories.php?selected_box=catalog');
  
  define('FILENAME_CLIENT_LIST', 'client_list.php');//LL ancien customers.php
  define('FILENAME_CLIENT_EDIT', 'client_edit.php');//LL ancien customers.php
  define('FILENAME_CLIENT_FAMILY', 'family_client.php');
  define('FILENAME_CLIENT_VERSEMENT', 'client_versement.php');//LL permet de consulter et remplir le porte monnaie virtuel du client
  
  define('FILENAME_CMD_EDIT', 'cmd_edit.php');//LL ancien orders.php cmd_edit.php
  define('FILENAME_CMD_LIST', 'cmd_list.php');//LL ancien orders.php
  define('FILENAME_CMD_ATTENTE', 'cmd_en_attente.php');
  
  define('FILENAME_CONFIGURATION', 'configuration.php');
  
  define('FILENAME_COUNTRIES', 'countries.php');
  define('FILENAME_CURRENCIES', 'currencies.php');
  define('FILENAME_CUSTOMERS', 'customers.php');
  define('FILENAME_CUSTOMERS_BIRTHDAY', 'birthday.php');
  define('FILENAME_DEFAULT', 'index.php');
  define('FILENAME_DEFINE_LANGUAGE', 'define_language.php');
  define('FILENAME_DELETE_BY_REF', 'delete_by_ref.php');
  define('FILENAME_DESTOCKAGE', 'destockages.php');
  define('FILENAME_DUPLICATE', 'duplicate.php');
  define('FILENAME_DUPLICATE_2', 'duplicate_2.php');
  define('FILENAME_FAMILY', 'family.php');

  define('FILENAME_FAQ_MANAGER', 'faq_manager.php');
  define('FILENAME_FAQ_VIEW', 'faq_view.php');
  define('FILENAME_FAQ_VIEW_ALL', 'faq_view_all.php');
  define('FILENAME_FAQ_FAMILY', 'faq_family.php');
  define('FILENAME_FAQ_FAMILY_ACTIONS', 'faq_family_actions.php');

  define('FILENAME_FILE_MANAGER', 'file_manager.php');
  define('FILENAME_FORBIDEN', 'forbiden.php');
  define('FILENAME_GEO_ZONES', 'geo_zones.php');
  
  define('FILENAME_GOOGLE_LISTE1', 'fichiers_google_liste1.php');
  define('FILENAME_GOOGLE_LISTE_REF', 'fichiers_google_liste_ref.php');
  define('FILENAME_GOOGLE_LISTE_CODE_BARRE', 'fichiers_google_liste_code_barre.php');
  define('FILENAME_GOOGLE_LISTE_FAMILLE', 'fichiers_google_liste_famille.php');
  define('FILENAME_GOOGLE_SITEMAPS', 'googlesitemap.php');
  
  define('FILENAME_HEADER_TAGS_CONTROLLER', 'header_tags_controller.php');
  define('FILENAME_HEADER_TAGS_FRENCH', 'header_tags_french.php');
  define('FILENAME_HEADER_TAGS_FILL_TAGS', 'header_tags_fill_tags.php');
	
  define('FILENAME_IMAGES_A_AJOUTER', 'images_a_ajouter.php');  
  define('FILENAME_LANGUAGES', 'languages.php');
  define('FILENAME_LOGIN', 'login.php');
  define('FILENAME_LOGOFF', 'logoff.php');
  define('FILENAME_MARGIN_REPORT', 'margin_report.php');
  define('FILENAME_MAIL', 'mail.php');
  define('FILENAME_MANUFACTURERS', 'manufacturers.php');
  define('FILENAME_MISE_A_JOUR_STOCK', 'mise_a_jour_stock.php');
  define('FILENAME_MODULES', 'modules.php');
  
  define('FILENAME_NEWSLETTER_CREATION', 'newsletter_creation.php');
  define('FILENAME_NEWSLETTER_LIST', 'newsletter_list.php');
  
  define('FILENAME_ORDERS', 'orders.php');
  define('FILENAME_ORDERS_PACKINGSLIP', 'packingslip.php');
  define('FILENAME_ORDERS_STATUS', 'orders_status.php');
  define('FILENAME_ORDERS_RECLAMAPOSTE', 'reclamaposte.php');
  
  define('FILENAME_PACK', 'pack.php');
  define('FILENAME_PASSWORD_FORGOTTEN', 'password_forgotten.php');
  
  define('FILENAME_PRODUCTS_ATTRIBUTES', 'products_attributes.php');
  define('FILENAME_PRODUCTS_EXPECTED', 'products_expected.php');
  define('FILENAME_ARTICLE_IMAGE_SIMPLE', 'article_image_simple.php');
  
  define('FILENAME_QUICK_UPDATES', 'quick_updates.php');
  define('FILENAME_QUICK_UPDATES2', 'mise_a_jour_02.php');
  define('FILENAME_RAYON', 'rayon.php');
  define('FILENAME_RAYON_EDIT', 'rayon_edit.php');
  define('FILENAME_RECOVER_CART_SALES', 'recover_cart_sales.php');
  define('FILENAME_REFERENCEMENT', 'referencement.php');
  define('FILENAME_REPORTS_NEWSLETTER', 'stats_newsletter.php');
  define('FILENAME_REVIEWS', 'reviews.php');
  define('FILENAME_SERVER_INFO', 'server_info.php');
  define('FILENAME_SHIPPING_MODULES', 'shipping_modules.php');
  define('FILENAME_SPECIALS', 'promotions.php');
  
  define('FILENAME_STATS_COMMANDES', 'stats_sales.php');
  define('FILENAME_STATS_COMMANDES_CHIFFRES', 'stats_cmd.php');
  define('FILENAME_STATS_CUSTOMERS', 'stats_customers.php');
  define('FILENAME_STATS_CUSTOMERS_ORDERS', 'stats_customers_orders.php'); 
  define('FILENAME_STATS_CUSTOMER_REGISTRATION', 'stats_customer_registrations.php');
  define('FILENAME_STATS_PRODUCTS_ORDERS', 'stats_products_orders.php');
  define('FILENAME_STATS_PRODUCTS_PURCHASED', 'stats_products_purchased.php');
  define('FILENAME_STATS_PRODUCTS_VIEWED', 'stats_products_viewed.php');
  define('FILENAME_STATS_RECOVER_CART_SALES', 'stats_recover_cart_sales.php');
  define('FILENAME_STATS_VENTES', 'stats_ventes.php');
  define('FILENAME_STATS_VISITE', 'stats_visite.php');

  define('FILENAME_STOCK_FOURNISSEURS', 'gestion_fournisseurs.php');
  define('FILENAME_STOCK_INFO_CLIENT', 'info_newstock_client.php');
  define('FILENAME_STOCK_LIVRAISON', 'livraison.php');
  define('FILENAME_STOCK_REF_FOURNISSEUR', 'gestion_ref_fournisseurs.php');
  define('FILENAME_STOCK_VALORISATION', 'stock_valorisation.php');
  
  define('FILENAME_TAX_CLASSES', 'tax_classes.php');
  define('FILENAME_TAX_RATES', 'tax_rates.php');
  define('FILENAME_CATEGORIE_SANS_IMAGES', 'categorie_sans_images.php');
  define('FILENAME_TYPE_PRODUCTS_PICTURES', 'article_logos.php');
  define('FILENAME_WHOS_ONLINE', 'whos_online.php');
  define('FILENAME_ZONES', 'zones.php'); 
  
  define('FILENAME_DEFINE_MAINPAGE', 'define_mainpage.php');  
  define('FILENAME_MAINPAGE', 'define_mainpage.php'); 
  define('FILENAME_DEFINE_SLIDER', 'define_slider.php'); 
  define('FILENAME_SLIDER', 'define_slider.php');
  
?>