<?php 
/*
 Page de configuration generale
*/ 

	define('NOM_DU_SITE', 'General Army Store');
	define('NOM_DU_SITE_MAJ', 'GENERAL ARMY STORE');
	define('TEL_NORMAL', '09 53 31 59 47');
	define('TEL_PRO', '09 53 31 59 47');
	define('ADRESSE_SITE', 'www.GeneralArmyStore.fr');
	define('TINY_MCE_URL', 'https://generalarmystore.fr/');
	define('HORAIRES_BOUTIQUE', 'du lundi au vendredi de 10h � 12h30 et de 14h � 18h30');
	
	//information concernant les locaux de la soci�t�
	define('ADRESSE_LOCAUX', 'GENERAL ARMY STORE - 21, rus Pasteur - Entr�e rue H. Bajard - 26260 SAINT DONAT sur L\'HERBASSE');
	
	define('CODE_ETAB', '13485');
	define('CODE_GUICHET', '00800');
	define('NUM_COMPTE', '08912951801');
	define('RIB', '32');
	define('DOMICILIATION', 'CE LR Perpignan Roussillon');
	define('TITULAIRE', 'General Army Store');
	define('IBAN', 'FR76 1348 5008 0008 9129 5180 132');
	define('CODE_SWIFT', 'CEPAFRPP348');
	
	define('WEBSITE', 'https://www.generalarmystore.fr');
	define('BASE_DIR', '/gas');
	define('BASE_DIR_ADMIN', '/gasadmin');
	define('ABSOLUTE_DIR', '/var/www/vhosts/generalarmystore.fr/httpdocs');
	define('DATABASE_TABLE_DIR', '/var/www/vhosts/generalarmystore.fr/httpdocs/gasadmin/includes/database_tables.php');
	define('DIR_LANGUAGES', '/var/www/vhosts/generalarmystore.fr/httpdocs/gas/includes/languages/french/');
	
// Define the webserver and path parameters
// * DIR_FS_* = Filesystem directories (local/physical)
// * DIR_WS_* = Webserver directories (virtual/URL)
  define('HTTP_SERVER', 'https://www.generalarmystore.fr'); // eg, https://localhost - should not be empty for productive servers
  define('HTTP_CATALOG_SERVER', 'https://www.generalarmystore.fr');
  define('HTTPS_CATALOG_SERVER', '');
  define('ENABLE_SSL_CATALOG', 'false'); // secure webserver for catalog module
  
  define('DIR_WS_SITE', 'gas'); 
  define('DIR_WS_ADMIN', '/gasadmin/');
  define('DIR_FS_ADMIN', '/var/www/vhosts/generalarmystore.fr/httpdocs'.DIR_WS_ADMIN);
  define('DIR_WS_CATALOG', '/');
  define('DIR_WS_IMAGES', 'images/');
  define('DIR_WS_ICONS', DIR_WS_IMAGES . 'icons/');
  define('DIR_WS_BANNER_IMAGES', DIR_WS_CATALOG . DIR_WS_SITE.'/images/bannieres/');
  define('DIR_WS_CATALOG_IMAGES', DIR_WS_CATALOG . DIR_WS_SITE . '/images/');
  define('DIR_WS_PRODUCTS_IMAGES', DIR_WS_CATALOG . DIR_WS_SITE.'/images/products_pictures/');
  define('DIR_WS_CATEGORIE_HEADER_IMAGES', DIR_WS_CATALOG . DIR_WS_SITE.'/images/categories_header_pictures/');
  define('DIR_WS_CATEGORIE_IMAGES', DIR_WS_CATALOG . DIR_WS_SITE.'/images/categories_pictures/');
  define('DIR_WS_INCLUDES', 'includes/');
  define('DIR_WS_ARTICLE', DIR_WS_INCLUDES . 'article/');
  define('DIR_WS_BOXES', DIR_WS_INCLUDES . 'boxes/');
  define('DIR_WS_FUNCTIONS', DIR_WS_INCLUDES . 'functions/');
  define('DIR_WS_CLASSES', DIR_WS_INCLUDES . 'classes/');
  define('DIR_WS_MODULES', DIR_WS_INCLUDES . 'modules/');
  define('DIR_WS_LANGUAGES', DIR_WS_INCLUDES . 'languages/');
  define('DIR_WS_CATALOG_LANGUAGES', DIR_WS_CATALOG . 'includes/languages/'); 
  define('DIR_WS_CATALOG_LANGUAGES_GAS', DIR_WS_CATALOG . 'gas/includes/languages/'); // utilis� pour les images
  
  define('DIR_FS_DOCUMENT_ROOT', '/var/www/vhosts/generalarmystore.fr/httpdocs/'); 
  define('DIR_FS_CATALOG', '/var/www/vhosts/generalarmystore.fr/httpdocs/');  // /var/www/vhosts/generalarmystore.fr/httpdocs/gas/
  define('DIR_FS_CATALOG_INCLUDES', DIR_FS_CATALOG . 'includes/');
  define('DIR_FS_CATALOG_LANGUAGES', DIR_FS_CATALOG . 'includes/languages/');
  define('DIR_FS_CATALOG_IMAGES', DIR_FS_CATALOG . DIR_WS_SITE .'/images/');
  define('DIR_FS_PRODUCTS_IMAGES', DIR_FS_CATALOG . DIR_WS_SITE.'/images/products_pictures/');
  define('DIR_FS_CATEGORIE_HEADER_IMAGES', DIR_FS_CATALOG . DIR_WS_SITE.'/images/categories_header_pictures/');
  define('DIR_FS_CATEGORIE_IMAGES', DIR_FS_CATALOG . DIR_WS_SITE.'/images/categories_pictures/'); 
  define('DIR_FS_BANNER_IMAGES', DIR_FS_CATALOG . DIR_WS_SITE.'/images/bannieres/');
  
  define('DIR_FS_CATALOG_IMAGES_MARQUES', DIR_FS_CATALOG_IMAGES . 'marques_pictures/');
  define('DIR_WS_CATALOG_IMAGES_MARQUES', DIR_WS_CATALOG_IMAGES . 'marques_pictures/');
  define('DIR_FS_CATALOG_IMAGES_LOGOS', DIR_FS_CATALOG_IMAGES .'type_products_pictures/');
  define('DIR_WS_CATALOG_IMAGES_LOGOS', DIR_WS_CATALOG_IMAGES .'type_products_pictures/');
  
  define('DIR_FS_CATALOG_VIDEOS', DIR_FS_CATALOG . DIR_WS_SITE.'/videos/videos/');
  define('DIR_FS_CATALOG_VIDEOS_PREVIEW', DIR_FS_CATALOG . 'videos/videos_preview/');
  define('DIR_FS_CATALOG_MODULES', DIR_FS_CATALOG . 'includes/modules/');
  define('DIR_FS_BACKUP', DIR_FS_ADMIN . 'backups/');

 //image par defaut
  define('DEFAULT_IMAGE', 'no_picture_big.jpg');
  
// define our database connection
  define('DB_SERVER', 'localhost');
  define('DB_SERVER_USERNAME', 'generalarmystore');
  define('DB_SERVER_PASSWORD', 'EveiLaiz5g');
  define('DB_DATABASE', 'generalarmystore');
  define('USE_PCONNECT', 'false'); 
  define('STORE_SESSIONS', 'mysql'); 
  
// Taux des r�ductions quantitatives
	define('TAUX_PART_BY_1', '1');
	define('TAUX_PART_BY_5', '0.985'); // avant 0.97 (3%)  -  apres 0.985
	define('TAUX_PART_BY_10', '0.97'); // avant 0.93 (7%)  -  apres 0.97
	
	define('TAUX_PRO_BY_1', '1');
	define('TAUX_PRO_BY_5', '0.985'); // 0.985
	define('TAUX_PRO_BY_10', '0.97'); // 0.97
	
	define('TAUX_ADM_BY_1', '0.95'); // 1 NEW
	define('TAUX_ADM_BY_5', '0.94'); // 0.985 NEW
	define('TAUX_ADM_BY_10', '0.925'); // 0.97 NEW
	
	define('TAUX_REV_BY_1', '1.20'); // 1.20
	define('TAUX_REV_BY_5', '1.188'); // 1.188
	define('TAUX_REV_BY_10', '1.176'); // 1.176
	
	
	define('GEO_ZONE_AVEC_TVA', 9);
	define('GEO_ZONE_SANS_TVA', 8);
	
// permet d'afficher une couleur a la marge d'un commande dans la page cmd_list.php
	$marge_rouge = 10; 
	$marge_orange = 30; 
	
?>
