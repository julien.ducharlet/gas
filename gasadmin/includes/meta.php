<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css" media="screen,print">
<link rel="stylesheet" type="text/css" href="includes/lightbox.css" media="screen">
<link rel="stylesheet" type="text/css" href="includes/menu.css" media="screen">
<link rel="stylesheet" type="text/css" href="includes/css/print.css" media="print">
<script language="javascript" type="text/javascript" src="includes/javascript/jQuery/jquery-1.4.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jQuery/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jQuery/metadata.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jQuery/cookie.js"></script>
<script language="javascript" type="text/javascript" src="includes/menu.js"></script>
<script language="javascript" type="text/javascript" src="includes/general.js"></script>