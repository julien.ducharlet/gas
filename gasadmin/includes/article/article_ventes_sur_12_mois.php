<?php
/* 
include : artilce_vente_sur_12_mois.php
Version du : 08/07/2009
Par : Thierry POULAIN
*/
?>
<div style="margin: 5px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
    <table width="1300" border="0" cellspacing="0" cellpadding="2">
		<?php
        	if ($_GET['pID']!=''){
				?>
                <tr>
                    <td class="main" style="font-weight: bold;">Les Mois pr&eacute;c&eacute;dents</td>
                    <td rowspan="2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="24"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') ?> </td>
                                <?php
                                ### DEBUT Recuperation de la date d'il y a 12 mois
                                $Y=date('Y');
                                $M=date('n');
                                $M=$M+1;
                                
                                for($i=12; $i>0; $i--){
									if($M==1){ 
										$Y=date('Y')-1;
										$M=13;
										
										for($i;$i>0; $i--){
											$M=$M-1;
											/*if($M<10){
												$M='0'.$M;
											}*/
											$NOW=date($Y.'-'.$M);
										}
									} 
									else {
										$Y=date('Y');
										$M=$M - 1;
										/*if($M<10){
											$M='0'.$M;
										}*/
										$NOW=date($Y.'-'.$M);
									}
                                }
                                ### FIN Recuperation de la date d'il y a 12 mois
                                
                                $Somme=0;
                                $Nb_NC=0;
                                list($an,$mois)=split("-",$NOW);
                                
                                for($i=1; $i<13; $i++){
									if($mois==13){
										$an=$an+1; $mois=1;
										
										if($mois<10)
										{$mois='0'.$mois;}
										
										$NOW=$an."-".$mois;
										$debut=date($an.'-'.$mois.'-01 00:00:00');
										$F=$mois+1;
										
										if($F<10)
										{$F='0'.$F;}
										
										$fin=date($an.'-'.$F.'-01 00:00:00');
										
										$requete="select SUM(products_quantity) as Total from orders O, orders_products P where P.orders_id=O.orders_id and products_id=".$_GET['pID']." and  date_purchased > '" . $debut . "' and date_purchased < '".$fin."' ";
										$order_query = tep_db_query($requete);
										
										$TOTAL=tep_db_fetch_array($order_query);
										
										$date_ajout="select products_date_added from products where products_id=".$_GET['pID'];
										$resultat=tep_db_query($date_ajout);
										$AJOUT=tep_db_fetch_array($resultat);
										
										//echo $debut . "***" . $fin . "=>" . $TOTAL['Total'] . "<br />";
										
										if($AJOUT['products_date_added']>$fin){
											$res="N.C";
											$Nb_NC=$Nb_NC+1;
										} else {
											if($TOTAL['Total']==''){
												$TOTAL['Total']=0;
											}
											$res=$TOTAL['Total'];
											$Somme=$Somme+$TOTAL['Total'];
										}
										?>
										
										<td width="8%">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="center" class="smalltext"><?php echo mois_francais($mois); ?></td>
												</tr>
												<tr>
													<td align="center" class="main"><?php echo $res; ?></td>
												</tr>
											</table>
										</td>
								
										<?php
										$mois=$mois+1;
									} else {
										if($mois<10)
										{ $mois='0'.$mois; }
										
										$NOW=$an."-".$mois;
										$debut=date($an.'-'.$mois.'-01 00:00:00');
										$F=$mois+1;
										
										if($F<10)
										{ $F='0'.$F; }
										
										$fin=date($an.'-'.$F.'-01 00:00:00');
										
										$requete="select SUM(products_quantity) as Total from orders O, orders_products P where P.orders_id=O.orders_id and products_id=".$_GET['pID']." and  date_purchased > '" . $debut . "' and date_purchased < '".$fin."' ";
										
										$order_query = tep_db_query($requete);
										
										$TOTAL=tep_db_fetch_array($order_query);
										
										$date_ajout="select products_date_added from products where products_id=".$_GET['pID'];
										$resultat=tep_db_query($date_ajout);
										$AJOUT=tep_db_fetch_array($resultat);
										
										//echo $debut . "***" . $fin . "=>" . $TOTAL['Total'] . "<br />";
										
										if($AJOUT['products_date_added']>$fin)
										{
											$res="N.C";
											$Nb_NC=$Nb_NC+1;
										}else{
											if($TOTAL['Total']=='')
											{ $TOTAL['Total']=0; }
											
											$res=$TOTAL['Total'];
											$Somme=$Somme+$TOTAL['Total'];
										}
										
										?>
								
										<td width="8%">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="center" class="smalltext"><?php echo mois_francais($mois); ?></td>
												</tr>
												<tr>
													<td align="center" class="main"><?php echo $res; ?></td>
												</tr>
											</table>
										</td>
								
										<?php
										$mois=$mois+1;
									}
                                }
                                
                                if ($Nb_NC != 12){
									$moyenne=($Somme/(12-$Nb_NC));
									$moyenne_arr=round($moyenne,2);
                                } 
                                else {
									$moyenne_arr=0;
                                }
                                ?>
                        
                                <td width="10%">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" class="smalltext"><?php echo Moyenne; ?></td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="main"><?php echo $moyenne_arr; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td class="main" style="font-weight: bold;">Vente par Mois</td>
                </tr>
			<?
            } 
		?>
    </table>
</div>