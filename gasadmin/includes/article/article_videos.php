<?php
/* include : artilce_videos.php
Version du : 31/08/2018
Par : Thierry POULAIN */
?>

<div style="height: 225px; margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td class="main" valign="top">
				Vid�o de l'article 1<br>
				<span class="smalltext">(Entrer que le code de la vid�o Youtube)</span>
			</td>
			<td class="main">
				<?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_video_1', $pInfo->products_video_1) . ' &nbsp;'; ?>
            </td>
        </tr>
		<tr>
            <td class="main" valign="top">
				Largeur de la vid�o 1<br />
				<span class="smalltext">(Uniquement des chiffres. Si 0 la taille est de 940px)</span>
			</td>
			<td class="main">
				<?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_video_1_largeur', $pInfo->products_video_1_largeur); ?>
            </td>
        </tr>
		<tr>
            <td class="main" valign="top">
				Hauteur de la vid�o 1<br />
				<span class="smalltext">(Uniquement des chiffres. Si 0 la taille est de 658px)</span>
			</td>
			<td class="main">
				<?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_video_1_hauteur', $pInfo->products_video_1_hauteur); ?>
            </td>
        </tr>
		
		<tr>
			<td colspan="2" class="main" style="height:20px;"> </td>
		</tr>
		
		<tr>
            <td class="main" valign="top">
				Vid�o de l'article 2<br>
				<span class="smalltext">(Entrer que le code de la vid�o Youtube)</span>
			</td>
			<td class="main">
				<?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_video_2', $pInfo->products_video_2) . ' &nbsp;'; ?>
            </td>
        </tr>
		<tr>
            <td class="main" valign="top">
				Largeur de la vid�o 2<br />
				<span class="smalltext">(Uniquement des chiffres. Si 0 la taille est de 940px)</span>
			</td>
			<td class="main">
				<?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_video_2_largeur', $pInfo->products_video_2_largeur); ?>
            </td>
        </tr>
		<tr>
            <td class="main" valign="top">
				Hauteur de la vid�o 2<br />
				<span class="smalltext">(Uniquement des chiffres. Si 0 la taille est de 658px)</span>
			</td>
			<td class="main">
				<?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_video_2_hauteur', $pInfo->products_video_2_hauteur); ?>
            </td>
        </tr>
		
        <tr>
            <td class="main" valign="top">Vid&eacute;o de l'article en .FLV (FLASH)<br /><span class="smalltext">(Dimension 569 x 320 pixels et Maxi 5 Mo)</span></td>
            <td class="main">
            <?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_file_field('products_video') . '
            <a href="' . tep_href_link(FILENAME_ARTICLE_EDIT_IMAGES_VIDEOS, tep_get_all_get_params(array('pID', 'action')) . 'pID=' . $_GET['pID'] . '&action=deleteprodvideo'). '" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer l\'image de base  (DEFINITIVEMENT) ?\'))    
            {return true;} else {return false;}"><img src="' .  DIR_WS_IMAGES . 'icons/supprimer.png'  . '" alt="Supprimer" style="vertical-align: middle; border: 0px; margin-left: 5px;"/></a>
            <br />' . tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;<span class="smalltext">Actuellement : ' . $pInfo->products_video . tep_draw_hidden_field('products_previous_video', $pInfo->products_video) . '</span>'; ?></td>
        </tr>
    </table>
</div>