<?php
/* 
include : article_vente_flash.php 
Version du : 08/07/2009
Par : Thierry POULAIN
*/

$infos_ventes_flash_query = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS_VENTES_FLASH . " WHERE products_id = '" . $_GET['pID'] . "'");
$infos_ventes_flash = tep_db_fetch_array($infos_ventes_flash_query);

?>

<div style="margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
    <table style="margin: auto; margin-top: 14px; margin-bottom: 14px; text-align: center;" border="0" width="50%" cellspacing="0" cellpadding="0">
        <tr>
            <th>Date de d&eacute;but</th>
            <th>Heure de d&eacute;but</th>
        </tr>
        <tr>
            <td><input type="text" value="<?php if($infos_ventes_flash['debut_vente_flash'] != '') echo date('d-m-Y', strtotime($infos_ventes_flash['debut_vente_flash'])); ?>" name="date_deb_flash" id="date_deb_flash"/>&nbsp;&nbsp;<a href="#validation" onClick="displayCalendar(date_deb_flash,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" style="vertical-align: middle; border: 0px;"></a></td>
            <td>
                <select name="heure_deb_flash" id="heure_deb_flash">
                <?php
                    for ($i=0; $i<24; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>" <?php if($i == date('H', strtotime($infos_ventes_flash['debut_vente_flash']))) echo 'selected="selected"'; ?>><?php echo $i; ?></option>
                        <?php
                    }
                ?>
                </select>
                h
                <select name="minutes_deb_flash" id="minutes_deb_flash">
                <?php
                    for ($i=0; $i<60; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>" <?php if($i == date('i', strtotime($infos_ventes_flash['debut_vente_flash']))) echo 'selected="selected"'; ?>><?php echo $i; ?></option>
                        <?php
                    }
                ?>
                </select>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <th>Date de fin</th>
            <th>Heure de fin</th>
        </tr>
        <tr>
            <td><input type="text" value="<?php if($infos_ventes_flash['fin_vente_flash'] != '') echo date('d-m-Y', strtotime($infos_ventes_flash['fin_vente_flash'])); ?>" name="date_fin_flash" id="date_fin_flash" />&nbsp;&nbsp;<a href="#validation" onClick="displayCalendar(date_fin_flash,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" style="vertical-align: middle; border: 0px;"></a></td>
            <td>
                <select name="heure_fin_flash" id="heure_fin_flash">
                <?php
                    for ($i=0; $i<24; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>" <?php if($i == date('H', strtotime($infos_ventes_flash['fin_vente_flash']))) echo 'selected="selected"'; ?>><?php echo $i; ?></option>
                        <?php
                    }
                ?>
                </select>
                h
                <select name="minutes_fin_flash" id="minutes_fin_flash">
                <?php
                    for ($i=0; $i<60; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>" <?php if($i == date('i', strtotime($infos_ventes_flash['fin_vente_flash']))) echo 'selected="selected"'; ?>><?php echo $i; ?></option>
                        <?php
                    }
                ?>
                </select>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td style="font-weight: bold;">Prix de Vente Flash (HT)</td>
            <td><input type="text" value="<?php echo $infos_ventes_flash['prix_vente']; ?>" name="prix_vente_flash" id="prix_vente_flash" /></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
        	<td colspan="2"><input type="button" value="Supprimer la vente flash" onclick="suppr_vente_flash(<?php echo $_GET['pID']; ?>);" /></td>
        </tr>
    </table>
</div>