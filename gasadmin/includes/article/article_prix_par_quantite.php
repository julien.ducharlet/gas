<?php
/* 
include : article_prix_par_quantite.php
Version du : 08/07/2009
Par : Thierry POULAIN
*/
?>

<div style="margin: 5px; margin-bottom: 10px; padding: 5px; padding-bottom: 13px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
	<?php
		$infos_prices_query = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS_PRICE_BY_QUANTITY . " WHERE products_id = " . $_GET['pID']);
		$infos_prices = tep_db_fetch_array($infos_prices_query);
		
		$price = array("part_1" => $infos_prices['part_price_by_1'], "pro_1" => $infos_prices['pro_price_by_1'], "rev_1" => $infos_prices['rev_price_by_1'], "adm_1" => $infos_prices['adm_price_by_1'],
					   "part_5" => $infos_prices['part_price_by_5'], "pro_5" => $infos_prices['pro_price_by_5'], "rev_5" => $infos_prices['rev_price_by_5'], "adm_5" => $infos_prices['adm_price_by_5'],
					   "part_10" => $infos_prices['part_price_by_10'], "pro_10" => $infos_prices['pro_price_by_10'], "rev_10" => $infos_prices['rev_price_by_10'], "adm_10" => $infos_prices['adm_price_by_10']);
	?>
    <table style="width: 75%; text-align: center; margin: auto;" id="tab_prix_qte">
        <tr>
            <th>&nbsp;</th>
            <th>Prix part.</th>
            <th>Prix prof.</th>
            <th>Prix rev.</th>
            <th>Prix adm.</th>
        </tr>
        <tr>
            <td>&nbsp;pour 1</td>
            <td><input type="text" value="<?php echo $price['part_1']; ?>" name="price_part_1" id="price_part_1" size="8"/></td>
            <td><input type="text" value="<?php echo $price['pro_1']; ?>" name="price_pro_1" id="price_pro_1" size="8"/></td>
            <td><input type="text" value="<?php echo $price['rev_1']; ?>" name="price_rev_1" id="price_rev_1" size="8"/></td>
            <td><input type="text" value="<?php echo $price['adm_1']; ?>" name="price_adm_1" id="price_adm_1" size="8"/></td>
        </tr>
        <tr>
            <td>&nbsp;de 5 &agrave; 9&nbsp;</td>
            <td><input type="text" value="<?php echo $price['part_5']; ?>" name="price_part_5" id="price_part_5" size="8"/></td>
            <td><input type="text" value="<?php echo $price['pro_5']; ?>" name="price_pro_5" id="price_pro_5" size="8"/></td>
            <td><input type="text" value="<?php echo $price['rev_5']; ?>" name="price_rev_5" id="price_rev_5" size="8"/></td>
            <td><input type="text" value="<?php echo $price['adm_5']; ?>" name="price_adm_5" id="price_adm_5" size="8"/></td>
		</tr>
		<tr>
        	<td>&nbsp;10 et plus&nbsp;</td>
            <td><input type="text" value="<?php echo $price['part_10']; ?>" name="price_part_10" id="price_part_10" size="8"/></td>
            <td><input type="text" value="<?php echo $price['pro_10']; ?>" name="price_pro_10" id="price_pro_10" size="8"/></td>
            <td><input type="text" value="<?php echo $price['rev_10']; ?>" name="price_rev_10" id="price_rev_10" size="8"/></td>
            <td><input type="text" value="<?php echo $price['adm_10']; ?>" name="price_adm_10" id="price_adm_10" size="8"/></td>
		</tr>
        <tr>
        	<td>&nbsp;</td>
            <td><input type="button" value="Taux par d�faut" onclick="prix_par_qte_defaut(<?php echo $_GET['pID']; ?>, 'part')"/></td>
            <td><input type="button" value="Taux par d�faut" onclick="prix_par_qte_defaut(<?php echo $_GET['pID']; ?>, 'pro')"/></td>
            <td><input type="button" value="Taux par d�faut" onclick="prix_par_qte_defaut(<?php echo $_GET['pID']; ?>, 'rev')"/></td>
            <td><input type="button" value="Taux par d�faut" onclick="prix_par_qte_defaut(<?php echo $_GET['pID']; ?>, 'adm')"/></td>
        </tr>

    </table>
</div>
