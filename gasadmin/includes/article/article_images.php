<?php
/* 
include : artilce_images.php
Version du : 08/07/2009
Par : Thierry POULAIN
*/
?>

<div style="height: 215px; margin: 5px; margin-bottom: 10px; padding: 5px; padding-top: 15px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td class="main" valign="top">Image 1 de l'article<br /><span class="smalltext">(Dimension 800 x 800 pixels)</span></td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_file_field('products_bimage') . tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;<a href="../' . DIR_WS_SITE.'/images/products_pictures/grande/' . $pInfo->products_bimage . '" target="_blank">' . $pInfo->products_bimage . '</a>' . tep_draw_hidden_field('products_previous_bimage', $pInfo->products_bimage); ?></td>
        </tr>
        <?php for($i=2;$i<6;$i++){?>
            <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
            <tr>
            	<td class="main" valign="top">Image <?php echo $i;?> de l'article<br /><span class="smalltext">(Dimension 800 x 800 pixels)</span></td>
                <td class="main">
                    <?php
                        echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_file_field('products_bimage'.$i) . tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;<a href="../'.DIR_WS_SITE.'/images/products_pictures/grande/'.$products_bimage_name_supp[$i].'" target="_blank">' . $products_bimage_name_supp[$i] ."</a>";
                        if(!empty($products_bimage_name_supp[$i])){
                            echo tep_draw_hidden_field('products_previous_bimage'.$i, $products_bimage_name_supp[$i]);
                            echo '<a href="' . tep_href_link(FILENAME_ARTICLE_EDIT_IMAGES_VIDEOS, tep_get_all_get_params(array('pID','action2','image_name')) . 'pID=' . $_GET['pID'] . '&image_name='.$products_bimage_name_supp[$i].'&action2=deleteimage'). '" onclick="if (window.confirm(\'Etes vous sur de vouloir effacer l\'image de base  (DEFINITIVEMENT) ?\'))    
                            {return true;} else {return false;}"><img src="' .  DIR_WS_IMAGES . 'icons/supprimer.png'  . '" alt="Supprimer" style="vertical-align: middle; border: 0px; margin-left: 5px;"/></a>';
                        }
                    ?>
                </td>
            </tr>
        <?php }?>
    </table>
</div>