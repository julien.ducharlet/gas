<?php
/* 
include : article_stock_global.php
Version du : 08/07/2009
Par : Thierry POULAIN
*/
?>
<div style="margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF; font-size: 1em;">

<script language="javascript"><!--
	
	function maj_stock_bdd(pID) {
		
		stock_reel = $("#products_quantity_reel").val();
		stock_virtuel = $("#products_quantity").val();
		
		$.ajax({
			type: "GET",
			cache: false,
			url: 'includes/article/article_stock_global_ajax.php?stock_reel='+ stock_reel +'&stock_virtuel='+ stock_virtuel +'&pID='+ pID,
			data: 'lightbox=no',
			dataType: "text",
			processData: false,
			success: function(data) {
				
			}
		});
	}
	
	function maj_qte_bdd(pID, type) {
		
		qte = $("#products_quantity_"+ type).val();
		
		$.ajax({
			type: "GET",
			cache: false,
			url: 'includes/article/article_stock_global_ajax.php?type='+ type +'&valeur='+ qte +'&pID='+ pID,
			data: 'lightbox=no',
			dataType: "text",
			processData: false,
			success: function(data) {
				
			}
		});
	}
//--></script>
    <form name="new_product" method="post" action="#">
        <?php
			
            $recherche_options = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS_ATTRIBUTES . " WHERE products_id = " . $_GET['pID']);
			
			$hasOption = (tep_db_num_rows($recherche_options)>0) ? 1 : 0;
			//$editable = ($_SERVER['PHP_SELF']==BASE_DIR_ADMIN .'/article_edit_prix_fournisseurs.php' && $hasOption) ? false : true;
			$disable = ($hasOption) ? 'disabled="disabled"' : '';
			
                ?>
                <table width="100%">
                    <tr>
                    	<td class="main" align="left">
                        <?php if($_SERVER['PHP_SELF'] == BASE_DIR_ADMIN .'/article_edit_stocks_options.php') { ?>
                        	
                        	<img border="0" title="Info" alt="Info" src="images/icons/editer.png" class="edit">
                        <?php } ?>
                        </td>
                        <td class="main" align="center">
                            <?php
							
                                echo 'Stock r&eacute;el : ';
								
								if($hasOption) {
									
                                	echo tep_draw_input_field('products_quantity_reel', $pInfo->products_quantity_reel,'maxlength="4" size="4" style="text-align:center;"  disabled="disabled" "id="products_quantity_reel"');
								}
								else {
									
									echo tep_draw_input_field('products_quantity_reel', $pInfo->products_quantity_reel,'maxlength="4" size="4" style="text-align:center;" onBlur="updateStock(); maj_stock_bdd(' . $_GET['pID'] . '); "id="products_quantity_reel"');
								}
								
                                echo tep_draw_input_field('products_quantity_reel_old', $pInfo->products_quantity_reel,'maxlength="4" size="4" id="products_quantity_reel_old" style="text-align:center;"',false,"hidden");
                            ?>
                        </td>
                        <td class="main" align="center">
                            <?php 
                                echo 'Stock virtuel : '; 
                                //echo tep_draw_input_field('products_quantity', $pInfo->products_quantity,'maxlength="4" size="4" style="text-align:center;"',false,"hidden");
                                echo tep_draw_input_field('products_quantity', $pInfo->products_quantity, 'maxlength="4" size="4" id="products_quantity" style="text-align:center;" disabled '); 
                            ?>
                        </td>

                        <?php
                        	if (tep_db_num_rows($recherche_options) > 0) {
								$qte_min = '';
								$qte_ideale = '';
								$qte_max = '';
							} else { 
								$qte_min = $pInfo->products_quantity_min;
								$qte_ideale = $pInfo->products_quantity_ideal;
								$qte_max = $pInfo->products_quantity_max;
							}
						?>
                        
                        <td class="main" align="center"><?php echo 'Qt&eacute; min (alerte stock): '; echo tep_draw_input_field('products_quantity_min', $pInfo->products_quantity_min,'maxlength="4" size="4" style="text-align:center;" id="products_quantity_min" onBlur="maj_qte_bdd(' . $_GET['pID'] . ', \'min\');"'); ?></td>
                        <td class="main" align="center"><?php echo 'Qt&eacute; id&eacute;ale : '; echo tep_draw_input_field('products_quantity_ideal', $pInfo->products_quantity_ideal,'maxlength="4" size="4" style="text-align:center;" id="products_quantity_ideal" onBlur="maj_qte_bdd(' . $_GET['pID'] . ', \'ideal\');"'); ?></td>
                        <td class="main" align="center"><?php echo 'Qt&eacute; max : '; echo tep_draw_input_field('products_quantity_max', $pInfo->products_quantity_max,'maxlength="4" size="4" style="text-align:center;" id="products_quantity_max" onBlur="maj_qte_bdd(' . $_GET['pID'] . ', \'max\');"'); //MODIF QUANTITE ?></td>
                    </tr>
                    <tr>
                
                	<?php
                		if($_SERVER['PHP_SELF']==BASE_DIR_ADMIN .'/article_edit_prix_fournisseurs.php') {
							
							if($hasOption) {
								
								echo '<td colspan="3" class="main" style="color:#FF0000;text-align: center; font-size: 0.8em; font-weight: bold;">Champs verrouillés. Pour les modifier cliquez ici : <a href="article_edit_stocks_options.php?cPath=<'. $_GET['cPath'] .'&pID='. $_GET['pID'] .'&action=new_product" style="font-weight: bold; font-size: 1em; color: red;">Stocks / Options</a>.</td><td colspan="3" class="main" style="color:#FF0000;text-align: center; font-size: 0.8em; font-weight: bold;">Modifier les champs ci-dessus dans les options de l\'article.</td>';
							}
						}
					?>
                	
                    </tr>
                </table>
</div>
