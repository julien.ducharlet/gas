<?php
require("../configure.php");
require("../database_tables.php");
require("../functions/general.php");
require("../functions/database.php");

tep_db_connect();

$type_client = $_GET['col'];

switch($type_client) {
	case 'part' : 
		$price_by_1 = $_GET['prix_vente'] * TAUX_PART_BY_1;
		$price_by_5 = $_GET['prix_vente'] * TAUX_PART_BY_5;
		$price_by_10 = $_GET['prix_vente'] * TAUX_PART_BY_10;
	break;
	
	case 'pro' : 
		$price_by_1 = $_GET['prix_vente'] * TAUX_PRO_BY_1;
		$price_by_5 = $_GET['prix_vente'] * TAUX_PRO_BY_5;
		$price_by_10 = $_GET['prix_vente'] * TAUX_PRO_BY_10;
	break;
	
	case 'rev' : 
		$price_by_1 = $_GET['prix_achat'] * TAUX_REV_BY_1;
		$price_by_5 = $_GET['prix_achat'] * TAUX_REV_BY_5;
		$price_by_10 = $_GET['prix_achat'] * TAUX_REV_BY_10;
	break;
	
	case 'adm' : 
		$price_by_1 = $_GET['prix_vente'] * TAUX_ADM_BY_1;
		$price_by_5 = $_GET['prix_vente'] * TAUX_ADM_BY_5;
		$price_by_10 = $_GET['prix_vente'] * TAUX_ADM_BY_10;
	break;
	
	default : 
	break;
}

// On regarde si l'entr�e existe dans la base
$existe = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS_PRICE_BY_QUANTITY . " WHERE products_id = '" . $_GET['products_id'] . "'");

//Si oui on met � jour sinon on cr�e une nouvelle entr�e dans la base
if (tep_db_num_rows($existe) != 0) {
	
	tep_db_query("UPDATE " . TABLE_PRODUCTS_PRICE_BY_QUANTITY . " 
				  SET ". $type_client ."_price_by_1 = '" . $price_by_1 . "', ". $type_client ."_price_by_5 = '" . $price_by_5 . "', ". $type_client ."_price_by_10 = '" . $price_by_10 . "' WHERE products_id = '" . $_GET['products_id'] . "'");
} else {
	
	tep_db_query("INSERT INTO " . TABLE_PRODUCTS_PRICE_BY_QUANTITY . " (". $type_client ."_price_by_1, ". $type_client ."_price_by_5, ". $type_client ."_price_by_10, products_id) 
				  VALUES ('" . $price_by_1 . "', '" . $price_by_5 . "', '" . $price_by_10 . "', '" . $_GET['products_id'] . "')");
}

// On affiche ces nouvelles infos
echo sprintf('%.4f', $price_by_1) .','. sprintf('%.4f', $price_by_5) .','. sprintf('%.4f', $price_by_10);
