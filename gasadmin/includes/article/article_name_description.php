<?php
/* 
include : artilce_name_description.php
Version du : 09/07/2009
Par : Thierry POULAIN
*/

if (isset($hauteur) && $hauteur != '') {
	$hauteur -= 110;
	echo '<div style="height: ' . $hauteur . 'px; margin: 5px; margin-bottom: 10px; padding: 60px 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">';
} else {
	echo '<div style="margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">';
}
?>
    <table>
        <?php
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            ?>
            <tr>
                <td style="width: 300px;"><?php if ($i == 0) echo 'Nom de l\'article :'; ?></td>
                <td>
                    <?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('products_name[' . $languages[$i]['id'] . ']', (isset($products_name[$languages[$i]['id']]) ? $products_name[$languages[$i]['id']] : tep_get_products_name($pInfo->products_id, $languages[$i]['id'])),'size="73"'); ?>
                </td>
            </tr>
            <?php
        }
        ?>
        
        <?php for ($i=0, $n=sizeof($languages); $i<$n; $i++) { ?>
            <tr>
                <td valign="right">
                    <?php if ($i == 0) echo 'Description de l\'article :'; ?>
                </td>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="main" valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                            <td class="main"><?php echo tep_draw_textarea_field('products_description[' . $languages[$i]['id'] . ']', 'soft', '70', '15', (isset($products_description[$languages[$i]['id']]) ? stripslashes($products_description[$languages[$i]['id']]) : tep_get_products_description($pInfo->products_id, $languages[$i]['id']))); ?>	  </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>
