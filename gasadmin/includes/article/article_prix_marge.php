<?php
/* 
include : article_prix_marge.php 
Version du : 08/07/2009
Par : Thierry POULAIN
*/
?>

<div style="margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
    	<tr>
            <td class="main">TVA de l'article :</td>
            <td align="left"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_pull_down_menu('products_tax_class_id', $tax_class_array, $pInfo->products_tax_class_id, 'id="products_tax_class_id" onchange="updateGross(); s_updateGross()"'); ?></td>
        </tr>
        <tr>
            <td class="main">Prix d'achat (HT) :</td>
            <td align="left"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_cost', $pInfo->products_cost, 'id="prix" size="13" onKeyUp="updateMarge();"'); ?></td>
        </tr>
        <tr>
            <td class="main">Prix pour Particulier (HT et TTC) :</td>
            <td align="left">
                <?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_price_aff', $pInfo->products_price, 'size="13" id="prix_vente" disabled  onKeyUp="updateGross()"'); ?>
                <?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_price', $pInfo->products_price, 'id="products_price" size="13"  onKeyUp="updateGross()"',false,"hidden"); ?>
                soit <?php echo '&nbsp;' . tep_draw_input_field('products_price_gross', $pInfo->products_price, 'id="products_price_gross" size="13" OnKeyUp="updateNet()"'); ?>&nbsp;&nbsp;TTC
            </td>
        </tr>          
        <tr>
            <td class="main">Marge (HT) :</td>
            <td align="left"><?php 
                echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_marge_aff', '', 'id="products_marge_aff" size="13" disabled');
                echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;&nbsp;';
                ?>
                soit 
                <?php echo '&nbsp;' . tep_draw_input_field('products_marge_pourcent', '', 'id="products_marge_pourcent" size="13" disabled').' de coefficient'; ?>
            </td>
        </tr>
        
        <tr>
            <td class="main">Ce produit a une Ecotaxe de : </td>
            <td align="left"><?php 
                echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_ecotaxe', $pInfo->products_ecotaxe, 'size="13"');
                echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;&nbsp;';
                ?>
            </td>
        </tr>
        
        <script language="javascript"><!--
        updateGross();
        updateMarge();
        //--></script>
    </table>
</div>