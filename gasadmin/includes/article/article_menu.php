<?php
/* 
include : artilce_menu.php
Version du : 08/07/2009
Par : Thierry POULAIN
*/
?>

<div style="background-color: #FFC; border: 1px solid #F90; padding: 2px; margin: 5px; margin-bottom: 10px;">
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td><a href="<?php echo tep_href_link(FILENAME_ARTICLE_EDIT, tep_get_all_get_params(array('action')));?>&action=new_product">Informations</a></td>
            <?php if (isset($_GET['pID']) && $_GET['pID'] != '') { ?>
                <td><a href="<?php echo tep_href_link(FILENAME_ARTICLE_EDIT_IMAGES_VIDEOS, tep_get_all_get_params(array('action')));?>&action=new_product">Pictogrammes</a></td>
                <td><a href="<?php echo tep_href_link(FILENAME_ARTICLE_EDIT_PRIX_FOURNISSEURS, tep_get_all_get_params(array('action')));?>&action=new_product">Prix / Fournisseurs</a></td>
                <td><a href="<?php echo tep_href_link(FILENAME_ARTICLE_EDIT_STOCKS_OPTIONS, tep_get_all_get_params(array('action')));?>&action=new_product">Stocks / Options</a></td>
                <td><a href="<?php echo tep_href_link(FILENAME_ARTICLE_EDIT_REFERENCEMENT, tep_get_all_get_params(array('action')));?>&action=new_product">R&eacute;f&eacute;rencement</a></td>
                <td><a href="<?php echo tep_href_link(FILENAME_ARTICLE_EDIT_STATISTIQUES, tep_get_all_get_params(array('action')));?>&action=new_product">Statistiques</a></td>
                <td><a href="<?php echo tep_href_link(FILENAME_DUPLICATE) . "?cPath=". $_GET['cPath'] ."&pID=" . $_GET['pID'];?>">Dupliquer</a></td>
                <td><a href="<?php echo tep_href_link(FILENAME_ARTICLE_LIST) . "?cPath=". $_GET['cPath'] ."&pID=" . $_GET['pID'] ."&action=delete_product";?>">Supprimer</a></td>
                <td><a href="<?php echo tep_href_link(FILENAME_ARTICLE_TO_CATEGORIES, tep_get_all_get_params(array('action')));?>&action=new_product">Dans les catégories</a></td>
				<td><a href="cmd_en_attente.php?pID=<?php echo $_GET['pID'] ;?>" target="blank">CMD avec l'article</a></td>
			<?php } else { ?>
				<td style="color: #CCC;" class="smallText">Images / Vid&eacute;os</td>
                <td style="color: #CCC;" class="smallText">Pictogrammes</td>
                <td style="color: #CCC;" class="smallText">Stocks / Options</td>
                <td style="color: #CCC;" class="smallText">R&eacute;f&eacute;rencement</td>
                <td style="color: #CCC;" class="smallText">Statistiques</td>
                <td style="color: #CCC;" class="smallText">Dupliquer</td>
            	<td style="color: #CCC;" class="smallText">Dans les catégories</td>
				<td style="color: #CCC;" class="smallText">CMD avec l'article</td>
			<?php } ?>
        </tr>
    </table>
</div>
