<?php
/* 
include : article_prix_promotion.php 
Version du : 08/07/2009
Par : Thierry POULAIN
*/
?>

<div style="margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
	<?php 
		$recup_infos_promo = tep_db_query("SELECT * FROM " . TABLE_SPECIALS . " WHERE products_id = '" . $_GET['pID'] . "'");
		$infos_promo = tep_db_fetch_array($recup_infos_promo);
	?>
    <table border="0" cellspacing="0" cellpadding="5" id="table_specials">
        <tr>
            <td colspan="2" align="center">
                <div id="ajax_chargement"></div>
            </td>
        </tr>
        
        <tr>
            <td class="main"><?php echo 'Prix promotionnel (HT) :'; ?></td>
            <td class="main"><input type="text" name="specials_price" id="specials_price" value="<?php echo $infos_promo['specials_new_products_price']; ?>" onKeyUp="s_updateGross()" /></td>
        </tr>
        
        <tr>
            <td class="main"><?php echo 'Prix promotionnel (TTC) :'; ?></td>
            <td class="main"><input type="text" name="s_products_price_gross" id="s_products_price_gross" value="<?php if ($infos_promo['specials_new_products_price'] != 0) echo money_format('%i', $infos_promo['specials_new_products_price']*1.2); ?>" OnKeyUp="s_updateNet()" /></td>
        </tr>
        
        <tr>
            <td class="main"><?php echo 'Date d\'expiration :'; ?>&nbsp;</td>
            <td><input type="text" id="date_fin_promo" name="date_fin_promo" value="<?php if ($infos_promo['expires_date'] != 0) echo date('d-m-Y', strtotime($infos_promo['expires_date'])); ?>"  />&nbsp;&nbsp;<a href="#validation" onClick="displayCalendar(date_fin_promo,'dd-mm-yyyy',this)"><img src="images/icons/calendar.jpg" style="vertical-align: middle; border: 0px;"></a></td>
        </tr>
        
        <tr>
        	<td colspan="2" style="text-align: center;"><input type="button" value="Supprimer la Promotion" onclick="suppr_promo(<?php echo $_GET['pID']; ?>);" /></td>
		</tr>
        
    </table>
    
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
            <td colspan="2" class="main"><br><?php echo '<b>Remarque sur les promotions :</b><ul><li>Si vous entrez un prix, le s&eacute;parateur d&eacute;cimal doit &ecirc;tre un \'.\' (point d&eacute;cimal), exemple: <b>49.99</b></li><li>Laissez la date d\'&eacute;ch&eacute;ance vide pour une promotion illimit�e</li></ul>'; ?></td>
        </tr>
    </table>
</div>