<?php
/* 
include : article_logos.php
Version du : 08/07/2009
Par : Thierry POULAIN
*/
?>

<div style="margin: 5px; margin-bottom: 10px; padding: 5px; border-top: 1px solid #090; border-bottom: 1px solid #090; background-color: #EFF;">
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>
            <?php
				
				//$query = 'insert into '. TABLE_PRODUCTS_LOGOS .' (logos_name, logos_hover) value=(\''. $_POST['logo_name'] .', \''. $_POST['logo_hover'] .'\')';
            	
				$logos = array();
				$i = 0;
				
				$query = 'select logo_id from '. TABLE_PRODUCTS_TO_LOGOS .' where products_id='. $_GET['pID'];
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$logos[] = $data['logo_id'];
				}
				
				$query = 'select logo_id, logo_name from '. TABLE_PRODUCTS_LOGOS .' order by logo_name';
				$query = tep_db_query($query);
				
				while($data = tep_db_fetch_array($query)) {
					
					$checked = (in_array($data['logo_id'], $logos)) ? 'checked="checked"' : '';
					?>
                    
                    <div style="float:left; margin:5 10 10 10px; text-align:center;">
                    	<img alt="<?php echo $data['logo_name']; ?>" src="<?php echo DIR_WS_CATALOG_IMAGES_LOGOS . $data['logo_name']; ?>" style="max-width:100px;"/><br />
                    	<input type="checkbox" <?php echo $checked; ?> name="logo_<?php echo $i; ?>" />
                        <input type="hidden" name="logo_id_<?php echo $i; ?>" value="<?php echo $data['logo_id']; ?>" />
                    </div>
                    
                    <?php
					
					$i++;
				}
			?>
            
            <input type="hidden" name="nbre_logos" value="<?php echo $i; ?>"  />
            
            <div style="clear:both"></div>
            </td>
        </tr>
    </table>
</div>