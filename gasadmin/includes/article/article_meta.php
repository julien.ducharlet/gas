<?php
/* 
include : artilce_meta.php
Version du : 09/07/2009
Par : Thierry POULAIN
*/
?>


<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<?php
                        $parameters = array('products_url' => '',
                                            'products_baseline' => '',
                                            'balise_title_lien_image' => '',
                                            'balise_title_lien_texte' => '',
                                            'products_date_added' => '',
                                            'products_last_modified' => '',
                                            'products_date_available' => '',
                                            'products_status' => '');
                        
                        $pInfo = new objectInfo($parameters);
                        
                        $product_query = tep_db_query("select pd.products_name, pd.products_description, pd.products_head_title_tag, pd.products_head_desc_tag, pd.products_head_keywords_tag, pd.products_url, pd.products_baseline, pd.balise_title_lien_image, pd.balise_title_lien_texte, p.products_id, p.products_date_added, p.products_last_modified from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$HTTP_GET_VARS['pID'] . "' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "'");
                        $product = tep_db_fetch_array($product_query);
                        $pInfo->objectInfo($product);
                        
                        $languages = tep_get_languages();
                        
						echo tep_draw_form('new_product', FILENAME_ARTICLE_EDIT_REFERENCEMENT, 'cPath=' . $cPath . '&pID=' . $HTTP_GET_VARS['pID'] . '&action=update_product', 'post', 'enctype="multipart/form-data"');
                        ?>
                      
                        <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        
                                        <tr>
                                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                                        </tr>
                                        <?php         
                                        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                                        ?>
                                            
                                            <tr>
                                                <td class="main"><?php if ($i == 0) echo 'URL de l\'article <span class="smalltext">(idem au nom si vide)</span>'; ?></td>
	                                            <td>
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="main" valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                                            <td class="main"><?php echo tep_draw_input_field('products_url[' . $languages[$i]['id'] . ']', (isset($products_url[$languages[$i]['id']]) ? $products_url[$languages[$i]['id']] : tep_get_products_url($pInfo->products_id, $languages[$i]['id'])),'size="73"'); ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
											<tr>
                                                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="main" valign="top"><?php if ($i == 0) echo 'Titre de la page de l\'article <i><span class="smalltext">( Pas plus de 100 caractères )</span></i> :'; ?></td>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="main" valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                                            <td class="main"><?php echo tep_draw_textarea_field('products_head_title_tag[' . $languages[$i]['id'] . ']', 'soft', '70', '1', (isset($products_head_title_tag[$languages[$i]['id']]) ? stripslashes($products_head_title_tag[$languages[$i]['id']]) : tep_get_products_head_title_tag($pInfo->products_id, $languages[$i]['id']))); ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                                            </tr>          
                                            <tr>
                                                <td class="main" valign="top"><?php if ($i == 0) echo 'Description de l\'article <i><span class="smalltext">( Pas plus de 200 caractères )</span></i> : '; ?></td>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="main" valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                                            <td class="main"><?php echo tep_draw_textarea_field('products_head_desc_tag[' . $languages[$i]['id'] . ']', 'soft', '70', '2', (isset($products_head_desc_tag[$languages[$i]['id']]) ? stripslashes($products_head_desc_tag[$languages[$i]['id']]) : tep_get_products_head_desc_tag($pInfo->products_id, $languages[$i]['id']))); ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                                            </tr>          
                                            <tr>
                                                <td class="main" valign="top"><?php if ($i == 0) echo 'Mots cl&eacute;s de l\'article <i><span class="smalltext">( Pas plus de 1000 caract&egrave;res )</span></i> : '; ?></td>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="main" valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                                            <td class="main"><?php echo tep_draw_textarea_field('products_head_keywords_tag[' . $languages[$i]['id'] . ']', 'soft', '70', '5', (isset($products_head_keywords_tag[$languages[$i]['id']]) ? stripslashes($products_head_keywords_tag[$languages[$i]['id']]) : tep_get_products_head_keywords_tag($pInfo->products_id, $languages[$i]['id']))); ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
											<tr>
                                                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                                            </tr>
											<tr>
                                                <td class="main"><?php if ($i == 0) echo 'Balise TITLE au survol de ce produit pour les images'; ?></td>
	                                            <td>
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="main" valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                                            <td class="main"><?php echo tep_draw_input_field('balise_title_lien_image[' . $languages[$i]['id'] . ']', (isset($balise_title_lien_image[$languages[$i]['id']]) ? $products_url[$languages[$i]['id']] : tep_get_products_balise_title_lien_image($pInfo->products_id, $languages[$i]['id'])),'size="73"'); ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
											<tr>
                                                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="main"><?php if ($i == 0) echo 'Balise TITLE au survol de ce produit pour le texte'; ?></td>
	                                            <td>
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="main" valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                                            <td class="main"><?php echo tep_draw_input_field('balise_title_lien_texte[' . $languages[$i]['id'] . ']', (isset($balise_title_lien_texte[$languages[$i]['id']]) ? $products_url[$languages[$i]['id']] : tep_get_products_balise_title_lien_texte($pInfo->products_id, $languages[$i]['id'])),'size="73"'); ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
											<tr>
                                                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="main" valign="top"><?php if ($i == 0) echo 'BASELINE de cette cat&eacute;gorie pour le r&eacute;f&eacute;rencement'; ?></td>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="main" valign="top"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                                                            <td class="main"><?php echo tep_draw_textarea_field('products_baseline[' . $languages[$i]['id'] . ']', 'soft', '70', '2', (isset($products_baseline[$languages[$i]['id']]) ? stripslashes($products_baseline[$languages[$i]['id']]) : tep_get_products_baseline($pInfo->products_id, $languages[$i]['id']))); ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        <tr>
                                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="main"><hr></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                            </tr>
                            <tr>
                                <td class="main" align="center">
									<?php 
									echo tep_draw_hidden_field('products_date_added', (tep_not_null($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d')));
									echo tep_image_submit('button_update.gif', IMAGE_UPDATE);
									?>
								</td>
                            </tr>
                        </table>
                        </form>
		</td>
	</tr>
</table>
