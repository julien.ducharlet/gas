<?php

  class order {
    var $info, $totals, $products, $customer, $delivery;

    function order($order_id) {
      $this->info = array();
      $this->totals = array();
      $this->products = array();
      $this->customer = array();
      $this->delivery = array();

      $this->query($order_id);
    }

    function query($order_id) {
      $order_query = tep_db_query("SELECT * FROM " . TABLE_ORDERS . " WHERE orders_id = '" . (int)$order_id . "'");
      $order = tep_db_fetch_array($order_query);

      //<!-- ORDERS EDITOR AJAX -->
	  $totals_query = tep_db_query("SELECT title, text, value, class FROM " . TABLE_ORDERS_TOTAL . " WHERE orders_id = '" . (int)$order_id . "' ORDER BY sort_order");
	  //<!-- ORDERS EDITOR AJAX -->
	  
	  
	  while ($totals = tep_db_fetch_array($totals_query)) {
	  
	    //<!-- ORDERS EDITOR AJAX -->
        $this->totals[] = array('title' => $totals['title'],
														'text' => $totals['text'],
														'value' => $totals['value'],
														'class' => $totals['class']);

		//<!-- ORDERS EDITOR AJAX -->						
								
      }

      $this->info = array(
						  'currency' => $order['currency'],
                          'currency_value' => $order['currency_value'],
                          'payment_method' => $order['payment_method'],
                          'cc_type' => $order['cc_type'],
                          'cc_owner' => $order['cc_owner'],
                          'cc_number' => $order['cc_number'],
                          'cc_expires' => $order['cc_expires'],
						  'pays_cb_paiement' => $order['pays_cb_paiement'],
                          'date_purchased' => $order['date_purchased'],
						  'orders_date_fin_devis' => $order['orders_date_fin_devis'],
                          'orders_status' => $order['orders_status'],
                          'last_modified' => $order['last_modified'],
						  'poids' => $order['poids'],
						  'marge' => $order['marge'],
                          'total' => $order['total'],
						  'ss_total' => $order['ss_total'],
						  'tva_total' => $order['tva_total'],
						  'remise' => $order['remise'],
						  'remise_pourcent' => $order['remise_pourcent'],
						  'remise_porte_monnaie' => $order['remise_porte_monnaie'],
						  'frais_port' => $order['frais_port'],
						  'frais_port_client' => $order['frais_port_client'],
						  'tva_port' => $order['tva_port'],
						  'orders_numero_facture' => $order['orders_numero_facture'],
						  'orders_date_facture' => $order['orders_date_facture']);  
						  

      $this->customer = array('name' => $order['customers_name'],
                              'company' => $order['customers_company'],
                              'street_address' => $order['customers_street_address'],
                              'street_address_3' => $order['customers_street_address_3'],
                              'street_address_4' => $order['customers_street_address_4'],
                              'suburb' => $order['customers_suburb'],
                              'city' => $order['customers_city'],
                              'postcode' => $order['customers_postcode'],
                              'state' => $order['customers_state'],
                              'country' => $order['customers_country'],
                              'format_id' => $order['customers_address_format_id'],
                              'telephone' => $order['customers_telephone'],
							  'id' => $order['customers_id'],
							  'ip' => $order['customer_ip'],
                              'email_address' => $order['customers_email_address']);

      $this->delivery = array('name' => $order['delivery_name'],
                              'company' => $order['delivery_company'],
                              'street_address' => $order['delivery_street_address'],
                              'street_address_3' => $order['delivery_street_address_3'],
                              'street_address_4' => $order['delivery_street_address_4'],
                              'suburb' => $order['delivery_suburb'],
                              'city' => $order['delivery_city'],
                              'postcode' => $order['delivery_postcode'],
                              'state' => $order['delivery_state'],
                              'country' => $order['delivery_country'],
                              'format_id' => $order['delivery_address_format_id']);

      $this->billing = array('name' => $order['billing_name'],
                             'company' => $order['billing_company'],
                             'street_address' => $order['billing_street_address'],
                             'suburb' => $order['billing_suburb'],
                             'city' => $order['billing_city'],
                             'postcode' => $order['billing_postcode'],
                             'state' => $order['billing_state'],
                             'country' => $order['billing_country'],
                             'format_id' => $order['billing_address_format_id']);

      $index = 0;
	  
	  //<!-- ORDERS EDITOR AJAX -->	
	  	  $orders_products_query = tep_db_query("SELECT orders_products_id, products_id, cpath, products_name, products_model, products_cost, products_price, products_tax, products_quantity, final_price FROM " . TABLE_ORDERS_PRODUCTS . " WHERE orders_id = '" . (int)$order_id . "'");
	  //<!-- ORDERS EDITOR AJAX -->	
	  
      while ($orders_products = tep_db_fetch_array($orders_products_query)) {
	  
	    $this->products[$index] = array('orders_products_id' => $orders_products['orders_products_id'],
										'qty' => $orders_products['products_quantity'],
                                        'id' => $orders_products['products_id'],
										'cpath' => $orders_products['cpath'],
										'name' => $orders_products['products_name'],
                                        'model' => $orders_products['products_model'],
										'model_origine' => $orders_products['products_ref_origine'],
                                        'tax' => $orders_products['products_tax'],
                                        'cost' => $orders_products['products_cost'],
										'price' => $orders_products['products_price'],
                                        'final_price' => $orders_products['final_price']);
		
        $subindex = 0;
        $attributes_query = tep_db_query("SELECT products_options, options_values_id, products_options_values, options_values_price, price_prefix FROM " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " WHERE orders_id = '" . (int)$order_id . "' AND orders_products_id = '" . (int)$orders_products['orders_products_id'] . "'");
        if (tep_db_num_rows($attributes_query)) {
          while ($attributes = tep_db_fetch_array($attributes_query)) {
            $this->products[$index]['attributes'][$subindex] = array('option' => $attributes['products_options'],
																	 'value_id' => $attributes['options_values_id'],
                                                                     'value' => $attributes['products_options_values'],
                                                                     'prefix' => $attributes['price_prefix'],
                                                                     'price' => $attributes['options_values_price']);

            $subindex++;
          }
        }
        $index++;
      }
    }
  }
?>
