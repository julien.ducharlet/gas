var xmlHttp;

function  createRequest() {
	if(window.ActiveXObject) {
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	} else if(window.XMLHttpRequest) {
		xmlHttp = new XMLHttpRequest();
	}
}

function updateOrderField(orderID, table, field, pred) {
	var newValue = prompt("Nouvelle valeur :", pred);
	var record_value=false;
	if (newValue != '') {
		record_value=true;
	}
	else{
		if (confirm('Etes vous sur de vouloir supprimer le contenu de ce champ?')) {
			record_value=true;
		}
	}
	if (newValue != null && record_value) {
		createRequest();
		var url = "orders_ajax.php?action=update_order_field&oID=" + orderID + "&db_table=" + table + "&field=" + field + "&new_value=" + newValue;
		xmlHttp.open("GET", url, true);
		xmlHttp.onreadystatechange  =  StateChange;
		xmlHttp.send(null);
	}
}

function addProduct(orderID) {
	if (navigator.appName == "Microsoft Internet Explorer"){
		var largeur = screen.width;
		var hauteur = screen.height;
	} else {
		var largeur = window.innerWidth;
		var hauteur = window.innerHeight;
	}
	document.getElementById('add-Product').style.top = (hauteur/2)-100;
	document.getElementById('add-Product').style.left = (largeur/2)-100;
	document.getElementById('add-Product').style.display = 'block';
	document.getElementById('keywords').focus();
}

//this code is highly inspirated of AJAX Quick Search ( contrib: https://www.oscommerce.com/community/contributions,3413/category/search,ajax )
function loadXMLDoc(key) {
	var url="orders_ajax.php?action=search&keyword="+key;
	createRequest();
	xmlHttp.open("GET", url, true);
	xmlHttp.onreadystatechange  =  searchProduct;
	xmlHttp.send(null);
}

function selectProduct(prID, prName) {
	document.getElementById("addProductFind").innerHTML = prName;
	document.getElementById('addProductSearch').style.display= 'none';
	document.getElementById('addProductFind').style.display= 'block';
	var url="orders_ajax.php?action=attributes&prID=" + prID;
	createRequest();
	xmlHttp.open("GET", url, true);
	xmlHttp.onreadystatechange  = checkAttributes;
	xmlHttp.send(null);
}

function setAttr(){
	var oID = getURLGetElement('oID');
	var url="orders_ajax.php?action=set_attributes&oID=" + oID;
	var postVar = "products_quantity=";
	postVar += prompt("unidades:", "1");
	for (var i = 0; i < document.attributes.elements.length; i++){
		postVar += '&' + document.attributes.elements[i].name + '=' + document.attributes.elements[i].value;
	}
	xmlHttp.open("POST", url, true);
	xmlHttp.onreadystatechange = hideAddProducts();
	xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	xmlHttp.send(postVar);
	//FRAN�AIS
	alert('Article ajout�.' + "\n" + 'Rafraichir le navigateur pour voir les changements.');
	
}

function  StateChange() {
	if(xmlHttp.readyState == 4) {
		//alert(xmlHttp.responseText);
		window. location. href = window. location;
	}
}

function  searchProduct() {
	if(xmlHttp.readyState == 4) {
		document.getElementById("quicksearch").innerHTML = xmlHttp.responseText;
	}
}

function checkAttributes() {
	if(xmlHttp.readyState == 4) {
		document.getElementById("ProdAttr").innerHTML = xmlHttp.responseText;
	}
}

function hideAddProducts() {
	document.getElementById('add-Product').style.display = 'none';
	document.getElementById('addProductFind').style.display = 'none';
	document.getElementById('ProdAttr').innerHTML = '&nbsp;';
	document.getElementById('keywords').value = '';
	document.getElementById('quicksearch').innerHTML = '';
	document.getElementById('addProductSearch').style.display = 'block';
}

function getURLGetElement(getEl) {
	var regexS = "[\\?&]"+getEl+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var tmpURL = window.location.href;
	var results = regex.exec( tmpURL );
	if( results == null )
	    return "";
	  else
	    return results[1];
}