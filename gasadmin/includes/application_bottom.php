<?php
/*
  $Id: application_bottom.php,v 1.8 2002/03/15 02:40:38 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

// close session (store variables)
	tep_session_close();
	
	if (STORE_PAGE_PARSE_TIME == 'true') {
		if (!is_object($logger)) $logger = new logger;
		?>
		<table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
				<td align="center" class="smallText"><?php echo $logger->timer_stop(DISPLAY_PAGE_PARSE_TIME); ?></td>
			</tr>
		</table>
		<?php
	}
?>