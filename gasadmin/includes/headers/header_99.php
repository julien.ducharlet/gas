<?php
/*
  include : header_02.php COMMERCIAL 
*/
?> 

<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
  	<td bgcolor="#FFCC66" align="center">
    	<strong>Header du Groupe : <?php echo $myAccount["admin_groups_name"] ;?></strong>
    </td>
  </tr>
</table>

<table width="100%" cellspacing="0" cellpadding="5" border="1">
	<tr height="80">
		<td width="20%" align="left" valign="top">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="25" align="right"><img name="" src="images/icons/icone_voir_mon_compte.gif" width="20" height="20" alt=""></td>
						 
					<td>&nbsp;<a href="admin_account.php" target="_self">Voir mon Compte (<?php echo $myAccount["admin_firstname"];?>)</a>&nbsp;-&nbsp;<a href="<?php echo tep_href_link(FILENAME_LOGOFF, '', 'NONSSL'); ?>"><b>Déconnexion</b></a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_les_commandes.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<a href="<?php echo tep_href_link(FILENAME_CMD_LIST, '');?>" target="_self">Voir les Commandes</a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_les_clients.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<a href="<?php echo FILENAME_CLIENT_LIST;?>" target="_self">Voir les Clients</a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_les_articles.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<a href="<?php echo FILENAME_CATEGORIE_LIST;?>?selected_box=catalog" target="_self">Voir les Articles</a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_la_boutique.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<?php echo '<a href="' . tep_catalog_href_link() . '" target="_blank">Voir la Boutique</a>'; ?></td>
				</tr>
			</table>
			
		</td>
		
		
		<td align="center">
			<font color="#FF0000"><strong>EN COURS DE MODIFICATION !<br />
			<br />
			MERCI DE PREVENIR LE DIRECTEUR SI LE PROBLEME PERSISTE</strong></font>
		</td>
		
		
	</tr>
</table>	



<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr class="headerBar">
		<td height="25" align="center" class="headerBarContent">		
			<form target="_blank" method="get" action="<?php echo FILENAME_CMD_EDIT;?>">
				Recherche Commande : <input name="oID" type="text"  />
				<input name="action" value="edit" type="hidden"  />		
			</form>
		</td>
		<td align="center" class="headerBarContent">
			<form target="_blank" method="get" action="<?php echo FILENAME_CLIENT_LIST;?>">
				Recherche Client : <input name="search" type="text"  />		
			</form>
		</td>
		<td align="center" class="headerBarContent">
			<form target="_blank" method="get" action="<?php echo FILENAME_ARTICLE_LIST;?>">
				Recherche Article : <input name="search_article" type="text"  />		
			</form>    
		</td>
	</tr>
</table>

