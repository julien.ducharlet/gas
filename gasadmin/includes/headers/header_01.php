<?php
/*
  include : header_01.php ADMINISTRATEUR
*/

  if ($messageStack->size > 0) {
    echo $messageStack->output();
  }
$today_header=date("Ymd"); 
$month_header=date("Ym");

//requete nombres de  commandes pour ce  jour
//* Total orders
  $orders_query_raw_day = "select count(*) as total from " . TABLE_ORDERS . " where date_format(date_purchased,'%Y%m%d') = ".$today_header;
  $orders_query_day = tep_db_query($orders_query_raw_day);
  $orders_day = tep_db_fetch_array($orders_query_day);
  $count_orders_day = $orders_day['total'];
//requete nombres de  commandes pour ce  mois en cours
//* Total orders for the month
  $orders_query_raw_month = "select count(*) as total from " . TABLE_ORDERS . " where date_format(date_purchased,'%Y%m') = ".$month_header;
  $orders_query_month = tep_db_query($orders_query_raw_month);
  $orders_month = tep_db_fetch_array($orders_query_month);
  $count_orders_month = $orders_month['total'];
  
//marge

//* Marge total GLOBAL des articles vendus pour la journ�e
  $tot_products_query_raw_day = "select marge from " . TABLE_ORDERS . " o, " . TABLE_ORDERS_STATUS . " os
  where o.orders_status=os.orders_status_id and date_format(o.date_purchased,'%Y%m%d') = ".$today_header;
  $tot_products_query_day = tep_db_query($tot_products_query_raw_day);
  $marge_day=0;
  while($tot_products_day = tep_db_fetch_array($tot_products_query_day)){
  	$marge_day+=$tot_products_day['marge'];
  }

//* Marge total REEL des articles vendus pour la journ�e sans les commande ou l'on a pas recu le reglement (Ch�que, virement, ...)
  $tot_products_query_raw_day_reel = "select sum(marge) as total_marge from " . TABLE_ORDERS . "
  where orders_numero_facture!=0 and date_format(orders_date_facture,'%Y%m%d') = ".$today_header;
  $tot_products_query_day_reel = tep_db_query($tot_products_query_raw_day_reel);
  
  $tot_products_day_reel = tep_db_fetch_array($tot_products_query_day_reel);
  $marge_day_reel = $tot_products_day_reel['total_marge'];
  

//* Marge total GLOBAL des articles vendus pour le mois en cours
  $tot_products_query_raw_month = "select marge from " . TABLE_ORDERS . "
  where date_format(date_purchased,'%Y%m') = ".$month_header;
  $tot_products_query_month = tep_db_query($tot_products_query_raw_month);
  $marge_month=0;
  while($tot_products_month = tep_db_fetch_array($tot_products_query_month)){
  	$marge_month+=$tot_products_month['marge'];
  }

//* Marge total REEL des articles vendus pour le mois en cours
// where orders_numero_facture!=0 and date_format(date_purchased,'%Y%m') = ".$month_header;
  $tot_products_query_raw_month_reel = "select marge from " . TABLE_ORDERS . "
  where orders_numero_facture!=0 and date_format(orders_date_facture,'%Y%m') = ".$month_header;
  $tot_products_query_month_reel = tep_db_query($tot_products_query_raw_month_reel);
  $marge_month_reel=0;
  while($tot_products_month_reel = tep_db_fetch_array($tot_products_query_month_reel)){
  	$marge_month_reel+=$tot_products_month_reel['marge'];
  }

  
//nouveaux clients
//pour ce jours 
// Total new_customers dans la periode
  $new_customers_query_raw_day = "select count(customers_info_id) as tot_new_customers from " . TABLE_CUSTOMERS_INFO . " 
  where date_format(customers_info_date_account_created,'%Y%m%d')=".$today_header;
  $new_customers_query_day = tep_db_query($new_customers_query_raw_day);
  $new_customers_day = tep_db_fetch_array($new_customers_query_day);
  $new_customers_count_day = $new_customers_day['tot_new_customers'];
 // Total new_customers dans la periode
  $new_customers_query_raw_month = "select count(customers_info_id) as tot_new_customers from " . TABLE_CUSTOMERS_INFO . " 
  where date_format(customers_info_date_account_created,'%Y%m')=".$month_header;
  $new_customers_query_month = tep_db_query($new_customers_query_raw_month);
  $new_customers_month = tep_db_fetch_array($new_customers_query_month);
  $new_customers_count_month = $new_customers_month['tot_new_customers']; 
//WHO'S ONLINE
	  
?>

<!--
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
  	<td bgcolor="#FFCC66" align="center">
    	<strong>Header du Groupe : <?php echo $myAccount["admin_groups_name"] ;?></strong> &nbsp; <?php echo $myAccount["admin_groups_id"] ;?>
    </td>
  </tr>
</table>
-->

<table width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
		<td width="15%" align="center" class="smalltext">


			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_mon_compte.gif" width="20" height="20" alt=""></td>
						 
					<td>&nbsp;<a href="admin_account.php" target="_self">Voir mon Compte (<?php echo $myAccount["admin_firstname"];?>)</a>&nbsp;-&nbsp;<a href="<?php echo tep_href_link(FILENAME_LOGOFF, '', 'NONSSL'); ?>"><b>D�connexion</b></a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_les_commandes.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<a href="<?php echo tep_href_link(FILENAME_CMD_LIST, '');?>" target="_self">Voir les Commandes</a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_les_clients.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<a href="<?php echo FILENAME_CLIENT_LIST;?>" target="_self">Voir les Clients</a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_les_articles.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<a href="<?php echo FILENAME_CATEGORIE_LIST;?>" target="_self">Voir les Articles</a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_la_boutique.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<?php echo '<a href="' . tep_catalog_href_link() . '" target="_blank">Voir la Boutique</a>'; ?></td>
				</tr>
			</table>
			
    
		</td>
		
		<?php 
		// Compte le nombre de client de l'utilisateur admin
		$req_my_customers = tep_db_query("select customers_admin from " . TABLE_CUSTOMERS . " where customers_admin='" . $myAccount["admin_id"] ."'");
			$res_my_customers = tep_db_num_rows($req_my_customers);
		// Compte le nombre de client de l'utilisateur admin
		$req_pro_no_commercial = tep_db_query("select customers_admin from " . TABLE_CUSTOMERS . " where customers_admin='0' and customers_type='2' ");
			$res_pro_no_commercial = tep_db_num_rows($req_pro_no_commercial);
		// Compte le nombre de client de l'utilisateur admin
		$req_admin_no_commercial = tep_db_query("select customers_admin from " . TABLE_CUSTOMERS . " where customers_admin='0' and customers_type='4' ");
			$res_admin_no_commercial = tep_db_num_rows($req_admin_no_commercial);	
		?>
		
		<td width="15%" align="center">
			<a href="client_list.php?customers_admin=<?php echo $myAccount["admin_id"];?>&nb_resultats=20">Liste de mes Clients : <strong><?php echo $res_my_customers; ?></strong></a><br />
			<br /> 
			<a href="client_list.php?customers_admin=00&nb_resultats=20&type_client=2">Professionnels sans Commercial : <strong><?php echo $res_pro_no_commercial; ?></strong></a><br />
			<a href="client_list.php?customers_admin=00&nb_resultats=20&type_client=4">Administrations sans Commercial : <strong><?php echo $res_admin_no_commercial; ?></strong></a><br />
			<br />
			<a href="recover_cart_sales.php">Panier perdu � relancer : <font color="#FF0000"><strong>XX</strong></font></a><br />
		</td>
		<?php
		// Compte le nombre de CMD en traitement en cours
		$req_orders_statut_en_cours = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='1'");
			$res_orders_statut_en_cours = tep_db_num_rows($req_orders_statut_en_cours);
		// Compte le nombre de CMD en Relicat
		$req_orders_statut_reliquat = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='23'");
			$res_orders_statut_reliquat = tep_db_num_rows($req_orders_statut_reliquat);
		// Compte le nombre de CMD en probleme de rupture
		$req_orders_statut_rupture = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='15'");
			$res_orders_statut_rupture = tep_db_num_rows($req_orders_statut_rupture);
		// Compte le nombre de CMD en attente de ch�que
		$req_orders_statut_cheque = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='6'");
			$res_orders_statut_cheque = tep_db_num_rows($req_orders_statut_cheque);
		// Compte le nombre de CMD en attente de Virement
		$req_orders_statut_virement = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='7'");
			$res_orders_statut_virement = tep_db_num_rows($req_orders_statut_virement);
		// Compte le nombre de CMD en attente de CB
		$req_orders_statut_cb = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='27'");
			$res_orders_statut_cb = tep_db_num_rows($req_orders_statut_cb);
		// Compte le nombre de CMD en attente de PayPal
		$req_orders_statut_paypal = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='36'");
			$res_orders_statut_paypal = tep_db_num_rows($req_orders_statut_paypal);
		// Compte le nombre de CMD en R�ception de votre r�glement
		$req_orders_statut_reception_reglement = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='37'");
			$res_orders_statut_reception_reglement = tep_db_num_rows($req_orders_statut_reception_reglement);
		// Compte le nombre de CMD en attente de mandat administratif
		$req_orders_statut_mandat_administratif = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='28'");
			$res_orders_statut_mandat_administratif = tep_db_num_rows($req_orders_statut_mandat_administratif);
		// Compte le nombre de CMD en attente de remboursement
		$req_orders_statut_rbs = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='19'");
			$res_orders_statut_rbs = tep_db_num_rows($req_orders_statut_rbs);		
		// Compte le nombre de devis en attente de paiement
		$req_orders_statut_devis = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='21'");
			$res_orders_statut_devis = tep_db_num_rows($req_orders_statut_devis);
		?>
		
		<td width="15%" align="center">
			<a href="cmd_list.php?status=1&nb_resultats=100">CMD � envoyer : <strong><?php echo $res_orders_statut_en_cours; ?></strong></a><br />
			<a href="cmd_list.php?status=23&nb_resultats=100">CMD avec Reliquat : <strong><?php echo $res_orders_statut_reliquat; ?></strong></a><br />
			<a href="cmd_list.php?status=15&nb_resultats=100">CMD avec Rupture : <strong><?php echo $res_orders_statut_rupture; ?></strong></a><br />
			<a href="cmd_list.php?status=19&nb_resultats=100">CMD remboursement a faire : <strong><?php echo $res_orders_statut_rbs; ?></strong></a><br />
			<a href="cmd_list.php?status=21&nb_resultats=100">Devis en attente : <strong><?php echo $res_orders_statut_devis; ?></strong></a><br />
		</td>
		<td width="15%" align="center">
			<a href="cmd_list.php?status=6&nb_resultats=100">En attente de Ch�que : <strong><?php echo $res_orders_statut_cheque; ?></strong></a><br />
			<a href="cmd_list.php?status=7&nb_resultats=100">En attente de Virement : <strong><?php echo $res_orders_statut_virement; ?></strong></a><br />
			<a href="cmd_list.php?status=27&nb_resultats=100">En attente de CB : <strong><?php echo $res_orders_statut_cb; ?></strong></a><br />
			<a href="cmd_list.php?status=36&nb_resultats=100">En attente de PayPal : <strong><?php echo $res_orders_statut_paypal; ?></strong></a><br><br>
			<?php 
			if ($res_orders_statut_reception_reglement > 0) {
			?>
				<a href="cmd_list.php?status=37&nb_resultats=100"><font color="red"><strong>Reception du reglement : <?php echo $res_orders_statut_reception_reglement; ?></strong></font></a><br>
			<?php
			}
			?> 
			<?php 
			if ($res_orders_statut_mandat_administratif > 0) {
			?>
				<a href="cmd_list.php?status=28&nb_resultats=100"><font color="red"><strong>En attente de mandat admin. : <?php echo $res_orders_statut_mandat_administratif; ?></strong></font></a><br>
			<?php
			}
			?> 
			
		</td>			
			
		<td width="12%" align="center" class="smallText">
			<u><a href="margin_report.php?report_id=daily"><b>Stats pour Aujourd'hui</b></a></u><br />
			<br />
			<?php echo $count_orders_day." commande(s)"; ?>
			<br />
			<div class="tooltip">
				<?php echo number_format($marge_day,2)." � de marges global"; ?>
				<span class="tooltiptext">info bas�e sur <br>la date de cmd</span>
			</div>			
			<br />
			<div class="tooltip">
				<?php echo number_format($marge_day_reel,2)." � de marges r�el"; ?>
				<span class="tooltiptext">info bas�e sur <br>la date de facture</span>
			</div>
			<br />
			<?php echo $new_customers_count_day." inscription(s)"; ?>
			<br />
		</td>


		<td width="13%" align="center" class="smallText">
			<u><a href="margin_report.php?report_id=monthly"><b>Stats pour <? echo mois_francais(date(m)); ?> </b></a></u><br /> 
			<br />
			<?php echo $count_orders_month." commande(s)"; ?>
			<br />
			<div class="tooltip">	
				<?php echo number_format($marge_month,2)." � de marges global"; ?>
				<span class="tooltiptext">info bas�e sur <br>la date de cmd</span>
			</div>	
			<br />
			<div class="tooltip">
				<?php echo number_format($marge_month_reel,2)." � de marges r�el"; ?> 
				<span class="tooltiptext">info bas�e sur <br>la date de facture</span>
			</div>
			<br />
			<?php echo $new_customers_count_month." inscription(s)"; ?><br />
		</td>
	
	
		<td width="15%" align="center" class="smallText">
			<u><b>Clients en ligne</b></u><br />
			<br />
			<?php $whos_online_query = tep_db_query("select customer_id from " . TABLE_WHOS_ONLINE." where customer_id>=0"); ?>
			<a href="whos_online.php" target="_blank"><?php $count_connecte=tep_db_num_rows($whos_online_query);
			echo sprintf('Il y a actuellement %s visiteurs', $count_connecte); ?></a> 
			<br/>
			<?php $whos_online_panier = tep_db_query("select distinct(wo.customer_id) from " . TABLE_WHOS_ONLINE." wo, ".TABLE_CUSTOMERS_BASKET." cb where wo.customer_id=cb.customers_id");
			$guest_online_panier = tep_db_query("select customers_id from ".TABLE_CUSTOMERS_BASKET." cb where cb.customers_id=0");	?>
			<?php $count_panier=tep_db_num_rows($whos_online_panier)+tep_db_num_rows($guest_online_panier);
			echo sprintf('%s avec un panier', $count_panier); ?> <img src="images/icon_status_green.gif" width="10" height="10" align="absmiddle" />
			<br />
			<?php echo ($count_connecte-$count_panier)." sans panier";?> <img src="images/icon_status_green_light.gif" width="10" height="10" align="absmiddle" />
			<br />
			<?php $whos_online_robots = tep_db_query("select customer_id from " . TABLE_WHOS_ONLINE." where customer_id<0"); ?>
			<?php echo sprintf('%s robot(s)', tep_db_num_rows($whos_online_robots)); ?> 
			<img src="images/icon_status_green_border_light.gif" width="10" height="10" align="absmiddle" />
			<br>
			<br>
			Header Groupe : <?php echo $myAccount["admin_groups_id"] ;?>
		</td>
		
		
	</tr>
</table>


<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr class="headerBar">
		<td height="25" align="center" class="headerBarContent">		
			<form target="_blank" method="get" action="<?php echo FILENAME_CMD_EDIT;?>">
				Recherche Commande : <input name="oID" type="text"  />
				<input name="action" value="edit" type="hidden"  />		
			</form>
		</td>
		<td align="center" class="headerBarContent">
			<form target="_blank" method="get" action="<?php echo FILENAME_CLIENT_LIST;?>">
				Recherche Client : <input name="search" type="text"  />		
			</form>
		</td>
		<td align="center" class="headerBarContent">
			<form target="_blank" method="get" action="<?php echo FILENAME_ARTICLE_LIST;?>">
				Recherche Article : <input name="search_article" type="text"  />		
			</form>    
		</td>
	</tr>
</table>


