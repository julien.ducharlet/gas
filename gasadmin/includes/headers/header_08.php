<?php
/*
  include : header_02.php COMMERCIAL 
*/
?>

<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
  	<td bgcolor="#FFCC66" align="center">
    	<strong>Header du Groupe : <?php echo $myAccount["admin_groups_name"] ;?></strong> 
    </td>
  </tr>
</table>

<table width="100%" cellspacing="0" cellpadding="5" border="1">
	<tr height="80">
		<td width="20%" align="left" valign="top">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="25" align="right"><img name="" src="images/icons/icone_voir_mon_compte.gif" width="20" height="20" alt=""></td>
						 
					<td>&nbsp;<a href="admin_account.php" target="_self">Voir mon Compte (<?php echo $myAccount["admin_firstname"];?>)</a>&nbsp;-&nbsp;<a href="<?php echo tep_href_link(FILENAME_LOGOFF, '', 'NONSSL'); ?>"><b>D�connexion</b></a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_les_commandes.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<a href="<?php echo tep_href_link(FILENAME_CMD_LIST, '');?>" target="_self">Voir les Commandes</a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_les_clients.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<a href="<?php echo FILENAME_CLIENT_LIST;?>" target="_self">Voir les Clients</a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_les_articles.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<a href="<?php echo FILENAME_CATEGORIE_LIST;?>?selected_box=catalog" target="_self">Voir les Articles</a></td>
				</tr>
				<tr>
					<td align="right"><img name="" src="images/icons/icone_voir_la_boutique.gif" width="20" height="20" alt=""></td>
					<td>&nbsp;<?php echo '<a href="' . tep_catalog_href_link() . '" target="_blank">Voir la Boutique</a>'; ?></td>
				</tr>
			</table>
			
		</td>
		
		<?php 
		// Compte le nombre de client de l'utilisateur admin
		$req_my_customers = tep_db_query("select customers_admin from " . TABLE_CUSTOMERS . " where customers_admin='" . $myAccount["admin_id"] ."'");
			$res_my_customers = tep_db_num_rows($req_my_customers);
		// Compte le nombre de client de l'utilisateur admin
		$req_pro_no_commercial = tep_db_query("select customers_admin from " . TABLE_CUSTOMERS . " where customers_admin='0' and customers_type='2' ");
			$res_pro_no_commercial = tep_db_num_rows($req_pro_no_commercial);
		// Compte le nombre de client de l'utilisateur admin
		$req_admin_no_commercial = tep_db_query("select customers_admin from " . TABLE_CUSTOMERS . " where customers_admin='0' and customers_type='4' ");
			$res_admin_no_commercial = tep_db_num_rows($req_admin_no_commercial);	
		?>
		
		<td width="15%" align="center">
			<a href="client_list.php?customers_admin=<?php echo $myAccount["admin_id"];?>&nb_resultats=20">Liste de mes Clients : <strong><?php echo $res_my_customers; ?></strong></a><br />
			<br /> 
			<a href="client_list.php?customers_admin=00&nb_resultats=20&type_client=2">Professionnels sans Commercial : <strong><?php echo $res_pro_no_commercial; ?></strong></a><br />
			<a href="client_list.php?customers_admin=00&nb_resultats=20&type_client=4">Administrations sans Commercial : <strong><?php echo $res_admin_no_commercial; ?></strong></a><br />
			<br />
			<a href="recover_cart_sales.php">Panier perdu � relancer : <font color="#FF0000"><strong>XX</strong></font></a><br />
		</td>
		<?php
		// Compte le nombre de CMD en traitement en cours
		$req_orders_statut_en_cours = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='1'");
			$res_orders_statut_en_cours = tep_db_num_rows($req_orders_statut_en_cours);
		// Compte le nombre de CMD en Relicat
		$req_orders_statut_reliquat = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='23'");
			$res_orders_statut_reliquat = tep_db_num_rows($req_orders_statut_reliquat);
		// Compte le nombre de CMD en probleme de rupture
		$req_orders_statut_rupture = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='15'");
			$res_orders_statut_rupture = tep_db_num_rows($req_orders_statut_rupture);
		// Compte le nombre de CMD en attente de ch�que
		$req_orders_statut_cheque = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='6'");
			$res_orders_statut_cheque = tep_db_num_rows($req_orders_statut_cheque);
		// Compte le nombre de CMD en attente de Virement
		$req_orders_statut_virement = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='7'");
			$res_orders_statut_virement = tep_db_num_rows($req_orders_statut_virement);
		// Compte le nombre de CMD en attente de CB
		$req_orders_statut_cb = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='27'");
			$res_orders_statut_cb = tep_db_num_rows($req_orders_statut_cb);
		// Compte le nombre de CMD en attente de mandat cash
		$req_orders_statut_mandat = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='29'");
			$res_orders_statut_mandat = tep_db_num_rows($req_orders_statut_mandat);
		// Compte le nombre de CMD en attente de remboursement
		$req_orders_statut_rbs = tep_db_query("select orders_status from " . TABLE_ORDERS . " where orders_status='19'");
			$res_orders_statut_rbs = tep_db_num_rows($req_orders_statut_rbs);		
		?>
		
		<td width="15%" align="center">
			<a href="cmd_list.php?status=1&nb_resultats=100">CMD � envoyer : <strong><?php echo $res_orders_statut_en_cours; ?></strong></a><br />
			<a href="cmd_list.php?status=23&nb_resultats=100">CMD avec Reliquat : <strong><?php echo $res_orders_statut_reliquat; ?></strong></a><br />
			<a href="cmd_list.php?status=15&nb_resultats=100">CMD avec Rupture : <strong><?php echo $res_orders_statut_rupture; ?></strong></a><br />
			<a href="cmd_list.php?status=19&nb_resultats=100">CMD remboursement a faire : <strong><?php echo $res_orders_statut_rbs; ?></strong></a><br />
		</td>
		<td width="15%" align="center">
			<a href="cmd_list.php?status=6&nb_resultats=100">En attente de Ch�que : <strong><?php echo $res_orders_statut_cheque; ?></strong></a><br />
			<a href="cmd_list.php?status=7&nb_resultats=100">En attente de Virement : <strong><?php echo $res_orders_statut_virement; ?></strong></a><br />
			<a href="cmd_list.php?status=27&nb_resultats=100">En attente de CB : <strong><?php echo $res_orders_statut_cb; ?></strong></a><br />
			<a href="cmd_list.php?status=29&nb_resultats=100">En attente de Mandat Cash : <strong><?php echo $res_orders_statut_mandat; ?></strong></a><br />
		</td>
		
		
		<td align="center">
			A AJOUTER : (Faire des propositions � Thierry)<br />			
		</td>
	</tr>
</table>	



<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr class="headerBar">
		<td height="25" align="center" class="headerBarContent">		
			<form target="_blank" method="get" action="<?php echo FILENAME_CMD_EDIT;?>">
				Recherche Commande : <input name="oID" type="text"  />
				<input name="action" value="edit" type="hidden"  />		
			</form>
		</td>
		<td align="center" class="headerBarContent">
			<form target="_blank" method="get" action="<?php echo FILENAME_CLIENT_LIST;?>">
				Recherche Client : <input name="search" type="text"  />		
			</form>
		</td>
		<td align="center" class="headerBarContent">
			<form target="_blank" method="get" action="<?php echo FILENAME_ARTICLE_LIST;?>">
				Recherche Article : <input name="search" type="text"  />		
			</form>    
		</td>
	</tr>
</table>

