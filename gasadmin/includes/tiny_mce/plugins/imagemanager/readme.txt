You can find the documentation with installation instuctions here:

ImageManager documentation:
https://wiki.moxiecode.com/index.php/MCImageManager:Index

FileManager documentation:
https://wiki.moxiecode.com/index.php/MCFileManager:Index

If you still have problems installing it use the forums at https://tinymce.moxiecode.com/punbb/index.php for support.
