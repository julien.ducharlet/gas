<?php
/*
  $Id: comptabilite.php
*/
?>
<!-- comptabilite //-->
          <tr>
            <td> 
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => 'Comptabilité',
                     'link'  => tep_href_link(FILENAME_COMPTABILITE_ACCUEIL, 'selected_box=comptabilite'));
										 

	if ($selected_box == 'comptabilite' || $menu_dhtml == true) {
    $contents[] = array('text'  => 

									tep_admin_files_boxes(FILENAME_EXPORT_FACTURE_COMPTABILITE, 'Export Facture Comptabilité') .
									tep_admin_files_boxes(FILENAME_COMPTABILITE_LIST_FACTURE, 'Générer la liste des Factures (pour contrôle Fiscal)'));

  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- comptabilite_eof //-->
