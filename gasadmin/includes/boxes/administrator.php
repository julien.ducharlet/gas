<?php
/*
  Boxe : administrator.php
*/
?>
<!-- administrator //-->
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('link'  => tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('selected_box')) . 'selected_box=administrator'));


    $contents[] = array('text'  => tep_admin_files_boxes(FILENAME_ADMIN_MEMBERS, 'Utilisateurs & Groupes d\'utilisateurs') .
                                   tep_admin_files_boxes(FILENAME_ADMIN_FILES, 'Gestion des Menus'));

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- administrator_eof //-->
