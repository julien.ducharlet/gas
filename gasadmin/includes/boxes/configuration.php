<?php
/*
  Boxe : configuration.php
*/
?>

<!-- configuration //-->

          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('link'  => tep_href_link(FILENAME_CONFIGURATION, 'gID=1&selected_box=configuration'));

    
	
    $cfg_groups = '';
    $configuration_groups_query = tep_db_query("select configuration_group_id as cgID, configuration_group_title as cgTitle from " . TABLE_CONFIGURATION_GROUP . " where visible = '1' order by sort_order");
    while ($configuration_groups = tep_db_fetch_array($configuration_groups_query)) {
      $cfg_groups .= '<a href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $configuration_groups['cgID'], 'NONSSL') . '" class="menuBoxContentLink">' . $configuration_groups['cgTitle'] . '</a><br>';
    }

    $contents[] = array('text'  => 	tep_admin_files_boxes(FILENAME_MODULE_LIVRAISON, 'Module de livraison') .
								   	tep_admin_files_boxes(FILENAME_MODULE_PAIEMENT, 'Module de paiement').
									"<hr>" .
									tep_admin_files_boxes(FILENAME_COUNTRIES, 'Les Pays') .
                                   	tep_admin_files_boxes(FILENAME_ZONES, 'Les Zones') .
                                   	tep_admin_files_boxes(FILENAME_GEO_ZONES, 'Les Zones Fiscales') .
                                   	tep_admin_files_boxes(FILENAME_TAX_CLASSES, 'Les classes Fiscales') .
                                   	tep_admin_files_boxes(FILENAME_TAX_RATES, 'Les Taux Fiscaux') .
									"<hr>" .
									tep_admin_files_boxes(FILENAME_CURRENCIES, 'Les Devises') .
                                   	tep_admin_files_boxes(FILENAME_LANGUAGES, 'Les Langues') .
                                   	tep_admin_files_boxes(FILENAME_ORDERS_STATUS, 'Les statuts des commandes') .
									"<hr>" .
									$cfg_groups
									);


  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
		  
<!-- configuration_eof //-->
