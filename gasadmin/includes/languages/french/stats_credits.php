<?php
/*
  $Id: stats_customers.php,v 1.9 2002/03/30 15:03:59 harley_vb Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Ch�ques cadeaux utilis�s');

define('TABLE_HEADING_NUMBER', 'ID client');
define('TABLE_HEADING_CUSTOMERS', 'Client');
define('TABLE_HEADING_TOTAL_PURCHASED', 'Cr�dit');
?>