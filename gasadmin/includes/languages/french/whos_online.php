<?php
/*
  $Id: whos_online.php,v 3.2 2007/04/27 nerbonne Exp $
  updated french version
  version franšaise par heuricles
  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

// added for version 1.9
define('AZER_WHOSONLINE_WHOIS_URL', 'https://www.dnsstuff.com/tools/whois.ch?ip='); //for version 2.9 by azer - whois ip
define('TEXT_NOT_AVAILABLE', '   <b>Note:</b> N/A = IP non accessible'); //for version 2.9 by azer was missing
define('TEXT_LAST_REFRESH', 'Dernier refresh &agrave;'); //for version 2.9 by azer was missing
define('TEXT_EMPTY', 'Vide'); //for version 2.8 by azer was missing
define('TEXT_MY_IP_ADDRESS', 'Mon adresse IP '); //for version 2.8 by azer was missing
define('TABLE_HEADING_COUNTRY', 'Pays'); // azerc : 25oct05 for contrib whos_online with country and flag
// added for version 1.9 EOF *************************************************

define('HEADING_TITLE', 'Qui est en ligne?');  // Version update to 3.2 because of multiple 1.x and 2.x jumble.  apr-07 by nerbonne
define('TABLE_HEADING_ONLINE', 'En ligne');
define('TABLE_HEADING_CUSTOMER_ID', 'ID');
define('TABLE_HEADING_FULL_NAME', 'Nom');
define('TABLE_HEADING_IP_ADDRESS', 'Adresse IP');
define('TABLE_HEADING_ENTRY_TIME', 'Heure d\'entr&eacute;e');
define('TABLE_HEADING_LAST_CLICK', 'Dernier clic');
define('TABLE_HEADING_LAST_PAGE_URL', 'Derni&egrave;re URL visit&eacute;e');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_SHOPPING_CART', 'Panier utilisateur');
define('TEXT_SHOPPING_CART_SUBTOTAL', 'Sous-total');
define('TEXT_NUMBER_OF_CUSTOMERS', 'Il y a actuellement %s client(s) en ligne');
define('TABLE_HEADING_HTTP_REFERER', 'Origine?');
define('TEXT_HTTP_REFERER_URL', 'URLd\'origine');
define('TEXT_HTTP_REFERER_FOUND', 'Oui');
define('TEXT_HTTP_REFERER_NOT_FOUND', 'Inconnu');
define('TEXT_STATUS_ACTIVE_CART', 'Actif avec panier');
define('TEXT_STATUS_ACTIVE_NOCART', 'Actif sans panier');
define('TEXT_STATUS_INACTIVE_CART', 'Inactif avec panier');
define('TEXT_STATUS_INACTIVE_NOCART', 'Inactif sans panier');
define('TEXT_STATUS_NO_SESSION_BOT', 'Bot inactif sans session?'); 
define('TEXT_STATUS_INACTIVE_BOT', 'Bot inactif avec session '); 
define('TEXT_STATUS_ACTIVE_BOT', 'Bot actif avec session '); 
define('TABLE_HEADING_COUNTRY', 'Pays');
define('TABLE_HEADING_USER_SESSION', 'Session?');
define('TEXT_IN_SESSION', 'Oui');
define('TEXT_NO_SESSION', 'Non');

define('TEXT_OSCID', 'osCsid');
define('TEXT_PROFILE_DISPLAY', 'D&eacute;tails pour');
define('TEXT_USER_AGENT', 'Navigateur');
define('TEXT_ERROR', 'Erreur!');
define('TEXT_ADMIN', 'Admin');
define('TEXT_DUPLICATE_IP', 'IP en double');
define('TEXT_BOTS', 'Bot(s)');
define('TEXT_ME', 'Ma propre session');
define('TEXT_ALL', 'Tous');
define('TEXT_REAL_CUSTOMERS', 'R&eacute;el(s) visiteur(s)');

define('TEXT_YOUR_IP_ADDRESS', 'Votre adresse IP');
define('TEXT_SET_REFRESH_RATE', 'Actu tous les');
define('TEXT_NONE_', 'Aucun');
define('TEXT_CUSTOMERS', 'Visiteur(s)');
?>