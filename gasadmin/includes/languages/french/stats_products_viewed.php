<?php
/*
  $Id: stats_products_viewed.php,v 1.5 2002/03/30 15:51:03 harley_vb Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Les produits les plus consult&eacute;s');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_VIEWED', 'Vu');

define('TABLE_TEXT_RAZ_VUE','Remettre � z�ro les compteurs pour les vus');
define('TABLE_TEXT_RAZ_ACHETE','Remettre � z�ro les compteurs pour les achete');
define('TABLE_TEXT_RAZ_CONFIRM','�tes vous certain(e)s de vouloir remettre � z�ro les compteurs ?');
define('TABLE_TEXT_RAZ_END',' produit(s) remis � 0.');

define('FILTRE_TEXT','Afficher ');
define('FILTRE_OPTION_1','Les produits vus au moins 1 fois');
define('FILTRE_OPTION_2','Tous les produits');
define('FILTRE_OPTION_3','Les produits vus aucune fois');

define('TEXT_SHOW', 'Afficher :');
define('TEXT_ALL_MANUFACTURERS', 'Tous les fabricants');
define('TEXT_ALL_CATEGORIES_BY_MANUFACTURER', 'Toutes les cat�gories par Fabricants');
define('TEXT_ALL_CATEGORIES', 'Toutes les cat�gories');

define('TABLE_HEADING_PURCHASED', 'Achet&eacute;');
define('BUTTON_BACK_TO_MAIN', 'retour menu principal');








?>