<?php
/*
  $Id: stats_customers.php,v 1.6 2002/03/30 15:54:15 harley_vb Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Les meilleurs clients');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_HEADING_CUSTOMERS', 'Clients');
define('TABLE_HEADING_TOTAL_PURCHASED', 'Total achet&eacute;');
define('TABLE_HEADING_NUMBER_ORDERS', 'Nb Commandes');
define('TABLE_HEADING_MARGIN', 'Marge');
define('PRICE_COST_NO_FIXED', 'Prix d\'achat non renseigné');
define('TABLE_HEADING_NUMBER_PRODUCTS', 'Nb Produits');
define('TEXT_ALL_MOIS','Tous les mois');
define('TEXT_ALL_ANNEE','Toutes les ann&eacute;es');
?>