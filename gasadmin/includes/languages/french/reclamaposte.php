<?php
/*
  Page : reclamaposte
*/

//
// D�fintion des Template d'Email de Notification Via CRON
//
// Template 1 - Sujet Email - avec injection du num�ro de commande
define('LAPOSTE_EMAIL_SUBJECT', 'Votre commande: %s - Informations Transporteur');
// Template 1 - Texte Email - avec injection du nom de client, num�ro de tracking, date du status, description du nouveau status
define('LAPOSTE_EMAIL_TEXT', '%s, bonjour !<br /><br />
						
						Nous tenons &agrave; vous informer que le colis exp&eacute;di&eacute; via Coliposte<br />
						et qui a pour num&eacute;ro %s est en cours de livraison.<br />
						
						Ce transporteur nous a donn&eacute; l\'information suivante :<br />
						
						%s - %s<br />
						
						Nous vous invitons &agrave; suivre directement l\'avancement de la livraison<br />
						de ce colis sur le site :<br />
						
						https://www.coliposte.net/gp/services/accueil_gp.jsp<br />
						
						et &agrave; prendre contact avec votre bureau de poste en cas de probl&egrave;me.<br />
						
						ATTENTION ! Un avis de passage n\'est pas forc&eacute;ment gliss&eacute; dans votre boite aux lettres &agrave; la livraison du colis.<br />
						Aussi, nous vous recommandons de contacter votre bureau de Poste muni de votre num&eacute;ro de suivi si vous n\'avez pas<br />
						r&eacute;ceptionn&eacute; votre colis sous une semaine. Au del&agrave; de ce d&eacute;lai, et si vous n\'avez toujours rien recu, contactez notre service clients.<br />
						
						Notre service clients se tient bien &eacute;videmment &agrave; votre enti&egrave;re disposition.<br />
						
						Bien &agrave; vous,<br />
						
						Service Commercial '. NOM_DU_SITE . '<br />
						service-commercial@'. NOM_DU_SITE . '<br />
						'. ADRESSE_SITE);
?>
