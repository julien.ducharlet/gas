<?php
/*
  $Id: export_orders.php,v 1.12 2002/01/12 18:46:27 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Export des commandes vers un fichier CSV');

define('INPUT_START', 'Du num�ro');
define('INPUT_END', 'Au num�ro');
define('INPUT_VALID', 'Exporter les commandes');
define('INPUT_DESC', 'Selectionnez les num�ros de commande � exporter. Laisser les deux champs vides pour tout exporter ou l\'un des deux champs pour s�lectionner toutes les commandes du num�ro X ou toutes les commandes jusqu\'au num�ro X.');

?>