<?php
/*
  $Id: stats_customer_registrations.php,v 1.20 2005/08/17 12:07:21 

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Rapport de Cr�ation de Compte de Client');

define('TEXT_TO', ' au ');
define('TEXT_DATAFROM', 'Donn�es du ');
define('TEXT_TOTAL_ACCOUNTS', 'Total de comptes client : ');
define('TEXT_YEARLY', ' Annuellement ');
define('TEXT_MONTHLY', ' Mensuellement ');
define('TEXT_WEEKLY', ' Par semaine ');
define('TEXT_DAILY', ' Quotidiennement ');
define('TEXT_BEGINDATE', 'Du ');
define('TEXT_ENDDATE', 'Au ');
define('TEXT_D_M_Y', '  (MM JJ ANNEE)');
define('TEXT_NUMBER_OF_ACCOUNTS_CREATED', 'Nombre de Comptes Cr��s');
define('TEXT_SUBMIT', 'Rechercher');
?>
