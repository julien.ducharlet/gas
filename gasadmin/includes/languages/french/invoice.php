<?php
/*
  $Id: invoice.php,v 1.1 2002/06/11 18:17:59 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TABLE_HEADING_COMMENTS', 'Commentaires');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Mod&egrave;le');
define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_TAX', 'Taxe');
define('TABLE_HEADING_TOTAL', 'Total');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Prix (ht)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Prix (ttc)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (ht)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total (ttc)');

define('ENTRY_SOLD_TO', 'VENDU A :');
define('ENTRY_SHIP_TO', 'LIVRE A :');
define('ENTRY_PAYMENT_METHOD', 'M&eacute;thode de paiement :');
define('ENTRY_SUB_TOTAL', 'Sous-Total :');
define('ENTRY_TAX', 'Taxe :');
define('ENTRY_SHIPPING', 'Exp&eacute;dition :');
define('ENTRY_TOTAL', 'Total :');

define('HEADING_TITLE_IDMEMBRE', '<b>Code Client :</b>');
define('HEADING_TITLE_IDCOMMANDE', '<b>Num�ro de Commande :</b>');
define('TITLE_PRINT_INVOICE', 'Facture N&deg; : ');
define('TITLE_PRINT_DATEINVOICE', 'Date de facture : ');

?>