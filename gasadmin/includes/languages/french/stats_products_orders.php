<?php
/*
  $Id: stats_products_orders.php,v 1 05 nov 2007 

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2005 osCommerce

  originally developed by paddybl
  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Statistiques produits vendu');
define('HEADING_DAY','Afficher pour le jour:');
define('HEADING_MONTH', 'Afficher pour le mois:');
define('HEADING_YEAR', 'Afficher pour l\'ann&eacute;e:');
define('HEADING_TITLE_NO_STATUS', 'Masquer les commandes:');
define('HEADING_TITLE_STATUS', 'N\'afficher que les commandes:');
define('TEXT_NO_ORDERS', 'Aucune');
define('TEXT_ALL_ORDERS', 'Toutes');
define('HEADING_MANUFACTURER', 'Choisissez un fabriquant: ');
define('TEXT_SELECT_MANUFACTURER', 'Choisissez un fabriquant');
define('TEXT_ALL_MANUFACTURERS', 'Tous les fabriquants');
define('HEADING_CATEGORIES', 'Choisissez une categories: ');
define('TEXT_SELECT_CATEGORY', 'Choisissez une categories');
define('TEXT_ALL_CATEGORIES', 'Toutes les categories');
define('HEADING_ATTRIBUTES', 'Choisissez une option');
define('TEXT_ALL_OPTIONS', 'Toutes les options');
define('HEADING_PRODUCTS', 'Choisissez un produit: ');
define('TEXT_SELECT_PRODUCT', 'Choisissez un produit');
define('TEXT_ALL_PRODUCTS', 'Tous les produits');
define('TEXT_ALL_DAYS', 'Tous les jours');
define('TEXT_ALL_MOIS', 'Tous les mois');
define('TEXT_ALL_ANNEE', 'Toutes les ann&eacute;es');
define('NEW_CUSTOMERS', 'Nouveaux clients:');
define('NUMBER_ORDER', 'Nombre de commandes:');
define('CUSTOMERS_BOUGHT', 'Clients ayant achet&eacute;:');
define('NEW_CUSTOMERS_BOUGHT', 'Nouveaux clients ayant achet&eacute;:');
define('TOTAL_TTC', 'Total ventes TTC:');
define('TOTAL_HT', 'Total ventes HT:');
define('TOTAL_TAX', 'Total taxes:');
define('BASKET_TTC', 'Vente moyenne TTC:');
define('BASKET_HT', 'Panier moyen, articles HT:');
define('JAN', 'Janvier');
define('FEV', 'F&eacute;vrier');
define('MAR', 'Mars');
define('AVR', 'Avril');
define('MAI', 'Mai');
define('JUN', 'Juin');
define('JUI', 'Juillet');
define('AOU', 'Ao&ucirc;t');
define('SEP', 'Septembre');
define('OCT', 'Octobre');
define('NOV', 'Novembre');
define('DEC', 'D&eacute;cembre');
define('DATE_ORDERS','Date d\'achat');
define('ORDER_ID','Num�ro de commande');
define('ORDER_CUSTOMER','Nom client, E-mail<br />T�l�phone');
define('ORDER_ADDRESS','Adresse');
define('ORDER_PRODUCT','ID, Nom et<br />option(s) du produit');
define('ORDER_QUANTITY','Quantit�');
define('ORDER_STATUS','Statut commande');
define('ORDER_TOTAL','Montant HT <br />M�thode de paiement');
define('TEXT_NO_PRODUCTS', 'Aucun produit pour ces crit�res de s�lection');
define('TEXT_RESULT_PAGES', 'R&eacute;sultat : ');
define('PREVNEXT_TITLE_PAGE_NO', 'Page %d');
define('TEXT_SORT_PRODUCTS', 'Tri des produits ');
define('TEXT_DESCENDINGLY', 'descendant');
define('TEXT_ASCENDINGLY', 'ascendant');
define('TEXT_BY', ' par ');
define('TEXT_BUTTON_REPORT_PRINT','Imprimer');
define('TEXT_BUTTON_REPORT_PRINT_DESC','Imprimer ce rapport');
define('TEXT_PHONE','T�l.: ');
define('QUANTITY_PRODUCTS','Nombre de produits: ');
define('HEADING_NO_OPTIONS',' Erreur: Vous ne pouvez pas utiliser les filtres par options (consultez le fichier d\'installation)');
define('TEXT_EDIT_ORDER','Editer la commande');
define('TEXT_MAILTO','Envoyer un E-mail');
define('TEXT_VIEW_PRODUCT','Visualiser le produit (nouvelle fen�tre)');
define('TEXT_CHOOSE_PRODUCT','Choisir ce produit');
define('IMAGE_BUTTON_PRINT','Imprimer');
define('TEXT_INFO_DATE_ADDED', 'Date ajout :');
define('TEXT_INFO_LAST_MODIFIED', 'Derni&egrave;re modification :');
define('TEXT_SPECIAL_INFO_DATE_ADDED', 'Date ajout (Promo):');
define('TEXT_SPECIAL_INFO_LAST_MODIFIED', 'Derni&egrave;re modification (Promo):');
define('TEXT_INFO_NEW_PRICE', 'Nouveau Prix :');
define('TEXT_INFO_ORIGINAL_PRICE', 'Prix original :');
define('TEXT_INFO_PERCENTAGE', 'Pourcentage :');
define('TEXT_INFO_EXPIRES_DATE', 'Expire le :');
define('TEXT_INFO_STATUS_CHANGE', 'Changement de statut :');
define('TEXT_INFO_COST','Prix achat :');
define('TEXT_GRATUIT','0');
define('TEXT_INFO_MARGE','Marge :');
?>