<?php
/*
  $Id: duplicate.php - Version 1 - 11/11/2006

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

//define('HEADING_TITLE', 'Dupliquer un Article');
//define('TABLE_HEADING_PRODUCTS', 'Article � dupliquer');
//define('TEXT_DUPLICATE_PRODUCT', 'Selectionnez l\'article :');
//define('TEXT_DUPLICATE_SELECTIONNEZ', 'Selectionnez le mode :');
//define('TEXT_DUPLICATE_COPIER', 'Copier');
//define('TEXT_DUPLICATE_DUPLIQUER', 'Dupliquer');

//define('TEXT_DUPLICATE_SELECT', 'choisisissez dans la liste');
//define('TEXT_DUPLICATE_VERIF_CAT', 'V�rifier si le produit existe dans <br />la ou les cat�gorie(s) s�lectionn�e(s) <br /> (pour le duplicate) :');
//define('TEXT_DUPLICATE_OPTION', 'Dupliquer �galement les options<br /> (pour le duplicate) :');

//define('TEXT_DUPLICATE_SELECTIONNEZ_CAT', 'Selectionnez les cat�gories :');
//define('TEXT_SPECIALS_PRICE_TIP', '<b>Informations :</b><ul><li>Pour seletionner plusieurs cat�gories, maintenez la touche CTRL appuy�e et cliquez pour selectionner.<br /><br /></li><li>Si vous choisissez de "<b>Copier</b>" un article, il sera visible imm�diatement par vos visiteurs sur votre boutique.<br />Par cette fonction, vous ajouter un seul et m�me article dans diff�rentes cat�gories et si vous modifiez par exemple le titre il sera chang� dans tous les articles qui ont �t� copi�s<br /><br /></li><li>Si vous choisissez de "<b>Dupliquer</b>" un article, il sera visible qu\'apr�s l\'avoir mit en service dans chaques cat�gories.<br />Par cette fonction, vous ajoutez plusieurs nouveaux articles ind�pendants les uns des autres et c\'est pour cette raison qu\'il ne sont pas mis en ligne directement pour que vous puissiez les modifier avant de les afficher.</li></ul>');

//define('TEXT_DUPLICATE_ACTION_NONCOPIE','non copi�, existe d�j�');
//define('TEXT_DUPLICATE_ACTION_COPIE','copi�');

//define('TEXT_DUPLICATE_ACTION_DUPLICATE_OVER','dupliqu�, attention produit en double');
//define('TEXT_DUPLICATE_ACTION_NONDUPLICATE','non dupliqu�');
//define('TEXT_DUPLICATE_ACTION_DUPLICATE','dupliqu�');

//define('TEXT_DUPLICATE_OPTION_DUPLICATE','option(s) dupliqu�e(s)');
//define('TEXT_DUPLICATE_OPTION_NONDUPLICATE','option(s) non dupliqu�e(s)');

//define('TEXT_DUPLICATE_SUBMIT_PRODUIT','Il faut choisir un produit !');
//define('TEXT_DUPLICATE_SUBMIT_CATEGORIES','Il faut choisir une ou plusieurs cat�gorie(s) !');
?>