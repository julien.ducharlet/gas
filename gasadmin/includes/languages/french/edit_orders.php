<?php
/*
  $Id: edit_orders.php,v 1.25 2003/08/07 00:28:44 jwh Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce
  
  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Editer une commande');
define('HEADING_TITLE_NUMBER', 'N&deg;');
define('HEADING_TITLE_DATE', 'du');
define('HEADING_SUBTITLE', 'Veuillez �diter toutes les pi�ces comme d�sir�es et cliquez sur le bouton "mise � jour" ci-dessous.');
define('HEADING_TITLE_SEARCH', 'Num�ro de commande :');
define('HEADING_TITLE_STATUS', 'Statut :');
define('ADDING_TITLE', 'Ajout d\'un produit � la commande');

define('HINT_UPDATE_TO_CC', '<font color="#FF0000">Conseil : </font>Choisisez le paiement par "carte de cr�dit" pour afficher les champs additionnels.');
define('HINT_TOTALS', '<font color="#FF0000">Conseil : </font>Vous �tes libre de donner des escomptes en ajoutant des montants n�gatifs � la liste.<br />Les champs avec pour valeurs 0 seront supprim�s apr�s mise � jour de la commande except� la livraison.');


define('TABLE_HEADING_COMMENTS', 'Commentaires');
define('TABLE_HEADING_CUSTOMERS', 'Clients');
define('TABLE_HEADING_ORDER_TOTAL', 'Total Commande');
define('TABLE_HEADING_DATE_PURCHASED', 'Date d\'achat');
define('TABLE_HEADING_STATUS', 'Nouveau statut');
define('TABLE_HEADING_ACTION', 'Action');


define('TABLE_HEADING_TOTAL_MODULE', 'Descriptions');
define('TABLE_HEADING_TOTAL_AMOUNT', 'Tarifs (n�gatif mettre -)');

define('TABLE_HEADING_CUSTOMER_NOTIFIED', 'Client notifi&eacute;');
define('TABLE_HEADING_DATE_ADDED', 'Date d\'ajout');

define('ENTRY_CUSTOMER', 'Client :');
define('ENTRY_CUSTOMER_NAME', 'Nom ');
define('ENTRY_CUSTOMER_COMPANY', 'Soci�t� ');
define('ENTRY_CUSTOMER_ADDRESS', 'Adresse ');
define('ENTRY_CUSTOMER_SUBURB', 'Compl�ment d\'adresse ');
define('ENTRY_CUSTOMER_CITY', 'Ville ');
define('ENTRY_CUSTOMER_STATE', 'D�partement ');
define('ENTRY_CUSTOMER_POSTCODE', 'Code postal ');
define('ENTRY_CUSTOMER_COUNTRY', 'Pays ');
define('ENTRY_CUSTOMER_PHONE', 'T�l�phone ');
define('ENTRY_CUSTOMER_EMAIL', 'E-Mail ');

define('ENTRY_SOLD_TO', 'Vendu � :');
define('ENTRY_DELIVERY_TO', 'Livrer � :');
define('ENTRY_SHIP_TO', 'Exp�dier � :');
define('ENTRY_SHIPPING_ADDRESS', 'Adresse de livraison');
define('ENTRY_BILLING_ADDRESS', 'Adresse de facturation');
define('ENTRY_PAYMENT_METHOD', 'Type de paiement :');
define('ENTRY_CREDIT_CARD_TYPE', 'Type de carte :');
define('ENTRY_CREDIT_CARD_OWNER', 'Propri�taire de la carte :');
define('ENTRY_CREDIT_CARD_NUMBER', 'Num�ro de la carte :');
define('ENTRY_CREDIT_CARD_EXPIRES', 'Date d\'expriration :');
define('ENTRY_SUB_TOTAL', 'Sous-total : ');
define('ENTRY_TAX', 'TVA :');
define('ENTRY_SHIPPING', 'Frais de port :');
define('ENTRY_TOTAL', 'Total :');
define('ENTRY_DATE_PURCHASED', 'Date d\'achat :');
define('ENTRY_STATUS', 'Staut de la commande :');
define('ENTRY_DATE_LAST_UPDATED', 'Dernier mis � jour :');
define('ENTRY_NOTIFY_CUSTOMER', 'Informer le client :');
define('ENTRY_NOTIFY_COMMENTS', 'Envoyez les commentaires :');
define('ENTRY_PRINTABLE', 'Imprimer la facture');

define('TEXT_INFO_HEADING_DELETE_ORDER', 'Effacer la commande');
define('TEXT_INFO_DELETE_INTRO', 'Voulez vous vraiment supprimer cette commande');
define('TEXT_INFO_RESTOCK_PRODUCT_QUANTITY', 'Ajustez les quantit�s');
define('TEXT_DATE_ORDER_CREATED', 'Cr�� :');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Derni�re Mise � jour :');
define('TEXT_DATE_ORDER_ADDNEW', 'Ajoutez le nouveau produit');
define('TEXT_INFO_PAYMENT_METHOD', 'M�thode de paiement :');

define('TEXT_ALL_ORDERS', 'Toutes commandes');
define('TEXT_NO_ORDER_HISTORY', 'Aucune commande trouv�');

define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_SUBJECT', 'Votre commande a �t� mis � jour');
define('EMAIL_TEXT_ORDER_NUMBER', 'Commande num�ro :');
define('EMAIL_TEXT_INVOICE_URL', 'URL d�taill� de la facture :');
define('EMAIL_TEXT_DATE_ORDERED', 'Date de la commande :');
define('EMAIL_TEXT_STATUS_UPDATE', 'Le statut de votre commande a �t� mis � jour.' . "\n\n" . 'Nouveau statut : %s' . "\n\n" . 'Si vous avez des questions, veuillez r�pondre � cet email.' . "\n\n" . 'Cordialement,' . "\n". 'Le service commercial' . "\n");
define('EMAIL_TEXT_COMMENTS_UPDATE', 'Commentaires :' . "\n\n%s\n\n");

define('ERROR_ORDER_DOES_NOT_EXIST', 'Erreur : Aucune commande.');
define('SUCCESS_ORDER_UPDATED', 'Accompli :  La commande a �t� mise � jour avec succ�s.');
define('WARNING_ORDER_NOT_UPDATED', 'Attention :  Aucun changement n\'a �t� effectu�.');

define('ADDPRODUCT_TEXT_CATEGORY_CONFIRM', 'OK');
define('ADDPRODUCT_TEXT_SELECT_PRODUCT', 'Choisir un produit');
define('ADDPRODUCT_TEXT_PRODUCT_CONFIRM', 'OK');
define('ADDPRODUCT_TEXT_SELECT_OPTIONS', 'Choisir une option');
define('ADDPRODUCT_TEXT_OPTIONS_CONFIRM', 'OK');
define('ADDPRODUCT_TEXT_OPTIONS_NOTEXIST', 'Le produit n\'a aucune option...');
define('ADDPRODUCT_TEXT_CONFIRM_QUANTITY', 'Qt� de produit');
define('ADDPRODUCT_TEXT_CONFIRM_ADDNOW', 'Ajouter');
define('ADDPRODUCT_TEXT_STEP', '�tape');
define('ADDPRODUCT_TEXT_STEP1', ' &laquo; Choisir dans le catalogue. ');
define('ADDPRODUCT_TEXT_STEP2', ' &laquo; Choisir un produit. ');
define('ADDPRODUCT_TEXT_STEP3', ' &laquo; Choisir une option. ');

define('MENUE_TITLE_CUSTOMER', '1. Informations client');
define('MENUE_TITLE_PAYMENT', '2. M�thode de paiement');
define('MENUE_TITLE_ORDER', '3. Produits');
define('MENUE_TITLE_TOTAL', '4. Avoir, Livraison et Total');
define('MENUE_TITLE_STATUS', '5. Statut et Notification');
define('MENUE_TITLE_UPDATE', '6. Mise � jour commande');
?>