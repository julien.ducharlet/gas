<?php
/*
  $Id: header_tags_popup_help.php,v 1.0 2005/09/22 
   
  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce
  
  Released under the GNU General Public License
*/
?>
<style type="text/css">
.popupText {color: #000; font-size: 12px; } 
</style>
<table border="0" cellpadding="0" cellspacing="0" class="popupText">
  <tr><td><hr class="solid"></td></tr>
  <tr>
   <td class="popupText"><p><b>Pourquoi HTTA, HTDA, HTKA et HTCA sont ils utilis�s ?</b><br /><br />
    Concerne les en-t�te des pages qui peuvent etre par d�faut ou sinon vous pouvez cr�er votre propre ensemble d'en-t�te pour chaque page.

<pre>
HT = Header Tags  
T  = (Title) Titre
A  = (All) Tout 
D  = Description
K  = (Keywords) Mots cl�s 
C  = Categories *
</pre>  
<b>* info :</b> 	
L'option de HTCA fonctionne seulement pour le page d'index et product_info. Pour la page d'index, elle ajoute le nom de la cat�gorie au titre. Pour la page product_info, si on le v�rifie, le texte dans les bo�tes dans la commande des textes sera appos� au titre, � la description et aux mots-cl�s, respectivement.<br /><br />

Si HTTA est plac� dessus (v�rifi�), alors il indique que l'affichage l'en-t�te �tiquette le titre tout (titre de d�faut plus celui que vous avez �tabli).<br /><br />

Ainsi si vous faites v�rifier l'option, les deux titres seront montr�s. Disons que votre titre est "Monsite" et le titre de d�faut est "osCommerce".<br />
<pre>
Avec HTTA actif, le titre est 
"Monsite Oscommerce" 
Avec HTTA inactif, le titre est 
"Monsite"
</pre>
</p>
<p>
Si le nom de la section est en <font color="red">ROUGE</font>, cela signifie que ce dossier n'a pas le code correct d'en-t�te install� pour lui. Voir le fichier Install_Catalog.txt pour des instructions sur la fa�on de l'utiliser.</p>
  </td>
 </tr> 
</table>
