<?php
/*
  $Id: customers.php,v 1.12 2002/01/12 18:46:27 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Les Clients');
define('HEADING_TITLE_SEARCH', 'Rechercher :');

define('TABLE_HEADING_FIRSTNAME', 'Pr�nom');
define('TABLE_HEADING_LASTNAME', 'Nom');
define('TABLE_HEADING_EMAIL', 'Adresse Email');
define('TABLE_HEADING_DATE_OF_BIRTHDAY', 'Date de Naissance');
define('TABLE_HEADING_ACCOUNT_CREATED', 'Compte cr��');
define('TABLE_HEADING_ACTION', 'Action');


define('TEXT_DATE_ACCOUNT_CREATED', 'Compte cr�� :');
define('TEXT_DATE_ACCOUNT_LAST_MODIFIED', 'Derni�re modification :');
define('TEXT_INFO_DATE_LAST_LOGON', 'Derni�re connexion :');
define('TEXT_INFO_NUMBER_OF_LOGONS', 'Nombre de connexions :');
define('TEXT_INFO_COUNTRY', 'Pays :');
define('TEXT_INFO_NUMBER_OF_REVIEWS', 'Nombre de critiques :');
define('TEXT_INFO_INSCRIT_NEWSLETTER', 'Inscrit � la newsletter :');
define('TEXT_DELETE_INTRO', 'Etes vous s&ucirc;r de vouloir supprimer ce client ?');
define('TEXT_DELETE_REVIEWS', 'Supprimer %s critique(s)');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Supprimer client');
define('TYPE_BELOW', 'Type ci-dessous');
define('PLEASE_SELECT', 'Choisissez en un');
?>