<?php
/*
  $Id: quick_updates.php v2.80 2005/10/16 13:55:34 HRB Exp $

  CONTRIBUTION : Quick Update Version : 2.5b full modifi� par Thierry POULAIN
  
  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2005 osCommerce

  Released under the GNU General Public License
*/

define('WARNING_MESSAGE','<b>Pensez � cliquez sur mise � jour avant de changer ou de quitter cette page!! </b>');
define('TOP_BAR_TITLE', 'Quick Update Version : 2.5b full');
define('HEADING_TITLE', 'Quick Update Version : 2.5b full');
define('TEXT_MARGE_INFO','Modifier la marge commerciale. Si vous cochez ceci, vos produits seront enregistr� au taux entr�. Entrer un nombre n�gatif pour faire une remise');
define('TEXT_PRODUCTS_UPDATED', 'produit actualis�!');
define('TEXT_IMAGE_PREVIEW','produit pr�c�dent');
define('TEXT_IMAGE_SWITCH_EDIT','Passez � l\'�dition compl�te');
define('TEXT_QTY_UPDATED', 'Valeur(s) chang�e(s)');
define('TEXT_INPUT_SPEC_PRICE','<b>(+/-) valeur ou taux :</b>');
define('TEXT_SPEC_PRICE_INFO1','ie : 10, 15%, -20, -25%');
define('TEXT_SPEC_PRICE_INFO2','<b>Nota: </b>D�cochez les produit que vous ne voulez pas modifier.');
define('TEXT_MAXI_ROW_BY_PAGE', 'Nombre maximal de ligne par page');
define('TEXT_SPECIALS_PRODUCTS', 'Prix sp�cial du produit !');
define('TEXT_ALL_MANUFACTURERS', 'Tous les types');
define('NO_TAX_TEXT','- N/A -');
define('NO_MANUFACTURER','- N/A -');
define('TABLE_HEADING_CATEGORIES', 'Categories');
define('TABLE_HEADING_MODEL', '<br />Mod�le');
define('TABLE_HEADING_PRODUCTS', '<br />Nom');
define('TABLE_HEADING_PRICE', '<br />Prix<br />de vente');
define('TABLE_HEADING_COST', '<br />Prix<br />d\'Achat');
define('TABLE_HEADING_TAX', '<br />Taxe');
define('TABLE_HEADING_WEIGHT', '<br />Poids');
define('TABLE_HEADING_QUANTITY', '<br />Qt�.');
define('TABLE_HEADING_QUANTITY_MINI', '<br />Qt�<br />mini');
define('TABLE_HEADING_QUANTITY_IDEAL', '<br />Qt�<br />id�al');
define('TABLE_HEADING_QUANTITY_MAXI', '<br />Qt�<br />maxi');

define('TABLE_HEADING_STATUS', '<br />Statut <br />(on/off)');

define('TABLE_HEADING_MANUFACTURERS', '<br />Type d\'article');
define('TABLE_HEADING_IMAGE', '<br />Image');

define('TABLE_HEADING_AFFNEWS', '<br />News <br />(on/off)');


define('TABLE_HEADING_SHIP_PRICE', 'Prix<br />D\'exp�diion 1');
define('TABLE_HEADING_SHIP_PRICE_TWO', 'Prix<br />D\'exp�diion 2');
define('DISPLAY_CATEGORIES', 'S�lectionner une cat�gorie:');
define('DISPLAY_MANUFACTURERS', 'S�lectionner un type d\'accessoire');
define('PRINT_TEXT', 'Imprimer cette page');
define('TOTAL_COST', 'Prix TTC');

define('TEXT_SORT_ALL', '');
define('TEXT_DESCENDINGLY', '- De Z � A');
define('TEXT_ASCENDINGLY', '- De A � Z');
define('TEXT_BY', ' par ');
?>
