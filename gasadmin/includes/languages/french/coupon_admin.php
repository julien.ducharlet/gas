<?php
/*
  $Id: coupon_admin.php,v 1.1.2.5 2003/05/13 23:28:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com
  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
define('TEXT_COUPON_REDEEMED', 'Ch�ques cadeaux rachet�s');
define('REDEEM_DATE_LAST', 'Derni�re date utilis�');
define('TOP_BAR_TITLE', 'Statistique');
define('HEADING_TITLE', 'Les Ch�ques cadeaux');
define('HEADING_TITLE_STATUS', 'Statut : ');
define('TEXT_CUSTOMER', 'Client :');
define('TEXT_COUPON', 'Nom du coupon :');
define('TEXT_COUPON_ALL', 'Tous les coupons');
define('TEXT_COUPON_ACTIVE', 'Coupons actifs');
define('TEXT_COUPON_INACTIVE', 'Coupons inactifs');
define('TEXT_SUBJECT', 'Sujet :');
define('TEXT_FROM', 'De :');
define('TEXT_FREE_SHIPPING', 'Exp&eacute;dition gratuite');
define('TEXT_MESSAGE', 'Message :');
define('TEXT_SELECT_CUSTOMER', 'Selection du client');
define('TEXT_ALL_CUSTOMERS', 'Tous les clients');
define('TEXT_NEWSLETTER_CUSTOMERS', 'A tous les abonn�s de la newsletter');
define('TEXT_CONFIRM_DELETE', '&Ecirc;tes-vous s&ucirc;rs que vous voulez supprimer ce coupon ?');

define('TEXT_TO_REDEEM', 'Vous pouvez utiliser le coupon pendant la proc�dure de paiement. Entrez simplement le code dans la case appropri�e, et cliquez sur le bouton utiliser.');
define('TEXT_IN_CASE', ' en cas de probleme ');
define('TEXT_VOUCHER_IS', 'Le code du coupon est ');
define('TEXT_REMEMBER', 'Ne perdez pas le code du coupon, assurez vous de le garder pour pouvoir profiter de cette offre sp�ciale.');
define('TEXT_VISIT', 'Bonne visite : ' . HTTP_SERVER . DIR_WS_CATALOG);
define('TEXT_ENTER_CODE', ' et entrez le code ');

define('TABLE_HEADING_ACTION', 'Action');

define('CUSTOMER_ID', 'Identifiant client');
define('CUSTOMER_NAME', 'Nom du client');
define('REDEEM_DATE', 'Date d\'utilisation');
define('IP_ADDRESS', 'Adresse IP');

define('TEXT_REDEMPTIONS', 'Rapport d\'utilisation');
define('TEXT_REDEMPTIONS_TOTAL', 'Total d\'utilisation');
define('TEXT_REDEMPTIONS_CUSTOMER', 'Pour ce client');
define('TEXT_NO_FREE_SHIPPING', 'Aucune livraison gratuite');

define('NOTICE_EMAIL_SENT_TO', 'Note : Email envoy&eacute; a %s');
define('ERROR_NO_CUSTOMER_SELECTED', 'Erreur : aucun client n\'a &eacute;t&eacute; s&eacute;lectionn&eacute;.');
define('COUPON_NAME', 'Nom du coupon');
//define('COUPON_VALUE', 'Valeur du coupon');
define('COUPON_AMOUNT', 'Valeur du coupon');
define('COUPON_CODE', 'Code du coupon');
define('COUPON_STARTDATE', 'Date d\'effet');
define('COUPON_FINISHDATE', 'Date de fin d\'effet');
define('COUPON_FREE_SHIP', 'Livraison gratuite');
define('COUPON_DESC', 'Description du coupon');
define('COUPON_MIN_ORDER', 'Commande minimum pour le coupon');
define('COUPON_USES_COUPON', 'Utilisations par coupon');
define('COUPON_USES_USER', 'Utilisations par client');
define('COUPON_PRODUCTS', 'Liste des produits valable');
define('COUPON_CATEGORIES', 'Liste des cat&eacute;gories valable');
define('VOUCHER_NUMBER_USED', 'Nombre d\'utilisation');
define('DATE_CREATED', 'Date de cr&eacute;ation');
define('DATE_MODIFIED', 'Date de modification');
define('TEXT_HEADING_NEW_COUPON', 'Cr&eacute;er un nouveau coupon');
define('TEXT_NEW_INTRO', 'Remplissez les diff&eacute;rentes informations du coupon.<br />');


define('COUPON_NAME_HELP', 'Donner un nom au ch�que cadeau');
define('COUPON_AMOUNT_HELP', 'Mettre "5" pour une remise fixe de 5 Euros ou bien "5%" pour une remise de 5%.');
define('COUPON_CODE_HELP', 'Vous pouvez indiquer le code du ch�que cadeau que vous souhaitez. <br />Laissez le formulaire vide pour obtenir un code automatiquement.');
define('COUPON_STARTDATE_HELP', 'D&eacute;but de la date d\'effet du coupon.');
define('COUPON_FINISHDATE_HELP', 'Fin de la date d\'effet du coupon.');
define('COUPON_FREE_SHIP_HELP', 'Offrir la livraison avec l\'utilisation de ce coupon');
define('COUPON_DESC_HELP', 'Description du ch�que cadeau (visible ???) pour le client');
define('COUPON_MIN_ORDER_HELP', 'Montant minimum d\'une commande &agrave; partir duquel le coupon devient valide');
define('COUPON_USES_COUPON_HELP', 'Le maximum de fois que le coupon peut etre utilis&eacute;.<br />Laisser vide si vous ne voulez pas de limite');
define('COUPON_USES_USER_HELP', 'Le maximum de fois qu\'un client peut utiliser ce coupon.<br />Laisser vide si vous ne voulez pas de limite.');
define('COUPON_PRODUCTS_HELP', 'Liste des ID des produits (s&eacute;par&eacute; par une virgule) que vous voulez associer au ch�que cadeau.<br />Laisser vide si vous ne voulez pas de limite.');
define('COUPON_CATEGORIES_HELP', 'Liste des cat&eacute;gories qui peuvent etre associ&eacute;s au coupon.<br />Laisser vide si vous ne voulez pas de limite.');
define('ERROR_NO_COUPON_AMOUNT', 'Erreur : Vous n\'avez pas remplis le minimum pour valider le ch�que cadeau');
define('ERROR_COUPON_EXISTS', 'Erreur : Un ch�que cadeau avec le m�me code existe d�j�');
define('COUPON_BUTTON_EMAIL_VOUCHER', 'Envoyer par email le cheque cadeau');
define('COUPON_BUTTON_EDIT_VOUCHER', 'Editer le cheque cadeau');
define('COUPON_BUTTON_DELETE_VOUCHER', 'Effacer le cheque cadeau');
define('COUPON_BUTTON_VOUCHER_REPORT', 'Statistique du cheque cadeau');
define('COUPON_STATUS', 'Status');
define('COUPON_STATUS_HELP', 'Indiquer le statut du ch�que cadeau.');
?>