<?php
/*
  $Id $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Fermeture de session :');
define('NAVBAR_TITLE', 'Fermeture de session');
define('TEXT_MAIN', '<br />Votre session sur <b>l�administration est ferme</b>. Merci de votre visite et � bient�t !<br /><br />');
define('TEXT_RELOGIN', 'R&eacute;ouverture de session');
?>
