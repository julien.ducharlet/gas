<?php
/*
  $Id: orders.php,v 1.25 2003/06/20 00:28:44 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

// ########## Ajout/editer commande et compte client ########## //-->
define('TABLE_HEADING_EDIT_ORDERS', 'Modifier la commande');
// ########## END - Ajout/editer commande et compte client ########## //-->

define('HEADING_TITLE', 'Les Commandes');
define('HEADING_TITLE_COMMANDE', 'Num�ro de commande :');
define('HEADING_TITLE_MEMBRE', 'Code Client :');
define('HEADING_TITLE_SEARCH', 'ID commande :');


define('TABLE_HEADING_COMMENTS', 'Commentaires');


define('TABLE_HEADING_QUANTITY', 'Qt�.');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Mod�le');
define('TABLE_HEADING_TAX', 'TVA');
define('TABLE_HEADING_TOTAL', 'Total');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Prix (ht)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Prix (ttc)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (ht)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total (ttc)');

define('TABLE_HEADING_CUSTOMER_NOTIFIED', 'Client notifi�');
define('TABLE_HEADING_DATE_ADDED', 'Date d\'ajout');

define('ENTRY_CUSTOMER', 'Adresse Client :');
define('ENTRY_SOLD_TO', 'VENDU A :');
define('ENTRY_DELIVERY_TO', 'Livraison � :');
define('ENTRY_SHIP_TO', 'LIVRE A :');
define('ENTRY_SHIPPING_ADDRESS', 'Exp�dition :');
define('ENTRY_BILLING_ADDRESS', 'Facturation :');
define('ENTRY_PAYMENT_METHOD', 'M�thode de paiement :');
define('ENTRY_CREDIT_CARD_TYPE', 'Type de carte de cr&eacute;dit :');
define('ENTRY_CREDIT_CARD_OWNER', 'Propri�taire de la carte de cr�dit :');
define('ENTRY_CREDIT_CARD_NUMBER', 'Num�ro de la carte de cr�dit :');
define('ENTRY_CREDIT_CARD_EXPIRES', 'Date d\'expiration de la carte de cr�dit :');
define('ENTRY_SUB_TOTAL', 'Sous-Total :');
define('ENTRY_TAX', 'TVA :');
define('ENTRY_SHIPPING', 'Exp�dition :');
define('ENTRY_TOTAL', 'Total :');
define('ENTRY_DATE_PURCHASED', 'Date d\'achat :');
define('ENTRY_STATUS', 'Modifier le Statut :');
define('ENTRY_DATE_LAST_UPDATED', 'Derni�re date de mise � jour :');
define('ENTRY_NOTIFY_CUSTOMER', 'Informer le client par email :');
define('ENTRY_NOTIFY_CUSTOMER_SMS_AU', 'Au : ');
define('ENTRY_NOTIFY_COMMENTS', 'Ajouter le commentaire dans l\'email :');
define('ENTRY_PRINTABLE', 'Imprimer la facture');

define('TEXT_INFO_HEADING_DELETE_ORDER', 'Supprimer la commande');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer cette commande ?');
define('TEXT_INFO_RESTOCK_PRODUCT_QUANTITY', 'Restaurer la valeur de stock');
define('TEXT_DATE_ORDER_CREATED', 'Date de cr&eacute;ation :');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Derni�re modification :');
define('TEXT_INFO_PAYMENT_METHOD', 'M�thode de paiement :');


define('TEXT_NO_ORDER_HISTORY', 'Aucun historique de commande disponible');

define('EMAIL_SEPARATOR', '======================================================');
define('EMAIL_TEXT_SUBJECT', 'Mise a jour de votre commande');
define('EMAIL_TEXT_ORDER_NUMBER', 'Num�ro de commande :');
define('EMAIL_TEXT_INVOICE_URL', 'Facture d�taill�e :');
define('EMAIL_TEXT_DATE_ORDERED', 'Date de commande :');
define('EMAIL_TEXT_STATUS_UPDATE', '');
define('EMAIL_TEXT_COMMENTS_UPDATE', STORE_NAME.' vous informe : ' . "\n\n%s\n");

define('ERROR_ORDER_DOES_NOT_EXIST', 'Erreur : La commande n\'existe pas.');
define('SUCCESS_ORDER_UPDATED', 'Bravo : La commande est mise � jour avec succ�s.');
define('WARNING_ORDER_NOT_UPDATED', 'Attention : Aucune modification n\'a �t� effectu�e. La commande n\'a pas �t� mis � jour.');
define('WARNING_ORDER_WITH_NUMERO', 'Attention : Un nouveau num&eacute;ro de facture n\'a pas &eacute;t&eacute; g&eacute;n&eacute;r&eacute;, car la commande avait d&eacute;j&agrave; fait l\'objet d\'une facture.');
// SUIVI COLLISSIMO+UPS MODIFICATION POUR REMPLACER UPS PAR DISTINGO + AJOUT DE CHRONOPOST
define('TABLE_HEADING_TRACKING', 'Num�ro De Suivi');
define('TABLE_HEADING_TRACK1', 'Envoi par Colissimo :');
define('TABLE_HEADING_TRACK2', 'Envoi par Distingo :');
define('TABLE_HEADING_TRACK3', 'Envoi par Chronopost :');
define('URL_TO_TRACK1', 'https://www.coliposte.net/particulier/suivi_particulier.jsp?colispart=');
define('URL_TO_TRACK2', 'https://www.csuivi.courrier.laposte.fr/default.asp?EZ_ACTION=rechercheRapide&tousObj=&numObjet=');
define('URL_TO_TRACK3', 'https://www.fr.chronopost.com/fr/tracking/result?listeNumeros=');
define('ENTRY_NOTIFY_TRACKING', 'Afficher le suivi colis dans l\'email :');
define('EMAIL_TEXT_TRACKING_NUMBER', 'Vous pouvez suivre votre colis en cliquant sur le lien ci-dessous.');
// FIN SUIVI COLLISSIMO+UPS MODIFICATION POUR REMPLACER UPS PAR DISTINGO + AJOUT DE CHRONOPOST

// POUR SMS
define('ENTRY_NOTIFY_CUSTOMER_SMS', 'Informer le client par SMS :');
define('ENTRY_PRICE_SMS', '(au prix de 0.06 �uro)');
define('MESSAGE_SMS', 'Votre commande vient d��tre mise � jour sur le site '.WEBSITE.'. Connectez vous sur votre compte pour voir les modifications.');
// FIN POUR SMS
?>