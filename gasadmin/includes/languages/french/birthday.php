<?php
/*
  Page: birthday.php
*/

define('HEADING_BIRTHDAYS', 'Les Anniverssaires des clients'); 

define('TEXT_BIRTHDAYS', '<br /><b>Ajouter sur cette page la possibilit� de pouvoir selectionner par des cases a cocher les clients pour pouvoir envoyer un email group� ou voir m�me la newsletter.</b><br />'); 
define('BOX_CUSTOMERS_BIRTHDAY', 'Anniversaires');
define('TEXT_BIRTHDAYS_NOWMONTH', ' Ce mois-ci');
define('TEXT_BIRTHDAYS_NEXTMONTH', ' Le mois prochain');
define('TEXT_BIRTHDAYS_NAME', ' Nom du client');
define('TEXT_BIRTHDAYS_EMAIL', ' E-mail client');
define('TEXT_BIRTHDAYS_DATE', ' Date anniversaire');
define('ICON_ORDERS', 'Voir toutes les commandes');

?>
