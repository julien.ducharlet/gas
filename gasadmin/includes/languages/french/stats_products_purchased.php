<?php
/*
  $Id: stats_products_purchased.php,v 1.5 2002/03/30 15:52:31 harley_vb Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Les produits les plus achet&eacute;s');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_PURCHASED', 'Achet&eacute;');

define('TABLE_TEXT_RAZ','Remettre � z�ro les compteurs');
define('TABLE_TEXT_RAZ_CONFIRM','�tes vous certain(e)s de vouloir remettre � z�ro les compteurs ?');
define('TABLE_TEXT_RAZ_END',' produit(s) remis � 0.');

define('FILTRE_TEXT','Afficher ');
define('FILTRE_OPTION_1','Les produits achet�s au moins 1 fois');
define('FILTRE_OPTION_2','Tous les produits');
define('FILTRE_OPTION_3','Les produits achet�s aucune fois');


?>