<?php
//Fichier de langue du script de mise a jour des stocks

define('EMAIL_SUJET', 'Arrivage de produits');
define('EMAIL_DEBUT', 'Bonjour,<br /><br />');
define('EMAIL_INTRO', 'Vous recevez cet email car vous avez demand� � etre averti de l\'arrivage dans nos stocks des articles suivants :  <br /><br />');
define('EMAIL_FOOTER', 'Nous esp�rons vous revoir prochainement sur '.STORE_NAME.'<br /><br />Tr�s cordialement,<br /><br />'.STORE_NAME.'<br /><a href="'.WEBSITE.'">'.WEBSITE.'/</a>');
?>