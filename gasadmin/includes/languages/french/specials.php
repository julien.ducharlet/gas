<?php
/*
  $Id: specials.php,v 1.10 2002/03/16 15:07:21 project3000 Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  	 

define('TEXT_SPECIALS_TAX_CLASS', 'TVA : ');
define('TEXT_SPECIALS_PRICE_ACHAT', 'Prix d\'achat (HT) :');
define('TEXT_SPECIALS_PRICE_GROSS', 'Prix promotionnel (TTC) :');

define('HEADING_TITLE', 'Promotions');

define('TABLE_HEADING_PRODUCTS', 'Produits');

define('TABLE_HEADING_PRODUCTS_PRICE_HT', 'Prix Article (HT)');
define('TABLE_HEADING_PRODUCTS_PRICE_TTC', 'Prix Article (TTC)');

define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_SPECIALS_PRODUCT', 'Produit :');

define('TEXT_SPECIALS_SPECIAL_PRICE', 'Prix promotionnel (HT) :');
define('TEXT_SPECIALS_PRICE_GROSS', 'Prix promotionnel (TTC) :');
define('TEXT_SPECIALS_TAX_CLASS', 'TVA : ');

define('TEXT_SPECIALS_EXPIRES_DATE', 'Date d\'expiration :');
define('TEXT_SPECIALS_PRICE_TIP', '<b>Remarque sur les promotions :</b><ul><li>Si vous entrez un prix, le s&eacute;parateur d&eacute;cimal doit &ecirc;tre un \'.\' (point d&eacute;cimal), exemple: <b>49.99</b></li><li>Laissez la date d\'&eacute;ch&eacute;ance vide pour une promotion illimit�e</li></ul>');

define('TEXT_INFO_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_INFO_LAST_MODIFIED', 'derni&egrave;re modification :');
define('TEXT_INFO_NEW_PRICE', 'Nouveau Prix :');
define('TEXT_INFO_ORIGINAL_PRICE', 'Prix original :');
define('TEXT_INFO_PERCENTAGE', 'Pourcentage :');
define('TEXT_INFO_EXPIRES_DATE', 'Expire le :');
define('TEXT_INFO_STATUS_CHANGE', 'Changement de statut :');

define('TEXT_INFO_HEADING_DELETE_SPECIALS', 'Supprimer promotion');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous s&ucirc;r de vouloir supprimer cette promotion ?');
?>