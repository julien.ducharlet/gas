<?php
/* */

if ($HTTP_GET_VARS['origin'] == FILENAME_CHECKOUT_PAYMENT) {
  define('NAVBAR_TITLE', 'Ordre');
  define('HEADING_TITLE', 'La commande en ligne est facile.');
  define('TEXT_STEP_BY_STEP', 'Nous suivrons avec vous pour le processus, point par point.');
} else {
  define('NAVBAR_TITLE', 'Ouverture');
  define('HEADING_TITLE', 'Bienvenue, signalez vous SVP !');
  define('TEXT_STEP_BY_STEP', ''); // should be empty
}

define('HEADING_RETURNING_ADMIN', 'Panneau d\'ouverture :');
define('HEADING_PASSWORD_FORGOTTEN', 'Mot de passe oubli� :');
define('TEXT_RETURNING_ADMIN', 'Personnel seulement !');
define('ENTRY_EMAIL_ADDRESS', 'Adresse Email :');
define('ENTRY_PASSWORD', 'Mot de passe :');
define('ENTRY_FIRSTNAME', 'Pr&eacute;nom :');
define('IMAGE_BUTTON_LOGIN', 'Confirmer');

define('TEXT_PASSWORD_FORGOTTEN', 'Mot de passe oubli&eacute; ?');

define('TEXT_LOGIN_ERROR', '<font color="#ff0000"><b>ERREUR :</b></font> Mauvais email ou mot de passe !');
define('TEXT_FORGOTTEN_ERROR', '<font color="#ff0000"><b>ERREUR :</b></font> Probl&egrave;me de pr&eacute;nom ou de mot de passe !');
define('TEXT_FORGOTTEN_FAIL', 'Vous avez que 3 essais pour des raisons de s&eacute;curit&eacute;, contactez SVP votre administrateur pour obtenir un nouveau mot de passe.<br />&nbsp;<br />&nbsp;');
define('TEXT_FORGOTTEN_SUCCESS', 'Le nouveau mot de passe va &ecirc;tre envoy&eacute; &agrave; votre adresse email. Veuillez v&eacute;rifier votre email et essayer de nouveau une ouverture.<br />&nbsp;<br />&nbsp;');

define('ADMIN_EMAIL_SUBJECT', 'Nouveau mot de passe'); 
define('ADMIN_EMAIL_TEXT', 'Bonjour %s,' . "\n\n" . 'Vous pouvez acc�der au panneau d\'administration avec le mot de passe suivant. Une fois que vous acc�dez � l\'administration, changez svp votre mot de passe ! ' . "\n\n" . 'Site Web : %s' . "\n" . 'Nom d\'utilisateur : %s' . "\n" . 'Mot de passe: %s' . "\n\n" . 'Merci !' . "\n" . '%s' . "\n\n" . 'Ceci est un message automatis�, veuillez ne pas repondre !'); 
?>
