<?php
/*
  $Id: recover_cart_sales.php,v 1.5 2005/06/22 06:10:35 lane Exp $
  Recover Cart Sales v2.22 FRENCH Language File

  Recover Cart Sales contribution: JM Ivler (c)
  Copyright (c) 2003-2005 JM Ivler / Ideas From the Deep / OSCommerce
  https://www.oscommerce.com

  Released under the GNU General Public License

  Modifed by Aalst (recover_cart_sales.php,v 1.2 .. 1.36)
  aalst@aalst.com
  
  Modifed by willross (recover_cart_sales.php,v 1.4)
  reply@qwest.net
  - don't forget to flush the 'scart' db table every so often

  Modifed by Lane (stats_recover_cart_sales.php,v 1.4d .. 2.22)
  lane@ifd.com www.osc-modsquad.com / www.ifd.com
*/

define('MESSAGE_STACK_CUSTOMER_ID', 'Panier pour Client-ID ');
define('MESSAGE_STACK_DELETE_SUCCESS', ' suppression r�ussie');
define('HEADING_TITLE', 'Traiter les paniers non valid�s v2.22');
define('HEADING_EMAIL_SENT', 'rapport par e-mail');
define('EMAIL_TEXT_LOGIN', 'Pour vous connecter � votre compte sur '.STORE_NAME.' : ');
define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_SUBJECT', 'Demande de '.  STORE_NAME );
define('EMAIL_TEXT_SALUTATION', 'Bonjour ' );
define('EMAIL_TEXT_NEWCUST_INTRO', "\n\n" . 'Vous avez choisi ' . STORE_NAME .
                                   ' pour vos achats. ');
define('EMAIL_TEXT_CURCUST_INTRO', "\n\n" . 'Nous vous remercions de la visite que vous avez faite sur ' . STORE_NAME . '. ');
define('EMAIL_TEXT_BODY_HEADER', 'Nous avons remarqu� lors de cette visite sur notre boutique, que vous avez choisi les articles suivants:' . "\n\n");
define('EMAIL_TEXT_BODY_FOOTER', 'Nous serions int�ress�s de savoir ce qui s\'est produit et s\'il y a une raison pour laquelle vous
avez d�cid� de ne pas acheter sur ' . STORE_NAME . '.' .
                                 "\n\n" . 'Auriez vous l\'amabilit� et la gentillesse de nous expliquer la raison de la non finalisation de votre commande ? ' . "\n" . 'Toujours soucieux de proposer le meilleur service � nos clients, votre exp�rience nous permettrait d\'am�liorer nos services pour vous faires des propositions adapt�es � vos attentes.' . "\n\n" . 'Cordialement,' ."\n\n");
define('DAYS_FIELD_PREFIX', 'Montrer depuis les derniers ');
define('DAYS_FIELD_POSTFIX', ' jours ');
define('DAYS_FIELD_BUTTON', 'Go');
define('TABLE_HEADING_DATE', 'DATE');
define('TABLE_HEADING_CONTACT', 'CONTACT');
define('TABLE_HEADING_CUSTOMER', 'NOM');
define('TABLE_HEADING_EMAIL', 'E-MAIL');
define('TABLE_HEADING_PHONE', 'TELEPHONE');
define('TABLE_HEADING_MODEL', 'MODEL');
define('TABLE_HEADING_DESCRIPTION', 'ARTICLE');
define('TABLE_HEADING_QUANTY', 'QTY');
define('TABLE_HEADING_PRICE', 'PRIX');
define('TABLE_HEADING_TOTAL', 'TOTAL');
define('TABLE_GRAND_TOTAL', 'Grand Total: ');
define('TABLE_CART_TOTAL', 'Panier Total: ');
define('TEXT_CURRENT_CUSTOMER', 'CUSTOMER');
define('TEXT_SEND_EMAIL', 'Envoyer e-mail');
define('TEXT_RETURN', '[Retour]');
define('TEXT_NOT_CONTACTED', 'Non contact�');
define('PSMSG', 'facultatif PS Message: ');
?>