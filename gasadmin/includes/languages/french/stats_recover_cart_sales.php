<?php
/*
  $Id: stats_recover_cart_sales.php,v 1.4 2005/06/22 06:10:35 lane Exp $
  Recover Cart Sales FRENCH Language File v2.22

  Recover Cart Sales contribution: JM Ivler 11/23/03
  Copyright (c) 2003-2005 JM Ivler / Ideas From the Deep / OSCommerce
  https://www.oscommerce.com

  Released under the GNU General Public License

  Modifed by Aalst (stats_recover_cart_sales.php,v 1.2 .. 1.36)
  aalst@aalst.com

  Modifed by Lane (stats_recover_cart_sales.php,v 1.4d .. 2.22)
  lane@ifd.com www.osc-modsquad.com / www.ifd.com
*/

define('HEADING_TITLE', 'Rapport paniers non valid�s');
define('DAYS_FIELD_PREFIX', 'Montrer depuis ');
define('DAYS_FIELD_POSTFIX', ' jours ');
define('DAYS_FIELD_BUTTON', 'Go');
define('TABLE_HEADING_SCART_ID', 'SCart ID');
define('TABLE_HEADING_NB_ORDER', 'Commande');
define('TABLE_HEADING_SCART_DATE', 'Date d\'ajout');
define('TABLE_HEADING_CUSTOMER', 'NOM');
define('TABLE_HEADING_ORDER_DATE', 'Date de commande');
define('TABLE_HEADING_ORDER_STATUS', 'Statut');
define('TABLE_HEADING_ORDER_AMOUNT', 'Montant');
define('TABLE_HEADING_DELETE_ACTION', 'Suppression');
define('TOTAL_RECORDS', 'Enregistrement examin�s:');
define('NUMBER_RELANCE', 'Nombre de relances:');
define('TOTAL_SALES', 'Ventes:');
define('TOTAL_SALES_EXPLANATION', ' clients notifi�s ont achet�');
define('ZERO_EXPLANATION', ' aucun client notifi� a achet�');
define('TOTAL_RECOVERED', 'Total Recouvr�:');
define('TEXT_ALL_MOIS','Tous les mois');
define('TEXT_ALL_ANNEE','Toutes les ann&eacute;es');
?>