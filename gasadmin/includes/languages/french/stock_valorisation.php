<?php
/*
  Page : stock_valorisation.php
*/

define('TOP_BAR_TITLE', 'Valorisation du Stock');
define('HEADING_TITLE', 'Valorisation du Stock');

define('TABLE_HEADING_CATEGORIE', 'Type Article');
define('TABLE_HEADING_NAME', 'Nom du Produit');
define('TABLE_HEADING_QT', 'Quantit�');
define('TABLE_HEADING_PURCHASE_PRICE', 'PA');
define('TABLE_HEADING_TOT_PURCHASE_PRICE', 'Total PA');
define('TABLE_HEADING_COST_PURCHASE_PRICE', 'PPCost');
define('TABLE_HEADING_SALE_PRICE', 'PV');
define('TABLE_HEADING_TOT_SALE_PRICE', 'Total PV');
define('TABLE_HEADING_COST_SALE_PRICE', 'SPCost');
define('TABLE_HEADING_TCOST_PURCHASE_PRICE', 'Co�t Total d\'achat : ');
define('TABLE_HEADING_TCOST_SALE_PRICE', 'Co�t Total de Vente : ');
define('TABLE_HEADING_TQT_ITEM', 'Quantit� Total d\'Article(s) : ');
define('TEXT_EXPORT_BUTTON', 'EXPORT CVS');
define('EXCLUDE_ID', 'ID DES CATEGORIES EXCLUS : ');
?>