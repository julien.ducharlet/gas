<?php
/*
  $Id: margin_report.php,v 2.56 2004/08/18 18:50:51 chaicka Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2004 osCommerce

  Released under the GNU General Public License
*/

define('MAX_DISPLAY_MARGIN_RESULTS', '50');
define('HEADING_TITLE', 'Rapport de Marge');

define('TABLE_HEADING_PRODUCT', 'Article ');
define('TABLE_HEADING_COST', 'Prix achat HT');
define('TABLE_HEADING_PRICE', 'Prix vente HT');
define('TABLE_HEADING_SPECIAL_PRICE', 'Prix Promo HT');
define('TABLE_HEADING_MARGIN_DOLLARS', 'Marge (�)');
define('TABLE_HEADING_MARGIN_PERCENTAGE', 'Marge (%)');

define('TEXT_FILE_DOES_NOT_EXIST', 'Ce fichier n\'existe pas');
define('TEXT_CACHE_DIRECTORY', 'R�pertoire Cache :');
define('TEXT_SHOW', 'Afficher :');
define('TEXT_ALL_MANUFACTURERS', 'Tous les fabricants');
define('TEXT_ALL_CATEGORIES_BY_MANUFACTURER', 'Toutes les cat�gories par Fabricants');
define('TEXT_ALL_CATEGORIES', 'Toutes les cat�gories');

define('ERROR_CACHE_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le r�pertoire Cache n\'existe pas. Merci de r�gler cela Configuration->Cache.');
define('ERROR_CACHE_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le r�pertoire Cache n\'est inscriptible.');

define('TEXT_SORT_PRODUCTS', 'Tri des produits ');
define('TEXT_DESCENDINGLY', 'descendant');
define('TEXT_ASCENDINGLY', 'ascendant');
define('TEXT_BY', ' par ');
define('TEXT_NO_PRODUCTS', 'Il n\'y a pas de produit dans la base.');
define('TEXT_RESULT_PAGES', 'R�sultat : ');

define('BUTTON_BACK_TO_MAIN', 'Retour Menu Principal');
define('TEXT_EXPORT_BUTTON', 'Exportation Excel');

define('TEXT_SELECT_REPORT', 'Choisissez un rapport');
define('TEXT_SELECT_REPORT_DAILY', 'Aujourd\'hui');
define('TEXT_SELECT_REPORT_WEEKLY', 'Cette semaine');
define('TEXT_SELECT_REPORT_MONTHLY', 'Ce mois');
define('TEXT_SELECT_REPORT_QUARTERLY', 'Par trimestre ');
define('TEXT_SELECT_REPORT_SEMIANNUALLY', 'Semestriel');
define('TEXT_SELECT_REPORT_ANNUALLY', 'Annuelle');

define('TEXT_REPORT_HEADER', 'Ce rapport est pour tous les ordres. ');
define('TEXT_REPORT_HEADER_FROM_DAY', 'Ce rapport est pour la date : ');
define('TEXT_REPORT_HEADER_FROM_YESTERDAY', 'Ce rapport est pour hier : ');
define('TEXT_REPORT_HEADER_FROM_WEEK', 'Ce rapport est depuis le d�but de la semaine : ');
define('TEXT_REPORT_HEADER_FROM_MONTH', 'Ce rapport est depuis le d�but du mois : ');
define('TEXT_REPORT_HEADER_FROM_QUARTER', 'Ce rapport est pour le trimestre d�butant le : ');
define('TEXT_REPORT_HEADER_FROM_SEMIYEAR', 'Ce rapport est pour le semestre d�butant le : ');
define('TEXT_REPORT_HEADER_FROM_YEAR', 'Ce rapport est depuis le d�but de l\'ann�e : ');
define('TEXT_REPORT_BETWEEN_DAYS', 'Ce rapport est pour les dates entre ');
define('TEXT_AND', ' et ');
define('TEXT_ORDER_ID', 'ID Commande');
define('TEXT_PRODUCTS_NAME', 'Nom du Produit');
define('TEXT_PRODUCTS_COST', 'Co�t du Produit');
define('TEXT_PRODUCTS_PRICE', 'Prix du Produit');
define('TEXT_SPECIAL_PRICE', 'Prix Promo');
define('TEXT_MARGIN_MONEY', 'Marge en Euros');
define('TEXT_MARGIN_PERCENTAGE', 'Marge en %');
define('TEXT_TOTAL_ITEMS_SOLD', 'Nbre Articles Vendus');
define('TEXT_ITEMS_SOLD', 'Nbre Articles Vendus');
define('TEXT_SALES_AMOUNT', 'Montant des Ventes');
define('TEXT_COST', 'Prix d\'achat');
define('TEXT_TOTAL_COST', 'Prix d\'achat Total');
define('TEXT_TOTAL_GROSS_PROFIT', 'B�n�fice Brut Total');
define('TEXT_GROSS_PROFIT', 'B�n�fice Brut');
define('TEXT_TOTAL', 'Total ');
define('TEXT_QUERY_DATES', 'Choisissez les dates (YYYY-MM-DD) :');
define('TEXT_START_DATE', 'Date du D�but');
define('TEXT_END_DATE', 'Date de Fin');
?>