<?php

define('HEADING_TITLE', 'Commandes Mensuelle');

define('TABLE_HEADING_MODEL', 'R�f�rence'); 
define('TABLE_HEADING_NAME', 'Nom de l\'article');
define('TABLE_HEADING_QUANTITY', 'Quantit�');
define('TABLE_HEADING_TOTAL', 'Total HT');
define('TABLE_HEADING_TOTAL_TAX', 'Total TTC');
define('TEXT_MONTHLY_SALES', 'Montant Total HT du mois :');
define('TEXT_MONTHLY_SALES_TAX', 'Montant Total TTC du mois :');
?>