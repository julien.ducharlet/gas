<?php
/*
  $Id: english.php,v 1.106 2003/06/20 00:18:31 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
// Ajout pour coupon reduction
define('BOX_HEADING_GV_ADMIN', 'Les Ch�ques Cadeaux');
define('BOX_COUPON_ADMIN','Cr�ation et Gestion');
define('BOX_GV_ADMIN_QUEUE', 'Ch�que cadeau en attente');
define('BOX_GV_ADMIN_MAIL', 'Ch�que cadeau par email');
define('BOX_GV_ADMIN_SENT', 'Ch�que cadeau envoy�');
define('BOX_REPORTS_CREDITS', 'Les ch�ques cadeaux');

define('IMAGE_RELEASE', 'Redeem Gift Voucher');
define('_JANUARY', 'Janvier');
define('_FEBRUARY', 'Fevrier');
define('_MARCH', 'Mars');
define('_APRIL', 'Avril');
define('_MAY', 'Mai');
define('_JUNE', 'Juin');
define('_JULY', 'Juillet');
define('_AUGUST', 'Aout');
define('_SEPTEMBER', 'Septembre');
define('_OCTOBER', 'Octobre');
define('_NOVEMBER', 'Novembre');
define('_DECEMBER', 'Decembre');

define('TEXT_DISPLAY_NUMBER_OF_GIFT_VOUCHERS', 'Afficher <b>%d</b> � <b>%d</b> (sur <b>%d</b> gift vouchers)');
define('TEXT_DISPLAY_NUMBER_OF_COUPONS', 'Afficher <b>%d</b> � <b>%d</b> (sur <b>%d</b> coupons)');

define('TEXT_VALID_PRODUCTS_LIST', 'Liste article');
define('TEXT_VALID_PRODUCTS_ID', 'ID articles');
define('TEXT_VALID_PRODUCTS_NAME', 'Nom Article');
define('TEXT_VALID_PRODUCTS_MODEL', 'R�f�rence Article');

define('TEXT_VALID_CATEGORIES_LIST', 'Liste des Cat�gorie');
define('TEXT_VALID_CATEGORIES_ID', 'ID cat�gorie');
define('TEXT_VALID_CATEGORIES_NAME', 'Nom de la Cat�gorie');

//modifs
define('BOX_REFERENCEMENT_TITRE', 'Outils R�f�rencements');
define('BOX_REFERENCEMENT_GOOGLE_SITEMAPS', 'GOOGLE SiteMaps');
define('BOX_REFERENCEMENT_GOOGLE_LISTE1', 'liste 1 pour Google');
define('BOX_REFERENCEMENT_GOOGLE_LISTE2', 'liste 2 pour Google');
define('BOX_REFERENCEMENT_GOOGLE_LISTE3', 'liste 3 pour Google');

define('BOX_STOCK_TITRE', 'Les Stocks / Fournisseurs');
define('BOX_STOCK_FOURNISSEURS', 'Gestion des fournisseurs');
define('BOX_STOCK_COMMANDE', 'Faire une commande');
define('BOX_STOCK_LIVRAISON', 'Livraison fournisseur');
define('BOX_STOCK_REF_FOURNISSEUR', 'Mise � jour Rapide');
define('BOX_STOCK_INFO_CLIENT', 'Pr�venir Client New Stock');
define('BOX_STOCK_VALORISATION', 'Valorisation du Stock');

//AJAX Orders Editor
define('DIV_ADD_PRODUCT_HEADING', '&nbsp;Ajouter un produit');
define('ADD_PRODUCT_SELECT_PRODUCT', '&nbsp;Nom ou r�f�rence de l\'article :');
define('PRODUCTS_SEARCH_RESULTS', '&nbsp;R&eacute;sultats de la recherche: ');
define('PRODUCTS_SEARCH_NO_RESULTS', '&nbsp;Il n\'y a pas de produit correspondant &agrave; votre recherche.');
define('TEXT_PRODUCT_OPTIONS', '&nbsp;Options');

//Admin begin
// header text in includes/header.php
define('HEADER_TITLE_ACCOUNT', 'Mon compte');
define('HEADER_TITLE_LOGOFF', 'Fermeture session');

// Admin Account
define('BOX_HEADING_MY_ACCOUNT', 'Mon compte');

// configuration box text in includes/boxes/administrator.php
define('BOX_HEADING_ADMINISTRATOR', 'L\'Administration');
define('BOX_ADMINISTRATOR_MEMBERS', 'Membres & Groupes');
define('BOX_ADMINISTRATOR_MEMBER', 'Membres');
define('BOX_ADMINISTRATOR_BOXES', 'Gestion des bo&icirc;tes');

// images
define('IMAGE_FILE_PERMISSION', 'Permission de dossier');
define('IMAGE_GROUPS', 'Liste des groupes');
define('IMAGE_INSERT_FILE', 'Editer le dossier');
define('IMAGE_MEMBERS', 'Liste des membres');
define('IMAGE_NEW_GROUP', 'Nouveau groupe');
define('IMAGE_NEW_MEMBER', 'Nouveau membres');
define('IMAGE_NEXT', 'Suivant');

// constants for use in tep_prev_next_display function
define('TEXT_DISPLAY_NUMBER_OF_FILENAMES', 'Affichage <b>%d</b> sur <b>%d</b> (Sur un total de <b>%d</b> fichiers)');
define('TEXT_DISPLAY_NUMBER_OF_MEMBERS', 'Affichage <b>%d</b> sur <b>%d</b> (Sur un total de <b>%d</b> membres)');
//Admin end

// look in your $PATH_LOCALE/locale directory for available locales..
// on RedHat6.0 I used 'en_US'
// on FreeBSD 4.0 I use 'en_US.ISO_8859-1'
// this may not work under win32 environments..
setlocale(LC_TIME, 'fr_FR.ISO_8859-1');
define('DATE_FORMAT_SHORT', '%d/%m/%Y');  // this is used for strftime()
define('DATE_FORMAT_LONG', '%A %d %B %Y'); // this is used for strftime()
define('DATE_FORMAT', 'd/m/Y'); // this is used for date()
define('PHP_DATE_TIME_FORMAT', 'd/m/Y H:i:s'); // this is used for date()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('DATE_MOIS', '%m'); // this is used for date()

////
// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
function tep_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 0, 2) . substr($date, 3, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 3, 2) . substr($date, 0, 2);
  }
}

// Global entries for the <html> tag
define('HTML_PARAMS','dir="ltr" lang="fr"');

// charset for web pages and emails
define('CHARSET', 'iso-8859-1');

// page title
define('TITLE', STORE_NAME);

// header text in includes/header.php
define('HEADER_TITLE_TOP', 'Administration');
define('HEADER_TITLE_SUPPORT_SITE', 'Site officiel');
define('HEADER_TITLE_ONLINE_CATALOG', 'Catalogue en ligne');
define('HEADER_TITLE_ADMINISTRATION', 'Administration');

// text for gender
define('MALE', 'Monsieur');
define('FEMALE', 'Madame');
define('MLLE', 'Mademoiselle');

// text for date of birth example
define('DOB_FORMAT_STRING', 'jj/mm/aaaa');

// configuration box text in includes/boxes/configuration.php
define('BOX_HEADING_CONFIGURATION', 'La Configuration');
define('BOX_CONFIGURATION_MYSTORE', 'Mon magasin');
define('BOX_CONFIGURATION_LOGGING', 'Enregistrement');
define('BOX_CONFIGURATION_CACHE', 'Cache');

// modules box text in includes/boxes/modules.php
define('BOX_HEADING_MODULES', 'Les Modules');
define('BOX_MODULES_PAYMENT', 'De Paiement');
define('BOX_MODULES_SHIPPING', 'De Livraison');
define('BOX_MODULES_ORDER_TOTAL', 'Config. Total commande');

// categories box text in includes/boxes/catalog.php
define('BOX_HEADING_CATALOG', 'La Gestion des Articles'); // Le Catalogue
// define_mainpage
define('BOX_CATALOG_DEFINE_MAINPAGE', 'Gestion Page d\'Accueil');
// define_mainpage_eof
define('BOX_CATALOG_CATEGORIES_PRODUCTS', 'Les Cat&eacute;gories/Produits');
// DEBUT Contribution DUPLICATE
define('BOX_CATALOG_DUPLICATE', 'Dupliquer un article 1');
// FIN Contribution DUPLICATE

// DEBUT Contribution DUPLICATE
define('BOX_CATALOG_DUPLICATE_2', 'Dupliquer un article 2');
// FIN Contribution DUPLICATE

// DEBUT DELETE BY REF
define('BOX_CATALOG_DELETE_BY_REF', 'Effacer toute une reference');
// FIN DELETE BY REF

define('BOX_CATALOG_CATEGORIES_PRODUCTS_ATTRIBUTES', 'Gestion Options Articles');
define('BOX_CATALOG_MANUFACTURERS', 'Gestion des Fabricants');
define('BOX_CATALOG_REVIEWS', 'Gestion des Critiques');
define('BOX_CATALOG_SPECIALS', 'Gestion des Promotions');
define('BOX_CATALOG_PRODUCTS_EXPECTED', 'Articles en attente');
define('BOX_CATALOG_QUICK_UPDATES', 'Mise � jour rapide');
define('BOX_CATALOG_IMAGES_A_AJOUTER', 'Liste des articles sans photo');
define('BOX_CATALOG_ARTICLES_A_AJOUTER', 'Liste des articles � Ajouter');
define('BOX_CATALOG_TELEPHONE_SANS_IMAGES', 'Telephone sans image');


// customers box text in includes/boxes/customers.php
define('BOX_HEADING_CUSTOMERS', 'Les Clients');

define('BOX_CREATE_ACCOUNT', 'Cr�er un compte client');
define('BOX_CUSTOMERS_CUSTOMERS', 'Les Clients');
define('BOX_CUSTOMERS_STATS_CUSTOMER_REGISTRATION', 'Stats des Inscriptions');
define('BOX_CUSTOMERS_BIRTHDAY', 'Les Anniverssaires du mois');


// customers box text in includes/boxes/orders.php
define('BOX_HEADING_ORDERS', 'Les Commandes');

define('BOX_CUSTOMERS_ORDERS', 'Les Commandes');
define('BOX_TOOLS_RECOVER_CART', 'Voir les Paniers Perdus');
define('BOX_REPORTS_RECOVER_CART_SALES', 'Stats des r�cup�rations');
define('BOX_REPORTS_COMMANDES', 'Stats Commandes');



// taxes box text in includes/boxes/taxes.php
define('BOX_HEADING_LOCATION_AND_TAXES', 'Les Lieux & Les Taxes');
define('BOX_TAXES_COUNTRIES', 'Les Pays');
define('BOX_TAXES_ZONES', 'Les Zones');
define('BOX_TAXES_GEO_ZONES', 'Les Zones fiscales');
define('BOX_TAXES_TAX_CLASSES', 'Les Classes fiscales');
define('BOX_TAXES_TAX_RATES', 'Les Taux fiscaux');

// reports box text in includes/boxes/reports.php
define('BOX_HEADING_REPORTS', 'Les Rapports');

define('BOX_REPORTS_PRODUCTS_VIEWED', 'Les Articles les plus vus');
define('BOX_REPORTS_PRODUCTS_PURCHASED', 'Les Articles recherch�s');
define('BOX_REPORTS_MARGIN_REPORT', 'Les Rapports de marge');
define('BOX_REPORTS_ORDERS_TOTAL', 'Liste des Meilleurs Clients');
define('BOX_REPORTS_CUSTOMERS_ORDERS', 'Les Statistiques clients'); 
define('BOX_REPORTS_MARGIN_COMPLETE', 'Les Statistiques TEST'); 
define('BOX_REPORTS_PRODUCTS_ORDERS','Statiques produits vendus');

// tools text in includes/boxes/tools.php
define('BOX_HEADING_TOOLS', 'Les Outils');
define('BOX_TOOLS_BACKUP', 'Sauvegarde de la BDD');
define('BOX_TOOLS_BANNER_MANAGER', 'Gestionnaire de banni�re');
define('BOX_TOOLS_CACHE', 'Controle du cache');
define('BOX_TOOLS_DEFINE_LANGUAGE', 'D�finissez les langues');
define('BOX_TOOLS_FILE_MANAGER', 'Gestionnaire de fichiers');
define('BOX_TOOLS_MAIL', 'Envoyez un Email');
define('BOX_TOOLS_NEWSLETTER_MANAGER', 'Gestion Newsletter');
define('BOX_TOOLS_SERVER_INFO', 'Info sur serveur');
define('BOX_TOOLS_WHOS_ONLINE', 'Qui est en ligne ?');

// localizaion box text in includes/boxes/localization.php
define('BOX_HEADING_LOCALIZATION', 'Devises/Langues/Statuts');
define('BOX_LOCALIZATION_CURRENCIES', 'Les Devises');
define('BOX_LOCALIZATION_LANGUAGES', 'Les Langues');
define('BOX_LOCALIZATION_ORDERS_STATUS', 'Les Statuts des commandes');


// BOXE REFERENCEMENT in includes/boxes/referencement.php
define('BOX_REFERENCEMENT_TITRE', 'Outils Referencements');
define('BOX_REFERENCEMENT_GOOGLE_SITEMAPS', 'GOOGLE SiteMaps');

// BOXE COMPTABILITE in includes/boxes/comptabilite.php
define('BOX_COMPTABILITE_TITRE', 'Comptabilit�');
define('BOX_COMPTABILITE_Z', 'Z de Caisse');
define('BOX_COMPTABILITE_EXPORT', 'Export des factures');

// BOXE NEWSLETTER in includes/boxes/newsletter.php
define('BOX_NEWSLETTER_TITRE', 'Gestion Newsletter');
define('BOX_NEWSLETTER_GESTION', 'Gestion Newsletter');
define('BOX_NEWSLETTER_CREER', 'Cr�er Newsletter');
define('BOX_NEWSLETTER_ENVOI', 'Envoyer Newsletter');
define('BOX_REPORTS_NEWSLETTER', 'Rapport Newsletter');





// javascript messages
define('JS_ERROR', 'Des erreurs sont survenues durant le traitement de votre formulaire !\nMerci de faire les corrections suivantes :\n\n');

define('JS_OPTIONS_VALUE_PRICE', '* Le nouveau attribut produit necessite un prix\n');
define('JS_OPTIONS_VALUE_PRICE_PREFIX', '* Le nouveau attribut produit necessite un pr&eacute;fixe de prix\n');

define('JS_PRODUCTS_NAME', '*  Le nouveau produit necessite un nom\n');
define('JS_PRODUCTS_DESCRIPTION', '* Le nouveau produit necessite une description\n');
define('JS_PRODUCTS_PRICE', '* Le nouveau produit necessite un prix\n');
define('JS_PRODUCTS_WEIGHT', '* Le nouveau produit necessite un poids\n');
define('JS_PRODUCTS_QUANTITY', '* Le nouveau produit necessite une quantit�\n');
define('JS_PRODUCTS_MODEL', '* Le nouveau produit necessite un mod�le\n');
define('JS_PRODUCTS_IMAGE', '* Le nouveau produit necessite une image\n');

define('JS_SPECIALS_PRODUCTS_PRICE', '* Un nouveau prix pour ce produit doit �tre fix�\n');

define('JS_GENDER', '* La valeur de \'Genre\' doit �tre choisie.\n');
define('JS_FIRST_NAME', '* L\'entr�e \'Pr�nom\' doit avoir au moins ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caract�res.\n');
define('JS_LAST_NAME', '* L\'entr�e \'Nom\' doit avoir au moins ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caract�res.\n');
define('JS_DOB', '* L\'entr�e \'Date de naissance\' doit avoir la forme: xx/xx/xxxx (10/09/1975).\n');
define('JS_EMAIL_ADDRESS', '* L\'entr�e \'Adresse �lectronique\' doit avoir au moins ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' caract�res.\n');
define('JS_ADDRESS', '* L\'entr�e \'Adresse\' doit avoir au moins ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caract�res.\n');
define('JS_POST_CODE', '* L\'entr�e \'Code postal\' doit avoir au moins ' . ENTRY_POSTCODE_MIN_LENGTH . ' caract�res.\n');
define('JS_CITY', '* L\'entr�e \'Ville\' doit avoir au moins ' . ENTRY_CITY_MIN_LENGTH . ' caract�res.\n');
define('JS_STATE', '* L\'entr�e \'Etat\' doit avoir �t� choisie.\n');
define('JS_STATE_SELECT', '-- Choisissez ci-dessus --');
define('JS_ZONE', '* L\'entr�e \'Etat\' doit �tre choisie parmi la liste pour ce pays.');
define('JS_COUNTRY', '* La valeur \'Pays\' doit �tre choisie.\n');
define('JS_TELEPHONE', '* L\'entr�e \'Num�ro de t�l�phone\' doit avoir au moins ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caract�res.\n');
define('JS_PASSWORD', '* Les entr�es \'Mot de passe\' et \'Confirmation\' doivent avoir au moins ' . ENTRY_PASSWORD_MIN_LENGTH . ' caract�res.\n');

define('JS_ORDER_DOES_NOT_EXIST', 'Le num�ro de commande %s n\'existe pas !');

define('CATEGORY_PERSONAL', 'Donn&eacute;es personnelles');
define('CATEGORY_ADDRESS', 'Adresse');
define('CATEGORY_CONTACT', 'Contact');
define('CATEGORY_COMPANY', 'Soci&eacute;t&eacute;');
define('CATEGORY_OPTIONS', 'Options');

define('ENTRY_GENDER', 'Genre :');
define('ENTRY_GENDER_ERROR', '&nbsp;<span class="errorText">requis</span>');
define('ENTRY_FIRST_NAME', 'Pr&eacute;nom :');
define('ENTRY_FIRST_NAME_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caract.</span>');
define('ENTRY_LAST_NAME', 'Nom :');
define('ENTRY_LAST_NAME_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caract.</span>');
define('ENTRY_DATE_OF_BIRTH', 'Date de naissance :');
define('ENTRY_DATE_OF_BIRTH_ERROR', '&nbsp;<span class="errorText">(ex. 21/05/1970)</span>');
define('ENTRY_EMAIL_ADDRESS', 'Adresse email :');
define('ENTRY_EMAIL_ADDRESS_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' caract.</span>');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', '&nbsp;<span class="errorText">L\'adresse �lectronique ne semble pas �tre valide!</span>');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', '&nbsp;<span class="errorText">Cette adresse �lectronique existe d�j�!</span>');
define('ENTRY_COMPANY', 'Nom de la soci&eacute;t&eacute; :');
define('ENTRY_COMPANY_ERROR', '');
define('ENTRY_STREET_ADDRESS', 'Adresse :');
define('ENTRY_STREET_ADDRESS_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caract.</span>');
define('ENTRY_SUBURB', 'Compl&eacute;ment adresse :');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_POST_CODE', 'Code postal :');
define('ENTRY_POST_CODE_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_POSTCODE_MIN_LENGTH . ' caract.</span>');
define('ENTRY_CITY', 'Ville :');
define('ENTRY_CITY_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_CITY_MIN_LENGTH . ' caract.</span>');
define('ENTRY_STATE', 'Etat :');
define('ENTRY_STATE_ERROR', '&nbsp;<span class="errorText">requis</span>');
define('ENTRY_COUNTRY', 'Pays :');
define('ENTRY_COUNTRY_ERROR', '');
define('ENTRY_TELEPHONE_NUMBER', 'T&eacute;l&eacute;phone portable :');
define('ENTRY_TELEPHONE_FIXE_NUMBER', 'T&eacute;l&eacute;phone fixe :');
define('ENTRY_LIVRAISON_METHOD', 'M&eacute;thode de livraison :');
define('ENTRY_TELEPHONE_NUMBER_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caract.</span>');
define('ENTRY_FAX_NUMBER', 'T�l�phone fixe :');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_NEWSLETTER', 'Bulletin d\'informations :');
define('ENTRY_NEWSLETTER_YES', 'Abonn&eacute;');
define('ENTRY_NEWSLETTER_NO', 'D&eacute;sabonn&eacute;');
define('ENTRY_NEWS_SMS', 'Reception SMS :');
define('ENTRY_NEWS_SMS_YES', 'Abonn&eacute;');
define('ENTRY_NEWS_SMS_NO', 'D&eacute;sabonn&eacute;');
define('ENTRY_NEWSLETTER_ERROR', '');

// Contribution ajout client & cr�ation de commande
define('BOX_HEADING_MANUAL_ORDER', 'Commande Manuelle');
define('BOX_MANUAL_ORDER_CREATE_ACCOUNT', 'Cr�er un compte');
define('BOX_MANUAL_ORDER_CREATE_ORDER', 'Cr�er une commande');

define('PULL_DOWN_DEFAULT', 'Choisissez');
define('TYPE_BELOW', 'Tapez ci-dessous');

define('JS_ERROR', 'Des erreurs sont survenues durant le traitement de votre formulaire.\n\nVeuillez effectuer les corrections suivantes :\n\n');
define('JS_GENDER', '* La valeur du genre doit �tre selectionn�e.\n');
define('JS_FIRST_NAME', '* Le nom doit avoit au moins ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' lettres.\n');
define('JS_LAST_NAME', '* Le pr�nom doit avoir au moins ' . ENTRY_LAST_NAME_MIN_LENGTH . ' lettres.\n');
define('JS_DOB', '* Votre date de naissance doit avoir ce format: JJ/MM/AAAA (ex. 03/02/1961)\n');
define('JS_EMAIL_ADDRESS', '* Votre adresse email doit contenir un minimum de ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' caract�res.\n');
define('JS_ADDRESS', '* Votre adresse doit contenir un minimum de ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caract�res.\n');
define('JS_POST_CODE', '* Le code postal doit avoir au moins ' . ENTRY_POSTCODE_MIN_LENGTH . ' num�ros.\n');
define('JS_CITY', '* Le champ Ville doit avoir au moins ' . ENTRY_CITY_MIN_LENGTH . ' lettres.\n');
define('JS_STATE', '* Le pays doit �tre s�lectionn�.\n');
define('JS_STATE_SELECT', '-- Choisissez --');
define('JS_ZONE', '* Veuillez choisir un pays � partir de la liste d�roulante.\n');
define('JS_COUNTRY', '* Veuillez choisir un pays � partir de la liste d�roulante.\n');
define('JS_TELEPHONE', '* Votre num�ro de t�l�phone doit contenir un minimum de ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caract�res.\n');
define('JS_PASSWORD', '* Votre mot de passe doit contenir un minimum de ' . ENTRY_PASSWORD_MIN_LENGTH . ' caract�res.\n');

define('CATEGORY_COMPANY', 'D&eacute;tails soci&eacute;t&eacute;');
define('CATEGORY_PERSONAL', 'Vos d&eacute;tails personnels');
define('CATEGORY_ADDRESS', 'Votre adresse');
define('CATEGORY_CONTACT', 'Vos coordonn&eacute;es');
define('CATEGORY_OPTIONS', 'Options');
define('CATEGORY_PASSWORD', 'Votre mot de passe');
define('CATEGORY_CORRECT', 'Si c\'est la bonne personne, Confirmez en cliquant ci-dessous.');
define('ENTRY_CUSTOMERS_ID', 'N&deg; identifiant client :');
define('ENTRY_CUSTOMERS_ID_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_COMPANY', 'Nom de la soci&eacute;t&eacute; :');
define('ENTRY_COMPANY_ERROR', '');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_GENDER', 'Civilit&eacute;es :');
define('ENTRY_GENDER_ERROR', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_GENDER_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_FIRST_NAME', 'Pr&eacutenom :');
define('ENTRY_FIRST_NAME_ERROR', '&nbsp;<small><font color="#FF0000">minimum ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caract&egrave;res.</font></small>');
define('ENTRY_FIRST_NAME_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_LAST_NAME', 'Nom :');
define('ENTRY_LAST_NAME_ERROR', '&nbsp;<small><font color="#FF0000">minimum ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caract&egrave;res.</font></small>');
define('ENTRY_LAST_NAME_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_DATE_OF_BIRTH', 'Date de naissance :');
define('ENTRY_DATE_OF_BIRTH_ERROR', '&nbsp;<small><font color="#FF0000">(ex. 03/02/1961)</font></small>');
define('ENTRY_DATE_OF_BIRTH_TEXT', '&nbsp;<small>(ex. 03/02/1961) <font color="#AABBDD">requis</font></small>');
define('ENTRY_EMAIL_ADDRESS', 'Adresse E-Mail :');
define('ENTRY_EMAIL_ADDRESS_ERROR', '&nbsp;<small><font color="#FF0000">minimum ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' caract&egrave;res.</font></small>');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', '&nbsp;<small><font color="#FF0000">Votre adresse email ne semble pas �tre valide - veuillez effectuer toutes les corrections n&eacute;cessaires.</font></small>');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', '&nbsp;<small><font color="#FF0000">Votre adresse email est d&eacute;j&agrave; enregistr&eacute;e sur notre site - Veuillez ouvrir une session avec cette adresse email ou cr&eacute;ez un compte avec une adresse diff&eacute;rente.</font></small>');
define('ENTRY_EMAIL_ADDRESS_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_STREET_ADDRESS', 'Adresse :');
define('ENTRY_STREET_ADDRESS_ERROR', '&nbsp;<small><font color="#FF0000">minimum ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caract&egrave;res.</font></small>');
define('ENTRY_STREET_ADDRESS_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_SUBURB', 'Compl&eacute;ment d\'adresse :');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Code postal :');
define('ENTRY_POST_CODE_ERROR', '&nbsp;<small><font color="#FF0000">minimum ' . ENTRY_POSTCODE_MIN_LENGTH . ' caract&egrave;res.</font></small>');
define('ENTRY_POST_CODE_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_CITY', 'Ville :');
define('ENTRY_CITY_ERROR', '&nbsp;<small><font color="#FF0000">minimum ' . ENTRY_CITY_MIN_LENGTH . ' caract&egrave;res.</font></small>');
define('ENTRY_CITY_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_STATE', 'D&eacute;partement :');
define('ENTRY_STATE_ERROR', '&nbsp;<small><font color="#FF0000">requis</font></small>');
define('ENTRY_STATE_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_COUNTRY', 'Pays :');
define('ENTRY_COUNTRY_ERROR', '');
define('ENTRY_COUNTRY_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_TELEPHONE_NUMBER', 'Num&eacute;ro de t&eacute;l&eacute;phone :');
define('ENTRY_TELEPHONE_NUMBER_ERROR', '&nbsp;<small><font color="#FF0000">minimum ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caract&egrave;res.</font></small>');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_FAX_NUMBER', 'Num&eacute;ro de Fax :');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Newsletter:');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'Abonner');
define('ENTRY_NEWSLETTER_NO', 'Non Abonner');
define('ENTRY_NEWSLETTER_ERROR', '');
define('ENTRY_PASSWORD', 'Mot de passe :');
define('ENTRY_PASSWORD_CONFIRMATION', 'Confirmer votre mot de passe :');
define('ENTRY_PASSWORD_CONFIRMATION_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('ENTRY_PASSWORD_ERROR', '&nbsp;<small><font color="#FF0000">minimum ' . ENTRY_PASSWORD_MIN_LENGTH . ' caract&egrave;res.</font></small>');
define('ENTRY_PASSWORD_TEXT', '&nbsp;<small><font color="#AABBDD">requis</font></small>');
define('PASSWORD_HIDDEN', '--INVISIBLE--');

define('IMAGE_BUTTON_BACK', 'Retour');
define('IMAGE_BUTTON_CONFIRM', 'Confirmer');
// ########## END - Ajout/edite commande et compte client ##########

// images
define('IMAGE_ANI_SEND_EMAIL', 'Envoyer un courrier &eacute;lectronique');
define('IMAGE_BACK', 'Retour');
define('IMAGE_BACKUP', 'Sauvegarde');
define('IMAGE_CANCEL', 'Annuler');
define('IMAGE_CONFIRM', 'Confirmer');
define('IMAGE_COPY', 'Copier');
define('IMAGE_COPY_TO', 'Copier vers');
define('IMAGE_DETAILS', 'D&eacute;tails');
define('IMAGE_DELETE', 'Supprimer');
define('IMAGE_EDIT', 'Editer');
define('IMAGE_EMAIL', 'Adresse Email');
define('IMAGE_FILE_MANAGER', 'Gestionnaire de fichiers');
define('IMAGE_ICON_STATUS_GREEN', 'Actif');
define('IMAGE_ICON_STATUS_GREEN_LIGHT', 'Activer');
define('IMAGE_ICON_STATUS_RED', 'Inactif');
define('IMAGE_ICON_STATUS_RED_LIGHT', 'D&eacute;sactiver');
define('IMAGE_ICON_INFO', 'Info');
define('IMAGE_INSERT', 'Ins&eacute;rer');
define('IMAGE_LOCK', 'Verouilller');
define('IMAGE_MODULE_INSTALL', 'Installez le module');
define('IMAGE_MODULE_REMOVE', 'Supprimer le module');
define('IMAGE_MOVE', 'D&eacute;placer');
define('IMAGE_NEW_BANNER', 'Nouvelle banni&egrave;re');
define('IMAGE_NEW_CATEGORY', 'Nouvelle cat&eacute;gorie');
define('IMAGE_NEW_COUNTRY', 'Nouveau pays');
define('IMAGE_NEW_CURRENCY', 'Nouvelle devises');
define('IMAGE_NEW_FILE', 'Nouveau fichier');
define('IMAGE_NEW_FOLDER', 'Nouveau dossier');
define('IMAGE_NEW_LANGUAGE', 'Nouvelle Langue');
define('IMAGE_NEW_NEWSLETTER', 'Nouveau bulletin d\'informations');
define('IMAGE_NEW_PRODUCT', 'Nouveau Produit');
define('IMAGE_NEW_TAX_CLASS', 'Nouvelle classe fiscale');
define('IMAGE_NEW_TAX_RATE', 'Nouveau taux fiscal');
define('IMAGE_NEW_TAX_ZONE', 'Nouvelle zone fiscale');
define('IMAGE_NEW_ZONE', 'Nouvelle zone');
define('IMAGE_ORDERS', 'Commandes');
define('IMAGE_ORDERS_INVOICE', 'Facture');
define('IMAGE_ORDERS_PACKINGSLIP', 'Emballage');
define('IMAGE_PREVIEW', 'Pr&eacute;visualiser');
define('IMAGE_RESTORE', 'Restaurer');
define('IMAGE_RESET', 'R&eacute;initialiser');
define('IMAGE_SAVE', 'Sauvegarder');
define('IMAGE_SEARCH', 'rechercher');
define('IMAGE_SELECT', 'Choisir');
define('IMAGE_SEND', 'Envoyer');
define('IMAGE_SEND_EMAIL', 'Envoyer un email');
define('IMAGE_UNLOCK', 'D&eacute;verrouiller');
define('IMAGE_UPDATE', 'Mettre &agrave; jour');
define('IMAGE_UPDATE_CURRENCIES', 'Mettre &agrave; jour le taux de change');
define('IMAGE_UPLOAD', 'Transf&eacute;rer');

define('ICON_CROSS', 'Faux');
define('ICON_CURRENT_FOLDER', 'Dossier courant');
define('ICON_DELETE', 'Supprimer');
define('ICON_ERROR', 'Erreur');
define('ICON_FILE', 'Fichier');
define('ICON_FILE_DOWNLOAD', 'T&eacute;l&eacute;charger');
define('ICON_FOLDER', 'Dossier');
define('ICON_LOCKED', 'Verrouill&eacute;');
define('ICON_PREVIOUS_LEVEL', 'Niveau pr&eacute;c&eacute;dent');
define('ICON_PREVIEW', 'Pr&eacute;visualiser');
define('ICON_STATISTICS', 'Statistiques');
define('ICON_SUCCESS', 'Succ&egrave;s');
define('ICON_TICK', 'Vrai');
define('ICON_UNLOCKED', 'D&eacute;verrouill&eacute;');
define('ICON_WARNING', 'Attention');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Page %s sur %d');
define('TEXT_DISPLAY_NUMBER_OF_BANNERS', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> banni&egrave;res)');
define('TEXT_DISPLAY_NUMBER_OF_COUNTRIES', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> pays)');
define('TEXT_DISPLAY_NUMBER_OF_CUSTOMERS', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> clients)');
define('TEXT_DISPLAY_NUMBER_OF_CURRENCIES', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> devises)');
define('TEXT_DISPLAY_NUMBER_OF_LANGUAGES', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> langues)');
define('TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> fabriquants)');
define('TEXT_DISPLAY_NUMBER_OF_FAMILY', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> famille)');
define('TEXT_DISPLAY_NUMBER_OF_FAMILY_CLIENT', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> famille Client)');
define('TEXT_DISPLAY_NUMBER_OF_NEWSLETTERS', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> bulletin d\'informations)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> commandes)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS_STATUS', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> statut commandes)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> produits)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_EXPECTED', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> produits en attente)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> critiques produit)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> produits en promotion)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_CLASSES', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> classes fiscales)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_ZONES', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> zones fiscales)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_RATES', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> taux fiscal)'); 
define('TEXT_DISPLAY_NUMBER_OF_ZONES', 'Affiche <b>%d</b> &agrave; <b>%d</b> (sur <b>%d</b> zones)');

define('PREVNEXT_BUTTON_PREV', ' <img src="images/icons/previous.gif" border="0" width="25" height="25" align="absmiddle" /> ');
define('PREVNEXT_BUTTON_NEXT', ' <img src="images/icons/next.gif" border="0" width="25" height="25" align="absmiddle" /> ');

define('TEXT_DEFAULT', 'd&eacute;faut');
define('TEXT_SET_DEFAULT', 'mettre par d&eacute;faut');
define('TEXT_FIELD_REQUIRED', '&nbsp;<span class="fieldRequired">*</span>');

define('ERROR_NO_DEFAULT_CURRENCY_DEFINED', 'Erreur : Il n\'y a actuellement aucune devise par d&eacute;faut. Veuillez en choisir une: Outils administration->Localisation->Devises');

define('TEXT_CACHE_CATEGORIES', 'Bo&icirc;te cat&eacute;gories');
define('TEXT_CACHE_MANUFACTURERS', 'Bo&icirc;te fabriquants');
define('TEXT_CACHE_ALSO_PURCHASED', 'Module d\'achat suppl&eacute;mentaire');

define('TEXT_NONE', '--aucun--');
define('TEXT_TOP', 'Haut');

define('ERROR_DESTINATION_DOES_NOT_EXIST', 'Erreur : le chemin cible n\'existe pas.');
define('ERROR_DESTINATION_NOT_WRITEABLE', 'Erreur : Impossible d\'&eacute;crire dans le r&eacute;pertoire cible.');
define('ERROR_FILE_NOT_SAVED', 'Erreur : fichier transf&eacute;rer non sauvegard&eacute;.');
define('ERROR_FILETYPE_NOT_ALLOWED', 'Erreur : type de fichier transf&eacute;r&eacute; non-permis.');
define('SUCCESS_FILE_SAVED_SUCCESSFULLY', 'Succ&egrave;s : Le fichier transf&eacute;r&eacute; a &eacute;t&eacute; sauvegard&eacute; avec succ&egrave;s.');
define('WARNING_NO_FILE_UPLOADED', 'Attention : fichier non transf&eacute;r&eacute;.');
define('WARNING_FILE_UPLOADS_DISABLED', 'Attention : transfert de fichier est d&eacute;sactiv&ea dans le fichier de configuration php.ini.');

// HTC BEGIN 
define('BOX_HEADING_HEADER_TAGS_CONTROLLER', 'Titres des pages et META');
define('BOX_HEADER_TAGS_ADD_A_PAGE', 'Gestion des pages');
define('BOX_HEADER_TAGS_FRENCH', 'Text Control');
//  define('BOX_HEADER_TAGS_ENGLISH', 'Text Control');
define('BOX_HEADER_TAGS_FILL_TAGS', 'Fill Tags');
// HTC END
?>