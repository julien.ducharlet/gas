<?php
/*  Page by Paul
	Vire les <b>, <i>, <u>, R des descriptions des articles*/

require('includes/application_top.php');

function rendre_clean($texte) {
	/*$texte = str_replace("<i>", "", $texte);
	$texte = str_replace("<u>", "", $texte);
	$texte = str_replace("<b>", "", $texte);
	$texte = str_replace("</i>", "", $texte);
	$texte = str_replace("</u>", "", $texte);
	$texte = str_replace("</b>", "", $texte);
	$texte = str_replace(chr(174), "", $texte);*/
	
	$texte = str_replace("#", "[@]", $texte);
	$texte = str_replace("[1] [@]", "[#]", $texte);
	
	$texte = addslashes($texte);
	
	return $texte;
}
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>

<script language="javascript">
function verif() {
	if (window.confirm("Voulez-vous vraiment mettre \340 jour la table ?\n\nINTERDICTION FORMELLE de s'en servir sans l'aval de l'administrateur du site !")){
		document.location.href="<?php echo $_SERVER['PHP_SELF']; ?>?lancer=true"
		return true;
	}
}
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<div style="text-align: center;">
    <?php if ($_GET['effectue']) { ?>
    <div style="background-color: #CFC; border: 1px solid #090; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Base de donn&eacute;es mise &agrave; jour avec <strong>succ&egrave;s</strong> !
    </div>
    <?php } else { ?>
    <div style="background-color: #FFC; border: 1px solid #FC0; margin: 10px; padding: 5px;">
	    <strong>Informations :</strong> Cette Vire les &lt;b&gt;, &lt;i&gt;, &lt;u&gt;, &reg; des descriptions des articles.
    </div>
    
    <br />
    
    <input type="button" value="Lancer la mise &agrave; jour de la base" onClick="verif();"/>
    <?php } ?>

</div>
<?php
if ($_REQUEST['lancer']) {
	/*$res=tep_db_query("SELECT products_id, products_name, products_description, products_url, products_head_title_tag, products_head_desc_tag, products_head_keywords_tag , products_baseline, balise_title_lien_image, balise_title_lien_texte 
					   FROM " . TABLE_PRODUCTS_DESCRIPTION . "");
	while($row=tep_db_fetch_array($res)) {
		$products_name = rendre_clean($row['products_name']);
		$products_description = rendre_clean($row['products_description']);
		$products_url = rendre_clean($row['products_url']);
		$products_head_title_tag = rendre_clean($row['products_head_title_tag']);
		$products_head_desc_tag = rendre_clean($row['products_head_desc_tag']);
		$products_head_keywords_tag  = rendre_clean($row['products_head_keywords_tag']);
		$products_baseline = rendre_clean($row['products_baseline']);
		$balise_title_lien_image = rendre_clean($row['balise_title_lien_image']);
		$balise_title_lien_texte = rendre_clean($row['balise_title_lien_texte']);
		
		tep_db_query("UPDATE " . TABLE_PRODUCTS_DESCRIPTION . " 
					  SET products_name = '" . $products_name . "',products_description = '" . $products_description . "', products_url = '" . $products_url . "', products_head_title_tag = '" . $products_head_title_tag . "', products_head_desc_tag = '" . $products_head_desc_tag . "', products_head_keywords_tag  = '" . $products_head_keywords_tag  . "', products_baseline = '" . $products_baseline . "', balise_title_lien_image = '" . $balise_title_lien_image . "', balise_title_lien_texte = '" . $balise_title_lien_texte . "' WHERE products_id = '" . $row['products_id'] . "'");
	}*/
	
	$res=tep_db_query("SELECT categories_id, categories_name, categories_htc_title_tag, categories_htc_desc_tag, categories_htc_keywords_tag, categories_htc_description, categories_url, categories_baseline, balise_title_lien_image, balise_title_lien_texte 
					   FROM " . TABLE_CATEGORIES_DESCRIPTION . "");
	$i = 0;
	while($row=tep_db_fetch_array($res)) {
		$categories_name = rendre_clean($row['categories_name']);
		$categories_htc_title_tag = rendre_clean($row['categories_htc_title_tag']);
		$categories_htc_desc_tag = rendre_clean($row['categories_htc_desc_tag']);
		$categories_htc_keywords_tag = rendre_clean($row['categories_htc_keywords_tag']);
		$categories_htc_description = rendre_clean($row['categories_htc_description']);
		$categories_url = rendre_clean($row['categories_url']);
		$categories_baseline = rendre_clean($row['categories_baseline']);
		$balise_title_lien_image = rendre_clean($row['balise_title_lien_image']);
		$balise_title_lien_texte = rendre_clean($row['balise_title_lien_texte']);
		
		tep_db_query("UPDATE " . TABLE_CATEGORIES_DESCRIPTION . " 
					  SET categories_name = '" . $categories_name . "',categories_htc_title_tag = '" . $categories_htc_title_tag . "', categories_htc_desc_tag = '" . $categories_htc_desc_tag . "', categories_htc_keywords_tag = '" . $categories_htc_keywords_tag . "', categories_htc_description = '" . $categories_htc_description . "', categories_url  = '" . $categories_url  . "', categories_baseline = '" . $categories_baseline . "', balise_title_lien_image = '" . $balise_title_lien_image . "', balise_title_lien_texte = '" . $balise_title_lien_texte . "' WHERE categories_id = '" . $row['categories_id'] . "'");
		$i++;
	}
	?>
	<META HTTP-EQUIV="refresh" content="0;URL='<?php echo $_SERVER['PHP_SELF']; ?>?effectue=true&traites=<?php echo $i; ?>'">
    <?php
}
?>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>