<?php
include("includes/database_tables.php");
require('includes/configure.php');
require('includes/filenames.php');
require('includes/functions/general.php');
require('includes/functions/database.php');
require('includes/functions/html_output.php');

tep_db_connect() or die('Unable to connect to database server!');

$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

?>

<a href="#" class="lbAction" rel="deactivate"><button>Fermer</button></a>
	
<script language="javascript" src="includes/general.js"></script>

<?php
$query = 'SELECT customers_argent_virtuel FROM '. TABLE_CUSTOMERS .' WHERE customers_id='. $_GET['id_client'];
$query = tep_db_query($query);
$data = tep_db_fetch_array($query);
?>
<h1>Faire un versement </h1><hr>
<form name="form" action="<?php echo FILENAME_CLIENT_LIST; ?>" method="post">
<table border="0" width="100%" cellspacing="2" cellpadding="2">
	
    <tbody>
    	<tr>
        	<td align="left" valign="top" class="dataTableContent">
            	<div>Montant pr&eacute;sent dans le porte-monnaie virtuel du client : <?php echo $data['customers_argent_virtuel'];?> Euros</div>
            </td>
        </tr>
		<tr>
        	<td align="left" valign="top" class="dataTableContent">
                <br /> 
                <?php 
                echo '<div style="float:left;width:150px;">Montant HT :</div>'. tep_draw_input_field('versement_somme', '', 'maxlength="32"');
                ?>
            </td>
    	</tr>
        <tr>
        	<td align="left" valign="top" class="dataTableContent">
			<?php
            echo '<div style="float:left;width:150px;">Raison Interne &nbsp; :</div>
					<textarea id="versement_raison" name="versement_raison" wrap="soft" cols="70" rows="3"></textarea>';
            ?>
            </td>
        </tr>
        <tr>
        	<td align="left" valign="top" class="dataTableContent">
			<?php
            echo '<div style="float:left;width:150px;">Raison Client (affich&eacute;e) &nbsp; :</div>
						<textarea id="versement_raison_client" name="versement_raison_client" wrap="soft" cols="70" rows="3"></textarea>';
            ?>
            </td>
        </tr>
		<tr>
    		<td valign="top" class="dataTableContent" align="center">
                <a href="#" onClick="if(document.form.versement_raison.value.length<50){alert('Raison trop courte');}else{document.form.submit();}">
                <img src='<?php echo DIR_WS_IMAGES;?>/bouton_crediter.png ' border="0" />
                </a>
                <input type="hidden" name="id_client" value="<?php echo $_REQUEST["id_client"];?>">
                <input type="hidden" name="action" value="add_versement">
            </td>
		</tr>
	</tbody>
</table>
<h1>Liste des versements</h1><hr>

<?php 
$query_liste = "SELECT * FROM ".TABLE_CUSTOMERS_ARGENT." WHERE customers_id='".$_REQUEST["id_client"]."' ORDER BY id_versement DESC";
$res_liste = tep_db_query($query_liste);
?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
	<thead>
		<tr style="border-bottom: 1px solid #000000">
			<th align="center" class="dataTableContent" style=" padding: 10px;">Date</th>
			<th align="center" class="dataTableContent" style="background-color:#FFFFFF;">Somme</th>
			<th align="center" class="dataTableContent">Raison</th>
			<th align="center" class="dataTableContent" style="background-color:#FFFFFF;">Raison Client</th>
			<th align="center" class="dataTableContent">Utilisateur</th>
		</tr>
	</thead>
    <tbody style="height:200px;overflow-x:hidden;overflow-y:auto;">
		<?php
		while($res_liste && $row_liste=tep_db_fetch_array($res_liste)) {
			
			$row_liste_raison = mb_convert_encoding($row_liste["raison"], "UTF-8");
			$row_liste_raison_client = mb_convert_encoding($row_liste["raison_client"], "UTF-8");

			echo "<tr><td align='center' class='smallText' style='width:80px; padding:20px 0 10px 0;'>". date('d-m-Y', strtotime($row_liste['date_versement'])) ."</td>";
			echo "<td align='center' class='smallText' style='background-color:#FFFFFF;'>". $row_liste["somme"]."</td>";
			echo "<td align='center' class='smallText' style='padding:0 20px 0 20px;'>".$row_liste_raison."</td>";
			echo "<td align='center' class='smallText' style='background-color:#FFFFFF;  padding:0 20px 0 20px;'>".$row_liste_raison_client."</td>";
			$res_user=tep_db_query("select admin_firstname from admin where admin_id='".$row_liste["user_id"]."'");
			$row_user=tep_db_fetch_array($res_user);
			if (!empty($row_user["admin_firstname"])) {
				echo "<td align='center' class='smallText'>".$row_user["admin_firstname"]."</td></tr>";
			} else {
				echo "<td align='center' class='smallText'>System</td></tr>";
			}
            
        }?>
    </tbody>
</table>
</form>

