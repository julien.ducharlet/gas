<?php
/*
OSC-EXPEDITOR by ASOU v1.0
Inspired by batch-invoice-printing-v1.1 by PandA.n1

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

$TRANCHE_SUP=79.99; // au DESSUS du montant on envoi AVEC remise contre signature
$TRANCHE_MOINS=80; // au DESSOUS du montant on envoi SANS remise contre signature

$TRANCHE_SUP_OM=79.99; // au DESSUS du montant on envoi AVEC remise contre signature
$TRANCHE_MOINS_OM=80; // au DESSOUS du montant on envoi SANS remise contre signature

$COLD = 'COLD';
$COL = 'COL';
$COLI = 'COLI';
$COM = 'COM';
$CDS = 'CDS';

// Get customer id
  	$oID = tep_db_prepare_input($oID);
  	$customers_query = tep_db_query('select customers_id from '. TABLE_ORDERS .' where orders_id = \''. (int)$oID .'\'');
	$customers = tep_db_fetch_array($customers_query);
	$customers_id = $customers['customers_id'];
	
	$oID = tep_db_prepare_input($oID);

	if(empty($_SESSION['commandes'])) {
		
		$shipping_total_weight_query = 'select poids
									from '. TABLE_ORDERS .'
									where orders_id = \''. (int)$oID .'\'
									group by orders_id';
	}
	else {
		
		$shipping_total_weight_query = 'select sum(products_weight*products_quantity_saisie) as poids
									from '. TABLE_ORDERS_PRODUCTS .'
									where orders_id = \''. (int)$oID .'\'
									and products_quantity_sent<=products_quantity
									and products_quantity_saisie>0
									group by orders_id';
	}
  	$shipping_total_weight_query = tep_db_query($shipping_total_weight_query);

	$shipping_total_weight = tep_db_fetch_array($shipping_total_weight_query);
	$shipping_total_weight = $shipping_total_weight['poids'];
	
	if($shipping_total_weight < 0.250) $shipping_total_weight = 0.250;

// Get order id and shipping infos
  $oID = tep_db_prepare_input($oID);
  $orders_query = tep_db_query('select orders_id from '. TABLE_ORDERS .' where orders_id = \''. (int)$oID .'\'');
$orders = tep_db_fetch_array($orders_query);
$order_id = $orders['orders_id'];
  $order = new order($oID);
	
// Get total of order
  $oID = tep_db_prepare_input($oID);
  $total = tep_db_query('select total from orders where orders_id = \''. (int)$oID .'\'');
  $TOTAL= tep_db_fetch_array($total);
  $MONTANT_TOTAL = $TOTAL['total'];
	

?>
<?php echo $HTTP_GET_VARS['oID']; ?>
<?
if (isset($order->delivery['name']) && tep_not_null($order->delivery['name'])) {
   $name = $order->delivery['name'];
} elseif (isset($order->customer['name']) && tep_not_null($order->delivery['name'])) {
   $name = $order->customer['name'];
}

echo $name;?>;

<?php echo $order->delivery['company'];?>;
<?php echo $order->delivery['street_address'];?>;
<?php echo $order->delivery['suburb'];?>;
<?php echo $order->delivery['postcode'];?>;
<?php echo $order->delivery['city'];?>;<?php 

//recuperation du code iso
$code_iso='select countries_iso_code_2  from countries where countries_name=\''.$order->delivery['country'].'\'';
$resultat_code_iso=mysql_query($code_iso);
$CODE_ISO=mysql_fetch_array($resultat_code_iso);

echo $ISO=$CODE_ISO['countries_iso_code_2'];
//fin recuperation

?>;
<?php echo $TOTAL['total']; ?>;
<?php echo $order->customer['telephone'];?>;
<?php echo $order->customer['telephone'];?>;
<?php echo $customers_id.'-'.$order_id; ?>;
<?php echo $order_id;?>;
<?php echo $shipping_total_weight; ?>;<?php 

if($ISO=='FR' and $MONTANT_TOTAL<$TRANCHE_MOINS){
	echo $COLD;
} else if ($ISO=='FR' and $MONTANT_TOTAL>$TRANCHE_SUP) {
	echo $COL;
} else if($ISO=='BE' or $ISO=='DE' or $ISO=='LU' or $ISO=='NL' or $ISO=='ES' or $ISO=='IT' or $ISO=='GB' or $ISO=='AT' or $ISO=='DK' or $ISO=='IE' or $ISO=='PT' or $ISO=='FI' or $ISO=='NO' or $ISO=='SE' or $ISO=='CH'){
	echo $COLI;
} else if(($ISO=='GP' or $ISO=='MQ' or $ISO=='GF' or $ISO=='RE' or $ISO=='YT' or $ISO=='PM' or $ISO=='NC' or $ISO=='PF' or $ISO=='WF') and $MONTANT_TOTAL<$TRANCHE_MOINS_OM){
	echo $COM;
} else if(($ISO=='GP' or $ISO=='MQ' or $ISO=='GF' or $ISO=='RE' or $ISO=='YT' or $ISO=='PM' or $ISO=='NC' or $ISO=='PF' or $ISO=='WF') and $MONTANT_TOTAL>$TRANCHE_SUP_OM){
	echo $CDS;
}
?><br/>



<? /*
Remplacer (a la fin) COLD par une des ligne suivante : 
Pour trouver le code ISO il faut utiliser dans la table orders le champ delivery_country et rechercher l'equilalent dans la table countries champ countries_name et recup�rer le champ countries_iso_code_2

COLD : (Access France) quand le colis est a livrer pour les code ISO : FR et qui font moins/ou de 80 Euros 
ou
COL : (Expert France) quand le colis est a livrer pour les code ISO : FR et qui font plus de 80 Euros 
ou


COLI : (Expert Inter) quand le colis est a livrer pour les code ISO : BE, DE, LU, NL, ES, IT, GB, AT, DK, IE, PT, FI, NO, SE, CH
ou
COM : (Access OM) quand le colis est a livrer pour les code ISO : GP, MQ, GF, RE, YT, PM, NC, PF, WF qui font moins/ou de 80 Euros 
ou
CDS : (Expert OM) quand le colis est a livrer pour les code ISO : GP, MQ, GF, RE, YT, PM, NC, PF, WF et qui font plus de 80 Euros 
 */ ?>

 
