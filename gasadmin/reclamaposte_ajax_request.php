<?php
/*
  $Id: reclamaposte_ajax_request.php, v 2.2 11/03/2009 phocea (forum oscommerce-fr.info) $

  This script is not included in the original version of osCommerce

  osCommerce, Open Source E-Commerce Solutions
  https://www.oscommerce.com

  Copyright (c) 2003 osCommerce

*/

  require('includes/application_top.php');
	include(DIR_WS_FUNCTIONS . 'reclamaposte.php');
	
	$order_id = $_GET['orderid'];
	$tracking_id = $_GET['tracknum'];
	$action = $_GET['action'];
	

	switch ($action) {
		case 'late':
			$newstatus = COLIPOSTE_LIVRAISON_RETARD;
			break;
		case 'intime':
			$newstatus = COLIPOSTE_LIVRAISON_OK;
			break;
		case 'cash':
			$newstatus = COLIPOSTE_RECLAMATION_REMBOURSE;
			break;
		case 'reset':
			$newstatus = '';
			break;
		default:
		$newstatus = -1;
			break;
	}	
	
	if ($newstatus != '') {
		$orders_query = 'update ' . TABLE_ORDERS_STATUS_HISTORY . ' set laposte_status = "' . $newstatus . '" where orders_id = "' . $order_id . '" and ' . TRACKING_DB_FIELD . ' = "'.$tracking_id.'"  ; ';
	} else {
		$orders_query = 'update ' . TABLE_ORDERS_STATUS_HISTORY . ' set laposte_status = NULL, laposte_md5 = NULL where orders_id = "' . $order_id . '" and ' . TRACKING_DB_FIELD . ' = "'.$tracking_id.'"  ; ';
	}
	$orders_query_r = tep_db_query($orders_query) ;

  echo getImageforStatus($newstatus);
?>